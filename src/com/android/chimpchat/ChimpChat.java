package com.android.chimpchat;

import com.android.chimpchat.adb.AdbBackend;
import com.android.chimpchat.core.IChimpBackend;
import com.android.chimpchat.core.IChimpDevice;
import java.util.Map;
import java.util.TreeMap;
//猩猩聊天
public class ChimpChat {
   private final IChimpBackend mBackend;
   private static String sAdbLocation;
   private static boolean sNoInitAdb;

   private ChimpChat(IChimpBackend backend) {
      this.mBackend = backend;
   }

   public static ChimpChat getInstance(Map<String, String> options) {
      sAdbLocation = (String)options.get("adbLocation");
      sNoInitAdb = Boolean.valueOf((String)options.get("noInitAdb")).booleanValue();
      IChimpBackend backend = createBackendByName((String)options.get("backend"));
      if(backend == null) {
         return null;
      } else {
         ChimpChat chimpchat = new ChimpChat(backend);
         return chimpchat;
      }
   }

   public static ChimpChat getInstance() {
      TreeMap options = new TreeMap();
      options.put("backend", "adb");
      return getInstance(options);
   }
   //ChimpBackend就是adb
   private static IChimpBackend createBackendByName(String backendName) {
      return "adb".equals(backendName)?new AdbBackend(sAdbLocation, sNoInitAdb):null;
   }

   public IChimpDevice waitForConnection(long timeoutMs, String deviceId) {
      return this.mBackend.waitForConnection(timeoutMs, deviceId);
   }

   public IChimpDevice waitForConnection() {
      return this.mBackend.waitForConnection(2147483647L, ".*");
   }

   public void shutdown() {
      this.mBackend.shutdown();
   }
}
