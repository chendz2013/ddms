package com.android.chimpchat.hierarchyviewer;

import com.android.ddmlib.IDevice;
import com.android.ddmlib.Log;
import com.android.hierarchyviewerlib.device.DeviceBridge;
import com.android.hierarchyviewerlib.device.ViewServerDevice;
import com.android.hierarchyviewerlib.models.ViewNode;
import com.android.hierarchyviewerlib.models.Window;
import com.android.hierarchyviewerlib.models.ViewNode.Property;
import java.util.Iterator;
import org.eclipse.swt.graphics.Point;

public class HierarchyViewer {
   public static final String TAG = "hierarchyviewer";
   private IDevice mDevice;

   public HierarchyViewer(IDevice device) {
      this.mDevice = device;
      this.setupViewServer();
   }

   private void setupViewServer() {
      DeviceBridge.setupDeviceForward(this.mDevice);
      if(!DeviceBridge.isViewServerRunning(this.mDevice) && !DeviceBridge.startViewServer(this.mDevice)) {
         try {
            Thread.sleep(2000L);
         } catch (InterruptedException var2) {
            ;
         }

         if(!DeviceBridge.startViewServer(this.mDevice)) {
            Log.e("hierarchyviewer", "Unable to debug device " + this.mDevice);
            throw new RuntimeException("Could not connect to the view server");
         }
      } else {
         DeviceBridge.loadViewServerInfo(this.mDevice);
      }
   }

   public ViewNode findViewById(String id) {
      ViewNode rootNode = DeviceBridge.loadWindowData(new Window(new ViewServerDevice(this.mDevice), "", -1));
      if(rootNode == null) {
         throw new RuntimeException("Could not dump view");
      } else {
         return this.findViewById(id, rootNode);
      }
   }

   public ViewNode findViewById(String id, ViewNode rootNode) {
      if(rootNode.id.equals(id)) {
         return rootNode;
      } else {
         Iterator i$ = rootNode.children.iterator();

         ViewNode found;
         do {
            if(!i$.hasNext()) {
               return null;
            }

            ViewNode child = (ViewNode)i$.next();
            found = this.findViewById(id, child);
         } while(found == null);

         return found;
      }
   }

   public String getFocusedWindowName() {
      int id = DeviceBridge.getFocusedWindow(this.mDevice);
      Window[] windows = DeviceBridge.loadWindows(new ViewServerDevice(this.mDevice), this.mDevice);
      Window[] arr$ = windows;
      int len$ = windows.length;

      for(int i$ = 0; i$ < len$; ++i$) {
         Window w = arr$[i$];
         if(w.getHashCode() == id) {
            return w.getTitle();
         }
      }

      return null;
   }

   public static Point getAbsolutePositionOfView(ViewNode node) {
      int x = node.left;
      int y = node.top;

      for(ViewNode p = node.parent; p != null; p = p.parent) {
         x += p.left - p.scrollX;
         y += p.top - p.scrollY;
      }

      return new Point(x, y);
   }

   public static Point getAbsoluteCenterOfView(ViewNode node) {
      Point point = getAbsolutePositionOfView(node);
      return new Point(point.x + node.width / 2, point.y + node.height / 2);
   }

   public boolean visible(ViewNode node) {
      boolean ret = node != null && node.namedProperties.containsKey("getVisibility()") && "VISIBLE".equalsIgnoreCase(((Property)node.namedProperties.get("getVisibility()")).value);
      return ret;
   }

   public String getText(ViewNode node) {
      if(node == null) {
         throw new RuntimeException("Node not found");
      } else {
         Property textProperty = (Property)node.namedProperties.get("text:mText");
         if(textProperty == null) {
            textProperty = (Property)node.namedProperties.get("mText");
            if(textProperty == null) {
               throw new RuntimeException("No text property on node");
            }
         }

         return textProperty.value;
      }
   }
}
