package com.android.chimpchat.core;

import com.android.chimpchat.ChimpManager;
import com.android.chimpchat.core.ChimpView;
import com.android.chimpchat.core.IChimpView;
import com.android.chimpchat.core.IMultiSelector;
import com.google.common.collect.Lists;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MultiSelectorText implements IMultiSelector {
   private static final Logger LOG = Logger.getLogger(ChimpView.class.getName());
   private String text;

   public MultiSelectorText(String text) {
      this.text = text;
   }

   public Collection<IChimpView> getViews(ChimpManager manager) {
      String response;
      List ids;
      try {
         response = manager.getViewsWithText(this.text);
         ids = Arrays.asList(response.split(" "));
      } catch (IOException var8) {
         LOG.log(Level.SEVERE, "Error communicating with device: " + var8.getMessage());
         return new ArrayList();
      }

      if(ids.size() % 2 != 0) {
         LOG.log(Level.SEVERE, "Error retrieving views: " + response);
         return Collections.emptyList();
      } else {
         ArrayList views = new ArrayList();

         for(int i = 0; i < ids.size() / 2; ++i) {
            ArrayList accessibilityIds = Lists.newArrayList(new String[]{(String)ids.get(2 * i), (String)ids.get(2 * i + 1)});
            ChimpView view = new ChimpView("accessibilityids", accessibilityIds);
            view.setManager(manager);
            views.add(view);
         }

         return views;
      }
   }
}
