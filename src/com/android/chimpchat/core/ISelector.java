package com.android.chimpchat.core;

import com.android.chimpchat.ChimpManager;
import com.android.chimpchat.core.IChimpView;

public interface ISelector {
   IChimpView getView(ChimpManager var1);
}
