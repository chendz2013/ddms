package com.android.chimpchat.core;

public class ChimpRect {
   public int left;
   public int top;
   public int right;
   public int bottom;

   public ChimpRect() {
   }

   public ChimpRect(int left, int top, int right, int bottom) {
      this.left = left;
      this.top = top;
      this.right = right;
      this.bottom = bottom;
   }

   public boolean equals(Object obj) {
      if(obj instanceof ChimpRect) {
         ChimpRect r = (ChimpRect)obj;
         if(r != null) {
            return this.left == r.left && this.top == r.top && this.right == r.right && this.bottom == r.bottom;
         }
      }

      return false;
   }

   public int getWidth() {
      return this.right - this.left;
   }

   public int getHeight() {
      return this.bottom - this.top;
   }

   public int[] getCenter() {
      int[] center = new int[]{this.left + this.getWidth() / 2, this.top + this.getHeight() / 2};
      return center;
   }

   public String toString() {
      StringBuilder sb = new StringBuilder();
      sb.append("ChimpRect ");
      sb.append("top: ").append(this.top).append(" ");
      sb.append("right: ").append(this.right).append(" ");
      sb.append("bottom: ").append(this.bottom).append(" ");
      sb.append("left: ").append(this.left).append(" ");
      return sb.toString();
   }
}
