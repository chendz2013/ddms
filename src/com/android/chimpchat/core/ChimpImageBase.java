package com.android.chimpchat.core;

import com.android.chimpchat.core.IChimpImage;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;

public abstract class ChimpImageBase implements IChimpImage {
   private static Logger LOG = Logger.getLogger(ChimpImageBase.class.getCanonicalName());
   private WeakReference<BufferedImage> cachedBufferedImage = null;

   public abstract BufferedImage createBufferedImage();

   public BufferedImage getBufferedImage() {
      BufferedImage img;
      if(this.cachedBufferedImage != null) {
         img = (BufferedImage)this.cachedBufferedImage.get();
         if(img != null) {
            return img;
         }
      }

      img = this.createBufferedImage();
      this.cachedBufferedImage = new WeakReference(img);
      return img;
   }

   public byte[] convertToBytes(String format) {
      BufferedImage argb = this.convertSnapshot();
      ByteArrayOutputStream os = new ByteArrayOutputStream();

      try {
         ImageIO.write(argb, format, os);
      } catch (IOException var5) {
         return new byte[0];
      }

      return os.toByteArray();
   }

   public boolean writeToFile(String path, String format) {
      if(format != null) {
         return this.writeToFileHelper(path, format);
      } else {
         int offset = path.lastIndexOf(46);
         if(offset < 0) {
            return this.writeToFileHelper(path, "png");
         } else {
            String ext = path.substring(offset + 1);
            Iterator writers = ImageIO.getImageWritersBySuffix(ext);
            if(!writers.hasNext()) {
               return this.writeToFileHelper(path, "png");
            } else {
               ImageWriter writer = (ImageWriter)writers.next();
               BufferedImage image = this.convertSnapshot();

               try {
                  File e = new File(path);
                  e.delete();
                  ImageOutputStream outputStream = ImageIO.createImageOutputStream(e);
                  writer.setOutput(outputStream);

                  try {
                     writer.write(image);
                  } finally {
                     writer.dispose();
                     outputStream.flush();
                  }

                  return true;
               } catch (IOException var14) {
                  return false;
               }
            }
         }
      }
   }

   public int getPixel(int x, int y) {
      BufferedImage image = this.getBufferedImage();
      return image.getRGB(x, y);
   }

   private BufferedImage convertSnapshot() {
      BufferedImage image = this.getBufferedImage();
      BufferedImage argb = new BufferedImage(image.getWidth(), image.getHeight(), 2);
      Graphics2D g = argb.createGraphics();
      g.drawImage(image, 0, 0, (ImageObserver)null);
      g.dispose();
      return argb;
   }

   private boolean writeToFileHelper(String path, String format) {
      BufferedImage argb = this.convertSnapshot();

      try {
         ImageIO.write(argb, format, new File(path));
         return true;
      } catch (IOException var5) {
         return false;
      }
   }

   public boolean sameAs(IChimpImage other, double percent) {
      BufferedImage otherImage = other.getBufferedImage();
      BufferedImage myImage = this.getBufferedImage();
      if(otherImage.getWidth() != myImage.getWidth()) {
         return false;
      } else if(otherImage.getHeight() != myImage.getHeight()) {
         return false;
      } else {
         int[] otherPixel = new int[1];
         int[] myPixel = new int[1];
         int width = myImage.getWidth();
         int height = myImage.getHeight();
         int numDiffPixels = 0;

         for(int numberPixels = 0; numberPixels < height; ++numberPixels) {
            for(int x = 0; x < width; ++x) {
               if(myImage.getRGB(x, numberPixels) != otherImage.getRGB(x, numberPixels)) {
                  ++numDiffPixels;
               }
            }
         }

         double var15 = (double)(height * width);
         double diffPercent = (double)numDiffPixels / var15;
         return percent <= 1.0D - diffPercent;
      }
   }

   public static IChimpImage loadImageFromFile(String path) {
      File f = new File(path);
      if(f.exists() && f.canRead()) {
         try {
            BufferedImage e = ImageIO.read(new File(path));
            if(e == null) {
               LOG.log(Level.WARNING, "Cannot decode file %s", path);
               return null;
            } else {
               return new ChimpImageBase.BufferedImageChimpImage(e);
            }
         } catch (IOException var3) {
            LOG.log(Level.WARNING, "Exception trying to decode image", var3);
            return null;
         }
      } else {
         LOG.log(Level.WARNING, "Cannot read file %s", path);
         return null;
      }
   }

   public IChimpImage getSubImage(int x, int y, int w, int h) {
      BufferedImage image = this.getBufferedImage();
      return new ChimpImageBase.BufferedImageChimpImage(image.getSubimage(x, y, w, h));
   }

   private static class BufferedImageChimpImage extends ChimpImageBase {
      private final BufferedImage image;

      public BufferedImageChimpImage(BufferedImage image) {
         this.image = image;
      }

      public BufferedImage createBufferedImage() {
         return this.image;
      }
   }
}
