package com.android.chimpchat.core;

import com.android.chimpchat.core.IChimpDevice;

public interface IChimpBackend {
   IChimpDevice waitForConnection();

   IChimpDevice waitForConnection(long var1, String var3);

   void shutdown();
}
