package com.android.chimpchat.core;

import java.awt.image.BufferedImage;

public interface IChimpImage {
   BufferedImage createBufferedImage();

   BufferedImage getBufferedImage();

   IChimpImage getSubImage(int var1, int var2, int var3, int var4);

   byte[] convertToBytes(String var1);

   boolean writeToFile(String var1, String var2);

   int getPixel(int var1, int var2);

   boolean sameAs(IChimpImage var1, double var2);
}
