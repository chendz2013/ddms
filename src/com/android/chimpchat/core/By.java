package com.android.chimpchat.core;

import com.android.chimpchat.core.IMultiSelector;
import com.android.chimpchat.core.ISelector;
import com.android.chimpchat.core.MultiSelectorText;
import com.android.chimpchat.core.SelectorAccessibilityIds;
import com.android.chimpchat.core.SelectorId;

public class By {
   public static ISelector id(String id) {
      return new SelectorId(id);
   }

   public static ISelector accessibilityIds(int windowId, long accessibilityId) {
      return new SelectorAccessibilityIds(windowId, accessibilityId);
   }

   public static IMultiSelector text(String searchText) {
      return new MultiSelectorText(searchText);
   }
}
