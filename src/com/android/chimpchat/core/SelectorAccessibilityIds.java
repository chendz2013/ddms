package com.android.chimpchat.core;

import com.android.chimpchat.ChimpManager;
import com.android.chimpchat.core.ChimpView;
import com.android.chimpchat.core.IChimpView;
import com.android.chimpchat.core.ISelector;
import com.google.common.collect.Lists;

public class SelectorAccessibilityIds implements ISelector {
   private int windowId;
   private long accessibilityId;

   public SelectorAccessibilityIds(int windowId, long accessibilityId) {
      this.windowId = windowId;
      this.accessibilityId = accessibilityId;
   }

   public IChimpView getView(ChimpManager manager) {
      ChimpView view = new ChimpView("accessibilityids", Lists.newArrayList(new String[]{Integer.toString(this.windowId), Long.toString(this.accessibilityId)}));
      view.setManager(manager);
      return view;
   }
}
