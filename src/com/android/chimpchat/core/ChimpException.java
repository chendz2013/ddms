package com.android.chimpchat.core;

public class ChimpException extends RuntimeException {
   public ChimpException(String s) {
      super(s);
   }
}
