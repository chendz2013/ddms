package com.android.chimpchat.core;

import java.util.HashMap;
import java.util.Map;

public enum TouchPressType {
   DOWN("down"),
   UP("up"),
   DOWN_AND_UP("downAndUp"),
   MOVE("move");

   private static final Map<String, TouchPressType> identifierToEnum;
   private String identifier;

   private TouchPressType(String identifier) {
      this.identifier = identifier;
   }

   public String getIdentifier() {
      return this.identifier;
   }

   public static TouchPressType fromIdentifier(String name) {
      return (TouchPressType)identifierToEnum.get(name);
   }

   static {
      identifierToEnum = new HashMap();
      TouchPressType[] arr$ = values();
      int len$ = arr$.length;

      for(int i$ = 0; i$ < len$; ++i$) {
         TouchPressType type = arr$[i$];
         identifierToEnum.put(type.identifier, type);
      }

   }
}
