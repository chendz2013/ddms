package com.android.chimpchat.core;

import com.android.chimpchat.ChimpManager;
import com.android.chimpchat.core.ChimpRect;
import com.android.chimpchat.core.IChimpView;
import com.google.common.collect.Lists;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ChimpView implements IChimpView {
   private static final Logger LOG = Logger.getLogger(ChimpView.class.getName());
   public static final String ACCESSIBILITY_IDS = "accessibilityids";
   public static final String VIEW_ID = "viewid";
   private String viewType;
   private List<String> ids;
   private ChimpManager manager;

   public ChimpView(String viewType, List<String> ids) {
      this.viewType = viewType;
      this.ids = ids;
   }

   public void setManager(ChimpManager manager) {
      this.manager = manager;
   }

   private String queryView(String query) {
      try {
         return this.manager.queryView(this.viewType, this.ids, query);
      } catch (IOException var3) {
         LOG.log(Level.SEVERE, "Error querying view: " + var3.getMessage());
         return "";
      }
   }

   public ChimpRect getLocation() {
      ArrayList result = Lists.newArrayList(this.queryView("getlocation").split(" "));
      if(result.size() == 4) {
         try {
            int e = Integer.parseInt((String)result.get(0));
            int top = Integer.parseInt((String)result.get(1));
            int width = Integer.parseInt((String)result.get(2));
            int height = Integer.parseInt((String)result.get(3));
            return new ChimpRect(e, top, e + width, top + height);
         } catch (NumberFormatException var6) {
            return new ChimpRect();
         }
      } else {
         return new ChimpRect();
      }
   }

   public String getText() {
      return this.queryView("gettext");
   }

   public String getViewClass() {
      return this.queryView("getclass");
   }

   public boolean getChecked() {
      return Boolean.valueOf(this.queryView("getchecked").trim()).booleanValue();
   }

   public boolean getEnabled() {
      return Boolean.valueOf(this.queryView("getenabled").trim()).booleanValue();
   }

   public boolean getSelected() {
      return Boolean.valueOf(this.queryView("getselected").trim()).booleanValue();
   }

   public void setSelected(boolean selected) {
      this.queryView("setselected " + selected);
   }

   public boolean getFocused() {
      return Boolean.valueOf(this.queryView("getselected").trim()).booleanValue();
   }

   public void setFocused(boolean focused) {
      this.queryView("setfocused " + focused);
   }

   public IChimpView getParent() {
      ArrayList results = Lists.newArrayList(this.queryView("getparent").split(" "));
      if(results.size() == 2) {
         ChimpView parent = new ChimpView("accessibilityids", results);
         parent.setManager(this.manager);
         return parent;
      } else {
         return null;
      }
   }

   public List<IChimpView> getChildren() {
      ArrayList results = Lists.newArrayList(this.queryView("getchildren").split(" "));
      if(results.size() % 2 != 0) {
         return new ArrayList();
      } else {
         ArrayList children = new ArrayList();

         for(int i = 0; i < results.size() / 2; ++i) {
            ArrayList ids = Lists.newArrayList(new String[]{(String)results.get(2 * i), (String)results.get(2 * i + 1)});
            ChimpView child = new ChimpView("accessibilityids", ids);
            child.setManager(this.manager);
            children.add(child);
         }

         return children;
      }
   }

   public IChimpView.AccessibilityIds getAccessibilityIds() {
      ArrayList results = Lists.newArrayList(this.queryView("getaccessibilityids").split(" "));
      if(results.size() == 2) {
         try {
            return new IChimpView.AccessibilityIds(Integer.parseInt((String)results.get(0)), Long.parseLong((String)results.get(1)));
         } catch (NumberFormatException var3) {
            LOG.log(Level.SEVERE, "Error retrieving accesibility ids: " + var3.getMessage());
         }
      }

      return new IChimpView.AccessibilityIds(0, 0L);
   }
}
