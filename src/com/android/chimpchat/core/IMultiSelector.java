package com.android.chimpchat.core;

import com.android.chimpchat.ChimpManager;
import com.android.chimpchat.core.IChimpView;
import java.util.Collection;

public interface IMultiSelector {
   Collection<IChimpView> getViews(ChimpManager var1);
}
