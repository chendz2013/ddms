package com.android.chimpchat.core;

import com.android.chimpchat.ChimpManager;
import com.android.chimpchat.core.ChimpView;
import com.android.chimpchat.core.IChimpView;
import com.android.chimpchat.core.ISelector;
import com.google.common.collect.Lists;

public class SelectorId implements ISelector {
   private String id;

   public SelectorId(String id) {
      this.id = id;
   }

   public IChimpView getView(ChimpManager manager) {
      ChimpView view = new ChimpView("viewid", Lists.newArrayList(new String[]{this.id}));
      view.setManager(manager);
      return view;
   }
}
