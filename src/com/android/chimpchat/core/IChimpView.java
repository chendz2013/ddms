package com.android.chimpchat.core;

import com.android.chimpchat.ChimpManager;
import com.android.chimpchat.core.ChimpRect;
import java.util.List;

public interface IChimpView {
   void setManager(ChimpManager var1);

   String getViewClass();

   String getText();

   ChimpRect getLocation();

   boolean getChecked();

   boolean getEnabled();

   boolean getSelected();

   void setSelected(boolean var1);

   boolean getFocused();

   void setFocused(boolean var1);

   IChimpView getParent();

   List<IChimpView> getChildren();

   IChimpView.AccessibilityIds getAccessibilityIds();

   public static class AccessibilityIds {
      private final int windowId;
      private final long nodeId;

      public AccessibilityIds() {
         this.windowId = 0;
         this.nodeId = 0L;
      }

      public AccessibilityIds(int windowId, long nodeId) {
         this.windowId = windowId;
         this.nodeId = nodeId;
      }

      public int getWindowId() {
         return this.windowId;
      }

      public long getNodeId() {
         return this.nodeId;
      }
   }
}
