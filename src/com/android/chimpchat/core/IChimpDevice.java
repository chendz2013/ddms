package com.android.chimpchat.core;

import com.android.chimpchat.ChimpManager;
import com.android.chimpchat.core.IChimpImage;
import com.android.chimpchat.core.IChimpView;
import com.android.chimpchat.core.IMultiSelector;
import com.android.chimpchat.core.ISelector;
import com.android.chimpchat.core.PhysicalButton;
import com.android.chimpchat.core.TouchPressType;
import com.android.chimpchat.hierarchyviewer.HierarchyViewer;
import java.util.Collection;
import java.util.Map;

public interface IChimpDevice {
   ChimpManager getManager();

   void dispose();

   HierarchyViewer getHierarchyViewer();

   IChimpImage takeSnapshot();

   void reboot(String var1);

   Collection<String> getPropertyList();

   String getProperty(String var1);

   String getSystemProperty(String var1);

   void touch(int var1, int var2, TouchPressType var3);

   void press(String var1, TouchPressType var2);

   void press(PhysicalButton var1, TouchPressType var2);

   void drag(int var1, int var2, int var3, int var4, int var5, long var6);

   void type(String var1);

   String shell(String var1);

   String shell(String var1, int var2);

   boolean installPackage(String var1);

   boolean removePackage(String var1);

   void startActivity(String var1, String var2, String var3, String var4, Collection<String> var5, Map<String, Object> var6, String var7, int var8);

   void broadcastIntent(String var1, String var2, String var3, String var4, Collection<String> var5, Map<String, Object> var6, String var7, int var8);

   Map<String, Object> instrument(String var1, Map<String, Object> var2);

   void wake();

   Collection<String> getViewIdList();

   IChimpView getView(ISelector var1);

   IChimpView getRootView();

   Collection<IChimpView> getViews(IMultiSelector var1);
}
