package com.android.chimpchat.adb;

import com.android.ddmlib.IShellOutputReceiver;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LoggingOutputReceiver implements IShellOutputReceiver {
   private final Logger log;
   private final Level level;

   public LoggingOutputReceiver(Logger log, Level level) {
      this.log = log;
      this.level = level;
   }

   public void addOutput(byte[] data, int offset, int length) {
      String message = new String(data, offset, length);
      String[] arr$ = message.split("\n");
      int len$ = arr$.length;

      for(int i$ = 0; i$ < len$; ++i$) {
         String line = arr$[i$];
         this.log.log(this.level, line);
      }

   }

   public void flush() {
   }

   public boolean isCancelled() {
      return false;
   }
}
