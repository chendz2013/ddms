package com.android.chimpchat.adb;

public class LinearInterpolator {
   private final int steps;

   public LinearInterpolator(int steps) {
      this.steps = steps;
   }

   private static float lerp(float start, float stop, float amount) {
      return start + (stop - start) * amount;
   }

   public void interpolate(LinearInterpolator.Point start, LinearInterpolator.Point end, LinearInterpolator.Callback callback) {
      Math.abs(end.getX() - start.getX());
      Math.abs(end.getY() - start.getY());
      float amount = (float)(1.0D / (double)this.steps);
      callback.start(start);

      for(int i = 1; i < this.steps; ++i) {
         float newX = lerp((float)start.getX(), (float)end.getX(), amount * (float)i);
         float newY = lerp((float)start.getY(), (float)end.getY(), amount * (float)i);
         callback.step(new LinearInterpolator.Point(Math.round(newX), Math.round(newY)));
      }

      callback.end(end);
   }

   public interface Callback {
      void start(LinearInterpolator.Point var1);

      void end(LinearInterpolator.Point var1);

      void step(LinearInterpolator.Point var1);
   }

   public static class Point {
      private final int x;
      private final int y;

      public Point(int x, int y) {
         this.x = x;
         this.y = y;
      }

      public String toString() {
         return "(" + this.x + "," + this.y + ")";
      }

      public boolean equals(Object obj) {
         if(!(obj instanceof LinearInterpolator.Point)) {
            return false;
         } else {
            LinearInterpolator.Point that = (LinearInterpolator.Point)obj;
            return this.x == that.x && this.y == that.y;
         }
      }

      public int hashCode() {
         return 1125274389 + this.x + this.y;
      }

      public int getX() {
         return this.x;
      }

      public int getY() {
         return this.y;
      }
   }
}
