package com.android.chimpchat.adb;

import com.android.chimpchat.adb.image.ImageUtils;
import com.android.chimpchat.core.ChimpImageBase;
import com.android.ddmlib.RawImage;
import java.awt.image.BufferedImage;

public class AdbChimpImage extends ChimpImageBase {
   private final RawImage image;

   AdbChimpImage(RawImage image) {
      this.image = image;
   }

   public BufferedImage createBufferedImage() {
      return ImageUtils.convertImage(this.image);
   }

   public RawImage getRawImage() {
      return this.image;
   }
}
