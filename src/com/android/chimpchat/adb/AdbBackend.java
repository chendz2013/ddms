package com.android.chimpchat.adb;

import com.android.SdkConstants;
import com.android.chimpchat.adb.AdbChimpDevice;
import com.android.chimpchat.core.IChimpBackend;
import com.android.chimpchat.core.IChimpDevice;
import com.android.ddmlib.AndroidDebugBridge;
import com.android.ddmlib.IDevice;
import com.android.ddmlib.IDevice.DeviceState;
import com.google.common.collect.Lists;
import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
//这个就是adb后台
public class AdbBackend implements IChimpBackend {
   private static Logger LOG = Logger.getLogger(AdbBackend.class.getCanonicalName());
   private static final int CONNECTION_ITERATION_TIMEOUT_MS = 200;
   private final List<IChimpDevice> devices;
   private final AndroidDebugBridge bridge;
   private final boolean initAdb;

   public AdbBackend() {
      this((String)null, false);
   }

   public AdbBackend(String adbLocation, boolean noInitAdb) {
      this.devices = Lists.newArrayList();
      this.initAdb = !noInitAdb;
      if(adbLocation == null) {
         adbLocation = this.findAdb();
      }

      if(this.initAdb) {
         AndroidDebugBridge.init(false);
      }

      this.bridge = AndroidDebugBridge.createBridge(adbLocation, true);
   }
   //查找adb
   private String findAdb() {
      String mrParentLocation = System.getProperty("com.android.monkeyrunner.bindir");
      if(mrParentLocation != null && mrParentLocation.length() != 0) {
         File platformTools = new File((new File(mrParentLocation)).getParent(), "platform-tools");
         return platformTools.isDirectory()?platformTools.getAbsolutePath() + File.separator + SdkConstants.FN_ADB:mrParentLocation + File.separator + SdkConstants.FN_ADB;
      } else {
         return SdkConstants.FN_ADB;
      }
   }

   private IDevice findAttachedDevice(String deviceIdRegex) {
      Pattern pattern = Pattern.compile(deviceIdRegex);
      IDevice[] arr$ = this.bridge.getDevices();
      int len$ = arr$.length;

      for(int i$ = 0; i$ < len$; ++i$) {
         IDevice device = arr$[i$];
         String serialNumber = device.getSerialNumber();
         if(pattern.matcher(serialNumber).matches()) {
            return device;
         }
      }

      return null;
   }

   public IChimpDevice waitForConnection() {
      return this.waitForConnection(2147483647L, ".*");
   }

   public IChimpDevice waitForConnection(long timeoutMs, String deviceIdRegex) {
      do {
         IDevice device = this.findAttachedDevice(deviceIdRegex);
         if(device != null && device.getState() == DeviceState.ONLINE) {
            AdbChimpDevice e = new AdbChimpDevice(device);
            this.devices.add(e);
            return e;
         }

         try {
            Thread.sleep(200L);
         } catch (InterruptedException var6) {
            LOG.log(Level.SEVERE, "Error sleeping", var6);
         }

         timeoutMs -= 200L;
      } while(timeoutMs > 0L);

      return null;
   }

   public void shutdown() {
      Iterator i$ = this.devices.iterator();

      while(i$.hasNext()) {
         IChimpDevice device = (IChimpDevice)i$.next();
         device.dispose();
      }

      if(this.initAdb) {
         AndroidDebugBridge.terminate();
      }

   }
}
