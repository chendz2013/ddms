package com.android.chimpchat.adb;

import com.android.chimpchat.ChimpManager;
import com.android.chimpchat.adb.AdbChimpImage;
import com.android.chimpchat.adb.CommandOutputCapture;
import com.android.chimpchat.adb.LinearInterpolator;
import com.android.chimpchat.adb.LoggingOutputReceiver;
import com.android.chimpchat.core.IChimpDevice;
import com.android.chimpchat.core.IChimpImage;
import com.android.chimpchat.core.IChimpView;
import com.android.chimpchat.core.IMultiSelector;
import com.android.chimpchat.core.ISelector;
import com.android.chimpchat.core.PhysicalButton;
import com.android.chimpchat.core.TouchPressType;
import com.android.chimpchat.hierarchyviewer.HierarchyViewer;
import com.android.ddmlib.AdbCommandRejectedException;
import com.android.ddmlib.DdmPreferences;
import com.android.ddmlib.IDevice;
import com.android.ddmlib.InstallException;
import com.android.ddmlib.ShellCommandUnresponsiveException;
import com.android.ddmlib.TimeoutException;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AdbChimpDevice implements IChimpDevice {
   private static final Logger LOG = Logger.getLogger(AdbChimpDevice.class.getName());
   private static final String[] ZERO_LENGTH_STRING_ARRAY = new String[0];
   private static final long MANAGER_CREATE_TIMEOUT_MS = 30000L;
   private static final long MANAGER_CREATE_WAIT_TIME_MS = 1000L;
   private final ExecutorService executor = Executors.newSingleThreadExecutor();
   private final IDevice device;
   private ChimpManager manager;

   public AdbChimpDevice(IDevice device) {
      this.device = device;
      this.manager = this.createManager("127.0.0.1", 12345);
      Preconditions.checkNotNull(this.manager);
   }

   public ChimpManager getManager() {
      return this.manager;
   }

   public void dispose() {
      try {
         this.manager.quit();
      } catch (IOException var2) {
         LOG.log(Level.SEVERE, "Error getting the manager to quit", var2);
      }

      this.manager.close();
      this.executor.shutdown();
      this.manager = null;
   }

   public HierarchyViewer getHierarchyViewer() {
      return new HierarchyViewer(this.device);
   }

   private void executeAsyncCommand(final String command, final LoggingOutputReceiver logger) {
      this.executor.submit(new Runnable() {
         public void run() {
            try {
               AdbChimpDevice.this.device.executeShellCommand(command, logger);
            } catch (TimeoutException var2) {
               AdbChimpDevice.LOG.log(Level.SEVERE, "Error starting command: " + command, var2);
               throw new RuntimeException(var2);
            } catch (AdbCommandRejectedException var3) {
               AdbChimpDevice.LOG.log(Level.SEVERE, "Error starting command: " + command, var3);
               throw new RuntimeException(var3);
            } catch (ShellCommandUnresponsiveException var4) {
               AdbChimpDevice.LOG.log(Level.INFO, "Error starting command: " + command, var4);
               throw new RuntimeException(var4);
            } catch (IOException var5) {
               AdbChimpDevice.LOG.log(Level.SEVERE, "Error starting command: " + command, var5);
               throw new RuntimeException(var5);
            }
         }
      });
   }

   private ChimpManager createManager(String address, int port) {
      try {
         this.device.createForward(port, port);
      } catch (TimeoutException var18) {
         LOG.log(Level.SEVERE, "Timeout creating adb port forwarding", var18);
         return null;
      } catch (AdbCommandRejectedException var19) {
         LOG.log(Level.SEVERE, "Adb rejected adb port forwarding command: " + var19.getMessage(), var19);
         return null;
      } catch (IOException var20) {
         LOG.log(Level.SEVERE, "Unable to create adb port forwarding: " + var20.getMessage(), var20);
         return null;
      }

      String command = "monkey --port " + port;
      this.executeAsyncCommand(command, new LoggingOutputReceiver(LOG, Level.FINE));

      try {
         Thread.sleep(1000L);
      } catch (InterruptedException var17) {
         LOG.log(Level.SEVERE, "Unable to sleep", var17);
      }

      InetAddress addr;
      try {
         addr = InetAddress.getByName(address);
      } catch (UnknownHostException var16) {
         LOG.log(Level.SEVERE, "Unable to convert address into InetAddress: " + address, var16);
         return null;
      }

      boolean success = false;
      ChimpManager mm = null;
      long start = System.currentTimeMillis();

      while(!success) {
         long now = System.currentTimeMillis();
         long diff = now - start;
         if(diff > 30000L) {
            LOG.severe("Timeout while trying to create chimp mananger");
            return null;
         }

         try {
            Thread.sleep(1000L);
         } catch (InterruptedException var15) {
            LOG.log(Level.SEVERE, "Unable to sleep", var15);
         }

         Socket monkeySocket;
         try {
            monkeySocket = new Socket(addr, port);
         } catch (IOException var23) {
            LOG.log(Level.FINE, "Unable to connect socket", var23);
            success = false;
            continue;
         }

         try {
            mm = new ChimpManager(monkeySocket);
         } catch (IOException var22) {
            LOG.log(Level.SEVERE, "Unable to open writer and reader to socket");
            continue;
         }

         try {
            mm.wake();
         } catch (IOException var21) {
            LOG.log(Level.FINE, "Unable to wake up device", var21);
            success = false;
            continue;
         }

         success = true;
      }

      return mm;
   }

   public IChimpImage takeSnapshot() {
      try {
         return new AdbChimpImage(this.device.getScreenshot());
      } catch (TimeoutException var2) {
         LOG.log(Level.SEVERE, "Unable to take snapshot", var2);
         return null;
      } catch (AdbCommandRejectedException var3) {
         LOG.log(Level.SEVERE, "Unable to take snapshot", var3);
         return null;
      } catch (IOException var4) {
         LOG.log(Level.SEVERE, "Unable to take snapshot", var4);
         return null;
      }
   }

   public String getSystemProperty(String key) {
      return this.device.getProperty(key);
   }

   public String getProperty(String key) {
      try {
         return this.manager.getVariable(key);
      } catch (IOException var3) {
         LOG.log(Level.SEVERE, "Unable to get variable: " + key, var3);
         return null;
      }
   }

   public Collection<String> getPropertyList() {
      try {
         return this.manager.listVariable();
      } catch (IOException var2) {
         LOG.log(Level.SEVERE, "Unable to get variable list", var2);
         return null;
      }
   }

   public void wake() {
      try {
         this.manager.wake();
      } catch (IOException var2) {
         LOG.log(Level.SEVERE, "Unable to wake device (too sleepy?)", var2);
      }

   }

   private String shell(String... args) {
      StringBuilder cmd = new StringBuilder();
      String[] arr$ = args;
      int len$ = args.length;

      for(int i$ = 0; i$ < len$; ++i$) {
         String arg = arr$[i$];
         cmd.append(arg).append(" ");
      }

      return this.shell(cmd.toString());
   }

   public String shell(String cmd) {
      return this.shell(cmd, DdmPreferences.getTimeOut());
   }

   public String shell(String cmd, int timeout) {
      CommandOutputCapture capture = new CommandOutputCapture();

      try {
         this.device.executeShellCommand(cmd, capture, timeout);
      } catch (TimeoutException var5) {
         LOG.log(Level.SEVERE, "Error executing command: " + cmd, var5);
         return null;
      } catch (ShellCommandUnresponsiveException var6) {
         LOG.log(Level.SEVERE, "Error executing command: " + cmd, var6);
         return null;
      } catch (AdbCommandRejectedException var7) {
         LOG.log(Level.SEVERE, "Error executing command: " + cmd, var7);
         return null;
      } catch (IOException var8) {
         LOG.log(Level.SEVERE, "Error executing command: " + cmd, var8);
         return null;
      }

      return capture.toString();
   }

   public boolean installPackage(String path) {
      try {
         String e = this.device.installPackage(path, true, new String[0]);
         if(e != null) {
            LOG.log(Level.SEVERE, "Got error installing package: " + e);
            return false;
         } else {
            return true;
         }
      } catch (InstallException var3) {
         LOG.log(Level.SEVERE, "Error installing package: " + path, var3);
         return false;
      }
   }

   public boolean removePackage(String packageName) {
      try {
         String e = this.device.uninstallPackage(packageName);
         if(e != null) {
            LOG.log(Level.SEVERE, "Got error uninstalling package " + packageName + ": " + e);
            return false;
         } else {
            return true;
         }
      } catch (InstallException var3) {
         LOG.log(Level.SEVERE, "Error installing package: " + packageName, var3);
         return false;
      }
   }

   public void press(String keyName, TouchPressType type) {
      try {
         switch(AdbChimpDevice.SyntheticClass_1.$SwitchMap$com$android$chimpchat$core$TouchPressType[type.ordinal()]) {
         case 1:
            this.manager.press(keyName);
            break;
         case 2:
            this.manager.keyDown(keyName);
            break;
         case 3:
            this.manager.keyUp(keyName);
         }
      } catch (IOException var4) {
         LOG.log(Level.SEVERE, "Error sending press event: " + keyName + " " + type, var4);
      }

   }

   public void press(PhysicalButton key, TouchPressType type) {
      this.press(key.getKeyName(), type);
   }

   public void type(String string) {
      try {
         this.manager.type(string);
      } catch (IOException var3) {
         LOG.log(Level.SEVERE, "Error Typing: " + string, var3);
      }

   }

   public void touch(int x, int y, TouchPressType type) {
      try {
         switch(AdbChimpDevice.SyntheticClass_1.$SwitchMap$com$android$chimpchat$core$TouchPressType[type.ordinal()]) {
         case 1:
            this.manager.tap(x, y);
            break;
         case 2:
            this.manager.touchDown(x, y);
            break;
         case 3:
            this.manager.touchUp(x, y);
            break;
         case 4:
            this.manager.touchMove(x, y);
         }
      } catch (IOException var5) {
         LOG.log(Level.SEVERE, "Error sending touch event: " + x + " " + y + " " + type, var5);
      }

   }

   public void reboot(String into) {
      try {
         this.device.reboot(into);
      } catch (TimeoutException var3) {
         LOG.log(Level.SEVERE, "Unable to reboot device", var3);
      } catch (AdbCommandRejectedException var4) {
         LOG.log(Level.SEVERE, "Unable to reboot device", var4);
      } catch (IOException var5) {
         LOG.log(Level.SEVERE, "Unable to reboot device", var5);
      }

   }

   public void startActivity(String uri, String action, String data, String mimetype, Collection<String> categories, Map<String, Object> extras, String component, int flags) {
      List intentArgs = this.buildIntentArgString(uri, action, data, mimetype, categories, extras, component, flags);
      this.shell((String[])Lists.asList("am", "start", intentArgs.toArray(ZERO_LENGTH_STRING_ARRAY)).toArray(ZERO_LENGTH_STRING_ARRAY));
   }

   public void broadcastIntent(String uri, String action, String data, String mimetype, Collection<String> categories, Map<String, Object> extras, String component, int flags) {
      List intentArgs = this.buildIntentArgString(uri, action, data, mimetype, categories, extras, component, flags);
      this.shell((String[])Lists.asList("am", "broadcast", intentArgs.toArray(ZERO_LENGTH_STRING_ARRAY)).toArray(ZERO_LENGTH_STRING_ARRAY));
   }

   private static boolean isNullOrEmpty(String string) {
      return string == null || string.length() == 0;
   }

   private List<String> buildIntentArgString(String uri, String action, String data, String mimetype, Collection<String> categories, Map<String, Object> extras, String component, int flags) {
      ArrayList parts = Lists.newArrayList();
      if(!isNullOrEmpty(action)) {
         parts.add("-a");
         parts.add(action);
      }

      if(!isNullOrEmpty(data)) {
         parts.add("-d");
         parts.add(data);
      }

      if(!isNullOrEmpty(mimetype)) {
         parts.add("-t");
         parts.add(mimetype);
      }

      Iterator i$ = categories.iterator();

      while(i$.hasNext()) {
         String entry = (String)i$.next();
         parts.add("-c");
         parts.add(entry);
      }

      i$ = extras.entrySet().iterator();

      while(i$.hasNext()) {
         Entry entry1 = (Entry)i$.next();
         Object value = entry1.getValue();
         String valueString;
         String arg;
         if(value instanceof Integer) {
            valueString = Integer.toString(((Integer)value).intValue());
            arg = "--ei";
         } else if(value instanceof Boolean) {
            valueString = Boolean.toString(((Boolean)value).booleanValue());
            arg = "--ez";
         } else {
            valueString = value.toString();
            arg = "--es";
         }

         parts.add(arg);
         parts.add(entry1.getKey());
         parts.add(valueString);
      }

      if(!isNullOrEmpty(component)) {
         parts.add("-n");
         parts.add(component);
      }

      if(flags != 0) {
         parts.add("-f");
         parts.add(Integer.toString(flags));
      }

      if(!isNullOrEmpty(uri)) {
         parts.add(uri);
      }

      return parts;
   }

   public Map<String, Object> instrument(String packageName, Map<String, Object> args) {
      ArrayList shellCmd = Lists.newArrayList(new String[]{"am", "instrument", "-w", "-r"});
      Iterator result = args.entrySet().iterator();

      while(result.hasNext()) {
         Entry entry = (Entry)result.next();
         String key = (String)entry.getKey();
         Object value = entry.getValue();
         if(key != null && value != null) {
            shellCmd.add("-e");
            shellCmd.add(key);
            shellCmd.add(value.toString());
         }
      }

      shellCmd.add(packageName);
      String result1 = this.shell((String[])shellCmd.toArray(ZERO_LENGTH_STRING_ARRAY));
      return convertInstrumentResult(result1);
   }

   @VisibleForTesting
   static Map<String, Object> convertInstrumentResult(String result) {
      HashMap map = Maps.newHashMap();
      Pattern pattern = Pattern.compile("^INSTRUMENTATION_(\\w+): ", 8);
      Matcher matcher = pattern.matcher(result);
      int previousEnd = 0;

      String previousWhich;
      String resultLine;
      int splitIndex;
      String key;
      String value;
      for(previousWhich = null; matcher.find(); previousWhich = matcher.group(1)) {
         if("RESULT".equals(previousWhich)) {
            resultLine = result.substring(previousEnd, matcher.start()).trim();
            splitIndex = resultLine.indexOf("=");
            key = resultLine.substring(0, splitIndex);
            value = resultLine.substring(splitIndex + 1);
            map.put(key, value);
         }

         previousEnd = matcher.end();
      }

      if("RESULT".equals(previousWhich)) {
         resultLine = result.substring(previousEnd, matcher.start()).trim();
         splitIndex = resultLine.indexOf("=");
         key = resultLine.substring(0, splitIndex);
         value = resultLine.substring(splitIndex + 1);
         map.put(key, value);
      }

      return map;
   }

   public void drag(int startx, int starty, int endx, int endy, int steps, long ms) {
      final long iterationTime = ms / (long)steps;
      LinearInterpolator lerp = new LinearInterpolator(steps);
      LinearInterpolator.Point start = new LinearInterpolator.Point(startx, starty);
      LinearInterpolator.Point end = new LinearInterpolator.Point(endx, endy);
      lerp.interpolate(start, end, new LinearInterpolator.Callback() {
         public void step(LinearInterpolator.Point point) {
            try {
               AdbChimpDevice.this.manager.touchMove(point.getX(), point.getY());
            } catch (IOException var4) {
               AdbChimpDevice.LOG.log(Level.SEVERE, "Error sending drag start event", var4);
            }

            try {
               Thread.sleep(iterationTime);
            } catch (InterruptedException var3) {
               AdbChimpDevice.LOG.log(Level.SEVERE, "Error sleeping", var3);
            }

         }

         public void start(LinearInterpolator.Point point) {
            try {
               AdbChimpDevice.this.manager.touchDown(point.getX(), point.getY());
               AdbChimpDevice.this.manager.touchMove(point.getX(), point.getY());
            } catch (IOException var4) {
               AdbChimpDevice.LOG.log(Level.SEVERE, "Error sending drag start event", var4);
            }

            try {
               Thread.sleep(iterationTime);
            } catch (InterruptedException var3) {
               AdbChimpDevice.LOG.log(Level.SEVERE, "Error sleeping", var3);
            }

         }

         public void end(LinearInterpolator.Point point) {
            try {
               AdbChimpDevice.this.manager.touchMove(point.getX(), point.getY());
               AdbChimpDevice.this.manager.touchUp(point.getX(), point.getY());
            } catch (IOException var3) {
               AdbChimpDevice.LOG.log(Level.SEVERE, "Error sending drag end event", var3);
            }

         }
      });
   }

   public Collection<String> getViewIdList() {
      try {
         return this.manager.listViewIds();
      } catch (IOException var2) {
         LOG.log(Level.SEVERE, "Error retrieving view IDs", var2);
         return new ArrayList();
      }
   }

   public IChimpView getView(ISelector selector) {
      return selector.getView(this.manager);
   }

   public Collection<IChimpView> getViews(IMultiSelector selector) {
      return selector.getViews(this.manager);
   }

   public IChimpView getRootView() {
      try {
         return this.manager.getRootView();
      } catch (IOException var2) {
         LOG.log(Level.SEVERE, "Error retrieving root view");
         return null;
      }
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] $SwitchMap$com$android$chimpchat$core$TouchPressType = new int[TouchPressType.values().length];

      static {
         try {
            $SwitchMap$com$android$chimpchat$core$TouchPressType[TouchPressType.DOWN_AND_UP.ordinal()] = 1;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            $SwitchMap$com$android$chimpchat$core$TouchPressType[TouchPressType.DOWN.ordinal()] = 2;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            $SwitchMap$com$android$chimpchat$core$TouchPressType[TouchPressType.UP.ordinal()] = 3;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            $SwitchMap$com$android$chimpchat$core$TouchPressType[TouchPressType.MOVE.ordinal()] = 4;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }
}
