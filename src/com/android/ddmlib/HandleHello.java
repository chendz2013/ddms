package com.android.ddmlib;

import com.android.ddmlib.ByteBufferUtil;
import com.android.ddmlib.ChunkHandler;
import com.android.ddmlib.Client;
import com.android.ddmlib.ClientData;
import com.android.ddmlib.HandleProfiling;
import com.android.ddmlib.JdwpPacket;
import com.android.ddmlib.Log;
import com.android.ddmlib.MonitorThread;
import java.io.IOException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;

/**
 * 基本握手协议
 */
final class HandleHello extends ChunkHandler {
   public static final int CHUNK_HELO = ChunkHandler.type("HELO");
   public static final int CHUNK_FEAT = ChunkHandler.type("FEAT");
   private static final HandleHello mInst = new HandleHello();

   public static void register(MonitorThread mt) {
      mt.registerChunkHandler(CHUNK_HELO, mInst);
   }

   public void clientReady(Client client) throws IOException {
      Log.d("ddm-hello", "Now ready: " + client);
   }

   public void clientDisconnected(Client client) {
      Log.d("ddm-hello", "Now disconnected: " + client);
   }

   public static void sendHelloCommands(Client client, int serverProtocolVersion) throws IOException {
      sendHELO(client, serverProtocolVersion);
      sendFEAT(client);
      HandleProfiling.sendMPRQ(client);
   }

   public void handleChunk(Client client, int type, ByteBuffer data, boolean isReply, int msgId) {
      Log.d("ddm-hello", "handling " + ChunkHandler.name(type));
      if(type == CHUNK_HELO) {
         assert isReply;

         handleHELO(client, data);
      } else if(type == CHUNK_FEAT) {
         handleFEAT(client, data);
      } else {
         this.handleUnknownChunk(client, type, data, isReply, msgId);
      }

   }

   private static void handleHELO(Client client, ByteBuffer data) {
      int version = data.getInt();
      int pid = data.getInt();
      int vmIdentLen = data.getInt();
      int appNameLen = data.getInt();
      //读取VM版本
      String vmIdent = ByteBufferUtil.getString(data, vmIdentLen);
      //读取App名称
      String appName = ByteBufferUtil.getString(data, appNameLen);
      //读取UserId
      int userId = -1;
      boolean validUserId = false;
      if(data.hasRemaining()) {
         try {
            userId = data.getInt();
            validUserId = true;
         } catch (BufferUnderflowException var17) {
            int abi = 20 + appNameLen * 2 + vmIdentLen * 2;
            Log.e("ddm-hello", "Insufficient data in HELO chunk to retrieve user id.");
            Log.e("ddm-hello", "Actual chunk length: " + data.capacity());
            Log.e("ddm-hello", "Expected chunk length: " + abi);
         }
      }
      //获取ABI的信息
      boolean validAbi = false;
      String abi1 = null;
      if(data.hasRemaining()) {
         try {
            int hasJvmFlags = data.getInt();
            abi1 = ByteBufferUtil.getString(data, hasJvmFlags);
            validAbi = true;
         } catch (BufferUnderflowException var16) {
            Log.e("ddm-hello", "Insufficient data in HELO chunk to retrieve ABI.");
         }
      }
      //获取JVM设置信息
      boolean hasJvmFlags1 = false;
      String jvmFlags = null;
      if(data.hasRemaining()) {
         try {
            int cd = data.getInt();
            jvmFlags = ByteBufferUtil.getString(data, cd);
            hasJvmFlags1 = true;
         } catch (BufferUnderflowException var15) {
            Log.e("ddm-hello", "Insufficient data in HELO chunk to retrieve JVM flags");
         }
      }

      Log.d("ddm-hello", "HELO: v=" + version + ", pid=" + pid + ", vm=\'" + vmIdent + "\', app=\'" + appName + "\'");
      ClientData cd1 = client.getClientData();
      if(cd1.getPid() == pid) {
         cd1.setVmIdentifier(vmIdent);
         cd1.setClientDescription(appName);
         cd1.isDdmAware(true);
         if(validUserId) {
            cd1.setUserId(userId);
         }

         if(validAbi) {
            cd1.setAbi(abi1);
         }

         if(hasJvmFlags1) {
            cd1.setJvmFlags(jvmFlags);
         }
      } else {
         Log.e("ddm-hello", "Received pid (" + pid + ") does not match client pid (" + cd1.getPid() + ")");
      }

      client = checkDebuggerPortForAppName(client, appName);
      if(client != null) {
         client.update(1);
      }

   }

   public static void sendHELO(Client client, int serverProtocolVersion) throws IOException {
      ByteBuffer rawBuf = allocBuffer(4);
      JdwpPacket packet = new JdwpPacket(rawBuf);
      ByteBuffer buf = getChunkDataBuf(rawBuf);
      buf.putInt(serverProtocolVersion);
      finishChunkPacket(packet, CHUNK_HELO, buf.position());
      Log.d("ddm-hello", "Sending " + name(CHUNK_HELO) + " ID=0x" + Integer.toHexString(packet.getId()));
      client.sendAndConsume(packet, mInst);
   }

   private static void handleFEAT(Client client, ByteBuffer data) {
      int featureCount = data.getInt();

      for(int i = 0; i < featureCount; ++i) {
         int len = data.getInt();
         String feature = ByteBufferUtil.getString(data, len);
         client.getClientData().addFeature(feature);
         Log.d("ddm-hello", "Feature: " + feature);
      }

   }

   public static void sendFEAT(Client client) throws IOException {
      ByteBuffer rawBuf = allocBuffer(0);
      JdwpPacket packet = new JdwpPacket(rawBuf);
      ByteBuffer buf = getChunkDataBuf(rawBuf);
      finishChunkPacket(packet, CHUNK_FEAT, buf.position());
      Log.d("ddm-heap", "Sending " + name(CHUNK_FEAT));
      client.sendAndConsume(packet, mInst);
   }
}
