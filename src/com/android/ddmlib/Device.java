package com.android.ddmlib;

import com.android.ddmlib.AdbCommandRejectedException;
import com.android.ddmlib.AdbHelper;
import com.android.ddmlib.AndroidDebugBridge;
import com.android.ddmlib.BatteryFetcher;
import com.android.ddmlib.Client;
import com.android.ddmlib.ClientData;
import com.android.ddmlib.CollectingOutputReceiver;
import com.android.ddmlib.DdmPreferences;
import com.android.ddmlib.DeviceMonitor;
import com.android.ddmlib.FileListingService;
import com.android.ddmlib.IDevice;
import com.android.ddmlib.IShellOutputReceiver;
import com.android.ddmlib.InstallException;
import com.android.ddmlib.Log;
import com.android.ddmlib.MultiLineReceiver;
import com.android.ddmlib.NullOutputReceiver;
import com.android.ddmlib.PropertyFetcher;
import com.android.ddmlib.RawImage;
import com.android.ddmlib.ScreenRecorderOptions;
import com.android.ddmlib.ShellCommandUnresponsiveException;
import com.android.ddmlib.SyncException;
import com.android.ddmlib.SyncService;
import com.android.ddmlib.TimeoutException;
import com.android.ddmlib.log.LogReceiver;
import com.google.common.base.CharMatcher;
import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.common.util.concurrent.Atomics;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

final class Device implements IDevice {
   static final String RE_EMULATOR_SN = "emulator-(\\d+)";
   private final String mSerialNumber;
   private String mAvdName = null;
   private IDevice.DeviceState mState = null;
   private final PropertyFetcher mPropFetcher = new PropertyFetcher(this);
   private final Map<String, String> mMountPoints = new HashMap();
   private final BatteryFetcher mBatteryFetcher = new BatteryFetcher(this);
   private final List<Client> mClients = new ArrayList();
   private final Map<Integer, String> mClientInfo = new ConcurrentHashMap();
   private DeviceMonitor mMonitor;
   private static final String LOG_TAG = "Device";
   private static final char SEPARATOR = '-';
   private static final String UNKNOWN_PACKAGE = "";
   private static final long GET_PROP_TIMEOUT_MS = 100L;
   private static final long INSTALL_TIMEOUT_MINUTES;
   private SocketChannel mSocketChannel;
   private Integer mLastBatteryLevel = null;
   private long mLastBatteryCheckTime = 0L;
   private static final String SCREEN_RECORDER_DEVICE_PATH = "/system/bin/screenrecord";
   private static final long LS_TIMEOUT_SEC = 2L;
   private Boolean mHasScreenRecorder;
   private Set<String> mHardwareCharacteristics;
   private int mApiLevel;
   private String mName;
   private static final CharMatcher UNSAFE_PM_INSTALL_SESSION_SPLIT_NAME_CHARS;

   public String getSerialNumber() {
      return this.mSerialNumber;
   }

   public String getAvdName() {
      return this.mAvdName;
   }

   void setAvdName(String avdName) {
      if(!this.isEmulator()) {
         throw new IllegalArgumentException("Cannot set the AVD name of the device is not an emulator");
      } else {
         this.mAvdName = avdName;
      }
   }

   public String getName() {
      if(this.mName != null) {
         return this.mName;
      } else if(this.isOnline()) {
         this.mName = this.constructName();
         return this.mName;
      } else {
         return this.constructName();
      }
   }

   private String constructName() {
      String manufacturer;
      if(this.isEmulator()) {
         manufacturer = this.getAvdName();
         return manufacturer != null?String.format("%s [%s]", new Object[]{manufacturer, this.getSerialNumber()}):this.getSerialNumber();
      } else {
         manufacturer = null;
         String model = null;

         try {
            manufacturer = this.cleanupStringForDisplay(this.getProperty("ro.product.manufacturer"));
            model = this.cleanupStringForDisplay(this.getProperty("ro.product.model"));
         } catch (Exception var4) {
            ;
         }

         StringBuilder sb = new StringBuilder(20);
         if(manufacturer != null) {
            sb.append(manufacturer);
            sb.append('-');
         }

         if(model != null) {
            sb.append(model);
            sb.append('-');
         }

         sb.append(this.getSerialNumber());
         return sb.toString();
      }
   }

   private String cleanupStringForDisplay(String s) {
      if(s == null) {
         return null;
      } else {
         StringBuilder sb = new StringBuilder(s.length());

         for(int i = 0; i < s.length(); ++i) {
            char c = s.charAt(i);
            if(Character.isLetterOrDigit(c)) {
               sb.append(Character.toLowerCase(c));
            } else {
               sb.append('_');
            }
         }

         return sb.toString();
      }
   }

   public IDevice.DeviceState getState() {
      return this.mState;
   }

   void setState(IDevice.DeviceState state) {
      this.mState = state;
   }

   public Map<String, String> getProperties() {
      return Collections.unmodifiableMap(this.mPropFetcher.getProperties());
   }

   public int getPropertyCount() {
      return this.mPropFetcher.getProperties().size();
   }

   public String getProperty(String name) {
      Future future = this.mPropFetcher.getProperty(name);

      try {
         return (String)future.get(100L, TimeUnit.MILLISECONDS);
      } catch (InterruptedException var4) {
         ;
      } catch (ExecutionException var5) {
         ;
      } catch (java.util.concurrent.TimeoutException var6) {
         ;
      }

      return null;
   }

   public boolean arePropertiesSet() {
      return this.mPropFetcher.arePropertiesSet();
   }

   public String getPropertyCacheOrSync(String name) throws TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException, IOException {
      Future future = this.mPropFetcher.getProperty(name);

      try {
         return (String)future.get();
      } catch (InterruptedException var4) {
         ;
      } catch (ExecutionException var5) {
         ;
      }

      return null;
   }

   public String getPropertySync(String name) throws TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException, IOException {
      Future future = this.mPropFetcher.getProperty(name);

      try {
         return (String)future.get();
      } catch (InterruptedException var4) {
         ;
      } catch (ExecutionException var5) {
         ;
      }

      return null;
   }

   public Future<String> getSystemProperty(String name) {
      return this.mPropFetcher.getProperty(name);
   }

   public boolean supportsFeature(IDevice.Feature feature) {
      switch(Device.SyntheticClass_1.$SwitchMap$com$android$ddmlib$IDevice$Feature[feature.ordinal()]) {
      case 1:
         if(this.getApiLevel() < 19) {
            return false;
         }

         if(this.mHasScreenRecorder == null) {
            this.mHasScreenRecorder = Boolean.valueOf(this.hasBinary("/system/bin/screenrecord"));
         }

         return this.mHasScreenRecorder.booleanValue();
      case 2:
         return this.getApiLevel() >= 19;
      default:
         return false;
      }
   }

   public boolean supportsFeature(IDevice.HardwareFeature feature) {
      if(this.mHardwareCharacteristics == null) {
         try {
            String e = this.getProperty("ro.build.characteristics");
            if(e == null) {
               return false;
            }

            this.mHardwareCharacteristics = Sets.newHashSet(Splitter.on(',').split(e));
         } catch (Exception var3) {
            this.mHardwareCharacteristics = Collections.emptySet();
         }
      }

      return this.mHardwareCharacteristics.contains(feature.getCharacteristic());
   }

   public int getApiLevel() {
      if(this.mApiLevel > 0) {
         return this.mApiLevel;
      } else {
         String buildApi = this.getProperty("ro.build.version.sdk");
         if(buildApi == null) {
            throw new IllegalStateException("Unexpected error: Device does not have a build API level.");
         } else {
            try {
               this.mApiLevel = Integer.parseInt(buildApi);
               return this.mApiLevel;
            } catch (NumberFormatException var3) {
               throw new IllegalStateException("Unexpected error: Build API level \'" + buildApi + "\' is not an integer: ");
            }
         }
      }
   }

   private boolean hasBinary(String path) {
      CountDownLatch latch = new CountDownLatch(1);
      CollectingOutputReceiver receiver = new CollectingOutputReceiver(latch);

      try {
         this.executeShellCommand("ls " + path, receiver);
      } catch (Exception var6) {
         return false;
      }

      try {
         latch.await(2L, TimeUnit.SECONDS);
      } catch (InterruptedException var5) {
         return false;
      }

      String value = receiver.getOutput().trim();
      return !value.endsWith("No such file or directory");
   }

   public String getMountPoint(String name) {
      String mount = (String)this.mMountPoints.get(name);
      if(mount == null) {
         try {
            mount = this.queryMountPoint(name);
            this.mMountPoints.put(name, mount);
         } catch (TimeoutException var4) {
            ;
         } catch (AdbCommandRejectedException var5) {
            ;
         } catch (ShellCommandUnresponsiveException var6) {
            ;
         } catch (IOException var7) {
            ;
         }
      }

      return mount;
   }

   private String queryMountPoint(String name) throws TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException, IOException {
      final AtomicReference ref = Atomics.newReference();
      this.executeShellCommand("echo $" + name, new MultiLineReceiver() {
         public boolean isCancelled() {
            return false;
         }

         public void processNewLines(String[] lines) {
            String[] arr$ = lines;
            int len$ = lines.length;

            for(int i$ = 0; i$ < len$; ++i$) {
               String line = arr$[i$];
               if(!line.isEmpty()) {
                  ref.set(line);
               }
            }

         }
      });
      return (String)ref.get();
   }

   public String toString() {
      return this.mSerialNumber;
   }

   public boolean isOnline() {
      return this.mState == IDevice.DeviceState.ONLINE;
   }

   public boolean isEmulator() {
      return this.mSerialNumber.matches("emulator-(\\d+)");
   }

   public boolean isOffline() {
      return this.mState == IDevice.DeviceState.OFFLINE;
   }

   public boolean isBootLoader() {
      return this.mState == IDevice.DeviceState.BOOTLOADER;
   }

   public SyncService getSyncService() throws TimeoutException, AdbCommandRejectedException, IOException {
      SyncService syncService = new SyncService(AndroidDebugBridge.getSocketAddress(), this);
      return syncService.openSync()?syncService:null;
   }

   public FileListingService getFileListingService() {
      return new FileListingService(this);
   }

   public RawImage getScreenshot() throws TimeoutException, AdbCommandRejectedException, IOException {
      return this.getScreenshot(0L, TimeUnit.MILLISECONDS);
   }

   public RawImage getScreenshot(long timeout, TimeUnit unit) throws TimeoutException, AdbCommandRejectedException, IOException {
      return AdbHelper.getFrameBuffer(AndroidDebugBridge.getSocketAddress(), this, timeout, unit);
   }

   public void startScreenRecorder(String remoteFilePath, ScreenRecorderOptions options, IShellOutputReceiver receiver) throws TimeoutException, AdbCommandRejectedException, IOException, ShellCommandUnresponsiveException {
      this.executeShellCommand(getScreenRecorderCommand(remoteFilePath, options), receiver, 0L, (TimeUnit)null);
   }

   static String getScreenRecorderCommand(String remoteFilePath, ScreenRecorderOptions options) {
      StringBuilder sb = new StringBuilder();
      sb.append("screenrecord");
      sb.append(' ');
      if(options.width > 0 && options.height > 0) {
         sb.append("--size ");
         sb.append(options.width);
         sb.append('x');
         sb.append(options.height);
         sb.append(' ');
      }

      if(options.bitrateMbps > 0) {
         sb.append("--bit-rate ");
         sb.append(options.bitrateMbps * 1000000);
         sb.append(' ');
      }

      if(options.timeLimit > 0L) {
         sb.append("--time-limit ");
         long seconds = TimeUnit.SECONDS.convert(options.timeLimit, options.timeLimitUnits);
         if(seconds > 180L) {
            seconds = 180L;
         }

         sb.append(seconds);
         sb.append(' ');
      }

      sb.append(remoteFilePath);
      return sb.toString();
   }

   public void executeShellCommand(String command, IShellOutputReceiver receiver) throws TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException, IOException {
      AdbHelper.executeRemoteCommand(AndroidDebugBridge.getSocketAddress(), command, this, receiver, DdmPreferences.getTimeOut());
   }

   public void executeShellCommand(String command, IShellOutputReceiver receiver, int maxTimeToOutputResponse) throws TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException, IOException {
      AdbHelper.executeRemoteCommand(AndroidDebugBridge.getSocketAddress(), command, this, receiver, maxTimeToOutputResponse);
   }

   public void executeShellCommand(String command, IShellOutputReceiver receiver, long maxTimeToOutputResponse, TimeUnit maxTimeUnits) throws TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException, IOException {
      AdbHelper.executeRemoteCommand(AndroidDebugBridge.getSocketAddress(), command, this, receiver, maxTimeToOutputResponse, maxTimeUnits);
   }

   public void runEventLogService(LogReceiver receiver) throws TimeoutException, AdbCommandRejectedException, IOException {
      AdbHelper.runEventLogService(AndroidDebugBridge.getSocketAddress(), this, receiver);
   }

   public void runLogService(String logname, LogReceiver receiver) throws TimeoutException, AdbCommandRejectedException, IOException {
      AdbHelper.runLogService(AndroidDebugBridge.getSocketAddress(), this, logname, receiver);
   }

   public void createForward(int localPort, int remotePort) throws TimeoutException, AdbCommandRejectedException, IOException {
      AdbHelper.createForward(AndroidDebugBridge.getSocketAddress(), this, String.format("tcp:%d", new Object[]{Integer.valueOf(localPort)}), String.format("tcp:%d", new Object[]{Integer.valueOf(remotePort)}));
   }

   public void createForward(int localPort, String remoteSocketName, IDevice.DeviceUnixSocketNamespace namespace) throws TimeoutException, AdbCommandRejectedException, IOException {
      AdbHelper.createForward(AndroidDebugBridge.getSocketAddress(), this, String.format("tcp:%d", new Object[]{Integer.valueOf(localPort)}), String.format("%s:%s", new Object[]{namespace.getType(), remoteSocketName}));
   }

   public void removeForward(int localPort, int remotePort) throws TimeoutException, AdbCommandRejectedException, IOException {
      AdbHelper.removeForward(AndroidDebugBridge.getSocketAddress(), this, String.format("tcp:%d", new Object[]{Integer.valueOf(localPort)}), String.format("tcp:%d", new Object[]{Integer.valueOf(remotePort)}));
   }

   public void removeForward(int localPort, String remoteSocketName, IDevice.DeviceUnixSocketNamespace namespace) throws TimeoutException, AdbCommandRejectedException, IOException {
      AdbHelper.removeForward(AndroidDebugBridge.getSocketAddress(), this, String.format("tcp:%d", new Object[]{Integer.valueOf(localPort)}), String.format("%s:%s", new Object[]{namespace.getType(), remoteSocketName}));
   }

   Device(DeviceMonitor monitor, String serialNumber, IDevice.DeviceState deviceState) {
      this.mMonitor = monitor;
      this.mSerialNumber = serialNumber;
      this.mState = deviceState;
   }

   DeviceMonitor getMonitor() {
      return this.mMonitor;
   }

   public boolean hasClients() {
      List var1 = this.mClients;
      synchronized(this.mClients) {
         return !this.mClients.isEmpty();
      }
   }

   public Client[] getClients() {
      List var1 = this.mClients;
      synchronized(this.mClients) {
         return (Client[])this.mClients.toArray(new Client[this.mClients.size()]);
      }
   }

   public Client getClient(String applicationName) {
      List var2 = this.mClients;
      synchronized(this.mClients) {
         Iterator i$ = this.mClients.iterator();

         Client c;
         do {
            if(!i$.hasNext()) {
               return null;
            }

            c = (Client)i$.next();
         } while(!applicationName.equals(c.getClientData().getClientDescription()));

         return c;
      }
   }

   void addClient(Client client) {
      List var2 = this.mClients;
      synchronized(this.mClients) {
         this.mClients.add(client);
      }

      this.addClientInfo(client);
   }

   List<Client> getClientList() {
      return this.mClients;
   }

   void clearClientList() {
      List var1 = this.mClients;
      synchronized(this.mClients) {
         this.mClients.clear();
      }

      this.clearClientInfo();
   }

   void removeClient(Client client, boolean notify) {
      this.mMonitor.addPortToAvailableList(client.getDebuggerListenPort());
      List var3 = this.mClients;
      synchronized(this.mClients) {
         this.mClients.remove(client);
      }

      if(notify) {
         this.mMonitor.getServer().deviceChanged(this, 2);
      }

      this.removeClientInfo(client);
   }

   void setClientMonitoringSocket(SocketChannel socketChannel) {
      this.mSocketChannel = socketChannel;
   }

   SocketChannel getClientMonitoringSocket() {
      return this.mSocketChannel;
   }

   void update(int changeMask) {
      this.mMonitor.getServer().deviceChanged(this, changeMask);
   }

   void update(Client client, int changeMask) {
      this.mMonitor.getServer().clientChanged(client, changeMask);
      this.updateClientInfo(client, changeMask);
   }

   void setMountingPoint(String name, String value) {
      this.mMountPoints.put(name, value);
   }

   private void addClientInfo(Client client) {
      ClientData cd = client.getClientData();
      this.setClientInfo(cd.getPid(), cd.getClientDescription());
   }

   private void updateClientInfo(Client client, int changeMask) {
      if((changeMask & 1) == 1) {
         this.addClientInfo(client);
      }

   }

   private void removeClientInfo(Client client) {
      int pid = client.getClientData().getPid();
      this.mClientInfo.remove(Integer.valueOf(pid));
   }

   private void clearClientInfo() {
      this.mClientInfo.clear();
   }

   private void setClientInfo(int pid, String pkgName) {
      if(pkgName == null) {
         pkgName = "";
      }

      this.mClientInfo.put(Integer.valueOf(pid), pkgName);
   }

   public String getClientName(int pid) {
      String pkgName = (String)this.mClientInfo.get(Integer.valueOf(pid));
      return pkgName == null?"":pkgName;
   }

   public void pushFile(String local, String remote) throws IOException, AdbCommandRejectedException, TimeoutException, SyncException {
      SyncService sync = null;

      try {
         String e = getFileName(local);
         Log.d(e, String.format("Uploading %1$s onto device \'%2$s\'", new Object[]{e, this.getSerialNumber()}));
         sync = this.getSyncService();
         if(sync == null) {
            throw new IOException("Unable to open sync connection!");
         }

         String message = String.format("Uploading file onto device \'%1$s\'", new Object[]{this.getSerialNumber()});
         Log.d("Device", message);
         sync.pushFile(local, remote, SyncService.getNullProgressMonitor());
      } catch (TimeoutException var11) {
         Log.e("Device", "Error during Sync: timeout.");
         throw var11;
      } catch (SyncException var12) {
         Log.e("Device", String.format("Error during Sync: %1$s", new Object[]{var12.getMessage()}));
         throw var12;
      } catch (IOException var13) {
         Log.e("Device", String.format("Error during Sync: %1$s", new Object[]{var13.getMessage()}));
         throw var13;
      } finally {
         if(sync != null) {
            sync.close();
         }

      }

   }

   public void pullFile(String remote, String local) throws IOException, AdbCommandRejectedException, TimeoutException, SyncException {
      SyncService sync = null;

      try {
         String e = getFileName(remote);
         Log.d(e, String.format("Downloading %1$s from device \'%2$s\'", new Object[]{e, this.getSerialNumber()}));
         sync = this.getSyncService();
         if(sync == null) {
            throw new IOException("Unable to open sync connection!");
         }

         String message = String.format("Downloading file from device \'%1$s\'", new Object[]{this.getSerialNumber()});
         Log.d("Device", message);
         sync.pullFile(remote, local, SyncService.getNullProgressMonitor());
      } catch (TimeoutException var11) {
         Log.e("Device", "Error during Sync: timeout.");
         throw var11;
      } catch (SyncException var12) {
         Log.e("Device", String.format("Error during Sync: %1$s", new Object[]{var12.getMessage()}));
         throw var12;
      } catch (IOException var13) {
         Log.e("Device", String.format("Error during Sync: %1$s", new Object[]{var13.getMessage()}));
         throw var13;
      } finally {
         if(sync != null) {
            sync.close();
         }

      }

   }

   public String installPackage(String packageFilePath, boolean reinstall, String... extraArgs) throws InstallException {
      try {
         String e = this.syncPackageToDevice(packageFilePath);
         String result = this.installRemotePackage(e, reinstall, extraArgs);
         this.removeRemotePackage(e);
         return result;
      } catch (IOException var6) {
         throw new InstallException(var6);
      } catch (AdbCommandRejectedException var7) {
         throw new InstallException(var7);
      } catch (TimeoutException var8) {
         throw new InstallException(var8);
      } catch (SyncException var9) {
         throw new InstallException(var9);
      }
   }

   public void installPackages(List<String> apkFilePaths, int timeOutInMs, boolean reinstall, String... extraArgs) throws InstallException {
      assert !apkFilePaths.isEmpty();

      if(this.getApiLevel() < 21) {
         Log.w("Internal error : installPackages invoked with device < 21 for %s", Joiner.on(",").join(apkFilePaths));
         if(apkFilePaths.size() == 1) {
            this.installPackage((String)apkFilePaths.get(0), reinstall, extraArgs);
         } else {
            Log.e("Internal error : installPackages invoked with device < 21 for multiple APK : %s", Joiner.on(",").join(apkFilePaths));
            throw new InstallException("Internal error : installPackages invoked with device < 21 for multiple APK : " + Joiner.on(",").join(apkFilePaths));
         }
      } else {
         String mainPackageFilePath = (String)apkFilePaths.get(0);
         Log.d(mainPackageFilePath, String.format("Uploading main %1$s and %2$s split APKs onto device \'%3$s\'", new Object[]{mainPackageFilePath, Joiner.on(',').join(apkFilePaths), this.getSerialNumber()}));

         try {
            ImmutableList e = extraArgs != null?ImmutableList.copyOf(extraArgs):ImmutableList.of();
            String sessionId = this.createMultiInstallSession(apkFilePaths, e, reinstall);
            if(sessionId == null) {
               Log.d(mainPackageFilePath, "Failed to establish session, quit installation");
               throw new InstallException("Failed to establish session");
            } else {
               Log.d(mainPackageFilePath, String.format("Established session id=%1$s", new Object[]{sessionId}));
               int index = 0;

               boolean allUploadSucceeded;
               for(allUploadSucceeded = true; allUploadSucceeded && index < apkFilePaths.size(); allUploadSucceeded = this.uploadAPK(sessionId, (String)apkFilePaths.get(index), index++)) {
                  ;
               }

               String command = allUploadSucceeded?"pm install-commit " + sessionId:"pm install-abandon " + sessionId;
               Device.InstallReceiver receiver = new Device.InstallReceiver();
               this.executeShellCommand(command, receiver, (long)timeOutInMs, TimeUnit.MILLISECONDS);
               String errorMessage = receiver.getErrorMessage();
               if(errorMessage != null) {
                  String message = String.format("Failed to finalize session : %1$s", new Object[]{errorMessage});
                  Log.e(mainPackageFilePath, message);
                  throw new InstallException(message);
               } else if(!allUploadSucceeded) {
                  throw new InstallException("Unable to upload some APKs");
               }
            }
         } catch (TimeoutException var14) {
            Log.e("Device", "Error during Sync: timeout.");
            throw new InstallException(var14);
         } catch (IOException var15) {
            Log.e("Device", String.format("Error during Sync: %1$s", new Object[]{var15.getMessage()}));
            throw new InstallException(var15);
         } catch (AdbCommandRejectedException var16) {
            throw new InstallException(var16);
         } catch (ShellCommandUnresponsiveException var17) {
            Log.e("Device", String.format("Error during shell execution: %1$s", new Object[]{var17.getMessage()}));
            throw new InstallException(var17);
         }
      }
   }

   private String createMultiInstallSession(List<String> apkFileNames, Collection<String> extraArgs, boolean reinstall) throws TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException, IOException {
      List apkFiles = Lists.transform(apkFileNames, new Function() {
         public File apply(Object input) {
            return new File((String)input);
         }
      });
      long totalFileSize = 0L;

      File receiver;
      for(Iterator parameters = apkFiles.iterator(); parameters.hasNext(); totalFileSize += receiver.length()) {
         receiver = (File)parameters.next();
         if(!receiver.exists() || !receiver.isFile()) {
            throw new IllegalArgumentException(receiver.getAbsolutePath() + " is not a file");
         }
      }

      StringBuilder parameters1 = new StringBuilder();
      if(reinstall) {
         parameters1.append("-r ");
      }

      parameters1.append(Joiner.on(' ').join(extraArgs));
      Device.MultiInstallReceiver receiver1 = new Device.MultiInstallReceiver(null);
      String cmd = String.format("pm install-create %1$s -S %2$d", new Object[]{parameters1.toString(), Long.valueOf(totalFileSize)});
      this.executeShellCommand(cmd, receiver1, DdmPreferences.getTimeOut());
      return receiver1.getSessionId();
   }

   private boolean uploadAPK(String sessionId, String apkFilePath, int uniqueId) {
      Log.d(sessionId, String.format("Uploading APK %1$s ", new Object[]{apkFilePath}));
      File fileToUpload = new File(apkFilePath);
      if(!fileToUpload.exists()) {
         Log.e(sessionId, String.format("File not found: %1$s", new Object[]{apkFilePath}));
         return false;
      } else if(fileToUpload.isDirectory()) {
         Log.e(sessionId, String.format("Directory upload not supported: %1$s", new Object[]{apkFilePath}));
         return false;
      } else {
         String baseName = fileToUpload.getName().lastIndexOf(46) != -1?fileToUpload.getName().substring(0, fileToUpload.getName().lastIndexOf(46)):fileToUpload.getName();
         baseName = UNSAFE_PM_INSTALL_SESSION_SPLIT_NAME_CHARS.replaceFrom(baseName, '_');
         String command = String.format("pm install-write -S %d %s %d_%s -", new Object[]{Long.valueOf(fileToUpload.length()), sessionId, Integer.valueOf(uniqueId), baseName});
         Log.d(sessionId, String.format("Executing : %1$s", new Object[]{command}));
         BufferedInputStream inputStream = null;

         boolean var9;
         try {
            inputStream = new BufferedInputStream(new FileInputStream(fileToUpload));
            Device.InstallReceiver e = new Device.InstallReceiver();
            AdbHelper.executeRemoteCommand(AndroidDebugBridge.getSocketAddress(), AdbHelper.AdbService.EXEC, command, this, e, (long)DdmPreferences.getTimeOut(), TimeUnit.MILLISECONDS, inputStream);
            if(e.getErrorMessage() != null) {
               Log.e(sessionId, String.format("Error while uploading %1$s : %2$s", new Object[]{fileToUpload.getName(), e.getErrorMessage()}));
            } else {
               Log.d(sessionId, String.format("Successfully uploaded %1$s", new Object[]{fileToUpload.getName()}));
            }

            var9 = e.getErrorMessage() == null;
            return var9;
         } catch (Exception var19) {
            Log.e(sessionId, (Throwable)var19);
            var9 = false;
         } finally {
            if(inputStream != null) {
               try {
                  inputStream.close();
               } catch (IOException var18) {
                  Log.e(sessionId, (Throwable)var18);
               }
            }

         }

         return var9;
      }
   }

   public String syncPackageToDevice(String localFilePath) throws IOException, AdbCommandRejectedException, TimeoutException, SyncException {
      SyncService sync = null;

      String message;
      try {
         String e = getFileName(localFilePath);
         String remoteFilePath = String.format("/data/local/tmp/%1$s", new Object[]{e});
         Log.d(e, String.format("Uploading %1$s onto device \'%2$s\'", new Object[]{e, this.getSerialNumber()}));
         sync = this.getSyncService();
         if(sync == null) {
            throw new IOException("Unable to open sync connection!");
         }

         message = String.format("Uploading file onto device \'%1$s\'", new Object[]{this.getSerialNumber()});
         Log.d("Device", message);
         sync.pushFile(localFilePath, remoteFilePath, SyncService.getNullProgressMonitor());
         message = remoteFilePath;
      } catch (TimeoutException var11) {
         Log.e("Device", "Error during Sync: timeout.");
         throw var11;
      } catch (SyncException var12) {
         Log.e("Device", String.format("Error during Sync: %1$s", new Object[]{var12.getMessage()}));
         throw var12;
      } catch (IOException var13) {
         Log.e("Device", String.format("Error during Sync: %1$s", new Object[]{var13.getMessage()}));
         throw var13;
      } finally {
         if(sync != null) {
            sync.close();
         }

      }

      return message;
   }

   private static String getFileName(String filePath) {
      return (new File(filePath)).getName();
   }

   public String installRemotePackage(String remoteFilePath, boolean reinstall, String... extraArgs) throws InstallException {
      try {
         Device.InstallReceiver e = new Device.InstallReceiver();
         StringBuilder optionString = new StringBuilder();
         if(reinstall) {
            optionString.append("-r ");
         }

         if(extraArgs != null) {
            optionString.append(Joiner.on(' ').join(extraArgs));
         }

         String cmd = String.format("pm install %1$s \"%2$s\"", new Object[]{optionString.toString(), remoteFilePath});
         this.executeShellCommand(cmd, e, INSTALL_TIMEOUT_MINUTES, TimeUnit.MINUTES);
         return e.getErrorMessage();
      } catch (TimeoutException var7) {
         throw new InstallException(var7);
      } catch (AdbCommandRejectedException var8) {
         throw new InstallException(var8);
      } catch (ShellCommandUnresponsiveException var9) {
         throw new InstallException(var9);
      } catch (IOException var10) {
         throw new InstallException(var10);
      }
   }

   public void removeRemotePackage(String remoteFilePath) throws InstallException {
      try {
         this.executeShellCommand(String.format("rm \"%1$s\"", new Object[]{remoteFilePath}), new NullOutputReceiver(), INSTALL_TIMEOUT_MINUTES, TimeUnit.MINUTES);
      } catch (IOException var3) {
         throw new InstallException(var3);
      } catch (TimeoutException var4) {
         throw new InstallException(var4);
      } catch (AdbCommandRejectedException var5) {
         throw new InstallException(var5);
      } catch (ShellCommandUnresponsiveException var6) {
         throw new InstallException(var6);
      }
   }

   public String uninstallPackage(String packageName) throws InstallException {
      try {
         Device.InstallReceiver e = new Device.InstallReceiver();
         this.executeShellCommand("pm uninstall " + packageName, e, INSTALL_TIMEOUT_MINUTES, TimeUnit.MINUTES);
         return e.getErrorMessage();
      } catch (TimeoutException var3) {
         throw new InstallException(var3);
      } catch (AdbCommandRejectedException var4) {
         throw new InstallException(var4);
      } catch (ShellCommandUnresponsiveException var5) {
         throw new InstallException(var5);
      } catch (IOException var6) {
         throw new InstallException(var6);
      }
   }

   public void reboot(String into) throws TimeoutException, AdbCommandRejectedException, IOException {
      AdbHelper.reboot(into, AndroidDebugBridge.getSocketAddress(), this);
   }

   public Integer getBatteryLevel() throws TimeoutException, AdbCommandRejectedException, IOException, ShellCommandUnresponsiveException {
      return this.getBatteryLevel(300000L);
   }

   public Integer getBatteryLevel(long freshnessMs) throws TimeoutException, AdbCommandRejectedException, IOException, ShellCommandUnresponsiveException {
      Future futureBattery = this.getBattery(freshnessMs, TimeUnit.MILLISECONDS);

      try {
         return (Integer)futureBattery.get();
      } catch (InterruptedException var5) {
         return null;
      } catch (ExecutionException var6) {
         return null;
      }
   }

   public Future<Integer> getBattery() {
      return this.getBattery(5L, TimeUnit.MINUTES);
   }

   public Future<Integer> getBattery(long freshnessTime, TimeUnit timeUnit) {
      return this.mBatteryFetcher.getBattery(freshnessTime, timeUnit);
   }

   public List<String> getAbis() {
      String abiList = this.getProperty("ro.product.cpu.abilist");
      if(abiList != null) {
         return Lists.newArrayList(abiList.split(","));
      } else {
         ArrayList abis = Lists.newArrayListWithExpectedSize(2);
         String abi = this.getProperty("ro.product.cpu.abi");
         if(abi != null) {
            abis.add(abi);
         }

         abi = this.getProperty("ro.product.cpu.abi2");
         if(abi != null) {
            abis.add(abi);
         }

         return abis;
      }
   }

   public int getDensity() {
      String densityValue = this.getProperty("ro.sf.lcd_density");
      if(densityValue != null) {
         try {
            return Integer.parseInt(densityValue);
         } catch (NumberFormatException var3) {
            return -1;
         }
      } else {
         return -1;
      }
   }

   public String getLanguage() {
      return (String)this.getProperties().get("persist.sys.language");
   }

   public String getRegion() {
      return this.getProperty("persist.sys.country");
   }

   static {
      String installTimeout = System.getenv("ADB_INSTALL_TIMEOUT");
      long time = 4L;
      if(installTimeout != null) {
         try {
            time = Long.parseLong(installTimeout);
         } catch (NumberFormatException var4) {
            ;
         }
      }

      INSTALL_TIMEOUT_MINUTES = time;
      UNSAFE_PM_INSTALL_SESSION_SPLIT_NAME_CHARS = CharMatcher.inRange('a', 'z').or(CharMatcher.inRange('A', 'Z')).or(CharMatcher.anyOf("_-")).negate();
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] $SwitchMap$com$android$ddmlib$IDevice$Feature = new int[IDevice.Feature.values().length];

      static {
         try {
            $SwitchMap$com$android$ddmlib$IDevice$Feature[IDevice.Feature.SCREEN_RECORD.ordinal()] = 1;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            $SwitchMap$com$android$ddmlib$IDevice$Feature[IDevice.Feature.PROCSTATS.ordinal()] = 2;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }

   private static class MultiInstallReceiver extends MultiLineReceiver {
      private static final Pattern successPattern = Pattern.compile("Success: .*\\[(\\d*)\\]");
      String sessionId;

      private MultiInstallReceiver() {
         this.sessionId = null;
      }

      public boolean isCancelled() {
         return false;
      }

      public void processNewLines(String[] lines) {
         String[] arr$ = lines;
         int len$ = lines.length;

         for(int i$ = 0; i$ < len$; ++i$) {
            String line = arr$[i$];
            Matcher matcher = successPattern.matcher(line);
            if(matcher.matches()) {
               this.sessionId = matcher.group(1);
            }
         }

      }

      public String getSessionId() {
         return this.sessionId;
      }

      // $FF: synthetic method
      MultiInstallReceiver(Object x0) {
         this();
      }
   }

   private static final class InstallReceiver extends MultiLineReceiver {
      private static final String SUCCESS_OUTPUT = "Success";
      private static final Pattern FAILURE_PATTERN = Pattern.compile("Failure\\s+\\[(.*)\\]");
      private String mErrorMessage = null;

      public void processNewLines(String[] lines) {
         String[] arr$ = lines;
         int len$ = lines.length;

         for(int i$ = 0; i$ < len$; ++i$) {
            String line = arr$[i$];
            if(!line.isEmpty()) {
               if(line.startsWith("Success")) {
                  this.mErrorMessage = null;
               } else {
                  Matcher m = FAILURE_PATTERN.matcher(line);
                  if(m.matches()) {
                     this.mErrorMessage = m.group(1);
                  } else {
                     this.mErrorMessage = "Unknown failure";
                  }
               }
            }
         }

      }

      public boolean isCancelled() {
         return false;
      }

      public String getErrorMessage() {
         return this.mErrorMessage;
      }
   }
}
