package com.android.ddmlib;

import com.android.ddmlib.AndroidDebugBridge;
import com.android.ddmlib.ByteBufferUtil;
import com.android.ddmlib.Client;
import com.android.ddmlib.DebugPortManager;
import com.android.ddmlib.Device;
import com.android.ddmlib.DeviceMonitor;
import com.android.ddmlib.JdwpPacket;
import com.android.ddmlib.Log;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

abstract class ChunkHandler {
   public static final int CHUNK_HEADER_LEN = 8;
   public static final ByteOrder CHUNK_ORDER;
   public static final int CHUNK_FAIL;

   abstract void clientReady(Client var1) throws IOException;

   abstract void clientDisconnected(Client var1);

   abstract void handleChunk(Client var1, int var2, ByteBuffer var3, boolean var4, int var5);

   protected void handleUnknownChunk(Client client, int type, ByteBuffer data, boolean isReply, int msgId) {
      if(type == CHUNK_FAIL) {
         int errorCode = data.getInt();
         int msgLen = data.getInt();
         String msg = ByteBufferUtil.getString(data, msgLen);
         Log.w("ddms", "WARNING: failure code=" + errorCode + " msg=" + msg);
      } else {
         Log.w("ddms", "WARNING: received unknown chunk " + name(type) + ": len=" + data.limit() + ", reply=" + isReply + ", msgId=0x" + Integer.toHexString(msgId));
      }

      Log.w("ddms", "         client " + client + ", handler " + this);
   }

   public static String getString(ByteBuffer buf, int len) {
      return ByteBufferUtil.getString(buf, len);
   }

   static int type(String typeName) {
      int val = 0;
      if(typeName.length() != 4) {
         Log.e("ddms", "Type name must be 4 letter long");
         throw new RuntimeException("Type name must be 4 letter long");
      } else {
         for(int i = 0; i < 4; ++i) {
            val <<= 8;
            val |= (byte)typeName.charAt(i);
         }

         return val;
      }
   }

   static String name(int type) {
      char[] ascii = new char[]{(char)(type >> 24 & 255), (char)(type >> 16 & 255), (char)(type >> 8 & 255), (char)(type & 255)};
      return new String(ascii);
   }

   static ByteBuffer allocBuffer(int maxChunkLen) {
      ByteBuffer buf = ByteBuffer.allocate(19 + maxChunkLen);
      buf.order(CHUNK_ORDER);
      return buf;
   }

   static ByteBuffer getChunkDataBuf(ByteBuffer jdwpBuf) {
      assert jdwpBuf.position() == 0;

      jdwpBuf.position(19);
      ByteBuffer slice = jdwpBuf.slice();
      slice.order(CHUNK_ORDER);
      jdwpBuf.position(0);
      return slice;
   }

   static void finishChunkPacket(JdwpPacket packet, int type, int chunkLen) {
      ByteBuffer buf = packet.getPayload();
      buf.putInt(0, type);
      buf.putInt(4, chunkLen);
      packet.finishPacket(8 + chunkLen);
   }

   protected static Client checkDebuggerPortForAppName(Client client, String appName) {
      DebugPortManager.IDebugPortProvider provider = DebugPortManager.getProvider();
      if(provider != null) {
         Device device = client.getDeviceImpl();
         int newPort = provider.getPort(device, appName);
         if(newPort != -1 && newPort != client.getDebuggerListenPort()) {
            AndroidDebugBridge bridge = AndroidDebugBridge.getBridge();
            if(bridge != null) {
               DeviceMonitor deviceMonitor = bridge.getDeviceMonitor();
               if(deviceMonitor != null) {
                  deviceMonitor.addClientToDropAndReopen(client, newPort);
                  client = null;
               }
            }
         }
      }

      return client;
   }

   static {
      CHUNK_ORDER = ByteOrder.BIG_ENDIAN;
      CHUNK_FAIL = type("FAIL");
   }
}
