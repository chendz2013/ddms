package com.android.ddmlib;

public interface IStackTraceInfo {
   StackTraceElement[] getStackTrace();
}
