package com.android.ddmlib;

import com.android.ddmlib.ByteBufferUtil;
import com.android.ddmlib.ChunkHandler;
import com.android.ddmlib.Client;
import com.android.ddmlib.ClientData;
import com.android.ddmlib.Log;
import com.android.ddmlib.MonitorThread;
import java.io.IOException;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;

/**
 * 获取App名称和描述
 */
final class HandleAppName extends ChunkHandler {
   public static final int CHUNK_APNM = ChunkHandler.type("APNM");
   private static final HandleAppName mInst = new HandleAppName();

   public static void register(MonitorThread mt) {
      mt.registerChunkHandler(CHUNK_APNM, mInst);
   }

   public void clientReady(Client client) throws IOException {
   }

   public void clientDisconnected(Client client) {
   }

   public void handleChunk(Client client, int type, ByteBuffer data, boolean isReply, int msgId) {
      Log.d("ddm-appname", "handling " + ChunkHandler.name(type));
      if(type == CHUNK_APNM) {
         assert !isReply;

         handleAPNM(client, data);
      } else {
         this.handleUnknownChunk(client, type, data, isReply, msgId);
      }

   }

   private static void handleAPNM(Client client, ByteBuffer data) {
      int appNameLen = data.getInt();
      String appName = ByteBufferUtil.getString(data, appNameLen);
      int userId = -1;
      boolean validUserId = false;
      if(data.hasRemaining()) {
         try {
            userId = data.getInt();
            validUserId = true;
         } catch (BufferUnderflowException var9) {
            int expectedPacketLength = 8 + appNameLen * 2;
            Log.e("ddm-appname", "Insufficient data in APNM chunk to retrieve user id.");
            Log.e("ddm-appname", "Actual chunk length: " + data.capacity());
            Log.e("ddm-appname", "Expected chunk length: " + expectedPacketLength);
         }
      }

      Log.d("ddm-appname", "APNM: app=\'" + appName + "\'");
      ClientData cd = client.getClientData();
      synchronized(cd) {
         cd.setClientDescription(appName);
         if(validUserId) {
            cd.setUserId(userId);
         }
      }

      client = checkDebuggerPortForAppName(client, appName);
      if(client != null) {
         client.update(1);
      }

   }
}
