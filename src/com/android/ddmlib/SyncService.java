package com.android.ddmlib;

import com.android.ddmlib.AdbCommandRejectedException;
import com.android.ddmlib.AdbHelper;
import com.android.ddmlib.DdmPreferences;
import com.android.ddmlib.Device;
import com.android.ddmlib.FileListingService;
import com.android.ddmlib.Log;
import com.android.ddmlib.SyncException;
import com.android.ddmlib.TimeoutException;
import com.android.ddmlib.utils.ArrayHelper;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Date;

/**
 * 与设备之间的同步服务--本质还是adb的
 */
public class SyncService {
   private static final byte[] ID_OKAY = new byte[]{(byte)79, (byte)75, (byte)65, (byte)89};
   private static final byte[] ID_FAIL = new byte[]{(byte)70, (byte)65, (byte)73, (byte)76};
   private static final byte[] ID_STAT = new byte[]{(byte)83, (byte)84, (byte)65, (byte)84};
   private static final byte[] ID_RECV = new byte[]{(byte)82, (byte)69, (byte)67, (byte)86};
   private static final byte[] ID_DATA = new byte[]{(byte)68, (byte)65, (byte)84, (byte)65};
   private static final byte[] ID_DONE = new byte[]{(byte)68, (byte)79, (byte)78, (byte)69};
   private static final byte[] ID_SEND = new byte[]{(byte)83, (byte)69, (byte)78, (byte)68};
   private static final SyncService.NullSyncProgressMonitor sNullSyncProgressMonitor = new SyncService.NullSyncProgressMonitor();
   private static final int S_ISOCK = 49152;
   private static final int S_IFLNK = 40960;
   private static final int S_IFREG = 32768;
   private static final int S_IFBLK = 24576;
   private static final int S_IFDIR = 16384;
   private static final int S_IFCHR = 8192;
   private static final int S_IFIFO = 4096;
   private static final int SYNC_DATA_MAX = 65536;
   private static final int REMOTE_PATH_MAX_LENGTH = 1024;
   private InetSocketAddress mAddress;
   private Device mDevice;
   private SocketChannel mChannel;
   private byte[] mBuffer;

   SyncService(InetSocketAddress address, Device device) {
      this.mAddress = address;
      this.mDevice = device;
   }

   boolean openSync() throws TimeoutException, AdbCommandRejectedException, IOException {
      try {
         this.mChannel = SocketChannel.open(this.mAddress);
         this.mChannel.configureBlocking(false);
         AdbHelper.setDevice(this.mChannel, this.mDevice);
         byte[] e = AdbHelper.formAdbRequest("sync:");
         AdbHelper.write(this.mChannel, e, -1, DdmPreferences.getTimeOut());
         AdbHelper.AdbResponse e2 = AdbHelper.readAdbResponse(this.mChannel, false);
         if(!e2.okay) {
            Log.w("ddms", "Got unhappy response from ADB sync req: " + e2.message);
            this.mChannel.close();
            this.mChannel = null;
            return false;
         } else {
            return true;
         }
      } catch (TimeoutException var5) {
         if(this.mChannel != null) {
            try {
               this.mChannel.close();
            } catch (IOException var4) {
               ;
            }

            this.mChannel = null;
         }

         throw var5;
      } catch (IOException var6) {
         if(this.mChannel != null) {
            try {
               this.mChannel.close();
            } catch (IOException var3) {
               ;
            }

            this.mChannel = null;
         }

         throw var6;
      }
   }

   public void close() {
      if(this.mChannel != null) {
         try {
            this.mChannel.close();
         } catch (IOException var2) {
            ;
         }

         this.mChannel = null;
      }

   }

   public static SyncService.ISyncProgressMonitor getNullProgressMonitor() {
      return sNullSyncProgressMonitor;
   }

   public void pull(FileListingService.FileEntry[] entries, String localPath, SyncService.ISyncProgressMonitor monitor) throws SyncException, IOException, TimeoutException {
      File f = new File(localPath);
      if(!f.exists()) {
         throw new SyncException(SyncException.SyncError.NO_DIR_TARGET);
      } else if(!f.isDirectory()) {
         throw new SyncException(SyncException.SyncError.TARGET_IS_FILE);
      } else {
         FileListingService fls = new FileListingService(this.mDevice);
         int total = this.getTotalRemoteFileSize(entries, fls);
         monitor.start(total);
         this.doPull(entries, localPath, fls, monitor);
         monitor.stop();
      }
   }

   public void pullFile(FileListingService.FileEntry remote, String localFilename, SyncService.ISyncProgressMonitor monitor) throws IOException, SyncException, TimeoutException {
      int total = remote.getSizeValue();
      monitor.start(total);
      this.doPullFile(remote.getFullPath(), localFilename, monitor);
      monitor.stop();
   }

   public void pullFile(String remoteFilepath, String localFilename, SyncService.ISyncProgressMonitor monitor) throws TimeoutException, IOException, SyncException {
      SyncService.FileStat fileStat = this.statFile(remoteFilepath);
      if(fileStat != null && fileStat.getMode() == 0) {
         throw new SyncException(SyncException.SyncError.NO_REMOTE_OBJECT);
      } else {
         monitor.start(0);
         this.doPullFile(remoteFilepath, localFilename, monitor);
         monitor.stop();
      }
   }

   public void push(String[] local, FileListingService.FileEntry remote, SyncService.ISyncProgressMonitor monitor) throws SyncException, IOException, TimeoutException {
      if(!remote.isDirectory()) {
         throw new SyncException(SyncException.SyncError.REMOTE_IS_FILE);
      } else {
         ArrayList files = new ArrayList();
         String[] fileArray = local;
         int total = local.length;

         for(int i$ = 0; i$ < total; ++i$) {
            String path = fileArray[i$];
            files.add(new File(path));
         }

         File[] var9 = (File[])files.toArray(new File[files.size()]);
         total = this.getTotalLocalFileSize(var9);
         monitor.start(total);
         this.doPush(var9, remote.getFullPath(), monitor);
         monitor.stop();
      }
   }

   public void pushFile(String local, String remote, SyncService.ISyncProgressMonitor monitor) throws SyncException, IOException, TimeoutException {
      File f = new File(local);
      if(!f.exists()) {
         throw new SyncException(SyncException.SyncError.NO_LOCAL_FILE);
      } else if(f.isDirectory()) {
         throw new SyncException(SyncException.SyncError.LOCAL_IS_DIRECTORY);
      } else {
         monitor.start((int)f.length());
         this.doPushFile(local, remote, monitor);
         monitor.stop();
      }
   }

   private int getTotalRemoteFileSize(FileListingService.FileEntry[] entries, FileListingService fls) {
      int count = 0;
      FileListingService.FileEntry[] arr$ = entries;
      int len$ = entries.length;

      for(int i$ = 0; i$ < len$; ++i$) {
         FileListingService.FileEntry e = arr$[i$];
         int type = e.getType();
         if(type == 1) {
            FileListingService.FileEntry[] children = fls.getChildren(e, false, (FileListingService.IListingReceiver)null);
            count += this.getTotalRemoteFileSize(children, fls) + 1;
         } else if(type == 0) {
            count += e.getSizeValue();
         }
      }

      return count;
   }

   private int getTotalLocalFileSize(File[] files) {
      int count = 0;
      File[] arr$ = files;
      int len$ = files.length;

      for(int i$ = 0; i$ < len$; ++i$) {
         File f = arr$[i$];
         if(f.exists()) {
            if(f.isDirectory()) {
               return this.getTotalLocalFileSize(f.listFiles()) + 1;
            }

            if(f.isFile()) {
               count = (int)((long)count + f.length());
            }
         }
      }

      return count;
   }

   private void doPull(FileListingService.FileEntry[] entries, String localPath, FileListingService fileListingService, SyncService.ISyncProgressMonitor monitor) throws SyncException, IOException, TimeoutException {
      FileListingService.FileEntry[] arr$ = entries;
      int len$ = entries.length;

      for(int i$ = 0; i$ < len$; ++i$) {
         FileListingService.FileEntry e = arr$[i$];
         if(monitor.isCanceled()) {
            throw new SyncException(SyncException.SyncError.CANCELED);
         }

         int type = e.getType();
         String dest;
         if(type == 1) {
            monitor.startSubTask(e.getFullPath());
            dest = localPath + File.separator + e.getName();
            File d = new File(dest);
            d.mkdir();
            FileListingService.FileEntry[] children = fileListingService.getChildren(e, true, (FileListingService.IListingReceiver)null);
            this.doPull(children, dest, fileListingService, monitor);
            monitor.advance(1);
         } else if(type == 0) {
            monitor.startSubTask(e.getFullPath());
            dest = localPath + File.separator + e.getName();
            this.doPullFile(e.getFullPath(), dest, monitor);
         }
      }

   }

   private void doPullFile(String remotePath, String localPath, SyncService.ISyncProgressMonitor monitor) throws IOException, SyncException, TimeoutException {
      Object msg = null;
      byte[] pullResult = new byte[8];
      int timeOut = DdmPreferences.getTimeOut();

      try {
         byte[] f = remotePath.getBytes("ISO-8859-1");
         if(f.length > 1024) {
            throw new SyncException(SyncException.SyncError.REMOTE_PATH_LENGTH);
         }

         byte[] msg1 = createFileReq(ID_RECV, f);
         AdbHelper.write(this.mChannel, msg1, -1, timeOut);
         AdbHelper.read(this.mChannel, pullResult, -1, (long)timeOut);
         if(!checkResult(pullResult, ID_DATA) && !checkResult(pullResult, ID_DONE)) {
            throw new SyncException(SyncException.SyncError.TRANSFER_PROTOCOL_ERROR, this.readErrorMessage(pullResult, timeOut));
         }
      } catch (UnsupportedEncodingException var17) {
         throw new SyncException(SyncException.SyncError.REMOTE_PATH_ENCODING, var17);
      }

      File f1 = new File(localPath);
      FileOutputStream fos = null;

      try {
         fos = new FileOutputStream(f1);
         byte[] e = new byte[65536];

         while(!monitor.isCanceled()) {
            if(checkResult(pullResult, ID_DONE)) {
               fos.flush();
               return;
            }

            if(!checkResult(pullResult, ID_DATA)) {
               throw new SyncException(SyncException.SyncError.TRANSFER_PROTOCOL_ERROR, this.readErrorMessage(pullResult, timeOut));
            }

            int length = ArrayHelper.swap32bitFromArray(pullResult, 4);
            if(length > 65536) {
               throw new SyncException(SyncException.SyncError.BUFFER_OVERRUN);
            }

            AdbHelper.read(this.mChannel, e, length, (long)timeOut);
            AdbHelper.read(this.mChannel, pullResult, -1, (long)timeOut);
            fos.write(e, 0, length);
            monitor.advance(length);
         }

         throw new SyncException(SyncException.SyncError.CANCELED);
      } catch (IOException var15) {
         Log.e("ddms", String.format("Failed to open local file %s for writing, Reason: %s", new Object[]{f1.getAbsolutePath(), var15.toString()}));
         throw new SyncException(SyncException.SyncError.FILE_WRITE_ERROR);
      } finally {
         if(fos != null) {
            fos.close();
         }

      }
   }

   private void doPush(File[] fileArray, String remotePath, SyncService.ISyncProgressMonitor monitor) throws SyncException, IOException, TimeoutException {
      File[] arr$ = fileArray;
      int len$ = fileArray.length;

      for(int i$ = 0; i$ < len$; ++i$) {
         File f = arr$[i$];
         if(monitor.isCanceled()) {
            throw new SyncException(SyncException.SyncError.CANCELED);
         }

         if(f.exists()) {
            String remoteFile;
            if(f.isDirectory()) {
               remoteFile = remotePath + "/" + f.getName();
               monitor.startSubTask(remoteFile);
               this.doPush(f.listFiles(), remoteFile, monitor);
               monitor.advance(1);
            } else if(f.isFile()) {
               remoteFile = remotePath + "/" + f.getName();
               monitor.startSubTask(remoteFile);
               this.doPushFile(f.getAbsolutePath(), remoteFile, monitor);
            }
         }
      }

   }

   private void doPushFile(String localPath, String remotePath, SyncService.ISyncProgressMonitor monitor) throws SyncException, IOException, TimeoutException {
      FileInputStream fis = null;
      int timeOut = DdmPreferences.getTimeOut();
      File f = new File(localPath);

      byte[] msg;
      try {
         byte[] time = remotePath.getBytes("ISO-8859-1");
         if(time.length > 1024) {
            throw new SyncException(SyncException.SyncError.REMOTE_PATH_LENGTH);
         }

         fis = new FileInputStream(f);
         msg = createSendFileReq(ID_SEND, time, 420);
         AdbHelper.write(this.mChannel, msg, -1, timeOut);
         System.arraycopy(ID_DATA, 0, this.getBuffer(), 0, ID_DATA.length);

         while(true) {
            if(monitor.isCanceled()) {
               throw new SyncException(SyncException.SyncError.CANCELED);
            }

            int readCount = fis.read(this.getBuffer(), 8, 65536);
            if(readCount == -1) {
               break;
            }

            ArrayHelper.swap32bitsToArray(readCount, this.getBuffer(), 4);
            AdbHelper.write(this.mChannel, this.getBuffer(), readCount + 8, timeOut);
            monitor.advance(readCount);
         }
      } catch (UnsupportedEncodingException var13) {
         throw new SyncException(SyncException.SyncError.REMOTE_PATH_ENCODING, var13);
      } finally {
         if(fis != null) {
            fis.close();
         }

      }

      long time1 = f.lastModified() / 1000L;
      msg = createReq(ID_DONE, (int)time1);
      AdbHelper.write(this.mChannel, msg, -1, timeOut);
      byte[] result = new byte[8];
      AdbHelper.read(this.mChannel, result, -1, (long)timeOut);
      if(!checkResult(result, ID_OKAY)) {
         throw new SyncException(SyncException.SyncError.TRANSFER_PROTOCOL_ERROR, this.readErrorMessage(result, timeOut));
      }
   }

   private String readErrorMessage(byte[] result, int timeOut) throws TimeoutException, IOException {
      if(checkResult(result, ID_FAIL)) {
         int len = ArrayHelper.swap32bitFromArray(result, 4);
         if(len > 0) {
            AdbHelper.read(this.mChannel, this.getBuffer(), len, (long)timeOut);
            String message = new String(this.getBuffer(), 0, len);
            Log.e("ddms", "transfer error: " + message);
            return message;
         }
      }

      return null;
   }

   public SyncService.FileStat statFile(String path) throws TimeoutException, IOException {
      byte[] msg = createFileReq(ID_STAT, path);
      AdbHelper.write(this.mChannel, msg, -1, DdmPreferences.getTimeOut());
      byte[] statResult = new byte[16];
      AdbHelper.read(this.mChannel, statResult, -1, (long)DdmPreferences.getTimeOut());
      if(!checkResult(statResult, ID_STAT)) {
         return null;
      } else {
         int mode = ArrayHelper.swap32bitFromArray(statResult, 4);
         int size = ArrayHelper.swap32bitFromArray(statResult, 8);
         int lastModifiedSecs = ArrayHelper.swap32bitFromArray(statResult, 12);
         return new SyncService.FileStat(mode, size, lastModifiedSecs);
      }
   }

   private static byte[] createReq(byte[] command, int value) {
      byte[] array = new byte[8];
      System.arraycopy(command, 0, array, 0, 4);
      ArrayHelper.swap32bitsToArray(value, array, 4);
      return array;
   }

   private static byte[] createFileReq(byte[] command, String path) {
      Object pathContent = null;

      byte[] pathContent1;
      try {
         pathContent1 = path.getBytes("ISO-8859-1");
      } catch (UnsupportedEncodingException var4) {
         return null;
      }

      return createFileReq(command, pathContent1);
   }

   private static byte[] createFileReq(byte[] command, byte[] path) {
      byte[] array = new byte[8 + path.length];
      System.arraycopy(command, 0, array, 0, 4);
      ArrayHelper.swap32bitsToArray(path.length, array, 4);
      System.arraycopy(path, 0, array, 8, path.length);
      return array;
   }

   private static byte[] createSendFileReq(byte[] command, byte[] path, int mode) {
      String modeStr = "," + (mode & 511);
      Object modeContent = null;

      byte[] modeContent1;
      try {
         modeContent1 = modeStr.getBytes("ISO-8859-1");
      } catch (UnsupportedEncodingException var6) {
         return null;
      }

      byte[] array = new byte[8 + path.length + modeContent1.length];
      System.arraycopy(command, 0, array, 0, 4);
      ArrayHelper.swap32bitsToArray(path.length + modeContent1.length, array, 4);
      System.arraycopy(path, 0, array, 8, path.length);
      System.arraycopy(modeContent1, 0, array, 8 + path.length, modeContent1.length);
      return array;
   }

   private static boolean checkResult(byte[] result, byte[] code) {
      return result[0] == code[0] && result[1] == code[1] && result[2] == code[2] && result[3] == code[3];
   }

   private static int getFileType(int mode) {
      return (mode & '쀀') == '쀀'?6:((mode & 'ꀀ') == 'ꀀ'?5:((mode & '耀') == '耀'?0:((mode & 24576) == 24576?3:((mode & 16384) == 16384?1:((mode & 8192) == 8192?4:((mode & 4096) == 4096?7:8))))));
   }

   private byte[] getBuffer() {
      if(this.mBuffer == null) {
         this.mBuffer = new byte[65544];
      }

      return this.mBuffer;
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
   }

   private static class NullSyncProgressMonitor implements SyncService.ISyncProgressMonitor {
      private NullSyncProgressMonitor() {
      }

      public void advance(int work) {
      }

      public boolean isCanceled() {
         return false;
      }

      public void start(int totalWork) {
      }

      public void startSubTask(String name) {
      }

      public void stop() {
      }

      // $FF: synthetic method
      NullSyncProgressMonitor(SyncService.SyntheticClass_1 x0) {
         this();
      }
   }

   public static class FileStat {
      private final int myMode;
      private final int mySize;
      private final Date myLastModified;

      public FileStat(int mode, int size, int lastModifiedSecs) {
         this.myMode = mode;
         this.mySize = size;
         this.myLastModified = new Date((long)lastModifiedSecs * 1000L);
      }

      public int getMode() {
         return this.myMode;
      }

      public int getSize() {
         return this.mySize;
      }

      public Date getLastModified() {
         return this.myLastModified;
      }
   }

   public interface ISyncProgressMonitor {
      void start(int var1);

      void stop();

      boolean isCanceled();

      void startSubTask(String var1);

      void advance(int var1);
   }
}
