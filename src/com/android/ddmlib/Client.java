package com.android.ddmlib;

import com.android.ddmlib.ChunkHandler;
import com.android.ddmlib.ClientData;
import com.android.ddmlib.DdmPreferences;
import com.android.ddmlib.Debugger;
import com.android.ddmlib.Device;
import com.android.ddmlib.HandleExit;
import com.android.ddmlib.HandleHeap;
import com.android.ddmlib.HandleHello;
import com.android.ddmlib.HandleNativeHeap;
import com.android.ddmlib.HandleProfiling;
import com.android.ddmlib.HandleThread;
import com.android.ddmlib.HandleViewDebug;
import com.android.ddmlib.IDevice;
import com.android.ddmlib.JdwpPacket;
import com.android.ddmlib.Log;
import com.android.ddmlib.MonitorThread;
import java.io.IOException;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public class Client {
   private static final int SERVER_PROTOCOL_VERSION = 1;
   public static final int CHANGE_NAME = 1;
   public static final int CHANGE_DEBUGGER_STATUS = 2;
   public static final int CHANGE_PORT = 4;
   public static final int CHANGE_THREAD_MODE = 8;
   public static final int CHANGE_THREAD_DATA = 16;
   public static final int CHANGE_HEAP_MODE = 32;
   public static final int CHANGE_HEAP_DATA = 64;
   public static final int CHANGE_NATIVE_HEAP_DATA = 128;
   public static final int CHANGE_THREAD_STACKTRACE = 256;
   public static final int CHANGE_HEAP_ALLOCATIONS = 512;
   public static final int CHANGE_HEAP_ALLOCATION_STATUS = 1024;
   public static final int CHANGE_METHOD_PROFILING_STATUS = 2048;
   public static final int CHANGE_HPROF = 4096;
   public static final int CHANGE_INFO = 7;
   private SocketChannel mChan;
   private Debugger mDebugger;
   private int mDebuggerListenPort;
   private HashMap<Integer, ChunkHandler> mOutstandingReqs;
   private ClientData mClientData;
   private boolean mThreadUpdateEnabled;
   private boolean mHeapInfoUpdateEnabled;
   private boolean mHeapSegmentUpdateEnabled;
   private static final int INITIAL_BUF_SIZE = 2048;
   private static final int MAX_BUF_SIZE = 838860800;
   private ByteBuffer mReadBuffer;
   private static final int WRITE_BUF_SIZE = 256;
   private ByteBuffer mWriteBuffer;
   private Device mDevice;
   private int mConnState;
   private static final int ST_INIT = 1;
   private static final int ST_NOT_JDWP = 2;
   private static final int ST_AWAIT_SHAKE = 10;
   private static final int ST_NEED_DDM_PKT = 11;
   private static final int ST_NOT_DDM = 12;
   private static final int ST_READY = 13;
   private static final int ST_ERROR = 20;
   private static final int ST_DISCONNECTED = 21;

   Client(Device device, SocketChannel chan, int pid) {
      this.mDevice = device;
      this.mChan = chan;
      this.mReadBuffer = ByteBuffer.allocate(2048);
      this.mWriteBuffer = ByteBuffer.allocate(256);
      this.mOutstandingReqs = new HashMap();
      this.mConnState = 1;
      this.mClientData = new ClientData(pid);
      this.mThreadUpdateEnabled = DdmPreferences.getInitialThreadUpdate();
      this.mHeapInfoUpdateEnabled = DdmPreferences.getInitialHeapUpdate();
      this.mHeapSegmentUpdateEnabled = DdmPreferences.getInitialHeapUpdate();
   }

   public String toString() {
      return "[Client pid: " + this.mClientData.getPid() + "]";
   }

   public IDevice getDevice() {
      return this.mDevice;
   }

   Device getDeviceImpl() {
      return this.mDevice;
   }

   public int getDebuggerListenPort() {
      return this.mDebuggerListenPort;
   }

   public boolean isDdmAware() {
      switch(this.mConnState) {
      case 1:
      case 2:
      case 10:
      case 11:
      case 12:
      case 20:
      case 21:
         return false;
      case 3:
      case 4:
      case 5:
      case 6:
      case 7:
      case 8:
      case 9:
      case 14:
      case 15:
      case 16:
      case 17:
      case 18:
      case 19:
      default:
         assert false;

         return false;
      case 13:
         return true;
      }
   }

   public boolean isDebuggerAttached() {
      return this.mDebugger.isDebuggerAttached();
   }

   Debugger getDebugger() {
      return this.mDebugger;
   }

   public ClientData getClientData() {
      return this.mClientData;
   }

   public void executeGarbageCollector() {
      try {
         HandleHeap.sendHPGC(this);
      } catch (IOException var2) {
         Log.w("ddms", "Send of HPGC message failed");
      }

   }

   public void dumpHprof() {
      boolean canStream = this.mClientData.hasFeature("hprof-heap-dump-streaming");

      try {
         if(canStream) {
            HandleHeap.sendHPDS(this);
         } else {
            String e = "/sdcard/" + this.mClientData.getClientDescription().replaceAll("\\:.*", "") + ".hprof";
            HandleHeap.sendHPDU(this, e);
         }
      } catch (IOException var3) {
         Log.w("ddms", "Send of HPDU message failed");
      }

   }

   /** @deprecated */
   public void toggleMethodProfiling() {
      try {
         switch(Client.SyntheticClass_1.$SwitchMap$com$android$ddmlib$ClientData$MethodProfilingStatus[this.mClientData.getMethodProfilingStatus().ordinal()]) {
         case 1:
            this.stopMethodTracer();
            break;
         case 2:
            this.stopSamplingProfiler();
            break;
         case 3:
            this.startMethodTracer();
         }
      } catch (IOException var2) {
         Log.w("ddms", "Toggle method profiling failed");
      }

   }

   private int getProfileBufferSize() {
      return DdmPreferences.getProfilerBufferSizeMb() * 1024 * 1024;
   }

   public void startMethodTracer() throws IOException {
      boolean canStream = this.mClientData.hasFeature("method-trace-profiling-streaming");
      int bufferSize = this.getProfileBufferSize();
      if(canStream) {
         HandleProfiling.sendMPSS(this, bufferSize, 0);
      } else {
         String file = "/sdcard/" + this.mClientData.getClientDescription().replaceAll("\\:.*", "") + ".trace";
         HandleProfiling.sendMPRS(this, file, bufferSize, 0);
      }

   }

   /**
    * 停止方法跟踪
    * @throws IOException
     */
   public void stopMethodTracer() throws IOException {
      boolean canStream = this.mClientData.hasFeature("method-trace-profiling-streaming");
      if(canStream) {
         HandleProfiling.sendMPSE(this);
      } else {
         HandleProfiling.sendMPRE(this);
      }

   }

   public void startSamplingProfiler(int samplingInterval, TimeUnit timeUnit) throws IOException {
      int bufferSize = this.getProfileBufferSize();
      HandleProfiling.sendSPSS(this, bufferSize, samplingInterval, timeUnit);
   }

   /**
    * 停止采样
    * @throws IOException
     */
   public void stopSamplingProfiler() throws IOException {
      HandleProfiling.sendSPSE(this);
   }

   public boolean startOpenGlTracing() {
      boolean canTraceOpenGl = this.mClientData.hasFeature("opengl-tracing");
      if(!canTraceOpenGl) {
         return false;
      } else {
         try {
            HandleViewDebug.sendStartGlTracing(this);
            return true;
         } catch (IOException var3) {
            Log.w("ddms", "Start OpenGL Tracing failed");
            return false;
         }
      }
   }

   public boolean stopOpenGlTracing() {
      boolean canTraceOpenGl = this.mClientData.hasFeature("opengl-tracing");
      if(!canTraceOpenGl) {
         return false;
      } else {
         try {
            HandleViewDebug.sendStopGlTracing(this);
            return true;
         } catch (IOException var3) {
            Log.w("ddms", "Stop OpenGL Tracing failed");
            return false;
         }
      }
   }

   public void requestMethodProfilingStatus() {
      try {
         HandleHeap.sendREAQ(this);
      } catch (IOException var2) {
         Log.e("ddmlib", (Throwable)var2);
      }

   }

   public void setThreadUpdateEnabled(boolean enabled) {
      this.mThreadUpdateEnabled = enabled;
      if(!enabled) {
         this.mClientData.clearThreads();
      }

      try {
         HandleThread.sendTHEN(this, enabled);
      } catch (IOException var3) {
         var3.printStackTrace();
      }

      this.update(8);
   }

   public boolean isThreadUpdateEnabled() {
      return this.mThreadUpdateEnabled;
   }

   public void requestThreadUpdate() {
      HandleThread.requestThreadUpdate(this);
   }

   public void requestThreadStackTrace(int threadId) {
      HandleThread.requestThreadStackCallRefresh(this, threadId);
   }

   public void setHeapUpdateEnabled(boolean enabled) {
      this.setHeapInfoUpdateEnabled(enabled);
      this.setHeapSegmentUpdateEnabled(enabled);
   }

   public void setHeapInfoUpdateEnabled(boolean enabled) {
      this.mHeapInfoUpdateEnabled = enabled;

      try {
         HandleHeap.sendHPIF(this, enabled?3:0);
      } catch (IOException var3) {
         ;
      }

      this.update(32);
   }

   public void setHeapSegmentUpdateEnabled(boolean enabled) {
      this.mHeapSegmentUpdateEnabled = enabled;

      try {
         HandleHeap.sendHPSG(this, enabled?1:0, 0);
      } catch (IOException var3) {
         ;
      }

      this.update(32);
   }

   void initializeHeapUpdateStatus() throws IOException {
      this.setHeapInfoUpdateEnabled(this.mHeapInfoUpdateEnabled);
   }

   public void updateHeapInfo() {
      try {
         HandleHeap.sendHPIF(this, 1);
      } catch (IOException var2) {
         ;
      }

   }

   public boolean isHeapUpdateEnabled() {
      return this.mHeapInfoUpdateEnabled || this.mHeapSegmentUpdateEnabled;
   }

   public boolean requestNativeHeapInformation() {
      try {
         HandleNativeHeap.sendNHGT(this);
         return true;
      } catch (IOException var2) {
         Log.e("ddmlib", (Throwable)var2);
         return false;
      }
   }

   public void enableAllocationTracker(boolean enable) {
      try {
         HandleHeap.sendREAE(this, enable);
      } catch (IOException var3) {
         Log.e("ddmlib", (Throwable)var3);
      }

   }

   public void requestAllocationStatus() {
      try {
         HandleHeap.sendREAQ(this);
      } catch (IOException var2) {
         Log.e("ddmlib", (Throwable)var2);
      }

   }

   /**
    * 请求分配细节
    */
   public void requestAllocationDetails() {
      try {
         HandleHeap.sendREAL(this);
      } catch (IOException var2) {
         Log.e("ddmlib", (Throwable)var2);
      }

   }

   public void kill() {
      try {
         HandleExit.sendEXIT(this, 1);
      } catch (IOException var2) {
         Log.w("ddms", "Send of EXIT message failed");
      }

   }

   void register(Selector sel) throws IOException {
      if(this.mChan != null) {
         this.mChan.register(sel, 1, this);
      }

   }

   public void setAsSelectedClient() {
      MonitorThread monitorThread = MonitorThread.getInstance();
      if(monitorThread != null) {
         monitorThread.setSelectedClient(this);
      }

   }

   public boolean isSelectedClient() {
      MonitorThread monitorThread = MonitorThread.getInstance();
      return monitorThread != null?monitorThread.getSelectedClient() == this:false;
   }

   void listenForDebugger(int listenPort) throws IOException {
      this.mDebuggerListenPort = listenPort;
      this.mDebugger = new Debugger(this, listenPort);
   }

   boolean sendHandshake() {
      assert this.mWriteBuffer.position() == 0;

      try {
         JdwpPacket.putHandshake(this.mWriteBuffer);
         int ioe = this.mWriteBuffer.position();
         this.mWriteBuffer.flip();
         if(this.mChan.write(this.mWriteBuffer) != ioe) {
            throw new IOException("partial handshake write");
         }
      } catch (IOException var6) {
         Log.e("ddms-client", "IO error during handshake: " + var6.getMessage());
         this.mConnState = 20;
         this.close(true);
         boolean var2 = false;
         return var2;
      } finally {
         this.mWriteBuffer.clear();
      }

      this.mConnState = 10;
      return true;
   }

   void sendAndConsume(JdwpPacket packet) throws IOException {
      this.sendAndConsume(packet, (ChunkHandler)null);
   }

   void sendAndConsume(JdwpPacket packet, ChunkHandler replyHandler) throws IOException {
      SocketChannel chan = this.mChan;
      if(chan == null) {
         Log.v("ddms", "Not sending packet -- client is closed");
      } else {
         if(replyHandler != null) {
            this.addRequestId(packet.getId(), replyHandler);
         }

         synchronized(chan) {
            try {
               packet.writeAndConsume(chan);
            } catch (IOException var7) {
               this.removeRequestId(packet.getId());
               throw var7;
            }

         }
      }
   }

   void forwardPacketToDebugger(JdwpPacket packet) throws IOException {
      Debugger dbg = this.mDebugger;
      if(dbg == null) {
         Log.d("ddms", "Discarding packet");
         packet.consume();
      } else {
         dbg.sendAndConsume(packet);
      }

   }

   void read() throws IOException, BufferOverflowException {
      if(this.mReadBuffer.position() == this.mReadBuffer.capacity()) {
         if(this.mReadBuffer.capacity() * 2 > 838860800) {
            Log.e("ddms", "Exceeded MAX_BUF_SIZE!");
            throw new BufferOverflowException();
         }

         Log.d("ddms", "Expanding read buffer to " + this.mReadBuffer.capacity() * 2);
         ByteBuffer newBuffer = ByteBuffer.allocate(this.mReadBuffer.capacity() * 2);
         this.mReadBuffer.position(0);
         newBuffer.put(this.mReadBuffer);
         this.mReadBuffer = newBuffer;
      }

      int count = this.mChan.read(this.mReadBuffer);
      if(count < 0) {
         throw new IOException("read failed");
      } else {
         Log.v("ddms", "Read " + count + " bytes from " + this);
      }
   }

   JdwpPacket getJdwpPacket() throws IOException {
      if(this.mConnState == 10) {
         int result = JdwpPacket.findHandshake(this.mReadBuffer);
         switch(result) {
         case 1:
            Log.d("ddms", "Good handshake from client, sending HELO to " + this.mClientData.getPid());
            JdwpPacket.consumeHandshake(this.mReadBuffer);
            this.mConnState = 11;
            HandleHello.sendHelloCommands(this, 1);
            return this.getJdwpPacket();
         case 2:
            Log.d("ddms", "No handshake from client yet.");
            break;
         case 3:
            Log.d("ddms", "Bad handshake from client");
            if(MonitorThread.getInstance().getRetryOnBadHandshake()) {
               this.mDevice.getMonitor().addClientToDropAndReopen(this, -1);
            } else {
               this.mConnState = 2;
               this.close(true);
            }
            break;
         default:
            Log.e("ddms", "Unknown packet while waiting for client handshake");
         }

         return null;
      } else if(this.mConnState != 11 && this.mConnState != 12 && this.mConnState != 13) {
         Log.e("ddms", "Receiving data in state = " + this.mConnState);
         return null;
      } else {
         if(this.mReadBuffer.position() != 0) {
            Log.v("ddms", "Checking " + this.mReadBuffer.position() + " bytes");
         }

         return JdwpPacket.findPacket(this.mReadBuffer);
      }
   }

   private void addRequestId(int id, ChunkHandler handler) {
      HashMap var3 = this.mOutstandingReqs;
      synchronized(this.mOutstandingReqs) {
         Log.v("ddms", "Adding req 0x" + Integer.toHexString(id) + " to set");
         this.mOutstandingReqs.put(Integer.valueOf(id), handler);
      }
   }

   void removeRequestId(int id) {
      HashMap var2 = this.mOutstandingReqs;
      synchronized(this.mOutstandingReqs) {
         Log.v("ddms", "Removing req 0x" + Integer.toHexString(id) + " from set");
         this.mOutstandingReqs.remove(Integer.valueOf(id));
      }
   }

   ChunkHandler isResponseToUs(int id) {
      HashMap var2 = this.mOutstandingReqs;
      synchronized(this.mOutstandingReqs) {
         ChunkHandler handler = (ChunkHandler)this.mOutstandingReqs.get(Integer.valueOf(id));
         if(handler != null) {
            Log.v("ddms", "Found 0x" + Integer.toHexString(id) + " in request set - " + handler);
            return handler;
         } else {
            return null;
         }
      }
   }

   void packetFailed(JdwpPacket reply) {
      if(this.mConnState == 11) {
         Log.d("ddms", "Marking " + this + " as non-DDM client");
         this.mConnState = 12;
      } else if(this.mConnState != 12) {
         Log.w("ddms", "WEIRD: got JDWP failure packet on DDM req");
      }

   }

   synchronized boolean ddmSeen() {
      if(this.mConnState == 11) {
         this.mConnState = 13;
         return false;
      } else {
         if(this.mConnState != 13) {
            Log.w("ddms", "WEIRD: in ddmSeen with state=" + this.mConnState);
         }

         return true;
      }
   }

   void close(boolean notify) {
      Log.d("ddms", "Closing " + this.toString());
      this.mOutstandingReqs.clear();

      try {
         if(this.mChan != null) {
            this.mChan.close();
            this.mChan = null;
         }

         if(this.mDebugger != null) {
            this.mDebugger.close();
            this.mDebugger = null;
         }
      } catch (IOException var3) {
         Log.w("ddms", "failed to close " + this);
      }

      this.mDevice.removeClient(this, notify);
   }

   public boolean isValid() {
      return this.mChan != null;
   }

   void update(int changeMask) {
      this.mDevice.update(this, changeMask);
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] $SwitchMap$com$android$ddmlib$ClientData$MethodProfilingStatus = new int[ClientData.MethodProfilingStatus.values().length];

      static {
         try {
            $SwitchMap$com$android$ddmlib$ClientData$MethodProfilingStatus[ClientData.MethodProfilingStatus.TRACER_ON.ordinal()] = 1;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            $SwitchMap$com$android$ddmlib$ClientData$MethodProfilingStatus[ClientData.MethodProfilingStatus.SAMPLER_ON.ordinal()] = 2;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            $SwitchMap$com$android$ddmlib$ClientData$MethodProfilingStatus[ClientData.MethodProfilingStatus.OFF.ordinal()] = 3;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }
}
