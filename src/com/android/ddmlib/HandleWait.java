package com.android.ddmlib;

import com.android.ddmlib.ChunkHandler;
import com.android.ddmlib.Client;
import com.android.ddmlib.ClientData;
import com.android.ddmlib.Log;
import com.android.ddmlib.MonitorThread;
import java.io.IOException;
import java.nio.ByteBuffer;

final class HandleWait extends ChunkHandler {
   public static final int CHUNK_WAIT = ChunkHandler.type("WAIT");
   private static final HandleWait mInst = new HandleWait();

   public static void register(MonitorThread mt) {
      mt.registerChunkHandler(CHUNK_WAIT, mInst);
   }

   public void clientReady(Client client) throws IOException {
   }

   public void clientDisconnected(Client client) {
   }

   public void handleChunk(Client client, int type, ByteBuffer data, boolean isReply, int msgId) {
      Log.d("ddm-wait", "handling " + ChunkHandler.name(type));
      if(type == CHUNK_WAIT) {
         assert !isReply;

         handleWAIT(client, data);
      } else {
         this.handleUnknownChunk(client, type, data, isReply, msgId);
      }

   }

   private static void handleWAIT(Client client, ByteBuffer data) {
      byte reason = data.get();
      Log.d("ddm-wait", "WAIT: reason=" + reason);
      ClientData cd = client.getClientData();
      synchronized(cd) {
         cd.setDebuggerConnectionStatus(ClientData.DebuggerStatus.WAITING);
      }

      client.update(2);
   }
}
