package com.android.ddmlib;

import com.android.ddmlib.DdmPreferences;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public final class Log {
   private static Log.LogLevel sLevel = DdmPreferences.getLogLevel();
   private static Log.ILogOutput sLogOutput;
   private static final char[] mSpaceLine = new char[72];
   private static final char[] mHexDigit = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

   public static void v(String tag, String message) {
      println(Log.LogLevel.VERBOSE, tag, message);
   }

   public static void d(String tag, String message) {
      println(Log.LogLevel.DEBUG, tag, message);
   }

   public static void i(String tag, String message) {
      println(Log.LogLevel.INFO, tag, message);
   }

   public static void w(String tag, String message) {
      println(Log.LogLevel.WARN, tag, message);
   }

   public static void e(String tag, String message) {
      println(Log.LogLevel.ERROR, tag, message);
   }

   public static void logAndDisplay(Log.LogLevel logLevel, String tag, String message) {
      if(sLogOutput != null) {
         sLogOutput.printAndPromptLog(logLevel, tag, message);
      } else {
         println(logLevel, tag, message);
      }

   }

   public static void e(String tag, Throwable throwable) {
      if(throwable != null) {
         StringWriter sw = new StringWriter();
         PrintWriter pw = new PrintWriter(sw);
         throwable.printStackTrace(pw);
         println(Log.LogLevel.ERROR, tag, throwable.getMessage() + '\n' + sw.toString());
      }

   }

   static void setLevel(Log.LogLevel logLevel) {
      sLevel = logLevel;
   }

   public static void setLogOutput(Log.ILogOutput logOutput) {
      sLogOutput = logOutput;
   }

   static void hexDump(String tag, Log.LogLevel level, byte[] data, int offset, int length) {
      byte kHexOffset = 6;
      byte kAscOffset = 55;
      char[] line = new char[mSpaceLine.length];
      boolean needErase = true;

      int count;
      for(int baseAddr = 0; length != 0; baseAddr += count) {
         if(length > 16) {
            count = 16;
         } else {
            count = length;
            needErase = true;
         }

         if(needErase) {
            System.arraycopy(mSpaceLine, 0, line, 0, mSpaceLine.length);
            needErase = false;
         }

         int addr = baseAddr & '\uffff';

         int ch;
         for(ch = 3; addr != 0; addr >>>= 4) {
            line[ch] = mHexDigit[addr & 15];
            --ch;
         }

         ch = kHexOffset;

         for(int i = 0; i < count; ++i) {
            byte val = data[offset + i];
            line[ch++] = mHexDigit[val >>> 4 & 15];
            line[ch++] = mHexDigit[val & 15];
            ++ch;
            if(val >= 32 && val < 127) {
               line[kAscOffset + i] = (char)val;
            } else {
               line[kAscOffset + i] = 46;
            }
         }

         println(level, tag, new String(line));
         length -= count;
         offset += count;
      }

   }

   static void hexDump(byte[] data) {
      hexDump("ddms", Log.LogLevel.DEBUG, data, 0, data.length);
   }

   private static void println(Log.LogLevel logLevel, String tag, String message) {
      if(logLevel.getPriority() >= sLevel.getPriority()) {
         if(sLogOutput != null) {
            sLogOutput.printLog(logLevel, tag, message);
         } else {
            printLog(logLevel, tag, message);
         }
      }

   }

   public static void printLog(Log.LogLevel logLevel, String tag, String message) {
      System.out.print(getLogFormatString(logLevel, tag, message));
   }

   public static String getLogFormatString(Log.LogLevel logLevel, String tag, String message) {
      SimpleDateFormat formatter = new SimpleDateFormat("hh:mm:ss", Locale.getDefault());
      return String.format("%s %c/%s: %s\n", new Object[]{formatter.format(new Date()), Character.valueOf(logLevel.getPriorityLetter()), tag, message});
   }

   static {
      for(int i = mSpaceLine.length - 1; i >= 0; mSpaceLine[i--] = 32) {
         ;
      }

      mSpaceLine[0] = mSpaceLine[1] = mSpaceLine[2] = mSpaceLine[3] = 48;
      mSpaceLine[4] = 45;
   }

   static final class Config {
      static final boolean LOGV = true;
      static final boolean LOGD = true;
   }

   public interface ILogOutput {
      void printLog(Log.LogLevel var1, String var2, String var3);

      void printAndPromptLog(Log.LogLevel var1, String var2, String var3);
   }

   public static enum LogLevel {
      VERBOSE(2, "verbose", 'V'),
      DEBUG(3, "debug", 'D'),
      INFO(4, "info", 'I'),
      WARN(5, "warn", 'W'),
      ERROR(6, "error", 'E'),
      ASSERT(7, "assert", 'A');

      private int mPriorityLevel;
      private String mStringValue;
      private char mPriorityLetter;

      private LogLevel(int intPriority, String stringValue, char priorityChar) {
         this.mPriorityLevel = intPriority;
         this.mStringValue = stringValue;
         this.mPriorityLetter = priorityChar;
      }

      public static Log.LogLevel getByString(String value) {
         Log.LogLevel[] arr$ = values();
         int len$ = arr$.length;

         for(int i$ = 0; i$ < len$; ++i$) {
            Log.LogLevel mode = arr$[i$];
            if(mode.mStringValue.equals(value)) {
               return mode;
            }
         }

         return null;
      }

      public static Log.LogLevel getByLetter(char letter) {
         Log.LogLevel[] arr$ = values();
         int len$ = arr$.length;

         for(int i$ = 0; i$ < len$; ++i$) {
            Log.LogLevel mode = arr$[i$];
            if(mode.mPriorityLetter == letter) {
               return mode;
            }
         }

         return null;
      }

      public static Log.LogLevel getByLetterString(String letter) {
         return !letter.isEmpty()?getByLetter(letter.charAt(0)):null;
      }

      public char getPriorityLetter() {
         return this.mPriorityLetter;
      }

      public int getPriority() {
         return this.mPriorityLevel;
      }

      public String getStringValue() {
         return this.mStringValue;
      }
   }
}
