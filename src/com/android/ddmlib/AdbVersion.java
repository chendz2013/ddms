package com.android.ddmlib;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AdbVersion implements Comparable<AdbVersion> {
   public static final AdbVersion UNKNOWN = new AdbVersion(-1, -1, -1);
   private static final Pattern ADB_VERSION_PATTERN = Pattern.compile("^.*(\\d+)\\.(\\d+)\\.(\\d+).*");
   public final int major;
   public final int minor;
   public final int micro;

   private AdbVersion(int major, int minor, int micro) {
      this.major = major;
      this.minor = minor;
      this.micro = micro;
   }

   public String toString() {
      return String.format(Locale.US, "%1$d.%2$d.%3$d", new Object[]{Integer.valueOf(this.major), Integer.valueOf(this.minor), Integer.valueOf(this.micro)});
   }

   public int compareTo(AdbVersion o) {
      return this.major != o.major?this.major - o.major:(this.minor != o.minor?this.minor - o.minor:this.micro - o.micro);
   }

   public static AdbVersion parseFrom(String input) {
      Matcher matcher = ADB_VERSION_PATTERN.matcher(input);
      if(matcher.matches()) {
         int major = Integer.parseInt(matcher.group(1));
         int minor = Integer.parseInt(matcher.group(2));
         int micro = Integer.parseInt(matcher.group(3));
         return new AdbVersion(major, minor, micro);
      } else {
         return UNKNOWN;
      }
   }

   public boolean equals(Object o) {
      if(this == o) {
         return true;
      } else if(o != null && this.getClass() == o.getClass()) {
         AdbVersion version = (AdbVersion)o;
         return this.major != version.major?false:(this.minor != version.minor?false:this.micro == version.micro);
      } else {
         return false;
      }
   }

   public int hashCode() {
      int result = this.major;
      result = 31 * result + this.minor;
      result = 31 * result + this.micro;
      return result;
   }
}
