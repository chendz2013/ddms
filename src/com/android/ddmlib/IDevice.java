package com.android.ddmlib;

import com.android.ddmlib.AdbCommandRejectedException;
import com.android.ddmlib.Client;
import com.android.ddmlib.FileListingService;
import com.android.ddmlib.IShellEnabledDevice;
import com.android.ddmlib.IShellOutputReceiver;
import com.android.ddmlib.InstallException;
import com.android.ddmlib.RawImage;
import com.android.ddmlib.ScreenRecorderOptions;
import com.android.ddmlib.ShellCommandUnresponsiveException;
import com.android.ddmlib.SyncException;
import com.android.ddmlib.SyncService;
import com.android.ddmlib.TimeoutException;
import com.android.ddmlib.log.LogReceiver;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public interface IDevice extends IShellEnabledDevice {
   String PROP_BUILD_VERSION = "ro.build.version.release";
   String PROP_BUILD_API_LEVEL = "ro.build.version.sdk";
   String PROP_BUILD_CODENAME = "ro.build.version.codename";
   String PROP_BUILD_TAGS = "ro.build.tags";
   String PROP_BUILD_TYPE = "ro.build.type";
   String PROP_DEVICE_MODEL = "ro.product.model";
   String PROP_DEVICE_MANUFACTURER = "ro.product.manufacturer";
   String PROP_DEVICE_CPU_ABI_LIST = "ro.product.cpu.abilist";
   String PROP_DEVICE_CPU_ABI = "ro.product.cpu.abi";
   String PROP_DEVICE_CPU_ABI2 = "ro.product.cpu.abi2";
   String PROP_BUILD_CHARACTERISTICS = "ro.build.characteristics";
   String PROP_DEVICE_DENSITY = "ro.sf.lcd_density";
   String PROP_DEVICE_LANGUAGE = "persist.sys.language";
   String PROP_DEVICE_REGION = "persist.sys.country";
   String PROP_DEBUGGABLE = "ro.debuggable";
   String FIRST_EMULATOR_SN = "emulator-5554";
   int CHANGE_STATE = 1;
   int CHANGE_CLIENT_LIST = 2;
   int CHANGE_BUILD_INFO = 4;
   /** @deprecated */
   @Deprecated
   String PROP_BUILD_VERSION_NUMBER = "ro.build.version.sdk";
   String MNT_EXTERNAL_STORAGE = "EXTERNAL_STORAGE";
   String MNT_ROOT = "ANDROID_ROOT";
   String MNT_DATA = "ANDROID_DATA";

   String getSerialNumber();

   String getAvdName();

   IDevice.DeviceState getState();

   /** @deprecated */
   @Deprecated
   Map<String, String> getProperties();

   /** @deprecated */
   @Deprecated
   int getPropertyCount();

   String getProperty(String var1);

   boolean arePropertiesSet();

   /** @deprecated */
   @Deprecated
   String getPropertySync(String var1) throws TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException, IOException;

   /** @deprecated */
   @Deprecated
   String getPropertyCacheOrSync(String var1) throws TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException, IOException;

   boolean supportsFeature(IDevice.Feature var1);

   boolean supportsFeature(IDevice.HardwareFeature var1);

   String getMountPoint(String var1);

   boolean isOnline();

   boolean isEmulator();

   boolean isOffline();

   boolean isBootLoader();

   boolean hasClients();

   Client[] getClients();

   Client getClient(String var1);

   SyncService getSyncService() throws TimeoutException, AdbCommandRejectedException, IOException;

   FileListingService getFileListingService();

   RawImage getScreenshot() throws TimeoutException, AdbCommandRejectedException, IOException;

   RawImage getScreenshot(long var1, TimeUnit var3) throws TimeoutException, AdbCommandRejectedException, IOException;

   void startScreenRecorder(String var1, ScreenRecorderOptions var2, IShellOutputReceiver var3) throws TimeoutException, AdbCommandRejectedException, IOException, ShellCommandUnresponsiveException;

   /** @deprecated */
   @Deprecated
   void executeShellCommand(String var1, IShellOutputReceiver var2, int var3) throws TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException, IOException;

   void executeShellCommand(String var1, IShellOutputReceiver var2) throws TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException, IOException;

   void runEventLogService(LogReceiver var1) throws TimeoutException, AdbCommandRejectedException, IOException;

   void runLogService(String var1, LogReceiver var2) throws TimeoutException, AdbCommandRejectedException, IOException;

   void createForward(int var1, int var2) throws TimeoutException, AdbCommandRejectedException, IOException;

   void createForward(int var1, String var2, IDevice.DeviceUnixSocketNamespace var3) throws TimeoutException, AdbCommandRejectedException, IOException;

   void removeForward(int var1, int var2) throws TimeoutException, AdbCommandRejectedException, IOException;

   void removeForward(int var1, String var2, IDevice.DeviceUnixSocketNamespace var3) throws TimeoutException, AdbCommandRejectedException, IOException;

   String getClientName(int var1);

   void pushFile(String var1, String var2) throws IOException, AdbCommandRejectedException, TimeoutException, SyncException;

   void pullFile(String var1, String var2) throws IOException, AdbCommandRejectedException, TimeoutException, SyncException;

   String installPackage(String var1, boolean var2, String... var3) throws InstallException;

   void installPackages(List<String> var1, int var2, boolean var3, String... var4) throws InstallException;

   String syncPackageToDevice(String var1) throws TimeoutException, AdbCommandRejectedException, IOException, SyncException;

   String installRemotePackage(String var1, boolean var2, String... var3) throws InstallException;

   void removeRemotePackage(String var1) throws InstallException;

   String uninstallPackage(String var1) throws InstallException;

   void reboot(String var1) throws TimeoutException, AdbCommandRejectedException, IOException;

   /** @deprecated */
   @Deprecated
   Integer getBatteryLevel() throws TimeoutException, AdbCommandRejectedException, IOException, ShellCommandUnresponsiveException;

   /** @deprecated */
   @Deprecated
   Integer getBatteryLevel(long var1) throws TimeoutException, AdbCommandRejectedException, IOException, ShellCommandUnresponsiveException;

   Future<Integer> getBattery();

   Future<Integer> getBattery(long var1, TimeUnit var3);

   List<String> getAbis();

   int getDensity();

   String getLanguage();

   String getRegion();

   int getApiLevel();

   public static enum DeviceUnixSocketNamespace {
      ABSTRACT("localabstract"),
      FILESYSTEM("localfilesystem"),
      RESERVED("localreserved");

      private String mType;

      private DeviceUnixSocketNamespace(String type) {
         this.mType = type;
      }

      String getType() {
         return this.mType;
      }
   }

   public static enum DeviceState {
      BOOTLOADER("bootloader"),
      OFFLINE("offline"),
      ONLINE("device"),
      RECOVERY("recovery"),
      UNAUTHORIZED("unauthorized");

      private String mState;

      private DeviceState(String state) {
         this.mState = state;
      }

      public static IDevice.DeviceState getState(String state) {
         IDevice.DeviceState[] arr$ = values();
         int len$ = arr$.length;

         for(int i$ = 0; i$ < len$; ++i$) {
            IDevice.DeviceState deviceState = arr$[i$];
            if(deviceState.mState.equals(state)) {
               return deviceState;
            }
         }

         return null;
      }
   }

   public static enum HardwareFeature {
      WATCH("watch"),
      TV("tv");

      private final String mCharacteristic;

      private HardwareFeature(String characteristic) {
         this.mCharacteristic = characteristic;
      }

      public String getCharacteristic() {
         return this.mCharacteristic;
      }
   }

   public static enum Feature {
      SCREEN_RECORD,
      PROCSTATS;
   }
}
