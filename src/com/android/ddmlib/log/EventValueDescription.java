package com.android.ddmlib.log;

import com.android.ddmlib.log.EventContainer;
import com.android.ddmlib.log.InvalidValueTypeException;
import java.util.Locale;

public final class EventValueDescription {
   private String mName;
   private EventContainer.EventValueType mEventValueType;
   private EventValueDescription.ValueType mValueType;

   EventValueDescription(String name, EventContainer.EventValueType type) {
      this.mName = name;
      this.mEventValueType = type;
      if(this.mEventValueType != EventContainer.EventValueType.INT && this.mEventValueType != EventContainer.EventValueType.LONG) {
         this.mValueType = EventValueDescription.ValueType.NOT_APPLICABLE;
      } else {
         this.mValueType = EventValueDescription.ValueType.BYTES;
      }

   }

   EventValueDescription(String name, EventContainer.EventValueType type, EventValueDescription.ValueType valueType) throws InvalidValueTypeException {
      this.mName = name;
      this.mEventValueType = type;
      this.mValueType = valueType;
      this.mValueType.checkType(this.mEventValueType);
   }

   public String getName() {
      return this.mName;
   }

   public EventContainer.EventValueType getEventValueType() {
      return this.mEventValueType;
   }

   public EventValueDescription.ValueType getValueType() {
      return this.mValueType;
   }

   public String toString() {
      return this.mValueType != EventValueDescription.ValueType.NOT_APPLICABLE?String.format("%1$s (%2$s, %3$s)", new Object[]{this.mName, this.mEventValueType.toString(), this.mValueType.toString()}):String.format("%1$s (%2$s)", new Object[]{this.mName, this.mEventValueType.toString()});
   }

   public boolean checkForType(Object value) {
      switch(EventValueDescription.SyntheticClass_1.$SwitchMap$com$android$ddmlib$log$EventContainer$EventValueType[this.mEventValueType.ordinal()]) {
      case 1:
         return value instanceof Integer;
      case 2:
         return value instanceof Long;
      case 3:
         return value instanceof String;
      case 4:
         return value instanceof Object[];
      default:
         return false;
      }
   }

   public Object getObjectFromString(String value) {
      switch(EventValueDescription.SyntheticClass_1.$SwitchMap$com$android$ddmlib$log$EventContainer$EventValueType[this.mEventValueType.ordinal()]) {
      case 1:
         try {
            return Integer.valueOf(value);
         } catch (NumberFormatException var4) {
            return null;
         }
      case 2:
         try {
            return Long.valueOf(value);
         } catch (NumberFormatException var3) {
            return null;
         }
      case 3:
         return value;
      default:
         return null;
      }
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] $SwitchMap$com$android$ddmlib$log$EventContainer$EventValueType = new int[EventContainer.EventValueType.values().length];

      static {
         try {
            $SwitchMap$com$android$ddmlib$log$EventContainer$EventValueType[EventContainer.EventValueType.INT.ordinal()] = 1;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            $SwitchMap$com$android$ddmlib$log$EventContainer$EventValueType[EventContainer.EventValueType.LONG.ordinal()] = 2;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            $SwitchMap$com$android$ddmlib$log$EventContainer$EventValueType[EventContainer.EventValueType.STRING.ordinal()] = 3;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            $SwitchMap$com$android$ddmlib$log$EventContainer$EventValueType[EventContainer.EventValueType.LIST.ordinal()] = 4;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }

   public static enum ValueType {
      NOT_APPLICABLE(0),
      OBJECTS(1),
      BYTES(2),
      MILLISECONDS(3),
      ALLOCATIONS(4),
      ID(5),
      PERCENT(6);

      private int mValue;

      public void checkType(EventContainer.EventValueType type) throws InvalidValueTypeException {
         if(type != EventContainer.EventValueType.INT && type != EventContainer.EventValueType.LONG && this != NOT_APPLICABLE) {
            throw new InvalidValueTypeException(String.format("%1$s doesn\'t support type %2$s", new Object[]{type, this}));
         }
      }

      public static EventValueDescription.ValueType getValueType(int value) {
         EventValueDescription.ValueType[] arr$ = values();
         int len$ = arr$.length;

         for(int i$ = 0; i$ < len$; ++i$) {
            EventValueDescription.ValueType type = arr$[i$];
            if(type.mValue == value) {
               return type;
            }
         }

         return null;
      }

      public int getValue() {
         return this.mValue;
      }

      public String toString() {
         return super.toString().toLowerCase(Locale.US);
      }

      private ValueType(int value) {
         this.mValue = value;
      }
   }
}
