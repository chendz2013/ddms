package com.android.ddmlib.log;

import com.android.ddmlib.log.EventContainer;
import com.android.ddmlib.log.EventValueDescription;
import com.android.ddmlib.log.InvalidTypeException;
import com.android.ddmlib.log.InvalidValueTypeException;
import com.android.ddmlib.log.LogReceiver;

final class GcEventContainer extends EventContainer {
   public static final int GC_EVENT_TAG = 20001;
   private String processId;
   private long gcTime;
   private long bytesFreed;
   private long objectsFreed;
   private long actualSize;
   private long allowedSize;
   private long softLimit;
   private long objectsAllocated;
   private long bytesAllocated;
   private long zActualSize;
   private long zAllowedSize;
   private long zObjectsAllocated;
   private long zBytesAllocated;
   private long dlmallocFootprint;
   private long mallinfoTotalAllocatedSpace;
   private long externalLimit;
   private long externalBytesAllocated;

   GcEventContainer(LogReceiver.LogEntry entry, int tag, Object data) {
      super(entry, tag, data);
      this.init(data);
   }

   GcEventContainer(int tag, int pid, int tid, int sec, int nsec, Object data) {
      super(tag, pid, tid, sec, nsec, data);
      this.init(data);
   }

   private void init(Object data) {
      if(data instanceof Object[]) {
         Object[] values = (Object[])((Object[])data);

         for(int i = 0; i < values.length; ++i) {
            if(values[i] instanceof Long) {
               this.parseDvmHeapInfo(((Long)values[i]).longValue(), i);
            }
         }
      }

   }

   public EventContainer.EventValueType getType() {
      return EventContainer.EventValueType.LIST;
   }

   public boolean testValue(int index, Object value, EventContainer.CompareMethod compareMethod) throws InvalidTypeException {
      if(index == 0) {
         if(!(value instanceof String)) {
            throw new InvalidTypeException();
         }
      } else if(!(value instanceof Long)) {
         throw new InvalidTypeException();
      }

      switch(GcEventContainer.SyntheticClass_1.$SwitchMap$com$android$ddmlib$log$EventContainer$CompareMethod[compareMethod.ordinal()]) {
      case 1:
         if(index == 0) {
            return this.processId.equals(value);
         }

         return this.getValueAsLong(index) == ((Long)value).longValue();
      case 2:
         return this.getValueAsLong(index) <= ((Long)value).longValue();
      case 3:
         return this.getValueAsLong(index) < ((Long)value).longValue();
      case 4:
         return this.getValueAsLong(index) >= ((Long)value).longValue();
      case 5:
         return this.getValueAsLong(index) > ((Long)value).longValue();
      case 6:
         return (this.getValueAsLong(index) & ((Long)value).longValue()) != 0L;
      default:
         throw new ArrayIndexOutOfBoundsException();
      }
   }

   public Object getValue(int valueIndex) {
      if(valueIndex == 0) {
         return this.processId;
      } else {
         try {
            return Long.valueOf(this.getValueAsLong(valueIndex));
         } catch (InvalidTypeException var3) {
            return null;
         }
      }
   }

   public double getValueAsDouble(int valueIndex) throws InvalidTypeException {
      return (double)this.getValueAsLong(valueIndex);
   }

   public String getValueAsString(int valueIndex) {
      switch(valueIndex) {
      case 0:
         return this.processId;
      default:
         try {
            return Long.toString(this.getValueAsLong(valueIndex));
         } catch (InvalidTypeException var3) {
            throw new ArrayIndexOutOfBoundsException();
         }
      }
   }

   static EventValueDescription[] getValueDescriptions() {
      try {
         return new EventValueDescription[]{new EventValueDescription("Process Name", EventContainer.EventValueType.STRING), new EventValueDescription("GC Time", EventContainer.EventValueType.LONG, EventValueDescription.ValueType.MILLISECONDS), new EventValueDescription("Freed Objects", EventContainer.EventValueType.LONG, EventValueDescription.ValueType.OBJECTS), new EventValueDescription("Freed Bytes", EventContainer.EventValueType.LONG, EventValueDescription.ValueType.BYTES), new EventValueDescription("Soft Limit", EventContainer.EventValueType.LONG, EventValueDescription.ValueType.BYTES), new EventValueDescription("Actual Size (aggregate)", EventContainer.EventValueType.LONG, EventValueDescription.ValueType.BYTES), new EventValueDescription("Allowed Size (aggregate)", EventContainer.EventValueType.LONG, EventValueDescription.ValueType.BYTES), new EventValueDescription("Allocated Objects (aggregate)", EventContainer.EventValueType.LONG, EventValueDescription.ValueType.OBJECTS), new EventValueDescription("Allocated Bytes (aggregate)", EventContainer.EventValueType.LONG, EventValueDescription.ValueType.BYTES), new EventValueDescription("Actual Size", EventContainer.EventValueType.LONG, EventValueDescription.ValueType.BYTES), new EventValueDescription("Allowed Size", EventContainer.EventValueType.LONG, EventValueDescription.ValueType.BYTES), new EventValueDescription("Allocated Objects", EventContainer.EventValueType.LONG, EventValueDescription.ValueType.OBJECTS), new EventValueDescription("Allocated Bytes", EventContainer.EventValueType.LONG, EventValueDescription.ValueType.BYTES), new EventValueDescription("Actual Size (zygote)", EventContainer.EventValueType.LONG, EventValueDescription.ValueType.BYTES), new EventValueDescription("Allowed Size (zygote)", EventContainer.EventValueType.LONG, EventValueDescription.ValueType.BYTES), new EventValueDescription("Allocated Objects (zygote)", EventContainer.EventValueType.LONG, EventValueDescription.ValueType.OBJECTS), new EventValueDescription("Allocated Bytes (zygote)", EventContainer.EventValueType.LONG, EventValueDescription.ValueType.BYTES), new EventValueDescription("External Allocation Limit", EventContainer.EventValueType.LONG, EventValueDescription.ValueType.BYTES), new EventValueDescription("External Bytes Allocated", EventContainer.EventValueType.LONG, EventValueDescription.ValueType.BYTES), new EventValueDescription("dlmalloc Footprint", EventContainer.EventValueType.LONG, EventValueDescription.ValueType.BYTES), new EventValueDescription("Malloc Info: Total Allocated Space", EventContainer.EventValueType.LONG, EventValueDescription.ValueType.BYTES)};
      } catch (InvalidValueTypeException var1) {
         assert false;

         return null;
      }
   }

   private void parseDvmHeapInfo(long data, int index) {
      switch(index) {
      case 0:
         this.gcTime = float12ToInt((int)(data >> 12 & 4095L));
         this.bytesFreed = float12ToInt((int)(data & 4095L));
         byte[] dataArray = new byte[8];
         put64bitsToArray(data, dataArray, 0);
         this.processId = new String(dataArray, 0, 5);
         break;
      case 1:
         this.objectsFreed = float12ToInt((int)(data >> 48 & 4095L));
         this.actualSize = float12ToInt((int)(data >> 36 & 4095L));
         this.allowedSize = float12ToInt((int)(data >> 24 & 4095L));
         this.objectsAllocated = float12ToInt((int)(data >> 12 & 4095L));
         this.bytesAllocated = float12ToInt((int)(data & 4095L));
         break;
      case 2:
         this.softLimit = float12ToInt((int)(data >> 48 & 4095L));
         this.zActualSize = float12ToInt((int)(data >> 36 & 4095L));
         this.zAllowedSize = float12ToInt((int)(data >> 24 & 4095L));
         this.zObjectsAllocated = float12ToInt((int)(data >> 12 & 4095L));
         this.zBytesAllocated = float12ToInt((int)(data & 4095L));
         break;
      case 3:
         this.dlmallocFootprint = float12ToInt((int)(data >> 36 & 4095L));
         this.mallinfoTotalAllocatedSpace = float12ToInt((int)(data >> 24 & 4095L));
         this.externalLimit = float12ToInt((int)(data >> 12 & 4095L));
         this.externalBytesAllocated = float12ToInt((int)(data & 4095L));
      }

   }

   private static long float12ToInt(int f12) {
      return (long)((f12 & 511) << (f12 >>> 9) * 4);
   }

   private static void put64bitsToArray(long value, byte[] dest, int offset) {
      dest[offset + 7] = (byte)((int)(value & 255L));
      dest[offset + 6] = (byte)((int)((value & 65280L) >> 8));
      dest[offset + 5] = (byte)((int)((value & 16711680L) >> 16));
      dest[offset + 4] = (byte)((int)((value & 4278190080L) >> 24));
      dest[offset + 3] = (byte)((int)((value & 1095216660480L) >> 32));
      dest[offset + 2] = (byte)((int)((value & 280375465082880L) >> 40));
      dest[offset + 1] = (byte)((int)((value & 71776119061217280L) >> 48));
      dest[offset + 0] = (byte)((int)((value & -72057594037927936L) >> 56));
   }

   private long getValueAsLong(int valueIndex) throws InvalidTypeException {
      switch(valueIndex) {
      case 0:
         throw new InvalidTypeException();
      case 1:
         return this.gcTime;
      case 2:
         return this.objectsFreed;
      case 3:
         return this.bytesFreed;
      case 4:
         return this.softLimit;
      case 5:
         return this.actualSize;
      case 6:
         return this.allowedSize;
      case 7:
         return this.objectsAllocated;
      case 8:
         return this.bytesAllocated;
      case 9:
         return this.actualSize - this.zActualSize;
      case 10:
         return this.allowedSize - this.zAllowedSize;
      case 11:
         return this.objectsAllocated - this.zObjectsAllocated;
      case 12:
         return this.bytesAllocated - this.zBytesAllocated;
      case 13:
         return this.zActualSize;
      case 14:
         return this.zAllowedSize;
      case 15:
         return this.zObjectsAllocated;
      case 16:
         return this.zBytesAllocated;
      case 17:
         return this.externalLimit;
      case 18:
         return this.externalBytesAllocated;
      case 19:
         return this.dlmallocFootprint;
      case 20:
         return this.mallinfoTotalAllocatedSpace;
      default:
         throw new ArrayIndexOutOfBoundsException();
      }
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] $SwitchMap$com$android$ddmlib$log$EventContainer$CompareMethod = new int[EventContainer.CompareMethod.values().length];

      static {
         try {
            $SwitchMap$com$android$ddmlib$log$EventContainer$CompareMethod[EventContainer.CompareMethod.EQUAL_TO.ordinal()] = 1;
         } catch (NoSuchFieldError var6) {
            ;
         }

         try {
            $SwitchMap$com$android$ddmlib$log$EventContainer$CompareMethod[EventContainer.CompareMethod.LESSER_THAN.ordinal()] = 2;
         } catch (NoSuchFieldError var5) {
            ;
         }

         try {
            $SwitchMap$com$android$ddmlib$log$EventContainer$CompareMethod[EventContainer.CompareMethod.LESSER_THAN_STRICT.ordinal()] = 3;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            $SwitchMap$com$android$ddmlib$log$EventContainer$CompareMethod[EventContainer.CompareMethod.GREATER_THAN.ordinal()] = 4;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            $SwitchMap$com$android$ddmlib$log$EventContainer$CompareMethod[EventContainer.CompareMethod.GREATER_THAN_STRICT.ordinal()] = 5;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            $SwitchMap$com$android$ddmlib$log$EventContainer$CompareMethod[EventContainer.CompareMethod.BIT_CHECK.ordinal()] = 6;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }
}
