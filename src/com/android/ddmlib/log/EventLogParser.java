package com.android.ddmlib.log;

import com.android.ddmlib.IDevice;
import com.android.ddmlib.Log;
import com.android.ddmlib.MultiLineReceiver;
import com.android.ddmlib.log.EventContainer;
import com.android.ddmlib.log.EventValueDescription;
import com.android.ddmlib.log.GcEventContainer;
import com.android.ddmlib.log.InvalidValueTypeException;
import com.android.ddmlib.log.LogReceiver;
import com.android.ddmlib.utils.ArrayHelper;
import com.google.common.base.Charsets;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class EventLogParser {
   private static final String EVENT_TAG_MAP_FILE = "/system/etc/event-log-tags";
   private static final int EVENT_TYPE_INT = 0;
   private static final int EVENT_TYPE_LONG = 1;
   private static final int EVENT_TYPE_STRING = 2;
   private static final int EVENT_TYPE_LIST = 3;
   private static final Pattern PATTERN_SIMPLE_TAG = Pattern.compile("^(\\d+)\\s+([A-Za-z0-9_]+)\\s*$");
   private static final Pattern PATTERN_TAG_WITH_DESC = Pattern.compile("^(\\d+)\\s+([A-Za-z0-9_]+)\\s*(.*)\\s*$");
   private static final Pattern PATTERN_DESCRIPTION = Pattern.compile("\\(([A-Za-z0-9_\\s]+)\\|(\\d+)(\\|\\d+){0,1}\\)");
   private static final Pattern TEXT_LOG_LINE = Pattern.compile("(\\d\\d)-(\\d\\d)\\s(\\d\\d):(\\d\\d):(\\d\\d).(\\d{3})\\s+I/([a-zA-Z0-9_]+)\\s*\\(\\s*(\\d+)\\):\\s+(.*)");
   private final TreeMap<Integer, String> mTagMap = new TreeMap();
   private final TreeMap<Integer, EventValueDescription[]> mValueDescriptionMap = new TreeMap();

   public boolean init(IDevice device) {
      try {
         device.executeShellCommand("cat /system/etc/event-log-tags", new MultiLineReceiver() {
            public void processNewLines(String[] lines) {
               String[] arr$ = lines;
               int len$ = lines.length;

               for(int i$ = 0; i$ < len$; ++i$) {
                  String line = arr$[i$];
                  EventLogParser.this.processTagLine(line);
               }

            }

            public boolean isCancelled() {
               return false;
            }
         });
         return true;
      } catch (Exception var3) {
         return false;
      }
   }

   public boolean init(String[] tagFileContent) {
      String[] arr$ = tagFileContent;
      int len$ = tagFileContent.length;

      for(int i$ = 0; i$ < len$; ++i$) {
         String line = arr$[i$];
         this.processTagLine(line);
      }

      return true;
   }

   public boolean init(String filePath) {
      BufferedReader reader = null;

      boolean var4;
      try {
         reader = new BufferedReader(new FileReader(filePath));
         String e = null;

         do {
            e = reader.readLine();
            if(e != null) {
               this.processTagLine(e);
            }
         } while(e != null);

         var4 = true;
         return var4;
      } catch (IOException var14) {
         var4 = false;
      } finally {
         try {
            if(reader != null) {
               reader.close();
            }
         } catch (IOException var13) {
            ;
         }

      }

      return var4;
   }

   private void processTagLine(String line) {
      if(!line.isEmpty() && line.charAt(0) != 35) {
         Matcher m = PATTERN_TAG_WITH_DESC.matcher(line);
         int value;
         String name;
         if(m.matches()) {
            try {
               value = Integer.parseInt(m.group(1));
               name = m.group(2);
               if(name != null && this.mTagMap.get(Integer.valueOf(value)) == null) {
                  this.mTagMap.put(Integer.valueOf(value), name);
               }

               if(value == 20001) {
                  this.mValueDescriptionMap.put(Integer.valueOf(value), GcEventContainer.getValueDescriptions());
               } else {
                  String description = m.group(3);
                  if(description != null && !description.isEmpty()) {
                     EventValueDescription[] desc = this.processDescription(description);
                     if(desc != null) {
                        this.mValueDescriptionMap.put(Integer.valueOf(value), desc);
                     }
                  }
               }
            } catch (NumberFormatException var7) {
               ;
            }
         } else {
            m = PATTERN_SIMPLE_TAG.matcher(line);
            if(m.matches()) {
               value = Integer.parseInt(m.group(1));
               name = m.group(2);
               if(name != null && this.mTagMap.get(Integer.valueOf(value)) == null) {
                  this.mTagMap.put(Integer.valueOf(value), name);
               }
            }
         }
      }

   }

   private EventValueDescription[] processDescription(String description) {
      String[] descriptions = description.split("\\s*,\\s*");
      ArrayList list = new ArrayList();
      String[] arr$ = descriptions;
      int len$ = descriptions.length;

      for(int i$ = 0; i$ < len$; ++i$) {
         String desc = arr$[i$];
         Matcher m = PATTERN_DESCRIPTION.matcher(desc);
         if(m.matches()) {
            try {
               String e = m.group(1);
               String typeString = m.group(2);
               int typeValue = Integer.parseInt(typeString);
               EventContainer.EventValueType eventValueType = EventContainer.EventValueType.getEventValueType(typeValue);
               if(eventValueType == null) {
                  ;
               }

               typeString = m.group(3);
               if(typeString != null && !typeString.isEmpty()) {
                  typeString = typeString.substring(1);
                  typeValue = Integer.parseInt(typeString);
                  EventValueDescription.ValueType valueType = EventValueDescription.ValueType.getValueType(typeValue);
                  list.add(new EventValueDescription(e, eventValueType, valueType));
               } else {
                  list.add(new EventValueDescription(e, eventValueType));
               }
            } catch (NumberFormatException var14) {
               ;
            } catch (InvalidValueTypeException var15) {
               ;
            }
         } else {
            Log.e("EventLogParser", String.format("Can\'t parse %1$s", new Object[]{description}));
         }
      }

      if(list.isEmpty()) {
         return null;
      } else {
         return (EventValueDescription[])list.toArray(new EventValueDescription[list.size()]);
      }
   }

   public EventContainer parse(LogReceiver.LogEntry entry) {
      if(entry.len < 4) {
         return null;
      } else {
         byte inOffset = 0;
         int tagValue = ArrayHelper.swap32bitFromArray(entry.data, inOffset);
         int inOffset1 = inOffset + 4;
         String tag = (String)this.mTagMap.get(Integer.valueOf(tagValue));
         if(tag == null) {
            Log.e("EventLogParser", String.format("unknown tag number: %1$d", new Object[]{Integer.valueOf(tagValue)}));
         }

         ArrayList list = new ArrayList();
         if(parseBinaryEvent(entry.data, inOffset1, list) == -1) {
            return null;
         } else {
            Object data;
            if(list.size() == 1) {
               data = list.get(0);
            } else {
               data = list.toArray();
            }

            Object event = null;
            if(tagValue == 20001) {
               event = new GcEventContainer(entry, tagValue, data);
            } else {
               event = new EventContainer(entry, tagValue, data);
            }

            return (EventContainer)event;
         }
      }
   }

   public EventContainer parse(String textLogLine) {
      if(textLogLine.isEmpty()) {
         return null;
      } else {
         Matcher m = TEXT_LOG_LINE.matcher(textLogLine);
         if(m.matches()) {
            try {
               int e = Integer.parseInt(m.group(1));
               int day = Integer.parseInt(m.group(2));
               int hours = Integer.parseInt(m.group(3));
               int minutes = Integer.parseInt(m.group(4));
               int seconds = Integer.parseInt(m.group(5));
               int milliseconds = Integer.parseInt(m.group(6));
               Calendar cal = Calendar.getInstance();
               cal.set(cal.get(1), e - 1, day, hours, minutes, seconds);
               int sec = (int)Math.floor((double)(cal.getTimeInMillis() / 1000L));
               int nsec = milliseconds * 1000000;
               String tag = m.group(7);
               int tagValue = -1;
               Set tagSet = this.mTagMap.entrySet();
               Iterator pid = tagSet.iterator();

               while(pid.hasNext()) {
                  Entry data = (Entry)pid.next();
                  if(tag.equals(data.getValue())) {
                     tagValue = ((Integer)data.getKey()).intValue();
                     break;
                  }
               }

               if(tagValue == -1) {
                  return null;
               } else {
                  int pid1 = Integer.parseInt(m.group(8));
                  Object data1 = this.parseTextData(m.group(9), tagValue);
                  if(data1 == null) {
                     return null;
                  } else {
                     Object event = null;
                     if(tagValue == 20001) {
                        event = new GcEventContainer(tagValue, pid1, -1, sec, nsec, data1);
                     } else {
                        event = new EventContainer(tagValue, pid1, -1, sec, nsec, data1);
                     }

                     return (EventContainer)event;
                  }
               }
            } catch (NumberFormatException var18) {
               return null;
            }
         } else {
            return null;
         }
      }
   }

   public Map<Integer, String> getTagMap() {
      return this.mTagMap;
   }

   public Map<Integer, EventValueDescription[]> getEventInfoMap() {
      return this.mValueDescriptionMap;
   }

   private static int parseBinaryEvent(byte[] eventData, int dataOffset, ArrayList<Object> list) {
      if(eventData.length - dataOffset < 1) {
         return -1;
      } else {
         int offset = dataOffset + 1;
         byte type = eventData[dataOffset];
         int var9;
         switch(type) {
         case 0:
            if(eventData.length - offset < 4) {
               return -1;
            }

            var9 = ArrayHelper.swap32bitFromArray(eventData, offset);
            offset += 4;
            list.add(Integer.valueOf(var9));
            break;
         case 1:
            if(eventData.length - offset < 8) {
               return -1;
            }

            long var10 = ArrayHelper.swap64bitFromArray(eventData, offset);
            offset += 8;
            list.add(Long.valueOf(var10));
            break;
         case 2:
            if(eventData.length - offset < 4) {
               return -1;
            }

            var9 = ArrayHelper.swap32bitFromArray(eventData, offset);
            offset += 4;
            if(eventData.length - offset < var9) {
               return -1;
            }

            String var11 = new String(eventData, offset, var9, Charsets.UTF_8);
            list.add(var11);
            offset += var9;
            break;
         case 3:
            if(eventData.length - offset < 1) {
               return -1;
            }

            byte count = eventData[offset++];
            ArrayList subList = new ArrayList();

            for(int i = 0; i < count; ++i) {
               int result = parseBinaryEvent(eventData, offset, subList);
               if(result == -1) {
                  return result;
               }

               offset += result;
            }

            list.add(subList.toArray());
            break;
         default:
            Log.e("EventLogParser", String.format("Unknown binary event type %1$d", new Object[]{Integer.valueOf(type)}));
            return -1;
         }

         return offset - dataOffset;
      }
   }

   private Object parseTextData(String data, int tagValue) {
      EventValueDescription[] desc = (EventValueDescription[])this.mValueDescriptionMap.get(Integer.valueOf(tagValue));
      if(desc == null) {
         return null;
      } else if(desc.length == 1) {
         return this.getObjectFromString(data, desc[0].getEventValueType());
      } else if(data.startsWith("[") && data.endsWith("]")) {
         data = data.substring(1, data.length() - 1);
         String[] values = data.split(",");
         Object[] objects;
         if(tagValue == 20001) {
            objects = new Object[]{this.getObjectFromString(values[0], EventContainer.EventValueType.LONG), this.getObjectFromString(values[1], EventContainer.EventValueType.LONG)};
            return objects;
         } else if(values.length != desc.length) {
            return null;
         } else {
            objects = new Object[values.length];

            for(int i = 0; i < desc.length; ++i) {
               Object obj = this.getObjectFromString(values[i], desc[i].getEventValueType());
               if(obj == null) {
                  return null;
               }

               objects[i] = obj;
            }

            return objects;
         }
      } else {
         return null;
      }
   }

   private Object getObjectFromString(String value, EventContainer.EventValueType type) {
      try {
         switch(EventLogParser.SyntheticClass_1.$SwitchMap$com$android$ddmlib$log$EventContainer$EventValueType[type.ordinal()]) {
         case 1:
            return Integer.valueOf(value);
         case 2:
            return Long.valueOf(value);
         case 3:
            return value;
         }
      } catch (NumberFormatException var4) {
         ;
      }

      return null;
   }

   public void saveTags(String filePath) throws IOException {
      File destFile = new File(filePath);
      destFile.createNewFile();
      FileOutputStream fos = null;

      try {
         fos = new FileOutputStream(destFile);
         Iterator i$ = this.mTagMap.keySet().iterator();

         while(i$.hasNext()) {
            Integer key = (Integer)i$.next();
            String tagName = (String)this.mTagMap.get(key);
            EventValueDescription[] descriptors = (EventValueDescription[])this.mValueDescriptionMap.get(key);
            String line = null;
            if(descriptors != null) {
               StringBuilder buffer = new StringBuilder();
               buffer.append(String.format("%1$d %2$s", new Object[]{key, tagName}));
               boolean first = true;
               EventValueDescription[] arr$ = descriptors;
               int len$ = descriptors.length;

               for(int i$1 = 0; i$1 < len$; ++i$1) {
                  EventValueDescription evd = arr$[i$1];
                  if(first) {
                     buffer.append(" (");
                     first = false;
                  } else {
                     buffer.append(",(");
                  }

                  buffer.append(evd.getName());
                  buffer.append("|");
                  buffer.append(evd.getEventValueType().getValue());
                  buffer.append("|");
                  buffer.append(evd.getValueType().getValue());
                  buffer.append("|)");
               }

               buffer.append("\n");
               line = buffer.toString();
            } else {
               line = String.format("%1$d %2$s\n", new Object[]{key, tagName});
            }

            byte[] var18 = line.getBytes();
            fos.write(var18);
         }
      } finally {
         if(fos != null) {
            fos.close();
         }

      }

   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] $SwitchMap$com$android$ddmlib$log$EventContainer$EventValueType = new int[EventContainer.EventValueType.values().length];

      static {
         try {
            $SwitchMap$com$android$ddmlib$log$EventContainer$EventValueType[EventContainer.EventValueType.INT.ordinal()] = 1;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            $SwitchMap$com$android$ddmlib$log$EventContainer$EventValueType[EventContainer.EventValueType.LONG.ordinal()] = 2;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            $SwitchMap$com$android$ddmlib$log$EventContainer$EventValueType[EventContainer.EventValueType.STRING.ordinal()] = 3;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }
}
