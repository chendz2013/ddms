package com.android.ddmlib.log;

import com.android.ddmlib.log.InvalidTypeException;
import com.android.ddmlib.log.LogReceiver;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EventContainer {
   public int mTag;
   public int pid;
   public int tid;
   public int sec;
   public int nsec;
   private Object mData;

   EventContainer(LogReceiver.LogEntry entry, int tag, Object data) {
      this.getType(data);
      this.mTag = tag;
      this.mData = data;
      this.pid = entry.pid;
      this.tid = entry.tid;
      this.sec = entry.sec;
      this.nsec = entry.nsec;
   }

   EventContainer(int tag, int pid, int tid, int sec, int nsec, Object data) {
      this.getType(data);
      this.mTag = tag;
      this.mData = data;
      this.pid = pid;
      this.tid = tid;
      this.sec = sec;
      this.nsec = nsec;
   }

   public final Integer getInt() throws InvalidTypeException {
      if(this.getType(this.mData) == EventContainer.EventValueType.INT) {
         return (Integer)this.mData;
      } else {
         throw new InvalidTypeException();
      }
   }

   public final Long getLong() throws InvalidTypeException {
      if(this.getType(this.mData) == EventContainer.EventValueType.LONG) {
         return (Long)this.mData;
      } else {
         throw new InvalidTypeException();
      }
   }

   public final String getString() throws InvalidTypeException {
      if(this.getType(this.mData) == EventContainer.EventValueType.STRING) {
         return (String)this.mData;
      } else {
         throw new InvalidTypeException();
      }
   }

   public Object getValue(int valueIndex) {
      return this.getValue(this.mData, valueIndex, true);
   }

   public double getValueAsDouble(int valueIndex) throws InvalidTypeException {
      return this.getValueAsDouble(this.mData, valueIndex, true);
   }

   public String getValueAsString(int valueIndex) throws InvalidTypeException {
      return this.getValueAsString(this.mData, valueIndex, true);
   }

   public EventContainer.EventValueType getType() {
      return this.getType(this.mData);
   }

   public final EventContainer.EventValueType getType(Object data) {
      if(data instanceof Integer) {
         return EventContainer.EventValueType.INT;
      } else if(data instanceof Long) {
         return EventContainer.EventValueType.LONG;
      } else if(data instanceof String) {
         return EventContainer.EventValueType.STRING;
      } else if(!(data instanceof Object[])) {
         return EventContainer.EventValueType.UNKNOWN;
      } else {
         Object[] objects = (Object[])((Object[])data);
         Object[] arr$ = objects;
         int len$ = objects.length;

         for(int i$ = 0; i$ < len$; ++i$) {
            Object obj = arr$[i$];
            EventContainer.EventValueType type = this.getType(obj);
            if(type == EventContainer.EventValueType.LIST || type == EventContainer.EventValueType.TREE) {
               return EventContainer.EventValueType.TREE;
            }
         }

         return EventContainer.EventValueType.LIST;
      }
   }

   public boolean testValue(int index, Object value, EventContainer.CompareMethod compareMethod) throws InvalidTypeException {
      EventContainer.EventValueType type = this.getType(this.mData);
      if(index > 0 && type != EventContainer.EventValueType.LIST) {
         throw new InvalidTypeException();
      } else {
         Object data = this.mData;
         if(type == EventContainer.EventValueType.LIST) {
            data = ((Object[])((Object[])this.mData))[index];
         }

         if(!data.getClass().equals(data.getClass())) {
            throw new InvalidTypeException();
         } else {
            switch(EventContainer.SyntheticClass_1.$SwitchMap$com$android$ddmlib$log$EventContainer$CompareMethod[compareMethod.ordinal()]) {
            case 1:
               return data.equals(value);
            case 2:
               if(data instanceof Integer) {
                  return ((Integer)data).compareTo((Integer)value) <= 0;
               } else {
                  if(data instanceof Long) {
                     return ((Long)data).compareTo((Long)value) <= 0;
                  }

                  throw new InvalidTypeException();
               }
            case 3:
               if(data instanceof Integer) {
                  return ((Integer)data).compareTo((Integer)value) < 0;
               } else {
                  if(data instanceof Long) {
                     return ((Long)data).compareTo((Long)value) < 0;
                  }

                  throw new InvalidTypeException();
               }
            case 4:
               if(data instanceof Integer) {
                  return ((Integer)data).compareTo((Integer)value) >= 0;
               } else {
                  if(data instanceof Long) {
                     return ((Long)data).compareTo((Long)value) >= 0;
                  }

                  throw new InvalidTypeException();
               }
            case 5:
               if(data instanceof Integer) {
                  return ((Integer)data).compareTo((Integer)value) > 0;
               } else {
                  if(data instanceof Long) {
                     return ((Long)data).compareTo((Long)value) > 0;
                  }

                  throw new InvalidTypeException();
               }
            case 6:
               if(data instanceof Integer) {
                  return (((Integer)data).intValue() & ((Integer)value).intValue()) != 0;
               } else {
                  if(data instanceof Long) {
                     return (((Long)data).longValue() & ((Long)value).longValue()) != 0L;
                  }

                  throw new InvalidTypeException();
               }
            default:
               throw new InvalidTypeException();
            }
         }
      }
   }

   private Object getValue(Object data, int valueIndex, boolean recursive) {
      EventContainer.EventValueType type = this.getType(data);
      switch(EventContainer.SyntheticClass_1.$SwitchMap$com$android$ddmlib$log$EventContainer$EventValueType[type.ordinal()]) {
      case 1:
      case 2:
      case 3:
         return data;
      case 4:
         if(recursive) {
            Object[] list = (Object[])((Object[])data);
            if(valueIndex >= 0 && valueIndex < list.length) {
               return this.getValue(list[valueIndex], valueIndex, false);
            }
         }
      default:
         return null;
      }
   }

   private double getValueAsDouble(Object data, int valueIndex, boolean recursive) throws InvalidTypeException {
      EventContainer.EventValueType type = this.getType(data);
      switch(EventContainer.SyntheticClass_1.$SwitchMap$com$android$ddmlib$log$EventContainer$EventValueType[type.ordinal()]) {
      case 1:
         throw new InvalidTypeException();
      case 2:
         return ((Integer)data).doubleValue();
      case 3:
         return ((Long)data).doubleValue();
      case 4:
         if(recursive) {
            Object[] list = (Object[])((Object[])data);
            if(valueIndex >= 0 && valueIndex < list.length) {
               return this.getValueAsDouble(list[valueIndex], valueIndex, false);
            }
         }
      default:
         throw new InvalidTypeException();
      }
   }

   private String getValueAsString(Object data, int valueIndex, boolean recursive) throws InvalidTypeException {
      EventContainer.EventValueType type = this.getType(data);
      switch(EventContainer.SyntheticClass_1.$SwitchMap$com$android$ddmlib$log$EventContainer$EventValueType[type.ordinal()]) {
      case 1:
         return (String)data;
      case 2:
         return data.toString();
      case 3:
         return data.toString();
      case 4:
         if(!recursive) {
            throw new InvalidTypeException("getValueAsString() doesn\'t support EventValueType.TREE");
         } else {
            Object[] list = (Object[])((Object[])data);
            if(valueIndex >= 0 && valueIndex < list.length) {
               return this.getValueAsString(list[valueIndex], valueIndex, false);
            }
         }
      default:
         throw new InvalidTypeException("getValueAsString() unsupported type:" + type);
      }
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] $SwitchMap$com$android$ddmlib$log$EventContainer$EventValueType;
      // $FF: synthetic field
      static final int[] $SwitchMap$com$android$ddmlib$log$EventContainer$CompareMethod = new int[EventContainer.CompareMethod.values().length];

      static {
         try {
            $SwitchMap$com$android$ddmlib$log$EventContainer$CompareMethod[EventContainer.CompareMethod.EQUAL_TO.ordinal()] = 1;
         } catch (NoSuchFieldError var10) {
            ;
         }

         try {
            $SwitchMap$com$android$ddmlib$log$EventContainer$CompareMethod[EventContainer.CompareMethod.LESSER_THAN.ordinal()] = 2;
         } catch (NoSuchFieldError var9) {
            ;
         }

         try {
            $SwitchMap$com$android$ddmlib$log$EventContainer$CompareMethod[EventContainer.CompareMethod.LESSER_THAN_STRICT.ordinal()] = 3;
         } catch (NoSuchFieldError var8) {
            ;
         }

         try {
            $SwitchMap$com$android$ddmlib$log$EventContainer$CompareMethod[EventContainer.CompareMethod.GREATER_THAN.ordinal()] = 4;
         } catch (NoSuchFieldError var7) {
            ;
         }

         try {
            $SwitchMap$com$android$ddmlib$log$EventContainer$CompareMethod[EventContainer.CompareMethod.GREATER_THAN_STRICT.ordinal()] = 5;
         } catch (NoSuchFieldError var6) {
            ;
         }

         try {
            $SwitchMap$com$android$ddmlib$log$EventContainer$CompareMethod[EventContainer.CompareMethod.BIT_CHECK.ordinal()] = 6;
         } catch (NoSuchFieldError var5) {
            ;
         }

         $SwitchMap$com$android$ddmlib$log$EventContainer$EventValueType = new int[EventContainer.EventValueType.values().length];

         try {
            $SwitchMap$com$android$ddmlib$log$EventContainer$EventValueType[EventContainer.EventValueType.STRING.ordinal()] = 1;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            $SwitchMap$com$android$ddmlib$log$EventContainer$EventValueType[EventContainer.EventValueType.INT.ordinal()] = 2;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            $SwitchMap$com$android$ddmlib$log$EventContainer$EventValueType[EventContainer.EventValueType.LONG.ordinal()] = 3;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            $SwitchMap$com$android$ddmlib$log$EventContainer$EventValueType[EventContainer.EventValueType.LIST.ordinal()] = 4;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }

   public static enum EventValueType {
      UNKNOWN(0),
      INT(1),
      LONG(2),
      STRING(3),
      LIST(4),
      TREE(5);

      private static final Pattern STORAGE_PATTERN;
      private int mValue;

      static EventContainer.EventValueType getEventValueType(int value) {
         EventContainer.EventValueType[] arr$ = values();
         int len$ = arr$.length;

         for(int i$ = 0; i$ < len$; ++i$) {
            EventContainer.EventValueType type = arr$[i$];
            if(type.mValue == value) {
               return type;
            }
         }

         return null;
      }

      public static String getStorageString(Object object) {
         return object instanceof String?STRING.mValue + "@" + object:(object instanceof Integer?INT.mValue + "@" + object.toString():(object instanceof Long?LONG.mValue + "@" + object.toString():null));
      }

      public static Object getObjectFromStorageString(String value) {
         Matcher m = STORAGE_PATTERN.matcher(value);
         if(m.matches()) {
            try {
               EventContainer.EventValueType nfe = getEventValueType(Integer.parseInt(m.group(1)));
               if(nfe == null) {
                  return null;
               }

               switch(EventContainer.SyntheticClass_1.$SwitchMap$com$android$ddmlib$log$EventContainer$EventValueType[nfe.ordinal()]) {
               case 1:
                  return m.group(2);
               case 2:
                  return Integer.valueOf(m.group(2));
               case 3:
                  return Long.valueOf(m.group(2));
               }
            } catch (NumberFormatException var3) {
               return null;
            }
         }

         return null;
      }

      public int getValue() {
         return this.mValue;
      }

      public String toString() {
         return super.toString().toLowerCase(Locale.US);
      }

      private EventValueType(int value) {
         this.mValue = value;
      }

      static {
         STORAGE_PATTERN = Pattern.compile("^(\\d+)@(.*)$");
      }
   }

   public static enum CompareMethod {
      EQUAL_TO("equals", "=="),
      LESSER_THAN("less than or equals to", "<="),
      LESSER_THAN_STRICT("less than", "<"),
      GREATER_THAN("greater than or equals to", ">="),
      GREATER_THAN_STRICT("greater than", ">"),
      BIT_CHECK("bit check", "&");

      private final String mName;
      private final String mTestString;

      private CompareMethod(String name, String testString) {
         this.mName = name;
         this.mTestString = testString;
      }

      public String toString() {
         return this.mName;
      }

      public String testString() {
         return this.mTestString;
      }
   }
}
