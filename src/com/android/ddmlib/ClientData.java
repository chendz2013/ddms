package com.android.ddmlib;

import com.android.ddmlib.AllocationInfo;
import com.android.ddmlib.Client;
import com.android.ddmlib.HeapSegment;
import com.android.ddmlib.NativeAllocationInfo;
import com.android.ddmlib.NativeLibraryMapInfo;
import com.android.ddmlib.ThreadInfo;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * Client的数据
 */
public class ClientData {
   private static final String PRE_INITIALIZED = "<pre-initialized>";
   public static final String FEATURE_PROFILING = "method-trace-profiling";
   public static final String FEATURE_PROFILING_STREAMING = "method-trace-profiling-streaming";
   public static final String FEATURE_SAMPLING_PROFILER = "method-sample-profiling";
   public static final String FEATURE_OPENGL_TRACING = "opengl-tracing";
   public static final String FEATURE_VIEW_HIERARCHY = "view-hierarchy";
   public static final String FEATURE_HPROF = "hprof-heap-dump";
   public static final String FEATURE_HPROF_STREAMING = "hprof-heap-dump-streaming";
   /** @deprecated */
   @Deprecated
   private static ClientData.IHprofDumpHandler sHprofDumpHandler;
   private static ClientData.IMethodProfilingHandler sMethodProfilingHandler;
   private static ClientData.IAllocationTrackingHandler sAllocationTrackingHandler;
   //DDM支持
   private boolean mIsDdmAware;
   private final int mPid;
   //VM的版本
   private String mVmIdentifier;
   //app的描述
   private String mClientDescription;
   private int mUserId;
   private boolean mValidUserId;
   //abi的版本
   private String mAbi;
   //jvm设置
   private String mJvmFlags;
   private ClientData.DebuggerStatus mDebuggerInterest;
   private final HashSet<String> mFeatures = new HashSet();
   private TreeMap<Integer, ThreadInfo> mThreadMap;
   private final ClientData.HeapData mHeapData = new ClientData.HeapData();
   private final ClientData.HeapData mNativeHeapData = new ClientData.HeapData();
   private ClientData.HprofData mHprofData = null;
   private HashMap<Integer, ClientData.HeapInfo> mHeapInfoMap = new HashMap();
   private ArrayList<NativeLibraryMapInfo> mNativeLibMapInfo = new ArrayList();
   private ArrayList<NativeAllocationInfo> mNativeAllocationList = new ArrayList();
   private int mNativeTotalMemory;
   private AllocationInfo[] mAllocations;
   private ClientData.AllocationTrackingStatus mAllocationStatus;
   /** @deprecated */
   @Deprecated
   private String mPendingHprofDump;
   private ClientData.MethodProfilingStatus mProfilingStatus;
   private String mPendingMethodProfiling;

   public void setHprofData(byte[] data) {
      this.mHprofData = new ClientData.HprofData(data);
   }

   public void setHprofData(String filename) {
      this.mHprofData = new ClientData.HprofData(filename);
   }

   public void clearHprofData() {
      this.mHprofData = null;
   }

   public ClientData.HprofData getHprofData() {
      return this.mHprofData;
   }

   /** @deprecated */
   @Deprecated
   public static void setHprofDumpHandler(ClientData.IHprofDumpHandler handler) {
      sHprofDumpHandler = handler;
   }

   /** @deprecated */
   @Deprecated
   static ClientData.IHprofDumpHandler getHprofDumpHandler() {
      return sHprofDumpHandler;
   }

   public static void setMethodProfilingHandler(ClientData.IMethodProfilingHandler handler) {
      sMethodProfilingHandler = handler;
   }

   static ClientData.IMethodProfilingHandler getMethodProfilingHandler() {
      return sMethodProfilingHandler;
   }

   public static void setAllocationTrackingHandler(ClientData.IAllocationTrackingHandler handler) {
      sAllocationTrackingHandler = handler;
   }

   static ClientData.IAllocationTrackingHandler getAllocationTrackingHandler() {
      return sAllocationTrackingHandler;
   }

   ClientData(int pid) {
      this.mAllocationStatus = ClientData.AllocationTrackingStatus.UNKNOWN;
      this.mProfilingStatus = ClientData.MethodProfilingStatus.UNKNOWN;
      this.mPid = pid;
      this.mDebuggerInterest = ClientData.DebuggerStatus.DEFAULT;
      this.mThreadMap = new TreeMap();
   }

   public boolean isDdmAware() {
      return this.mIsDdmAware;
   }

   void isDdmAware(boolean aware) {
      this.mIsDdmAware = aware;
   }

   public int getPid() {
      return this.mPid;
   }

   public String getVmIdentifier() {
      return this.mVmIdentifier;
   }

   void setVmIdentifier(String ident) {
      this.mVmIdentifier = ident;
   }

   public String getClientDescription() {
      return this.mClientDescription;
   }

   public int getUserId() {
      return this.mUserId;
   }

   public boolean isValidUserId() {
      return this.mValidUserId;
   }

   public String getAbi() {
      return this.mAbi;
   }

   public String getJvmFlags() {
      return this.mJvmFlags;
   }

   void setClientDescription(String description) {
      if(this.mClientDescription == null && !description.isEmpty() && !"<pre-initialized>".equals(description)) {
         this.mClientDescription = description;
      }

   }

   void setUserId(int id) {
      this.mUserId = id;
      this.mValidUserId = true;
   }

   void setAbi(String abi) {
      this.mAbi = abi;
   }

   void setJvmFlags(String jvmFlags) {
      this.mJvmFlags = jvmFlags;
   }

   public ClientData.DebuggerStatus getDebuggerConnectionStatus() {
      return this.mDebuggerInterest;
   }

   void setDebuggerConnectionStatus(ClientData.DebuggerStatus status) {
      this.mDebuggerInterest = status;
   }

   synchronized void setHeapInfo(int heapId, long maxSizeInBytes, long sizeInBytes, long bytesAllocated, long objectsAllocated, long timeStamp, byte reason) {
      this.mHeapInfoMap.put(Integer.valueOf(heapId), new ClientData.HeapInfo(maxSizeInBytes, sizeInBytes, bytesAllocated, objectsAllocated, timeStamp, reason));
   }

   public ClientData.HeapData getVmHeapData() {
      return this.mHeapData;
   }

   ClientData.HeapData getNativeHeapData() {
      return this.mNativeHeapData;
   }

   /**
    * 读取Heap的ID
    * @return
     */
   public synchronized Iterator<Integer> getVmHeapIds() {
      return this.mHeapInfoMap.keySet().iterator();
   }

   public synchronized ClientData.HeapInfo getVmHeapInfo(int heapId) {
      return (ClientData.HeapInfo)this.mHeapInfoMap.get(Integer.valueOf(heapId));
   }

   synchronized void addThread(int threadId, String threadName) {
      ThreadInfo attr = new ThreadInfo(threadId, threadName);
      this.mThreadMap.put(Integer.valueOf(threadId), attr);
   }

   synchronized void removeThread(int threadId) {
      this.mThreadMap.remove(Integer.valueOf(threadId));
   }

   public synchronized ThreadInfo[] getThreads() {
      Collection threads = this.mThreadMap.values();
      return (ThreadInfo[])threads.toArray(new ThreadInfo[threads.size()]);
   }

   synchronized ThreadInfo getThread(int threadId) {
      return (ThreadInfo)this.mThreadMap.get(Integer.valueOf(threadId));
   }

   synchronized void clearThreads() {
      this.mThreadMap.clear();
   }

   public synchronized List<NativeAllocationInfo> getNativeAllocationList() {
      return Collections.unmodifiableList(this.mNativeAllocationList);
   }

   synchronized void addNativeAllocation(NativeAllocationInfo allocInfo) {
      this.mNativeAllocationList.add(allocInfo);
   }

   synchronized void clearNativeAllocationInfo() {
      this.mNativeAllocationList.clear();
   }

   public synchronized int getTotalNativeMemory() {
      return this.mNativeTotalMemory;
   }

   synchronized void setTotalNativeMemory(int totalMemory) {
      this.mNativeTotalMemory = totalMemory;
   }

   synchronized void addNativeLibraryMapInfo(long startAddr, long endAddr, String library) {
      this.mNativeLibMapInfo.add(new NativeLibraryMapInfo(startAddr, endAddr, library));
   }

   public synchronized List<NativeLibraryMapInfo> getMappedNativeLibraries() {
      return Collections.unmodifiableList(this.mNativeLibMapInfo);
   }

   synchronized void setAllocationStatus(ClientData.AllocationTrackingStatus status) {
      this.mAllocationStatus = status;
   }

   public synchronized ClientData.AllocationTrackingStatus getAllocationStatus() {
      return this.mAllocationStatus;
   }

   synchronized void setAllocations(AllocationInfo[] allocs) {
      this.mAllocations = allocs;
   }

   public synchronized AllocationInfo[] getAllocations() {
      return this.mAllocations;
   }

   void addFeature(String feature) {
      this.mFeatures.add(feature);
   }

   public boolean hasFeature(String feature) {
      return this.mFeatures.contains(feature);
   }

   /** @deprecated */
   @Deprecated
   void setPendingHprofDump(String pendingHprofDump) {
      this.mPendingHprofDump = pendingHprofDump;
   }

   /** @deprecated */
   @Deprecated
   String getPendingHprofDump() {
      return this.mPendingHprofDump;
   }

   /** @deprecated */
   @Deprecated
   public boolean hasPendingHprofDump() {
      return this.mPendingHprofDump != null;
   }

   synchronized void setMethodProfilingStatus(ClientData.MethodProfilingStatus status) {
      this.mProfilingStatus = status;
   }

   public synchronized ClientData.MethodProfilingStatus getMethodProfilingStatus() {
      return this.mProfilingStatus;
   }

   void setPendingMethodProfiling(String pendingMethodProfiling) {
      this.mPendingMethodProfiling = pendingMethodProfiling;
   }

   String getPendingMethodProfiling() {
      return this.mPendingMethodProfiling;
   }

   public interface IAllocationTrackingHandler {
      void onSuccess(byte[] var1, Client var2);
   }

   public interface IMethodProfilingHandler {
      void onSuccess(String var1, Client var2);

      void onSuccess(byte[] var1, Client var2);

      void onStartFailure(Client var1, String var2);

      void onEndFailure(Client var1, String var2);
   }

   /** @deprecated */
   @Deprecated
   public interface IHprofDumpHandler {
      void onSuccess(String var1, Client var2);

      void onSuccess(byte[] var1, Client var2);

      void onEndFailure(Client var1, String var2);
   }

   public static class HprofData {
      public final ClientData.HprofData.Type type;
      public final String filename;
      public final byte[] data;

      public HprofData(String filename) {
         this.type = ClientData.HprofData.Type.FILE;
         this.filename = filename;
         this.data = null;
      }

      public HprofData(byte[] data) {
         this.type = ClientData.HprofData.Type.DATA;
         this.data = data;
         this.filename = null;
      }

      public static enum Type {
         FILE,
         DATA;
      }
   }

   public static class HeapInfo {
      public long maxSizeInBytes;
      public long sizeInBytes;
      public long bytesAllocated;
      public long objectsAllocated;
      public long timeStamp;
      public byte reason;

      public HeapInfo(long maxSizeInBytes, long sizeInBytes, long bytesAllocated, long objectsAllocated, long timeStamp, byte reason) {
         this.maxSizeInBytes = maxSizeInBytes;
         this.sizeInBytes = sizeInBytes;
         this.bytesAllocated = bytesAllocated;
         this.objectsAllocated = objectsAllocated;
         this.timeStamp = timeStamp;
         this.reason = reason;
      }
   }

   public static class HeapData {
      private TreeSet<HeapSegment> mHeapSegments = new TreeSet();
      private boolean mHeapDataComplete = false;
      private byte[] mProcessedHeapData;
      private Map<Integer, ArrayList<HeapSegment.HeapSegmentElement>> mProcessedHeapMap;

      public synchronized void clearHeapData() {
         this.mHeapSegments = new TreeSet();
         this.mHeapDataComplete = false;
      }

      synchronized void addHeapData(ByteBuffer data) {
         if(this.mHeapDataComplete) {
            this.clearHeapData();
         }

         HeapSegment hs;
         try {
            hs = new HeapSegment(data);
         } catch (BufferUnderflowException var4) {
            System.err.println("Discarding short HPSG data (length " + data.limit() + ")");
            return;
         }

         this.mHeapSegments.add(hs);
      }

      synchronized void sealHeapData() {
         this.mHeapDataComplete = true;
      }

      public boolean isHeapDataComplete() {
         return this.mHeapDataComplete;
      }
      //读取HeapSegment列表
      public Collection<HeapSegment> getHeapSegments() {
         return this.isHeapDataComplete()?this.mHeapSegments:null;
      }

      public void setProcessedHeapData(byte[] heapData) {
         this.mProcessedHeapData = heapData;
      }
      //读取已经处理的Heap数据
      public byte[] getProcessedHeapData() {
         return this.mProcessedHeapData;
      }

      public void setProcessedHeapMap(Map<Integer, ArrayList<HeapSegment.HeapSegmentElement>> heapMap) {
         this.mProcessedHeapMap = heapMap;
      }

      public Map<Integer, ArrayList<HeapSegment.HeapSegmentElement>> getProcessedHeapMap() {
         return this.mProcessedHeapMap;
      }
   }

   public static enum MethodProfilingStatus {
      UNKNOWN,
      OFF,
      TRACER_ON,
      SAMPLER_ON;
   }

   public static enum AllocationTrackingStatus {
      UNKNOWN,
      OFF,
      ON;
   }

   public static enum DebuggerStatus {
      DEFAULT,
      WAITING,
      ATTACHED,
      ERROR;
   }
}
