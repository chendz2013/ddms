package com.android.ddmlib;

public interface IShellOutputReceiver {
   void addOutput(byte[] var1, int var2, int var3);

   void flush();

   boolean isCancelled();
}
