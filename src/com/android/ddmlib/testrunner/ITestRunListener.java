package com.android.ddmlib.testrunner;

import com.android.ddmlib.testrunner.TestIdentifier;
import java.util.Map;

public interface ITestRunListener {
   void testRunStarted(String var1, int var2);

   void testStarted(TestIdentifier var1);

   void testFailed(TestIdentifier var1, String var2);

   void testAssumptionFailure(TestIdentifier var1, String var2);

   void testIgnored(TestIdentifier var1);

   void testEnded(TestIdentifier var1, Map<String, String> var2);

   void testRunFailed(String var1);

   void testRunStopped(long var1);

   void testRunEnded(long var1, Map<String, String> var3);
}
