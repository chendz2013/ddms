package com.android.ddmlib.testrunner;

public class TestIdentifier {
   private final String mClassName;
   private final String mTestName;

   public TestIdentifier(String className, String testName) {
      if(className != null && testName != null) {
         this.mClassName = className;
         this.mTestName = testName;
      } else {
         throw new IllegalArgumentException("className and testName must be non-null");
      }
   }

   public String getClassName() {
      return this.mClassName;
   }

   public String getTestName() {
      return this.mTestName;
   }

   public int hashCode() {
      boolean prime = true;
      byte result = 1;
      int result1 = 31 * result + (this.mClassName == null?0:this.mClassName.hashCode());
      result1 = 31 * result1 + (this.mTestName == null?0:this.mTestName.hashCode());
      return result1;
   }

   public boolean equals(Object obj) {
      if(this == obj) {
         return true;
      } else if(obj == null) {
         return false;
      } else if(this.getClass() != obj.getClass()) {
         return false;
      } else {
         TestIdentifier other = (TestIdentifier)obj;
         if(this.mClassName == null) {
            if(other.mClassName != null) {
               return false;
            }
         } else if(!this.mClassName.equals(other.mClassName)) {
            return false;
         }

         if(this.mTestName == null) {
            if(other.mTestName != null) {
               return false;
            }
         } else if(!this.mTestName.equals(other.mTestName)) {
            return false;
         }

         return true;
      }
   }

   public String toString() {
      return String.format("%s#%s", new Object[]{this.getClassName(), this.getTestName()});
   }
}
