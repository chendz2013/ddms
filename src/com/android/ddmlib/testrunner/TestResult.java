package com.android.ddmlib.testrunner;

import java.util.Arrays;
import java.util.Map;

public class TestResult {
   private TestResult.TestStatus mStatus;
   private String mStackTrace;
   private Map<String, String> mMetrics;
   private long mStartTime = 0L;
   private long mEndTime = 0L;

   public TestResult() {
      this.mStatus = TestResult.TestStatus.INCOMPLETE;
      this.mStartTime = System.currentTimeMillis();
   }

   public TestResult.TestStatus getStatus() {
      return this.mStatus;
   }

   public String getStackTrace() {
      return this.mStackTrace;
   }

   public Map<String, String> getMetrics() {
      return this.mMetrics;
   }

   public void setMetrics(Map<String, String> metrics) {
      this.mMetrics = metrics;
   }

   public long getStartTime() {
      return this.mStartTime;
   }

   public long getEndTime() {
      return this.mEndTime;
   }

   public TestResult setStatus(TestResult.TestStatus status) {
      this.mStatus = status;
      return this;
   }

   public void setStackTrace(String trace) {
      this.mStackTrace = trace;
   }

   public void setEndTime(long currentTimeMillis) {
      this.mEndTime = currentTimeMillis;
   }

   public int hashCode() {
      return Arrays.hashCode(new Object[]{this.mMetrics, this.mStackTrace, this.mStatus});
   }

   public boolean equals(Object obj) {
      if(this == obj) {
         return true;
      } else if(obj == null) {
         return false;
      } else if(this.getClass() != obj.getClass()) {
         return false;
      } else {
         TestResult other = (TestResult)obj;
         return equal(this.mMetrics, other.mMetrics) && equal(this.mStackTrace, other.mStackTrace) && equal(this.mStatus, other.mStatus);
      }
   }

   private static boolean equal(Object a, Object b) {
      return a == b || a != null && a.equals(b);
   }

   public static enum TestStatus {
      FAILURE,
      PASSED,
      INCOMPLETE,
      ASSUMPTION_FAILURE,
      IGNORED;
   }
}
