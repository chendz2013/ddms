package com.android.ddmlib.testrunner;

import com.android.ddmlib.Log;
import com.android.ddmlib.testrunner.ITestRunListener;
import com.android.ddmlib.testrunner.TestIdentifier;
import com.android.ddmlib.testrunner.TestResult;
import com.android.ddmlib.testrunner.TestRunResult;
import com.google.common.collect.ImmutableMap;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.Map.Entry;
import org.kxml2.io.KXmlSerializer;

public class XmlTestRunListener implements ITestRunListener {
   private static final String LOG_TAG = "XmlResultReporter";
   private static final String TEST_RESULT_FILE_SUFFIX = ".xml";
   private static final String TEST_RESULT_FILE_PREFIX = "test_result_";
   private static final String TESTSUITE = "testsuite";
   private static final String TESTCASE = "testcase";
   private static final String ERROR = "error";
   private static final String FAILURE = "failure";
   private static final String SKIPPED_TAG = "skipped";
   private static final String ATTR_NAME = "name";
   private static final String ATTR_TIME = "time";
   private static final String ATTR_ERRORS = "errors";
   private static final String ATTR_FAILURES = "failures";
   private static final String ATTR_SKIPPED = "skipped";
   private static final String ATTR_ASSERTIOMS = "assertions";
   private static final String ATTR_TESTS = "tests";
   private static final String PROPERTIES = "properties";
   private static final String PROPERTY = "property";
   private static final String ATTR_CLASSNAME = "classname";
   private static final String TIMESTAMP = "timestamp";
   private static final String HOSTNAME = "hostname";
   private static final String ns = null;
   private String mHostName = "localhost";
   private File mReportDir = new File(System.getProperty("java.io.tmpdir"));
   private String mReportPath = "";
   private TestRunResult mRunResult = new TestRunResult();

   public void setReportDir(File file) {
      this.mReportDir = file;
   }

   public void setHostName(String hostName) {
      this.mHostName = hostName;
   }

   public TestRunResult getRunResult() {
      return this.mRunResult;
   }

   public void testRunStarted(String runName, int numTests) {
      this.mRunResult = new TestRunResult();
      this.mRunResult.testRunStarted(runName, numTests);
   }

   public void testStarted(TestIdentifier test) {
      this.mRunResult.testStarted(test);
   }

   public void testFailed(TestIdentifier test, String trace) {
      this.mRunResult.testFailed(test, trace);
   }

   public void testAssumptionFailure(TestIdentifier test, String trace) {
      this.mRunResult.testAssumptionFailure(test, trace);
   }

   public void testIgnored(TestIdentifier test) {
      this.mRunResult.testIgnored(test);
   }

   public void testEnded(TestIdentifier test, Map<String, String> testMetrics) {
      this.mRunResult.testEnded(test, testMetrics);
   }

   public void testRunFailed(String errorMessage) {
      this.mRunResult.testRunFailed(errorMessage);
   }

   public void testRunStopped(long elapsedTime) {
      this.mRunResult.testRunStopped(elapsedTime);
   }

   public void testRunEnded(long elapsedTime, Map<String, String> runMetrics) {
      this.mRunResult.testRunEnded(elapsedTime, runMetrics);
      this.generateDocument(this.mReportDir, elapsedTime);
   }

   private void generateDocument(File reportDir, long elapsedTime) {
      String timestamp = this.getTimestamp();
      OutputStream stream = null;

      try {
         stream = this.createOutputResultStream(reportDir);
         KXmlSerializer ignored = new KXmlSerializer();
         ignored.setOutput(stream, "UTF-8");
         ignored.startDocument("UTF-8", (Boolean)null);
         ignored.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
         this.printTestResults(ignored, timestamp, elapsedTime);
         ignored.endDocument();
         String msg = String.format("XML test result file generated at %s. %s", new Object[]{this.getAbsoluteReportPath(), this.mRunResult.getTextSummary()});
         Log.logAndDisplay(Log.LogLevel.INFO, "XmlResultReporter", msg);
      } catch (IOException var16) {
         Log.e("XmlResultReporter", "Failed to generate report data");
      } finally {
         if(stream != null) {
            try {
               stream.close();
            } catch (IOException var15) {
               ;
            }
         }

      }

   }

   private String getAbsoluteReportPath() {
      return this.mReportPath;
   }

   String getTimestamp() {
      SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd\'T\'HH:mm:ss", Locale.getDefault());
      TimeZone gmt = TimeZone.getTimeZone("UTC");
      dateFormat.setTimeZone(gmt);
      dateFormat.setLenient(true);
      String timestamp = dateFormat.format(new Date());
      return timestamp;
   }

   protected File getResultFile(File reportDir) throws IOException {
      File reportFile = File.createTempFile("test_result_", ".xml", reportDir);
      Log.i("XmlResultReporter", String.format("Created xml report file at %s", new Object[]{reportFile.getAbsolutePath()}));
      return reportFile;
   }

   OutputStream createOutputResultStream(File reportDir) throws IOException {
      File reportFile = this.getResultFile(reportDir);
      this.mReportPath = reportFile.getAbsolutePath();
      return new BufferedOutputStream(new FileOutputStream(reportFile));
   }

   protected String getTestSuiteName() {
      return this.mRunResult.getName();
   }

   void printTestResults(KXmlSerializer serializer, String timestamp, long elapsedTime) throws IOException {
      serializer.startTag(ns, "testsuite");
      String name = this.getTestSuiteName();
      if(name != null) {
         serializer.attribute(ns, "name", name);
      }

      serializer.attribute(ns, "tests", Integer.toString(this.mRunResult.getNumTests()));
      serializer.attribute(ns, "failures", Integer.toString(this.mRunResult.getNumAllFailedTests()));
      serializer.attribute(ns, "errors", "0");
      serializer.attribute(ns, "skipped", Integer.toString(this.mRunResult.getNumTestsInState(TestResult.TestStatus.IGNORED)));
      serializer.attribute(ns, "time", Double.toString((double)elapsedTime / 1000.0D));
      serializer.attribute(ns, "timestamp", timestamp);
      serializer.attribute(ns, "hostname", this.mHostName);
      serializer.startTag(ns, "properties");
      Iterator testResults = this.getPropertiesAttributes().entrySet().iterator();

      while(testResults.hasNext()) {
         Entry i$ = (Entry)testResults.next();
         serializer.startTag(ns, "property");
         serializer.attribute(ns, "name", (String)i$.getKey());
         serializer.attribute(ns, "value", (String)i$.getValue());
         serializer.endTag(ns, "property");
      }

      serializer.endTag(ns, "properties");
      Map testResults1 = this.mRunResult.getTestResults();
      Iterator i$1 = testResults1.entrySet().iterator();

      while(i$1.hasNext()) {
         Entry testEntry = (Entry)i$1.next();
         this.print(serializer, (TestIdentifier)testEntry.getKey(), (TestResult)testEntry.getValue());
      }

      serializer.endTag(ns, "testsuite");
   }

   protected Map<String, String> getPropertiesAttributes() {
      return ImmutableMap.of();
   }

   protected String getTestName(TestIdentifier testId) {
      return testId.getTestName();
   }

   void print(KXmlSerializer serializer, TestIdentifier testId, TestResult testResult) throws IOException {
      serializer.startTag(ns, "testcase");
      serializer.attribute(ns, "name", this.getTestName(testId));
      serializer.attribute(ns, "classname", testId.getClassName());
      long elapsedTimeMs = testResult.getEndTime() - testResult.getStartTime();
      serializer.attribute(ns, "time", Double.toString((double)elapsedTimeMs / 1000.0D));
      switch(XmlTestRunListener.SyntheticClass_1.$SwitchMap$com$android$ddmlib$testrunner$TestResult$TestStatus[testResult.getStatus().ordinal()]) {
      case 1:
         this.printFailedTest(serializer, "failure", testResult.getStackTrace());
         break;
      case 2:
         this.printFailedTest(serializer, "skipped", testResult.getStackTrace());
         break;
      case 3:
         serializer.startTag(ns, "skipped");
         serializer.endTag(ns, "skipped");
      }

      serializer.endTag(ns, "testcase");
   }

   private void printFailedTest(KXmlSerializer serializer, String tag, String stack) throws IOException {
      serializer.startTag(ns, tag);
      serializer.text(this.sanitize(stack));
      serializer.endTag(ns, tag);
   }

   private String sanitize(String text) {
      return text.replace("\u0000", "<\\0>");
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] $SwitchMap$com$android$ddmlib$testrunner$TestResult$TestStatus = new int[TestResult.TestStatus.values().length];

      static {
         try {
            $SwitchMap$com$android$ddmlib$testrunner$TestResult$TestStatus[TestResult.TestStatus.FAILURE.ordinal()] = 1;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            $SwitchMap$com$android$ddmlib$testrunner$TestResult$TestStatus[TestResult.TestStatus.ASSUMPTION_FAILURE.ordinal()] = 2;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            $SwitchMap$com$android$ddmlib$testrunner$TestResult$TestStatus[TestResult.TestStatus.IGNORED.ordinal()] = 3;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }
}
