package com.android.ddmlib.testrunner;

import com.android.ddmlib.Log;
import com.android.ddmlib.testrunner.ITestRunListener;
import com.android.ddmlib.testrunner.TestIdentifier;
import com.android.ddmlib.testrunner.TestResult;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

public class TestRunResult implements ITestRunListener {
   private static final String LOG_TAG = TestRunResult.class.getSimpleName();
   private String mTestRunName = "not started";
   private Map<TestIdentifier, TestResult> mTestResults = new LinkedHashMap();
   private Map<String, String> mRunMetrics = new HashMap();
   private boolean mIsRunComplete = false;
   private long mElapsedTime = 0L;
   private int[] mStatusCounts = new int[TestResult.TestStatus.values().length];
   private boolean mIsCountDirty = true;
   private String mRunFailureError = null;
   private boolean mAggregateMetrics = false;

   public void setAggregateMetrics(boolean metricAggregation) {
      this.mAggregateMetrics = metricAggregation;
   }

   public String getName() {
      return this.mTestRunName;
   }

   public Map<TestIdentifier, TestResult> getTestResults() {
      return this.mTestResults;
   }

   public Map<String, String> getRunMetrics() {
      return this.mRunMetrics;
   }

   public Set<TestIdentifier> getCompletedTests() {
      LinkedHashSet completedTests = new LinkedHashSet();
      Iterator i$ = this.getTestResults().entrySet().iterator();

      while(i$.hasNext()) {
         Entry testEntry = (Entry)i$.next();
         if(!((TestResult)testEntry.getValue()).getStatus().equals(TestResult.TestStatus.INCOMPLETE)) {
            completedTests.add(testEntry.getKey());
         }
      }

      return completedTests;
   }

   public boolean isRunFailure() {
      return this.mRunFailureError != null;
   }

   public boolean isRunComplete() {
      return this.mIsRunComplete;
   }

   public void setRunComplete(boolean runComplete) {
      this.mIsRunComplete = runComplete;
   }

   public int getNumTestsInState(TestResult.TestStatus status) {
      if(this.mIsCountDirty) {
         for(int i$ = 0; i$ < this.mStatusCounts.length; ++i$) {
            this.mStatusCounts[i$] = 0;
         }

         TestResult r;
         for(Iterator var4 = this.mTestResults.values().iterator(); var4.hasNext(); ++this.mStatusCounts[r.getStatus().ordinal()]) {
            r = (TestResult)var4.next();
         }

         this.mIsCountDirty = false;
      }

      return this.mStatusCounts[status.ordinal()];
   }

   public int getNumTests() {
      return this.mTestResults.size();
   }

   public int getNumCompleteTests() {
      return this.getNumTests() - this.getNumTestsInState(TestResult.TestStatus.INCOMPLETE);
   }

   public boolean hasFailedTests() {
      return this.getNumAllFailedTests() > 0;
   }

   public int getNumAllFailedTests() {
      return this.getNumTestsInState(TestResult.TestStatus.FAILURE);
   }

   public long getElapsedTime() {
      return this.mElapsedTime;
   }

   public String getRunFailureMessage() {
      return this.mRunFailureError;
   }

   public void testRunStarted(String runName, int testCount) {
      this.mTestRunName = runName;
      this.mIsRunComplete = false;
      this.mRunFailureError = null;
   }

   public void testStarted(TestIdentifier test) {
      this.addTestResult(test, new TestResult());
   }

   private void addTestResult(TestIdentifier test, TestResult testResult) {
      this.mIsCountDirty = true;
      this.mTestResults.put(test, testResult);
   }

   private void updateTestResult(TestIdentifier test, TestResult.TestStatus status, String trace) {
      TestResult r = (TestResult)this.mTestResults.get(test);
      if(r == null) {
         Log.d(LOG_TAG, String.format("received test event without test start for %s", new Object[]{test}));
         r = new TestResult();
      }

      r.setStatus(status);
      r.setStackTrace(trace);
      this.addTestResult(test, r);
   }

   public void testFailed(TestIdentifier test, String trace) {
      this.updateTestResult(test, TestResult.TestStatus.FAILURE, trace);
   }

   public void testAssumptionFailure(TestIdentifier test, String trace) {
      this.updateTestResult(test, TestResult.TestStatus.ASSUMPTION_FAILURE, trace);
   }

   public void testIgnored(TestIdentifier test) {
      this.updateTestResult(test, TestResult.TestStatus.IGNORED, (String)null);
   }

   public void testEnded(TestIdentifier test, Map<String, String> testMetrics) {
      TestResult result = (TestResult)this.mTestResults.get(test);
      if(result == null) {
         result = new TestResult();
      }

      if(result.getStatus().equals(TestResult.TestStatus.INCOMPLETE)) {
         result.setStatus(TestResult.TestStatus.PASSED);
      }

      result.setEndTime(System.currentTimeMillis());
      result.setMetrics(testMetrics);
      this.addTestResult(test, result);
   }

   public void testRunFailed(String errorMessage) {
      this.mRunFailureError = errorMessage;
   }

   public void testRunStopped(long elapsedTime) {
      this.mElapsedTime += elapsedTime;
      this.mIsRunComplete = true;
   }

   public void testRunEnded(long elapsedTime, Map<String, String> runMetrics) {
      if(this.mAggregateMetrics) {
         Iterator i$ = runMetrics.entrySet().iterator();

         while(i$.hasNext()) {
            Entry entry = (Entry)i$.next();
            String existingValue = (String)this.mRunMetrics.get(entry.getKey());
            String combinedValue = this.combineValues(existingValue, (String)entry.getValue());
            this.mRunMetrics.put((String)entry.getKey(), combinedValue);
         }
      } else {
         this.mRunMetrics.putAll(runMetrics);
      }

      this.mElapsedTime += elapsedTime;
      this.mIsRunComplete = true;
   }

   private String combineValues(String existingValue, String newValue) {
      if(existingValue != null) {
         try {
            Long e1 = Long.valueOf(Long.parseLong(existingValue));
            Long newDouble1 = Long.valueOf(Long.parseLong(newValue));
            return Long.toString(e1.longValue() + newDouble1.longValue());
         } catch (NumberFormatException var6) {
            try {
               Double e = Double.valueOf(Double.parseDouble(existingValue));
               Double newDouble = Double.valueOf(Double.parseDouble(newValue));
               return Double.toString(e.doubleValue() + newDouble.doubleValue());
            } catch (NumberFormatException var5) {
               ;
            }
         }
      }

      return newValue;
   }

   public String getTextSummary() {
      StringBuilder builder = new StringBuilder();
      builder.append(String.format("Total tests %d, ", new Object[]{Integer.valueOf(this.getNumTests())}));
      TestResult.TestStatus[] arr$ = TestResult.TestStatus.values();
      int len$ = arr$.length;

      for(int i$ = 0; i$ < len$; ++i$) {
         TestResult.TestStatus status = arr$[i$];
         int count = this.getNumTestsInState(status);
         if(count > 0) {
            builder.append(String.format("%s %d, ", new Object[]{status.toString().toLowerCase(), Integer.valueOf(count)}));
         }
      }

      return builder.toString();
   }
}
