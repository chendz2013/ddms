package com.android.ddmlib.testrunner;

import com.android.ddmlib.AdbCommandRejectedException;
import com.android.ddmlib.IShellEnabledDevice;
import com.android.ddmlib.Log;
import com.android.ddmlib.ShellCommandUnresponsiveException;
import com.android.ddmlib.TimeoutException;
import com.android.ddmlib.testrunner.IRemoteAndroidTestRunner;
import com.android.ddmlib.testrunner.ITestRunListener;
import com.android.ddmlib.testrunner.InstrumentationResultParser;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

public class RemoteAndroidTestRunner implements IRemoteAndroidTestRunner {
   private final String mPackageName;
   private final String mRunnerName;
   private IShellEnabledDevice mRemoteDevice;
   private long mMaxTimeToOutputResponse;
   private TimeUnit mMaxTimeUnits;
   private String mRunName;
   private Map<String, String> mArgMap;
   private InstrumentationResultParser mParser;
   private static final String LOG_TAG = "RemoteAndroidTest";
   private static final String DEFAULT_RUNNER_NAME = "android.test.InstrumentationTestRunner";
   private static final char CLASS_SEPARATOR = ',';
   private static final char METHOD_SEPARATOR = '#';
   private static final char RUNNER_SEPARATOR = '/';
   private static final String CLASS_ARG_NAME = "class";
   private static final String LOG_ARG_NAME = "log";
   private static final String DEBUG_ARG_NAME = "debug";
   private static final String COVERAGE_ARG_NAME = "coverage";
   private static final String PACKAGE_ARG_NAME = "package";
   private static final String SIZE_ARG_NAME = "size";
   private static final String DELAY_MSEC_ARG_NAME = "delay_msec";
   private String mRunOptions;
   private static final int TEST_COLLECTION_TIMEOUT = 120000;

   public RemoteAndroidTestRunner(String packageName, String runnerName, IShellEnabledDevice remoteDevice) {
      this.mMaxTimeToOutputResponse = 0L;
      this.mMaxTimeUnits = TimeUnit.MILLISECONDS;
      this.mRunName = null;
      this.mRunOptions = "";
      this.mPackageName = packageName;
      this.mRunnerName = runnerName;
      this.mRemoteDevice = remoteDevice;
      this.mArgMap = new Hashtable();
   }

   public RemoteAndroidTestRunner(String packageName, IShellEnabledDevice remoteDevice) {
      this(packageName, (String)null, remoteDevice);
   }

   public String getPackageName() {
      return this.mPackageName;
   }

   public String getRunnerName() {
      return this.mRunnerName == null?"android.test.InstrumentationTestRunner":this.mRunnerName;
   }

   private String getRunnerPath() {
      return this.getPackageName() + '/' + this.getRunnerName();
   }

   public void setClassName(String className) {
      this.addInstrumentationArg("class", className);
   }

   public void setClassNames(String[] classNames) {
      StringBuilder classArgBuilder = new StringBuilder();

      for(int i = 0; i < classNames.length; ++i) {
         if(i != 0) {
            classArgBuilder.append(',');
         }

         classArgBuilder.append(classNames[i]);
      }

      this.setClassName(classArgBuilder.toString());
   }

   public void setMethodName(String className, String testName) {
      this.setClassName(className + '#' + testName);
   }

   public void setTestPackageName(String packageName) {
      this.addInstrumentationArg("package", packageName);
   }

   public void addInstrumentationArg(String name, String value) {
      if(name != null && value != null) {
         this.mArgMap.put(name, value);
      } else {
         throw new IllegalArgumentException("name or value arguments cannot be null");
      }
   }

   public void removeInstrumentationArg(String name) {
      if(name == null) {
         throw new IllegalArgumentException("name argument cannot be null");
      } else {
         this.mArgMap.remove(name);
      }
   }

   public void addBooleanArg(String name, boolean value) {
      this.addInstrumentationArg(name, Boolean.toString(value));
   }

   public void setLogOnly(boolean logOnly) {
      this.addBooleanArg("log", logOnly);
   }

   public void setDebug(boolean debug) {
      this.addBooleanArg("debug", debug);
   }

   public void setCoverage(boolean coverage) {
      this.addBooleanArg("coverage", coverage);
   }

   public void setTestSize(IRemoteAndroidTestRunner.TestSize size) {
      this.addInstrumentationArg("size", size.getRunnerValue());
   }

   public void setTestCollection(boolean collect) {
      if(collect) {
         this.setLogOnly(true);
         this.setMaxTimeToOutputResponse(120000L, TimeUnit.MILLISECONDS);
         if(this.getApiLevel() < 16) {
            this.addInstrumentationArg("delay_msec", "15");
         }
      } else {
         this.setLogOnly(false);
         this.setMaxTimeToOutputResponse(this.mMaxTimeToOutputResponse, this.mMaxTimeUnits);
         if(this.getApiLevel() < 16) {
            this.removeInstrumentationArg("delay_msec");
         }
      }

   }

   private int getApiLevel() {
      try {
         return Integer.parseInt((String)this.mRemoteDevice.getSystemProperty("ro.build.version.sdk").get());
      } catch (Exception var2) {
         return -1;
      }
   }

   public void setMaxtimeToOutputResponse(int maxTimeToOutputResponse) {
      this.setMaxTimeToOutputResponse((long)maxTimeToOutputResponse, TimeUnit.MILLISECONDS);
   }

   public void setMaxTimeToOutputResponse(long maxTimeToOutputResponse, TimeUnit maxTimeUnits) {
      this.mMaxTimeToOutputResponse = maxTimeToOutputResponse;
      this.mMaxTimeUnits = maxTimeUnits;
   }

   public void setRunName(String runName) {
      this.mRunName = runName;
   }

   public void run(ITestRunListener... listeners) throws TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException, IOException {
      this.run((Collection)Arrays.asList(listeners));
   }

   public void run(Collection<ITestRunListener> listeners) throws TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException, IOException {
      String runCaseCommandStr = String.format("am instrument -w -r %1$s %2$s %3$s", new Object[]{this.getRunOptions(), this.getArgsCommand(), this.getRunnerPath()});
      Log.i("RemoteAndroidTest", String.format("Running %1$s on %2$s", new Object[]{runCaseCommandStr, this.mRemoteDevice.getName()}));
      String runName = this.mRunName == null?this.mPackageName:this.mRunName;
      this.mParser = new InstrumentationResultParser(runName, listeners);

      try {
         this.mRemoteDevice.executeShellCommand(runCaseCommandStr, this.mParser, this.mMaxTimeToOutputResponse, this.mMaxTimeUnits);
      } catch (IOException var5) {
         Log.w("RemoteAndroidTest", String.format("IOException %1$s when running tests %2$s on %3$s", new Object[]{var5.toString(), this.getPackageName(), this.mRemoteDevice.getName()}));
         this.mParser.handleTestRunFailed(var5.toString());
         throw var5;
      } catch (ShellCommandUnresponsiveException var6) {
         Log.w("RemoteAndroidTest", String.format("ShellCommandUnresponsiveException %1$s when running tests %2$s on %3$s", new Object[]{var6.toString(), this.getPackageName(), this.mRemoteDevice.getName()}));
         this.mParser.handleTestRunFailed(String.format("Failed to receive adb shell test output within %1$d ms. Test may have timed out, or adb connection to device became unresponsive", new Object[]{Long.valueOf(this.mMaxTimeToOutputResponse)}));
         throw var6;
      } catch (TimeoutException var7) {
         Log.w("RemoteAndroidTest", String.format("TimeoutException when running tests %1$s on %2$s", new Object[]{this.getPackageName(), this.mRemoteDevice.getName()}));
         this.mParser.handleTestRunFailed(var7.toString());
         throw var7;
      } catch (AdbCommandRejectedException var8) {
         Log.w("RemoteAndroidTest", String.format("AdbCommandRejectedException %1$s when running tests %2$s on %3$s", new Object[]{var8.toString(), this.getPackageName(), this.mRemoteDevice.getName()}));
         this.mParser.handleTestRunFailed(var8.toString());
         throw var8;
      }
   }

   public String getRunOptions() {
      return this.mRunOptions;
   }

   public void setRunOptions(String options) {
      this.mRunOptions = options;
   }

   public void cancel() {
      if(this.mParser != null) {
         this.mParser.cancel();
      }

   }

   private String getArgsCommand() {
      StringBuilder commandBuilder = new StringBuilder();
      Iterator i$ = this.mArgMap.entrySet().iterator();

      while(i$.hasNext()) {
         Entry argPair = (Entry)i$.next();
         String argCmd = String.format(" -e %1$s %2$s", new Object[]{argPair.getKey(), argPair.getValue()});
         commandBuilder.append(argCmd);
      }

      return commandBuilder.toString();
   }
}
