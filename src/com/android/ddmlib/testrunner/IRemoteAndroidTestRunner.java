package com.android.ddmlib.testrunner;

import com.android.ddmlib.AdbCommandRejectedException;
import com.android.ddmlib.ShellCommandUnresponsiveException;
import com.android.ddmlib.TimeoutException;
import com.android.ddmlib.testrunner.ITestRunListener;
import java.io.IOException;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

public interface IRemoteAndroidTestRunner {
   String getPackageName();

   String getRunnerName();

   void setClassName(String var1);

   void setClassNames(String[] var1);

   void setMethodName(String var1, String var2);

   void setTestPackageName(String var1);

   void setTestSize(IRemoteAndroidTestRunner.TestSize var1);

   void addInstrumentationArg(String var1, String var2);

   void removeInstrumentationArg(String var1);

   void addBooleanArg(String var1, boolean var2);

   void setLogOnly(boolean var1);

   void setDebug(boolean var1);

   void setCoverage(boolean var1);

   void setTestCollection(boolean var1);

   /** @deprecated */
   @Deprecated
   void setMaxtimeToOutputResponse(int var1);

   void setMaxTimeToOutputResponse(long var1, TimeUnit var3);

   void setRunName(String var1);

   void run(ITestRunListener... var1) throws TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException, IOException;

   void run(Collection<ITestRunListener> var1) throws TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException, IOException;

   void cancel();

   public static enum TestSize {
      SMALL("small"),
      MEDIUM("medium"),
      LARGE("large");

      private String mRunnerValue;

      private TestSize(String runnerValue) {
         this.mRunnerValue = runnerValue;
      }

      String getRunnerValue() {
         return this.mRunnerValue;
      }

      public static IRemoteAndroidTestRunner.TestSize getTestSize(String value) {
         StringBuilder msgBuilder = new StringBuilder("Unknown TestSize ");
         msgBuilder.append(value);
         msgBuilder.append(", Must be one of ");
         IRemoteAndroidTestRunner.TestSize[] arr$ = values();
         int len$ = arr$.length;

         for(int i$ = 0; i$ < len$; ++i$) {
            IRemoteAndroidTestRunner.TestSize size = arr$[i$];
            if(size.getRunnerValue().equals(value)) {
               return size;
            }

            msgBuilder.append(size.getRunnerValue());
            msgBuilder.append(", ");
         }

         throw new IllegalArgumentException(msgBuilder.toString());
      }
   }
}
