package com.android.ddmlib.testrunner;

import com.android.ddmlib.Log;
import com.android.ddmlib.MultiLineReceiver;
import com.android.ddmlib.testrunner.ITestRunListener;
import com.android.ddmlib.testrunner.TestIdentifier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InstrumentationResultParser extends MultiLineReceiver {
   private static final Set<String> KNOWN_KEYS = new HashSet();
   private final Collection<ITestRunListener> mTestListeners;
   private final String mTestRunName;
   private InstrumentationResultParser.TestResult mCurrentTestResult;
   private InstrumentationResultParser.TestResult mLastTestResult;
   private String mCurrentKey;
   private StringBuilder mCurrentValue;
   private boolean mTestStartReported;
   private boolean mTestRunFinished;
   private boolean mTestRunFailReported;
   private long mTestTime;
   private boolean mIsCancelled;
   private int mNumTestsRun;
   private int mNumTestsExpected;
   private boolean mInInstrumentationResultKey;
   private Map<String, String> mInstrumentationResultBundle;
   private Map<String, String> mTestMetrics;
   private static final String LOG_TAG = "InstrumentationResultParser";
   static final String NO_TEST_RESULTS_MSG = "No test results";
   static final String INCOMPLETE_TEST_ERR_MSG_PREFIX = "Test failed to run to completion";
   static final String INCOMPLETE_TEST_ERR_MSG_POSTFIX = "Check device logcat for details";
   static final String INCOMPLETE_RUN_ERR_MSG_PREFIX = "Test run failed to complete";

   public InstrumentationResultParser(String runName, Collection<ITestRunListener> listeners) {
      this.mCurrentTestResult = null;
      this.mLastTestResult = null;
      this.mCurrentKey = null;
      this.mCurrentValue = null;
      this.mTestStartReported = false;
      this.mTestRunFinished = false;
      this.mTestRunFailReported = false;
      this.mTestTime = 0L;
      this.mIsCancelled = false;
      this.mNumTestsRun = 0;
      this.mNumTestsExpected = 0;
      this.mInInstrumentationResultKey = false;
      this.mInstrumentationResultBundle = new HashMap();
      this.mTestMetrics = new HashMap();
      this.mTestRunName = runName;
      this.mTestListeners = new ArrayList(listeners);
   }

   public InstrumentationResultParser(String runName, ITestRunListener listener) {
      this(runName, (Collection)Collections.singletonList(listener));
   }

   public void processNewLines(String[] lines) {
      String[] arr$ = lines;
      int len$ = lines.length;

      for(int i$ = 0; i$ < len$; ++i$) {
         String line = arr$[i$];
         this.parse(line);
         Log.v("InstrumentationResultParser", line);
      }

   }

   private void parse(String line) {
      if(line.startsWith("INSTRUMENTATION_STATUS_CODE: ")) {
         this.submitCurrentKeyValue();
         this.mInInstrumentationResultKey = false;
         this.parseStatusCode(line);
      } else if(line.startsWith("INSTRUMENTATION_STATUS: ")) {
         this.submitCurrentKeyValue();
         this.mInInstrumentationResultKey = false;
         this.parseKey(line, "INSTRUMENTATION_STATUS: ".length());
      } else if(line.startsWith("INSTRUMENTATION_RESULT: ")) {
         this.submitCurrentKeyValue();
         this.mInInstrumentationResultKey = true;
         this.parseKey(line, "INSTRUMENTATION_RESULT: ".length());
      } else if(!line.startsWith("INSTRUMENTATION_FAILED: ") && !line.startsWith("INSTRUMENTATION_CODE: ")) {
         if(line.startsWith("Time: ")) {
            this.parseTime(line);
         } else if(this.mCurrentValue != null) {
            this.mCurrentValue.append("\r\n");
            this.mCurrentValue.append(line);
         } else if(!line.trim().isEmpty()) {
            Log.d("InstrumentationResultParser", "unrecognized line " + line);
         }
      } else {
         this.submitCurrentKeyValue();
         this.mInInstrumentationResultKey = false;
         this.mTestRunFinished = true;
      }

   }

   private void submitCurrentKeyValue() {
      if(this.mCurrentKey != null && this.mCurrentValue != null) {
         String statusValue = this.mCurrentValue.toString();
         if(this.mInInstrumentationResultKey) {
            if(!KNOWN_KEYS.contains(this.mCurrentKey)) {
               this.mInstrumentationResultBundle.put(this.mCurrentKey, statusValue);
            } else if(this.mCurrentKey.equals("shortMsg")) {
               this.handleTestRunFailed(String.format("Instrumentation run failed due to \'%1$s\'", new Object[]{statusValue}));
            }
         } else {
            InstrumentationResultParser.TestResult testInfo = this.getCurrentTestInfo();
            if(this.mCurrentKey.equals("class")) {
               testInfo.mTestClass = statusValue.trim();
            } else if(this.mCurrentKey.equals("test")) {
               testInfo.mTestName = statusValue.trim();
            } else if(this.mCurrentKey.equals("numtests")) {
               try {
                  testInfo.mNumTests = Integer.valueOf(Integer.parseInt(statusValue));
               } catch (NumberFormatException var4) {
                  Log.w("InstrumentationResultParser", "Unexpected integer number of tests, received " + statusValue);
               }
            } else if(this.mCurrentKey.equals("Error")) {
               this.handleTestRunFailed(statusValue);
            } else if(this.mCurrentKey.equals("stack")) {
               testInfo.mStackTrace = statusValue;
            } else if(!KNOWN_KEYS.contains(this.mCurrentKey)) {
               this.mTestMetrics.put(this.mCurrentKey, statusValue);
            }
         }

         this.mCurrentKey = null;
         this.mCurrentValue = null;
      }

   }

   private Map<String, String> getAndResetTestMetrics() {
      Map retVal = this.mTestMetrics;
      this.mTestMetrics = new HashMap();
      return retVal;
   }

   private InstrumentationResultParser.TestResult getCurrentTestInfo() {
      if(this.mCurrentTestResult == null) {
         this.mCurrentTestResult = new InstrumentationResultParser.TestResult();
      }

      return this.mCurrentTestResult;
   }

   private void clearCurrentTestInfo() {
      this.mLastTestResult = this.mCurrentTestResult;
      this.mCurrentTestResult = null;
   }

   private void parseKey(String line, int keyStartPos) {
      int endKeyPos = line.indexOf(61, keyStartPos);
      if(endKeyPos != -1) {
         this.mCurrentKey = line.substring(keyStartPos, endKeyPos).trim();
         this.parseValue(line, endKeyPos + 1);
      }

   }

   private void parseValue(String line, int valueStartPos) {
      this.mCurrentValue = new StringBuilder();
      this.mCurrentValue.append(line.substring(valueStartPos));
   }

   private void parseStatusCode(String line) {
      String value = line.substring("INSTRUMENTATION_STATUS_CODE: ".length()).trim();
      InstrumentationResultParser.TestResult testInfo = this.getCurrentTestInfo();
      testInfo.mCode = Integer.valueOf(-1);

      try {
         testInfo.mCode = Integer.valueOf(Integer.parseInt(value));
      } catch (NumberFormatException var5) {
         Log.w("InstrumentationResultParser", "Expected integer status code, received: " + value);
         testInfo.mCode = Integer.valueOf(-1);
      }

      if(testInfo.mCode.intValue() != 2) {
         this.reportResult(testInfo);
         this.clearCurrentTestInfo();
      }

   }

   public boolean isCancelled() {
      return this.mIsCancelled;
   }

   public void cancel() {
      this.mIsCancelled = true;
   }

   private void reportResult(InstrumentationResultParser.TestResult testInfo) {
      if(!testInfo.isComplete()) {
         Log.w("InstrumentationResultParser", "invalid instrumentation status bundle " + testInfo.toString());
      } else {
         this.reportTestRunStarted(testInfo);
         TestIdentifier testId = new TestIdentifier(testInfo.mTestClass, testInfo.mTestName);
         Map metrics;
         Iterator i$;
         ITestRunListener listener;
         switch(testInfo.mCode.intValue()) {
         case -4:
            metrics = this.getAndResetTestMetrics();
            i$ = this.mTestListeners.iterator();

            while(i$.hasNext()) {
               listener = (ITestRunListener)i$.next();
               listener.testAssumptionFailure(testId, this.getTrace(testInfo));
               listener.testEnded(testId, metrics);
            }

            ++this.mNumTestsRun;
            break;
         case -3:
            metrics = this.getAndResetTestMetrics();
            i$ = this.mTestListeners.iterator();

            while(i$.hasNext()) {
               listener = (ITestRunListener)i$.next();
               listener.testStarted(testId);
               listener.testIgnored(testId);
               listener.testEnded(testId, metrics);
            }

            ++this.mNumTestsRun;
            break;
         case -2:
            metrics = this.getAndResetTestMetrics();
            i$ = this.mTestListeners.iterator();

            while(i$.hasNext()) {
               listener = (ITestRunListener)i$.next();
               listener.testFailed(testId, this.getTrace(testInfo));
               listener.testEnded(testId, metrics);
            }

            ++this.mNumTestsRun;
            break;
         case -1:
            metrics = this.getAndResetTestMetrics();
            i$ = this.mTestListeners.iterator();

            while(i$.hasNext()) {
               listener = (ITestRunListener)i$.next();
               listener.testFailed(testId, this.getTrace(testInfo));
               listener.testEnded(testId, metrics);
            }

            ++this.mNumTestsRun;
            break;
         case 0:
            metrics = this.getAndResetTestMetrics();
            i$ = this.mTestListeners.iterator();

            while(i$.hasNext()) {
               listener = (ITestRunListener)i$.next();
               listener.testEnded(testId, metrics);
            }

            ++this.mNumTestsRun;
            break;
         case 1:
            i$ = this.mTestListeners.iterator();

            while(i$.hasNext()) {
               listener = (ITestRunListener)i$.next();
               listener.testStarted(testId);
            }

            return;
         default:
            metrics = this.getAndResetTestMetrics();
            Log.e("InstrumentationResultParser", "Unknown status code received: " + testInfo.mCode);
            i$ = this.mTestListeners.iterator();

            while(i$.hasNext()) {
               listener = (ITestRunListener)i$.next();
               listener.testEnded(testId, metrics);
            }

            ++this.mNumTestsRun;
         }

      }
   }

   private void reportTestRunStarted(InstrumentationResultParser.TestResult testInfo) {
      if(!this.mTestStartReported && testInfo.mNumTests != null) {
         Iterator i$ = this.mTestListeners.iterator();

         while(i$.hasNext()) {
            ITestRunListener listener = (ITestRunListener)i$.next();
            listener.testRunStarted(this.mTestRunName, testInfo.mNumTests.intValue());
         }

         this.mNumTestsExpected = testInfo.mNumTests.intValue();
         this.mTestStartReported = true;
      }

   }

   private String getTrace(InstrumentationResultParser.TestResult testInfo) {
      if(testInfo.mStackTrace != null) {
         return testInfo.mStackTrace;
      } else {
         Log.e("InstrumentationResultParser", "Could not find stack trace for failed test ");
         return (new Throwable("Unknown failure")).toString();
      }
   }

   private void parseTime(String line) {
      Pattern timePattern = Pattern.compile(String.format("%s\\s*([\\d\\.]+)", new Object[]{"Time: "}));
      Matcher timeMatcher = timePattern.matcher(line);
      if(timeMatcher.find()) {
         String timeString = timeMatcher.group(1);

         try {
            float e = Float.parseFloat(timeString);
            this.mTestTime = (long)(e * 1000.0F);
         } catch (NumberFormatException var6) {
            Log.w("InstrumentationResultParser", String.format("Unexpected time format %1$s", new Object[]{line}));
         }
      } else {
         Log.w("InstrumentationResultParser", String.format("Unexpected time format %1$s", new Object[]{line}));
      }

   }

   public void handleTestRunFailed(String errorMsg) {
      errorMsg = errorMsg == null?"Unknown error":errorMsg;
      Log.i("InstrumentationResultParser", String.format("test run failed: \'%1$s\'", new Object[]{errorMsg}));
      if(this.mLastTestResult != null && this.mLastTestResult.isComplete() && 1 == this.mLastTestResult.mCode.intValue()) {
         TestIdentifier i$ = new TestIdentifier(this.mLastTestResult.mTestClass, this.mLastTestResult.mTestName);
         Iterator listener = this.mTestListeners.iterator();

         while(listener.hasNext()) {
            ITestRunListener listener1 = (ITestRunListener)listener.next();
            listener1.testFailed(i$, String.format("%1$s. Reason: \'%2$s\'. %3$s", new Object[]{"Test failed to run to completion", errorMsg, "Check device logcat for details"}));
            listener1.testEnded(i$, this.getAndResetTestMetrics());
         }
      }

      Iterator i$1 = this.mTestListeners.iterator();

      while(i$1.hasNext()) {
         ITestRunListener listener2 = (ITestRunListener)i$1.next();
         if(!this.mTestStartReported) {
            listener2.testRunStarted(this.mTestRunName, 0);
         }

         listener2.testRunFailed(errorMsg);
         listener2.testRunEnded(this.mTestTime, this.mInstrumentationResultBundle);
      }

      this.mTestStartReported = true;
      this.mTestRunFailReported = true;
   }

   public void done() {
      super.done();
      if(!this.mTestRunFailReported) {
         this.handleOutputDone();
      }

   }

   private void handleOutputDone() {
      if(!this.mTestStartReported && !this.mTestRunFinished) {
         this.handleTestRunFailed("No test results");
      } else {
         ITestRunListener listener;
         if(this.mNumTestsExpected > this.mNumTestsRun) {
            String i$ = String.format("%1$s. Expected %2$d tests, received %3$d", new Object[]{"Test run failed to complete", Integer.valueOf(this.mNumTestsExpected), Integer.valueOf(this.mNumTestsRun)});
            this.handleTestRunFailed(i$);
         } else {
            for(Iterator i$1 = this.mTestListeners.iterator(); i$1.hasNext(); listener.testRunEnded(this.mTestTime, this.mInstrumentationResultBundle)) {
               listener = (ITestRunListener)i$1.next();
               if(!this.mTestStartReported) {
                  listener.testRunStarted(this.mTestRunName, 0);
               }
            }
         }
      }

   }

   static {
      KNOWN_KEYS.add("test");
      KNOWN_KEYS.add("class");
      KNOWN_KEYS.add("stack");
      KNOWN_KEYS.add("numtests");
      KNOWN_KEYS.add("Error");
      KNOWN_KEYS.add("shortMsg");
      KNOWN_KEYS.add("stream");
      KNOWN_KEYS.add("id");
      KNOWN_KEYS.add("current");
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
   }

   private static class TestResult {
      private Integer mCode;
      private String mTestName;
      private String mTestClass;
      private String mStackTrace;
      private Integer mNumTests;

      private TestResult() {
         this.mCode = null;
         this.mTestName = null;
         this.mTestClass = null;
         this.mStackTrace = null;
         this.mNumTests = null;
      }

      boolean isComplete() {
         return this.mCode != null && this.mTestName != null && this.mTestClass != null;
      }

      public String toString() {
         StringBuilder output = new StringBuilder();
         if(this.mTestClass != null) {
            output.append(this.mTestClass);
            output.append('#');
         }

         if(this.mTestName != null) {
            output.append(this.mTestName);
         }

         return output.length() > 0?output.toString():"unknown result";
      }

      // $FF: synthetic method
      TestResult(InstrumentationResultParser.SyntheticClass_1 x0) {
         this();
      }
   }

   private static class Prefixes {
      private static final String STATUS = "INSTRUMENTATION_STATUS: ";
      private static final String STATUS_CODE = "INSTRUMENTATION_STATUS_CODE: ";
      private static final String STATUS_FAILED = "INSTRUMENTATION_FAILED: ";
      private static final String CODE = "INSTRUMENTATION_CODE: ";
      private static final String RESULT = "INSTRUMENTATION_RESULT: ";
      private static final String TIME_REPORT = "Time: ";
   }

   private static class StatusCodes {
      private static final int START = 1;
      private static final int IN_PROGRESS = 2;
      private static final int ASSUMPTION_FAILURE = -4;
      private static final int IGNORED = -3;
      private static final int FAILURE = -2;
      private static final int ERROR = -1;
      private static final int OK = 0;
   }

   private static class StatusKeys {
      private static final String TEST = "test";
      private static final String CLASS = "class";
      private static final String STACK = "stack";
      private static final String NUMTESTS = "numtests";
      private static final String ERROR = "Error";
      private static final String SHORTMSG = "shortMsg";
   }
}
