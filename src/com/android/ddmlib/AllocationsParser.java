package com.android.ddmlib;

import com.android.ddmlib.AllocationInfo;
import com.android.ddmlib.ByteBufferUtil;
import java.nio.ByteBuffer;

public class AllocationsParser {
   private static String descriptorToDot(String str) {
      int array;
      for(array = 0; str.startsWith("["); ++array) {
         str = str.substring(1);
      }

      int len = str.length();
      if(len >= 2 && str.charAt(0) == 76 && str.charAt(len - 1) == 59) {
         str = str.substring(1, len - 1);
         str = str.replace('/', '.');
      } else if("C".equals(str)) {
         str = "char";
      } else if("B".equals(str)) {
         str = "byte";
      } else if("Z".equals(str)) {
         str = "boolean";
      } else if("S".equals(str)) {
         str = "short";
      } else if("I".equals(str)) {
         str = "int";
      } else if("J".equals(str)) {
         str = "long";
      } else if("F".equals(str)) {
         str = "float";
      } else if("D".equals(str)) {
         str = "double";
      }

      for(int a = 0; a < array; ++a) {
         str = str + "[]";
      }

      return str;
   }

   private static void readStringTable(ByteBuffer data, String[] strings) {
      int count = strings.length;

      for(int i = 0; i < count; ++i) {
         int nameLen = data.getInt();
         String descriptor = ByteBufferUtil.getString(data, nameLen);
         strings[i] = descriptorToDot(descriptor);
      }

   }

   public static AllocationInfo[] parse(ByteBuffer data) {
      int messageHdrLen = data.get() & 255;
      int entryHdrLen = data.get() & 255;
      int stackFrameLen = data.get() & 255;
      int numEntries = data.getShort() & '\uffff';
      int offsetToStrings = data.getInt();
      int numClassNames = data.getShort() & '\uffff';
      int numMethodNames = data.getShort() & '\uffff';
      int numFileNames = data.getShort() & '\uffff';
      data.position(offsetToStrings);
      String[] classNames = new String[numClassNames];
      String[] methodNames = new String[numMethodNames];
      String[] fileNames = new String[numFileNames];
      readStringTable(data, classNames);
      readStringTable(data, methodNames);
      readStringTable(data, fileNames);
      data.position(messageHdrLen);
      AllocationInfo[] allocations = new AllocationInfo[numEntries];

      for(int i = 0; i < numEntries; ++i) {
         int totalSize = data.getInt();
         int threadId = data.getShort() & '\uffff';
         int classNameIndex = data.getShort() & '\uffff';
         int stackDepth = data.get() & 255;

         for(int steArray = 9; steArray < entryHdrLen; ++steArray) {
            data.get();
         }

         StackTraceElement[] var28 = new StackTraceElement[stackDepth];

         for(int sti = 0; sti < stackDepth; ++sti) {
            int methodClassNameIndex = data.getShort() & '\uffff';
            int methodNameIndex = data.getShort() & '\uffff';
            int methodSourceFileIndex = data.getShort() & '\uffff';
            short lineNumber = data.getShort();
            String methodClassName = classNames[methodClassNameIndex];
            String methodName = methodNames[methodNameIndex];
            String methodSourceFile = fileNames[methodSourceFileIndex];
            var28[sti] = new StackTraceElement(methodClassName, methodName, methodSourceFile, lineNumber);

            for(int skip = 8; skip < stackFrameLen; ++skip) {
               data.get();
            }
         }

         allocations[i] = new AllocationInfo(numEntries - i, classNames[classNameIndex], totalSize, (short)threadId, var28);
      }

      return allocations;
   }
}
