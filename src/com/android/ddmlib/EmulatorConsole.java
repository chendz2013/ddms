package com.android.ddmlib;

import com.android.ddmlib.AdbHelper;
import com.android.ddmlib.DdmPreferences;
import com.android.ddmlib.IDevice;
import com.android.ddmlib.Log;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.security.InvalidParameterException;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class EmulatorConsole {
   private static final String DEFAULT_ENCODING = "ISO-8859-1";
   private static final int WAIT_TIME = 5;
   private static final int STD_TIMEOUT = 5000;
   private static final String HOST = "127.0.0.1";
   private static final String COMMAND_PING = "help\r\n";
   private static final String COMMAND_AVD_NAME = "avd name\r\n";
   private static final String COMMAND_KILL = "kill\r\n";
   private static final String COMMAND_GSM_STATUS = "gsm status\r\n";
   private static final String COMMAND_GSM_CALL = "gsm call %1$s\r\n";
   private static final String COMMAND_GSM_CANCEL_CALL = "gsm cancel %1$s\r\n";
   private static final String COMMAND_GSM_DATA = "gsm data %1$s\r\n";
   private static final String COMMAND_GSM_VOICE = "gsm voice %1$s\r\n";
   private static final String COMMAND_SMS_SEND = "sms send %1$s %2$s\r\n";
   private static final String COMMAND_NETWORK_STATUS = "network status\r\n";
   private static final String COMMAND_NETWORK_SPEED = "network speed %1$s\r\n";
   private static final String COMMAND_NETWORK_LATENCY = "network delay %1$s\r\n";
   private static final String COMMAND_GPS = "geo fix %1$f %2$f %3$f\r\n";
   private static final Pattern RE_KO = Pattern.compile("KO:\\s+(.*)");
   public static final int[] MIN_LATENCIES = new int[]{0, 150, 80, 35};
   public static final int[] DOWNLOAD_SPEEDS = new int[]{0, 14400, 'ꣀ', 80000, 236800, 1920000, 14400000};
   public static final String[] NETWORK_SPEEDS = new String[]{"full", "gsm", "hscsd", "gprs", "edge", "umts", "hsdpa"};
   public static final String[] NETWORK_LATENCIES = new String[]{"none", "gprs", "edge", "umts"};
   public static final String RESULT_OK = null;
   private static final Pattern sEmulatorRegexp = Pattern.compile("emulator-(\\d+)");
   private static final Pattern sVoiceStatusRegexp = Pattern.compile("gsm\\s+voice\\s+state:\\s*([a-z]+)", 2);
   private static final Pattern sDataStatusRegexp = Pattern.compile("gsm\\s+data\\s+state:\\s*([a-z]+)", 2);
   private static final Pattern sDownloadSpeedRegexp = Pattern.compile("\\s+download\\s+speed:\\s+(\\d+)\\s+bits.*", 2);
   private static final Pattern sMinLatencyRegexp = Pattern.compile("\\s+minimum\\s+latency:\\s+(\\d+)\\s+ms", 2);
   private static final HashMap<Integer, EmulatorConsole> sEmulators = new HashMap();
   private static final String LOG_TAG = "EmulatorConsole";
   private int mPort = -1;
   private SocketChannel mSocketChannel;
   private byte[] mBuffer = new byte[1024];

   /**
    * 读取模拟器的端口
    * @param d
    * @return
     */
   public static EmulatorConsole getConsole(IDevice d) {
      Integer port = getEmulatorPort(d.getSerialNumber());
      if(port == null) {
         Log.w("EmulatorConsole", "Failed to find emulator port from serial: " + d.getSerialNumber());
         return null;
      } else {
         EmulatorConsole console = retrieveConsole(port.intValue());
         if(!console.checkConnection()) {
            removeConsole(console.mPort);
            console = null;
         }

         return console;
      }
   }

   public static Integer getEmulatorPort(String serialNumber) {
      Matcher m = sEmulatorRegexp.matcher(serialNumber);
      if(m.matches()) {
         try {
            int port = Integer.parseInt(m.group(1));
            if(port > 0) {
               return Integer.valueOf(port);
            }
         } catch (NumberFormatException var4) {
            ;
         }
      }

      return null;
   }

   private static EmulatorConsole retrieveConsole(int port) {
      HashMap var1 = sEmulators;
      synchronized(sEmulators) {
         EmulatorConsole console = (EmulatorConsole)sEmulators.get(Integer.valueOf(port));
         if(console == null) {
            Log.v("EmulatorConsole", "Creating emulator console for " + Integer.toString(port));
            console = new EmulatorConsole(port);
            sEmulators.put(Integer.valueOf(port), console);
         }

         return console;
      }
   }

   private static void removeConsole(int port) {
      HashMap var1 = sEmulators;
      synchronized(sEmulators) {
         Log.v("EmulatorConsole", "Removing emulator console for " + Integer.toString(port));
         sEmulators.remove(Integer.valueOf(port));
      }
   }

   private EmulatorConsole(int port) {
      this.mPort = port;
   }

   private synchronized boolean checkConnection() {
      if(this.mSocketChannel == null) {
         try {
            InetAddress e = InetAddress.getByName("127.0.0.1");
            InetSocketAddress socketAddr = new InetSocketAddress(e, this.mPort);
            this.mSocketChannel = SocketChannel.open(socketAddr);
            this.mSocketChannel.configureBlocking(false);
            this.readLines();
         } catch (IOException var3) {
            Log.w("EmulatorConsole", "Failed to start Emulator console for " + Integer.toString(this.mPort));
            return false;
         }
      }

      return this.ping();
   }

   private synchronized boolean ping() {
      return this.sendCommand("help\r\n")?this.readLines() != null:false;
   }

   public synchronized void kill() {
      if(this.sendCommand("kill\r\n")) {
         this.close();
      }

   }

   public synchronized void close() {
      if(this.mPort != -1) {
         removeConsole(this.mPort);

         try {
            if(this.mSocketChannel != null) {
               this.mSocketChannel.close();
            }

            this.mSocketChannel = null;
            this.mPort = -1;
         } catch (IOException var2) {
            Log.w("EmulatorConsole", "Failed to close EmulatorConsole channel");
         }

      }
   }

   public synchronized String getAvdName() {
      if(this.sendCommand("avd name\r\n")) {
         String[] result = this.readLines();
         if(result != null && result.length == 2) {
            return result[0];
         }

         Matcher m = RE_KO.matcher(result[result.length - 1]);
         if(m.matches()) {
            return m.group(1);
         }

         Log.w("EmulatorConsole", "avd name result did not match expected");

         for(int i = 0; i < result.length; ++i) {
            Log.d("EmulatorConsole", result[i]);
         }
      }

      return null;
   }

   public synchronized EmulatorConsole.NetworkStatus getNetworkStatus() {
      if(this.sendCommand("network status\r\n")) {
         String[] result = this.readLines();
         if(this.isValid(result)) {
            EmulatorConsole.NetworkStatus status = new EmulatorConsole.NetworkStatus();
            String[] arr$ = result;
            int len$ = result.length;

            for(int i$ = 0; i$ < len$; ++i$) {
               String line = arr$[i$];
               Matcher m = sDownloadSpeedRegexp.matcher(line);
               String value;
               if(m.matches()) {
                  value = m.group(1);
                  status.speed = this.getSpeedIndex(value);
               } else {
                  m = sMinLatencyRegexp.matcher(line);
                  if(m.matches()) {
                     value = m.group(1);
                     status.latency = this.getLatencyIndex(value);
                  }
               }
            }

            return status;
         }
      }

      return null;
   }

   public synchronized EmulatorConsole.GsmStatus getGsmStatus() {
      if(this.sendCommand("gsm status\r\n")) {
         String[] result = this.readLines();
         if(this.isValid(result)) {
            EmulatorConsole.GsmStatus status = new EmulatorConsole.GsmStatus();
            String[] arr$ = result;
            int len$ = result.length;

            for(int i$ = 0; i$ < len$; ++i$) {
               String line = arr$[i$];
               Matcher m = sVoiceStatusRegexp.matcher(line);
               String value;
               if(m.matches()) {
                  value = m.group(1);
                  status.voice = EmulatorConsole.GsmMode.getEnum(value.toLowerCase(Locale.US));
               } else {
                  m = sDataStatusRegexp.matcher(line);
                  if(m.matches()) {
                     value = m.group(1);
                     status.data = EmulatorConsole.GsmMode.getEnum(value.toLowerCase(Locale.US));
                  }
               }
            }

            return status;
         }
      }

      return null;
   }

   public synchronized String setGsmVoiceMode(EmulatorConsole.GsmMode mode) throws InvalidParameterException {
      if(mode == EmulatorConsole.GsmMode.UNKNOWN) {
         throw new InvalidParameterException();
      } else {
         String command = String.format("gsm voice %1$s\r\n", new Object[]{mode.getTag()});
         return this.processCommand(command);
      }
   }

   public synchronized String setGsmDataMode(EmulatorConsole.GsmMode mode) throws InvalidParameterException {
      if(mode == EmulatorConsole.GsmMode.UNKNOWN) {
         throw new InvalidParameterException();
      } else {
         String command = String.format("gsm data %1$s\r\n", new Object[]{mode.getTag()});
         return this.processCommand(command);
      }
   }

   public synchronized String call(String number) {
      String command = String.format("gsm call %1$s\r\n", new Object[]{number});
      return this.processCommand(command);
   }

   public synchronized String cancelCall(String number) {
      String command = String.format("gsm cancel %1$s\r\n", new Object[]{number});
      return this.processCommand(command);
   }

   public synchronized String sendSms(String number, String message) {
      String command = String.format("sms send %1$s %2$s\r\n", new Object[]{number, message});
      return this.processCommand(command);
   }

   public synchronized String setNetworkSpeed(int selectionIndex) {
      String command = String.format("network speed %1$s\r\n", new Object[]{NETWORK_SPEEDS[selectionIndex]});
      return this.processCommand(command);
   }

   public synchronized String setNetworkLatency(int selectionIndex) {
      String command = String.format("network delay %1$s\r\n", new Object[]{NETWORK_LATENCIES[selectionIndex]});
      return this.processCommand(command);
   }

   public synchronized String sendLocation(double longitude, double latitude, double elevation) {
      Formatter formatter = new Formatter(Locale.US);

      String var8;
      try {
         formatter.format("geo fix %1$f %2$f %3$f\r\n", new Object[]{Double.valueOf(longitude), Double.valueOf(latitude), Double.valueOf(elevation)});
         var8 = this.processCommand(formatter.toString());
      } finally {
         formatter.close();
      }

      return var8;
   }

   private boolean sendCommand(String command) {
      boolean result = false;

      try {
         byte[] e;
         try {
            e = command.getBytes("ISO-8859-1");
         } catch (UnsupportedEncodingException var10) {
            Log.w("EmulatorConsole", "wrong encoding when sending " + command + " to " + Integer.toString(this.mPort));
            boolean var5 = result;
            return var5;
         }

         AdbHelper.write(this.mSocketChannel, e, e.length, DdmPreferences.getTimeOut());
         result = true;
         return result;
      } catch (Exception var11) {
         Log.d("EmulatorConsole", "Exception sending command " + command + " to " + Integer.toString(this.mPort));
         boolean e1 = false;
         return e1;
      } finally {
         if(!result) {
            removeConsole(this.mPort);
         }

      }
   }

   private String processCommand(String command) {
      if(this.sendCommand(command)) {
         String[] result = this.readLines();
         if(result != null && result.length > 0) {
            Matcher m = RE_KO.matcher(result[result.length - 1]);
            return m.matches()?m.group(1):RESULT_OK;
         } else {
            return "Unable to communicate with the emulator";
         }
      } else {
         return "Unable to send command to the emulator";
      }
   }

   private String[] readLines() {
      try {
         ByteBuffer e = ByteBuffer.wrap(this.mBuffer, 0, this.mBuffer.length);
         int numWaits = 0;
         boolean stop = false;

         while(e.position() != e.limit() && !stop) {
            int msg = this.mSocketChannel.read(e);
            if(msg < 0) {
               return null;
            }

            if(msg == 0) {
               if(numWaits * 5 > 5000) {
                  return null;
               }

               try {
                  Thread.sleep(5L);
               } catch (InterruptedException var6) {
                  ;
               }

               ++numWaits;
            } else {
               numWaits = 0;
            }

            if(e.position() >= 4) {
               int pos = e.position();
               if(this.endsWithOK(pos) || this.lastLineIsKO(pos)) {
                  stop = true;
               }
            }
         }

         String var8 = new String(this.mBuffer, 0, e.position(), "ISO-8859-1");
         return var8.split("\r\n");
      } catch (IOException var7) {
         Log.d("EmulatorConsole", "Exception reading lines for " + Integer.toString(this.mPort));
         return null;
      }
   }

   private boolean endsWithOK(int currentPosition) {
      return this.mBuffer[currentPosition - 1] == 10 && this.mBuffer[currentPosition - 2] == 13 && this.mBuffer[currentPosition - 3] == 75 && this.mBuffer[currentPosition - 4] == 79;
   }

   private boolean lastLineIsKO(int currentPosition) {
      if(this.mBuffer[currentPosition - 1] == 10 && this.mBuffer[currentPosition - 2] == 13) {
         boolean i = false;

         int var3;
         for(var3 = currentPosition - 3; var3 >= 0 && (this.mBuffer[var3] != 10 || var3 <= 0 || this.mBuffer[var3 - 1] != 13); --var3) {
            ;
         }

         return this.mBuffer[var3 + 1] == 75 && this.mBuffer[var3 + 2] == 79;
      } else {
         return false;
      }
   }

   private boolean isValid(String[] result) {
      return result != null && result.length > 0?!RE_KO.matcher(result[result.length - 1]).matches():false;
   }

   private int getLatencyIndex(String value) {
      try {
         int e = Integer.parseInt(value);

         for(int i = 0; i < MIN_LATENCIES.length; ++i) {
            if(MIN_LATENCIES[i] == e) {
               return i;
            }
         }
      } catch (NumberFormatException var4) {
         ;
      }

      return -1;
   }

   private int getSpeedIndex(String value) {
      try {
         int e = Integer.parseInt(value);

         for(int i = 0; i < DOWNLOAD_SPEEDS.length; ++i) {
            if(DOWNLOAD_SPEEDS[i] == e) {
               return i;
            }
         }
      } catch (NumberFormatException var4) {
         ;
      }

      return -1;
   }

   public static class NetworkStatus {
      public int speed = -1;
      public int latency = -1;
   }

   public static class GsmStatus {
      public EmulatorConsole.GsmMode voice;
      public EmulatorConsole.GsmMode data;

      public GsmStatus() {
         this.voice = EmulatorConsole.GsmMode.UNKNOWN;
         this.data = EmulatorConsole.GsmMode.UNKNOWN;
      }
   }

   public static enum GsmMode {
      UNKNOWN((String)null),
      UNREGISTERED(new String[]{"unregistered", "off"}),
      HOME(new String[]{"home", "on"}),
      ROAMING("roaming"),
      SEARCHING("searching"),
      DENIED("denied");

      private final String[] tags;

      private GsmMode(String tag) {
         if(tag != null) {
            this.tags = new String[]{tag};
         } else {
            this.tags = new String[0];
         }

      }

      private GsmMode(String[] tags) {
         this.tags = tags;
      }

      public static EmulatorConsole.GsmMode getEnum(String tag) {
         EmulatorConsole.GsmMode[] arr$ = values();
         int len$ = arr$.length;

         for(int i$ = 0; i$ < len$; ++i$) {
            EmulatorConsole.GsmMode mode = arr$[i$];
            String[] arr$1 = mode.tags;
            int len$1 = arr$1.length;

            for(int i$1 = 0; i$1 < len$1; ++i$1) {
               String t = arr$1[i$1];
               if(t.equals(tag)) {
                  return mode;
               }
            }
         }

         return UNKNOWN;
      }

      public String getTag() {
         return this.tags.length > 0?this.tags[0]:null;
      }
   }
}
