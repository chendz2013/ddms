package com.android.ddmlib;

import com.android.ddmlib.AdbVersion;
import com.android.ddmlib.Client;
import com.android.ddmlib.DdmPreferences;
import com.android.ddmlib.DeviceMonitor;
import com.android.ddmlib.HandleAppName;
import com.android.ddmlib.HandleHeap;
import com.android.ddmlib.HandleHello;
import com.android.ddmlib.HandleNativeHeap;
import com.android.ddmlib.HandleProfiling;
import com.android.ddmlib.HandleTest;
import com.android.ddmlib.HandleThread;
import com.android.ddmlib.HandleViewDebug;
import com.android.ddmlib.HandleWait;
import com.android.ddmlib.IDevice;
import com.android.ddmlib.Log;
import com.android.ddmlib.MonitorThread;
import com.google.common.base.Joiner;
import com.google.common.base.Throwables;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.SettableFuture;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.Thread.State;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * adb.exe的封装
 * adb的默认服务器是127.0.0.1:5037
 */
public final class AndroidDebugBridge {
   private static final AdbVersion MIN_ADB_VERSION = AdbVersion.parseFrom("1.0.20");
   private static final String ADB = "adb";
   private static final String DDMS = "ddms";
   private static final String SERVER_PORT_ENV_VAR = "ANDROID_ADB_SERVER_PORT";
   static final String DEFAULT_ADB_HOST = "127.0.0.1";
   static final int DEFAULT_ADB_PORT = 5037;
   private static int sAdbServerPort = 0;
   private static InetAddress sHostAddr;
   private static InetSocketAddress sSocketAddr;
   private static AndroidDebugBridge sThis;
   private static boolean sInitialized = false;
   private static boolean sClientSupport;
   private String mAdbOsLocation = null;
   private boolean mVersionCheck;
   private boolean mStarted = false;
   private DeviceMonitor mDeviceMonitor;
   private static final ArrayList<AndroidDebugBridge.IDebugBridgeChangeListener> sBridgeListeners = new ArrayList();
   private static final ArrayList<AndroidDebugBridge.IDeviceChangeListener> sDeviceListeners = new ArrayList();
   private static final ArrayList<AndroidDebugBridge.IClientChangeListener> sClientListeners = new ArrayList();
   private static final Object sLock;

   public static synchronized void initIfNeeded(boolean clientSupport) {
      if(!sInitialized) {
         init(clientSupport);
      }
   }

   public static synchronized void init(boolean clientSupport) {
      if(sInitialized) {
         throw new IllegalStateException("AndroidDebugBridge.init() has already been called.");
      } else {
         sInitialized = true;
         sClientSupport = clientSupport;
         //初始化socket地址
         initAdbSocketAddr();
         MonitorThread monitorThread = MonitorThread.createInstance();
         monitorThread.start();
         //添加各种HandleXxxx命令
         HandleHello.register(monitorThread);
         HandleAppName.register(monitorThread);
         HandleTest.register(monitorThread);
         HandleThread.register(monitorThread);
         HandleHeap.register(monitorThread);
         HandleWait.register(monitorThread);
         HandleProfiling.register(monitorThread);
         HandleNativeHeap.register(monitorThread);
         HandleViewDebug.register(monitorThread);
      }
   }

   public static synchronized void terminate() {
      if(sThis != null && sThis.mDeviceMonitor != null) {
         sThis.mDeviceMonitor.stop();
         sThis.mDeviceMonitor = null;
      }

      MonitorThread monitorThread = MonitorThread.getInstance();
      if(monitorThread != null) {
         monitorThread.quit();
      }

      sInitialized = false;
   }

   static boolean getClientSupport() {
      return sClientSupport;
   }

   public static InetSocketAddress getSocketAddress() {
      return sSocketAddr;
   }

   public static AndroidDebugBridge createBridge() {
      Object var0 = sLock;
      synchronized(sLock) {
         if(sThis != null) {
            return sThis;
         } else {
            try {
               sThis = new AndroidDebugBridge();
               sThis.start();
            } catch (InvalidParameterException var9) {
               sThis = null;
            }

            AndroidDebugBridge.IDebugBridgeChangeListener[] listenersCopy = (AndroidDebugBridge.IDebugBridgeChangeListener[])sBridgeListeners.toArray(new AndroidDebugBridge.IDebugBridgeChangeListener[sBridgeListeners.size()]);
            AndroidDebugBridge.IDebugBridgeChangeListener[] arr$ = listenersCopy;
            int len$ = listenersCopy.length;

            for(int i$ = 0; i$ < len$; ++i$) {
               AndroidDebugBridge.IDebugBridgeChangeListener listener = arr$[i$];

               try {
                  listener.bridgeChanged(sThis);
               } catch (Exception var8) {
                  Log.e("ddms", (Throwable)var8);
               }
            }

            return sThis;
         }
      }
   }

   public static AndroidDebugBridge createBridge(String osLocation, boolean forceNewBridge) {
      Object var2 = sLock;
      synchronized(sLock) {
         if(sThis != null) {
            if(sThis.mAdbOsLocation != null && sThis.mAdbOsLocation.equals(osLocation) && !forceNewBridge) {
               return sThis;
            }

            sThis.stop();
         }

         try {
            sThis = new AndroidDebugBridge(osLocation);
            sThis.start();
         } catch (InvalidParameterException var11) {
            sThis = null;
         }

         AndroidDebugBridge.IDebugBridgeChangeListener[] listenersCopy = (AndroidDebugBridge.IDebugBridgeChangeListener[])sBridgeListeners.toArray(new AndroidDebugBridge.IDebugBridgeChangeListener[sBridgeListeners.size()]);
         AndroidDebugBridge.IDebugBridgeChangeListener[] arr$ = listenersCopy;
         int len$ = listenersCopy.length;

         for(int i$ = 0; i$ < len$; ++i$) {
            AndroidDebugBridge.IDebugBridgeChangeListener listener = arr$[i$];

            try {
               listener.bridgeChanged(sThis);
            } catch (Exception var10) {
               Log.e("ddms", (Throwable)var10);
            }
         }

         return sThis;
      }
   }

   public static AndroidDebugBridge getBridge() {
      return sThis;
   }

   public static void disconnectBridge() {
      Object var0 = sLock;
      synchronized(sLock) {
         if(sThis != null) {
            sThis.stop();
            sThis = null;
            AndroidDebugBridge.IDebugBridgeChangeListener[] listenersCopy = (AndroidDebugBridge.IDebugBridgeChangeListener[])sBridgeListeners.toArray(new AndroidDebugBridge.IDebugBridgeChangeListener[sBridgeListeners.size()]);
            AndroidDebugBridge.IDebugBridgeChangeListener[] arr$ = listenersCopy;
            int len$ = listenersCopy.length;

            for(int i$ = 0; i$ < len$; ++i$) {
               AndroidDebugBridge.IDebugBridgeChangeListener listener = arr$[i$];

               try {
                  listener.bridgeChanged(sThis);
               } catch (Exception var8) {
                  Log.e("ddms", (Throwable)var8);
               }
            }
         }

      }
   }

   public static void addDebugBridgeChangeListener(AndroidDebugBridge.IDebugBridgeChangeListener listener) {
      Object var1 = sLock;
      synchronized(sLock) {
         if(!sBridgeListeners.contains(listener)) {
            sBridgeListeners.add(listener);
            if(sThis != null) {
               try {
                  listener.bridgeChanged(sThis);
               } catch (Exception var4) {
                  Log.e("ddms", (Throwable)var4);
               }
            }
         }

      }
   }

   public static void removeDebugBridgeChangeListener(AndroidDebugBridge.IDebugBridgeChangeListener listener) {
      Object var1 = sLock;
      synchronized(sLock) {
         sBridgeListeners.remove(listener);
      }
   }

   public static void addDeviceChangeListener(AndroidDebugBridge.IDeviceChangeListener listener) {
      Object var1 = sLock;
      synchronized(sLock) {
         if(!sDeviceListeners.contains(listener)) {
            sDeviceListeners.add(listener);
         }

      }
   }

   public static void removeDeviceChangeListener(AndroidDebugBridge.IDeviceChangeListener listener) {
      Object var1 = sLock;
      synchronized(sLock) {
         sDeviceListeners.remove(listener);
      }
   }

   public static void addClientChangeListener(AndroidDebugBridge.IClientChangeListener listener) {
      Object var1 = sLock;
      synchronized(sLock) {
         if(!sClientListeners.contains(listener)) {
            sClientListeners.add(listener);
         }

      }
   }

   public static void removeClientChangeListener(AndroidDebugBridge.IClientChangeListener listener) {
      Object var1 = sLock;
      synchronized(sLock) {
         sClientListeners.remove(listener);
      }
   }

   public IDevice[] getDevices() {
      Object var1 = sLock;
      synchronized(sLock) {
         if(this.mDeviceMonitor != null) {
            return this.mDeviceMonitor.getDevices();
         }
      }

      return new IDevice[0];
   }

   public boolean hasInitialDeviceList() {
      return this.mDeviceMonitor != null?this.mDeviceMonitor.hasInitialDeviceList():false;
   }

   public void setSelectedClient(Client selectedClient) {
      MonitorThread monitorThread = MonitorThread.getInstance();
      if(monitorThread != null) {
         monitorThread.setSelectedClient(selectedClient);
      }

   }

   public boolean isConnected() {
      MonitorThread monitorThread = MonitorThread.getInstance();
      return this.mDeviceMonitor != null && monitorThread != null?this.mDeviceMonitor.isMonitoring() && monitorThread.getState() != State.TERMINATED:false;
   }

   public int getConnectionAttemptCount() {
      return this.mDeviceMonitor != null?this.mDeviceMonitor.getConnectionAttemptCount():-1;
   }

   public int getRestartAttemptCount() {
      return this.mDeviceMonitor != null?this.mDeviceMonitor.getRestartAttemptCount():-1;
   }

   private AndroidDebugBridge(String osLocation) throws InvalidParameterException {
      if(osLocation != null && !osLocation.isEmpty()) {
         this.mAdbOsLocation = osLocation;

         try {
            this.checkAdbVersion();
         } catch (IOException var3) {
            throw new IllegalArgumentException(var3);
         }
      } else {
         throw new InvalidParameterException();
      }
   }

   private AndroidDebugBridge() {
   }

   private void checkAdbVersion() throws IOException {
      this.mVersionCheck = false;
      if(this.mAdbOsLocation != null) {
         File adb = new File(this.mAdbOsLocation);
         ListenableFuture future = getAdbVersion(adb);

         AdbVersion version;
         try {
            version = (AdbVersion)future.get(5L, TimeUnit.SECONDS);
         } catch (InterruptedException var6) {
            return;
         } catch (java.util.concurrent.TimeoutException var7) {
            String msg = "Unable to obtain result of \'adb version\'";
            Log.logAndDisplay(Log.LogLevel.ERROR, "adb", msg);
            return;
         } catch (ExecutionException var8) {
            Log.logAndDisplay(Log.LogLevel.ERROR, "adb", var8.getCause().getMessage());
            Throwables.propagateIfInstanceOf(var8.getCause(), IOException.class);
            return;
         }

         if(version.compareTo(MIN_ADB_VERSION) > 0) {
            this.mVersionCheck = true;
         } else {
            String message = String.format("Required minimum version of adb: %1$s.Current version is %2$s", new Object[]{MIN_ADB_VERSION, version});
            Log.logAndDisplay(Log.LogLevel.ERROR, "adb", message);
         }

      }
   }

   public static ListenableFuture<AdbVersion> getAdbVersion(final File adb) {
      final SettableFuture future = SettableFuture.create();
      (new Thread(new Runnable() {
         public void run() {
            ProcessBuilder pb = new ProcessBuilder(new String[]{adb.getPath(), "version"});
            pb.redirectErrorStream(true);
            Process p = null;

            try {
               p = pb.start();
            } catch (IOException var18) {
               future.setException(var18);
               return;
            }

            StringBuilder sb = new StringBuilder();
            BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));

            try {
               String e;
               try {
                  while((e = br.readLine()) != null) {
                     AdbVersion e1 = AdbVersion.parseFrom(e);
                     if(e1 != AdbVersion.UNKNOWN) {
                        future.set(e1);
                        return;
                     }

                     sb.append(e);
                     sb.append('\n');
                  }
               } catch (IOException var19) {
                  future.setException(var19);
                  return;
               }
            } finally {
               try {
                  br.close();
               } catch (IOException var17) {
                  future.setException(var17);
               }

            }

            future.setException(new RuntimeException("Unable to detect adb version, adb output: " + sb.toString()));
         }
      }, "Obtaining adb version")).start();
      return future;
   }

   boolean start() {
      if(this.mAdbOsLocation == null || sAdbServerPort == 0 || this.mVersionCheck && this.startAdb()) {
         this.mStarted = true;
         this.mDeviceMonitor = new DeviceMonitor(this);
         this.mDeviceMonitor.start();
         return true;
      } else {
         return false;
      }
   }

   boolean stop() {
      if(!this.mStarted) {
         return false;
      } else {
         if(this.mDeviceMonitor != null) {
            this.mDeviceMonitor.stop();
            this.mDeviceMonitor = null;
         }

         if(!this.stopAdb()) {
            return false;
         } else {
            this.mStarted = false;
            return true;
         }
      }
   }

   /**
    * 重启adb
    * @return
     */
   public boolean restart() {
      if(this.mAdbOsLocation == null) {
         Log.e("adb", "Cannot restart adb when AndroidDebugBridge is created without the location of adb.");
         return false;
      } else if(sAdbServerPort == 0) {
         Log.e("adb", "ADB server port for restarting AndroidDebugBridge is not set.");
         return false;
      } else if(!this.mVersionCheck) {
         Log.logAndDisplay(Log.LogLevel.ERROR, "adb", "Attempting to restart adb, but version check failed!");
         return false;
      } else {
         synchronized(this) {
            this.stopAdb();
            boolean restart = this.startAdb();
            if(restart && this.mDeviceMonitor == null) {
               this.mDeviceMonitor = new DeviceMonitor(this);
               this.mDeviceMonitor.start();
            }

            return restart;
         }
      }
   }

   void deviceConnected(IDevice device) {
      AndroidDebugBridge.IDeviceChangeListener[] listenersCopy = null;
      Object arr$ = sLock;
      synchronized(sLock) {
         listenersCopy = (AndroidDebugBridge.IDeviceChangeListener[])sDeviceListeners.toArray(new AndroidDebugBridge.IDeviceChangeListener[sDeviceListeners.size()]);
      }

      AndroidDebugBridge.IDeviceChangeListener[] var10 = listenersCopy;
      int len$ = listenersCopy.length;

      for(int i$ = 0; i$ < len$; ++i$) {
         AndroidDebugBridge.IDeviceChangeListener listener = var10[i$];

         try {
            listener.deviceConnected(device);
         } catch (Exception var8) {
            Log.e("ddms", (Throwable)var8);
         }
      }

   }

   void deviceDisconnected(IDevice device) {
      AndroidDebugBridge.IDeviceChangeListener[] listenersCopy = null;
      Object arr$ = sLock;
      synchronized(sLock) {
         listenersCopy = (AndroidDebugBridge.IDeviceChangeListener[])sDeviceListeners.toArray(new AndroidDebugBridge.IDeviceChangeListener[sDeviceListeners.size()]);
      }

      AndroidDebugBridge.IDeviceChangeListener[] var10 = listenersCopy;
      int len$ = listenersCopy.length;

      for(int i$ = 0; i$ < len$; ++i$) {
         AndroidDebugBridge.IDeviceChangeListener listener = var10[i$];

         try {
            listener.deviceDisconnected(device);
         } catch (Exception var8) {
            Log.e("ddms", (Throwable)var8);
         }
      }

   }

   void deviceChanged(IDevice device, int changeMask) {
      AndroidDebugBridge.IDeviceChangeListener[] listenersCopy = null;
      Object arr$ = sLock;
      synchronized(sLock) {
         listenersCopy = (AndroidDebugBridge.IDeviceChangeListener[])sDeviceListeners.toArray(new AndroidDebugBridge.IDeviceChangeListener[sDeviceListeners.size()]);
      }

      AndroidDebugBridge.IDeviceChangeListener[] var11 = listenersCopy;
      int len$ = listenersCopy.length;

      for(int i$ = 0; i$ < len$; ++i$) {
         AndroidDebugBridge.IDeviceChangeListener listener = var11[i$];

         try {
            listener.deviceChanged(device, changeMask);
         } catch (Exception var9) {
            Log.e("ddms", (Throwable)var9);
         }
      }

   }

   void clientChanged(Client client, int changeMask) {
      AndroidDebugBridge.IClientChangeListener[] listenersCopy = null;
      Object arr$ = sLock;
      synchronized(sLock) {
         listenersCopy = (AndroidDebugBridge.IClientChangeListener[])sClientListeners.toArray(new AndroidDebugBridge.IClientChangeListener[sClientListeners.size()]);
      }

      AndroidDebugBridge.IClientChangeListener[] var11 = listenersCopy;
      int len$ = listenersCopy.length;

      for(int i$ = 0; i$ < len$; ++i$) {
         AndroidDebugBridge.IClientChangeListener listener = var11[i$];

         try {
            listener.clientChanged(client, changeMask);
         } catch (Exception var9) {
            Log.e("ddms", (Throwable)var9);
         }
      }

   }

   DeviceMonitor getDeviceMonitor() {
      return this.mDeviceMonitor;
   }

   synchronized boolean startAdb() {
      if(this.mAdbOsLocation == null) {
         Log.e("adb", "Cannot start adb when AndroidDebugBridge is created without the location of adb.");
         return false;
      } else if(sAdbServerPort == 0) {
         Log.w("adb", "ADB server port for starting AndroidDebugBridge is not set.");
         return false;
      } else {
         int status = -1;
         String[] command = this.getAdbLaunchCommand("start-server");
         String commandString = Joiner.on(',').join(command);

         try {
            Log.d("ddms", String.format("Launching \'%1$s\' to ensure ADB is running.", new Object[]{commandString}));
            ProcessBuilder ie = new ProcessBuilder(command);
            if(DdmPreferences.getUseAdbHost()) {
               String errorOutput = DdmPreferences.getAdbHostValue();
               if(errorOutput != null && !errorOutput.isEmpty()) {
                  Map stdOutput = ie.environment();
                  stdOutput.put("ADBHOST", errorOutput);
               }
            }

            Process proc = ie.start();
            ArrayList errorOutput1 = new ArrayList();
            ArrayList stdOutput1 = new ArrayList();
            status = this.grabProcessOutput(proc, errorOutput1, stdOutput1, false);
         } catch (IOException var8) {
            Log.e("ddms", "Unable to run \'adb\': " + var8.getMessage());
         } catch (InterruptedException var9) {
            Log.e("ddms", "Unable to run \'adb\': " + var9.getMessage());
         }

         if(status != 0) {
            Log.e("ddms", String.format("\'%1$s\' failed -- run manually if necessary", new Object[]{commandString}));
            return false;
         } else {
            Log.d("ddms", String.format("\'%1$s\' succeeded", new Object[]{commandString}));
            return true;
         }
      }
   }

   private String[] getAdbLaunchCommand(String option) {
      ArrayList command = new ArrayList(4);
      command.add(this.mAdbOsLocation);
      if(sAdbServerPort != 5037) {
         command.add("-P");
         command.add(Integer.toString(sAdbServerPort));
      }

      command.add(option);
      return (String[])command.toArray(new String[command.size()]);
   }

   private synchronized boolean stopAdb() {
      if(this.mAdbOsLocation == null) {
         Log.e("adb", "Cannot stop adb when AndroidDebugBridge is created without the location of adb.");
         return false;
      } else if(sAdbServerPort == 0) {
         Log.e("adb", "ADB server port for restarting AndroidDebugBridge is not set");
         return false;
      } else {
         int status = -1;
         String[] command = this.getAdbLaunchCommand("kill-server");

         try {
            Process proc = Runtime.getRuntime().exec(command);
            status = proc.waitFor();
         } catch (IOException var5) {
            ;
         } catch (InterruptedException var6) {
            ;
         }

         String commandString = Joiner.on(',').join(command);
         if(status != 0) {
            Log.w("ddms", String.format("\'%1$s\' failed -- run manually if necessary", new Object[]{commandString}));
            return false;
         } else {
            Log.d("ddms", String.format("\'%1$s\' succeeded", new Object[]{commandString}));
            return true;
         }
      }
   }

   private int grabProcessOutput(final Process process, final ArrayList<String> errorOutput, final ArrayList<String> stdOutput, boolean waitForReaders) throws InterruptedException {
      assert errorOutput != null;

      assert stdOutput != null;

      Thread t1 = new Thread("") {
         public void run() {
            InputStreamReader is = new InputStreamReader(process.getErrorStream());
            BufferedReader errReader = new BufferedReader(is);

            try {
               while(true) {
                  String e = errReader.readLine();
                  if(e == null) {
                     break;
                  }

                  Log.e("adb", e);
                  errorOutput.add(e);
               }
            } catch (IOException var4) {
               ;
            }

         }
      };
      Thread t2 = new Thread("") {
         public void run() {
            InputStreamReader is = new InputStreamReader(process.getInputStream());
            BufferedReader outReader = new BufferedReader(is);

            try {
               while(true) {
                  String e = outReader.readLine();
                  if(e == null) {
                     break;
                  }

                  Log.d("adb", e);
                  stdOutput.add(e);
               }
            } catch (IOException var4) {
               ;
            }

         }
      };
      t1.start();
      t2.start();
      if(waitForReaders) {
         try {
            t1.join();
         } catch (InterruptedException var9) {
            ;
         }

         try {
            t2.join();
         } catch (InterruptedException var8) {
            ;
         }
      }

      return process.waitFor();
   }

   private static Object getLock() {
      return sLock;
   }

   private static void initAdbSocketAddr() {
      try {
         sAdbServerPort = getAdbServerPort();
         sHostAddr = InetAddress.getByName("127.0.0.1");
         sSocketAddr = new InetSocketAddress(sHostAddr, sAdbServerPort);
      } catch (UnknownHostException var1) {
         ;
      }

   }

   private static int getAdbServerPort() {
      Integer prop = Integer.getInteger("ANDROID_ADB_SERVER_PORT");
      String msg;
      if(prop != null) {
         try {
            return validateAdbServerPort(prop.toString());
         } catch (IllegalArgumentException var5) {
            msg = String.format("Invalid value (%1$s) for ANDROID_ADB_SERVER_PORT system property.", new Object[]{prop});
            Log.w("ddms", msg);
         }
      }

      try {
         String e = System.getenv("ANDROID_ADB_SERVER_PORT");
         if(e != null) {
            return validateAdbServerPort(e);
         }
      } catch (SecurityException var3) {
         Log.w("ddms", "No access to env variables allowed by current security manager. If you\'ve set ANDROID_ADB_SERVER_PORT: it\'s being ignored.");
      } catch (IllegalArgumentException var4) {
         msg = String.format("Invalid value (%1$s) for ANDROID_ADB_SERVER_PORT environment variable (%2$s).", new Object[]{prop, var4.getMessage()});
         Log.w("ddms", msg);
      }

      return 5037;
   }

   private static int validateAdbServerPort(String adbServerPort) throws IllegalArgumentException {
      try {
         int e = Integer.decode(adbServerPort).intValue();
         if(e > 0 && e < '\uffff') {
            return e;
         } else {
            throw new IllegalArgumentException("Should be > 0 and < 65535");
         }
      } catch (NumberFormatException var2) {
         throw new IllegalArgumentException("Not a valid port number");
      }
   }

   static {
      sLock = sBridgeListeners;
   }

   public interface IClientChangeListener {
      void clientChanged(Client var1, int var2);
   }

   public interface IDeviceChangeListener {
      void deviceConnected(IDevice var1);

      void deviceDisconnected(IDevice var1);

      void deviceChanged(IDevice var1, int var2);
   }

   public interface IDebugBridgeChangeListener {
      void bridgeChanged(AndroidDebugBridge var1);
   }
}
