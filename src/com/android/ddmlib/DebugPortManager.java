package com.android.ddmlib;

import com.android.ddmlib.IDevice;

public class DebugPortManager {
   private static DebugPortManager.IDebugPortProvider sProvider = null;

   public static void setProvider(DebugPortManager.IDebugPortProvider provider) {
      sProvider = provider;
   }

   static DebugPortManager.IDebugPortProvider getProvider() {
      return sProvider;
   }

   public interface IDebugPortProvider {
      int NO_STATIC_PORT = -1;

      int getPort(IDevice var1, String var2);
   }
}
