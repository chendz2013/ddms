package com.android.ddmlib;

import java.nio.ByteBuffer;

public final class RawImage {
   public int version;
   public int bpp;
   public int size;
   public int width;
   public int height;
   public int red_offset;
   public int red_length;
   public int blue_offset;
   public int blue_length;
   public int green_offset;
   public int green_length;
   public int alpha_offset;
   public int alpha_length;
   public byte[] data;

   public boolean readHeader(int version, ByteBuffer buf) {
      this.version = version;
      if(version == 16) {
         this.bpp = 16;
         this.size = buf.getInt();
         this.width = buf.getInt();
         this.height = buf.getInt();
         this.red_offset = 11;
         this.red_length = 5;
         this.green_offset = 5;
         this.green_length = 6;
         this.blue_offset = 0;
         this.blue_length = 5;
         this.alpha_offset = 0;
         this.alpha_length = 0;
      } else {
         if(version != 1) {
            return false;
         }

         this.bpp = buf.getInt();
         this.size = buf.getInt();
         this.width = buf.getInt();
         this.height = buf.getInt();
         this.red_offset = buf.getInt();
         this.red_length = buf.getInt();
         this.blue_offset = buf.getInt();
         this.blue_length = buf.getInt();
         this.green_offset = buf.getInt();
         this.green_length = buf.getInt();
         this.alpha_offset = buf.getInt();
         this.alpha_length = buf.getInt();
      }

      return true;
   }

   public int getRedMask() {
      return this.getMask(this.red_length, this.red_offset);
   }

   public int getGreenMask() {
      return this.getMask(this.green_length, this.green_offset);
   }

   public int getBlueMask() {
      return this.getMask(this.blue_length, this.blue_offset);
   }

   public static int getHeaderSize(int version) {
      switch(version) {
      case 1:
         return 12;
      case 16:
         return 3;
      default:
         return 0;
      }
   }

   public RawImage getRotated() {
      RawImage rotated = new RawImage();
      rotated.version = this.version;
      rotated.bpp = this.bpp;
      rotated.size = this.size;
      rotated.red_offset = this.red_offset;
      rotated.red_length = this.red_length;
      rotated.blue_offset = this.blue_offset;
      rotated.blue_length = this.blue_length;
      rotated.green_offset = this.green_offset;
      rotated.green_length = this.green_length;
      rotated.alpha_offset = this.alpha_offset;
      rotated.alpha_length = this.alpha_length;
      rotated.width = this.height;
      rotated.height = this.width;
      int count = this.data.length;
      rotated.data = new byte[count];
      int byteCount = this.bpp >> 3;
      int w = this.width;
      int h = this.height;

      for(int y = 0; y < h; ++y) {
         for(int x = 0; x < w; ++x) {
            System.arraycopy(this.data, (y * w + x) * byteCount, rotated.data, ((w - x - 1) * h + y) * byteCount, byteCount);
         }
      }

      return rotated;
   }

   public int getARGB(int index) {
      int value;
      int r;
      int g;
      int b;
      int a;
      if(this.bpp == 16) {
         value = this.data[index] & 255;
         value |= this.data[index + 1] << 8 & '\uff00';
         r = (value >>> 11 & 31) * 255 / 31;
         g = (value >>> 5 & 63) * 255 / 63;
         b = (value & 31) * 255 / 31;
         a = 255;
      } else {
         if(this.bpp != 32) {
            throw new UnsupportedOperationException("RawImage.getARGB(int) only works in 16 and 32 bit mode.");
         }

         value = this.data[index] & 255;
         value |= (this.data[index + 1] & 255) << 8;
         value |= (this.data[index + 2] & 255) << 16;
         value |= (this.data[index + 3] & 255) << 24;
         r = (value >>> this.red_offset & getMask(this.red_length)) << 8 - this.red_length;
         g = (value >>> this.green_offset & getMask(this.green_length)) << 8 - this.green_length;
         b = (value >>> this.blue_offset & getMask(this.blue_length)) << 8 - this.blue_length;
         a = (value >>> this.alpha_offset & getMask(this.alpha_length)) << 8 - this.alpha_length;
      }

      return a << 24 | r << 16 | g << 8 | b;
   }

   private int getMask(int length, int offset) {
      int res = getMask(length) << offset;
      return this.bpp == 32?Integer.reverseBytes(res):res;
   }

   private static int getMask(int length) {
      return (1 << length) - 1;
   }
}
