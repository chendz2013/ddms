package com.android.ddmlib;

import com.android.ddmlib.NativeStackCallInfo;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NativeAllocationInfo {
   public static final String END_STACKTRACE_KW = "EndStacktrace";
   public static final String BEGIN_STACKTRACE_KW = "BeginStacktrace:";
   public static final String TOTAL_SIZE_KW = "TotalSize:";
   public static final String SIZE_KW = "Size:";
   public static final String ALLOCATIONS_KW = "Allocations:";
   private static final int FLAG_ZYGOTE_CHILD = Integer.MIN_VALUE;
   private static final int FLAG_MASK = Integer.MIN_VALUE;
   private static final List<String> FILTERED_LIBRARIES = Arrays.asList(new String[]{"libc.so", "libc_malloc_debug_leak.so"});
   private static final List<Pattern> FILTERED_METHOD_NAME_PATTERNS = Arrays.asList(new Pattern[]{Pattern.compile("malloc", 2), Pattern.compile("calloc", 2), Pattern.compile("realloc", 2), Pattern.compile("operator new", 2), Pattern.compile("memalign", 2)});
   private final int mSize;
   private final boolean mIsZygoteChild;
   private int mAllocations;
   private final ArrayList<Long> mStackCallAddresses = new ArrayList();
   private ArrayList<NativeStackCallInfo> mResolvedStackCall = null;
   private boolean mIsStackCallResolved = false;

   public NativeAllocationInfo(int size, int allocations) {
      this.mSize = size & Integer.MAX_VALUE;
      this.mIsZygoteChild = (size & Integer.MIN_VALUE) != 0;
      this.mAllocations = allocations;
   }

   public void addStackCallAddress(long address) {
      this.mStackCallAddresses.add(Long.valueOf(address));
   }

   public int getSize() {
      return this.mSize;
   }

   public boolean isZygoteChild() {
      return this.mIsZygoteChild;
   }

   public int getAllocationCount() {
      return this.mAllocations;
   }

   public boolean isStackCallResolved() {
      return this.mIsStackCallResolved;
   }

   public List<Long> getStackCallAddresses() {
      return this.mStackCallAddresses;
   }

   public synchronized void setResolvedStackCall(List<NativeStackCallInfo> resolvedStackCall) {
      if(this.mResolvedStackCall == null) {
         this.mResolvedStackCall = new ArrayList();
      } else {
         this.mResolvedStackCall.clear();
      }

      this.mResolvedStackCall.addAll(resolvedStackCall);
      this.mIsStackCallResolved = !this.mResolvedStackCall.isEmpty();
   }

   public synchronized List<NativeStackCallInfo> getResolvedStackCall() {
      return this.mIsStackCallResolved?this.mResolvedStackCall:null;
   }

   public boolean equals(Object obj) {
      if(obj == this) {
         return true;
      } else if(obj instanceof NativeAllocationInfo) {
         NativeAllocationInfo mi = (NativeAllocationInfo)obj;
         return this.mSize == mi.mSize && this.mAllocations == mi.mAllocations?this.stackEquals(mi):false;
      } else {
         return false;
      }
   }

   public boolean stackEquals(NativeAllocationInfo mi) {
      if(this.mStackCallAddresses.size() != mi.mStackCallAddresses.size()) {
         return false;
      } else {
         int count = this.mStackCallAddresses.size();

         for(int i = 0; i < count; ++i) {
            long a = ((Long)this.mStackCallAddresses.get(i)).longValue();
            long b = ((Long)mi.mStackCallAddresses.get(i)).longValue();
            if(a != b) {
               return false;
            }
         }

         return true;
      }
   }

   public int hashCode() {
      byte result = 17;
      int result1 = 31 * result + this.mSize;
      result1 = 31 * result1 + this.mAllocations;
      result1 = 31 * result1 + this.mStackCallAddresses.size();

      long addr;
      for(Iterator i$ = this.mStackCallAddresses.iterator(); i$.hasNext(); result1 = 31 * result1 + (int)(addr ^ addr >>> 32)) {
         addr = ((Long)i$.next()).longValue();
      }

      return result1;
   }

   public String toString() {
      StringBuilder buffer = new StringBuilder();
      buffer.append("Allocations:");
      buffer.append(' ');
      buffer.append(this.mAllocations);
      buffer.append('\n');
      buffer.append("Size:");
      buffer.append(' ');
      buffer.append(this.mSize);
      buffer.append('\n');
      buffer.append("TotalSize:");
      buffer.append(' ');
      buffer.append(this.mSize * this.mAllocations);
      buffer.append('\n');
      if(this.mResolvedStackCall != null) {
         buffer.append("BeginStacktrace:");
         buffer.append('\n');
         Iterator i$ = this.mResolvedStackCall.iterator();

         while(i$.hasNext()) {
            NativeStackCallInfo source = (NativeStackCallInfo)i$.next();
            long addr = source.getAddress();
            if(addr != 0L) {
               if(source.getLineNumber() != -1) {
                  buffer.append(String.format("\t%1$08x\t%2$s --- %3$s --- %4$s:%5$d\n", new Object[]{Long.valueOf(addr), source.getLibraryName(), source.getMethodName(), source.getSourceFile(), Integer.valueOf(source.getLineNumber())}));
               } else {
                  buffer.append(String.format("\t%1$08x\t%2$s --- %3$s --- %4$s\n", new Object[]{Long.valueOf(addr), source.getLibraryName(), source.getMethodName(), source.getSourceFile()}));
               }
            }
         }

         buffer.append("EndStacktrace");
         buffer.append('\n');
      }

      return buffer.toString();
   }

   public synchronized NativeStackCallInfo getRelevantStackCallInfo() {
      if(this.mIsStackCallResolved && this.mResolvedStackCall != null) {
         Iterator i$ = this.mResolvedStackCall.iterator();

         while(i$.hasNext()) {
            NativeStackCallInfo info = (NativeStackCallInfo)i$.next();
            if(this.isRelevantLibrary(info.getLibraryName()) && this.isRelevantMethod(info.getMethodName())) {
               return info;
            }
         }

         if(!this.mResolvedStackCall.isEmpty()) {
            return (NativeStackCallInfo)this.mResolvedStackCall.get(0);
         }
      }

      return null;
   }

   private boolean isRelevantLibrary(String libPath) {
      Iterator i$ = FILTERED_LIBRARIES.iterator();

      String l;
      do {
         if(!i$.hasNext()) {
            return true;
         }

         l = (String)i$.next();
      } while(!libPath.endsWith(l));

      return false;
   }

   private boolean isRelevantMethod(String methodName) {
      Iterator i$ = FILTERED_METHOD_NAME_PATTERNS.iterator();

      Matcher m;
      do {
         if(!i$.hasNext()) {
            return true;
         }

         Pattern p = (Pattern)i$.next();
         m = p.matcher(methodName);
      } while(!m.find());

      return false;
   }
}
