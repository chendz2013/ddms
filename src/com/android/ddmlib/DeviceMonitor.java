package com.android.ddmlib;

import com.android.ddmlib.AdbCommandRejectedException;
import com.android.ddmlib.AdbHelper;
import com.android.ddmlib.AndroidDebugBridge;
import com.android.ddmlib.Client;
import com.android.ddmlib.ClientData;
import com.android.ddmlib.DdmPreferences;
import com.android.ddmlib.Device;
import com.android.ddmlib.EmulatorConsole;
import com.android.ddmlib.IDevice;
import com.android.ddmlib.Log;
import com.android.ddmlib.MonitorThread;
import com.android.ddmlib.TimeoutException;
import com.android.ddmlib.utils.DebuggerPorts;
import com.android.utils.Pair;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Queues;
import com.google.common.util.concurrent.Uninterruptibles;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousCloseException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

final class DeviceMonitor {
   private static final String ADB_TRACK_DEVICES_COMMAND = "host:track-devices";
   private static final String ADB_TRACK_JDWP_COMMAND = "track-jdwp";
   private final byte[] mLengthBuffer2 = new byte[4];
   private volatile boolean mQuit = false;
   private final AndroidDebugBridge mServer;
   private DeviceMonitor.DeviceListMonitorTask mDeviceListMonitorTask;
   private Selector mSelector;
   private final List<Device> mDevices = Lists.newCopyOnWriteArrayList();
   private final DebuggerPorts mDebuggerPorts = new DebuggerPorts(DdmPreferences.getDebugPortBase());
   private final Map<Client, Integer> mClientsToReopen = new HashMap();
   private final BlockingQueue<Pair<SocketChannel, Device>> mChannelsToRegister = Queues.newLinkedBlockingQueue();

   DeviceMonitor(AndroidDebugBridge server) {
      this.mServer = server;
   }

   void start() {
      this.mDeviceListMonitorTask = new DeviceMonitor.DeviceListMonitorTask(this.mServer, new DeviceMonitor.DeviceListUpdateListener(null));
      (new Thread(this.mDeviceListMonitorTask, "Device List Monitor")).start();
   }

   void stop() {
      this.mQuit = true;
      if(this.mDeviceListMonitorTask != null) {
         this.mDeviceListMonitorTask.stop();
      }

      if(this.mSelector != null) {
         this.mSelector.wakeup();
      }

   }

   boolean isMonitoring() {
      return this.mDeviceListMonitorTask != null && this.mDeviceListMonitorTask.isMonitoring();
   }

   int getConnectionAttemptCount() {
      return this.mDeviceListMonitorTask == null?0:this.mDeviceListMonitorTask.getConnectionAttemptCount();
   }

   int getRestartAttemptCount() {
      return this.mDeviceListMonitorTask == null?0:this.mDeviceListMonitorTask.getRestartAttemptCount();
   }

   boolean hasInitialDeviceList() {
      return this.mDeviceListMonitorTask != null && this.mDeviceListMonitorTask.hasInitialDeviceList();
   }

   Device[] getDevices() {
      return (Device[])this.mDevices.toArray(new Device[0]);
   }

   AndroidDebugBridge getServer() {
      return this.mServer;
   }

   void addClientToDropAndReopen(Client client, int port) {
      Map var3 = this.mClientsToReopen;
      synchronized(this.mClientsToReopen) {
         Log.d("DeviceMonitor", "Adding " + client + " to list of client to reopen (" + port + ").");
         if(this.mClientsToReopen.get(client) == null) {
            this.mClientsToReopen.put(client, Integer.valueOf(port));
         }
      }

      this.mSelector.wakeup();
   }

   private static SocketChannel openAdbConnection() {
      try {
         SocketChannel e = SocketChannel.open(AndroidDebugBridge.getSocketAddress());
         e.socket().setTcpNoDelay(true);
         return e;
      } catch (IOException var1) {
         return null;
      }
   }

   private void updateDevices(List<Device> newList) {
      DeviceMonitor.DeviceListComparisonResult result = DeviceMonitor.DeviceListComparisonResult.compare(this.mDevices, newList);
      Iterator newlyOnline = result.removed.iterator();

      while(newlyOnline.hasNext()) {
         IDevice i$ = (IDevice)newlyOnline.next();
         this.removeDevice((Device)i$);
         this.mServer.deviceDisconnected(i$);
      }

      ArrayList newlyOnline1 = Lists.newArrayListWithExpectedSize(this.mDevices.size());
      Iterator i$1 = result.updated.entrySet().iterator();

      while(i$1.hasNext()) {
         Entry device = (Entry)i$1.next();
         Device device1 = (Device)device.getKey();
         device1.setState((IDevice.DeviceState)device.getValue());
         device1.update(1);
         if(device1.isOnline()) {
            newlyOnline1.add(device1);
         }
      }

      i$1 = result.added.iterator();

      while(i$1.hasNext()) {
         IDevice device2 = (IDevice)i$1.next();
         this.mDevices.add((Device)device2);
         this.mServer.deviceConnected(device2);
         if(device2.isOnline()) {
            newlyOnline1.add((Device)device2);
         }
      }

      Device device3;
      if(AndroidDebugBridge.getClientSupport()) {
         i$1 = newlyOnline1.iterator();

         while(i$1.hasNext()) {
            device3 = (Device)i$1.next();
            if(!this.startMonitoringDevice(device3)) {
               Log.e("DeviceMonitor", "Failed to start monitoring " + device3.getSerialNumber());
            }
         }
      }

      i$1 = newlyOnline1.iterator();

      while(i$1.hasNext()) {
         device3 = (Device)i$1.next();
         queryAvdName(device3);
      }

   }

   private void removeDevice(Device device) {
      device.clearClientList();
      this.mDevices.remove(device);
      SocketChannel channel = device.getClientMonitoringSocket();
      if(channel != null) {
         try {
            channel.close();
         } catch (IOException var4) {
            ;
         }
      }

   }

   private static void queryAvdName(Device device) {
      if(device.isEmulator()) {
         EmulatorConsole console = EmulatorConsole.getConsole(device);
         if(console != null) {
            device.setAvdName(console.getAvdName());
            console.close();
         }

      }
   }

   private boolean startMonitoringDevice(Device device) {
      SocketChannel socketChannel = openAdbConnection();
      if(socketChannel != null) {
         try {
            boolean e = sendDeviceMonitoringRequest(socketChannel, device);
            if(e) {
               if(this.mSelector == null) {
                  this.startDeviceMonitorThread();
               }

               device.setClientMonitoringSocket(socketChannel);
               socketChannel.configureBlocking(false);

               try {
                  this.mChannelsToRegister.put(Pair.of(socketChannel, device));
               } catch (InterruptedException var8) {
                  ;
               }

               this.mSelector.wakeup();
               return true;
            }
         } catch (TimeoutException var9) {
            try {
               socketChannel.close();
            } catch (IOException var7) {
               ;
            }

            Log.d("DeviceMonitor", "Connection Failure when starting to monitor device \'" + device + "\' : timeout");
         } catch (AdbCommandRejectedException var10) {
            try {
               socketChannel.close();
            } catch (IOException var6) {
               ;
            }

            Log.d("DeviceMonitor", "Adb refused to start monitoring device \'" + device + "\' : " + var10.getMessage());
         } catch (IOException var11) {
            try {
               socketChannel.close();
            } catch (IOException var5) {
               ;
            }

            Log.d("DeviceMonitor", "Connection Failure when starting to monitor device \'" + device + "\' : " + var11.getMessage());
         }
      }

      return false;
   }

   private void startDeviceMonitorThread() throws IOException {
      this.mSelector = Selector.open();
      (new Thread("Device Client Monitor") {
         public void run() {
            DeviceMonitor.this.deviceClientMonitorLoop();
         }
      }).start();
   }

   private void deviceClientMonitorLoop() {
      label99:
      do {
         try {
            int e = this.mSelector.select();
            if(this.mQuit) {
               return;
            }

            Map keys = this.mClientsToReopen;
            int ioe;
            synchronized(this.mClientsToReopen) {
               if(!this.mClientsToReopen.isEmpty()) {
                  Set iter = this.mClientsToReopen.keySet();
                  MonitorThread key = MonitorThread.getInstance();
                  Iterator attachment = iter.iterator();

                  while(attachment.hasNext()) {
                     Client device = (Client)attachment.next();
                     Device socket = device.getDeviceImpl();
                     ioe = device.getClientData().getPid();
                     key.dropClient(device, false);
                     Uninterruptibles.sleepUninterruptibly(1L, TimeUnit.SECONDS);
                     int port = ((Integer)this.mClientsToReopen.get(device)).intValue();
                     if(port == -1) {
                        port = this.getNextDebuggerPort();
                     }

                     Log.d("DeviceMonitor", "Reopening " + device);
                     openClient(socket, ioe, port, key);
                     socket.update(2);
                  }

                  this.mClientsToReopen.clear();
               }
            }

            while(!this.mChannelsToRegister.isEmpty()) {
               try {
                  Pair keys1 = (Pair)this.mChannelsToRegister.take();
                  ((SocketChannel)keys1.getFirst()).register(this.mSelector, 1, keys1.getSecond());
               } catch (InterruptedException var11) {
                  ;
               }
            }

            if(e != 0) {
               Set keys2 = this.mSelector.selectedKeys();
               Iterator iter1 = keys2.iterator();

               while(true) {
                  SocketChannel socket1;
                  Device device1;
                  do {
                     Object attachment1;
                     do {
                        SelectionKey key1;
                        do {
                           do {
                              if(!iter1.hasNext()) {
                                 continue label99;
                              }

                              key1 = (SelectionKey)iter1.next();
                              iter1.remove();
                           } while(!key1.isValid());
                        } while(!key1.isReadable());

                        attachment1 = key1.attachment();
                     } while(!(attachment1 instanceof Device));

                     device1 = (Device)attachment1;
                     socket1 = device1.getClientMonitoringSocket();
                  } while(socket1 == null);

                  try {
                     ioe = readLength(socket1, this.mLengthBuffer2);
                     this.processIncomingJdwpData(device1, socket1, ioe);
                  } catch (IOException var12) {
                     Log.d("DeviceMonitor", "Error reading jdwp list: " + var12.getMessage());
                     socket1.close();
                     if(this.mDevices.contains(device1)) {
                        Log.d("DeviceMonitor", "Restarting monitoring service for " + device1);
                        this.startMonitoringDevice(device1);
                     }
                  }
               }
            }
         } catch (IOException var14) {
            Log.e("DeviceMonitor", "Connection error while monitoring clients.");
         }
      } while(!this.mQuit);

   }

   private static boolean sendDeviceMonitoringRequest(SocketChannel socket, Device device) throws TimeoutException, AdbCommandRejectedException, IOException {
      try {
         AdbHelper.setDevice(socket, device);
         AdbHelper.write(socket, AdbHelper.formAdbRequest("track-jdwp"));
         AdbHelper.AdbResponse e = AdbHelper.readAdbResponse(socket, false);
         if(!e.okay) {
            Log.e("DeviceMonitor", "adb refused request: " + e.message);
         }

         return e.okay;
      } catch (TimeoutException var3) {
         Log.e("DeviceMonitor", "Sending jdwp tracking request timed out!");
         throw var3;
      } catch (IOException var4) {
         Log.e("DeviceMonitor", "Sending jdwp tracking request failed!");
         throw var4;
      }
   }

   private void processIncomingJdwpData(Device device, SocketChannel monitorSocket, int length) throws IOException {
      if(length >= 0) {
         HashSet newPids = new HashSet();
         if(length > 0) {
            byte[] monitorThread = new byte[length];
            String clients = read(monitorSocket, monitorThread);
            String[] existingClients = clients == null?new String[0]:clients.split("\n");
            String[] clientsToRemove = existingClients;
            int pidsToAdd = existingClients.length;

            for(int i$ = 0; i$ < pidsToAdd; ++i$) {
               String newPid = clientsToRemove[i$];

               try {
                  newPids.add(Integer.valueOf(newPid));
               } catch (NumberFormatException var14) {
                  ;
               }
            }
         }

         MonitorThread var16 = MonitorThread.getInstance();
         List var17 = device.getClientList();
         HashMap var18 = new HashMap();
         Iterator var20;
         synchronized(var17) {
            var20 = var17.iterator();

            while(var20.hasNext()) {
               Client var25 = (Client)var20.next();
               var18.put(Integer.valueOf(var25.getClientData().getPid()), var25);
            }
         }

         HashSet var19 = new HashSet();
         var20 = var18.keySet().iterator();

         while(var20.hasNext()) {
            Integer var22 = (Integer)var20.next();
            if(!newPids.contains(var22)) {
               var19.add(var18.get(var22));
            }
         }

         HashSet var21 = new HashSet(newPids);
         var21.removeAll(var18.keySet());
         var16.dropClients(var19, false);
         Iterator var23 = var21.iterator();

         while(var23.hasNext()) {
            int var24 = ((Integer)var23.next()).intValue();
            openClient(device, var24, this.getNextDebuggerPort(), var16);
         }

         if(!var21.isEmpty() || !var19.isEmpty()) {
            this.mServer.deviceChanged(device, 2);
         }
      }

   }

   private static void openClient(Device device, int pid, int port, MonitorThread monitorThread) {
      SocketChannel clientSocket;
      try {
         clientSocket = AdbHelper.createPassThroughConnection(AndroidDebugBridge.getSocketAddress(), device, pid);
         clientSocket.configureBlocking(false);
      } catch (UnknownHostException var6) {
         Log.d("DeviceMonitor", "Unknown Jdwp pid: " + pid);
         return;
      } catch (TimeoutException var7) {
         Log.w("DeviceMonitor", "Failed to connect to client \'" + pid + "\': timeout");
         return;
      } catch (AdbCommandRejectedException var8) {
         Log.w("DeviceMonitor", "Adb rejected connection to client \'" + pid + "\': " + var8.getMessage());
         return;
      } catch (IOException var9) {
         Log.w("DeviceMonitor", "Failed to connect to client \'" + pid + "\': " + var9.getMessage());
         return;
      }

      createClient(device, pid, clientSocket, port, monitorThread);
   }

   private static void createClient(Device device, int pid, SocketChannel socket, int debuggerPort, MonitorThread monitorThread) {
      Client client = new Client(device, socket, pid);
      if(client.sendHandshake()) {
         try {
            if(AndroidDebugBridge.getClientSupport()) {
               client.listenForDebugger(debuggerPort);
            }
         } catch (IOException var7) {
            client.getClientData().setDebuggerConnectionStatus(ClientData.DebuggerStatus.ERROR);
            Log.e("ddms", "Can\'t bind to local " + debuggerPort + " for debugger");
         }

         client.requestAllocationStatus();
      } else {
         Log.e("ddms", "Handshake with " + client + " failed!");
      }

      if(client.isValid()) {
         device.addClient(client);
         monitorThread.addClient(client);
      }

   }

   private int getNextDebuggerPort() {
      return this.mDebuggerPorts.next();
   }

   void addPortToAvailableList(int port) {
      this.mDebuggerPorts.free(port);
   }

   private static int readLength(SocketChannel socket, byte[] buffer) throws IOException {
      String msg = read(socket, buffer);
      if(msg != null) {
         try {
            return Integer.parseInt(msg, 16);
         } catch (NumberFormatException var4) {
            ;
         }
      }

      throw new IOException("Unable to read length");
   }

   private static String read(SocketChannel socket, byte[] buffer) throws IOException {
      ByteBuffer buf = ByteBuffer.wrap(buffer, 0, buffer.length);

      int e;
      do {
         if(buf.position() == buf.limit()) {
            try {
               return new String(buffer, 0, buf.position(), "ISO-8859-1");
            } catch (UnsupportedEncodingException var4) {
               return null;
            }
         }

         e = socket.read(buf);
      } while(e >= 0);

      throw new IOException("EOF");
   }

   static class DeviceListMonitorTask implements Runnable {
      private final byte[] mLengthBuffer = new byte[4];
      private final AndroidDebugBridge mBridge;
      private final DeviceMonitor.DeviceListMonitorTask.UpdateListener mListener;
      private SocketChannel mAdbConnection = null;
      private boolean mMonitoring = false;
      private int mConnectionAttempt = 0;
      private int mRestartAttemptCount = 0;
      private boolean mInitialDeviceListDone = false;
      private volatile boolean mQuit;

      public DeviceListMonitorTask(AndroidDebugBridge bridge, DeviceMonitor.DeviceListMonitorTask.UpdateListener listener) {
         this.mBridge = bridge;
         this.mListener = listener;
      }

      public void run() {
         do {
            if(this.mAdbConnection == null) {
               Log.d("DeviceMonitor", "Opening adb connection");
               this.mAdbConnection = DeviceMonitor.openAdbConnection();
               if(this.mAdbConnection == null) {
                  ++this.mConnectionAttempt;
                  Log.e("DeviceMonitor", "Connection attempts: " + this.mConnectionAttempt);
                  if(this.mConnectionAttempt > 10) {
                     if(!this.mBridge.startAdb()) {
                        ++this.mRestartAttemptCount;
                        Log.e("DeviceMonitor", "adb restart attempts: " + this.mRestartAttemptCount);
                     } else {
                        Log.i("DeviceMonitor", "adb restarted");
                        this.mRestartAttemptCount = 0;
                     }
                  }

                  Uninterruptibles.sleepUninterruptibly(1L, TimeUnit.SECONDS);
               } else {
                  Log.d("DeviceMonitor", "Connected to adb for device monitoring");
                  this.mConnectionAttempt = 0;
               }
            }

            try {
               if(this.mAdbConnection != null && !this.mMonitoring) {
                  this.mMonitoring = this.sendDeviceListMonitoringRequest();
               }

               if(this.mMonitoring) {
                  int ioe = DeviceMonitor.readLength(this.mAdbConnection, this.mLengthBuffer);
                  if(ioe >= 0) {
                     this.processIncomingDeviceData(ioe);
                     this.mInitialDeviceListDone = true;
                  }
               }
            } catch (AsynchronousCloseException var2) {
               ;
            } catch (TimeoutException var3) {
               this.handleExceptionInMonitorLoop(var3);
            } catch (IOException var4) {
               this.handleExceptionInMonitorLoop(var4);
            }
         } while(!this.mQuit);

      }

      private boolean sendDeviceListMonitoringRequest() throws TimeoutException, IOException {
         byte[] request = AdbHelper.formAdbRequest("host:track-devices");

         try {
            AdbHelper.write(this.mAdbConnection, request);
            AdbHelper.AdbResponse e = AdbHelper.readAdbResponse(this.mAdbConnection, false);
            if(!e.okay) {
               Log.e("DeviceMonitor", "adb refused request: " + e.message);
            }

            return e.okay;
         } catch (IOException var3) {
            Log.e("DeviceMonitor", "Sending Tracking request failed!");
            this.mAdbConnection.close();
            throw var3;
         }
      }

      private void handleExceptionInMonitorLoop(Exception e) {
         if(!this.mQuit) {
            if(e instanceof TimeoutException) {
               Log.e("DeviceMonitor", "Adb connection Error: timeout");
            } else {
               Log.e("DeviceMonitor", "Adb connection Error:" + e.getMessage());
            }

            this.mMonitoring = false;
            if(this.mAdbConnection != null) {
               try {
                  this.mAdbConnection.close();
               } catch (IOException var3) {
                  ;
               }

               this.mAdbConnection = null;
               this.mListener.connectionError(e);
            }
         }

      }

      private void processIncomingDeviceData(int length) throws IOException {
         Map result;
         if(length <= 0) {
            result = Collections.emptyMap();
         } else {
            String response = DeviceMonitor.read(this.mAdbConnection, new byte[length]);
            result = parseDeviceListResponse(response);
         }

         this.mListener.deviceListUpdate(result);
      }

      static Map<String, IDevice.DeviceState> parseDeviceListResponse(String result) {
         HashMap deviceStateMap = Maps.newHashMap();
         String[] devices = result == null?new String[0]:result.split("\n");
         String[] arr$ = devices;
         int len$ = devices.length;

         for(int i$ = 0; i$ < len$; ++i$) {
            String d = arr$[i$];
            String[] param = d.split("\t");
            if(param.length == 2) {
               deviceStateMap.put(param[0], IDevice.DeviceState.getState(param[1]));
            }
         }

         return deviceStateMap;
      }

      boolean isMonitoring() {
         return this.mMonitoring;
      }

      boolean hasInitialDeviceList() {
         return this.mInitialDeviceListDone;
      }

      int getConnectionAttemptCount() {
         return this.mConnectionAttempt;
      }

      int getRestartAttemptCount() {
         return this.mRestartAttemptCount;
      }

      public void stop() {
         this.mQuit = true;
         if(this.mAdbConnection != null) {
            try {
               this.mAdbConnection.close();
            } catch (IOException var2) {
               ;
            }
         }

      }

      private interface UpdateListener {
         void connectionError(Exception var1);

         void deviceListUpdate(Map<String, IDevice.DeviceState> var1);
      }
   }

   static class DeviceListComparisonResult {
      public final Map<IDevice, IDevice.DeviceState> updated;
      public final List<IDevice> added;
      public final List<IDevice> removed;

      private DeviceListComparisonResult(Map<IDevice, IDevice.DeviceState> updated, List<IDevice> added, List<IDevice> removed) {
         this.updated = updated;
         this.added = added;
         this.removed = removed;
      }

      public static DeviceMonitor.DeviceListComparisonResult compare(List<? extends IDevice> previous, List<? extends IDevice> current) {
         ArrayList current1 = Lists.newArrayList(current);
         HashMap updated = Maps.newHashMapWithExpectedSize(current1.size());
         ArrayList added = Lists.newArrayListWithExpectedSize(1);
         ArrayList removed = Lists.newArrayListWithExpectedSize(1);
         Iterator i$ = previous.iterator();

         while(i$.hasNext()) {
            IDevice device = (IDevice)i$.next();
            IDevice currentDevice = find(current1, device);
            if(currentDevice != null) {
               if(currentDevice.getState() != device.getState()) {
                  updated.put(device, currentDevice.getState());
               }

               current1.remove(currentDevice);
            } else {
               removed.add(device);
            }
         }

         added.addAll(current1);
         return new DeviceMonitor.DeviceListComparisonResult(updated, added, removed);
      }

      private static IDevice find(List<? extends IDevice> devices, IDevice device) {
         Iterator i$ = devices.iterator();

         IDevice d;
         do {
            if(!i$.hasNext()) {
               return null;
            }

            d = (IDevice)i$.next();
         } while(!d.getSerialNumber().equals(device.getSerialNumber()));

         return d;
      }
   }

   private class DeviceListUpdateListener implements DeviceMonitor.DeviceListMonitorTask.UpdateListener {
      private DeviceListUpdateListener() {
      }

      public void connectionError(Exception e) {
         Iterator i$ = DeviceMonitor.this.mDevices.iterator();

         while(i$.hasNext()) {
            Device device = (Device)i$.next();
            DeviceMonitor.this.removeDevice(device);
            DeviceMonitor.this.mServer.deviceDisconnected(device);
         }

      }

      public void deviceListUpdate(Map<String, IDevice.DeviceState> devices) {
         ArrayList l = Lists.newArrayListWithExpectedSize(devices.size());
         Iterator i$ = devices.entrySet().iterator();

         while(i$.hasNext()) {
            Entry entry = (Entry)i$.next();
            l.add(new Device(DeviceMonitor.this, (String)entry.getKey(), (IDevice.DeviceState)entry.getValue()));
         }

         DeviceMonitor.this.updateDevices(l);
      }

      // $FF: synthetic method
      DeviceListUpdateListener(Object x1) {
         this();
      }
   }
}
