package com.android.ddmlib;

import com.android.ddmlib.Client;
import com.android.ddmlib.ClientData;
import com.android.ddmlib.JdwpPacket;
import com.android.ddmlib.Log;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

class Debugger {
   private static final int INITIAL_BUF_SIZE = 1024;
   private static final int MAX_BUF_SIZE = 32768;
   private ByteBuffer mReadBuffer;
   private static final int PRE_DATA_BUF_SIZE = 256;
   private ByteBuffer mPreDataBuffer;
   private int mConnState;
   private static final int ST_NOT_CONNECTED = 1;
   private static final int ST_AWAIT_SHAKE = 2;
   private static final int ST_READY = 3;
   private Client mClient;
   private int mListenPort;
   private ServerSocketChannel mListenChannel;
   private SocketChannel mChannel;

   Debugger(Client client, int listenPort) throws IOException {
      this.mClient = client;
      this.mListenPort = listenPort;
      this.mListenChannel = ServerSocketChannel.open();
      this.mListenChannel.configureBlocking(false);
      InetSocketAddress addr = new InetSocketAddress(InetAddress.getByName("localhost"), listenPort);
      this.mListenChannel.socket().setReuseAddress(true);
      this.mListenChannel.socket().bind(addr);
      this.mReadBuffer = ByteBuffer.allocate(1024);
      this.mPreDataBuffer = ByteBuffer.allocate(256);
      this.mConnState = 1;
      Log.d("ddms", "Created: " + this.toString());
   }

   boolean isDebuggerAttached() {
      return this.mChannel != null;
   }

   public String toString() {
      return "[Debugger " + this.mListenPort + "-->" + this.mClient.getClientData().getPid() + (this.mConnState != 3?" inactive]":" active]");
   }

   void registerListener(Selector sel) throws IOException {
      this.mListenChannel.register(sel, 16, this);
   }

   Client getClient() {
      return this.mClient;
   }

   synchronized SocketChannel accept() throws IOException {
      return this.accept(this.mListenChannel);
   }

   synchronized SocketChannel accept(ServerSocketChannel listenChan) throws IOException {
      if(listenChan != null) {
         SocketChannel newChan = listenChan.accept();
         if(this.mChannel != null) {
            Log.w("ddms", "debugger already talking to " + this.mClient + " on " + this.mListenPort);
            newChan.close();
            return null;
         } else {
            this.mChannel = newChan;
            this.mChannel.configureBlocking(false);
            this.mConnState = 2;
            return this.mChannel;
         }
      } else {
         return null;
      }
   }

   synchronized void closeData() {
      try {
         if(this.mChannel != null) {
            this.mChannel.close();
            this.mChannel = null;
            this.mConnState = 1;
            ClientData ioe = this.mClient.getClientData();
            ioe.setDebuggerConnectionStatus(ClientData.DebuggerStatus.DEFAULT);
            this.mClient.update(2);
         }
      } catch (IOException var2) {
         Log.w("ddms", "Failed to close data " + this);
      }

   }

   synchronized void close() {
      try {
         if(this.mListenChannel != null) {
            this.mListenChannel.close();
         }

         this.mListenChannel = null;
         this.closeData();
      } catch (IOException var2) {
         Log.w("ddms", "Failed to close listener " + this);
      }

   }

   void read() throws IOException {
      if(this.mReadBuffer.position() == this.mReadBuffer.capacity()) {
         if(this.mReadBuffer.capacity() * 2 > '耀') {
            throw new BufferOverflowException();
         }

         Log.d("ddms", "Expanding read buffer to " + this.mReadBuffer.capacity() * 2);
         ByteBuffer newBuffer = ByteBuffer.allocate(this.mReadBuffer.capacity() * 2);
         this.mReadBuffer.position(0);
         newBuffer.put(this.mReadBuffer);
         this.mReadBuffer = newBuffer;
      }

      int count = this.mChannel.read(this.mReadBuffer);
      Log.v("ddms", "Read " + count + " bytes from " + this);
      if(count < 0) {
         throw new IOException("read failed");
      }
   }

   JdwpPacket getJdwpPacket() throws IOException {
      if(this.mConnState == 2) {
         int result = JdwpPacket.findHandshake(this.mReadBuffer);
         switch(result) {
         case 1:
            Log.d("ddms", "Good handshake from debugger");
            JdwpPacket.consumeHandshake(this.mReadBuffer);
            this.sendHandshake();
            this.mConnState = 3;
            ClientData cd = this.mClient.getClientData();
            cd.setDebuggerConnectionStatus(ClientData.DebuggerStatus.ATTACHED);
            this.mClient.update(2);
            return this.getJdwpPacket();
         case 3:
            Log.d("ddms", "Bad handshake from debugger");
            throw new IOException("bad handshake");
         default:
            Log.e("ddms", "Unknown packet while waiting for client handshake");
         case 2:
            return null;
         }
      } else if(this.mConnState == 3) {
         if(this.mReadBuffer.position() != 0) {
            Log.v("ddms", "Checking " + this.mReadBuffer.position() + " bytes");
         }

         return JdwpPacket.findPacket(this.mReadBuffer);
      } else {
         Log.e("ddms", "Receiving data in state = " + this.mConnState);
         return null;
      }
   }

   void forwardPacketToClient(JdwpPacket packet) throws IOException {
      this.mClient.sendAndConsume(packet);
   }

   private synchronized void sendHandshake() throws IOException {
      ByteBuffer tempBuffer = ByteBuffer.allocate(JdwpPacket.HANDSHAKE_LEN);
      JdwpPacket.putHandshake(tempBuffer);
      int expectedLength = tempBuffer.position();
      tempBuffer.flip();
      if(this.mChannel.write(tempBuffer) != expectedLength) {
         throw new IOException("partial handshake write");
      } else {
         expectedLength = this.mPreDataBuffer.position();
         if(expectedLength > 0) {
            Log.d("ddms", "Sending " + this.mPreDataBuffer.position() + " bytes of saved data");
            this.mPreDataBuffer.flip();
            if(this.mChannel.write(this.mPreDataBuffer) != expectedLength) {
               throw new IOException("partial pre-data write");
            }

            this.mPreDataBuffer.clear();
         }

      }
   }

   synchronized void sendAndConsume(JdwpPacket packet) throws IOException {
      if(this.mChannel == null) {
         Log.d("ddms", "Saving packet 0x" + Integer.toHexString(packet.getId()));
         packet.movePacket(this.mPreDataBuffer);
      } else {
         packet.writeAndConsume(this.mChannel);
      }

   }
}
