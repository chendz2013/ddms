package com.android.ddmlib.logcat;

import com.android.ddmlib.Log;
import com.android.ddmlib.logcat.LogCatMessage;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public final class LogCatFilter {
   private static final String PID_KEYWORD = "pid:";
   private static final String APP_KEYWORD = "app:";
   private static final String TAG_KEYWORD = "tag:";
   private static final String TEXT_KEYWORD = "text:";
   private final String mName;
   private final String mTag;
   private final String mText;
   private final String mPid;
   private final String mAppName;
   private final Log.LogLevel mLogLevel;
   private boolean mCheckPid;
   private boolean mCheckAppName;
   private boolean mCheckTag;
   private boolean mCheckText;
   private Pattern mAppNamePattern;
   private Pattern mTagPattern;
   private Pattern mTextPattern;

   public LogCatFilter(String name, String tag, String text, String pid, String appName, Log.LogLevel logLevel) {
      this.mName = name.trim();
      this.mTag = tag.trim();
      this.mText = text.trim();
      this.mPid = pid.trim();
      this.mAppName = appName.trim();
      this.mLogLevel = logLevel;
      this.mCheckPid = !this.mPid.isEmpty();
      if(!this.mAppName.isEmpty()) {
         try {
            this.mAppNamePattern = Pattern.compile(this.mAppName, this.getPatternCompileFlags(this.mAppName));
            this.mCheckAppName = true;
         } catch (PatternSyntaxException var10) {
            this.mCheckAppName = false;
         }
      }

      if(!this.mTag.isEmpty()) {
         try {
            this.mTagPattern = Pattern.compile(this.mTag, this.getPatternCompileFlags(this.mTag));
            this.mCheckTag = true;
         } catch (PatternSyntaxException var9) {
            this.mCheckTag = false;
         }
      }

      if(!this.mText.isEmpty()) {
         try {
            this.mTextPattern = Pattern.compile(this.mText, this.getPatternCompileFlags(this.mText));
            this.mCheckText = true;
         } catch (PatternSyntaxException var8) {
            this.mCheckText = false;
         }
      }

   }

   private int getPatternCompileFlags(String regex) {
      char[] arr$ = regex.toCharArray();
      int len$ = arr$.length;

      for(int i$ = 0; i$ < len$; ++i$) {
         char c = arr$[i$];
         if(Character.isUpperCase(c)) {
            return 0;
         }
      }

      return 2;
   }

   public static List<LogCatFilter> fromString(String query, Log.LogLevel minLevel) {
      ArrayList filterSettings = new ArrayList();
      String[] arr$ = query.trim().split(" ");
      int len$ = arr$.length;

      for(int i$ = 0; i$ < len$; ++i$) {
         String s = arr$[i$];
         String tag = "";
         String text = "";
         String pid = "";
         String app = "";
         if(s.startsWith("pid:")) {
            pid = s.substring("pid:".length());
         } else if(s.startsWith("app:")) {
            app = s.substring("app:".length());
         } else if(s.startsWith("tag:")) {
            tag = s.substring("tag:".length());
         } else if(s.startsWith("text:")) {
            text = s.substring("text:".length());
         } else {
            text = s;
         }

         filterSettings.add(new LogCatFilter("livefilter-" + s, tag, text, pid, app, minLevel));
      }

      return filterSettings;
   }

   public String getName() {
      return this.mName;
   }

   public String getTag() {
      return this.mTag;
   }

   public String getText() {
      return this.mText;
   }

   public String getPid() {
      return this.mPid;
   }

   public String getAppName() {
      return this.mAppName;
   }

   public Log.LogLevel getLogLevel() {
      return this.mLogLevel;
   }

   public boolean matches(LogCatMessage m) {
      if(m.getLogLevel().getPriority() < this.mLogLevel.getPriority()) {
         return false;
      } else if(this.mCheckPid && !m.getPid().equals(this.mPid)) {
         return false;
      } else {
         Matcher matcher;
         if(this.mCheckAppName) {
            matcher = this.mAppNamePattern.matcher(m.getAppName());
            if(!matcher.find()) {
               return false;
            }
         }

         if(this.mCheckTag) {
            matcher = this.mTagPattern.matcher(m.getTag());
            if(!matcher.find()) {
               return false;
            }
         }

         if(this.mCheckText) {
            matcher = this.mTextPattern.matcher(m.getMessage());
            if(!matcher.find()) {
               return false;
            }
         }

         return true;
      }
   }
}
