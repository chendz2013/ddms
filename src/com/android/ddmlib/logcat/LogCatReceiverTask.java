package com.android.ddmlib.logcat;

import com.android.ddmlib.AdbCommandRejectedException;
import com.android.ddmlib.IDevice;
import com.android.ddmlib.Log;
import com.android.ddmlib.MultiLineReceiver;
import com.android.ddmlib.ShellCommandUnresponsiveException;
import com.android.ddmlib.TimeoutException;
import com.android.ddmlib.logcat.LogCatListener;
import com.android.ddmlib.logcat.LogCatMessage;
import com.android.ddmlib.logcat.LogCatMessageParser;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

public class LogCatReceiverTask implements Runnable {
   private static final String LOGCAT_COMMAND = "logcat -v long";
   private static final int DEVICE_POLL_INTERVAL_MSEC = 1000;
   private static final LogCatMessage sDeviceDisconnectedMsg = errorMessage("Device disconnected: 1");
   private static final LogCatMessage sConnectionTimeoutMsg = errorMessage("LogCat Connection timed out");
   private static final LogCatMessage sConnectionErrorMsg = errorMessage("LogCat Connection error");
   private final IDevice mDevice;
   private final LogCatReceiverTask.LogCatOutputReceiver mReceiver;
   private final LogCatMessageParser mParser;
   private final AtomicBoolean mCancelled;
   private final Set<LogCatListener> mListeners = new HashSet();

   public LogCatReceiverTask(IDevice device) {
      this.mDevice = device;
      this.mReceiver = new LogCatReceiverTask.LogCatOutputReceiver();
      this.mParser = new LogCatMessageParser();
      this.mCancelled = new AtomicBoolean();
   }

   public void run() {
      while(!this.mDevice.isOnline()) {
         try {
            Thread.sleep(1000L);
         } catch (InterruptedException var2) {
            return;
         }
      }

      try {
         this.mDevice.executeShellCommand("logcat -v long", this.mReceiver, 0);
      } catch (TimeoutException var3) {
         this.notifyListeners(Collections.singletonList(sConnectionTimeoutMsg));
      } catch (AdbCommandRejectedException var4) {
         ;
      } catch (ShellCommandUnresponsiveException var5) {
         ;
      } catch (IOException var6) {
         this.notifyListeners(Collections.singletonList(sConnectionErrorMsg));
      }

      this.notifyListeners(Collections.singletonList(sDeviceDisconnectedMsg));
   }

   public void stop() {
      this.mCancelled.set(true);
   }

   public synchronized void addLogCatListener(LogCatListener l) {
      this.mListeners.add(l);
   }

   public synchronized void removeLogCatListener(LogCatListener l) {
      this.mListeners.remove(l);
   }

   private synchronized void notifyListeners(List<LogCatMessage> messages) {
      Iterator i$ = this.mListeners.iterator();

      while(i$.hasNext()) {
         LogCatListener l = (LogCatListener)i$.next();
         l.log(messages);
      }

   }

   private static LogCatMessage errorMessage(String msg) {
      return new LogCatMessage(Log.LogLevel.ERROR, "", "", "", "", "", msg);
   }

   private class LogCatOutputReceiver extends MultiLineReceiver {
      public LogCatOutputReceiver() {
         this.setTrimLine(false);
      }

      public boolean isCancelled() {
         return LogCatReceiverTask.this.mCancelled.get();
      }

      public void processNewLines(String[] lines) {
         if(!LogCatReceiverTask.this.mCancelled.get()) {
            this.processLogLines(lines);
         }

      }

      private void processLogLines(String[] lines) {
         List newMessages = LogCatReceiverTask.this.mParser.processLogLines(lines, LogCatReceiverTask.this.mDevice);
         if(!newMessages.isEmpty()) {
            LogCatReceiverTask.this.notifyListeners(newMessages);
         }

      }
   }
}
