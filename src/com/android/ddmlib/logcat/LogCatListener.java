package com.android.ddmlib.logcat;

import com.android.ddmlib.logcat.LogCatMessage;
import java.util.List;

public interface LogCatListener {
   void log(List<LogCatMessage> var1);
}
