package com.android.ddmlib.logcat;

import com.android.ddmlib.Log;

public final class LogCatMessage {
   private final Log.LogLevel mLogLevel;
   private final String mPid;
   private final String mTid;
   private final String mAppName;
   private final String mTag;
   private final String mTime;
   private final String mMessage;

   public LogCatMessage(Log.LogLevel logLevel, String pid, String tid, String appName, String tag, String time, String msg) {
      this.mLogLevel = logLevel;
      this.mPid = pid;
      this.mAppName = appName;
      this.mTag = tag;
      this.mTime = time;
      this.mMessage = msg;

      long tidValue;
      try {
         tidValue = Long.decode(tid.trim()).longValue();
      } catch (NumberFormatException var11) {
         tidValue = -1L;
      }

      this.mTid = Long.toString(tidValue);
   }

   public Log.LogLevel getLogLevel() {
      return this.mLogLevel;
   }

   public String getPid() {
      return this.mPid;
   }

   public String getTid() {
      return this.mTid;
   }

   public String getAppName() {
      return this.mAppName;
   }

   public String getTag() {
      return this.mTag;
   }

   public String getTime() {
      return this.mTime;
   }

   public String getMessage() {
      return this.mMessage;
   }

   public String toString() {
      return this.mTime + ": " + this.mLogLevel.getPriorityLetter() + "/" + this.mTag + "(" + this.mPid + "): " + this.mMessage;
   }
}
