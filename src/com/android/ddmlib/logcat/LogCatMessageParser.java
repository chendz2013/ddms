package com.android.ddmlib.logcat;

import com.android.ddmlib.IDevice;
import com.android.ddmlib.Log;
import com.android.ddmlib.logcat.LogCatMessage;
import com.google.common.primitives.Ints;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class LogCatMessageParser {
   private Log.LogLevel mCurLogLevel;
   private String mCurPid;
   private String mCurTid;
   private String mCurTag;
   private String mCurTime;
   private static final Pattern sLogHeaderPattern = Pattern.compile("^\\[\\s(\\d\\d-\\d\\d\\s\\d\\d:\\d\\d:\\d\\d\\.\\d+)\\s+(\\d*):\\s*(\\S+)\\s([VDIWEAF])/(.*)\\]$");

   public LogCatMessageParser() {
      this.mCurLogLevel = Log.LogLevel.WARN;
      this.mCurPid = "?";
      this.mCurTid = "?";
      this.mCurTag = "?";
      this.mCurTime = "?:??";
   }

   public List<LogCatMessage> processLogLines(String[] lines, IDevice device) {
      ArrayList messages = new ArrayList(lines.length);
      String[] arr$ = lines;
      int len$ = lines.length;

      for(int i$ = 0; i$ < len$; ++i$) {
         String line = arr$[i$];
         if(!line.isEmpty()) {
            Matcher matcher = sLogHeaderPattern.matcher(line);
            if(matcher.matches()) {
               this.mCurTime = matcher.group(1);
               this.mCurPid = matcher.group(2);
               this.mCurTid = matcher.group(3);
               this.mCurLogLevel = Log.LogLevel.getByLetterString(matcher.group(4));
               this.mCurTag = matcher.group(5).trim();
               if(this.mCurLogLevel == null && matcher.group(4).equals("F")) {
                  this.mCurLogLevel = Log.LogLevel.ASSERT;
               }
            } else {
               String pkgName = "";
               Integer pid = Ints.tryParse(this.mCurPid);
               if(pid != null && device != null) {
                  pkgName = device.getClientName(pid.intValue());
               }

               LogCatMessage m = new LogCatMessage(this.mCurLogLevel, this.mCurPid, this.mCurTid, pkgName, this.mCurTag, this.mCurTime, line);
               messages.add(m);
            }
         }
      }

      return messages;
   }
}
