package com.android.ddmlib;

import com.android.ddmlib.AdbCommandRejectedException;
import com.android.ddmlib.DdmPreferences;
import com.android.ddmlib.Device;
import com.android.ddmlib.IDevice;
import com.android.ddmlib.IShellOutputReceiver;
import com.android.ddmlib.Log;
import com.android.ddmlib.RawImage;
import com.android.ddmlib.ShellCommandUnresponsiveException;
import com.android.ddmlib.TimeoutException;
import com.android.ddmlib.log.LogReceiver;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.SocketChannel;
import java.util.concurrent.TimeUnit;

final class AdbHelper {
   static final int WAIT_TIME = 5;
   static final String DEFAULT_ENCODING = "ISO-8859-1";

   public static SocketChannel open(InetSocketAddress adbSockAddr, Device device, int devicePort) throws IOException, TimeoutException, AdbCommandRejectedException {
      SocketChannel adbChan = SocketChannel.open(adbSockAddr);

      try {
         adbChan.socket().setTcpNoDelay(true);
         adbChan.configureBlocking(false);
         setDevice(adbChan, device);
         byte[] e = createAdbForwardRequest((String)null, devicePort);
         write(adbChan, e);
         AdbHelper.AdbResponse resp = readAdbResponse(adbChan, false);
         if(!resp.okay) {
            throw new AdbCommandRejectedException(resp.message);
         } else {
            adbChan.configureBlocking(true);
            return adbChan;
         }
      } catch (TimeoutException var6) {
         adbChan.close();
         throw var6;
      } catch (IOException var7) {
         adbChan.close();
         throw var7;
      } catch (AdbCommandRejectedException var8) {
         adbChan.close();
         throw var8;
      }
   }

   public static SocketChannel createPassThroughConnection(InetSocketAddress adbSockAddr, Device device, int pid) throws TimeoutException, AdbCommandRejectedException, IOException {
      SocketChannel adbChan = SocketChannel.open(adbSockAddr);

      try {
         adbChan.socket().setTcpNoDelay(true);
         adbChan.configureBlocking(false);
         setDevice(adbChan, device);
         byte[] e = createJdwpForwardRequest(pid);
         write(adbChan, e);
         AdbHelper.AdbResponse resp = readAdbResponse(adbChan, false);
         if(!resp.okay) {
            throw new AdbCommandRejectedException(resp.message);
         } else {
            adbChan.configureBlocking(true);
            return adbChan;
         }
      } catch (TimeoutException var6) {
         adbChan.close();
         throw var6;
      } catch (IOException var7) {
         adbChan.close();
         throw var7;
      } catch (AdbCommandRejectedException var8) {
         adbChan.close();
         throw var8;
      }
   }

   private static byte[] createAdbForwardRequest(String addrStr, int port) {
      String reqStr;
      if(addrStr == null) {
         reqStr = "tcp:" + port;
      } else {
         reqStr = "tcp:" + port + ":" + addrStr;
      }

      return formAdbRequest(reqStr);
   }

   private static byte[] createJdwpForwardRequest(int pid) {
      String reqStr = String.format("jdwp:%1$d", new Object[]{Integer.valueOf(pid)});
      return formAdbRequest(reqStr);
   }

   public static byte[] formAdbRequest(String req) {
      String resultStr = String.format("%04X%s", new Object[]{Integer.valueOf(req.length()), req});

      byte[] result;
      try {
         result = resultStr.getBytes("ISO-8859-1");
      } catch (UnsupportedEncodingException var4) {
         var4.printStackTrace();
         return null;
      }

      assert result.length == req.length() + 4;

      return result;
   }

   static AdbHelper.AdbResponse readAdbResponse(SocketChannel chan, boolean readDiagString) throws TimeoutException, IOException {
      AdbHelper.AdbResponse resp = new AdbHelper.AdbResponse();
      byte[] reply = new byte[4];
      read(chan, reply);
      if(isOkay(reply)) {
         resp.okay = true;
      } else {
         readDiagString = true;
         resp.okay = false;
      }

      try {
         if(readDiagString) {
            byte[] e = new byte[4];
            read(chan, e);
            String lenStr = replyToString(e);

            int len;
            try {
               len = Integer.parseInt(lenStr, 16);
            } catch (NumberFormatException var8) {
               Log.w("ddms", "Expected digits, got \'" + lenStr + "\': " + e[0] + " " + e[1] + " " + e[2] + " " + e[3]);
               Log.w("ddms", "reply was " + replyToString(reply));
               return resp;
            }

            byte[] msg = new byte[len];
            read(chan, msg);
            resp.message = replyToString(msg);
            Log.v("ddms", "Got reply \'" + replyToString(reply) + "\', diag=\'" + resp.message + "\'");
         }
      } catch (Exception var9) {
         ;
      }

      return resp;
   }

   static RawImage getFrameBuffer(InetSocketAddress adbSockAddr, Device device, long timeout, TimeUnit unit) throws TimeoutException, AdbCommandRejectedException, IOException {
      RawImage imageParams = new RawImage();
      byte[] request = formAdbRequest("framebuffer:");
      byte[] nudge = new byte[]{(byte)0};
      SocketChannel adbChan = null;

      Object var14;
      try {
         adbChan = SocketChannel.open(adbSockAddr);
         adbChan.configureBlocking(false);
         setDevice(adbChan, device);
         write(adbChan, request);
         AdbHelper.AdbResponse resp = readAdbResponse(adbChan, false);
         if(!resp.okay) {
            throw new AdbCommandRejectedException(resp.message);
         }

         byte[] reply = new byte[4];
         read(adbChan, reply);
         ByteBuffer buf = ByteBuffer.wrap(reply);
         buf.order(ByteOrder.LITTLE_ENDIAN);
         int version = buf.getInt();
         int headerSize = RawImage.getHeaderSize(version);
         reply = new byte[headerSize * 4];
         read(adbChan, reply);
         buf = ByteBuffer.wrap(reply);
         buf.order(ByteOrder.LITTLE_ENDIAN);
         if(imageParams.readHeader(version, buf)) {
            Log.d("ddms", "image params: bpp=" + imageParams.bpp + ", size=" + imageParams.size + ", width=" + imageParams.width + ", height=" + imageParams.height);
            write(adbChan, nudge);
            reply = new byte[imageParams.size];
            read(adbChan, reply, imageParams.size, unit.toMillis(timeout));
            imageParams.data = reply;
            return imageParams;
         }

         Log.e("Screenshot", "Unsupported protocol: " + version);
         var14 = null;
      } finally {
         if(adbChan != null) {
            adbChan.close();
         }

      }

      return (RawImage)var14;
   }

   /** @deprecated */
   @Deprecated
   static void executeRemoteCommand(InetSocketAddress adbSockAddr, String command, IDevice device, IShellOutputReceiver rcvr, int maxTimeToOutputResponse) throws TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException, IOException {
      executeRemoteCommand(adbSockAddr, command, device, rcvr, (long)maxTimeToOutputResponse, TimeUnit.MILLISECONDS);
   }

   static void executeRemoteCommand(InetSocketAddress adbSockAddr, String command, IDevice device, IShellOutputReceiver rcvr, long maxTimeToOutputResponse, TimeUnit maxTimeUnits) throws TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException, IOException {
      executeRemoteCommand(adbSockAddr, AdbHelper.AdbService.SHELL, command, device, rcvr, maxTimeToOutputResponse, maxTimeUnits, (InputStream)null);
   }

   static void executeRemoteCommand(InetSocketAddress adbSockAddr, AdbHelper.AdbService adbService, String command, IDevice device, IShellOutputReceiver rcvr, long maxTimeToOutputResponse, TimeUnit maxTimeUnits, InputStream is) throws TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException, IOException {
      long maxTimeToOutputMs = 0L;
      if(maxTimeToOutputResponse > 0L) {
         if(maxTimeUnits == null) {
            throw new NullPointerException("Time unit must not be null for non-zero max.");
         }

         maxTimeToOutputMs = maxTimeUnits.toMillis(maxTimeToOutputResponse);
      }

      Log.v("ddms", "execute: running " + command);
      SocketChannel adbChan = null;

      try {
         adbChan = SocketChannel.open(adbSockAddr);
         adbChan.configureBlocking(false);
         setDevice(adbChan, device);
         byte[] request = formAdbRequest(adbService.name().toLowerCase() + ":" + command);
         write(adbChan, request);
         AdbHelper.AdbResponse resp = readAdbResponse(adbChan, false);
         if(!resp.okay) {
            Log.e("ddms", "ADB rejected shell command (" + command + "): " + resp.message);
            throw new AdbCommandRejectedException(resp.message);
         }

         byte[] data = new byte[16384];
         int buf;
         if(is != null) {
            while((buf = is.read(data)) != -1) {
               ByteBuffer timeToResponseCount = ByteBuffer.wrap(data, 0, buf);

               int written;
               for(written = 0; timeToResponseCount.hasRemaining(); written += adbChan.write(timeToResponseCount)) {
                  ;
               }

               if(written != buf) {
                  Log.e("ddms", "ADB write inconsistency, wrote " + written + "expected " + buf);
                  throw new AdbCommandRejectedException("write failed");
               }
            }
         }

         ByteBuffer buf1 = ByteBuffer.wrap(data);
         buf1.clear();
         long timeToResponseCount1 = 0L;

         while(true) {
            if(rcvr != null && rcvr.isCancelled()) {
               Log.v("ddms", "execute: cancelled");
               break;
            }

            int count = adbChan.read(buf1);
            if(count < 0) {
               rcvr.flush();
               Log.v("ddms", "execute \'" + command + "\' on \'" + device + "\' : EOF hit. Read: " + count);
               break;
            }

            if(count == 0) {
               try {
                  byte e = 25;
                  timeToResponseCount1 += (long)e;
                  if(maxTimeToOutputMs > 0L && timeToResponseCount1 > maxTimeToOutputMs) {
                     throw new ShellCommandUnresponsiveException();
                  }

                  Thread.sleep((long)e);
               } catch (InterruptedException var23) {
                  Thread.currentThread().interrupt();
                  throw new TimeoutException("executeRemoteCommand interrupted with immediate timeout via interruption.");
               }
            } else {
               timeToResponseCount1 = 0L;
               if(rcvr != null) {
                  rcvr.addOutput(buf1.array(), buf1.arrayOffset(), buf1.position());
               }

               buf1.rewind();
            }
         }
      } finally {
         if(adbChan != null) {
            adbChan.close();
         }

         Log.v("ddms", "execute: returning");
      }

   }

   public static void runEventLogService(InetSocketAddress adbSockAddr, Device device, LogReceiver rcvr) throws TimeoutException, AdbCommandRejectedException, IOException {
      runLogService(adbSockAddr, device, "events", rcvr);
   }

   public static void runLogService(InetSocketAddress adbSockAddr, Device device, String logName, LogReceiver rcvr) throws TimeoutException, AdbCommandRejectedException, IOException {
      SocketChannel adbChan = null;

      try {
         adbChan = SocketChannel.open(adbSockAddr);
         adbChan.configureBlocking(false);
         setDevice(adbChan, device);
         byte[] request = formAdbRequest("log:" + logName);
         write(adbChan, request);
         AdbHelper.AdbResponse resp = readAdbResponse(adbChan, false);
         if(!resp.okay) {
            throw new AdbCommandRejectedException(resp.message);
         }

         byte[] data = new byte[16384];
         ByteBuffer buf = ByteBuffer.wrap(data);

         while(rcvr == null || !rcvr.isCancelled()) {
            int count = adbChan.read(buf);
            if(count < 0) {
               break;
            }

            if(count == 0) {
               try {
                  Thread.sleep(25L);
               } catch (InterruptedException var14) {
                  Thread.currentThread().interrupt();
                  throw new TimeoutException("runLogService interrupted with immediate timeout via interruption.");
               }
            } else {
               if(rcvr != null) {
                  rcvr.parseNewData(buf.array(), buf.arrayOffset(), buf.position());
               }

               buf.rewind();
            }
         }
      } finally {
         if(adbChan != null) {
            adbChan.close();
         }

      }

   }

   public static void createForward(InetSocketAddress adbSockAddr, Device device, String localPortSpec, String remotePortSpec) throws TimeoutException, AdbCommandRejectedException, IOException {
      SocketChannel adbChan = null;

      try {
         adbChan = SocketChannel.open(adbSockAddr);
         adbChan.configureBlocking(false);
         byte[] request = formAdbRequest(String.format("host-serial:%1$s:forward:%2$s;%3$s", new Object[]{device.getSerialNumber(), localPortSpec, remotePortSpec}));
         write(adbChan, request);
         AdbHelper.AdbResponse resp = readAdbResponse(adbChan, false);
         if(!resp.okay) {
            Log.w("create-forward", "Error creating forward: " + resp.message);
            throw new AdbCommandRejectedException(resp.message);
         }
      } finally {
         if(adbChan != null) {
            adbChan.close();
         }

      }

   }

   public static void removeForward(InetSocketAddress adbSockAddr, Device device, String localPortSpec, String remotePortSpec) throws TimeoutException, AdbCommandRejectedException, IOException {
      SocketChannel adbChan = null;

      try {
         adbChan = SocketChannel.open(adbSockAddr);
         adbChan.configureBlocking(false);
         byte[] request = formAdbRequest(String.format("host-serial:%1$s:killforward:%2$s", new Object[]{device.getSerialNumber(), localPortSpec}));
         write(adbChan, request);
         AdbHelper.AdbResponse resp = readAdbResponse(adbChan, false);
         if(!resp.okay) {
            Log.w("remove-forward", "Error creating forward: " + resp.message);
            throw new AdbCommandRejectedException(resp.message);
         }
      } finally {
         if(adbChan != null) {
            adbChan.close();
         }

      }

   }

   static boolean isOkay(byte[] reply) {
      return reply[0] == 79 && reply[1] == 75 && reply[2] == 65 && reply[3] == 89;
   }

   static String replyToString(byte[] reply) {
      String result;
      try {
         result = new String(reply, "ISO-8859-1");
      } catch (UnsupportedEncodingException var3) {
         var3.printStackTrace();
         result = "";
      }

      return result;
   }

   static void read(SocketChannel chan, byte[] data) throws TimeoutException, IOException {
      read(chan, data, -1, (long)DdmPreferences.getTimeOut());
   }

   static void read(SocketChannel chan, byte[] data, int length, long timeout) throws TimeoutException, IOException {
      ByteBuffer buf = ByteBuffer.wrap(data, 0, length != -1?length:data.length);
      int numWaits = 0;

      while(buf.position() != buf.limit()) {
         int count = chan.read(buf);
         if(count < 0) {
            Log.d("ddms", "read: channel EOF");
            throw new IOException("EOF");
         }

         if(count == 0) {
            if(timeout != 0L && (long)(numWaits * 5) > timeout) {
               Log.d("ddms", "read: timeout");
               throw new TimeoutException();
            }

            try {
               Thread.sleep(5L);
            } catch (InterruptedException var9) {
               Thread.currentThread().interrupt();
               throw new TimeoutException("Read interrupted with immediate timeout via interruption.");
            }

            ++numWaits;
         } else {
            numWaits = 0;
         }
      }

   }

   static void write(SocketChannel chan, byte[] data) throws TimeoutException, IOException {
      write(chan, data, -1, DdmPreferences.getTimeOut());
   }

   static void write(SocketChannel chan, byte[] data, int length, int timeout) throws TimeoutException, IOException {
      ByteBuffer buf = ByteBuffer.wrap(data, 0, length != -1?length:data.length);
      int numWaits = 0;

      while(buf.position() != buf.limit()) {
         int count = chan.write(buf);
         if(count < 0) {
            Log.d("ddms", "write: channel EOF");
            throw new IOException("channel EOF");
         }

         if(count == 0) {
            if(timeout != 0 && numWaits * 5 > timeout) {
               Log.d("ddms", "write: timeout");
               throw new TimeoutException();
            }

            try {
               Thread.sleep(5L);
            } catch (InterruptedException var8) {
               Thread.currentThread().interrupt();
               throw new TimeoutException("Write interrupted with immediate timeout via interruption.");
            }

            ++numWaits;
         } else {
            numWaits = 0;
         }
      }

   }

   static void setDevice(SocketChannel adbChan, IDevice device) throws TimeoutException, AdbCommandRejectedException, IOException {
      if(device != null) {
         String msg = "host:transport:" + device.getSerialNumber();
         byte[] device_query = formAdbRequest(msg);
         write(adbChan, device_query);
         AdbHelper.AdbResponse resp = readAdbResponse(adbChan, false);
         if(!resp.okay) {
            throw new AdbCommandRejectedException(resp.message, true);
         }
      }

   }

   public static void reboot(String into, InetSocketAddress adbSockAddr, Device device) throws TimeoutException, AdbCommandRejectedException, IOException {
      byte[] request;
      if(into == null) {
         request = formAdbRequest("reboot:");
      } else {
         request = formAdbRequest("reboot:" + into);
      }

      SocketChannel adbChan = null;

      try {
         adbChan = SocketChannel.open(adbSockAddr);
         adbChan.configureBlocking(false);
         setDevice(adbChan, device);
         write(adbChan, request);
      } finally {
         if(adbChan != null) {
            adbChan.close();
         }

      }

   }

   public static enum AdbService {
      SHELL,
      EXEC;
   }

   static class AdbResponse {
      public boolean okay;
      public String message = "";
   }
}
