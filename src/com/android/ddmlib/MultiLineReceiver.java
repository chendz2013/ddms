package com.android.ddmlib;

import com.android.ddmlib.IShellOutputReceiver;
import com.google.common.base.Charsets;
import java.util.ArrayList;

public abstract class MultiLineReceiver implements IShellOutputReceiver {
   private boolean mTrimLines = true;
   private String mUnfinishedLine = null;
   private final ArrayList<String> mArray = new ArrayList();

   public void setTrimLine(boolean trim) {
      this.mTrimLines = trim;
   }

   public final void addOutput(byte[] data, int offset, int length) {
      if(!this.isCancelled()) {
         String s = new String(data, offset, length, Charsets.UTF_8);
         if(this.mUnfinishedLine != null) {
            s = this.mUnfinishedLine + s;
            this.mUnfinishedLine = null;
         }

         this.mArray.clear();
         int start = 0;

         while(true) {
            int lines = s.indexOf(10, start);
            if(lines == -1) {
               this.mUnfinishedLine = s.substring(start);
               if(!this.mArray.isEmpty()) {
                  String[] var9 = (String[])this.mArray.toArray(new String[this.mArray.size()]);
                  this.processNewLines(var9);
               }
               break;
            }

            byte newlineLength = 1;
            if(lines > 0 && s.charAt(lines - 1) == 13) {
               --lines;
               newlineLength = 2;
            }

            String line = s.substring(start, lines);
            if(this.mTrimLines) {
               line = line.trim();
            }

            this.mArray.add(line);
            start = lines + newlineLength;
         }
      }

   }

   public final void flush() {
      if(this.mUnfinishedLine != null) {
         this.processNewLines(new String[]{this.mUnfinishedLine});
      }

      this.done();
   }

   public void done() {
   }

   public abstract void processNewLines(String[] var1);
}
