package com.android.ddmlib;

import com.android.ddmlib.AdbCommandRejectedException;
import com.android.ddmlib.IDevice;
import com.android.ddmlib.Log;
import com.android.ddmlib.MultiLineReceiver;
import com.android.ddmlib.ShellCommandUnresponsiveException;
import com.android.ddmlib.TimeoutException;
import com.google.common.util.concurrent.SettableFuture;
import java.io.IOException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class BatteryFetcher {
   private static final String LOG_TAG = "BatteryFetcher";
   private static final long FETCH_BACKOFF_MS = 5000L;
   private static final long BATTERY_TIMEOUT = 2000L;
   private Integer mBatteryLevel = null;
   private final IDevice mDevice;
   private long mLastSuccessTime = 0L;
   private SettableFuture<Integer> mPendingRequest = null;

   public BatteryFetcher(IDevice device) {
      this.mDevice = device;
   }

   public synchronized Future<Integer> getBattery(long freshness, TimeUnit timeUnit) {
      SettableFuture result;
      if(this.mBatteryLevel != null && !this.isFetchRequired(freshness, timeUnit)) {
         result = SettableFuture.create();
         result.set(this.mBatteryLevel);
      } else {
         if(this.mPendingRequest == null) {
            this.mPendingRequest = SettableFuture.create();
            this.initiateBatteryQuery();
         }

         result = this.mPendingRequest;
      }

      return result;
   }

   private boolean isFetchRequired(long freshness, TimeUnit timeUnit) {
      long freshnessMs = timeUnit.toMillis(freshness);
      return System.currentTimeMillis() - this.mLastSuccessTime > freshnessMs;
   }

   private void initiateBatteryQuery() {
      final String threadName = String.format("query-battery-%s", new Object[]{this.mDevice.getSerialNumber()});
      Thread fetchThread = new Thread(threadName) {
         public void run() {
            Object exception = null;

            try {
               BatteryFetcher.SysFsBatteryLevelReceiver e = new BatteryFetcher.SysFsBatteryLevelReceiver();
               BatteryFetcher.this.mDevice.executeShellCommand("cat /sys/class/power_supply/*/capacity", e, 2000L, TimeUnit.MILLISECONDS);
               if(!BatteryFetcher.this.setBatteryLevel(e.getBatteryLevel())) {
                  BatteryFetcher.BatteryReceiver receiver = new BatteryFetcher.BatteryReceiver(null);
                  BatteryFetcher.this.mDevice.executeShellCommand("dumpsys battery", receiver, 2000L, TimeUnit.MILLISECONDS);
                  if(BatteryFetcher.this.setBatteryLevel(receiver.getBatteryLevel())) {
                     return;
                  }
               }

               exception = new IOException("Unrecognized response to battery level queries");
            } catch (TimeoutException var4) {
               exception = var4;
            } catch (AdbCommandRejectedException var5) {
               exception = var5;
            } catch (ShellCommandUnresponsiveException var6) {
               exception = var6;
            } catch (IOException var7) {
               exception = var7;
            }

            BatteryFetcher.this.handleBatteryLevelFailure((Exception)exception);
         }
      };
      fetchThread.setDaemon(true);
      fetchThread.start();
   }

   private synchronized boolean setBatteryLevel(Integer batteryLevel) {
      if(batteryLevel == null) {
         return false;
      } else {
         this.mLastSuccessTime = System.currentTimeMillis();
         this.mBatteryLevel = batteryLevel;
         if(this.mPendingRequest != null) {
            this.mPendingRequest.set(this.mBatteryLevel);
         }

         this.mPendingRequest = null;
         return true;
      }
   }

   private synchronized void handleBatteryLevelFailure(Exception e) {
      Log.w("BatteryFetcher", String.format("%s getting battery level for device %s: %s", new Object[]{e.getClass().getSimpleName(), this.mDevice.getSerialNumber(), e.getMessage()}));
      if(this.mPendingRequest != null && !this.mPendingRequest.setException(e)) {
         Log.e("BatteryFetcher", "Future.setException failed");
         this.mPendingRequest.set(null);
      }

      this.mPendingRequest = null;
   }

   private static final class BatteryReceiver extends MultiLineReceiver {
      private static final Pattern BATTERY_LEVEL = Pattern.compile("\\s*level: (\\d+)");
      private static final Pattern SCALE = Pattern.compile("\\s*scale: (\\d+)");
      private Integer mBatteryLevel;
      private Integer mBatteryScale;

      private BatteryReceiver() {
         this.mBatteryLevel = null;
         this.mBatteryScale = null;
      }

      public Integer getBatteryLevel() {
         return this.mBatteryLevel != null && this.mBatteryScale != null?Integer.valueOf(this.mBatteryLevel.intValue() * 100 / this.mBatteryScale.intValue()):null;
      }

      public void processNewLines(String[] lines) {
         String[] arr$ = lines;
         int len$ = lines.length;

         for(int i$ = 0; i$ < len$; ++i$) {
            String line = arr$[i$];
            Matcher batteryMatch = BATTERY_LEVEL.matcher(line);
            if(batteryMatch.matches()) {
               try {
                  this.mBatteryLevel = Integer.valueOf(Integer.parseInt(batteryMatch.group(1)));
               } catch (NumberFormatException var9) {
                  Log.w("BatteryFetcher", String.format("Failed to parse %s as an integer", new Object[]{batteryMatch.group(1)}));
               }
            }

            Matcher scaleMatch = SCALE.matcher(line);
            if(scaleMatch.matches()) {
               try {
                  this.mBatteryScale = Integer.valueOf(Integer.parseInt(scaleMatch.group(1)));
               } catch (NumberFormatException var10) {
                  Log.w("BatteryFetcher", String.format("Failed to parse %s as an integer", new Object[]{batteryMatch.group(1)}));
               }
            }
         }

      }

      public boolean isCancelled() {
         return false;
      }

      // $FF: synthetic method
      BatteryReceiver(Object x0) {
         this();
      }
   }

   static final class SysFsBatteryLevelReceiver extends MultiLineReceiver {
      private static final Pattern BATTERY_LEVEL = Pattern.compile("^(\\d+)[.\\s]*");
      private Integer mBatteryLevel = null;

      public Integer getBatteryLevel() {
         return this.mBatteryLevel;
      }

      public boolean isCancelled() {
         return false;
      }

      public void processNewLines(String[] lines) {
         String[] arr$ = lines;
         int len$ = lines.length;

         for(int i$ = 0; i$ < len$; ++i$) {
            String line = arr$[i$];
            Matcher batteryMatch = BATTERY_LEVEL.matcher(line);
            if(batteryMatch.matches()) {
               if(this.mBatteryLevel == null) {
                  this.mBatteryLevel = Integer.valueOf(Integer.parseInt(batteryMatch.group(1)));
               } else {
                  Integer tmpLevel = Integer.valueOf(Integer.parseInt(batteryMatch.group(1)));
                  if(!this.mBatteryLevel.equals(tmpLevel)) {
                     Log.w("BatteryFetcher", String.format("Multiple lines matched with different value; Original: %s, Current: %s (keeping original)", new Object[]{this.mBatteryLevel.toString(), tmpLevel.toString()}));
                  }
               }
            }
         }

      }
   }
}
