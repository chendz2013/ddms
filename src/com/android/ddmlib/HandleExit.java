package com.android.ddmlib;

import com.android.ddmlib.ChunkHandler;
import com.android.ddmlib.Client;
import com.android.ddmlib.JdwpPacket;
import com.android.ddmlib.Log;
import com.android.ddmlib.MonitorThread;
import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * 发送一个exit的命令
 */
final class HandleExit extends ChunkHandler {
   public static final int CHUNK_EXIT = type("EXIT");
   private static final HandleExit mInst = new HandleExit();

   public static void register(MonitorThread mt) {
   }

   public void clientReady(Client client) throws IOException {
   }

   public void clientDisconnected(Client client) {
   }

   public void handleChunk(Client client, int type, ByteBuffer data, boolean isReply, int msgId) {
      this.handleUnknownChunk(client, type, data, isReply, msgId);
   }

   public static void sendEXIT(Client client, int status) throws IOException {
      ByteBuffer rawBuf = allocBuffer(4);
      JdwpPacket packet = new JdwpPacket(rawBuf);
      ByteBuffer buf = getChunkDataBuf(rawBuf);
      buf.putInt(status);
      finishChunkPacket(packet, CHUNK_EXIT, buf.position());
      Log.d("ddm-exit", "Sending " + name(CHUNK_EXIT) + ": " + status);
      client.sendAndConsume(packet, mInst);
   }
}
