package com.android.ddmlib;

import com.android.ddmlib.AdbCommandRejectedException;
import com.android.ddmlib.Device;
import com.android.ddmlib.IDevice;
import com.android.ddmlib.MultiLineReceiver;
import com.android.ddmlib.ShellCommandUnresponsiveException;
import com.android.ddmlib.TimeoutException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class FileListingService {
   private static final Pattern sApkPattern = Pattern.compile(".*\\.apk", 2);
   private static final String PM_FULL_LISTING = "pm list packages -f";
   private static final Pattern sPmPattern = Pattern.compile("^package:(.+?)=(.+)$");
   public static final String DIRECTORY_DATA = "data";
   public static final String DIRECTORY_SDCARD = "sdcard";
   public static final String DIRECTORY_MNT = "mnt";
   public static final String DIRECTORY_SYSTEM = "system";
   public static final String DIRECTORY_TEMP = "tmp";
   public static final String DIRECTORY_APP = "app";
   public static final long REFRESH_RATE = 5000L;
   static final long REFRESH_TEST = 4000L;
   public static final int TYPE_FILE = 0;
   public static final int TYPE_DIRECTORY = 1;
   public static final int TYPE_DIRECTORY_LINK = 2;
   public static final int TYPE_BLOCK = 3;
   public static final int TYPE_CHARACTER = 4;
   public static final int TYPE_LINK = 5;
   public static final int TYPE_SOCKET = 6;
   public static final int TYPE_FIFO = 7;
   public static final int TYPE_OTHER = 8;
   public static final String FILE_SEPARATOR = "/";
   private static final String FILE_ROOT = "/";
   private static final Pattern LS_L_PATTERN = Pattern.compile("^([bcdlsp-][-r][-w][-xsS][-r][-w][-xsS][-r][-w][-xstST])\\s+(\\S+)\\s+(\\S+)\\s+([\\d\\s,]*)\\s+(\\d{4}-\\d\\d-\\d\\d)\\s+(\\d\\d:\\d\\d)\\s+(.*)$");
   private static final Pattern LS_LD_PATTERN = Pattern.compile("d[rwx-]{9}\\s+\\S+\\s+\\S+\\s+[0-9-]{10}\\s+\\d{2}:\\d{2}$");
   private Device mDevice;
   private FileListingService.FileEntry mRoot;
   private ArrayList<Thread> mThreadList = new ArrayList();

   FileListingService(Device device) {
      this.mDevice = device;
   }

   public FileListingService.FileEntry getRoot() {
      if(this.mDevice != null) {
         if(this.mRoot == null) {
            this.mRoot = new FileListingService.FileEntry((FileListingService.FileEntry)null, "", 1, true, null);
         }

         return this.mRoot;
      } else {
         return null;
      }
   }

   public FileListingService.FileEntry[] getChildren(final FileListingService.FileEntry entry, boolean useCache, final FileListingService.IListingReceiver receiver) {
      if(useCache && !entry.needFetch()) {
         return entry.getCachedChildren();
      } else if(receiver == null) {
         this.doLs(entry);
         return entry.getCachedChildren();
      } else {
         Thread t = new Thread("ls " + entry.getFullPath()) {
            public void run() {
               FileListingService.this.doLs(entry);
               receiver.setChildren(entry, entry.getCachedChildren());
               FileListingService.FileEntry[] children = entry.getCachedChildren();
               if(children.length > 0 && children[0].isApplicationPackage()) {
                  final HashMap map = new HashMap();
                  FileListingService.FileEntry[] t = children;
                  int e = children.length;

                  for(int i$ = 0; i$ < e; ++i$) {
                     FileListingService.FileEntry child = t[i$];
                     String path = child.getFullPath();
                     map.put(path, child);
                  }

                  String var11 = "pm list packages -f";

                  try {
                     FileListingService.this.mDevice.executeShellCommand(var11, new MultiLineReceiver() {
                        public void processNewLines(String[] lines) {
                           String[] arr$ = lines;
                           int len$ = lines.length;

                           for(int i$ = 0; i$ < len$; ++i$) {
                              String line = arr$[i$];
                              if(!line.isEmpty()) {
                                 Matcher m = FileListingService.sPmPattern.matcher(line);
                                 if(m.matches()) {
                                    FileListingService.FileEntry entryx = (FileListingService.FileEntry)map.get(m.group(1));
                                    if(entryx != null) {
                                       entryx.info = m.group(2);
                                       receiver.refreshEntry(entryx);
                                    }
                                 }
                              }
                           }

                        }

                        public boolean isCancelled() {
                           return false;
                        }
                     });
                  } catch (Exception var10) {
                     ;
                  }
               }

               synchronized(FileListingService.this.mThreadList) {
                  FileListingService.this.mThreadList.remove(this);
                  if(!FileListingService.this.mThreadList.isEmpty()) {
                     Thread var12 = (Thread)FileListingService.this.mThreadList.get(0);
                     var12.start();
                  }

               }
            }
         };
         ArrayList var5 = this.mThreadList;
         synchronized(this.mThreadList) {
            this.mThreadList.add(t);
            if(this.mThreadList.size() == 1) {
               t.start();
            }

            return null;
         }
      }
   }

   public FileListingService.FileEntry[] getChildrenSync(FileListingService.FileEntry entry) throws TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException, IOException {
      this.doLsAndThrow(entry);
      return entry.getCachedChildren();
   }

   private void doLs(FileListingService.FileEntry entry) {
      try {
         this.doLsAndThrow(entry);
      } catch (Exception var3) {
         ;
      }

   }

   private void doLsAndThrow(FileListingService.FileEntry entry) throws TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException, IOException {
      ArrayList entryList = new ArrayList();
      ArrayList linkList = new ArrayList();

      try {
         String command = "ls -l " + entry.getFullEscapedPath();
         if(entry.isDirectory()) {
            command = command + "/";
         }

         FileListingService.LsReceiver receiver = new FileListingService.LsReceiver(entry, entryList, linkList);
         this.mDevice.executeShellCommand(command, receiver);
         receiver.finishLinks(this.mDevice, entryList);
      } finally {
         entry.fetchTime = System.currentTimeMillis();
         Collections.sort(entryList, FileListingService.FileEntry.sEntryComparator);
         entry.setChildren(entryList);
      }

   }

   public interface IListingReceiver {
      void setChildren(FileListingService.FileEntry var1, FileListingService.FileEntry[] var2);

      void refreshEntry(FileListingService.FileEntry var1);
   }

   private static class LsReceiver extends MultiLineReceiver {
      private ArrayList<FileListingService.FileEntry> mEntryList;
      private ArrayList<String> mLinkList;
      private FileListingService.FileEntry[] mCurrentChildren;
      private FileListingService.FileEntry mParentEntry;

      public LsReceiver(FileListingService.FileEntry parentEntry, ArrayList<FileListingService.FileEntry> entryList, ArrayList<String> linkList) {
         this.mParentEntry = parentEntry;
         this.mCurrentChildren = parentEntry.getCachedChildren();
         this.mEntryList = entryList;
         this.mLinkList = linkList;
      }

      public void processNewLines(String[] lines) {
         String[] arr$ = lines;
         int len$ = lines.length;

         for(int i$ = 0; i$ < len$; ++i$) {
            String line = arr$[i$];
            if(!line.isEmpty()) {
               Matcher m = FileListingService.LS_L_PATTERN.matcher(line);
               if(m.matches()) {
                  String name = m.group(7);
                  String permissions = m.group(1);
                  String owner = m.group(2);
                  String group = m.group(3);
                  String size = m.group(4);
                  String date = m.group(5);
                  String time = m.group(6);
                  String info = null;
                  byte objectType = 8;
                  switch(permissions.charAt(0)) {
                  case '-':
                     objectType = 0;
                     break;
                  case 'b':
                     objectType = 3;
                     break;
                  case 'c':
                     objectType = 4;
                     break;
                  case 'd':
                     objectType = 1;
                     break;
                  case 'l':
                     objectType = 5;
                     break;
                  case 'p':
                     objectType = 7;
                     break;
                  case 's':
                     objectType = 6;
                  }

                  if(objectType == 5) {
                     String[] entry = name.split("\\s->\\s");
                     if(entry.length == 2) {
                        name = entry[0];
                        info = entry[1];
                        String[] pathSegments = info.split("/");
                        if(pathSegments.length == 1 && "..".equals(pathSegments[0])) {
                           objectType = 2;
                        }
                     }

                     info = "-> " + info;
                  }

                  FileListingService.FileEntry var18 = this.getExistingEntry(name);
                  if(var18 == null) {
                     var18 = new FileListingService.FileEntry(this.mParentEntry, name, objectType, false, null);
                  }

                  var18.permissions = permissions;
                  var18.size = size;
                  var18.date = date;
                  var18.time = time;
                  var18.owner = owner;
                  var18.group = group;
                  if(objectType == 5) {
                     var18.info = info;
                  }

                  this.mEntryList.add(var18);
               }
            }
         }

      }

      private FileListingService.FileEntry getExistingEntry(String name) {
         for(int i = 0; i < this.mCurrentChildren.length; ++i) {
            FileListingService.FileEntry e = this.mCurrentChildren[i];
            if(e != null && name.equals(e.name)) {
               this.mCurrentChildren[i] = null;
               return e;
            }
         }

         return null;
      }

      public boolean isCancelled() {
         return false;
      }

      public void finishLinks(IDevice device, ArrayList<FileListingService.FileEntry> entries) throws TimeoutException, AdbCommandRejectedException, ShellCommandUnresponsiveException, IOException {
         final int[] nLines = new int[]{0};
         MultiLineReceiver receiver = new MultiLineReceiver() {
            public void processNewLines(String[] lines) {
               String[] arr$ = lines;
               int len$ = lines.length;

               for(int i$ = 0; i$ < len$; ++i$) {
                  String line = arr$[i$];
                  Matcher m = FileListingService.LS_LD_PATTERN.matcher(line);
                  if(m.matches()) {
                     ++nLines[0];
                  }
               }

            }

            public boolean isCancelled() {
               return false;
            }
         };
         Iterator i$ = entries.iterator();

         while(i$.hasNext()) {
            FileListingService.FileEntry entry = (FileListingService.FileEntry)i$.next();
            if(entry.getType() == 5) {
               nLines[0] = 0;
               String command = String.format("ls -l -d %s%s", new Object[]{entry.getFullEscapedPath(), "/"});
               device.executeShellCommand(command, receiver);
               if(nLines[0] > 0) {
                  entry.setType(2);
               }
            }
         }

      }
   }

   public static final class FileEntry {
      private static final Pattern sEscapePattern = Pattern.compile("([\\\\()*+?\"\'&#/\\s])");
      private static Comparator<FileListingService.FileEntry> sEntryComparator = new Comparator() {
         @Override
         public int compare(Object o1, Object o2) {
            return compare((FileEntry)o1, (FileEntry)o2);
         }

         public int compare(FileListingService.FileEntry o1, FileListingService.FileEntry o2) {
            return o1 instanceof FileListingService.FileEntry && o2 instanceof FileListingService.FileEntry?o1.name.compareTo(o2.name):0;
         }
      };
      FileListingService.FileEntry parent;
      String name;
      String info;
      String permissions;
      String size;
      String date;
      String time;
      String owner;
      String group;
      int type;
      boolean isAppPackage;
      boolean isRoot;
      long fetchTime;
      final ArrayList<FileListingService.FileEntry> mChildren;

      private FileEntry(FileListingService.FileEntry parent, String name, int type, boolean isRoot) {
         this.fetchTime = 0L;
         this.mChildren = new ArrayList();
         this.parent = parent;
         this.name = name;
         this.type = type;
         this.isRoot = isRoot;
         this.checkAppPackageStatus();
      }

      public String getName() {
         return this.name;
      }

      public String getSize() {
         return this.size;
      }

      public int getSizeValue() {
         return Integer.parseInt(this.size);
      }

      public String getDate() {
         return this.date;
      }

      public String getTime() {
         return this.time;
      }

      public String getPermissions() {
         return this.permissions;
      }

      public String getOwner() {
         return this.owner;
      }

      public String getGroup() {
         return this.group;
      }

      public String getInfo() {
         return this.info;
      }

      public String getFullPath() {
         if(this.isRoot) {
            return "/";
         } else {
            StringBuilder pathBuilder = new StringBuilder();
            this.fillPathBuilder(pathBuilder, false);
            return pathBuilder.toString();
         }
      }

      public String getFullEscapedPath() {
         StringBuilder pathBuilder = new StringBuilder();
         this.fillPathBuilder(pathBuilder, true);
         return pathBuilder.toString();
      }

      public String[] getPathSegments() {
         ArrayList list = new ArrayList();
         this.fillPathSegments(list);
         return (String[])list.toArray(new String[list.size()]);
      }

      public int getType() {
         return this.type;
      }

      public void setType(int type) {
         this.type = type;
      }

      public boolean isDirectory() {
         return this.type == 1 || this.type == 2;
      }

      public FileListingService.FileEntry getParent() {
         return this.parent;
      }

      public FileListingService.FileEntry[] getCachedChildren() {
         return (FileListingService.FileEntry[])this.mChildren.toArray(new FileListingService.FileEntry[this.mChildren.size()]);
      }

      public FileListingService.FileEntry findChild(String name) {
         Iterator i$ = this.mChildren.iterator();

         FileListingService.FileEntry entry;
         do {
            if(!i$.hasNext()) {
               return null;
            }

            entry = (FileListingService.FileEntry)i$.next();
         } while(!entry.name.equals(name));

         return entry;
      }

      public boolean isRoot() {
         return this.isRoot;
      }

      void addChild(FileListingService.FileEntry child) {
         this.mChildren.add(child);
      }

      void setChildren(ArrayList<FileListingService.FileEntry> newChildren) {
         this.mChildren.clear();
         this.mChildren.addAll(newChildren);
      }

      boolean needFetch() {
         if(this.fetchTime == 0L) {
            return true;
         } else {
            long current = System.currentTimeMillis();
            return current - this.fetchTime > 4000L;
         }
      }

      public boolean isApplicationPackage() {
         return this.isAppPackage;
      }

      public boolean isAppFileName() {
         Matcher m = FileListingService.sApkPattern.matcher(this.name);
         return m.matches();
      }

      protected void fillPathBuilder(StringBuilder pathBuilder, boolean escapePath) {
         if(!this.isRoot) {
            if(this.parent != null) {
               this.parent.fillPathBuilder(pathBuilder, escapePath);
            }

            pathBuilder.append("/");
            pathBuilder.append(escapePath?escape(this.name):this.name);
         }
      }

      protected void fillPathSegments(ArrayList<String> list) {
         if(!this.isRoot) {
            if(this.parent != null) {
               this.parent.fillPathSegments(list);
            }

            list.add(this.name);
         }
      }

      private void checkAppPackageStatus() {
         this.isAppPackage = false;
         String[] segments = this.getPathSegments();
         if(this.type == 0 && segments.length == 3 && this.isAppFileName()) {
            this.isAppPackage = "app".equals(segments[1]) && ("system".equals(segments[0]) || "data".equals(segments[0]));
         }

      }

      public static String escape(String entryName) {
         return sEscapePattern.matcher(entryName).replaceAll("\\\\$1");
      }

      // $FF: synthetic method
      FileEntry(FileListingService.FileEntry x0, String x1, int x2, boolean x3, Object x4) {
         this(x0, x1, x2, x3);
      }
   }
}
