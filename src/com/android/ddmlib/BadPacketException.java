package com.android.ddmlib;

class BadPacketException extends RuntimeException {
   public BadPacketException() {
   }

   public BadPacketException(String msg) {
      super(msg);
   }
}
