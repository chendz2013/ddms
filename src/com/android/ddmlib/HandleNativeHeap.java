package com.android.ddmlib;

import com.android.ddmlib.ChunkHandler;
import com.android.ddmlib.Client;
import com.android.ddmlib.ClientData;
import com.android.ddmlib.JdwpPacket;
import com.android.ddmlib.Log;
import com.android.ddmlib.MonitorThread;
import com.android.ddmlib.NativeAllocationInfo;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

final class HandleNativeHeap extends ChunkHandler {
   public static final int CHUNK_NHGT = type("NHGT");
   public static final int CHUNK_NHSG = type("NHSG");
   public static final int CHUNK_NHST = type("NHST");
   public static final int CHUNK_NHEN = type("NHEN");
   private static final HandleNativeHeap mInst = new HandleNativeHeap();

   public static void register(MonitorThread mt) {
      mt.registerChunkHandler(CHUNK_NHGT, mInst);
      mt.registerChunkHandler(CHUNK_NHSG, mInst);
      mt.registerChunkHandler(CHUNK_NHST, mInst);
      mt.registerChunkHandler(CHUNK_NHEN, mInst);
   }

   public void clientReady(Client client) throws IOException {
   }

   public void clientDisconnected(Client client) {
   }

   public void handleChunk(Client client, int type, ByteBuffer data, boolean isReply, int msgId) {
      Log.d("ddm-nativeheap", "handling " + ChunkHandler.name(type));
      if(type == CHUNK_NHGT) {
         this.handleNHGT(client, data);
      } else if(type == CHUNK_NHST) {
         client.getClientData().getNativeHeapData().clearHeapData();
      } else if(type == CHUNK_NHEN) {
         client.getClientData().getNativeHeapData().sealHeapData();
      } else if(type == CHUNK_NHSG) {
         this.handleNHSG(client, data);
      } else {
         this.handleUnknownChunk(client, type, data, isReply, msgId);
      }

      client.update(128);
   }

   public static void sendNHGT(Client client) throws IOException {
      ByteBuffer rawBuf = allocBuffer(0);
      JdwpPacket packet = new JdwpPacket(rawBuf);
      ByteBuffer buf = getChunkDataBuf(rawBuf);
      finishChunkPacket(packet, CHUNK_NHGT, buf.position());
      Log.d("ddm-nativeheap", "Sending " + name(CHUNK_NHGT));
      client.sendAndConsume(packet, mInst);
      rawBuf = allocBuffer(2);
      packet = new JdwpPacket(rawBuf);
      buf = getChunkDataBuf(rawBuf);
      buf.put((byte)0);
      buf.put((byte)1);
      finishChunkPacket(packet, CHUNK_NHSG, buf.position());
      Log.d("ddm-nativeheap", "Sending " + name(CHUNK_NHSG));
      client.sendAndConsume(packet, mInst);
   }

   private void handleNHGT(Client client, ByteBuffer data) {
      ClientData clientData = client.getClientData();
      Log.d("ddm-nativeheap", "NHGT: " + data.limit() + " bytes");
      data.order(ByteOrder.LITTLE_ENDIAN);
      int signature = data.getInt(0);
      short pointerSize = 4;
      if(signature == -2128394787) {
         int buffer = data.getInt();
         short mapSize = data.getShort();
         if(mapSize != 2) {
            Log.e("ddms", "Unknown header version: " + mapSize);
            return;
         }

         pointerSize = data.getShort();
      }

      Object var18;
      if(pointerSize == 4) {
         var18 = new HandleNativeHeap.NativeBuffer32(data);
      } else {
         if(pointerSize != 8) {
            Log.e("ddms", "Unknown pointer size: " + pointerSize);
            return;
         }

         var18 = new HandleNativeHeap.NativeBuffer64(data);
      }

      clientData.clearNativeAllocationInfo();
      int var19 = ((HandleNativeHeap.NativeBuffer)var18).getSizeT();
      int allocSize = ((HandleNativeHeap.NativeBuffer)var18).getSizeT();
      int allocInfoSize = ((HandleNativeHeap.NativeBuffer)var18).getSizeT();
      int totalMemory = ((HandleNativeHeap.NativeBuffer)var18).getSizeT();
      int backtraceSize = ((HandleNativeHeap.NativeBuffer)var18).getSizeT();
      Log.d("ddms", "mapSize: " + var19);
      Log.d("ddms", "allocSize: " + allocSize);
      Log.d("ddms", "allocInfoSize: " + allocInfoSize);
      Log.d("ddms", "totalMemory: " + totalMemory);
      clientData.setTotalNativeMemory(totalMemory);
      if(allocInfoSize != 0) {
         if(var19 > 0) {
            byte[] iterations = new byte[var19];
            data.get(iterations, 0, var19);
            this.parseMaps(clientData, iterations);
         }

         int var20 = allocSize / allocInfoSize;

         for(int i = 0; i < var20; ++i) {
            NativeAllocationInfo info = new NativeAllocationInfo(((HandleNativeHeap.NativeBuffer)var18).getSizeT(), ((HandleNativeHeap.NativeBuffer)var18).getSizeT());

            for(int j = 0; j < backtraceSize; ++j) {
               long addr = ((HandleNativeHeap.NativeBuffer)var18).getPtr();
               if(addr != 0L) {
                  info.addStackCallAddress(addr);
               }
            }

            clientData.addNativeAllocation(info);
         }

      }
   }

   private void handleNHSG(Client client, ByteBuffer data) {
      byte[] dataCopy = new byte[data.limit()];
      data.rewind();
      data.get(dataCopy);
      data = ByteBuffer.wrap(dataCopy);
      client.getClientData().getNativeHeapData().addHeapData(data);
   }

   private void parseMaps(ClientData clientData, byte[] maps) {
      InputStreamReader input = new InputStreamReader(new ByteArrayInputStream(maps));
      BufferedReader reader = new BufferedReader(input);

      String line;
      try {
         while((line = reader.readLine()) != null) {
            Log.d("ddms", "line: " + line);
            int e = line.lastIndexOf(32);
            if(e != -1) {
               String library = line.substring(e + 1);
               if(library.startsWith("/")) {
                  int dashIndex = line.indexOf(45);
                  int spaceIndex = line.indexOf(32, dashIndex);
                  if(dashIndex != -1 && spaceIndex != -1) {
                     long startAddr = 0L;
                     long endAddr = 0L;

                     try {
                        startAddr = Long.parseLong(line.substring(0, dashIndex), 16);
                        endAddr = Long.parseLong(line.substring(dashIndex + 1, spaceIndex), 16);
                     } catch (NumberFormatException var15) {
                        var15.printStackTrace();
                        continue;
                     }

                     clientData.addNativeLibraryMapInfo(startAddr, endAddr, library);
                     Log.d("ddms", library + "(" + Long.toHexString(startAddr) + " - " + Long.toHexString(endAddr) + ")");
                  }
               }
            }
         }
      } catch (IOException var16) {
         var16.printStackTrace();
      }

   }

   final class NativeBuffer64 extends HandleNativeHeap.NativeBuffer {
      public NativeBuffer64(ByteBuffer buffer) {
         super(buffer);
      }

      public int getSizeT() {
         return (int)this.mBuffer.getLong();
      }

      public long getPtr() {
         return this.mBuffer.getLong();
      }
   }

   final class NativeBuffer32 extends HandleNativeHeap.NativeBuffer {
      public NativeBuffer32(ByteBuffer buffer) {
         super(buffer);
      }

      public int getSizeT() {
         return this.mBuffer.getInt();
      }

      public long getPtr() {
         return (long)this.mBuffer.getInt() & 4294967295L;
      }
   }

   abstract class NativeBuffer {
      protected ByteBuffer mBuffer;

      public NativeBuffer(ByteBuffer buffer) {
         this.mBuffer = buffer;
      }

      public abstract int getSizeT();

      public abstract long getPtr();
   }
}
