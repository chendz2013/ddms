package com.android.ddmlib;

import com.android.ddmlib.BadPacketException;
import com.android.ddmlib.ChunkHandler;
import com.android.ddmlib.Log;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.SocketChannel;

final class JdwpPacket {
   public static final int JDWP_HEADER_LEN = 11;
   public static final int HANDSHAKE_GOOD = 1;
   public static final int HANDSHAKE_NOTYET = 2;
   public static final int HANDSHAKE_BAD = 3;
   private static final int DDMS_CMD_SET = 199;
   private static final int DDMS_CMD = 1;
   private static final int REPLY_PACKET = 128;
   private static final byte[] mHandshake = new byte[]{(byte)74, (byte)68, (byte)87, (byte)80, (byte)45, (byte)72, (byte)97, (byte)110, (byte)100, (byte)115, (byte)104, (byte)97, (byte)107, (byte)101};
   public static final int HANDSHAKE_LEN;
   private ByteBuffer mBuffer;
   private int mLength;
   private int mId;
   private int mFlags;
   private int mCmdSet;
   private int mCmd;
   private int mErrCode;
   private boolean mIsNew;
   private static int sSerialId;

   JdwpPacket(ByteBuffer buf) {
      this.mBuffer = buf;
      this.mIsNew = true;
   }

   void finishPacket(int payloadLength) {
      assert this.mIsNew;

      ByteOrder oldOrder = this.mBuffer.order();
      this.mBuffer.order(ChunkHandler.CHUNK_ORDER);
      this.mLength = 11 + payloadLength;
      this.mId = getNextSerial();
      this.mFlags = 0;
      this.mCmdSet = 199;
      this.mCmd = 1;
      this.mBuffer.putInt(0, this.mLength);
      this.mBuffer.putInt(4, this.mId);
      this.mBuffer.put(8, (byte)this.mFlags);
      this.mBuffer.put(9, (byte)this.mCmdSet);
      this.mBuffer.put(10, (byte)this.mCmd);
      this.mBuffer.order(oldOrder);
      this.mBuffer.position(this.mLength);
   }

   private static synchronized int getNextSerial() {
      return sSerialId++;
   }

   ByteBuffer getPayload() {
      int oldPosn = this.mBuffer.position();
      this.mBuffer.position(11);
      ByteBuffer buf = this.mBuffer.slice();
      this.mBuffer.position(oldPosn);
      if(this.mLength > 0) {
         buf.limit(this.mLength - 11);
      } else {
         assert this.mIsNew;
      }

      buf.order(ChunkHandler.CHUNK_ORDER);
      return buf;
   }

   boolean isDdmPacket() {
      return (this.mFlags & 128) == 0 && this.mCmdSet == 199 && this.mCmd == 1;
   }

   boolean isReply() {
      return (this.mFlags & 128) != 0;
   }

   boolean isError() {
      return this.isReply() && this.mErrCode != 0;
   }

   boolean isEmpty() {
      return this.mLength == 11;
   }

   int getId() {
      return this.mId;
   }

   int getLength() {
      return this.mLength;
   }

   void writeAndConsume(SocketChannel chan) throws IOException {
      assert this.mLength > 0;

      this.mBuffer.flip();
      int oldLimit = this.mBuffer.limit();
      this.mBuffer.limit(this.mLength);

      while(this.mBuffer.position() != this.mBuffer.limit()) {
         chan.write(this.mBuffer);
      }

      assert this.mBuffer.position() == this.mLength;

      this.mBuffer.limit(oldLimit);
      this.mBuffer.compact();
   }

   void movePacket(ByteBuffer buf) {
      Log.v("ddms", "moving " + this.mLength + " bytes");
      int oldPosn = this.mBuffer.position();
      this.mBuffer.position(0);
      this.mBuffer.limit(this.mLength);
      buf.put(this.mBuffer);
      this.mBuffer.position(this.mLength);
      this.mBuffer.limit(oldPosn);
      this.mBuffer.compact();
   }

   void consume() {
      this.mBuffer.flip();
      this.mBuffer.position(this.mLength);
      this.mBuffer.compact();
      this.mLength = 0;
   }

   static JdwpPacket findPacket(ByteBuffer buf) {
      int count = buf.position();
      if(count < 11) {
         return null;
      } else {
         ByteOrder oldOrder = buf.order();
         buf.order(ChunkHandler.CHUNK_ORDER);
         int length = buf.getInt(0);
         int id = buf.getInt(4);
         int flags = buf.get(8) & 255;
         int cmdSet = buf.get(9) & 255;
         int cmd = buf.get(10) & 255;
         buf.order(oldOrder);
         if(length < 11) {
            throw new BadPacketException();
         } else if(count < length) {
            return null;
         } else {
            JdwpPacket pkt = new JdwpPacket(buf);
            pkt.mLength = length;
            pkt.mId = id;
            pkt.mFlags = flags;
            if((flags & 128) == 0) {
               pkt.mCmdSet = cmdSet;
               pkt.mCmd = cmd;
               pkt.mErrCode = -1;
            } else {
               pkt.mCmdSet = -1;
               pkt.mCmd = -1;
               pkt.mErrCode = cmdSet | cmd << 8;
            }

            return pkt;
         }
      }
   }

   static int findHandshake(ByteBuffer buf) {
      int count = buf.position();
      if(count < mHandshake.length) {
         return 2;
      } else {
         for(int i = mHandshake.length - 1; i >= 0; --i) {
            if(buf.get(i) != mHandshake[i]) {
               return 3;
            }
         }

         return 1;
      }
   }

   static void consumeHandshake(ByteBuffer buf) {
      buf.flip();
      buf.position(mHandshake.length);
      buf.compact();
   }

   static void putHandshake(ByteBuffer buf) {
      buf.put(mHandshake);
   }

   static {
      HANDSHAKE_LEN = mHandshake.length;
      sSerialId = 1073741824;
   }
}
