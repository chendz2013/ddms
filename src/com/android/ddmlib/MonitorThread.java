package com.android.ddmlib;

import com.android.ddmlib.AndroidDebugBridge;
import com.android.ddmlib.ChunkHandler;
import com.android.ddmlib.Client;
import com.android.ddmlib.DdmPreferences;
import com.android.ddmlib.Debugger;
import com.android.ddmlib.JdwpPacket;
import com.android.ddmlib.Log;
import java.io.IOException;
import java.net.BindException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.nio.channels.CancelledKeyException;
import java.nio.channels.NotYetBoundException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

final class MonitorThread extends Thread {
   private static final int CLIENT_READY = 2;
   private static final int CLIENT_DISCONNECTED = 3;
   private volatile boolean mQuit = false;
   private ArrayList<Client> mClientList = new ArrayList();
   private Selector mSelector;
   private HashMap<Integer, ChunkHandler> mHandlerMap = new HashMap();
   private ServerSocketChannel mDebugSelectedChan;
   private int mNewDebugSelectedPort = DdmPreferences.getSelectedDebugPort();
   private int mDebugSelectedPort = -1;
   private Client mSelectedClient = null;
   private static MonitorThread sInstance;

   private MonitorThread() {
      super("Monitor");
   }

   static MonitorThread createInstance() {
      return sInstance = new MonitorThread();
   }

   static MonitorThread getInstance() {
      return sInstance;
   }

   synchronized void setDebugSelectedPort(int port) throws IllegalStateException {
      if(sInstance != null) {
         if(AndroidDebugBridge.getClientSupport()) {
            if(this.mDebugSelectedChan != null) {
               Log.d("ddms", "Changing debug-selected port to " + port);
               this.mNewDebugSelectedPort = port;
               this.wakeup();
            } else {
               this.mNewDebugSelectedPort = port;
            }

         }
      }
   }

   synchronized void setSelectedClient(Client selectedClient) {
      if(sInstance != null) {
         if(this.mSelectedClient != selectedClient) {
            Client oldClient = this.mSelectedClient;
            this.mSelectedClient = selectedClient;
            if(oldClient != null) {
               oldClient.update(4);
            }

            if(this.mSelectedClient != null) {
               this.mSelectedClient.update(4);
            }
         }

      }
   }

   Client getSelectedClient() {
      return this.mSelectedClient;
   }

   boolean getRetryOnBadHandshake() {
      return true;
   }

   Client[] getClients() {
      ArrayList var1 = this.mClientList;
      synchronized(this.mClientList) {
         return (Client[])this.mClientList.toArray(new Client[this.mClientList.size()]);
      }
   }

   synchronized void registerChunkHandler(int type, ChunkHandler handler) {
      if(sInstance != null) {
         HashMap var3 = this.mHandlerMap;
         synchronized(this.mHandlerMap) {
            if(this.mHandlerMap.get(Integer.valueOf(type)) == null) {
               this.mHandlerMap.put(Integer.valueOf(type), handler);
            }

         }
      }
   }

   public void run() {
      Log.d("ddms", "Monitor is up");

      try {
         this.mSelector = Selector.open();
      } catch (IOException var8) {
         Log.logAndDisplay(Log.LogLevel.ERROR, "ddms", "Failed to initialize Monitor Thread: " + var8.getMessage());
         return;
      }

      while(!this.mQuit) {
         try {
            ArrayList e = this.mClientList;
            synchronized(this.mClientList) {
               ;
            }

            try {
               if(AndroidDebugBridge.getClientSupport() && (this.mDebugSelectedChan == null || this.mNewDebugSelectedPort != this.mDebugSelectedPort) && this.mNewDebugSelectedPort != -1 && this.reopenDebugSelectedPort()) {
                  this.mDebugSelectedPort = this.mNewDebugSelectedPort;
               }
            } catch (IOException var11) {
               Log.e("ddms", "Failed to reopen debug port for Selected Client to: " + this.mNewDebugSelectedPort);
               Log.e("ddms", (Throwable)var11);
               this.mNewDebugSelectedPort = this.mDebugSelectedPort;
            }

            int e1;
            try {
               e1 = this.mSelector.select();
            } catch (IOException var9) {
               var9.printStackTrace();
               continue;
            } catch (CancelledKeyException var10) {
               continue;
            }

            if(e1 != 0) {
               Set keys = this.mSelector.selectedKeys();
               Iterator iter = keys.iterator();

               while(iter.hasNext()) {
                  SelectionKey key = (SelectionKey)iter.next();
                  iter.remove();

                  try {
                     if(key.attachment() instanceof Client) {
                        this.processClientActivity(key);
                     } else if(key.attachment() instanceof Debugger) {
                        this.processDebuggerActivity(key);
                     } else if(key.attachment() instanceof MonitorThread) {
                        this.processDebugSelectedActivity(key);
                     } else {
                        Log.e("ddms", "unknown activity key");
                     }
                  } catch (Exception var6) {
                     Log.e("ddms", "Exception during activity from Selector.");
                     Log.e("ddms", (Throwable)var6);
                  }
               }
            }
         } catch (Exception var12) {
            Log.e("ddms", "Exception MonitorThread.run()");
            Log.e("ddms", (Throwable)var12);
         }
      }

   }

   int getDebugSelectedPort() {
      return this.mDebugSelectedPort;
   }

   private void processClientActivity(SelectionKey key) {
      Client client = (Client)key.attachment();

      try {
         if(!key.isReadable() || !key.isValid()) {
            Log.d("ddms", "Invalid key from " + client + ". Dropping client.");
            this.dropClient(client, true);
            return;
         }

         client.read();

         for(JdwpPacket ex = client.getJdwpPacket(); ex != null; ex = client.getJdwpPacket()) {
            if(ex.isDdmPacket()) {
               assert !ex.isReply();

               this.callHandler(client, ex, (ChunkHandler)null);
               ex.consume();
            } else if(ex.isReply() && client.isResponseToUs(ex.getId()) != null) {
               ChunkHandler handler = client.isResponseToUs(ex.getId());
               if(ex.isError()) {
                  client.packetFailed(ex);
               } else if(ex.isEmpty()) {
                  Log.d("ddms", "Got empty reply for 0x" + Integer.toHexString(ex.getId()) + " from " + client);
               } else {
                  this.callHandler(client, ex, handler);
               }

               ex.consume();
               client.removeRequestId(ex.getId());
            } else {
               Log.v("ddms", "Forwarding client " + (ex.isReply()?"reply":"event") + " 0x" + Integer.toHexString(ex.getId()) + " to " + client.getDebugger());
               client.forwardPacketToDebugger(ex);
            }
         }
      } catch (CancelledKeyException var5) {
         this.dropClient(client, true);
      } catch (IOException var6) {
         this.dropClient(client, true);
      } catch (Exception var7) {
         Log.e("ddms", (Throwable)var7);
         this.dropClient(client, true);
         if(var7 instanceof BufferOverflowException) {
            Log.w("ddms", "Client data packet exceeded maximum buffer size " + client);
         } else {
            Log.e("ddms", (Throwable)var7);
         }
      }

   }

   private void callHandler(Client client, JdwpPacket packet, ChunkHandler handler) {
      if(!client.ddmSeen()) {
         this.broadcast(2, client);
      }

      ByteBuffer buf = packet.getPayload();
      boolean reply = true;
      int type = buf.getInt();
      int length = buf.getInt();
      if(handler == null) {
         HashMap ibuf = this.mHandlerMap;
         synchronized(this.mHandlerMap) {
            handler = (ChunkHandler)this.mHandlerMap.get(Integer.valueOf(type));
            reply = false;
         }
      }

      if(handler == null) {
         Log.w("ddms", "Received unsupported chunk type " + ChunkHandler.name(type) + " (len=" + length + ")");
      } else {
         Log.d("ddms", "Calling handler for " + ChunkHandler.name(type) + " [" + handler + "] (len=" + length + ")");
         ByteBuffer ibuf1 = buf.slice();
         ByteBuffer roBuf = ibuf1.asReadOnlyBuffer();
         roBuf.order(ChunkHandler.CHUNK_ORDER);
         ArrayList var10 = this.mClientList;
         synchronized(this.mClientList) {
            handler.handleChunk(client, type, roBuf, reply, packet.getId());
         }
      }

   }

   synchronized void dropClient(Client client, boolean notify) {
      if(sInstance != null) {
         ArrayList var3 = this.mClientList;
         synchronized(this.mClientList) {
            if(!this.mClientList.remove(client)) {
               return;
            }
         }

         client.close(notify);
         this.broadcast(3, client);
         this.wakeup();
      }
   }

   synchronized void dropClients(Collection<? extends Client> clients, boolean notify) {
      Iterator i$ = clients.iterator();

      while(i$.hasNext()) {
         Client c = (Client)i$.next();
         this.dropClient(c, notify);
      }

   }

   private void processDebuggerActivity(SelectionKey key) {
      Debugger dbg = (Debugger)key.attachment();

      try {
         if(key.isAcceptable()) {
            try {
               this.acceptNewDebugger(dbg, (ServerSocketChannel)null);
            } catch (IOException var4) {
               Log.w("ddms", "debugger accept() failed");
               var4.printStackTrace();
            }
         } else if(key.isReadable()) {
            this.processDebuggerData(key);
         } else {
            Log.d("ddm-debugger", "key in unknown state");
         }
      } catch (CancelledKeyException var5) {
         ;
      }

   }

   private void acceptNewDebugger(Debugger dbg, ServerSocketChannel acceptChan) throws IOException {
      ArrayList var3 = this.mClientList;
      synchronized(this.mClientList) {
         SocketChannel chan;
         if(acceptChan == null) {
            chan = dbg.accept();
         } else {
            chan = dbg.accept(acceptChan);
         }

         if(chan != null) {
            chan.socket().setTcpNoDelay(true);
            this.wakeup();

            try {
               chan.register(this.mSelector, 1, dbg);
            } catch (IOException var7) {
               dbg.closeData();
               throw var7;
            } catch (RuntimeException var8) {
               dbg.closeData();
               throw var8;
            }
         } else {
            Log.w("ddms", "ignoring duplicate debugger");
         }

      }
   }

   private void processDebuggerData(SelectionKey key) {
      Debugger dbg = (Debugger)key.attachment();

      try {
         dbg.read();

         for(JdwpPacket ioe = dbg.getJdwpPacket(); ioe != null; ioe = dbg.getJdwpPacket()) {
            Log.v("ddms", "Forwarding dbg req 0x" + Integer.toHexString(ioe.getId()) + " to " + dbg.getClient());
            dbg.forwardPacketToClient(ioe);
         }
      } catch (IOException var5) {
         Log.d("ddms", "Closing connection to debugger " + dbg);
         dbg.closeData();
         Client client = dbg.getClient();
         if(client.isDdmAware()) {
            Log.d("ddms", " (recycling client connection as well)");
            client.getDeviceImpl().getMonitor().addClientToDropAndReopen(client, -1);
         } else {
            Log.d("ddms", " (recycling client connection as well)");
            client.getDeviceImpl().getMonitor().addClientToDropAndReopen(client, -1);
         }
      }

   }

   private void wakeup() {
      this.mSelector.wakeup();
   }

   synchronized void quit() {
      this.mQuit = true;
      this.wakeup();
      Log.d("ddms", "Waiting for Monitor thread");

      try {
         this.join();
         ArrayList e = this.mClientList;
         synchronized(this.mClientList) {
            Iterator i$ = this.mClientList.iterator();

            while(true) {
               if(!i$.hasNext()) {
                  this.mClientList.clear();
                  break;
               }

               Client c = (Client)i$.next();
               c.close(false);
               this.broadcast(3, c);
            }
         }

         if(this.mDebugSelectedChan != null) {
            this.mDebugSelectedChan.close();
            this.mDebugSelectedChan.socket().close();
            this.mDebugSelectedChan = null;
         }

         this.mSelector.close();
      } catch (InterruptedException var6) {
         var6.printStackTrace();
      } catch (IOException var7) {
         var7.printStackTrace();
      }

      sInstance = null;
   }

   synchronized void addClient(Client client) {
      if(sInstance != null) {
         Log.d("ddms", "Adding new client " + client);
         ArrayList var2 = this.mClientList;
         synchronized(this.mClientList) {
            this.mClientList.add(client);

            try {
               this.wakeup();
               client.register(this.mSelector);
               Debugger ioe = client.getDebugger();
               if(ioe != null) {
                  ioe.registerListener(this.mSelector);
               }
            } catch (IOException var5) {
               var5.printStackTrace();
            }

         }
      }
   }

   private void broadcast(int event, Client client) {
      Log.d("ddms", "broadcast " + event + ": " + client);
      HashMap iter = this.mHandlerMap;
      HashSet set;
      synchronized(this.mHandlerMap) {
         Collection handler = this.mHandlerMap.values();
         set = new HashSet(handler);
      }

      Iterator iter1 = set.iterator();

      while(iter1.hasNext()) {
         ChunkHandler handler1 = (ChunkHandler)iter1.next();
         switch(event) {
         case 2:
            try {
               handler1.clientReady(client);
               break;
            } catch (IOException var7) {
               Log.w("ddms", "Got exception while broadcasting \'ready\'");
               return;
            }
         case 3:
            handler1.clientDisconnected(client);
            break;
         default:
            throw new UnsupportedOperationException();
         }
      }

   }

   private boolean reopenDebugSelectedPort() throws IOException {
      Log.d("ddms", "reopen debug-selected port: " + this.mNewDebugSelectedPort);
      if(this.mDebugSelectedChan != null) {
         this.mDebugSelectedChan.close();
      }

      this.mDebugSelectedChan = ServerSocketChannel.open();
      this.mDebugSelectedChan.configureBlocking(false);
      InetSocketAddress addr = new InetSocketAddress(InetAddress.getByName("localhost"), this.mNewDebugSelectedPort);
      this.mDebugSelectedChan.socket().setReuseAddress(true);

      try {
         this.mDebugSelectedChan.socket().bind(addr);
         if(this.mSelectedClient != null) {
            this.mSelectedClient.update(4);
         }

         this.mDebugSelectedChan.register(this.mSelector, 16, this);
         return true;
      } catch (BindException var3) {
         this.displayDebugSelectedBindError(this.mNewDebugSelectedPort);
         this.mDebugSelectedChan = null;
         this.mNewDebugSelectedPort = -1;
         return false;
      }
   }

   private void processDebugSelectedActivity(SelectionKey key) {
      assert key.isAcceptable();

      ServerSocketChannel acceptChan = (ServerSocketChannel)key.channel();
      if(this.mSelectedClient != null) {
         Debugger e = this.mSelectedClient.getDebugger();
         if(e != null) {
            Log.d("ddms", "Accepting connection on \'debug selected\' port");

            try {
               this.acceptNewDebugger(e, acceptChan);
            } catch (IOException var5) {
               ;
            }

            return;
         }
      }

      Log.w("ddms", "Connection on \'debug selected\' port, but none selected");

      try {
         SocketChannel e1 = acceptChan.accept();
         e1.close();
      } catch (IOException var6) {
         ;
      } catch (NotYetBoundException var7) {
         this.displayDebugSelectedBindError(this.mDebugSelectedPort);
      }

   }

   private void displayDebugSelectedBindError(int port) {
      String message = String.format("Could not open Selected VM debug port (%1$d). Make sure you do not have another instance of DDMS or of the eclipse plugin running. If it\'s being used by something else, choose a new port number in the preferences.", new Object[]{Integer.valueOf(port)});
      Log.logAndDisplay(Log.LogLevel.ERROR, "ddms", message);
   }
}
