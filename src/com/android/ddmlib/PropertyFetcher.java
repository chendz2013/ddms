package com.android.ddmlib;

import com.android.ddmlib.IDevice;
import com.android.ddmlib.Log;
import com.android.ddmlib.MultiLineReceiver;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Maps;
import com.google.common.util.concurrent.SettableFuture;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class PropertyFetcher {
   private static final String GETPROP_COMMAND = "getprop";
   private static final Pattern GETPROP_PATTERN = Pattern.compile("^\\[([^]]+)\\]\\:\\s*\\[(.*)\\]$");
   private static final int GETPROP_TIMEOUT_SEC = 2;
   private static final int EXPECTED_PROP_COUNT = 150;
   private final Map<String, String> mProperties = Maps.newHashMapWithExpectedSize(150);
   private final IDevice mDevice;
   private PropertyFetcher.CacheState mCacheState;
   private final Map<String, SettableFuture<String>> mPendingRequests;

   public PropertyFetcher(IDevice device) {
      this.mCacheState = PropertyFetcher.CacheState.UNPOPULATED;
      this.mPendingRequests = Maps.newHashMapWithExpectedSize(4);
      this.mDevice = device;
   }

   public synchronized Map<String, String> getProperties() {
      return this.mProperties;
   }

   public synchronized Future<String> getProperty(String name) {
      SettableFuture result;
      if(this.mCacheState.equals(PropertyFetcher.CacheState.FETCHING)) {
         result = this.addPendingRequest(name);
      } else if((!this.mDevice.isOnline() || !this.mCacheState.equals(PropertyFetcher.CacheState.UNPOPULATED)) && isRoProp(name)) {
         result = SettableFuture.create();
         result.set(this.mProperties.get(name));
      } else {
         result = this.addPendingRequest(name);
         this.mCacheState = PropertyFetcher.CacheState.FETCHING;
         this.initiatePropertiesQuery();
      }

      return result;
   }

   private SettableFuture<String> addPendingRequest(String name) {
      SettableFuture future = (SettableFuture)this.mPendingRequests.get(name);
      if(future == null) {
         future = SettableFuture.create();
         this.mPendingRequests.put(name, future);
      }

      return future;
   }

   private void initiatePropertiesQuery() {
      final String threadName = String.format("query-prop-%s", new Object[]{this.mDevice.getSerialNumber()});
      Thread propThread = new Thread(threadName) {
         public void run() {
            try {
               PropertyFetcher.GetPropReceiver e = new PropertyFetcher.GetPropReceiver();
               PropertyFetcher.this.mDevice.executeShellCommand("getprop", e, 2L, TimeUnit.SECONDS);
               PropertyFetcher.this.populateCache(e.getCollectedProperties());
            } catch (Exception var2) {
               PropertyFetcher.this.handleException(var2);
            }

         }
      };
      propThread.setDaemon(true);
      propThread.start();
   }

   private synchronized void populateCache(Map<String, String> props) {
      this.mCacheState = props.isEmpty()?PropertyFetcher.CacheState.UNPOPULATED:PropertyFetcher.CacheState.POPULATED;
      if(!props.isEmpty()) {
         this.mProperties.putAll(props);
      }

      Iterator i$ = this.mPendingRequests.entrySet().iterator();

      while(i$.hasNext()) {
         Entry entry = (Entry)i$.next();
         ((SettableFuture)entry.getValue()).set(this.mProperties.get(entry.getKey()));
      }

      this.mPendingRequests.clear();
   }

   private synchronized void handleException(Exception e) {
      this.mCacheState = PropertyFetcher.CacheState.UNPOPULATED;
      Log.w("PropertyFetcher", String.format("%s getting properties for device %s: %s", new Object[]{e.getClass().getSimpleName(), this.mDevice.getSerialNumber(), e.getMessage()}));
      Iterator i$ = this.mPendingRequests.entrySet().iterator();

      while(i$.hasNext()) {
         Entry entry = (Entry)i$.next();
         ((SettableFuture)entry.getValue()).setException(e);
      }

      this.mPendingRequests.clear();
   }

   /** @deprecated */
   @Deprecated
   public synchronized boolean arePropertiesSet() {
      return PropertyFetcher.CacheState.POPULATED.equals(this.mCacheState);
   }

   private static boolean isRoProp(String propName) {
      return propName.startsWith("ro.");
   }

   @VisibleForTesting
   static class GetPropReceiver extends MultiLineReceiver {
      private final Map<String, String> mCollectedProperties = Maps.newHashMapWithExpectedSize(150);

      public void processNewLines(String[] lines) {
         String[] arr$ = lines;
         int len$ = lines.length;

         for(int i$ = 0; i$ < len$; ++i$) {
            String line = arr$[i$];
            if(!line.isEmpty() && !line.startsWith("#")) {
               Matcher m = PropertyFetcher.GETPROP_PATTERN.matcher(line);
               if(m.matches()) {
                  String label = m.group(1);
                  String value = m.group(2);
                  if(!label.isEmpty()) {
                     this.mCollectedProperties.put(label, value);
                  }
               }
            }
         }

      }

      public boolean isCancelled() {
         return false;
      }

      Map<String, String> getCollectedProperties() {
         return this.mCollectedProperties;
      }
   }

   private static enum CacheState {
      UNPOPULATED,
      FETCHING,
      POPULATED;
   }
}
