package com.android.ddmlib.utils;

public final class ArrayHelper {
   public static void swap32bitsToArray(int value, byte[] dest, int offset) {
      dest[offset] = (byte)(value & 255);
      dest[offset + 1] = (byte)((value & '\uff00') >> 8);
      dest[offset + 2] = (byte)((value & 16711680) >> 16);
      dest[offset + 3] = (byte)((value & -16777216) >> 24);
   }

   public static int swap32bitFromArray(byte[] value, int offset) {
      byte v = 0;
      int v1 = v | value[offset] & 255;
      v1 |= (value[offset + 1] & 255) << 8;
      v1 |= (value[offset + 2] & 255) << 16;
      v1 |= (value[offset + 3] & 255) << 24;
      return v1;
   }

   public static int swapU16bitFromArray(byte[] value, int offset) {
      byte v = 0;
      int v1 = v | value[offset] & 255;
      v1 |= (value[offset + 1] & 255) << 8;
      return v1;
   }

   public static long swap64bitFromArray(byte[] value, int offset) {
      long v = 0L;
      v |= (long)value[offset] & 255L;
      v |= ((long)value[offset + 1] & 255L) << 8;
      v |= ((long)value[offset + 2] & 255L) << 16;
      v |= ((long)value[offset + 3] & 255L) << 24;
      v |= ((long)value[offset + 4] & 255L) << 32;
      v |= ((long)value[offset + 5] & 255L) << 40;
      v |= ((long)value[offset + 6] & 255L) << 48;
      v |= ((long)value[offset + 7] & 255L) << 56;
      return v;
   }
}
