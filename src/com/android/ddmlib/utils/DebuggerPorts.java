package com.android.ddmlib.utils;

import java.util.ArrayList;
import java.util.List;

public class DebuggerPorts {
   private final List<Integer> mDebuggerPorts = new ArrayList();

   public DebuggerPorts(int basePort) {
      this.mDebuggerPorts.add(Integer.valueOf(basePort));
   }

   public int next() {
      List var1 = this.mDebuggerPorts;
      synchronized(this.mDebuggerPorts) {
         if(!this.mDebuggerPorts.isEmpty()) {
            int port = ((Integer)this.mDebuggerPorts.get(0)).intValue();
            this.mDebuggerPorts.remove(0);
            if(this.mDebuggerPorts.isEmpty()) {
               this.mDebuggerPorts.add(Integer.valueOf(port + 1));
            }

            return port;
         } else {
            return -1;
         }
      }
   }

   public void free(int port) {
      if(port > 0) {
         List var2 = this.mDebuggerPorts;
         synchronized(this.mDebuggerPorts) {
            if(this.mDebuggerPorts.indexOf(Integer.valueOf(port)) == -1) {
               int count = this.mDebuggerPorts.size();

               for(int i = 0; i < count; ++i) {
                  if(port < ((Integer)this.mDebuggerPorts.get(i)).intValue()) {
                     this.mDebuggerPorts.add(i, Integer.valueOf(port));
                     break;
                  }
               }
            }

         }
      }
   }
}
