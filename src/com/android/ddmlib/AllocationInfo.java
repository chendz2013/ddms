package com.android.ddmlib;

import com.android.ddmlib.IStackTraceInfo;
import com.google.common.collect.Lists;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

/**
 * Allocation分配信息
 */
public class AllocationInfo implements IStackTraceInfo {
   private final String mAllocatedClass;
   private final int mAllocNumber;
   private final int mAllocationSize;
   private final short mThreadId;
   private final StackTraceElement[] mStackTrace;

   AllocationInfo(int allocNumber, String allocatedClass, int allocationSize, short threadId, StackTraceElement[] stackTrace) {
      this.mAllocNumber = allocNumber;
      this.mAllocatedClass = allocatedClass;
      this.mAllocationSize = allocationSize;
      this.mThreadId = threadId;
      this.mStackTrace = stackTrace;
   }

   public int getAllocNumber() {
      return this.mAllocNumber;
   }

   public String getAllocatedClass() {
      return this.mAllocatedClass;
   }

   public int getSize() {
      return this.mAllocationSize;
   }

   public short getThreadId() {
      return this.mThreadId;
   }

   public StackTraceElement[] getStackTrace() {
      return this.mStackTrace;
   }

   public int compareTo(AllocationInfo otherAlloc) {
      return otherAlloc.mAllocationSize - this.mAllocationSize;
   }

   public String getAllocationSite() {
      return this.mStackTrace.length > 0?this.mStackTrace[0].toString():null;
   }

   public String getFirstTraceClassName() {
      return this.mStackTrace.length > 0?this.mStackTrace[0].getClassName():null;
   }

   public String getFirstTraceMethodName() {
      return this.mStackTrace.length > 0?this.mStackTrace[0].getMethodName():null;
   }

   public boolean filter(String filter, boolean fullTrace, Locale locale) {
      return this.allocatedClassMatches(filter, locale) || !this.getMatchingStackFrames(filter, fullTrace, locale).isEmpty();
   }

   public boolean allocatedClassMatches(String pattern, Locale locale) {
      return this.mAllocatedClass.toLowerCase(locale).contains(pattern.toLowerCase(locale));
   }

   public List<String> getMatchingStackFrames(String filter, boolean fullTrace, Locale locale) {
      filter = filter.toLowerCase(locale);
      if(this.mStackTrace.length > 0) {
         int length = fullTrace?this.mStackTrace.length:1;
         ArrayList matchingFrames = Lists.newArrayListWithExpectedSize(length);

         for(int i = 0; i < length; ++i) {
            String frameString = this.mStackTrace[i].toString();
            if(frameString.toLowerCase(locale).contains(filter)) {
               matchingFrames.add(frameString);
            }
         }

         return matchingFrames;
      } else {
         return Collections.emptyList();
      }
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] $SwitchMap$com$android$ddmlib$AllocationInfo$SortMode = new int[AllocationInfo.SortMode.values().length];

      static {
         try {
            $SwitchMap$com$android$ddmlib$AllocationInfo$SortMode[AllocationInfo.SortMode.NUMBER.ordinal()] = 1;
         } catch (NoSuchFieldError var7) {
            ;
         }

         try {
            $SwitchMap$com$android$ddmlib$AllocationInfo$SortMode[AllocationInfo.SortMode.SIZE.ordinal()] = 2;
         } catch (NoSuchFieldError var6) {
            ;
         }

         try {
            $SwitchMap$com$android$ddmlib$AllocationInfo$SortMode[AllocationInfo.SortMode.CLASS.ordinal()] = 3;
         } catch (NoSuchFieldError var5) {
            ;
         }

         try {
            $SwitchMap$com$android$ddmlib$AllocationInfo$SortMode[AllocationInfo.SortMode.THREAD.ordinal()] = 4;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            $SwitchMap$com$android$ddmlib$AllocationInfo$SortMode[AllocationInfo.SortMode.IN_CLASS.ordinal()] = 5;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            $SwitchMap$com$android$ddmlib$AllocationInfo$SortMode[AllocationInfo.SortMode.IN_METHOD.ordinal()] = 6;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            $SwitchMap$com$android$ddmlib$AllocationInfo$SortMode[AllocationInfo.SortMode.ALLOCATION_SITE.ordinal()] = 7;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }

   public static final class AllocationSorter implements Comparator<AllocationInfo> {
      private AllocationInfo.SortMode mSortMode;
      private boolean mDescending;

      public AllocationSorter() {
         this.mSortMode = AllocationInfo.SortMode.SIZE;
         this.mDescending = true;
      }

      public void setSortMode(AllocationInfo.SortMode mode) {
         if(this.mSortMode == mode) {
            this.mDescending = !this.mDescending;
         } else {
            this.mSortMode = mode;
         }

      }

      public void setSortMode(AllocationInfo.SortMode mode, boolean descending) {
         this.mSortMode = mode;
         this.mDescending = descending;
      }

      public AllocationInfo.SortMode getSortMode() {
         return this.mSortMode;
      }

      public boolean isDescending() {
         return this.mDescending;
      }

      public int compare(AllocationInfo o1, AllocationInfo o2) {
         int diff = 0;
         switch(AllocationInfo.SyntheticClass_1.$SwitchMap$com$android$ddmlib$AllocationInfo$SortMode[this.mSortMode.ordinal()]) {
         case 1:
            diff = o1.mAllocNumber - o2.mAllocNumber;
         case 2:
         default:
            break;
         case 3:
            diff = o1.mAllocatedClass.compareTo(o2.mAllocatedClass);
            break;
         case 4:
            diff = o1.mThreadId - o2.mThreadId;
            break;
         case 5:
            String class1 = o1.getFirstTraceClassName();
            String class2 = o2.getFirstTraceClassName();
            diff = compareOptionalString(class1, class2);
            break;
         case 6:
            String method1 = o1.getFirstTraceMethodName();
            String method2 = o2.getFirstTraceMethodName();
            diff = compareOptionalString(method1, method2);
            break;
         case 7:
            String desc1 = o1.getAllocationSite();
            String desc2 = o2.getAllocationSite();
            diff = compareOptionalString(desc1, desc2);
         }

         if(diff == 0) {
            diff = o1.mAllocationSize - o2.mAllocationSize;
         }

         if(this.mDescending) {
            diff = -diff;
         }

         return diff;
      }

      private static int compareOptionalString(String str1, String str2) {
         return str1 != null?(str2 == null?-1:str1.compareTo(str2)):(str2 == null?0:1);
      }
   }

   public static enum SortMode {
      NUMBER,
      SIZE,
      CLASS,
      THREAD,
      ALLOCATION_SITE,
      IN_CLASS,
      IN_METHOD;
   }
}
