package com.android.ddmlib;

import com.android.ddmlib.ByteBufferUtil;
import com.android.ddmlib.ChunkHandler;
import com.android.ddmlib.Client;
import com.android.ddmlib.JdwpPacket;
import com.android.ddmlib.Log;
import com.android.ddmlib.MonitorThread;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public final class HandleViewDebug extends ChunkHandler {
   public static final int CHUNK_VUGL = type("VUGL");
   public static final int CHUNK_VULW = type("VULW");
   public static final int CHUNK_VURT = type("VURT");
   private static final int VURT_DUMP_HIERARCHY = 1;
   private static final int VURT_CAPTURE_LAYERS = 2;
   private static final int VURT_DUMP_THEME = 3;
   public static final int CHUNK_VUOP = type("VUOP");
   private static final int VUOP_CAPTURE_VIEW = 1;
   private static final int VUOP_DUMP_DISPLAYLIST = 2;
   private static final int VUOP_PROFILE_VIEW = 3;
   private static final int VUOP_INVOKE_VIEW_METHOD = 4;
   private static final int VUOP_SET_LAYOUT_PARAMETER = 5;
   private static final String TAG = "ddmlib";
   private static final HandleViewDebug sInstance = new HandleViewDebug();
   private static final HandleViewDebug.ViewDumpHandler sViewOpNullChunkHandler;

   public static void register(MonitorThread mt) {
      mt.registerChunkHandler(CHUNK_VUGL, sInstance);
      mt.registerChunkHandler(CHUNK_VULW, sInstance);
      mt.registerChunkHandler(CHUNK_VUOP, sInstance);
      mt.registerChunkHandler(CHUNK_VURT, sInstance);
   }

   public void clientReady(Client client) throws IOException {
   }

   public void clientDisconnected(Client client) {
   }

   public static void listViewRoots(Client client, HandleViewDebug.ViewDumpHandler replyHandler) throws IOException {
      ByteBuffer buf = allocBuffer(8);
      JdwpPacket packet = new JdwpPacket(buf);
      ByteBuffer chunkBuf = getChunkDataBuf(buf);
      chunkBuf.putInt(1);
      finishChunkPacket(packet, CHUNK_VULW, chunkBuf.position());
      client.sendAndConsume(packet, replyHandler);
   }

   public static void dumpViewHierarchy(Client client, String viewRoot, boolean skipChildren, boolean includeProperties, HandleViewDebug.ViewDumpHandler handler) throws IOException {
      ByteBuffer buf = allocBuffer(8 + viewRoot.length() * 2 + 4 + 4);
      JdwpPacket packet = new JdwpPacket(buf);
      ByteBuffer chunkBuf = getChunkDataBuf(buf);
      chunkBuf.putInt(1);
      chunkBuf.putInt(viewRoot.length());
      ByteBufferUtil.putString(chunkBuf, viewRoot);
      chunkBuf.putInt(skipChildren?1:0);
      chunkBuf.putInt(includeProperties?1:0);
      finishChunkPacket(packet, CHUNK_VURT, chunkBuf.position());
      client.sendAndConsume(packet, handler);
   }

   public static void captureLayers(Client client, String viewRoot, HandleViewDebug.ViewDumpHandler handler) throws IOException {
      int bufLen = 8 + viewRoot.length() * 2;
      ByteBuffer buf = allocBuffer(bufLen);
      JdwpPacket packet = new JdwpPacket(buf);
      ByteBuffer chunkBuf = getChunkDataBuf(buf);
      chunkBuf.putInt(2);
      chunkBuf.putInt(viewRoot.length());
      ByteBufferUtil.putString(chunkBuf, viewRoot);
      finishChunkPacket(packet, CHUNK_VURT, chunkBuf.position());
      client.sendAndConsume(packet, handler);
   }

   private static void sendViewOpPacket(Client client, int op, String viewRoot, String view, byte[] extra, HandleViewDebug.ViewDumpHandler handler) throws IOException {
      int bufLen = 8 + viewRoot.length() * 2 + 4 + view.length() * 2;
      if(extra != null) {
         bufLen += extra.length;
      }

      ByteBuffer buf = allocBuffer(bufLen);
      JdwpPacket packet = new JdwpPacket(buf);
      ByteBuffer chunkBuf = getChunkDataBuf(buf);
      chunkBuf.putInt(op);
      chunkBuf.putInt(viewRoot.length());
      ByteBufferUtil.putString(chunkBuf, viewRoot);
      chunkBuf.putInt(view.length());
      ByteBufferUtil.putString(chunkBuf, view);
      if(extra != null) {
         chunkBuf.put(extra);
      }

      finishChunkPacket(packet, CHUNK_VUOP, chunkBuf.position());
      if(handler != null) {
         client.sendAndConsume(packet, handler);
      } else {
         client.sendAndConsume(packet);
      }

   }

   public static void profileView(Client client, String viewRoot, String view, HandleViewDebug.ViewDumpHandler handler) throws IOException {
      sendViewOpPacket(client, 3, viewRoot, view, (byte[])null, handler);
   }

   public static void captureView(Client client, String viewRoot, String view, HandleViewDebug.ViewDumpHandler handler) throws IOException {
      sendViewOpPacket(client, 1, viewRoot, view, (byte[])null, handler);
   }

   public static void invalidateView(Client client, String viewRoot, String view) throws IOException {
      invokeMethod(client, viewRoot, view, "invalidate", new Object[0]);
   }

   public static void requestLayout(Client client, String viewRoot, String view) throws IOException {
      invokeMethod(client, viewRoot, view, "requestLayout", new Object[0]);
   }

   public static void dumpDisplayList(Client client, String viewRoot, String view) throws IOException {
      sendViewOpPacket(client, 2, viewRoot, view, (byte[])null, sViewOpNullChunkHandler);
   }

   public static void dumpTheme(Client client, String viewRoot, HandleViewDebug.ViewDumpHandler handler) throws IOException {
      ByteBuffer buf = allocBuffer(8 + viewRoot.length() * 2);
      JdwpPacket packet = new JdwpPacket(buf);
      ByteBuffer chunkBuf = getChunkDataBuf(buf);
      chunkBuf.putInt(3);
      chunkBuf.putInt(viewRoot.length());
      ByteBufferUtil.putString(chunkBuf, viewRoot);
      finishChunkPacket(packet, CHUNK_VURT, chunkBuf.position());
      client.sendAndConsume(packet, handler);
   }

   public static void invokeMethod(Client client, String viewRoot, String view, String method, Object... args) throws IOException {
      int len = 4 + method.length() * 2;
      if(args != null) {
         len += 4;
         len += 10 * args.length;
      }

      byte[] extra = new byte[len];
      ByteBuffer b = ByteBuffer.wrap(extra);
      b.putInt(method.length());
      ByteBufferUtil.putString(b, method);
      if(args != null) {
         b.putInt(args.length);

         for(int i = 0; i < args.length; ++i) {
            Object arg = args[i];
            if(arg instanceof Boolean) {
               b.putChar('Z');
               b.put((byte)(((Boolean)arg).booleanValue()?1:0));
            } else if(arg instanceof Byte) {
               b.putChar('B');
               b.put(((Byte)arg).byteValue());
            } else if(arg instanceof Character) {
               b.putChar('C');
               b.putChar(((Character)arg).charValue());
            } else if(arg instanceof Short) {
               b.putChar('S');
               b.putShort(((Short)arg).shortValue());
            } else if(arg instanceof Integer) {
               b.putChar('I');
               b.putInt(((Integer)arg).intValue());
            } else if(arg instanceof Long) {
               b.putChar('J');
               b.putLong(((Long)arg).longValue());
            } else if(arg instanceof Float) {
               b.putChar('F');
               b.putFloat(((Float)arg).floatValue());
            } else {
               if(!(arg instanceof Double)) {
                  Log.e("ddmlib", "View method invocation only supports primitive arguments, supplied: " + arg);
                  return;
               }

               b.putChar('D');
               b.putDouble(((Double)arg).doubleValue());
            }
         }
      }

      sendViewOpPacket(client, 4, viewRoot, view, extra, sViewOpNullChunkHandler);
   }

   public static void setLayoutParameter(Client client, String viewRoot, String view, String parameter, int value) throws IOException {
      int len = 4 + parameter.length() * 2 + 4;
      byte[] extra = new byte[len];
      ByteBuffer b = ByteBuffer.wrap(extra);
      b.putInt(parameter.length());
      ByteBufferUtil.putString(b, parameter);
      b.putInt(value);
      sendViewOpPacket(client, 5, viewRoot, view, extra, sViewOpNullChunkHandler);
   }

   public void handleChunk(Client client, int type, ByteBuffer data, boolean isReply, int msgId) {
   }

   public static void sendStartGlTracing(Client client) throws IOException {
      ByteBuffer buf = allocBuffer(4);
      JdwpPacket packet = new JdwpPacket(buf);
      ByteBuffer chunkBuf = getChunkDataBuf(buf);
      chunkBuf.putInt(1);
      finishChunkPacket(packet, CHUNK_VUGL, chunkBuf.position());
      client.sendAndConsume(packet);
   }

   public static void sendStopGlTracing(Client client) throws IOException {
      ByteBuffer buf = allocBuffer(4);
      JdwpPacket packet = new JdwpPacket(buf);
      ByteBuffer chunkBuf = getChunkDataBuf(buf);
      chunkBuf.putInt(0);
      finishChunkPacket(packet, CHUNK_VUGL, chunkBuf.position());
      client.sendAndConsume(packet);
   }

   static {
      sViewOpNullChunkHandler = new HandleViewDebug.NullChunkHandler(CHUNK_VUOP);
   }

   private static class NullChunkHandler extends HandleViewDebug.ViewDumpHandler {
      public NullChunkHandler(int chunkType) {
         super(chunkType);
      }

      protected void handleViewDebugResult(ByteBuffer data) {
      }
   }

   public abstract static class ViewDumpHandler extends ChunkHandler {
      private final CountDownLatch mLatch = new CountDownLatch(1);
      private final int mChunkType;

      public ViewDumpHandler(int chunkType) {
         this.mChunkType = chunkType;
      }

      void clientReady(Client client) throws IOException {
      }

      void clientDisconnected(Client client) {
      }

      void handleChunk(Client client, int type, ByteBuffer data, boolean isReply, int msgId) {
         if(type != this.mChunkType) {
            this.handleUnknownChunk(client, type, data, isReply, msgId);
         } else {
            this.handleViewDebugResult(data);
            this.mLatch.countDown();
         }
      }

      protected abstract void handleViewDebugResult(ByteBuffer var1);

      protected void waitForResult(long timeout, TimeUnit unit) {
         try {
            this.mLatch.await(timeout, unit);
         } catch (InterruptedException var5) {
            ;
         }

      }
   }
}
