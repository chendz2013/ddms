package com.android.ddmlib;

import java.util.concurrent.TimeUnit;

public class ScreenRecorderOptions {
   public final int width;
   public final int height;
   public final int bitrateMbps;
   public final long timeLimit;
   public final TimeUnit timeLimitUnits;

   private ScreenRecorderOptions(ScreenRecorderOptions.Builder builder) {
      this.width = builder.mWidth;
      this.height = builder.mHeight;
      this.bitrateMbps = builder.mBitRate;
      this.timeLimit = builder.mTime;
      this.timeLimitUnits = builder.mTimeUnits;
   }

   // $FF: synthetic method
   ScreenRecorderOptions(ScreenRecorderOptions.Builder x0, ScreenRecorderOptions.SyntheticClass_1 x1) {
      this(x0);
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
   }

   public static class Builder {
      private int mWidth;
      private int mHeight;
      private int mBitRate;
      private long mTime;
      private TimeUnit mTimeUnits;

      public ScreenRecorderOptions.Builder setSize(int w, int h) {
         this.mWidth = w;
         this.mHeight = h;
         return this;
      }

      public ScreenRecorderOptions.Builder setBitRate(int bitRateMbps) {
         this.mBitRate = bitRateMbps;
         return this;
      }

      public ScreenRecorderOptions.Builder setTimeLimit(long time, TimeUnit units) {
         this.mTime = time;
         this.mTimeUnits = units;
         return this;
      }

      public ScreenRecorderOptions build() {
         return new ScreenRecorderOptions(this);
      }
   }
}
