package com.android.ddmlib;

public final class DdmConstants {
   public static final int PLATFORM_UNKNOWN = 0;
   public static final int PLATFORM_LINUX = 1;
   public static final int PLATFORM_WINDOWS = 2;
   public static final int PLATFORM_DARWIN = 3;
   public static final int CURRENT_PLATFORM = currentPlatform();
   public static final String EXTENSION = "trace";
   public static final String DOT_TRACE = ".trace";
   public static final String FN_HPROF_CONVERTER;
   public static final String FN_TRACEVIEW;

   public static int currentPlatform() {
      String os = System.getProperty("os.name");
      return os.startsWith("Mac OS")?3:(os.startsWith("Windows")?2:(os.startsWith("Linux")?1:0));
   }

   static {
      FN_HPROF_CONVERTER = CURRENT_PLATFORM == 2?"hprof-conv.exe":"hprof-conv";
      FN_TRACEVIEW = CURRENT_PLATFORM == 2?"traceview.bat":"traceview";
   }
}
