package com.android.ddms;

import java.util.ArrayList;
import java.util.Iterator;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

/**
 * 端口编辑对话框
 */
public class StaticPortEditDialog extends Dialog {
   private static final int DLG_WIDTH = 400;
   private static final int DLG_HEIGHT = 200;
   private Shell mParent;
   private Shell mShell;
   private boolean mOk;
   private String mAppName;
   private String mPortNumber;
   private Button mOkButton;
   private Label mWarning;
   private ArrayList<Integer> mPorts;
   private int mEditPort;
   private String mDeviceSn;

   public StaticPortEditDialog(Shell parent, ArrayList<Integer> ports) {
      super(parent, 67680);
      this.mOk = false;
      this.mEditPort = -1;
      this.mPorts = ports;
      this.mDeviceSn = "emulator-5554";
   }

   public StaticPortEditDialog(Shell shell, ArrayList<Integer> ports, String oldDeviceSN, String oldAppName, String oldPortNumber) {
      this(shell, ports);
      this.mDeviceSn = oldDeviceSN;
      this.mAppName = oldAppName;
      this.mPortNumber = oldPortNumber;
      this.mEditPort = Integer.valueOf(this.mPortNumber).intValue();
   }

   public boolean open() {
      this.createUI();
      if(this.mParent != null && this.mShell != null) {
         this.mShell.setMinimumSize(400, 200);
         Rectangle r = this.mParent.getBounds();
         int cx = r.x + r.width / 2;
         int x = cx - 200;
         int cy = r.y + r.height / 2;
         int y = cy - 100;
         this.mShell.setBounds(x, y, 400, 200);
         this.mShell.open();
         Display display = this.mParent.getDisplay();

         while(!this.mShell.isDisposed()) {
            if(!display.readAndDispatch()) {
               display.sleep();
            }
         }

         return this.mOk;
      } else {
         return false;
      }
   }

   public String getDeviceSN() {
      return this.mDeviceSn;
   }

   public String getAppName() {
      return this.mAppName;
   }

   public int getPortNumber() {
      return Integer.valueOf(this.mPortNumber).intValue();
   }

   private void createUI() {
      this.mParent = this.getParent();
      this.mShell = new Shell(this.mParent, this.getStyle());
      this.mShell.setText("静态端口");
      this.mShell.setLayout(new GridLayout(1, false));
      this.mShell.addListener(21, new Listener() {
         public void handleEvent(Event event) {
         }
      });
      Composite main = new Composite(this.mShell, 0);
      main.setLayoutData(new GridData(1808));
      main.setLayout(new GridLayout(2, false));
      Label l0 = new Label(main, 0);
      l0.setText("设备名称:");
      final Text deviceSNText = new Text(main, 2052);
      deviceSNText.setLayoutData(new GridData(768));
      if(this.mDeviceSn != null) {
         deviceSNText.setText(this.mDeviceSn);
      }

      deviceSNText.addModifyListener(new ModifyListener() {
         public void modifyText(ModifyEvent e) {
            StaticPortEditDialog.this.mDeviceSn = deviceSNText.getText().trim();
            StaticPortEditDialog.this.validate();
         }
      });
      Label l = new Label(main, 0);
      l.setText("App名称:");
      final Text appNameText = new Text(main, 2052);
      if(this.mAppName != null) {
         appNameText.setText(this.mAppName);
      }

      appNameText.setLayoutData(new GridData(768));
      appNameText.addModifyListener(new ModifyListener() {
         public void modifyText(ModifyEvent e) {
            StaticPortEditDialog.this.mAppName = appNameText.getText().trim();
            StaticPortEditDialog.this.validate();
         }
      });
      Label l2 = new Label(main, 0);
      l2.setText("调试端口:");
      final Text debugPortText = new Text(main, 2052);
      if(this.mPortNumber != null) {
         debugPortText.setText(this.mPortNumber);
      }

      debugPortText.setLayoutData(new GridData(768));
      debugPortText.addModifyListener(new ModifyListener() {
         public void modifyText(ModifyEvent e) {
            StaticPortEditDialog.this.mPortNumber = debugPortText.getText().trim();
            StaticPortEditDialog.this.validate();
         }
      });
      Composite warningComp = new Composite(this.mShell, 0);
      warningComp.setLayoutData(new GridData(768));
      warningComp.setLayout(new GridLayout(1, true));
      this.mWarning = new Label(warningComp, 0);
      this.mWarning.setText("");
      this.mWarning.setLayoutData(new GridData(768));
      Composite bottomComp = new Composite(this.mShell, 0);
      bottomComp.setLayoutData(new GridData(64));
      bottomComp.setLayout(new GridLayout(2, true));
      this.mOkButton = new Button(bottomComp, 0);
      this.mOkButton.setText("确认");
      this.mOkButton.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            StaticPortEditDialog.this.mOk = true;
            StaticPortEditDialog.this.mShell.close();
         }
      });
      this.mOkButton.setEnabled(false);
      this.mShell.setDefaultButton(this.mOkButton);
      Button cancelButton = new Button(bottomComp, 0);
      cancelButton.setText("取消");
      cancelButton.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            StaticPortEditDialog.this.mShell.close();
         }
      });
      this.validate();
   }

   private void validate() {
      this.mWarning.setText("");
      if(this.mDeviceSn != null && this.mDeviceSn.length() != 0) {
         if(this.mAppName != null && this.mAppName.length() != 0) {
            String packageError = "App名必须是有效的Java包名.";
            String[] packageSegments = this.mAppName.split("\\.");
            String[] port = packageSegments;
            int len$ = packageSegments.length;

            for(int i$ = 0; i$ < len$; ++i$) {
               String i = port[i$];
               if(!i.matches("^[a-zA-Z][a-zA-Z0-9]*")) {
                  this.mWarning.setText(packageError);
                  this.mOkButton.setEnabled(false);
                  return;
               }

               if(!i.matches("^[a-z][a-z0-9]*")) {
                  this.mWarning.setText("推荐采用小写的包名.");
               }
            }

            if(this.mAppName.charAt(this.mAppName.length() - 1) == 46) {
               this.mWarning.setText(packageError);
               this.mOkButton.setEnabled(false);
            } else if(this.mPortNumber != null && this.mPortNumber.length() != 0) {
               if(!this.mPortNumber.matches("[0-9]*")) {
                  this.mWarning.setText("端口号无效.");
                  this.mOkButton.setEnabled(false);
               } else {
                  long var7 = Long.valueOf(this.mPortNumber).longValue();
                  if(var7 >= 32767L) {
                     this.mOkButton.setEnabled(false);
                  } else {
                     if(var7 != (long)this.mEditPort) {
                        Iterator var8 = this.mPorts.iterator();

                        while(var8.hasNext()) {
                           Integer var9 = (Integer)var8.next();
                           if(var7 == (long)var9.intValue()) {
                              this.mWarning.setText("端口已经在使用.");
                              this.mOkButton.setEnabled(false);
                              return;
                           }
                        }
                     }

                     this.mOkButton.setEnabled(true);
                  }
               }
            } else {
               this.mWarning.setText("端口没有输入.");
               this.mOkButton.setEnabled(false);
            }
         } else {
            this.mWarning.setText("App名没有输入.");
            this.mOkButton.setEnabled(false);
         }
      } else {
         this.mWarning.setText("设备名没有输入.");
         this.mOkButton.setEnabled(false);
      }
   }
}
