package com.android.ddms;

/**
 * This class represents the class name, field name, method name, etc.
 * possibly found in a stack frame. Values that are not defined are null.
 */
public class FrameInfo
{
    private final String className;
    private final String sourceFile;
    private final int    lineNumber;
    private final String type;
    private final String fieldName;
    private final String methodName;
    private final String arguments;


    /**
     * Creates a new FrameInfo with the given information.
     * Any undefined values can be null.
     */
    public FrameInfo(String className,
                     String sourceFile,
                     int    lineNumber,
                     String type,
                     String fieldName,
                     String methodName,
                     String arguments)
    {
        this.className  = className;
        this.sourceFile = sourceFile;
        this.lineNumber = lineNumber;
        this.type       = type;
        this.fieldName  = fieldName;
        this.methodName = methodName;
        this.arguments  = arguments;
    }


    public String getClassName()
    {
        return className;
    }


    public String getSourceFile()
    {
        return sourceFile;
    }


    public int getLineNumber()
    {
        return lineNumber;
    }


    public String getType()
    {
        return type;
    }


    public String getFieldName()
    {
        return fieldName;
    }


    public String getMethodName()
    {
        return methodName;
    }


    public String getArguments()
    {
        return arguments;
    }


    // Implementations for Object.

    public String toString()
    {
        return FrameInfo.class.getName() + "(class=["+className+"], line=["+lineNumber+"], type=["+type+"], field=["+fieldName+"], method=["+methodName+"], arguments=["+arguments+"]";
    }
}
