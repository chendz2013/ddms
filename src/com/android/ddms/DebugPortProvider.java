package com.android.ddms;

import com.android.ddmlib.IDevice;
import com.android.ddmlib.DebugPortManager.IDebugPortProvider;
import com.android.ddms.PrefsDialog;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.eclipse.jface.preference.PreferenceStore;

/**
 * 调试端口
 */
public class DebugPortProvider implements IDebugPortProvider {
   private static DebugPortProvider sThis = new DebugPortProvider();
   public static final String PREFS_STATIC_PORT_LIST = "android.staticPortList";
   private Map<String, Map<String, Integer>> mMap;

   public static DebugPortProvider getInstance() {
      return sThis;
   }

   private DebugPortProvider() {
      this.computePortList();
   }

   public int getPort(IDevice device, String appName) {
      if(this.mMap != null) {
         Map deviceMap = (Map)this.mMap.get(device.getSerialNumber());
         if(deviceMap != null) {
            Integer i = (Integer)deviceMap.get(appName);
            if(i != null) {
               return i.intValue();
            }
         }
      }

      return -1;
   }

   public Map<String, Map<String, Integer>> getPortList() {
      return this.mMap;
   }

   private void computePortList() {
      this.mMap = new HashMap();
      PreferenceStore store = PrefsDialog.getStore();
      String value = store.getString("android.staticPortList");
      if(value != null && value.length() > 0) {
         String[] portSegments = value.split("\\|");
         String[] arr$ = portSegments;
         int len$ = portSegments.length;

         for(int i$ = 0; i$ < len$; ++i$) {
            String seg = arr$[i$];
            String[] entry = seg.split(":");
            String deviceName = null;
            if(entry.length == 3) {
               deviceName = entry[2];
            } else {
               deviceName = "emulator-5554";
            }

            Map deviceMap = (Map)this.mMap.get(deviceName);
            if(deviceMap == null) {
               deviceMap = new HashMap();
               this.mMap.put(deviceName, deviceMap);
            }

            ((Map)deviceMap).put(entry[0], Integer.valueOf(entry[1]));
         }
      }

   }

   public void setPortList(Map<String, Map<String, Integer>> map) {
      this.mMap.clear();
      this.mMap.putAll(map);
      StringBuilder sb = new StringBuilder();
      Set deviceKeys = map.keySet();
      Iterator value = deviceKeys.iterator();

      while(true) {
         String store;
         Map deviceMap;
         do {
            if(!value.hasNext()) {
               String value1 = sb.toString();
               PreferenceStore store1 = PrefsDialog.getStore();
               store1.setValue("android.staticPortList", value1);
               return;
            }

            store = (String)value.next();
            deviceMap = (Map)map.get(store);
         } while(deviceMap == null);

         Set appKeys = deviceMap.keySet();
         Iterator i$ = appKeys.iterator();

         while(i$.hasNext()) {
            String appKey = (String)i$.next();
            Integer port = (Integer)deviceMap.get(appKey);
            if(port != null) {
               sb.append(appKey).append(':').append(port.intValue()).append(':').append(store).append('|');
            }
         }
      }
   }
}
