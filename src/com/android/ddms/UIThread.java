package com.android.ddms;

import com.android.SdkConstants;
import com.android.ddmlib.*;
import com.android.ddmlib.AndroidDebugBridge.IClientChangeListener;
import com.android.ddmlib.ClientData.IHprofDumpHandler;
import com.android.ddmlib.ClientData.MethodProfilingStatus;
import com.android.ddmlib.IDevice.Feature;
import com.android.ddmlib.Log.ILogOutput;
import com.android.ddmlib.Log.LogLevel;
import com.android.ddms.AboutDialog;
import com.android.ddms.DeviceCommandDialog;
import com.android.ddms.PrefsDialog;
import com.android.ddms.StaticPortConfigDialog;
import com.android.ddmuilib.AllocationPanel;
import com.android.ddmuilib.DdmUiPreferences;
import com.android.ddmuilib.DevicePanel;
import com.android.ddmuilib.EmulatorControlPanel;
import com.android.ddmuilib.HeapPanel;
import com.android.ddmuilib.ITableFocusListener;
import com.android.ddmuilib.ImageLoader;
import com.android.ddmuilib.InfoPanel;
import com.android.ddmuilib.NativeHeapPanel;
import com.android.ddmuilib.ScreenShotDialog;
import com.android.ddmuilib.SysinfoPanel;
import com.android.ddmuilib.TablePanel;
import com.android.ddmuilib.ThreadPanel;
import com.android.ddmuilib.DevicePanel.IUiSelectionListener;
import com.android.ddmuilib.ITableFocusListener.IFocusedTableActivator;
import com.android.ddmuilib.actions.ToolItemAction;
import com.android.ddmuilib.explorer.DeviceExplorer;
import com.android.ddmuilib.handler.BaseFileHandler;
import com.android.ddmuilib.handler.MethodProfilingHandler;
import com.android.ddmuilib.log.event.EventLogPanel;
import com.android.ddmuilib.logcat.LogCatPanel;
import com.android.ddmuilib.logcat.LogColors;
import com.android.ddmuilib.logcat.LogFilter;
import com.android.ddmuilib.logcat.LogPanel;
import com.android.ddmuilib.logcat.LogPanel.ILogFilterStorageManager;
import com.android.ddmuilib.net.NetworkPanel;
import com.android.ddmuilib.screenrecord.ScreenRecorderAction;
import com.android.menubar.IMenuBarCallback;
import com.android.menubar.IMenuBarEnhancer;
import com.android.menubar.MenuBarEnhancer;
import com.android.menubar.IMenuBarEnhancer.MenuBarMode;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import com.android.traceview.DmTraceReader;
import com.android.traceview.MainWindow;
import com.android.traceview.TraceUnits;
import com.google.minijoe.compiler.*;
import com.google.minijoe.compiler.ast.Program;
import com.google.minijoe.compiler.visitor.CodeGenerationVisitor;
import com.google.minijoe.compiler.visitor.DeclarationVisitor;
import com.google.minijoe.compiler.visitor.RoundtripVisitor;
import com.google.minijoe.sys.JsArray;
import com.google.minijoe.sys.JsFunction;
import com.google.minijoe.sys.JsObject;
import com.google.minijoe.sys.JsSystem;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferenceStore;
import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTError;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ControlListener;
import org.eclipse.swt.events.MenuAdapter;
import org.eclipse.swt.events.MenuEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.events.ShellListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.*;
import java.io.*;
import java.util.concurrent.TimeUnit;

public class UIThread extends  JsObject implements IUiSelectionListener, IClientChangeListener {
   public static final String APP_NAME = "DDMS";
   public static final int PANEL_CLIENT_LIST = -1;
   public static final int PANEL_INFO = 0;
   public static final int PANEL_THREAD = 1;
   public static final int PANEL_HEAP = 2;
   private static final int PANEL_NATIVE_HEAP = 3;
   private static final int PANEL_ALLOCATIONS = 4;
   private static final int PANEL_SYSINFO = 5;
   private static final int PANEL_NETWORK = 6;
   private static final int PANEL_COUNT = 7;
   private static TablePanel[] mPanels = new TablePanel[7];
   private static final String[] mPanelNames = new String[]{"信息", "线程", "VM堆", "Native堆", "分配跟踪", "系统信息", "网络"};
   private static final String[] mPanelTips = new String[]{"客户端信息", "线程状态", "VM堆状态", "Native堆状态", "分配跟踪器", "系统信息图", "网络使用"};
   private static final String PREFERENCE_LOGSASH = "logSashLocation";
   private static final String PREFERENCE_SASH = "sashLocation";
   private static final String PREFS_COL_TIME = "logcat.time";
   private static final String PREFS_COL_LEVEL = "logcat.level";
   private static final String PREFS_COL_PID = "logcat.pid";
   private static final String PREFS_COL_TAG = "logcat.tag";
   private static final String PREFS_COL_MESSAGE = "logcat.message";
   private static final String PREFS_FILTERS = "logcat.filter";
   private static UIThread mInstance = new UIThread();
   private Display mDisplay;
   private DevicePanel mDevicePanel;
   private IDevice mCurrentDevice = null;
   private Client mCurrentClient = null;
   private Label mStatusLine;
   private ToolItem mTBShowThreadUpdates;
   private ToolItem mTBShowHeapUpdates;
   private ToolItem mTBHalt;
   private ToolItem mTBCauseGc;
   private ToolItem mTBDumpHprof;
   private ToolItem mTBProfiling;
   private static final String USE_OLD_LOGCAT_VIEW = System.getenv("ANDROID_USE_OLD_LOGCAT_VIEW");
   private LogPanel mLogPanel;
   private LogCatPanel mLogCatPanel;
   private ToolItemAction mCreateFilterAction;
   private ToolItemAction mDeleteFilterAction;
   private ToolItemAction mEditFilterAction;
   private ToolItemAction mExportAction;
   private ToolItemAction mClearAction;
   private ToolItemAction[] mLogLevelActions;
   private String[] mLogLevelIcons = new String[]{"v.png", "d.png", "i.png", "w.png", "e.png"};
   protected Clipboard mClipboard;
   private MenuItem mCopyMenuItem;
   private MenuItem mSelectAllMenuItem;
   private UIThread.TableFocusListener mTableListener;
   private DeviceExplorer mExplorer = null;
   private Shell mExplorerShell = null;
   private EmulatorControlPanel mEmulatorPanel;
   private EventLogPanel mEventLogPanel;
   private Image mTracingStartImage;
   private Image mTracingStopImage;
   private ImageLoader mDdmUiLibLoader;
   //js函数
   static final int ID_EVAL = 100;
   static final int ID_HALT_VM = 101;
   static final int ID_FORCE_GC = 102;
   static final int ID_RESTART_ADB = 103;
   static final int ID_PS = 104;
   static final int ID_DEVICE_STATE = 105;
   static final int ID_DUMP_SYS = 106;
   static final int ID_RADIO_STATE = 107;
   static final int ID_LOGCAT = 108;
   static final int ID_DUMP_HPROF = 109;
   static final int ID_LAUNCH_UC = 110;
   static final int ID_ENABLE_HEAP_UPDATE = 111;
   static final int ID_ENABLE_THREAD_UPDATE = 112;
   static final int ID_START_SAMPLE = 113;
   static final int ID_END_SAMPLE = 114;
   static final int ID_START_TRACE = 115;
   static final int ID_END_TRACE = 116;

   static final int ID_LAUNCH_CAMERA = 150;
   static final int ID_FORCE_STOP_UC = 151;
   static final int ID_LOW_BATTERY = 152;
   static final int ID_START_VIEWACTION = 153;
   static final int ID_KILL_ALL = 154;
   static final int ID_DISPLAY_DENSITY = 155;
   static final int ID_WIFI_SETTING = 156;
   static final int ID_UC_ADDBOOKMARK = 157;
   static final int ID_SEARCH = 158;
   static final int ID_CATA_DEFAULT = 159;
   static final int ID_OPEN_URL = 160;
   static final int ID_LAUNCH_MUSIC = 161;
   static final int ID_OPEN_LOC = 162;

   static final int ID_GET_PROP = 163;
   static final int ID_WATCH_PROP = 164;
   static final int ID_SHUTDOWN_BC = 165;

   static final int ID_LIST_PKG = 180;
   static final int ID_LIST_USER = 181;




   static final JsObject COMPILER_PROTOTYPE = new JsObject(OBJECT_PROTOTYPE);

   public static boolean useOldLogCatView() {
      return USE_OLD_LOGCAT_VIEW != null;
   }

   private UIThread() {
      super(COMPILER_PROTOTYPE);
      scopeChain = JsSystem.createGlobal();
      addVar("eval", new JsFunction(ID_EVAL, 2));
      addVar("haltVM", new JsFunction(ID_HALT_VM, 0));
      addVar("forceGC", new JsFunction(ID_FORCE_GC, 0));
      addVar("restartAdb", new JsFunction(ID_RESTART_ADB, 0));
      addVar("progressState", new JsFunction(ID_PS, 0));
      addVar("deviceState", new JsFunction(ID_DEVICE_STATE, 0));
      addVar("radioState", new JsFunction(ID_RADIO_STATE, 0));
      addVar("dumpSys", new JsFunction(ID_DUMP_SYS, 0));
      addVar("logcat", new JsFunction(ID_LOGCAT, 0));
      addVar("dumpHprof", new JsFunction(ID_DUMP_HPROF, 0));
      addVar("launchUC", new JsFunction(ID_LAUNCH_UC, 0));
      addVar("enableHeapUpdate", new JsFunction(ID_ENABLE_HEAP_UPDATE, 0));
      addVar("enableThreadUpdate", new JsFunction(ID_ENABLE_THREAD_UPDATE, 0));
      addVar("startSample", new JsFunction(ID_START_SAMPLE, 0));
      addVar("endSample", new JsFunction(ID_END_SAMPLE, 0));
      addVar("startTrace", new JsFunction(ID_START_TRACE, 0));
      addVar("endTrace", new JsFunction(ID_END_TRACE, 0));

      addVar("launchCamera", new JsFunction(ID_LAUNCH_CAMERA, 0));
      addVar("forceStopUC", new JsFunction(ID_FORCE_STOP_UC,0));
      addVar("lowBattery", new JsFunction(ID_LOW_BATTERY, 0));
      addVar("startView", new JsFunction(ID_START_VIEWACTION, 0));
      addVar("killAll", new JsFunction(ID_KILL_ALL, 0));
      addVar("displayDensity", new JsFunction(ID_DISPLAY_DENSITY, 1));
      addVar("wifiSetting", new JsFunction(ID_WIFI_SETTING,0));
      addVar("addBookmark", new JsFunction(ID_UC_ADDBOOKMARK, 0));
      addVar("search", new JsFunction(ID_SEARCH, 0));
      addVar("cataDefault", new JsFunction(ID_CATA_DEFAULT, 0));
      addVar("openURL", new JsFunction(ID_OPEN_URL, 1));
      addVar("launchMusic", new JsFunction(ID_LAUNCH_MUSIC, 0));
      addVar("openLoc", new JsFunction(ID_OPEN_LOC, 1));
      addVar("getProp", new JsFunction(ID_GET_PROP, 0));
      addVar("watchProp", new JsFunction(ID_WATCH_PROP, 0));
      addVar("shutdownBC", new JsFunction(ID_SHUTDOWN_BC, 0));

      addVar("listPackage", new JsFunction(ID_LIST_PKG, 0));
      addVar("listUser", new JsFunction(ID_LIST_USER, 0));
      //----
      mPanels[0] = new InfoPanel();
      mPanels[1] = new ThreadPanel();
      mPanels[2] = new HeapPanel();
      if(PrefsDialog.getStore().getBoolean("native")) {
         if(System.getenv("ANDROID_DDMS_OLD_HEAP_PANEL") != null) {
            mPanels[3] = new NativeHeapPanel();
         } else {
            mPanels[3] = new com.android.ddmuilib.heap.NativeHeapPanel(this.getStore());
         }
      } else {
         mPanels[3] = null;
      }

      mPanels[4] = new AllocationPanel();
      mPanels[5] = new SysinfoPanel();
      mPanels[6] = new NetworkPanel();
   }

   public void evalNative(int index, JsArray stack, int sp, int parCount) {
      switch (index) {

         case ID_HALT_VM:
            if (this.mCurrentClient != null) {
               Client client = this.mCurrentClient;
               client.kill();
            }else{
               System.out.println("current client no selected");
            }
            break;

         case ID_FORCE_GC:
            if(this.mCurrentClient != null) {
               this.mCurrentClient.executeGarbageCollector();
            }else{
               System.out.println("current client no selected");
            }
            break;

         case ID_RESTART_ADB:
            AndroidDebugBridge bridge = AndroidDebugBridge.getBridge();
            if(bridge != null) {
               bridge.restart();
            }
            break;

         case ID_PS:
            execCommand("ps -x");
            break;


         case ID_DEVICE_STATE:
            execCommand("system/bin/dumpstate /proc/self/fd/0");
            break;

         case ID_DUMP_SYS:
            execCommand("dumpsys");
            break;

         case ID_RADIO_STATE:
            execCommand("cat /data/logs/radio.4 /data/logs/radio.3 /data/logs/radio.2 /data/logs/radio.1 /data/logs/radio");
            break;

         case ID_LOGCAT:
            execCommand("logcat '*:d jdwp:w'");
            break;


         case ID_DUMP_HPROF:
            if(this.mCurrentClient != null) {
               this.mCurrentClient.dumpHprof();
            }
            break;

         case ID_LAUNCH_UC:
            String cmd2 = "am start  -n \"com.UCMobile/com.UCMobile.main.UCMobile\" -a android.intent.action.MAIN -c android.intent.category.LAUNCHER";
            execCommand(cmd2);
            break;


         case ID_ENABLE_HEAP_UPDATE:
            this.mCurrentClient.setHeapUpdateEnabled(true);
            break;

         case ID_ENABLE_THREAD_UPDATE:
            this.mCurrentClient.setThreadUpdateEnabled(true);
            break;

         case ID_START_SAMPLE:
            try {
               ClientData cd = this.mCurrentClient.getClientData();
               boolean supportsSampling = cd.hasFeature("method-sample-profiling");
               if (supportsSampling) {
                  this.mCurrentClient.startSamplingProfiler(1000, TimeUnit.MICROSECONDS);
               }else{
                  System.out.println("sampling not support");
               }
            }catch(Exception ex){
               ex.printStackTrace();
            }
            break;

         case ID_END_SAMPLE:
            try {
               this.mCurrentClient.stopSamplingProfiler();
            }catch(Exception ex){
               ex.printStackTrace();
            }
            break;

         case ID_START_TRACE:
            try {
               this.mCurrentClient.startMethodTracer();
            }catch (Exception ex){
               ex.printStackTrace();
            }
            break;

         case ID_END_TRACE:
            try {
               this.mCurrentClient.stopMethodTracer();
            }catch(Exception ex){
               ex.printStackTrace();
            }
            break;

         case ID_EVAL:
            try {
               stack.setObject(
                       sp,
                       eval(stack.getString(sp + 2),
                               stack.isNull(sp + 3) ? stack.getJsObject(sp) : stack.getJsObject(sp + 3))
               );
            } catch (Exception e) {
               throw new RuntimeException("" + e);
            }

            break;

         case ID_LAUNCH_CAMERA:
            this.execCommand("am start -n com.android.camera/.Camera");
            break;

         case ID_FORCE_STOP_UC:
            this.execCommand("am force-stop com.UCMobile");
            break;

         case ID_LOW_BATTERY:
            this.execCommand("am broadcast -a android.intent.action.BATTERY_CHANGED --ei \"level\" 3 --ei \"scale\" 100");
            break;

         case ID_START_VIEWACTION:
            this.execCommand("am start -a android.intent.action.VIEW");
            break;

         case ID_KILL_ALL://Kill all background processes.
            this.execCommand("am kill-all");
            break;

         case ID_DISPLAY_DENSITY:
            int density = stack.getInt(sp+2);
            this.execCommand("am display-density "+ density);
            break;

         case ID_WIFI_SETTING:
            this.execCommand("am start -a android.settings.WIFI_SETTINGS");
            break;

         case ID_UC_ADDBOOKMARK:
            this.execCommand("am start -n  com.UCMobile/com.uc.browser.FavoriteActivity");
            break;

         case ID_SEARCH:
            this.execCommand("am start -a android.intent.action.SEARCH");
            break;

         case ID_CATA_DEFAULT:
            this.execCommand("am start -a com.UCMobile.intent.action.INVOKE");
            break;

         case ID_OPEN_URL:
            this.execCommand("am start -a android.intent.action.VIEW -d "+stack.getString(sp+2));
            //openURL之后进入了浏览器的选择处理
            break;

         case ID_LAUNCH_MUSIC:
            this.execCommand("am start -n com.android.music/com.android.music.MusicBrowserActivity");
            break;


         case ID_OPEN_LOC:
            this.execCommand("am start -a android.intent.action.VIEW -d geo:0,0?q=beijing");
            break;
         //----------------
         case ID_LIST_PKG:
            this.execCommand("pm list packages -f");
            break;

         case ID_LIST_USER:
            this.execCommand("pm list users");
            break;
         //--------------
         case ID_GET_PROP:
            this.execCommand("getprop");
            break;

         case ID_WATCH_PROP:
            this.execCommand("watchprops");
            break;

         case ID_SHUTDOWN_BC:
            this.execCommand("am broadcast -a android.intent.action.ACTION_SHUTDOWN");
            break;

         default:
            super.evalNative(index, stack, sp, parCount);
      }
   }

   private void execCommand(String cmd) {
      if (this.mCurrentDevice == null){
         System.out.println("no select device");
         return;
      }
      try{
         this.mCurrentDevice.executeShellCommand(cmd, new IShellOutputReceiver() {
            @Override
            public void addOutput(byte[] var1, int var2, int var3) {
               try {
                  String str = new String(var1, var2, var3, "ISO-8859-1");
                  //怎么处理好呢？
                  System.out.println(str);
               } catch (Exception ex) {
                  ex.printStackTrace();
               }
            }

            @Override
            public void flush() {

            }

            @Override
            public boolean isCancelled() {
               return false;
            }
         });
      }catch(Exception ex){
         ex.printStackTrace();
      }
   }

   public static void compile(String input, OutputStream os) throws CompilerException, IOException {
      Lexer lexer = new Lexer(input);
      Parser parser = new Parser(lexer);

      Program program = parser.parseProgram();

      if (Config.DEBUG_SOURCE) {
         Writer w = new OutputStreamWriter(System.out);
         new RoundtripVisitor(w).visit(program);
         w.flush();
      }

      // handle variable and function declarations
      new DeclarationVisitor().visit(program);

      DataOutputStream dos = new DataOutputStream(os);
      new CodeGenerationVisitor(dos).visit(program);
      dos.flush();
   }

   public static Object eval(String input, JsObject context) throws CompilerException, IOException {
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      compile(input, baos);
      byte[] code = baos.toByteArray();

      if (Config.DEBUG_DISSASSEMBLY) {
         new Disassembler(new DataInputStream(new ByteArrayInputStream(code))).dump();
      }

      return JsFunction.exec(new DataInputStream(new ByteArrayInputStream(code)), context);
   }

   public static UIThread getInstance() {
      return mInstance;
   }

   public Display getDisplay() {
      return this.mDisplay;
   }

   public void asyncExec(Runnable r) {
      if(this.mDisplay != null && !this.mDisplay.isDisposed()) {
         this.mDisplay.asyncExec(r);
      }

   }

   public IPreferenceStore getStore() {
      return PrefsDialog.getStore();
   }

   public void runUI(String ddmsParentLocation) {
      Display.setAppName("DDMS");
      this.mDisplay = Display.getDefault();
      Shell shell = new Shell(this.mDisplay, 1264);
      this.mDdmUiLibLoader = ImageLoader.getDdmUiLibLoader();
      shell.setImage(ImageLoader.getLoader(this.getClass()).loadImage(this.mDisplay, "ddms-128.png", 100, 50, (Color)null));
      Log.setLogOutput(new ILogOutput() {
         public void printAndPromptLog(final LogLevel logLevel, final String tag, final String message) {
            Log.printLog(logLevel, tag, message);
            UIThread.this.mDisplay.asyncExec(new Runnable() {
               public void run() {
                  Shell activeShell = UIThread.this.mDisplay.getActiveShell();
                  if(logLevel == LogLevel.ERROR) {
                     MessageDialog.openError(activeShell, tag, message);
                  } else {
                     MessageDialog.openWarning(activeShell, tag, message);
                  }

               }
            });
         }

         public void printLog(LogLevel logLevel, String tag, String message) {
            Log.printLog(logLevel, tag, message);
         }
      });
      ClientData.setHprofDumpHandler(new UIThread.HProfHandler(shell));
      ClientData.setMethodProfilingHandler(new MethodProfilingHandler(shell));
      //读取adb的位置
      String adbLocation;
      if(ddmsParentLocation != null && ddmsParentLocation.length() != 0) {
         File arr$ = new File((new File(ddmsParentLocation)).getParent(), "platform-tools");
         if(arr$.isDirectory()) {
            adbLocation = arr$.getAbsolutePath() + File.separator + SdkConstants.FN_ADB;
         } else {
            String len$ = System.getenv("ANDROID_HOST_OUT");
            if(len$ != null) {
               adbLocation = len$ + File.separator + "bin" + File.separator + SdkConstants.FN_ADB;
            } else {
               adbLocation = SdkConstants.FN_ADB;
            }
         }
      } else {
         adbLocation = SdkConstants.FN_ADB;
      }
      //初始化adb
      AndroidDebugBridge.init(true);
      //
      AndroidDebugBridge.createBridge(adbLocation, true);
      //adb监听UIThread
      AndroidDebugBridge.addClientChangeListener(this);
      //
      shell.setText("Dalvik Debug Monitor");
      this.setConfirmClose(shell);
      //创建菜单
      this.createMenus(shell);
      //创建各种panel
      this.createWidgets(shell);
      //开始打开
      shell.pack();
      this.setSizeAndPosition(shell);
      shell.open();
      Log.d("ddms", "UI is up");

      while(!shell.isDisposed()) {
         if(!this.mDisplay.readAndDispatch()) {
            this.mDisplay.sleep();
         }
      }

      if(useOldLogCatView()) {
         this.mLogPanel.stopLogCat(true);
      }

      this.mDevicePanel.dispose();
      TablePanel[] var8 = mPanels;
      int var9 = var8.length;

      for(int i$ = 0; i$ < var9; ++i$) {
         TablePanel panel = var8[i$];
         if(panel != null) {
            panel.dispose();
         }
      }

      ImageLoader.dispose();
      this.mDisplay.dispose();
      Log.d("ddms", "UI is down");
   }

   private void setSizeAndPosition(final Shell shell) {
      shell.setMinimumSize(400, 200);
      PreferenceStore prefs = PrefsDialog.getStore();
      int x = prefs.getInt("shellX");
      int y = prefs.getInt("shellY");
      int w = prefs.getInt("shellWidth");
      int h = prefs.getInt("shellHeight");
      Rectangle rect = this.mDisplay.getClientArea();
      if(w > rect.width) {
         w = rect.width;
         prefs.setValue("shellWidth", rect.width);
      }

      if(h > rect.height) {
         h = rect.height;
         prefs.setValue("shellHeight", rect.height);
      }

      if(x < rect.x) {
         x = rect.x;
         prefs.setValue("shellX", rect.x);
      } else if(x >= rect.x + rect.width) {
         x = rect.x + rect.width - w;
         prefs.setValue("shellX", rect.x);
      }

      if(y < rect.y) {
         y = rect.y;
         prefs.setValue("shellY", rect.y);
      } else if(y >= rect.y + rect.height) {
         y = rect.y + rect.height - h;
         prefs.setValue("shellY", rect.y);
      }

      shell.setBounds(x, y, w, h);
      shell.addControlListener(new ControlListener() {
         public void controlMoved(ControlEvent e) {
            Rectangle controlBounds = shell.getBounds();
            PreferenceStore currentPrefs = PrefsDialog.getStore();
            currentPrefs.setValue("shellX", controlBounds.x);
            currentPrefs.setValue("shellY", controlBounds.y);
         }

         public void controlResized(ControlEvent e) {
            Rectangle controlBounds = shell.getBounds();
            PreferenceStore currentPrefs = PrefsDialog.getStore();
            currentPrefs.setValue("shellWidth", controlBounds.width);
            currentPrefs.setValue("shellHeight", controlBounds.height);
         }
      });
   }

   private void setExplorerSizeAndPosition(final Shell shell) {
      shell.setMinimumSize(400, 200);
      PreferenceStore prefs = PrefsDialog.getStore();
      int x = prefs.getInt("explorerShellX");
      int y = prefs.getInt("explorerShellY");
      int w = prefs.getInt("explorerShellWidth");
      int h = prefs.getInt("explorerShellHeight");
      Rectangle rect = this.mDisplay.getClientArea();
      if(w > rect.width) {
         w = rect.width;
         prefs.setValue("explorerShellWidth", rect.width);
      }

      if(h > rect.height) {
         h = rect.height;
         prefs.setValue("explorerShellHeight", rect.height);
      }

      if(x < rect.x) {
         x = rect.x;
         prefs.setValue("explorerShellX", rect.x);
      } else if(x >= rect.x + rect.width) {
         x = rect.x + rect.width - w;
         prefs.setValue("explorerShellX", rect.x);
      }

      if(y < rect.y) {
         y = rect.y;
         prefs.setValue("explorerShellY", rect.y);
      } else if(y >= rect.y + rect.height) {
         y = rect.y + rect.height - h;
         prefs.setValue("explorerShellY", rect.y);
      }

      shell.setBounds(x, y, w, h);
      shell.addControlListener(new ControlListener() {
         public void controlMoved(ControlEvent e) {
            Rectangle controlBounds = shell.getBounds();
            PreferenceStore currentPrefs = PrefsDialog.getStore();
            currentPrefs.setValue("explorerShellX", controlBounds.x);
            currentPrefs.setValue("explorerShellY", controlBounds.y);
         }

         public void controlResized(ControlEvent e) {
            Rectangle controlBounds = shell.getBounds();
            PreferenceStore currentPrefs = PrefsDialog.getStore();
            currentPrefs.setValue("explorerShellWidth", controlBounds.width);
            currentPrefs.setValue("explorerShellHeight", controlBounds.height);
         }
      });
   }

   private void setConfirmClose(Shell shell) {
   }

   private void createMenus(final Shell shell) {
      Menu menuBar = new Menu(shell, 2);
      MenuItem fileItem = new MenuItem(menuBar, 64);
      fileItem.setText("&文件");
      MenuItem editItem = new MenuItem(menuBar, 64);
      editItem.setText("&编辑");
      MenuItem actionItem = new MenuItem(menuBar, 64);
      actionItem.setText("&操作");
      MenuItem deviceItem = new MenuItem(menuBar, 64);
      deviceItem.setText("&设备");
      MenuItem scriptItem = new MenuItem(menuBar, 64);
      scriptItem.setText("&脚本");

      Menu fileMenu = new Menu(menuBar);
      fileItem.setMenu(fileMenu);
      Menu editMenu = new Menu(menuBar);
      editItem.setMenu(editMenu);
      Menu actionMenu = new Menu(menuBar);
      actionItem.setMenu(actionMenu);
      Menu deviceMenu = new Menu(menuBar);
      deviceItem.setMenu(deviceMenu);
      Menu scriptMenu = new Menu(menuBar);
      scriptItem.setMenu(scriptMenu);
      final String path = System.getProperty("user.dir")+"/script";
      String[] files = new File(path).list();
      for(int i=0; i < files.length; i++){
         final String file = files[i];
         if (file.endsWith(".js")){
            MenuItem item = new MenuItem(scriptMenu, 0);
            item.setText(file);
            item.addSelectionListener(
                    new SelectionAdapter() {
                       @Override
                       public void widgetSelected(SelectionEvent selectionEvent) {
                          //super.widgetSelected(selectionEvent);
                          //执行脚本
                          String str = new String(readFile(path +"/"+file));
                          try {
                             Object result = eval(str, UIThread.this);
                             System.out.println(result.toString());
                          }catch(CompilerException ex){
                             ex.printStackTrace();
                          }catch(IOException ex){
                             ex.printStackTrace();
                          }
                       }
                    }
            );
         }
      }
      //文件菜单
      MenuItem item = new MenuItem(fileMenu, 0);
      item.setText("&静态端口配置...");
      item.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            StaticPortConfigDialog dlg = new StaticPortConfigDialog(shell);
            dlg.open();
         }
      });
      IMenuBarEnhancer enhancer = MenuBarEnhancer.setupMenu("DDMS", fileMenu, new IMenuBarCallback() {
         public void printError(String format, Object... args) {
            Log.e("DDMS Menu Bar", String.format(format, args));
         }

         public void onPreferencesMenuSelected() {
            PrefsDialog.run(shell);
         }

         public void onAboutMenuSelected() {
            AboutDialog dlg = new AboutDialog(shell);
            dlg.open();
         }
      });
      if(enhancer.getMenuBarMode() == MenuBarMode.GENERIC) {
         new MenuItem(fileMenu, 2);
         item = new MenuItem(fileMenu, 0);
         item.setText("&退出\tCtrl-Q");
         item.setAccelerator(81 | SWT.MOD1);
         item.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent e) {
               shell.close();
            }
         });
      }
      //编辑菜单
      this.mCopyMenuItem = new MenuItem(editMenu, 0);
      this.mCopyMenuItem.setText("&复制\tCtrl-C");
      this.mCopyMenuItem.setAccelerator(67 | SWT.MOD1);
      this.mCopyMenuItem.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            UIThread.this.mTableListener.copy(UIThread.this.mClipboard);
         }
      });
      new MenuItem(editMenu, 2);
      this.mSelectAllMenuItem = new MenuItem(editMenu, 0);
      this.mSelectAllMenuItem.setText("&全选\tCtrl-A");
      this.mSelectAllMenuItem.setAccelerator(65 | SWT.MOD1);
      this.mSelectAllMenuItem.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            UIThread.this.mTableListener.selectAll();
         }
      });
      //操作菜单
      final MenuItem actionHaltItem = new MenuItem(actionMenu, 0);
      actionHaltItem.setText("&停止VM");
      actionHaltItem.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            //往JDWP发送一个exit的命名
            UIThread.this.mDevicePanel.killSelectedClient();
         }
      });
      final MenuItem actionCauseGcItem = new MenuItem(actionMenu, 0);
      actionCauseGcItem.setText("强制&GC");
      actionCauseGcItem.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            UIThread.this.mDevicePanel.forceGcOnSelectedClient();
         }
      });
      final MenuItem actionResetAdb = new MenuItem(actionMenu, 0);
      actionResetAdb.setText("&重启adb");
      actionResetAdb.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            AndroidDebugBridge bridge = AndroidDebugBridge.getBridge();
            if(bridge != null) {
               bridge.restart();
            }

         }
      });
      actionMenu.addMenuListener(new MenuAdapter() {
         public void menuShown(MenuEvent e) {
            actionHaltItem.setEnabled(UIThread.this.mTBHalt.getEnabled() && UIThread.this.mCurrentClient != null);
            actionCauseGcItem.setEnabled(UIThread.this.mTBCauseGc.getEnabled() && UIThread.this.mCurrentClient != null);
            actionResetAdb.setEnabled(true);
         }
      });
      //设备菜单
      final MenuItem screenShotItem = new MenuItem(deviceMenu, 0);
      screenShotItem.setText("屏幕捕获...\tCtrl-S");
      screenShotItem.setAccelerator(83 | SWT.MOD1);
      final MenuItem screenRecordItem = new MenuItem(deviceMenu, 0);
      screenRecordItem.setText("屏幕记录");
      SelectionAdapter selectionListener = new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            if(UIThread.this.mCurrentDevice != null) {
               if(e.getSource() == screenShotItem) {
                  ScreenShotDialog dlg = new ScreenShotDialog(shell);
                  dlg.open(UIThread.this.mCurrentDevice);
               } else if(e.getSource() == screenRecordItem) {
                  (new ScreenRecorderAction(shell, UIThread.this.mCurrentDevice)).performAction();
               }

            }
         }
      };
      screenShotItem.addSelectionListener(selectionListener);
      screenRecordItem.addSelectionListener(selectionListener);
      new MenuItem(deviceMenu, 2);
      final MenuItem explorerItem = new MenuItem(deviceMenu, 0);
      explorerItem.setText("文件管理...");
      explorerItem.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            UIThread.this.createFileExplorer();
         }
      });
      final MenuItem opentraceItem = new MenuItem(deviceMenu, 0);
      opentraceItem.setText("打开Trace文件");
      opentraceItem.addSelectionListener(
              new SelectionAdapter() {
                 @Override
                 public void widgetSelected(SelectionEvent selectionEvent) {
                      UIThread.this.openTraceView();
                 }
              }
      );

      //增加关联mapping文件
      final MenuItem applyMapping = new MenuItem(deviceMenu, 0);
      applyMapping.setText("关联Mapping文件");
      applyMapping.addSelectionListener(
              new SelectionAdapter() {
                 @Override
                 public void widgetSelected(SelectionEvent selectionEvent) {
                    UIThread.this.applyMapping();
                 }
              }
      );
      //添加脚本的执行
      final MenuItem jsScript = new MenuItem(deviceMenu, 0);
      jsScript.setText("执行脚本");
      jsScript.addSelectionListener(
              new SelectionAdapter() {
                 @Override
                 public void widgetSelected(SelectionEvent selectionEvent) {
                    UIThread.this.evalScript();
                 }
              }
      );

      new MenuItem(deviceMenu, 2);
      final MenuItem processItem = new MenuItem(deviceMenu, 0);
      processItem.setText("显示进程状态...");
      processItem.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            DeviceCommandDialog dlg = new DeviceCommandDialog("ps -x", "ps-x.txt", shell);
            dlg.open(UIThread.this.mCurrentDevice);
         }
      });
      final MenuItem deviceStateItem = new MenuItem(deviceMenu, 0);
      deviceStateItem.setText("导出设备状态...");
      deviceStateItem.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            DeviceCommandDialog dlg = new DeviceCommandDialog("/system/bin/dumpstate /proc/self/fd/0", "device-state.txt", shell);
            dlg.open(UIThread.this.mCurrentDevice);
         }
      });
      final MenuItem appStateItem = new MenuItem(deviceMenu, 0);
      appStateItem.setText("导出App状态...");
      appStateItem.setEnabled(false);
      appStateItem.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            DeviceCommandDialog dlg = new DeviceCommandDialog("dumpsys", "app-state.txt", shell);
            dlg.open(UIThread.this.mCurrentDevice);
         }
      });
      final MenuItem radioStateItem = new MenuItem(deviceMenu, 0);
      radioStateItem.setText("导出射频信息...");
      radioStateItem.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            DeviceCommandDialog dlg = new DeviceCommandDialog("cat /data/logs/radio.4 /data/logs/radio.3 /data/logs/radio.2 /data/logs/radio.1 /data/logs/radio", "radio-state.txt", shell);
            dlg.open(UIThread.this.mCurrentDevice);
         }
      });
      final MenuItem logCatItem = new MenuItem(deviceMenu, 0);
      logCatItem.setText("运行logcat...");
      logCatItem.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            DeviceCommandDialog dlg = new DeviceCommandDialog("logcat \'*:d jdwp:w\'", "log.txt", shell);
            dlg.open(UIThread.this.mCurrentDevice);
         }
      });
      deviceMenu.addMenuListener(new MenuAdapter() {
         public void menuShown(MenuEvent e) {
            boolean deviceEnabled = UIThread.this.mCurrentDevice != null;
            screenShotItem.setEnabled(deviceEnabled);
            explorerItem.setEnabled(deviceEnabled);
            processItem.setEnabled(deviceEnabled);
            deviceStateItem.setEnabled(deviceEnabled);
            appStateItem.setEnabled(deviceEnabled);
            radioStateItem.setEnabled(deviceEnabled);
            logCatItem.setEnabled(deviceEnabled);
            screenRecordItem.setEnabled(UIThread.this.mCurrentDevice != null && UIThread.this.mCurrentDevice.supportsFeature(Feature.SCREEN_RECORD));
         }
      });
      shell.setMenuBar(menuBar);
   }

   private void createWidgets(Shell shell) {
      Color darkGray = shell.getDisplay().getSystemColor(16);
      shell.setLayout(new GridLayout(1, false));
      final Composite panelArea = new Composite(shell, 2048);
      panelArea.setLayoutData(new GridData(1808));
      this.mStatusLine = new Label(shell, 0);
      this.mStatusLine.setLayoutData(new GridData(768));
      this.mStatusLine.setText("Initializing...");
      final PreferenceStore prefs = PrefsDialog.getStore();
      Composite topPanel = new Composite(panelArea, 0);
      final Sash sash = new Sash(panelArea, 256);
      sash.setBackground(darkGray);
      Composite bottomPanel = new Composite(panelArea, 0);
      panelArea.setLayout(new FormLayout());
      this.createTopPanel(topPanel, darkGray);
      this.mClipboard = new Clipboard(panelArea.getDisplay());
      if(useOldLogCatView()) {
         this.createBottomPanel(bottomPanel);
      } else {
         this.createLogCatView(bottomPanel);
      }

      FormData data = new FormData();
      data.top = new FormAttachment(0, 0);
      data.bottom = new FormAttachment(sash, 0);
      data.left = new FormAttachment(0, 0);
      data.right = new FormAttachment(100, 0);
      topPanel.setLayoutData(data);
      final FormData sashData = new FormData();
      if(prefs != null && prefs.contains("logSashLocation")) {
         sashData.top = new FormAttachment(0, prefs.getInt("logSashLocation"));
      } else {
         sashData.top = new FormAttachment(50, 0);
      }

      sashData.left = new FormAttachment(0, 0);
      sashData.right = new FormAttachment(100, 0);
      sash.setLayoutData(sashData);
      data = new FormData();
      data.top = new FormAttachment(sash, 0);
      data.bottom = new FormAttachment(100, 0);
      data.left = new FormAttachment(0, 0);
      data.right = new FormAttachment(100, 0);
      bottomPanel.setLayoutData(data);
      sash.addListener(13, new Listener() {
         public void handleEvent(Event e) {
            Rectangle sashRect = sash.getBounds();
            Rectangle panelRect = panelArea.getClientArea();
            int bottom = panelRect.height - sashRect.height - 100;
            e.y = Math.max(Math.min(e.y, bottom), 100);
            if(e.y != sashRect.y) {
               sashData.top = new FormAttachment(0, e.y);
               if(prefs != null) {
                  prefs.setValue("logSashLocation", e.y);
               }

               panelArea.layout();
            }

         }
      });
      this.mTableListener = new UIThread.TableFocusListener(null);
      if(useOldLogCatView()) {
         this.mLogPanel.setTableFocusListener(this.mTableListener);
      } else {
         this.mLogCatPanel.setTableFocusListener(this.mTableListener);
      }

      this.mEventLogPanel.setTableFocusListener(this.mTableListener);
      TablePanel[] arr$ = mPanels;
      int len$ = arr$.length;

      for(int i$ = 0; i$ < len$; ++i$) {
         TablePanel p = arr$[i$];
         if(p != null) {
            p.setTableFocusListener(this.mTableListener);
         }
      }

      this.mStatusLine.setText("");
   }

   /**
    * DevicePanel的工具栏
    * @param toolBar
     */
   private void createDevicePanelToolBar(ToolBar toolBar) {
      Display display = toolBar.getDisplay();
      this.mTBShowHeapUpdates = new ToolItem(toolBar, 32);
      this.mTBShowHeapUpdates.setImage(this.mDdmUiLibLoader.loadImage(display, "heap.png", 16, 16, (Color)null));
      this.mTBShowHeapUpdates.setToolTipText("Show heap updates");
      this.mTBShowHeapUpdates.setEnabled(false);
      this.mTBShowHeapUpdates.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            if(UIThread.this.mCurrentClient != null) {
               boolean enable = !UIThread.this.mCurrentClient.isHeapUpdateEnabled();
               UIThread.this.mCurrentClient.setHeapUpdateEnabled(enable);
            } else {
               e.doit = false;
            }

         }
      });
      this.mTBDumpHprof = new ToolItem(toolBar, 8);
      this.mTBDumpHprof.setToolTipText("Dump HPROF file");
      this.mTBDumpHprof.setEnabled(false);
      this.mTBDumpHprof.setImage(this.mDdmUiLibLoader.loadImage(display, "hprof.png", 16, 16, (Color)null));
      this.mTBDumpHprof.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            UIThread.this.mDevicePanel.dumpHprof();
            UIThread.this.enableButtons();
         }
      });
      this.mTBCauseGc = new ToolItem(toolBar, 8);
      this.mTBCauseGc.setToolTipText("Cause an immediate GC");
      this.mTBCauseGc.setEnabled(false);
      this.mTBCauseGc.setImage(this.mDdmUiLibLoader.loadImage(display, "gc.png", 16, 16, (Color)null));
      this.mTBCauseGc.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            UIThread.this.mDevicePanel.forceGcOnSelectedClient();
         }
      });
      new ToolItem(toolBar, 2);
      this.mTBShowThreadUpdates = new ToolItem(toolBar, 32);
      this.mTBShowThreadUpdates.setImage(this.mDdmUiLibLoader.loadImage(display, "thread.png", 16, 16, (Color)null));
      this.mTBShowThreadUpdates.setToolTipText("Show thread updates");
      this.mTBShowThreadUpdates.setEnabled(false);
      this.mTBShowThreadUpdates.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            if(UIThread.this.mCurrentClient != null) {
               boolean enable = !UIThread.this.mCurrentClient.isThreadUpdateEnabled();
               UIThread.this.mCurrentClient.setThreadUpdateEnabled(enable);
            } else {
               e.doit = false;
            }

         }
      });
      this.mTracingStartImage = this.mDdmUiLibLoader.loadImage(display, "tracing_start.png", 16, 16, (Color)null);
      this.mTracingStopImage = this.mDdmUiLibLoader.loadImage(display, "tracing_stop.png", 16, 16, (Color)null);
      this.mTBProfiling = new ToolItem(toolBar, 8);
      this.mTBProfiling.setToolTipText("Start Method Profiling");
      this.mTBProfiling.setEnabled(false);
      this.mTBProfiling.setImage(this.mTracingStartImage);
      this.mTBProfiling.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            UIThread.this.mDevicePanel.toggleMethodProfiling();
         }
      });
      new ToolItem(toolBar, 2);
      this.mTBHalt = new ToolItem(toolBar, 8);
      this.mTBHalt.setToolTipText("Halt the target VM");
      this.mTBHalt.setEnabled(false);
      this.mTBHalt.setImage(this.mDdmUiLibLoader.loadImage(display, "halt.png", 16, 16, (Color)null));
      this.mTBHalt.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            UIThread.this.mDevicePanel.killSelectedClient();
         }
      });
      toolBar.pack();
   }

   private void createTopPanel(final Composite comp, Color darkGray) {
      final PreferenceStore prefs = PrefsDialog.getStore();
      comp.setLayout(new FormLayout());
      Composite leftPanel = new Composite(comp, 0);
      final Sash sash = new Sash(comp, 512);
      sash.setBackground(darkGray);
      Composite rightPanel = new Composite(comp, 0);
      this.createLeftPanel(leftPanel);
      this.createRightPanel(rightPanel);
      FormData data = new FormData();
      data.top = new FormAttachment(0, 0);
      data.bottom = new FormAttachment(100, 0);
      data.left = new FormAttachment(0, 0);
      data.right = new FormAttachment(sash, 0);
      leftPanel.setLayoutData(data);
      final FormData sashData = new FormData();
      sashData.top = new FormAttachment(0, 0);
      sashData.bottom = new FormAttachment(100, 0);
      if(prefs != null && prefs.contains("sashLocation")) {
         sashData.left = new FormAttachment(0, prefs.getInt("sashLocation"));
      } else {
         sashData.left = new FormAttachment(0, 380);
      }

      sash.setLayoutData(sashData);
      data = new FormData();
      data.top = new FormAttachment(0, 0);
      data.bottom = new FormAttachment(100, 0);
      data.left = new FormAttachment(sash, 0);
      data.right = new FormAttachment(100, 0);
      rightPanel.setLayoutData(data);
      boolean minPanelWidth = true;
      sash.addListener(13, new Listener() {
         public void handleEvent(Event e) {
            Rectangle sashRect = sash.getBounds();
            Rectangle panelRect = comp.getClientArea();
            int right = panelRect.width - sashRect.width - 60;
            e.x = Math.max(Math.min(e.x, right), 60);
            if(e.x != sashRect.x) {
               sashData.left = new FormAttachment(0, e.x);
               if(prefs != null) {
                  prefs.setValue("sashLocation", e.x);
               }

               comp.layout();
            }

         }
      });
   }

   private void createBottomPanel(Composite comp) {
      PreferenceStore prefs = PrefsDialog.getStore();
      Display display = comp.getDisplay();
      LogColors colors = new LogColors();
      colors.infoColor = new Color(display, 0, 127, 0);
      colors.debugColor = new Color(display, 0, 0, 127);
      colors.errorColor = new Color(display, 255, 0, 0);
      colors.warningColor = new Color(display, 255, 127, 0);
      colors.verboseColor = new Color(display, 0, 0, 0);
      LogPanel.PREFS_TIME = "logcat.time";
      LogPanel.PREFS_LEVEL = "logcat.level";
      LogPanel.PREFS_PID = "logcat.pid";
      LogPanel.PREFS_TAG = "logcat.tag";
      LogPanel.PREFS_MESSAGE = "logcat.message";
      comp.setLayout(new GridLayout(1, false));
      ToolBar toolBar = new ToolBar(comp, 256);
      this.mCreateFilterAction = new ToolItemAction(toolBar, 8);
      this.mCreateFilterAction.item.setToolTipText("Create Filter");
      this.mCreateFilterAction.item.setImage(this.mDdmUiLibLoader.loadImage(this.mDisplay, "add.png", 16, 16, (Color)null));
      this.mCreateFilterAction.item.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            UIThread.this.mLogPanel.addFilter();
         }
      });
      this.mEditFilterAction = new ToolItemAction(toolBar, 8);
      this.mEditFilterAction.item.setToolTipText("Edit Filter");
      this.mEditFilterAction.item.setImage(this.mDdmUiLibLoader.loadImage(this.mDisplay, "edit.png", 16, 16, (Color)null));
      this.mEditFilterAction.item.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            UIThread.this.mLogPanel.editFilter();
         }
      });
      this.mDeleteFilterAction = new ToolItemAction(toolBar, 8);
      this.mDeleteFilterAction.item.setToolTipText("Delete Filter");
      this.mDeleteFilterAction.item.setImage(this.mDdmUiLibLoader.loadImage(this.mDisplay, "delete.png", 16, 16, (Color)null));
      this.mDeleteFilterAction.item.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            UIThread.this.mLogPanel.deleteFilter();
         }
      });
      new ToolItem(toolBar, 2);
      LogLevel[] levels = LogLevel.values();
      this.mLogLevelActions = new ToolItemAction[this.mLogLevelIcons.length];

      String fontStr;
      for(int colMode = 0; colMode < this.mLogLevelActions.length; ++colMode) {
         fontStr = levels[colMode].getStringValue();
         final ToolItemAction e2 = new ToolItemAction(toolBar, 32);
         this.mLogLevelActions[colMode] = e2;
         e2.item.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent e) {
               for(int k = 0; k < UIThread.this.mLogLevelActions.length; ++k) {
                  ToolItemAction a = UIThread.this.mLogLevelActions[k];
                  if(a == e2) {
                     a.setChecked(true);
                     UIThread.this.mLogPanel.setCurrentFilterLogLevel(k + 2);
                  } else {
                     a.setChecked(false);
                  }
               }

            }
         });
         e2.item.setToolTipText(fontStr);
         e2.item.setImage(this.mDdmUiLibLoader.loadImage(this.mDisplay, this.mLogLevelIcons[colMode], 16, 16, (Color)null));
      }

      new ToolItem(toolBar, 2);
      this.mClearAction = new ToolItemAction(toolBar, 8);
      this.mClearAction.item.setToolTipText("Clear Log");
      this.mClearAction.item.setImage(this.mDdmUiLibLoader.loadImage(this.mDisplay, "clear.png", 16, 16, (Color)null));
      this.mClearAction.item.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            UIThread.this.mLogPanel.clear();
         }
      });
      new ToolItem(toolBar, 2);
      this.mExportAction = new ToolItemAction(toolBar, 8);
      this.mExportAction.item.setToolTipText("Export Selection As Text...");
      this.mExportAction.item.setImage(this.mDdmUiLibLoader.loadImage(this.mDisplay, "save.png", 16, 16, (Color)null));
      this.mExportAction.item.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            UIThread.this.mLogPanel.save();
         }
      });
      toolBar.pack();
      this.mLogPanel = new LogPanel(colors, new UIThread.FilterStorage(null), 1);
      this.mLogPanel.setActions(this.mDeleteFilterAction, this.mEditFilterAction, this.mLogLevelActions);
      String var12 = prefs.getString("ddmsLogColumnMode");
      if("auto".equals(var12)) {
         this.mLogPanel.setColumnMode(1);
      }

      fontStr = PrefsDialog.getStore().getString("ddmsLogFont");
      if(fontStr != null) {
         try {
            FontData var13 = new FontData(fontStr);
            this.mLogPanel.setFont(new Font(display, var13));
         } catch (IllegalArgumentException var10) {
            ;
         } catch (SWTError var11) {
            ;
         }
      }

      this.mLogPanel.createPanel(comp);
      this.mLogPanel.startLogCat(this.mCurrentDevice);
   }

   private void createLogCatView(Composite parent) {
      IPreferenceStore prefStore = DdmUiPreferences.getStore();
      this.mLogCatPanel = new LogCatPanel(prefStore);
      this.mLogCatPanel.createPanel(parent);
      if(this.mCurrentDevice != null) {
         this.mLogCatPanel.deviceSelected(this.mCurrentDevice);
      }

   }

   /**
    * 左边的是devicePanel设备
    * @param comp
     */
   private void createLeftPanel(Composite comp) {
      comp.setLayout(new GridLayout(1, false));
      ToolBar toolBar = new ToolBar(comp, 131392);
      toolBar.setLayoutData(new GridData(768));
      this.createDevicePanelToolBar(toolBar);
      Composite c = new Composite(comp, 0);
      c.setLayoutData(new GridData(1808));
      this.mDevicePanel = new DevicePanel(true);
      this.mDevicePanel.createPanel(c);
      this.mDevicePanel.addSelectionListener(this);
   }

   /**
    * 右边的panel
    * @param comp
     */
   private void createRightPanel(Composite comp) {
      comp.setLayout(new FillLayout());
      TabFolder tabFolder = new TabFolder(comp, 0);

      TabItem item;
      for(int eventLogTopComposite = 0; eventLogTopComposite < mPanels.length; ++eventLogTopComposite) {
         if(mPanels[eventLogTopComposite] != null) {
            item = new TabItem(tabFolder, 0);
            item.setText(mPanelNames[eventLogTopComposite]);
            item.setToolTipText(mPanelTips[eventLogTopComposite]);
            item.setControl(mPanels[eventLogTopComposite].createPanel(tabFolder));
         }
      }
      //补充的部分---暂时去掉模拟器控制部分
      item = new TabItem(tabFolder, 0);
      item.setText("模拟器控制");
      item.setToolTipText("Emulator Control Panel");
      this.mEmulatorPanel = new EmulatorControlPanel();
      item.setControl(this.mEmulatorPanel.createPanel(tabFolder));
      item = new TabItem(tabFolder, 0);
      item.setText("事件日志");
      item.setToolTipText("Event Log");
      Composite var11 = new Composite(tabFolder, 0);
      item.setControl(var11);
      var11.setLayout(new GridLayout(1, false));
      ToolBar toolbar = new ToolBar(var11, 256);
      toolbar.setLayoutData(new GridData(768));
      ToolItemAction optionsAction = new ToolItemAction(toolbar, 8);
      optionsAction.item.setToolTipText("Opens the options panel");
      optionsAction.item.setImage(this.mDdmUiLibLoader.loadImage(comp.getDisplay(), "edit.png", 16, 16, (Color)null));
      ToolItemAction clearAction = new ToolItemAction(toolbar, 8);
      clearAction.item.setToolTipText("Clears the event log");
      clearAction.item.setImage(this.mDdmUiLibLoader.loadImage(comp.getDisplay(), "clear.png", 16, 16, (Color)null));
      new ToolItem(toolbar, 2);
      ToolItemAction saveAction = new ToolItemAction(toolbar, 8);
      saveAction.item.setToolTipText("Saves the event log");
      saveAction.item.setImage(this.mDdmUiLibLoader.loadImage(comp.getDisplay(), "save.png", 16, 16, (Color)null));
      ToolItemAction loadAction = new ToolItemAction(toolbar, 8);
      loadAction.item.setToolTipText("Loads an event log");
      loadAction.item.setImage(this.mDdmUiLibLoader.loadImage(comp.getDisplay(), "load.png", 16, 16, (Color)null));
      ToolItemAction importBugAction = new ToolItemAction(toolbar, 8);
      importBugAction.item.setToolTipText("Imports a bug report");
      importBugAction.item.setImage(this.mDdmUiLibLoader.loadImage(comp.getDisplay(), "importBug.png", 16, 16, (Color)null));
      this.mEventLogPanel = new EventLogPanel();
      this.mEventLogPanel.setActions(optionsAction, clearAction, saveAction, loadAction, importBugAction);
      this.mEventLogPanel.createPanel(var11);
   }

   /**
    * 执行脚本
    */
   private void evalScript(){
      FileDialog filedialog = new FileDialog(this.mDisplay.getActiveShell(), 4096);
      filedialog.setFilterExtensions(new String[]{"*.js"});
      filedialog.setFilterNames(new String[]{"js脚本"});
      String file = filedialog.open();
      if (file == null)
         return;
      String str = new String(readFile(file));
      try {

         Object result = eval(str, this);
         System.out.println(result);
      }catch(CompilerException ex){
         ex.printStackTrace();
      }catch(IOException ex){
         ex.printStackTrace();
      }

   }

   public static byte[] toByteArray(InputStream is) throws IOException {
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      byte[] buf = new byte[128];
      int count = 0;
      while ((count = is.read(buf, 0, buf.length)) != -1) {
         baos.write(buf, 0, count);
      }
      return baos.toByteArray();
   }

   public static byte[] readFile(String file) {
      FileInputStream fis = null;
      byte[] result = null;
      try {
         fis = new FileInputStream(file);
         result = toByteArray(fis);
         fis.close();
      }catch(IOException ex){}
      finally {
         if (fis != null){
            try {
               fis.close();
            }catch(IOException ex){}
         }
      }
      return result;
   }

   /**
    * 添加一个关联Mapping文件的功能
    */
   private void applyMapping(){
      //打开一个文件对话框，然后进行解析
      FileDialog filedialog = new FileDialog(this.mDisplay.getActiveShell(), 4096);
      filedialog.setFilterExtensions(new String[] {
              "*"
      });
      filedialog.setFilterNames(new String[] {
              "All files (*)"
      });
      String s = filedialog.open();
      if (s == null)
         return;
      // Create a remapper.
      FrameRemapper mapper = new FrameRemapper();

      // Read the mapping file.
      MappingReader mappingReader = new MappingReader(new File(s));
      try {
         mappingReader.pump(mapper);
      }catch (IOException ex){
         ex.printStackTrace();
      }
   }

   private void openTraceView(){
      FileDialog filedialog = new FileDialog(this.mDisplay.getActiveShell(), 4096);
      filedialog.setFilterExtensions(new String[] {
              "*"
      });
      filedialog.setFilterNames(new String[] {
              "All files (*)"
      });
      String s = filedialog.open();
      if (s == null)
         return;
      final String tempPath = s;
      //执行
      this.mDisplay.asyncExec(new Runnable() {
         public void run() {
            String traceName = tempPath;
            boolean regression = false;
            DmTraceReader reader = null;
            try {
               reader = new DmTraceReader(traceName, regression);
            }catch (IOException ex){
               ex.printStackTrace();
            }
            reader.getTraceUnits().setTimeScale(TraceUnits.TimeScale.MilliSeconds);
            MainWindow win = new MainWindow(traceName, reader);
            win.open();
         }
      }
      );
   }
   /**
    * 创建手机文件浏览器
    */
   private void createFileExplorer() {
      if(this.mExplorer == null) {
         this.mExplorerShell = new Shell(this.mDisplay);
         this.mExplorerShell.setLayout(new GridLayout(1, false));
         ToolBar toolBar = new ToolBar(this.mExplorerShell, 256);
         toolBar.setLayoutData(new GridData(768));
         ToolItemAction pullAction = new ToolItemAction(toolBar, 8);
         pullAction.item.setToolTipText("从设备拉取文件");
         Image image = this.mDdmUiLibLoader.loadImage("pull.png", this.mDisplay);
         if(image != null) {
            pullAction.item.setImage(image);
         } else {
            pullAction.item.setText("拉取");
         }

         ToolItemAction pushAction = new ToolItemAction(toolBar, 8);
         pushAction.item.setToolTipText("推送文件到设备");
         image = this.mDdmUiLibLoader.loadImage("push.png", this.mDisplay);
         if(image != null) {
            pushAction.item.setImage(image);
         } else {
            pushAction.item.setText("推送");
         }

         ToolItemAction deleteAction = new ToolItemAction(toolBar, 8);
         deleteAction.item.setToolTipText("删除");
         image = this.mDdmUiLibLoader.loadImage("delete.png", this.mDisplay);
         if(image != null) {
            deleteAction.item.setImage(image);
         } else {
            deleteAction.item.setText("删除");
         }

         ToolItemAction createNewFolderAction = new ToolItemAction(toolBar, 8);
         createNewFolderAction.item.setToolTipText("新建文件夹");
         image = this.mDdmUiLibLoader.loadImage("add.png", this.mDisplay);
         if(image != null) {
            createNewFolderAction.item.setImage(image);
         } else {
            createNewFolderAction.item.setText("文件文件夹");
         }

         this.mExplorer = new DeviceExplorer();
         this.mExplorer.setActions(pushAction, pullAction, deleteAction, createNewFolderAction);
         pullAction.item.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent e) {
               UIThread.this.mExplorer.pullSelection();
            }
         });
         pullAction.setEnabled(false);
         pushAction.item.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent e) {
               UIThread.this.mExplorer.pushIntoSelection();
            }
         });
         pushAction.setEnabled(false);
         deleteAction.item.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent e) {
               UIThread.this.mExplorer.deleteSelection();
            }
         });
         deleteAction.setEnabled(false);
         createNewFolderAction.item.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent e) {
               UIThread.this.mExplorer.createNewFolderInSelection();
            }
         });
         createNewFolderAction.setEnabled(false);
         Composite parent = new Composite(this.mExplorerShell, 0);
         parent.setLayoutData(new GridData(1808));
         this.mExplorer.createPanel(parent);
         this.mExplorer.switchDevice(this.mCurrentDevice);
         this.mExplorerShell.addShellListener(new ShellListener() {
            public void shellActivated(ShellEvent e) {
            }

            public void shellClosed(ShellEvent e) {
               UIThread.this.mExplorer = null;
               UIThread.this.mExplorerShell = null;
            }

            public void shellDeactivated(ShellEvent e) {
            }

            public void shellDeiconified(ShellEvent e) {
            }

            public void shellIconified(ShellEvent e) {
            }
         });
         this.mExplorerShell.pack();
         this.setExplorerSizeAndPosition(this.mExplorerShell);
         this.mExplorerShell.open();
      } else if(this.mExplorerShell != null) {
         this.mExplorerShell.forceActive();
      }

   }

   public void setStatusLine(final String str) {
      try {
         this.mDisplay.asyncExec(new Runnable() {
            public void run() {
               UIThread.this.doSetStatusLine(str);
            }
         });
      } catch (SWTException var3) {
         if(!this.mDisplay.isDisposed()) {
            throw var3;
         }
      }

   }

   private void doSetStatusLine(String str) {
      if(!this.mStatusLine.isDisposed()) {
         if(!this.mStatusLine.getText().equals(str)) {
            this.mStatusLine.setText(str);
         }

      }
   }

   public void displayError(final String msg) {
      try {
         this.mDisplay.syncExec(new Runnable() {
            public void run() {
               MessageDialog.openError(UIThread.this.mDisplay.getActiveShell(), "Error", msg);
            }
         });
      } catch (SWTException var3) {
         if(!this.mDisplay.isDisposed()) {
            throw var3;
         }
      }

   }

   private void enableButtons() {
      if(this.mCurrentClient != null) {
         this.mTBShowThreadUpdates.setSelection(this.mCurrentClient.isThreadUpdateEnabled());
         this.mTBShowThreadUpdates.setEnabled(true);
         this.mTBShowHeapUpdates.setSelection(this.mCurrentClient.isHeapUpdateEnabled());
         this.mTBShowHeapUpdates.setEnabled(true);
         this.mTBHalt.setEnabled(true);
         this.mTBCauseGc.setEnabled(true);
         ClientData data = this.mCurrentClient.getClientData();
         if(data.hasFeature("hprof-heap-dump")) {
            this.mTBDumpHprof.setEnabled(!data.hasPendingHprofDump());
            this.mTBDumpHprof.setToolTipText("Dump HPROF file");
         } else {
            this.mTBDumpHprof.setEnabled(false);
            this.mTBDumpHprof.setToolTipText("Dump HPROF file (not supported by this VM)");
         }

         if(data.hasFeature("method-trace-profiling")) {
            this.mTBProfiling.setEnabled(true);
            if(data.getMethodProfilingStatus() != MethodProfilingStatus.TRACER_ON && data.getMethodProfilingStatus() != MethodProfilingStatus.SAMPLER_ON) {
               this.mTBProfiling.setToolTipText("Start Method Profiling");
               this.mTBProfiling.setImage(this.mTracingStartImage);
            } else {
               this.mTBProfiling.setToolTipText("Stop Method Profiling");
               this.mTBProfiling.setImage(this.mTracingStopImage);
            }
         } else {
            this.mTBProfiling.setEnabled(false);
            this.mTBProfiling.setImage(this.mTracingStartImage);
            this.mTBProfiling.setToolTipText("Method Profiling (not supported by this VM)");
         }
      } else {
         this.mTBShowThreadUpdates.setSelection(false);
         this.mTBShowThreadUpdates.setEnabled(false);
         this.mTBShowHeapUpdates.setSelection(false);
         this.mTBShowHeapUpdates.setEnabled(false);
         this.mTBHalt.setEnabled(false);
         this.mTBCauseGc.setEnabled(false);
         this.mTBDumpHprof.setEnabled(false);
         this.mTBDumpHprof.setToolTipText("Dump HPROF file");
         this.mTBProfiling.setEnabled(false);
         this.mTBProfiling.setImage(this.mTracingStartImage);
         this.mTBProfiling.setToolTipText("Start Method Profiling");
      }

   }

   public void selectionChanged(IDevice selectedDevice, Client selectedClient) {
      TablePanel[] arr$;
      int len$;
      int i$;
      TablePanel panel;
      if(this.mCurrentDevice != selectedDevice) {
         this.mCurrentDevice = selectedDevice;
         arr$ = mPanels;
         len$ = arr$.length;

         for(i$ = 0; i$ < len$; ++i$) {
            panel = arr$[i$];
            if(panel != null) {
               panel.deviceSelected(this.mCurrentDevice);
            }
         }

         this.mEmulatorPanel.deviceSelected(this.mCurrentDevice);
         if(useOldLogCatView()) {
            this.mLogPanel.deviceSelected(this.mCurrentDevice);
         } else {
            this.mLogCatPanel.deviceSelected(this.mCurrentDevice);
         }

         if(this.mEventLogPanel != null) {
            this.mEventLogPanel.deviceSelected(this.mCurrentDevice);
         }

         if(this.mExplorer != null) {
            this.mExplorer.switchDevice(this.mCurrentDevice);
         }
      }

      if(this.mCurrentClient != selectedClient) {
         AndroidDebugBridge.getBridge().setSelectedClient(selectedClient);
         this.mCurrentClient = selectedClient;
         arr$ = mPanels;
         len$ = arr$.length;

         for(i$ = 0; i$ < len$; ++i$) {
            panel = arr$[i$];
            if(panel != null) {
               panel.clientSelected(this.mCurrentClient);
            }
         }

         this.enableButtons();
      }

   }

   public void clientChanged(Client client, int changeMask) {
      if((changeMask & 2048) == 2048 && this.mCurrentClient == client) {
         this.mDisplay.asyncExec(new Runnable() {
            public void run() {
               UIThread.this.enableButtons();
            }
         });
      }

   }

   private class HProfHandler extends BaseFileHandler implements IHprofDumpHandler {
      public HProfHandler(Shell parentShell) {
         super(parentShell);
      }

      public void onEndFailure(final Client client, final String message) {
         UIThread.this.mDisplay.asyncExec(new Runnable() {
            public void run() {
               try {
                  HProfHandler.this.displayErrorFromUiThread("Unable to create HPROF file for application \'%1$s\'\n\n%2$sCheck logcat for more information.", new Object[]{client.getClientData().getClientDescription(), message != null?message + "\n\n":""});
               } finally {
                  UIThread.this.enableButtons();
               }

            }
         });
      }

      public void onSuccess(final String remoteFilePath, final Client client) {
         UIThread.this.mDisplay.asyncExec(new Runnable() {
            public void run() {
               IDevice device = client.getDevice();

               try {
                  SyncService e = client.getDevice().getSyncService();
                  if(e != null) {
                     HProfHandler.this.promptAndPull(e, client.getClientData().getClientDescription() + ".hprof", remoteFilePath, "Save HPROF file");
                  } else {
                     HProfHandler.this.displayErrorFromUiThread("Unable to download HPROF file from device \'%1$s\'.", new Object[]{device.getSerialNumber()});
                  }
               } catch (SyncException var7) {
                  if(!var7.wasCanceled()) {
                     HProfHandler.this.displayErrorFromUiThread("Unable to download HPROF file from device \'%1$s\'.\n\n%2$s", new Object[]{device.getSerialNumber(), var7.getMessage()});
                  }
               } catch (Exception var8) {
                  HProfHandler.this.displayErrorFromUiThread("Unable to download HPROF file from device \'%1$s\'.", new Object[]{device.getSerialNumber()});
               } finally {
                  UIThread.this.enableButtons();
               }

            }
         });
      }

      public void onSuccess(final byte[] data, final Client client) {
         UIThread.this.mDisplay.asyncExec(new Runnable() {
            public void run() {
               HProfHandler.this.promptAndSave(client.getClientData().getClientDescription() + ".hprof", data, "Save HPROF file");
            }
         });
      }

      protected String getDialogTitle() {
         return "HPROF Error";
      }
   }

   private class TableFocusListener implements ITableFocusListener {
      private IFocusedTableActivator mCurrentActivator;

      private TableFocusListener() {
      }

      public void focusGained(IFocusedTableActivator activator) {
         this.mCurrentActivator = activator;
         if(!UIThread.this.mCopyMenuItem.isDisposed()) {
            UIThread.this.mCopyMenuItem.setEnabled(true);
            UIThread.this.mSelectAllMenuItem.setEnabled(true);
         }

      }

      public void focusLost(IFocusedTableActivator activator) {
         if(activator == this.mCurrentActivator) {
            activator = null;
            if(!UIThread.this.mCopyMenuItem.isDisposed()) {
               UIThread.this.mCopyMenuItem.setEnabled(false);
               UIThread.this.mSelectAllMenuItem.setEnabled(false);
            }
         }

      }

      public void copy(Clipboard clipboard) {
         if(this.mCurrentActivator != null) {
            this.mCurrentActivator.copy(clipboard);
         }

      }

      public void selectAll() {
         if(this.mCurrentActivator != null) {
            this.mCurrentActivator.selectAll();
         }

      }

      // $FF: synthetic method
      TableFocusListener(Object x1) {
         this();
      }
   }

   private final class FilterStorage implements ILogFilterStorageManager {
      private FilterStorage() {
      }

      public LogFilter[] getFilterFromStore() {
         String filterPrefs = PrefsDialog.getStore().getString("logcat.filter");
         String[] filters = filterPrefs.split("\\|");
         ArrayList list = new ArrayList(filters.length);
         String[] arr$ = filters;
         int len$ = filters.length;

         for(int i$ = 0; i$ < len$; ++i$) {
            String f = arr$[i$];
            if(f.length() > 0) {
               LogFilter logFilter = new LogFilter();
               if(logFilter.loadFromString(f)) {
                  list.add(logFilter);
               }
            }
         }

         return (LogFilter[])list.toArray(new LogFilter[list.size()]);
      }

      public void saveFilters(LogFilter[] filters) {
         StringBuilder sb = new StringBuilder();
         LogFilter[] arr$ = filters;
         int len$ = filters.length;

         for(int i$ = 0; i$ < len$; ++i$) {
            LogFilter f = arr$[i$];
            String filterString = f.toString();
            sb.append(filterString);
            sb.append('|');
         }

         PrefsDialog.getStore().setValue("logcat.filter", sb.toString());
      }

      public boolean requiresDefaultFilter() {
         return true;
      }

      // $FF: synthetic method
      FilterStorage(Object x1) {
         this();
      }
   }
}
