package com.android.ddms;

import com.android.ddmlib.AndroidDebugBridge;
import com.android.ddmlib.DebugPortManager;
import com.android.ddmlib.Log;
import com.android.ddms.DebugPortProvider;
import com.android.ddms.PrefsDialog;
import com.android.ddms.UIThread;
import com.android.sdkstats.SdkStatsService;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.Thread.UncaughtExceptionHandler;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.util.Properties;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

public class Main {
   public static String sRevision;

   public static void main(String[] args) {
      if(isMac()) {
         RuntimeMXBean shell = ManagementFactory.getRuntimeMXBean();
         System.setProperty("JAVA_STARTED_ON_FIRST_THREAD_" + shell.getName().split("@")[0], "1");
      }

      Thread.setDefaultUncaughtExceptionHandler(new Main.UncaughtHandler());
      PrefsDialog.init();
      Log.d("ddms", "Initializing");
      Display.setAppName("DDMS");
      Shell shell1 = new Shell(Display.getDefault());
      SdkStatsService stats = new SdkStatsService();
      stats.checkUserPermissionForPing(shell1);
      if(args.length >= 3 && args[0].equals("ping")) {
         stats.ping(args);
      } else {
         if(args.length > 0) {
            Log.e("ddms", "Unknown argument: " + args[0]);
            System.exit(1);
         }

         String ddmsParentLocation = System.getProperty("com.android.ddms.bindir");
         if(ddmsParentLocation == null) {
            ddmsParentLocation = System.getenv("com.android.ddms.bindir");
         }

         ping(stats, ddmsParentLocation);
         stats = null;
         DebugPortManager.setProvider(DebugPortProvider.getInstance());
         UIThread ui = UIThread.getInstance();

         try {
            ui.runUI(ddmsParentLocation);
         } finally {
            PrefsDialog.save();
            AndroidDebugBridge.terminate();
         }

         Log.d("ddms", "Bye");
         System.exit(0);
      }
   }

   static boolean isMac() {
      return System.getProperty("os.name").startsWith("Mac OS");
   }

   private static void ping(SdkStatsService stats, String ddmsParentLocation) {
      Properties p = new Properties();

      try {
         File e;
         if(ddmsParentLocation != null && ddmsParentLocation.length() > 0) {
            e = new File(ddmsParentLocation, "source.properties");
         } else {
            e = new File("source.properties");
         }

         FileInputStream fis = null;

         try {
            fis = new FileInputStream(e);
            p.load(fis);
         } finally {
            if(fis != null) {
               try {
                  fis.close();
               } catch (IOException var13) {
                  ;
               }
            }

         }

         sRevision = p.getProperty("Pkg.Revision");
         if(sRevision != null && sRevision.length() > 0) {
            stats.ping("ddms", sRevision);
         }
      } catch (FileNotFoundException var15) {
         ;
      } catch (IOException var16) {
         ;
      }

   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
   }

   private static class UncaughtHandler implements UncaughtExceptionHandler {
      private UncaughtHandler() {
      }

      public void uncaughtException(Thread t, Throwable e) {
         Log.e("ddms", "shutting down due to uncaught exception");
         Log.e("ddms", e);
         System.exit(1);
      }

      // $FF: synthetic method
      UncaughtHandler(Main.SyntheticClass_1 x0) {
         this();
      }
   }
}
