package com.android.ddmuilib;

import com.android.ddmuilib.IFindTarget;
import java.util.regex.Pattern;

public abstract class AbstractBufferFindTarget implements IFindTarget {
   private int mCurrentSearchIndex;
   private Pattern mLastSearchPattern;
   private String mLastSearchText;

   public boolean findAndSelect(String text, boolean isNewSearch, boolean searchForward) {
      boolean found = false;
      int maxIndex = this.getItemCount();
      synchronized(this) {
         if(isNewSearch) {
            this.mCurrentSearchIndex = this.getStartingIndex();
         } else {
            this.mCurrentSearchIndex = this.getNext(this.mCurrentSearchIndex, searchForward, maxIndex);
         }

         Pattern pattern;
         if(text.equals(this.mLastSearchText)) {
            pattern = this.mLastSearchPattern;
         } else {
            pattern = Pattern.compile(text, 2);
            this.mLastSearchPattern = pattern;
            this.mLastSearchText = text;
         }

         int index = this.mCurrentSearchIndex;

         do {
            String msgText = this.getItem(this.mCurrentSearchIndex);
            if(msgText != null && pattern.matcher(msgText).find()) {
               found = true;
               break;
            }

            this.mCurrentSearchIndex = this.getNext(this.mCurrentSearchIndex, searchForward, maxIndex);
         } while(index != this.mCurrentSearchIndex);
      }

      if(found) {
         this.selectAndReveal(this.mCurrentSearchIndex);
      }

      return found;
   }

   public void scrollBy(int delta) {
      synchronized(this) {
         if(this.mCurrentSearchIndex > 0) {
            this.mCurrentSearchIndex = Math.max(0, this.mCurrentSearchIndex - delta);
         }

      }
   }

   private int getNext(int index, boolean searchForward, int max) {
      index = searchForward?index + 1:index - 1;
      if(index == -1) {
         index = max - 1;
      }

      if(index == max) {
         index = 0;
      }

      return index;
   }

   public abstract int getItemCount();

   public abstract String getItem(int var1);

   public abstract void selectAndReveal(int var1);

   public abstract int getStartingIndex();
}
