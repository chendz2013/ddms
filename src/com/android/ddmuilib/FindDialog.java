package com.android.ddmuilib;

import com.android.ddmuilib.IFindTarget;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class FindDialog extends Dialog {
   private Label mStatusLabel;
   private Button mFindNext;
   private Button mFindPrevious;
   private final IFindTarget mTarget;
   private Text mSearchText;
   private String mPreviousSearchText;
   private final int mDefaultButtonId;
   public static final int FIND_NEXT_ID = 1024;
   public static final int FIND_PREVIOUS_ID = 1025;

   public FindDialog(Shell shell, IFindTarget target) {
      this(shell, target, 1025);
   }

   public FindDialog(Shell shell, IFindTarget target, int defaultButtonId) {
      super(shell);
      this.mTarget = target;
      this.mDefaultButtonId = defaultButtonId;
      this.setShellStyle(this.getShellStyle() & -65537 | 0);
      this.setBlockOnOpen(true);
   }

   protected Control createDialogArea(Composite parent) {
      Composite panel = new Composite(parent, 0);
      panel.setLayout(new GridLayout(2, false));
      panel.setLayoutData(new GridData(1808));
      Label lblMessage = new Label(panel, 0);
      lblMessage.setLayoutData(new GridData(131072, 16777216, false, false, 1, 1));
      lblMessage.setText("Find:");
      this.mSearchText = new Text(panel, 2048);
      this.mSearchText.setLayoutData(new GridData(4, 16777216, true, false, 1, 1));
      this.mSearchText.addModifyListener(new ModifyListener() {
         public void modifyText(ModifyEvent e) {
            boolean hasText = !FindDialog.this.mSearchText.getText().trim().isEmpty();
            FindDialog.this.mFindNext.setEnabled(hasText);
            FindDialog.this.mFindPrevious.setEnabled(hasText);
         }
      });
      this.mStatusLabel = new Label(panel, 0);
      this.mStatusLabel.setForeground(this.getShell().getDisplay().getSystemColor(4));
      GridData gd = new GridData();
      gd.horizontalSpan = 2;
      gd.grabExcessHorizontalSpace = true;
      this.mStatusLabel.setLayoutData(gd);
      return panel;
   }

   protected void createButtonsForButtonBar(Composite parent) {
      this.createButton(parent, 12, IDialogConstants.CLOSE_LABEL, false);
      this.mFindNext = this.createButton(parent, 1024, "Find Next", this.mDefaultButtonId == 1024);
      this.mFindPrevious = this.createButton(parent, 1025, "Find Previous", this.mDefaultButtonId != 1024);
      this.mFindNext.setEnabled(false);
      this.mFindPrevious.setEnabled(false);
   }

   protected void buttonPressed(int buttonId) {
      if(buttonId == 12) {
         this.close();
      } else {
         if((buttonId == 1025 || buttonId == 1024) && this.mTarget != null) {
            String searchText = this.mSearchText.getText();
            boolean newSearch = !searchText.equals(this.mPreviousSearchText);
            this.mPreviousSearchText = searchText;
            boolean searchForward = buttonId == 1024;
            boolean hasMatches = this.mTarget.findAndSelect(searchText, newSearch, searchForward);
            if(!hasMatches) {
               this.mStatusLabel.setText("String not found");
               this.mStatusLabel.pack();
            } else {
               this.mStatusLabel.setText("");
            }
         }

      }
   }
}
