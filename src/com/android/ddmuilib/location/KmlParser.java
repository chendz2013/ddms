package com.android.ddmuilib.location;

import com.android.ddmuilib.location.LocationPoint;
import com.android.ddmuilib.location.WayPoint;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

public class KmlParser {
   private static final String NS_KML_2 = "http://earth.google.com/kml/2.";
   private static final String NODE_PLACEMARK = "Placemark";
   private static final String NODE_NAME = "name";
   private static final String NODE_COORDINATES = "coordinates";
   private static final Pattern sLocationPattern = Pattern.compile("([^,]+),([^,]+)(?:,([^,]+))?");
   private static SAXParserFactory sParserFactory = SAXParserFactory.newInstance();
   private String mFileName;
   private KmlParser.KmlHandler mHandler;

   public KmlParser(String fileName) {
      this.mFileName = fileName;
   }

   public boolean parse() {
      try {
         try {
            SAXParser e = sParserFactory.newSAXParser();
            this.mHandler = new KmlParser.KmlHandler();
            e.parse(new InputSource(new FileReader(this.mFileName)), this.mHandler);
            boolean var2 = this.mHandler.getSuccess();
            return var2;
         } catch (ParserConfigurationException var8) {
            ;
         } catch (SAXException var9) {
            ;
         } catch (IOException var10) {
            ;
         }

         return false;
      } finally {
         ;
      }
   }

   public WayPoint[] getWayPoints() {
      return this.mHandler != null?this.mHandler.getWayPoints():null;
   }

   static {
      sParserFactory.setNamespaceAware(true);
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
   }

   private static class KmlHandler extends DefaultHandler {
      List<WayPoint> mWayPoints;
      WayPoint mCurrentWayPoint;
      final StringBuilder mStringAccumulator;
      boolean mSuccess;

      private KmlHandler() {
         this.mStringAccumulator = new StringBuilder();
         this.mSuccess = true;
      }

      public void startElement(String uri, String localName, String name, Attributes attributes) throws SAXException {
         try {
            if(uri.startsWith("http://earth.google.com/kml/2.") && "Placemark".equals(localName)) {
               if(this.mWayPoints == null) {
                  this.mWayPoints = new ArrayList();
               }

               this.mWayPoints.add(this.mCurrentWayPoint = new WayPoint());
            }
         } finally {
            this.mStringAccumulator.setLength(0);
         }

      }

      public void characters(char[] ch, int start, int length) throws SAXException {
         this.mStringAccumulator.append(ch, start, length);
      }

      public void endElement(String uri, String localName, String name) throws SAXException {
         if(uri.startsWith("http://earth.google.com/kml/2.")) {
            if("Placemark".equals(localName)) {
               this.mCurrentWayPoint = null;
            } else if("name".equals(localName)) {
               if(this.mCurrentWayPoint != null) {
                  this.mCurrentWayPoint.setName(this.mStringAccumulator.toString());
               }
            } else if("coordinates".equals(localName) && this.mCurrentWayPoint != null) {
               this.parseLocation(this.mCurrentWayPoint, this.mStringAccumulator.toString());
            }
         }

      }

      public void error(SAXParseException e) throws SAXException {
         this.mSuccess = false;
      }

      public void fatalError(SAXParseException e) throws SAXException {
         this.mSuccess = false;
      }

      private void parseLocation(LocationPoint locationNode, String location) {
         Matcher m = KmlParser.sLocationPattern.matcher(location);
         if(m.matches()) {
            try {
               double e = Double.parseDouble(m.group(1));
               double latitude = Double.parseDouble(m.group(2));
               locationNode.setLocation(e, latitude);
               if(m.groupCount() == 3) {
                  locationNode.setElevation(Double.parseDouble(m.group(3)));
               }
            } catch (NumberFormatException var8) {
               ;
            }
         }

      }

      WayPoint[] getWayPoints() {
         return this.mWayPoints != null?(WayPoint[])this.mWayPoints.toArray(new WayPoint[this.mWayPoints.size()]):null;
      }

      boolean getSuccess() {
         return this.mSuccess;
      }

      // $FF: synthetic method
      KmlHandler(KmlParser.SyntheticClass_1 x0) {
         this();
      }
   }
}
