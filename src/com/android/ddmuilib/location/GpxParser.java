package com.android.ddmuilib.location;

import com.android.ddmuilib.location.LocationPoint;
import com.android.ddmuilib.location.TrackPoint;
import com.android.ddmuilib.location.WayPoint;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

public class GpxParser {
   private static final String NS_GPX = "http://www.topografix.com/GPX/1/1";
   private static final String NODE_WAYPOINT = "wpt";
   private static final String NODE_TRACK = "trk";
   private static final String NODE_TRACK_SEGMENT = "trkseg";
   private static final String NODE_TRACK_POINT = "trkpt";
   private static final String NODE_NAME = "name";
   private static final String NODE_TIME = "time";
   private static final String NODE_ELEVATION = "ele";
   private static final String NODE_DESCRIPTION = "desc";
   private static final String ATTR_LONGITUDE = "lon";
   private static final String ATTR_LATITUDE = "lat";
   private static SAXParserFactory sParserFactory = SAXParserFactory.newInstance();
   private String mFileName;
   private GpxParser.GpxHandler mHandler;
   private static final Pattern ISO8601_TIME;

   public GpxParser(String fileName) {
      this.mFileName = fileName;
   }

   public boolean parse() {
      try {
         try {
            SAXParser e = sParserFactory.newSAXParser();
            this.mHandler = new GpxParser.GpxHandler();
            e.parse(new InputSource(new FileReader(this.mFileName)), this.mHandler);
            boolean var2 = this.mHandler.getSuccess();
            return var2;
         } catch (ParserConfigurationException var8) {
            ;
         } catch (SAXException var9) {
            ;
         } catch (IOException var10) {
            ;
         }

         return false;
      } finally {
         ;
      }
   }

   public WayPoint[] getWayPoints() {
      return this.mHandler != null?this.mHandler.getWayPoints():null;
   }

   public GpxParser.Track[] getTracks() {
      return this.mHandler != null?this.mHandler.getTracks():null;
   }

   static {
      sParserFactory.setNamespaceAware(true);
      ISO8601_TIME = Pattern.compile("(\\d{4})-(\\d\\d)-(\\d\\d)T(\\d\\d):(\\d\\d):(\\d\\d)(?:(\\.\\d+))?(Z)?");
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
   }

   public static final class Track {
      private String mName;
      private String mComment;
      private List<TrackPoint> mPoints = new ArrayList();

      void setName(String name) {
         this.mName = name;
      }

      public String getName() {
         return this.mName;
      }

      void setComment(String comment) {
         this.mComment = comment;
      }

      public String getComment() {
         return this.mComment;
      }

      void addPoint(TrackPoint trackPoint) {
         this.mPoints.add(trackPoint);
      }

      public TrackPoint[] getPoints() {
         return (TrackPoint[])this.mPoints.toArray(new TrackPoint[this.mPoints.size()]);
      }

      public long getFirstPointTime() {
         return this.mPoints.size() > 0?((TrackPoint)this.mPoints.get(0)).getTime():-1L;
      }

      public long getLastPointTime() {
         return this.mPoints.size() > 0?((TrackPoint)this.mPoints.get(this.mPoints.size() - 1)).getTime():-1L;
      }

      public int getPointCount() {
         return this.mPoints.size();
      }
   }

   private static class GpxHandler extends DefaultHandler {
      List<WayPoint> mWayPoints;
      List<GpxParser.Track> mTrackList;
      GpxParser.Track mCurrentTrack;
      TrackPoint mCurrentTrackPoint;
      WayPoint mCurrentWayPoint;
      final StringBuilder mStringAccumulator;
      boolean mSuccess;

      private GpxHandler() {
         this.mStringAccumulator = new StringBuilder();
         this.mSuccess = true;
      }

      public void startElement(String uri, String localName, String name, Attributes attributes) throws SAXException {
         try {
            if("http://www.topografix.com/GPX/1/1".equals(uri)) {
               if("wpt".equals(localName)) {
                  if(this.mWayPoints == null) {
                     this.mWayPoints = new ArrayList();
                  }

                  this.mWayPoints.add(this.mCurrentWayPoint = new WayPoint());
                  this.handleLocation(this.mCurrentWayPoint, attributes);
               } else if("trk".equals(localName)) {
                  if(this.mTrackList == null) {
                     this.mTrackList = new ArrayList();
                  }

                  this.mTrackList.add(this.mCurrentTrack = new GpxParser.Track());
               } else if(!"trkseg".equals(localName) && "trkpt".equals(localName) && this.mCurrentTrack != null) {
                  this.mCurrentTrack.addPoint(this.mCurrentTrackPoint = new TrackPoint());
                  this.handleLocation(this.mCurrentTrackPoint, attributes);
               }
            }
         } finally {
            this.mStringAccumulator.setLength(0);
         }

      }

      public void characters(char[] ch, int start, int length) throws SAXException {
         this.mStringAccumulator.append(ch, start, length);
      }

      public void endElement(String uri, String localName, String name) throws SAXException {
         if("http://www.topografix.com/GPX/1/1".equals(uri)) {
            if("wpt".equals(localName)) {
               this.mCurrentWayPoint = null;
            } else if("trk".equals(localName)) {
               this.mCurrentTrack = null;
            } else if("trkpt".equals(localName)) {
               this.mCurrentTrackPoint = null;
            } else if("name".equals(localName)) {
               if(this.mCurrentTrack != null) {
                  this.mCurrentTrack.setName(this.mStringAccumulator.toString());
               } else if(this.mCurrentWayPoint != null) {
                  this.mCurrentWayPoint.setName(this.mStringAccumulator.toString());
               }
            } else if("time".equals(localName)) {
               if(this.mCurrentTrackPoint != null) {
                  this.mCurrentTrackPoint.setTime(this.computeTime(this.mStringAccumulator.toString()));
               }
            } else if("ele".equals(localName)) {
               if(this.mCurrentTrackPoint != null) {
                  this.mCurrentTrackPoint.setElevation(Double.parseDouble(this.mStringAccumulator.toString()));
               } else if(this.mCurrentWayPoint != null) {
                  this.mCurrentWayPoint.setElevation(Double.parseDouble(this.mStringAccumulator.toString()));
               }
            } else if("desc".equals(localName) && this.mCurrentWayPoint != null) {
               this.mCurrentWayPoint.setDescription(this.mStringAccumulator.toString());
            }
         }

      }

      public void error(SAXParseException e) throws SAXException {
         this.mSuccess = false;
      }

      public void fatalError(SAXParseException e) throws SAXException {
         this.mSuccess = false;
      }

      private long computeTime(String timeString) {
         Matcher m = GpxParser.ISO8601_TIME.matcher(timeString);
         if(m.matches()) {
            try {
               int e = Integer.parseInt(m.group(1));
               int month = Integer.parseInt(m.group(2));
               int date = Integer.parseInt(m.group(3));
               int hourOfDay = Integer.parseInt(m.group(4));
               int minute = Integer.parseInt(m.group(5));
               int second = Integer.parseInt(m.group(6));
               int milliseconds = 0;
               String subSecondGroup = m.group(7);
               if(subSecondGroup != null) {
                  milliseconds = (int)(1000.0D * Double.parseDouble(subSecondGroup));
               }

               boolean utcTime = m.group(8) != null;
               Calendar c;
               if(utcTime) {
                  c = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
               } else {
                  c = Calendar.getInstance();
               }

               c.set(e, month, date, hourOfDay, minute, second);
               return c.getTimeInMillis() + (long)milliseconds;
            } catch (NumberFormatException var13) {
               ;
            }
         }

         return -1L;
      }

      private void handleLocation(LocationPoint locationNode, Attributes attributes) {
         try {
            double e = Double.parseDouble(attributes.getValue("lon"));
            double latitude = Double.parseDouble(attributes.getValue("lat"));
            locationNode.setLocation(e, latitude);
         } catch (NumberFormatException var7) {
            ;
         }

      }

      WayPoint[] getWayPoints() {
         return this.mWayPoints != null?(WayPoint[])this.mWayPoints.toArray(new WayPoint[this.mWayPoints.size()]):null;
      }

      GpxParser.Track[] getTracks() {
         return this.mTrackList != null?(GpxParser.Track[])this.mTrackList.toArray(new GpxParser.Track[this.mTrackList.size()]):null;
      }

      boolean getSuccess() {
         return this.mSuccess;
      }

      // $FF: synthetic method
      GpxHandler(GpxParser.SyntheticClass_1 x0) {
         this();
      }
   }
}
