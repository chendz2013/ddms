package com.android.ddmuilib.location;

import com.android.ddmuilib.location.LocationPoint;

public class TrackPoint extends LocationPoint {
   private long mTime;

   void setTime(long time) {
      this.mTime = time;
   }

   public long getTime() {
      return this.mTime;
   }
}
