package com.android.ddmuilib.location;

class LocationPoint {
   private double mLongitude;
   private double mLatitude;
   private boolean mHasElevation = false;
   private double mElevation;

   final void setLocation(double longitude, double latitude) {
      this.mLongitude = longitude;
      this.mLatitude = latitude;
   }

   public final double getLongitude() {
      return this.mLongitude;
   }

   public final double getLatitude() {
      return this.mLatitude;
   }

   final void setElevation(double elevation) {
      this.mElevation = elevation;
      this.mHasElevation = true;
   }

   public final boolean hasElevation() {
      return this.mHasElevation;
   }

   public final double getElevation() {
      return this.mElevation;
   }
}
