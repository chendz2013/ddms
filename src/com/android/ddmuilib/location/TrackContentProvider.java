package com.android.ddmuilib.location;

import com.android.ddmuilib.location.GpxParser;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;

public class TrackContentProvider implements IStructuredContentProvider {
   public Object[] getElements(Object inputElement) {
      return (Object[])(inputElement instanceof GpxParser.Track[]?(GpxParser.Track[])((GpxParser.Track[])inputElement):new Object[0]);
   }

   public void dispose() {
   }

   public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
   }
}
