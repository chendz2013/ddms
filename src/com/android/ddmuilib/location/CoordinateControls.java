package com.android.ddmuilib.location;

import java.text.DecimalFormat;
import java.text.ParseException;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

public final class CoordinateControls {
   private double mValue;
   private boolean mValueValidity = false;
   private Text mDecimalText;
   private Text mSexagesimalDegreeText;
   private Text mSexagesimalMinuteText;
   private Text mSexagesimalSecondText;
   private final DecimalFormat mDecimalFormat = new DecimalFormat();
   private int mManualTextChange = 0;
   private ModifyListener mSexagesimalListener = new ModifyListener() {
      public void modifyText(ModifyEvent event) {
         if(CoordinateControls.this.mManualTextChange <= 0) {
            try {
               CoordinateControls.this.mValue = CoordinateControls.this.getValueFromSexagesimalControls();
               CoordinateControls.this.setValueIntoDecimalControl(CoordinateControls.this.mValue);
               CoordinateControls.this.mValueValidity = true;
            } catch (ParseException var3) {
               CoordinateControls.this.mValueValidity = false;
               CoordinateControls.this.resetDecimalControls();
            }

         }
      }
   };

   public void createDecimalText(Composite parent) {
      this.mDecimalText = this.createTextControl(parent, "-199.999999", new ModifyListener() {
         public void modifyText(ModifyEvent event) {
            if(CoordinateControls.this.mManualTextChange <= 0) {
               try {
                  CoordinateControls.this.mValue = CoordinateControls.this.mDecimalFormat.parse(CoordinateControls.this.mDecimalText.getText()).doubleValue();
                  CoordinateControls.this.setValueIntoSexagesimalControl(CoordinateControls.this.mValue);
                  CoordinateControls.this.mValueValidity = true;
               } catch (ParseException var3) {
                  CoordinateControls.this.mValueValidity = false;
                  CoordinateControls.this.resetSexagesimalControls();
               }

            }
         }
      });
   }

   public void createSexagesimalDegreeText(Composite parent) {
      this.mSexagesimalDegreeText = this.createTextControl(parent, "-199", this.mSexagesimalListener);
   }

   public void createSexagesimalMinuteText(Composite parent) {
      this.mSexagesimalMinuteText = this.createTextControl(parent, "99", this.mSexagesimalListener);
   }

   public void createSexagesimalSecondText(Composite parent) {
      this.mSexagesimalSecondText = this.createTextControl(parent, "99.999", this.mSexagesimalListener);
   }

   public void setValue(double value) {
      this.mValue = value;
      this.mValueValidity = true;
      this.setValueIntoDecimalControl(value);
      this.setValueIntoSexagesimalControl(value);
   }

   public boolean isValueValid() {
      return this.mValueValidity;
   }

   public double getValue() {
      return this.mValue;
   }

   public void setEnabled(boolean enabled) {
      this.mDecimalText.setEnabled(enabled);
      this.mSexagesimalDegreeText.setEnabled(enabled);
      this.mSexagesimalMinuteText.setEnabled(enabled);
      this.mSexagesimalSecondText.setEnabled(enabled);
   }

   private void resetDecimalControls() {
      ++this.mManualTextChange;
      this.mDecimalText.setText("");
      --this.mManualTextChange;
   }

   private void resetSexagesimalControls() {
      ++this.mManualTextChange;
      this.mSexagesimalDegreeText.setText("");
      this.mSexagesimalMinuteText.setText("");
      this.mSexagesimalSecondText.setText("");
      --this.mManualTextChange;
   }

   private Text createTextControl(Composite parent, String defaultString, ModifyListener listener) {
      Text text = new Text(parent, 18436);
      text.addModifyListener(listener);
      ++this.mManualTextChange;
      text.setText(defaultString);
      text.pack();
      Point size = text.computeSize(-1, -1);
      text.setText("");
      --this.mManualTextChange;
      GridData gridData = new GridData();
      gridData.widthHint = size.x;
      text.setLayoutData(gridData);
      return text;
   }

   private double getValueFromSexagesimalControls() throws ParseException {
      double degrees = this.mDecimalFormat.parse(this.mSexagesimalDegreeText.getText()).doubleValue();
      double minutes = this.mDecimalFormat.parse(this.mSexagesimalMinuteText.getText()).doubleValue();
      double seconds = this.mDecimalFormat.parse(this.mSexagesimalSecondText.getText()).doubleValue();
      boolean isPositive = degrees >= 0.0D;
      degrees = Math.abs(degrees);
      double value = degrees + minutes / 60.0D + seconds / 3600.0D;
      return isPositive?value:-value;
   }

   private void setValueIntoDecimalControl(double value) {
      ++this.mManualTextChange;
      this.mDecimalText.setText(String.format("%.6f", new Object[]{Double.valueOf(value)}));
      --this.mManualTextChange;
   }

   private void setValueIntoSexagesimalControl(double value) {
      boolean isPositive = value >= 0.0D;
      value = Math.abs(value);
      double degrees = Math.floor(value);
      double minutes = Math.floor((value - degrees) * 60.0D);
      double seconds = (value - degrees) * 3600.0D - minutes * 60.0D;
      ++this.mManualTextChange;
      this.mSexagesimalDegreeText.setText(Integer.toString(isPositive?(int)degrees:(int)(-degrees)));
      this.mSexagesimalMinuteText.setText(Integer.toString((int)minutes));
      this.mSexagesimalSecondText.setText(String.format("%.3f", new Object[]{Double.valueOf(seconds)}));
      --this.mManualTextChange;
   }
}
