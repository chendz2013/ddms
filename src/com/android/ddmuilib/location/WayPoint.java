package com.android.ddmuilib.location;

import com.android.ddmuilib.location.LocationPoint;

public final class WayPoint extends LocationPoint {
   private String mName;
   private String mDescription;

   void setName(String name) {
      this.mName = name;
   }

   public String getName() {
      return this.mName;
   }

   void setDescription(String description) {
      this.mDescription = description;
   }

   public String getDescription() {
      return this.mDescription;
   }
}
