package com.android.ddmuilib.location;

import com.android.ddmuilib.location.WayPoint;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;

public class WayPointContentProvider implements IStructuredContentProvider {
   public Object[] getElements(Object inputElement) {
      return (Object[])(inputElement instanceof WayPoint[]?(WayPoint[])((WayPoint[])inputElement):new Object[0]);
   }

   public void dispose() {
   }

   public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
   }
}
