package com.android.ddmuilib;

import com.android.ddmlib.AdbCommandRejectedException;
import com.android.ddmlib.Client;
import com.android.ddmlib.ClientData;
import com.android.ddmlib.IDevice;
import com.android.ddmlib.IShellOutputReceiver;
import com.android.ddmlib.Log;
import com.android.ddmlib.NullOutputReceiver;
import com.android.ddmlib.ShellCommandUnresponsiveException;
import com.android.ddmlib.TimeoutException;
import com.android.ddmuilib.TablePanel;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.experimental.chart.swt.ChartComposite;

/**
 * 整个设备系统的信息
 */
public class SysinfoPanel extends TablePanel {
   private Label mLabel;
   private Button mFetchButton;
   private Combo mDisplayMode;
   private DefaultPieDataset mDataset;
   private DefaultCategoryDataset mBarDataSet;
   private StackLayout mStackLayout;
   private Composite mChartComposite;
   private Composite mPieChartComposite;
   private Composite mStackedBarComposite;
   private int mMode = 0;
   private String mGfxPackageName;
   private static final Object RECEIVER_LOCK = new Object();
   private SysinfoPanel.ShellOutputReceiver mLastOutputReceiver;
   private static final int MODE_CPU = 0;
   private static final int MODE_MEMINFO = 1;
   private static final int MODE_GFXINFO = 2;
   private static final String[] DUMP_COMMAND = new String[]{"dumpsys cpuinfo", "cat /proc/meminfo ; procrank", "dumpsys gfxinfo"};
   private static final String[] CAPTIONS = new String[]{"CPU负载", "内存", "帧渲染"};
   private static final String PROP_GFX_PROFILING = "debug.hwui.profile";

   private void generateDataset(File file) {
      if(file != null) {
         try {
            BufferedReader e = this.getBugreportReader(file);
            if(this.mMode == 0) {
               this.readCpuDataset(e);
            } else if(this.mMode == 1) {
               this.readMeminfoDataset(e);
            } else if(this.mMode == 2) {
               this.readGfxInfoDataset(e);
            }

            e.close();
         } catch (IOException var3) {
            Log.e("DDMS", var3);
         }

      }
   }

   public void deviceSelected() {
      if(this.getCurrentDevice() != null) {
         this.mFetchButton.setEnabled(true);
         this.loadFromDevice();
      } else {
         this.mFetchButton.setEnabled(false);
      }

   }

   public void clientSelected() {
   }

   public void setFocus() {
      this.mDisplayMode.setFocus();
   }

   private void loadFromDevice() {
      this.clearDataSet();
      if(this.mMode == 2) {
         boolean command = this.isGfxProfilingEnabled();
         if(!command) {
            if(!this.enableGfxProfiling()) {
               MessageDialog.openError(Display.getCurrent().getActiveShell(), "DDMS", "Unexpected error enabling graphics profiling on device.\n");
               return;
            }

            MessageDialog.openInformation(Display.getCurrent().getActiveShell(), "DDMS", "Graphics profiling was enabled on the device.\nIt may be necessary to relaunch your application to see profile information.");
         }
      }

      final String command1 = this.getDumpsysCommand(this.mMode);
      if(command1 != null) {
         Thread t = new Thread(new Runnable() {
            public void run() {
               try {
                  String e = null;
                  if(SysinfoPanel.this.mMode == 1) {
                     e = "------ MEMORY INFO ------\n";
                  }

                  IShellOutputReceiver receiver = SysinfoPanel.this.initShellOutputBuffer(e);
                  SysinfoPanel.this.getCurrentDevice().executeShellCommand(command1, receiver);
               } catch (IOException var3) {
                  Log.e("DDMS", var3);
               } catch (TimeoutException var4) {
                  Log.e("DDMS", var4);
               } catch (AdbCommandRejectedException var5) {
                  Log.e("DDMS", var5);
               } catch (ShellCommandUnresponsiveException var6) {
                  Log.e("DDMS", var6);
               }

            }
         }, "Sysinfo Output Collector");
         t.start();
      }
   }

   private boolean isGfxProfilingEnabled() {
      IDevice device = this.getCurrentDevice();
      if(device == null) {
         return false;
      } else {
         try {
            String prop = device.getPropertySync("debug.hwui.profile");
            return Boolean.valueOf(prop).booleanValue();
         } catch (Exception var4) {
            return false;
         }
      }
   }

   private boolean enableGfxProfiling() {
      IDevice device = this.getCurrentDevice();
      if(device == null) {
         return false;
      } else {
         try {
            device.executeShellCommand("setprop debug.hwui.profile true", new NullOutputReceiver());
            return true;
         } catch (Exception var3) {
            return false;
         }
      }
   }

   private String getDumpsysCommand(int mode) {
      if(mode == 2) {
         Client c = this.getCurrentClient();
         if(c == null) {
            return null;
         } else {
            ClientData cd = c.getClientData();
            if(cd == null) {
               return null;
            } else {
               this.mGfxPackageName = cd.getClientDescription();
               return this.mGfxPackageName == null?null:"dumpsys gfxinfo " + this.mGfxPackageName;
            }
         }
      } else {
         return mode < DUMP_COMMAND.length?DUMP_COMMAND[mode]:null;
      }
   }

   IShellOutputReceiver initShellOutputBuffer(String header) throws IOException {
      File f = File.createTempFile("ddmsfile", ".txt");
      f.deleteOnExit();
      Object var3 = RECEIVER_LOCK;
      synchronized(RECEIVER_LOCK) {
         if(this.mLastOutputReceiver != null) {
            this.mLastOutputReceiver.cancel();
         }

         this.mLastOutputReceiver = new SysinfoPanel.ShellOutputReceiver(f, header);
      }

      return this.mLastOutputReceiver;
   }

   protected Control createControl(Composite parent) {
      Composite top = new Composite(parent, 0);
      top.setLayout(new GridLayout(1, false));
      top.setLayoutData(new GridData(1808));
      Composite buttons = new Composite(top, 0);
      buttons.setLayout(new RowLayout());
      this.mDisplayMode = new Combo(buttons, 8);
      String[] arr$ = CAPTIONS;
      int len$ = arr$.length;

      for(int i$ = 0; i$ < len$; ++i$) {
         String mode = arr$[i$];
         this.mDisplayMode.add(mode);
      }

      this.mDisplayMode.select(this.mMode);
      this.mDisplayMode.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            SysinfoPanel.this.mMode = SysinfoPanel.this.mDisplayMode.getSelectionIndex();
            if(SysinfoPanel.this.getCurrentDevice() != null) {
               SysinfoPanel.this.loadFromDevice();
            }

         }
      });
      this.mFetchButton = new Button(buttons, 8);
      this.mFetchButton.setText("更新");
      this.mFetchButton.setEnabled(false);
      this.mFetchButton.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            SysinfoPanel.this.loadFromDevice();
         }
      });
      this.mLabel = new Label(top, 0);
      this.mLabel.setLayoutData(new GridData(768));
      this.mChartComposite = new Composite(top, 0);
      this.mChartComposite.setLayoutData(new GridData(1808));
      this.mStackLayout = new StackLayout();
      this.mChartComposite.setLayout(this.mStackLayout);
      this.mPieChartComposite = this.createPieChartComposite(this.mChartComposite);
      this.mStackedBarComposite = this.createStackedBarComposite(this.mChartComposite);
      this.mStackLayout.topControl = this.mPieChartComposite;
      return top;
   }

   private Composite createStackedBarComposite(Composite chartComposite) {
      this.mBarDataSet = new DefaultCategoryDataset();
      JFreeChart chart = ChartFactory.createStackedBarChart("每帧渲染时间", "帧#", "时间(ms)", this.mBarDataSet, PlotOrientation.VERTICAL, true, true, false);
      ChartComposite c = this.newChartComposite(chart, chartComposite);
      c.setLayoutData(new GridData(1808));
      return c;
   }

   private Composite createPieChartComposite(Composite chartComposite) {
      this.mDataset = new DefaultPieDataset();
      JFreeChart chart = ChartFactory.createPieChart("", this.mDataset, false, true, false);
      ChartComposite c = this.newChartComposite(chart, chartComposite);
      c.setLayoutData(new GridData(1808));
      return c;
   }

   private ChartComposite newChartComposite(JFreeChart chart, Composite parent) {
      return new ChartComposite(parent, 2048, chart, 420, 420, 300, 200, 3000, 3000, true, true, true, true, false, true);
   }

   public void clientChanged(Client client, int changeMask) {
   }

   private BufferedReader getBugreportReader(File file) throws IOException {
      return new BufferedReader(new FileReader(file));
   }

   private static long parseTimeMs(String s) {
      long total = 0L;
      Pattern p = Pattern.compile("([\\d\\.]+)\\s*([a-z]+)");
      Matcher m = p.matcher(s);

      while(m.find()) {
         String label = m.group(2);
         if("sec".equals(label)) {
            total += (long)(Double.parseDouble(m.group(1)) * 1000.0D);
         } else {
            long value = (long)Integer.parseInt(m.group(1));
            if("d".equals(label)) {
               total += value * 24L * 60L * 60L * 1000L;
            } else if("h".equals(label)) {
               total += value * 60L * 60L * 1000L;
            } else if("m".equals(label)) {
               total += value * 60L * 1000L;
            } else if("s".equals(label)) {
               total += value * 1000L;
            } else if("ms".equals(label)) {
               total += value;
            }
         }
      }

      return total;
   }

   private void readCpuDataset(BufferedReader br) throws IOException {
      this.updatePieDataSet(SysinfoPanel.BugReportParser.readCpuDataset(br), "");
   }

   private void readMeminfoDataset(BufferedReader br) throws IOException {
      this.updatePieDataSet(SysinfoPanel.BugReportParser.readMeminfoDataset(br), "PSS in kB");
   }

   private void readGfxInfoDataset(BufferedReader br) throws IOException {
      this.updateBarChartDataSet(SysinfoPanel.BugReportParser.parseGfxInfo(br), this.mGfxPackageName == null?"":this.mGfxPackageName);
   }

   private void clearDataSet() {
      this.mLabel.setText("");
      this.mDataset.clear();
      this.mBarDataSet.clear();
   }

   private void updatePieDataSet(final List<SysinfoPanel.BugReportParser.DataValue> data, final String label) {
      Display.getDefault().syncExec(new Runnable() {
         public void run() {
            SysinfoPanel.this.mLabel.setText(label);
            SysinfoPanel.this.mStackLayout.topControl = SysinfoPanel.this.mPieChartComposite;
            SysinfoPanel.this.mChartComposite.layout();
            Iterator i$ = data.iterator();

            while(i$.hasNext()) {
               SysinfoPanel.BugReportParser.DataValue d = (SysinfoPanel.BugReportParser.DataValue)i$.next();
               SysinfoPanel.this.mDataset.setValue(d.name, d.value);
            }

         }
      });
   }

   private void updateBarChartDataSet(final List<SysinfoPanel.BugReportParser.GfxProfileData> gfxProfileData, final String label) {
      Display.getDefault().syncExec(new Runnable() {
         public void run() {
            SysinfoPanel.this.mLabel.setText(label);
            SysinfoPanel.this.mStackLayout.topControl = SysinfoPanel.this.mStackedBarComposite;
            SysinfoPanel.this.mChartComposite.layout();

            for(int i = 0; i < gfxProfileData.size(); ++i) {
               SysinfoPanel.BugReportParser.GfxProfileData d = (SysinfoPanel.BugReportParser.GfxProfileData)gfxProfileData.get(i);
               String frameNumber = Integer.toString(i);
               SysinfoPanel.this.mBarDataSet.addValue(d.draw, "Draw", frameNumber);
               SysinfoPanel.this.mBarDataSet.addValue(d.process, "Process", frameNumber);
               SysinfoPanel.this.mBarDataSet.addValue(d.execute, "Execute", frameNumber);
            }

         }
      });
   }

   private class ShellOutputReceiver implements IShellOutputReceiver {
      private final OutputStream mStream;
      private final File mFile;
      private AtomicBoolean mCancelled = new AtomicBoolean();

      public ShellOutputReceiver(File f, String header) {
         this.mFile = f;

         try {
            this.mStream = new FileOutputStream(f);
         } catch (FileNotFoundException var5) {
            throw new IllegalArgumentException(var5);
         }

         if(header != null) {
            byte[] data = header.getBytes();
            this.addOutput(data, 0, data.length);
         }

      }

      public void addOutput(byte[] data, int offset, int length) {
         try {
            this.mStream.write(data, offset, length);
         } catch (IOException var5) {
            Log.e("DDMS", var5);
         }

      }

      public void flush() {
         try {
            this.mStream.close();
         } catch (IOException var2) {
            Log.e("DDMS", var2);
         }

         if(!this.isCancelled()) {
            SysinfoPanel.this.generateDataset(this.mFile);
         }

      }

      public boolean isCancelled() {
         return this.mCancelled.get();
      }

      public void cancel() {
         this.mCancelled.set(true);
      }

      public File getDataFile() {
         return this.mFile;
      }
   }

   public static final class BugReportParser {
      public static List<SysinfoPanel.BugReportParser.GfxProfileData> parseGfxInfo(BufferedReader br) throws IOException {
         Pattern headerPattern = Pattern.compile("\\s+Draw\\s+Process\\s+Execute");
         String line = null;

         while((line = br.readLine()) != null) {
            Matcher dataPattern = headerPattern.matcher(line);
            if(dataPattern.find()) {
               break;
            }
         }

         if(line == null) {
            return Collections.emptyList();
         } else {
            Pattern dataPattern1 = Pattern.compile("(\\d*\\.\\d+)\\s+(\\d*\\.\\d+)\\s+(\\d*\\.\\d+)");
            ArrayList data = new ArrayList(128);

            while((line = br.readLine()) != null) {
               Matcher m = dataPattern1.matcher(line);
               if(!m.find()) {
                  break;
               }

               double draw = safeParseDouble(m.group(1));
               double process = safeParseDouble(m.group(2));
               double execute = safeParseDouble(m.group(3));
               data.add(new SysinfoPanel.BugReportParser.GfxProfileData(draw, process, execute));
            }

            return data;
         }
      }

      public static List<SysinfoPanel.BugReportParser.DataValue> readWakelockDataset(BufferedReader br) throws IOException {
         ArrayList results = new ArrayList();
         Pattern lockPattern = Pattern.compile("Wake lock (\\S+): (.+) partial");
         Pattern totalPattern = Pattern.compile("Total: (.+) uptime");
         double total = 0.0D;
         boolean inCurrent = false;

         while(true) {
            String line = br.readLine();
            if(line == null || line.startsWith("DUMP OF SERVICE")) {
               if(total > 0.0D) {
                  results.add(new SysinfoPanel.BugReportParser.DataValue("Unlocked", total));
               }

               return results;
            }

            if(line.startsWith("Current Battery Usage Statistics")) {
               inCurrent = true;
            } else if(inCurrent) {
               Matcher m = lockPattern.matcher(line);
               if(m.find()) {
                  double value = (double)SysinfoPanel.parseTimeMs(m.group(2)) / 1000.0D;
                  results.add(new SysinfoPanel.BugReportParser.DataValue(m.group(1), value));
                  total -= value;
               } else {
                  m = totalPattern.matcher(line);
                  if(m.find()) {
                     total += (double)SysinfoPanel.parseTimeMs(m.group(1)) / 1000.0D;
                  }
               }
            }
         }
      }

      public static List<SysinfoPanel.BugReportParser.DataValue> readAlarmDataset(BufferedReader br) throws IOException {
         ArrayList results = new ArrayList();
         Pattern pattern = Pattern.compile("(\\d+) alarms: Intent .*\\.([^. ]+) flags");

         while(true) {
            String line = br.readLine();
            if(line == null || line.startsWith("DUMP OF SERVICE")) {
               return results;
            }

            Matcher m = pattern.matcher(line);
            if(m.find()) {
               long count = Long.parseLong(m.group(1));
               String name = m.group(2);
               results.add(new SysinfoPanel.BugReportParser.DataValue(name, (double)count));
            }
         }
      }

      public static List<SysinfoPanel.BugReportParser.DataValue> readCpuDataset(BufferedReader br) throws IOException {
         ArrayList results = new ArrayList();
         Pattern pattern1 = Pattern.compile("(\\S+): (\\S+)% = (.+)% user . (.+)% kernel");
         Pattern pattern2 = Pattern.compile("(\\S+)% (\\S+): (.+)% user . (.+)% kernel");

         while(true) {
            String line = br.readLine();
            if(line == null) {
               return results;
            }

            line = line.trim();
            if(!line.startsWith("Load:")) {
               String name = "";
               double user = 0.0D;
               double kernel = 0.0D;
               double both = 0.0D;
               boolean found = false;
               Matcher m = pattern1.matcher(line);
               if(m.find()) {
                  found = true;
                  name = m.group(1);
                  both = (double)safeParseLong(m.group(2));
                  user = (double)safeParseLong(m.group(3));
                  kernel = (double)safeParseLong(m.group(4));
               }

               m = pattern2.matcher(line);
               if(m.find()) {
                  found = true;
                  name = m.group(2);
                  both = safeParseDouble(m.group(1));
                  user = safeParseDouble(m.group(3));
                  kernel = safeParseDouble(m.group(4));
               }

               if(found) {
                  if("TOTAL".equals(name)) {
                     if(both < 100.0D) {
                        results.add(new SysinfoPanel.BugReportParser.DataValue("Idle", 100.0D - both));
                     }
                  } else {
                     if(user > 0.0D) {
                        results.add(new SysinfoPanel.BugReportParser.DataValue(name + " (user)", user));
                     }

                     if(kernel > 0.0D) {
                        results.add(new SysinfoPanel.BugReportParser.DataValue(name + " (kernel)", both - user));
                     }

                     if(user == 0.0D && kernel == 0.0D && both > 0.0D) {
                        results.add(new SysinfoPanel.BugReportParser.DataValue(name, both));
                     }
                  }
               }
            }
         }
      }

      private static long safeParseLong(String s) {
         try {
            return Long.parseLong(s);
         } catch (NumberFormatException var2) {
            return 0L;
         }
      }

      private static double safeParseDouble(String s) {
         try {
            return Double.parseDouble(s);
         } catch (NumberFormatException var2) {
            return 0.0D;
         }
      }

      public static List<SysinfoPanel.BugReportParser.DataValue> readMeminfoDataset(BufferedReader br) throws IOException {
         ArrayList results = new ArrayList();
         Pattern valuePattern = Pattern.compile("(\\d+) kB");
         long total = 0L;
         long other = 0L;
         String line = null;

         while((line = br.readLine()) != null) {
            if(!line.contains("----")) {
               Matcher procRankResults = valuePattern.matcher(line);
               if(!procRankResults.find()) {
                  break;
               }

               long i$ = Long.parseLong(procRankResults.group(1));
               if(line.startsWith("MemTotal")) {
                  total = i$;
               } else if(line.startsWith("MemFree")) {
                  results.add(new SysinfoPanel.BugReportParser.DataValue("Free", (double)i$));
                  total -= i$;
               } else if(line.startsWith("Slab")) {
                  results.add(new SysinfoPanel.BugReportParser.DataValue("Slab", (double)i$));
                  total -= i$;
               } else if(line.startsWith("PageTables")) {
                  results.add(new SysinfoPanel.BugReportParser.DataValue("PageTables", (double)i$));
                  total -= i$;
               } else if(line.startsWith("Buffers") && i$ > 0L) {
                  results.add(new SysinfoPanel.BugReportParser.DataValue("Buffers", (double)i$));
                  total -= i$;
               } else if(line.startsWith("Inactive")) {
                  results.add(new SysinfoPanel.BugReportParser.DataValue("Inactive", (double)i$));
                  total -= i$;
               } else if(line.startsWith("MemFree")) {
                  results.add(new SysinfoPanel.BugReportParser.DataValue("Free", (double)i$));
                  total -= i$;
               }
            }
         }

         List procRankResults1 = readProcRankDataset(br, line);

         SysinfoPanel.BugReportParser.DataValue procRank;
         for(Iterator i$1 = procRankResults1.iterator(); i$1.hasNext(); total = (long)((double)total - procRank.value)) {
            procRank = (SysinfoPanel.BugReportParser.DataValue)i$1.next();
            if(procRank.value > 2000.0D) {
               results.add(procRank);
            } else {
               other = (long)((double)other + procRank.value);
            }
         }

         if(other > 0L) {
            results.add(new SysinfoPanel.BugReportParser.DataValue("Other", (double)other));
         }

         if(total > 0L) {
            results.add(new SysinfoPanel.BugReportParser.DataValue("Unknown", (double)total));
         }

         return results;
      }

      static List<SysinfoPanel.BugReportParser.DataValue> readProcRankDataset(BufferedReader br, String header) throws IOException {
         ArrayList results = new ArrayList();
         if(header != null && header.contains("PID")) {
            Splitter PROCRANK_SPLITTER = Splitter.on(' ').omitEmptyStrings().trimResults();
            ArrayList fields = Lists.newArrayList(PROCRANK_SPLITTER.split(header));
            int pssIndex = fields.indexOf("Pss");
            int cmdIndex = fields.indexOf("cmdline");
            if(pssIndex != -1 && cmdIndex != -1) {
               String line;
               while((line = br.readLine()) != null) {
                  fields = Lists.newArrayList(PROCRANK_SPLITTER.split(line));
                  if(fields.size() < cmdIndex) {
                     break;
                  }

                  String cmdline = ((String)fields.get(cmdIndex)).replace("/system/bin/", "");
                  String pssInK = (String)fields.get(pssIndex);
                  if(pssInK.endsWith("K")) {
                     pssInK = pssInK.substring(0, pssInK.length() - 1);
                  }

                  long pss = safeParseLong(pssInK);
                  results.add(new SysinfoPanel.BugReportParser.DataValue(cmdline, (double)pss));
               }

               return results;
            } else {
               return results;
            }
         } else {
            return results;
         }
      }

      public static List<SysinfoPanel.BugReportParser.DataValue> readSyncDataset(BufferedReader br) throws IOException {
         ArrayList results = new ArrayList();

         while(true) {
            String line = br.readLine();
            if(line == null || line.startsWith("DUMP OF SERVICE")) {
               return results;
            }

            if(line.startsWith(" |") && line.length() > 70) {
               String authority = line.substring(3, 18).trim();
               String duration = line.substring(61, 70).trim();
               String[] durParts = duration.split(":");
               long dur;
               if(durParts.length == 2) {
                  dur = Long.parseLong(durParts[0]) * 60L + Long.parseLong(durParts[1]);
                  results.add(new SysinfoPanel.BugReportParser.DataValue(authority, (double)dur));
               } else if(duration.length() == 3) {
                  dur = Long.parseLong(durParts[0]) * 3600L + Long.parseLong(durParts[1]) * 60L + Long.parseLong(durParts[2]);
                  results.add(new SysinfoPanel.BugReportParser.DataValue(authority, (double)dur));
               }
            }
         }
      }

      public static final class GfxProfileData {
         final double draw;
         final double process;
         final double execute;

         public GfxProfileData(double draw, double process, double execute) {
            this.draw = draw;
            this.process = process;
            this.execute = execute;
         }
      }

      public static final class DataValue {
         final String name;
         final double value;

         public DataValue(String n, double v) {
            this.name = n;
            this.value = v;
         }
      }
   }
}
