package com.android.ddmuilib;

import com.android.ddmuilib.ClientDisplayPanel;
import com.android.ddmuilib.ITableFocusListener;
import java.util.Arrays;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;

public abstract class TablePanel extends ClientDisplayPanel {
   private ITableFocusListener mGlobalListener;

   public void setTableFocusListener(ITableFocusListener listener) {
      this.mGlobalListener = listener;
      this.setTableFocusListener();
   }

   protected void setTableFocusListener() {
   }

   protected final void addTableToFocusListener(final Table table, final int colStart, final int colEnd) {
      final ITableFocusListener.IFocusedTableActivator activator = new ITableFocusListener.IFocusedTableActivator() {
         public void copy(Clipboard clipboard) {
            int[] selection = table.getSelectionIndices();
            Arrays.sort(selection);
            StringBuilder sb = new StringBuilder();
            int[] data = selection;
            int len$ = selection.length;

            for(int i$ = 0; i$ < len$; ++i$) {
               int i = data[i$];
               TableItem item = table.getItem(i);

               for(int c = colStart; c <= colEnd; ++c) {
                  sb.append(item.getText(c));
                  sb.append('\t');
               }

               sb.append('\n');
            }

            String var10 = sb.toString();
            if(var10 != null && var10.length() > 0) {
               clipboard.setContents(new Object[]{var10}, new Transfer[]{TextTransfer.getInstance()});
            }

         }

         public void selectAll() {
            table.selectAll();
         }
      };
      table.addFocusListener(new FocusListener() {
         public void focusGained(FocusEvent e) {
            TablePanel.this.mGlobalListener.focusGained(activator);
         }

         public void focusLost(FocusEvent e) {
            TablePanel.this.mGlobalListener.focusLost(activator);
         }
      });
   }

   protected final void addTableToFocusListener(Table table) {
      this.addTableToFocusListener(table, 0, table.getColumnCount() - 1);
   }
}
