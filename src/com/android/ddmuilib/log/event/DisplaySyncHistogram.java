package com.android.ddmuilib.log.event;

import com.android.ddmlib.log.EventContainer;
import com.android.ddmlib.log.EventLogParser;
import com.android.ddmuilib.log.event.EventDisplay;
import com.android.ddmuilib.log.event.SyncCommon;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.SimpleTimePeriod;
import org.jfree.data.time.TimePeriodValues;
import org.jfree.data.time.TimePeriodValuesCollection;

public class DisplaySyncHistogram extends SyncCommon {
   Map<SimpleTimePeriod, Integer>[] mTimePeriodMap;
   private TimePeriodValues[] mDatasetsSyncHist;

   public DisplaySyncHistogram(String name) {
      super(name);
   }

   public Control createComposite(Composite parent, EventLogParser logParser, EventDisplay.ILogColumnListener listener) {
      Control composite = this.createCompositeChart(parent, logParser, "Sync Histogram");
      this.resetUI();
      return composite;
   }

   void resetUI() {
      super.resetUI();
      XYPlot xyPlot = this.mChart.getXYPlot();
      XYBarRenderer br = new XYBarRenderer();
      this.mDatasetsSyncHist = new TimePeriodValues[5];
      HashMap[] mTimePeriodMapTmp = new HashMap[5];
      this.mTimePeriodMap = mTimePeriodMapTmp;
      TimePeriodValuesCollection tpvc = new TimePeriodValuesCollection();
      xyPlot.setDataset(tpvc);
      xyPlot.setRenderer(br);

      for(int i = 0; i < 5; ++i) {
         br.setSeriesPaint(i, AUTH_COLORS[i]);
         this.mDatasetsSyncHist[i] = new TimePeriodValues(AUTH_NAMES[i]);
         tpvc.addSeries(this.mDatasetsSyncHist[i]);
         this.mTimePeriodMap[i] = new HashMap();
      }

   }

   void processSyncEvent(EventContainer event, int auth, long startTime, long stopTime, String details, boolean newEvent, int syncSource) {
      double delta;
      if(newEvent) {
         if(details.indexOf(120) >= 0 || details.indexOf(88) >= 0) {
            auth = 4;
         }

         delta = (double)(stopTime - startTime) * 100.0D / 1000.0D / 3600.0D;
         this.addHistEvent(0L, auth, delta);
      } else if(details.indexOf(120) >= 0 || details.indexOf(88) >= 0) {
         delta = (double)(stopTime - startTime) * 100.0D / 1000.0D / 3600.0D;
         this.addHistEvent(0L, auth, -delta);
         this.addHistEvent(0L, 4, delta);
      }

   }

   private void addHistEvent(long stopTime, int auth, double value) {
      SimpleTimePeriod hour = this.getTimePeriod(stopTime, this.mHistWidth);

      for(int i = auth; i <= 4; ++i) {
         this.addToPeriod(this.mDatasetsSyncHist, i, hour, value);
      }

   }

   private void addToPeriod(TimePeriodValues[] tpv, int auth, SimpleTimePeriod period, double value) {
      int index;
      if(this.mTimePeriodMap[auth].containsKey(period)) {
         index = ((Integer)this.mTimePeriodMap[auth].get(period)).intValue();
         double oldValue = tpv[auth].getValue(index).doubleValue();
         tpv[auth].update(index, Double.valueOf(oldValue + value));
      } else {
         index = tpv[auth].getItemCount();
         this.mTimePeriodMap[auth].put(period, Integer.valueOf(index));
         tpv[auth].add(period, value);
      }

   }

   private SimpleTimePeriod getTimePeriod(long time, long numHoursWide) {
      Date date = new Date(time);
      TimeZone zone = RegularTimePeriod.DEFAULT_TIME_ZONE;
      Calendar calendar = Calendar.getInstance(zone);
      calendar.setTime(date);
      long hoursOfYear = (long)(calendar.get(11) + calendar.get(6) * 24);
      int year = calendar.get(1);
      hoursOfYear = hoursOfYear / numHoursWide * numHoursWide;
      calendar.clear();
      calendar.set(year, 0, 1, 0, 0);
      long start = calendar.getTimeInMillis() + hoursOfYear * 3600L * 1000L;
      return new SimpleTimePeriod(start, start + numHoursWide * 3600L * 1000L);
   }

   int getDisplayType() {
      return 4;
   }
}
