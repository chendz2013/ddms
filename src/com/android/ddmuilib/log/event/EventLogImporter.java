package com.android.ddmuilib.log.event;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class EventLogImporter {
   private String[] mTags;
   private String[] mLog;

   public EventLogImporter(String filePath) throws FileNotFoundException {
      String top = System.getenv("ANDROID_BUILD_TOP");
      if(top == null) {
         throw new FileNotFoundException();
      } else {
         String tagFile = top + "/system/core/logcat/event-log-tags";
         BufferedReader tagReader = new BufferedReader(new InputStreamReader(new FileInputStream(tagFile)));
         BufferedReader eventReader = new BufferedReader(new InputStreamReader(new FileInputStream(filePath)));

         try {
            this.readTags(tagReader);
            this.readLog(eventReader);
         } catch (IOException var19) {
            ;
         } finally {
            if(tagReader != null) {
               try {
                  tagReader.close();
               } catch (IOException var18) {
                  ;
               }
            }

            if(eventReader != null) {
               try {
                  eventReader.close();
               } catch (IOException var17) {
                  ;
               }
            }

         }

      }
   }

   public String[] getTags() {
      return this.mTags;
   }

   public String[] getLog() {
      return this.mLog;
   }

   private void readTags(BufferedReader reader) throws IOException {
      ArrayList content = new ArrayList();

      String line;
      while((line = reader.readLine()) != null) {
         content.add(line);
      }

      this.mTags = (String[])content.toArray(new String[content.size()]);
   }

   private void readLog(BufferedReader reader) throws IOException {
      ArrayList content = new ArrayList();

      String line;
      while((line = reader.readLine()) != null) {
         content.add(line);
      }

      this.mLog = (String[])content.toArray(new String[content.size()]);
   }
}
