package com.android.ddmuilib.log.event;

import com.android.ddmlib.Client;
import com.android.ddmlib.IDevice;
import com.android.ddmlib.Log;
import com.android.ddmlib.Log.LogLevel;
import com.android.ddmlib.log.EventContainer;
import com.android.ddmlib.log.EventLogParser;
import com.android.ddmlib.log.LogReceiver;
import com.android.ddmlib.log.LogReceiver.ILogListener;
import com.android.ddmlib.log.LogReceiver.LogEntry;
import com.android.ddmuilib.DdmUiPreferences;
import com.android.ddmuilib.TablePanel;
import com.android.ddmuilib.actions.ICommonAction;
import com.android.ddmuilib.log.event.BugReportImporter;
import com.android.ddmuilib.log.event.EventDisplay;
import com.android.ddmuilib.log.event.EventDisplayOptions;
import com.android.ddmuilib.log.event.EventLogImporter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.regex.Pattern;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

public class EventLogPanel extends TablePanel implements ILogListener, EventDisplay.ILogColumnListener {
   private static final String TAG_FILE_EXT = ".tag";
   private static final String PREFS_EVENT_DISPLAY = "EventLogPanel.eventDisplay";
   private static final String EVENT_DISPLAY_STORAGE_SEPARATOR = "|";
   static final String PREFS_DISPLAY_WIDTH = "EventLogPanel.width";
   static final String PREFS_DISPLAY_HEIGHT = "EventLogPanel.height";
   private static final int DEFAULT_DISPLAY_WIDTH = 500;
   private static final int DEFAULT_DISPLAY_HEIGHT = 400;
   private IDevice mCurrentLoggedDevice;
   private String mCurrentLogFile;
   private LogReceiver mCurrentLogReceiver;
   private EventLogParser mCurrentEventLogParser;
   private Object mLock = new Object();
   private final ArrayList<EventContainer> mEvents = new ArrayList();
   private final ArrayList<EventContainer> mNewEvents = new ArrayList();
   private boolean mPendingDisplay = false;
   private final ArrayList<EventDisplay> mEventDisplays = new ArrayList();
   private final NumberFormat mFormatter = NumberFormat.getInstance();
   private Composite mParent;
   private ScrolledComposite mBottomParentPanel;
   private Composite mBottomPanel;
   private ICommonAction mOptionsAction;
   private ICommonAction mClearAction;
   private ICommonAction mSaveAction;
   private ICommonAction mLoadAction;
   private ICommonAction mImportAction;
   private File mTempFile = null;

   public EventLogPanel() {
      this.mFormatter.setGroupingUsed(true);
   }

   public void setActions(ICommonAction optionsAction, ICommonAction clearAction, ICommonAction saveAction, ICommonAction loadAction, ICommonAction importAction) {
      this.mOptionsAction = optionsAction;
      this.mOptionsAction.setRunnable(new Runnable() {
         public void run() {
            EventLogPanel.this.openOptionPanel();
         }
      });
      this.mClearAction = clearAction;
      this.mClearAction.setRunnable(new Runnable() {
         public void run() {
            EventLogPanel.this.clearLog();
         }
      });
      this.mSaveAction = saveAction;
      this.mSaveAction.setRunnable(new Runnable() {
         public void run() {
            try {
               FileDialog e1 = new FileDialog(EventLogPanel.this.mParent.getShell(), 8192);
               e1.setText("Save Event Log");
               e1.setFileName("event.log");
               String fileName = e1.open();
               if(fileName != null) {
                  EventLogPanel.this.saveLog(fileName);
               }
            } catch (IOException var3) {
               ;
            }

         }
      });
      this.mLoadAction = loadAction;
      this.mLoadAction.setRunnable(new Runnable() {
         public void run() {
            FileDialog fileDialog = new FileDialog(EventLogPanel.this.mParent.getShell(), 4096);
            fileDialog.setText("Load Event Log");
            String fileName = fileDialog.open();
            if(fileName != null) {
               EventLogPanel.this.loadLog(fileName);
            }

         }
      });
      this.mImportAction = importAction;
      this.mImportAction.setRunnable(new Runnable() {
         public void run() {
            FileDialog fileDialog = new FileDialog(EventLogPanel.this.mParent.getShell(), 4096);
            fileDialog.setText("Import Bug Report");
            String fileName = fileDialog.open();
            if(fileName != null) {
               EventLogPanel.this.importBugReport(fileName);
            }

         }
      });
      this.mOptionsAction.setEnabled(false);
      this.mClearAction.setEnabled(false);
      this.mSaveAction.setEnabled(false);
   }

   public void openOptionPanel() {
      try {
         EventDisplayOptions e = new EventDisplayOptions(this.mParent.getShell());
         if(e.open(this.mCurrentEventLogParser, this.mEventDisplays, this.mEvents)) {
            Object var2 = this.mLock;
            synchronized(this.mLock) {
               this.mEventDisplays.clear();
               this.mEventDisplays.addAll(e.getEventDisplays());
               this.saveEventDisplays();
               this.rebuildUi();
            }
         }
      } catch (SWTException var5) {
         Log.e("EventLog", var5);
      }

   }

   public void clearLog() {
      try {
         Object e = this.mLock;
         synchronized(this.mLock) {
            this.mEvents.clear();
            this.mNewEvents.clear();
            this.mPendingDisplay = false;
            Iterator i$ = this.mEventDisplays.iterator();

            while(i$.hasNext()) {
               EventDisplay eventDisplay = (EventDisplay)i$.next();
               eventDisplay.resetUI();
            }
         }
      } catch (SWTException var6) {
         Log.e("EventLog", var6);
      }

   }

   public void saveLog(String filePath) throws IOException {
      if(this.mCurrentLoggedDevice != null && this.mCurrentEventLogParser != null) {
         File destFile = new File(filePath);
         destFile.createNewFile();
         FileInputStream fis = new FileInputStream(this.mTempFile);
         FileOutputStream fos = new FileOutputStream(destFile);
         byte[] buffer = new byte[1024];

         int count;
         while((count = fis.read(buffer)) != -1) {
            fos.write(buffer, 0, count);
         }

         fos.close();
         fis.close();
         filePath = filePath + ".tag";
         this.mCurrentEventLogParser.saveTags(filePath);
      }

   }

   public void loadLog(String filePath) {
      if((new File(filePath + ".tag")).exists()) {
         this.startEventLogFromFiles(filePath);
      } else {
         try {
            EventLogImporter e = new EventLogImporter(filePath);
            String[] tags = e.getTags();
            String[] log = e.getLog();
            this.startEventLogFromContent(tags, log);
         } catch (FileNotFoundException var5) {
            Log.logAndDisplay(LogLevel.ERROR, "EventLog", String.format("Failure to read %1$s", new Object[]{filePath + ".tag"}));
         }
      }

   }

   public void importBugReport(String filePath) {
      try {
         BugReportImporter e = new BugReportImporter(filePath);
         String[] tags = e.getTags();
         String[] log = e.getLog();
         this.startEventLogFromContent(tags, log);
      } catch (FileNotFoundException var5) {
         Log.logAndDisplay(LogLevel.ERROR, "Import", "Unable to import bug report: " + var5.getMessage());
      }

   }

   public void clientSelected() {
   }

   public void deviceSelected() {
      this.startEventLog(this.getCurrentDevice());
   }

   public void clientChanged(Client client, int changeMask) {
   }

   protected Control createControl(Composite parent) {
      this.mParent = parent;
      this.mParent.addDisposeListener(new DisposeListener() {
         public void widgetDisposed(DisposeEvent e) {
            synchronized(EventLogPanel.this.mLock) {
               if(EventLogPanel.this.mCurrentLogReceiver != null) {
                  EventLogPanel.this.mCurrentLogReceiver.cancel();
                  EventLogPanel.this.mCurrentLogReceiver = null;
                  EventLogPanel.this.mCurrentEventLogParser = null;
                  EventLogPanel.this.mCurrentLoggedDevice = null;
                  EventLogPanel.this.mEventDisplays.clear();
                  EventLogPanel.this.mEvents.clear();
               }

            }
         }
      });
      IPreferenceStore store = DdmUiPreferences.getStore();
      store.setDefault("EventLogPanel.width", 500);
      store.setDefault("EventLogPanel.height", 400);
      this.mBottomParentPanel = new ScrolledComposite(parent, 512);
      this.mBottomParentPanel.setLayoutData(new GridData(1808));
      this.mBottomParentPanel.setExpandHorizontal(true);
      this.mBottomParentPanel.setExpandVertical(true);
      this.mBottomParentPanel.addControlListener(new ControlAdapter() {
         public void controlResized(ControlEvent e) {
            if(EventLogPanel.this.mBottomPanel != null) {
               Rectangle r = EventLogPanel.this.mBottomParentPanel.getClientArea();
               EventLogPanel.this.mBottomParentPanel.setMinSize(EventLogPanel.this.mBottomPanel.computeSize(r.width, -1));
            }

         }
      });
      this.prepareDisplayUi();
      this.loadEventDisplays();
      this.createDisplayUi();
      return this.mBottomParentPanel;
   }

   protected void postCreation() {
   }

   public void setFocus() {
      this.mBottomParentPanel.setFocus();
   }

   private void startEventLog(final IDevice device) {
      if(device != this.mCurrentLoggedDevice) {
         if(this.mCurrentLogReceiver != null) {
            this.stopEventLog(false);
         }

         this.mCurrentLoggedDevice = null;
         this.mCurrentLogFile = null;
         if(device != null) {
            this.mCurrentLogReceiver = new LogReceiver(this);
            (new Thread("EventLog") {
               public void run() {
                  while(!device.isOnline() && EventLogPanel.this.mCurrentLogReceiver != null && !EventLogPanel.this.mCurrentLogReceiver.isCancelled()) {
                     try {
                        sleep(2000L);
                     } catch (InterruptedException var8) {
                        return;
                     }
                  }

                  if(EventLogPanel.this.mCurrentLogReceiver != null && !EventLogPanel.this.mCurrentLogReceiver.isCancelled()) {
                     try {
                        try {
                           EventLogPanel.this.mCurrentLoggedDevice = device;
                           synchronized(EventLogPanel.this.mLock) {
                              EventLogPanel.this.mCurrentEventLogParser = new EventLogParser();
                              EventLogPanel.this.mCurrentEventLogParser.init(device);
                           }

                           EventLogPanel.this.updateEventDisplays();
                           EventLogPanel.this.mTempFile = File.createTempFile("android-event-", ".log");
                           device.runEventLogService(EventLogPanel.this.mCurrentLogReceiver);
                        } catch (Exception var10) {
                           Log.e("EventLog", var10);
                        }

                     } finally {
                        ;
                     }
                  }
               }
            }).start();
         }

      }
   }

   private void startEventLogFromFiles(final String fileName) {
      if(this.mCurrentLogReceiver != null) {
         this.stopEventLog(false);
      }

      this.mCurrentLoggedDevice = null;
      this.mCurrentLogFile = null;
      this.mCurrentLogReceiver = new LogReceiver(this);
      this.mSaveAction.setEnabled(false);
      (new Thread("EventLog") {
         public void run() {
            try {
               try {
                  EventLogPanel.this.mCurrentLogFile = fileName;
                  synchronized(EventLogPanel.this.mLock) {
                     EventLogPanel.this.mCurrentEventLogParser = new EventLogParser();
                     if(!EventLogPanel.this.mCurrentEventLogParser.init(fileName + ".tag")) {
                        EventLogPanel.this.mCurrentEventLogParser = null;
                        Log.logAndDisplay(LogLevel.ERROR, "EventLog", String.format("Failure to read %1$s", new Object[]{fileName + ".tag"}));
                        return;
                     }
                  }

                  EventLogPanel.this.updateEventDisplays();
                  EventLogPanel.this.runLocalEventLogService(fileName, EventLogPanel.this.mCurrentLogReceiver);
               } catch (Exception var8) {
                  Log.e("EventLog", var8);
               }

            } finally {
               ;
            }
         }
      }).start();
   }

   private void startEventLogFromContent(final String[] tags, final String[] log) {
      if(this.mCurrentLogReceiver != null) {
         this.stopEventLog(false);
      }

      this.mCurrentLoggedDevice = null;
      this.mCurrentLogFile = null;
      this.mCurrentLogReceiver = new LogReceiver(this);
      this.mSaveAction.setEnabled(false);
      (new Thread("EventLog") {
         public void run() {
            try {
               try {
                  synchronized(EventLogPanel.this.mLock) {
                     EventLogPanel.this.mCurrentEventLogParser = new EventLogParser();
                     if(!EventLogPanel.this.mCurrentEventLogParser.init(tags)) {
                        EventLogPanel.this.mCurrentEventLogParser = null;
                        return;
                     }
                  }

                  EventLogPanel.this.updateEventDisplays();
                  EventLogPanel.this.runLocalEventLogService(log, EventLogPanel.this.mCurrentLogReceiver);
               } catch (Exception var8) {
                  Log.e("EventLog", var8);
               }

            } finally {
               ;
            }
         }
      }).start();
   }

   public void stopEventLog(boolean inUiThread) {
      if(this.mCurrentLogReceiver != null) {
         this.mCurrentLogReceiver.cancel();
         Object var2 = this.mLock;
         synchronized(this.mLock) {
            this.mCurrentLogReceiver = null;
            this.mCurrentEventLogParser = null;
            this.mCurrentLoggedDevice = null;
            this.mEvents.clear();
            this.mNewEvents.clear();
            this.mPendingDisplay = false;
         }

         this.resetUI(inUiThread);
      }

      if(this.mTempFile != null) {
         this.mTempFile.delete();
         this.mTempFile = null;
      }

   }

   private void resetUI(boolean inUiThread) {
      this.mEvents.clear();
      if(inUiThread) {
         this.resetUiFromUiThread();
      } else {
         try {
            Display e = this.mBottomParentPanel.getDisplay();
            e.syncExec(new Runnable() {
               public void run() {
                  if(!EventLogPanel.this.mBottomParentPanel.isDisposed()) {
                     EventLogPanel.this.resetUiFromUiThread();
                  }

               }
            });
         } catch (SWTException var3) {
            ;
         }
      }

   }

   private void resetUiFromUiThread() {
      Object var1 = this.mLock;
      synchronized(this.mLock) {
         Iterator i$ = this.mEventDisplays.iterator();

         while(true) {
            if(!i$.hasNext()) {
               break;
            }

            EventDisplay eventDisplay = (EventDisplay)i$.next();
            eventDisplay.resetUI();
         }
      }

      this.mOptionsAction.setEnabled(false);
      this.mClearAction.setEnabled(false);
      this.mSaveAction.setEnabled(false);
   }

   private void prepareDisplayUi() {
      this.mBottomPanel = new Composite(this.mBottomParentPanel, 0);
      this.mBottomParentPanel.setContent(this.mBottomPanel);
   }

   private void createDisplayUi() {
      RowLayout rowLayout = new RowLayout();
      rowLayout.wrap = true;
      rowLayout.pack = false;
      rowLayout.justify = true;
      rowLayout.fill = true;
      rowLayout.type = 256;
      this.mBottomPanel.setLayout(rowLayout);
      IPreferenceStore store = DdmUiPreferences.getStore();
      int displayWidth = store.getInt("EventLogPanel.width");
      int displayHeight = store.getInt("EventLogPanel.height");
      Iterator i$ = this.mEventDisplays.iterator();

      while(i$.hasNext()) {
         EventDisplay eventDisplay = (EventDisplay)i$.next();
         Control c = eventDisplay.createComposite(this.mBottomPanel, this.mCurrentEventLogParser, this);
         if(c != null) {
            RowData table = new RowData();
            table.height = displayHeight;
            table.width = displayWidth;
            c.setLayoutData(table);
         }

         Table table1 = eventDisplay.getTable();
         if(table1 != null) {
            this.addTableToFocusListener(table1);
         }
      }

      this.mBottomPanel.layout();
      this.mBottomParentPanel.setMinSize(this.mBottomPanel.computeSize(-1, -1));
      this.mBottomParentPanel.layout();
   }

   private void rebuildUi() {
      Object var1 = this.mLock;
      synchronized(this.mLock) {
         this.mBottomPanel.dispose();
         this.mBottomPanel = null;
         this.prepareDisplayUi();
         this.createDisplayUi();
         boolean start_event = false;
         ArrayList r = this.mNewEvents;
         synchronized(this.mNewEvents) {
            this.mNewEvents.addAll(0, this.mEvents);
            if(!this.mPendingDisplay) {
               this.mPendingDisplay = true;
               start_event = true;
            }
         }

         if(start_event) {
            this.scheduleUIEventHandler();
         }

         Rectangle r1 = this.mBottomParentPanel.getClientArea();
         this.mBottomParentPanel.setMinSize(this.mBottomPanel.computeSize(r1.width, -1));
      }
   }

   public void newEntry(LogEntry entry) {
      Object var2 = this.mLock;
      synchronized(this.mLock) {
         if(this.mCurrentEventLogParser != null) {
            EventContainer event = this.mCurrentEventLogParser.parse(entry);
            if(event != null) {
               this.handleNewEvent(event);
            }
         }

      }
   }

   private void handleNewEvent(EventContainer event) {
      this.mEvents.add(event);
      boolean start_event = false;
      ArrayList var3 = this.mNewEvents;
      synchronized(this.mNewEvents) {
         this.mNewEvents.add(event);
         if(!this.mPendingDisplay) {
            this.mPendingDisplay = true;
            start_event = true;
         }
      }

      if(start_event) {
         this.scheduleUIEventHandler();
      }
   }

   private void scheduleUIEventHandler() {
      try {
         Display e = this.mBottomParentPanel.getDisplay();
         e.asyncExec(new Runnable() {
            public void run() {
               if(!EventLogPanel.this.mBottomParentPanel.isDisposed() && EventLogPanel.this.mCurrentEventLogParser != null) {
                  EventLogPanel.this.displayNewEvents();
               }

            }
         });
      } catch (SWTException var2) {
         ;
      }

   }

   public void newData(byte[] data, int offset, int length) {
      if(this.mTempFile != null) {
         try {
            FileOutputStream e = new FileOutputStream(this.mTempFile, true);
            e.write(data, offset, length);
            e.close();
         } catch (FileNotFoundException var5) {
            ;
         } catch (IOException var6) {
            ;
         }
      }

   }

   private void displayNewEvents() {
      int count = 0;
      Iterator event = this.mEventDisplays.iterator();

      while(event.hasNext()) {
         EventDisplay need_to_reloop = (EventDisplay)event.next();
         need_to_reloop.startMultiEventDisplay();
      }

      event = null;
      boolean var8 = false;

      EventDisplay eventDisplay;
      EventContainer var7;
      Iterator var9;
      do {
         ArrayList i$ = this.mNewEvents;
         synchronized(this.mNewEvents) {
            if(this.mNewEvents.size() > 0) {
               if(count > 200) {
                  var8 = true;
                  var7 = null;
               } else {
                  var7 = (EventContainer)this.mNewEvents.remove(0);
                  ++count;
               }
            } else {
               var7 = null;
               this.mPendingDisplay = false;
            }
         }

         if(var7 != null) {
            var9 = this.mEventDisplays.iterator();

            while(var9.hasNext()) {
               eventDisplay = (EventDisplay)var9.next();
               eventDisplay.newEvent(var7, this.mCurrentEventLogParser);
            }
         }
      } while(var7 != null);

      var9 = this.mEventDisplays.iterator();

      while(var9.hasNext()) {
         eventDisplay = (EventDisplay)var9.next();
         eventDisplay.endMultiEventDisplay();
      }

      if(var8) {
         this.scheduleUIEventHandler();
      }

   }

   private void loadEventDisplays() {
      IPreferenceStore store = DdmUiPreferences.getStore();
      String storage = store.getString("EventLogPanel.eventDisplay");
      if(storage.length() > 0) {
         String[] values = storage.split(Pattern.quote("|"));
         String[] arr$ = values;
         int len$ = values.length;

         for(int i$ = 0; i$ < len$; ++i$) {
            String value = arr$[i$];
            EventDisplay eventDisplay = EventDisplay.load(value);
            if(eventDisplay != null) {
               this.mEventDisplays.add(eventDisplay);
            }
         }
      }

   }

   private void saveEventDisplays() {
      IPreferenceStore store = DdmUiPreferences.getStore();
      boolean first = true;
      StringBuilder sb = new StringBuilder();
      Iterator i$ = this.mEventDisplays.iterator();

      while(i$.hasNext()) {
         EventDisplay eventDisplay = (EventDisplay)i$.next();
         String storage = eventDisplay.getStorageString();
         if(storage != null) {
            if(!first) {
               sb.append("|");
            } else {
               first = false;
            }

            sb.append(storage);
         }
      }

      store.setValue("EventLogPanel.eventDisplay", sb.toString());
   }

   private void updateEventDisplays() {
      try {
         Display e = this.mBottomParentPanel.getDisplay();
         e.asyncExec(new Runnable() {
            public void run() {
               if(!EventLogPanel.this.mBottomParentPanel.isDisposed()) {
                  Iterator i$ = EventLogPanel.this.mEventDisplays.iterator();

                  while(i$.hasNext()) {
                     EventDisplay eventDisplay = (EventDisplay)i$.next();
                     eventDisplay.setNewLogParser(EventLogPanel.this.mCurrentEventLogParser);
                  }

                  EventLogPanel.this.mOptionsAction.setEnabled(true);
                  EventLogPanel.this.mClearAction.setEnabled(true);
                  if(EventLogPanel.this.mCurrentLogFile == null) {
                     EventLogPanel.this.mSaveAction.setEnabled(true);
                  } else {
                     EventLogPanel.this.mSaveAction.setEnabled(false);
                  }
               }

            }
         });
      } catch (SWTException var2) {
         ;
      }

   }

   public void columnResized(int index, TableColumn sourceColumn) {
      Iterator i$ = this.mEventDisplays.iterator();

      while(i$.hasNext()) {
         EventDisplay eventDisplay = (EventDisplay)i$.next();
         eventDisplay.resizeColumn(index, sourceColumn);
      }

   }

   private void runLocalEventLogService(String fileName, LogReceiver logReceiver) throws IOException {
      byte[] buffer = new byte[256];
      FileInputStream fis = new FileInputStream(fileName);

      int count;
      try {
         while((count = fis.read(buffer)) != -1) {
            logReceiver.parseNewData(buffer, 0, count);
         }
      } finally {
         fis.close();
      }

   }

   private void runLocalEventLogService(String[] log, LogReceiver currentLogReceiver) {
      Object var3 = this.mLock;
      synchronized(this.mLock) {
         String[] arr$ = log;
         int len$ = log.length;

         for(int i$ = 0; i$ < len$; ++i$) {
            String line = arr$[i$];
            EventContainer event = this.mCurrentEventLogParser.parse(line);
            if(event != null) {
               this.handleNewEvent(event);
            }
         }

      }
   }
}
