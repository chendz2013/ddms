package com.android.ddmuilib.log.event;

import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Stroke;
import java.awt.geom.Rectangle2D;
import java.awt.geom.Line2D.Double;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CrosshairState;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.PlotRenderingInfo;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRendererState;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.RectangleEdge;

public class OccurrenceRenderer extends XYLineAndShapeRenderer {
   private static final long serialVersionUID = 1L;

   public void drawItem(Graphics2D g2, XYItemRendererState state, Rectangle2D dataArea, PlotRenderingInfo info, XYPlot plot, ValueAxis domainAxis, ValueAxis rangeAxis, XYDataset dataset, int series, int item, CrosshairState crosshairState, int pass) {
      TimeSeriesCollection timeDataSet = (TimeSeriesCollection)dataset;
      double x = timeDataSet.getX(series, item).doubleValue();
      double yMin = rangeAxis.getLowerBound();
      double yMax = rangeAxis.getUpperBound();
      RectangleEdge domainEdge = plot.getDomainAxisEdge();
      RectangleEdge rangeEdge = plot.getRangeAxisEdge();
      double x2D = domainAxis.valueToJava2D(x, dataArea, domainEdge);
      double yMin2D = rangeAxis.valueToJava2D(yMin, dataArea, rangeEdge);
      double yMax2D = rangeAxis.valueToJava2D(yMax, dataArea, rangeEdge);
      Paint p = this.getItemPaint(series, item);
      Stroke s = this.getItemStroke(series, item);
      Double line = null;
      PlotOrientation orientation = plot.getOrientation();
      if(orientation == PlotOrientation.HORIZONTAL) {
         line = new Double(yMin2D, x2D, yMax2D, x2D);
      } else if(orientation == PlotOrientation.VERTICAL) {
         line = new Double(x2D, yMin2D, x2D, yMax2D);
      }

      g2.setPaint(p);
      g2.setStroke(s);
      g2.draw(line);
   }
}
