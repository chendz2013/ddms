package com.android.ddmuilib.log.event;

import com.android.ddmlib.log.EventContainer;
import com.android.ddmlib.log.EventLogParser;
import com.android.ddmlib.log.InvalidTypeException;
import com.android.ddmuilib.log.event.EventDisplay;
import com.android.ddmuilib.log.event.SyncCommon;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.jfree.chart.labels.CustomXYToolTipGenerator;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.data.time.SimpleTimePeriod;
import org.jfree.data.time.TimePeriodValues;
import org.jfree.data.time.TimePeriodValuesCollection;

public class DisplaySyncPerf extends SyncCommon {
   CustomXYToolTipGenerator mTooltipGenerator;
   List<String>[] mTooltips;
   private static final int DB_QUERY = 4;
   private static final int DB_WRITE = 5;
   private static final int HTTP_NETWORK = 6;
   private static final int HTTP_PROCESSING = 7;
   private static final int NUM_SERIES = 8;
   private static final String[] SERIES_NAMES = new String[]{"Calendar", "Gmail", "Feeds", "Contacts", "DB Query", "DB Write", "HTTP Response", "HTTP Processing"};
   private static final Color[] SERIES_COLORS;
   private static final double[] SERIES_YCOORD;
   private static final int EVENT_DB_OPERATION = 52000;
   private static final int EVENT_HTTP_STATS = 52001;
   final int EVENT_DB_QUERY = 0;
   final int EVENT_DB_WRITE = 1;
   private TimePeriodValues[] mDatasets;

   public DisplaySyncPerf(String name) {
      super(name);
   }

   public Control createComposite(Composite parent, EventLogParser logParser, EventDisplay.ILogColumnListener listener) {
      Control composite = this.createCompositeChart(parent, logParser, "Sync Performance");
      this.resetUI();
      return composite;
   }

   void resetUI() {
      super.resetUI();
      XYPlot xyPlot = this.mChart.getXYPlot();
      xyPlot.getRangeAxis().setVisible(false);
      this.mTooltipGenerator = new CustomXYToolTipGenerator();
      List[] mTooltipsTmp = new List[8];
      this.mTooltips = mTooltipsTmp;
      XYBarRenderer br = new XYBarRenderer();
      br.setUseYInterval(true);
      this.mDatasets = new TimePeriodValues[8];
      DisplaySyncPerf.YIntervalTimePeriodValuesCollection tpvc = new DisplaySyncPerf.YIntervalTimePeriodValuesCollection(1.0D);
      xyPlot.setDataset(tpvc);
      xyPlot.setRenderer(br);

      for(int i = 0; i < 8; ++i) {
         br.setSeriesPaint(i, SERIES_COLORS[i]);
         this.mDatasets[i] = new TimePeriodValues(SERIES_NAMES[i]);
         tpvc.addSeries(this.mDatasets[i]);
         this.mTooltips[i] = new ArrayList();
         this.mTooltipGenerator.addToolTipSeries(this.mTooltips[i]);
         br.setSeriesToolTipGenerator(i, this.mTooltipGenerator);
      }

   }

   void newEvent(EventContainer event, EventLogParser logParser) {
      super.newEvent(event, logParser);

      try {
         String e;
         long endTime;
         if(event.mTag == '쬠') {
            e = event.getValueAsString(0);
            endTime = (long)event.sec * 1000L + (long)event.nsec / 1000000L;
            int netEndTime = Integer.parseInt(event.getValueAsString(1));
            long duration = Long.parseLong(event.getValueAsString(2));
            if(netEndTime == 0) {
               this.mDatasets[4].add(new SimpleTimePeriod(endTime - duration, endTime), SERIES_YCOORD[4]);
               this.mTooltips[4].add(e);
            } else if(netEndTime == 1) {
               this.mDatasets[5].add(new SimpleTimePeriod(endTime - duration, endTime), SERIES_YCOORD[5]);
               this.mTooltips[5].add(e);
            }
         } else if(event.mTag == '쬡') {
            e = event.getValueAsString(0) + ", tx:" + event.getValueAsString(3) + ", rx: " + event.getValueAsString(4);
            endTime = (long)event.sec * 1000L + (long)event.nsec / 1000000L;
            long netEndTime1 = endTime - Long.parseLong(event.getValueAsString(2));
            long netStartTime = netEndTime1 - Long.parseLong(event.getValueAsString(1));
            this.mDatasets[6].add(new SimpleTimePeriod(netStartTime, netEndTime1), SERIES_YCOORD[6]);
            this.mDatasets[7].add(new SimpleTimePeriod(netEndTime1, endTime), SERIES_YCOORD[7]);
            this.mTooltips[6].add(e);
            this.mTooltips[7].add(e);
         }
      } catch (NumberFormatException var10) {
         ;
      } catch (InvalidTypeException var11) {
         ;
      }

   }

   void processSyncEvent(EventContainer event, int auth, long startTime, long stopTime, String details, boolean newEvent, int syncSource) {
      if(newEvent) {
         this.mDatasets[auth].add(new SimpleTimePeriod(startTime, stopTime), SERIES_YCOORD[auth]);
      }

   }

   int getDisplayType() {
      return 5;
   }

   static {
      SERIES_COLORS = new Color[]{Color.MAGENTA, Color.GREEN, Color.BLUE, Color.ORANGE, Color.RED, Color.CYAN, Color.PINK, Color.DARK_GRAY};
      SERIES_YCOORD = new double[]{0.0D, 0.0D, 0.0D, 0.0D, 1.0D, 1.0D, 2.0D, 2.0D};
   }

   class YIntervalTimePeriodValuesCollection extends TimePeriodValuesCollection {
      private static final long serialVersionUID = 1L;
      private double yheight;

      YIntervalTimePeriodValuesCollection(double yheight) {
         this.yheight = yheight;
      }

      public Number getEndY(int series, int item) {
         return Double.valueOf(this.getY(series, item).doubleValue() + this.yheight);
      }
   }
}
