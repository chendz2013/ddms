package com.android.ddmuilib.log.event;

import com.android.ddmlib.Log;
import com.android.ddmlib.log.EventContainer;
import com.android.ddmlib.log.EventLogParser;
import com.android.ddmlib.log.InvalidTypeException;
import com.android.ddmlib.log.EventContainer.CompareMethod;
import com.android.ddmlib.log.EventContainer.EventValueType;
import com.android.ddmlib.log.EventValueDescription.ValueType;
import com.android.ddmuilib.log.event.DisplayFilteredLog;
import com.android.ddmuilib.log.event.DisplayGraph;
import com.android.ddmuilib.log.event.DisplayLog;
import com.android.ddmuilib.log.event.DisplaySync;
import com.android.ddmuilib.log.event.DisplaySyncHistogram;
import com.android.ddmuilib.log.event.DisplaySyncPerf;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.regex.Pattern;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.event.ChartChangeEvent;
import org.jfree.chart.event.ChartChangeEventType;
import org.jfree.chart.event.ChartChangeListener;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.time.Millisecond;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;
import org.jfree.experimental.chart.swt.ChartComposite;
import org.jfree.experimental.swt.SWTUtils;

abstract class EventDisplay {
   private static final String DISPLAY_DATA_STORAGE_SEPARATOR = ":";
   private static final String PID_STORAGE_SEPARATOR = ",";
   private static final String DESCRIPTOR_STORAGE_SEPARATOR = "$";
   private static final String DESCRIPTOR_DATA_STORAGE_SEPARATOR = "!";
   private static final String FILTER_VALUE_NULL = "<null>";
   public static final int DISPLAY_TYPE_LOG_ALL = 0;
   public static final int DISPLAY_TYPE_FILTERED_LOG = 1;
   public static final int DISPLAY_TYPE_GRAPH = 2;
   public static final int DISPLAY_TYPE_SYNC = 3;
   public static final int DISPLAY_TYPE_SYNC_HIST = 4;
   public static final int DISPLAY_TYPE_SYNC_PERF = 5;
   private static final int EVENT_CHECK_FAILED = 0;
   protected static final int EVENT_CHECK_SAME_TAG = 1;
   protected static final int EVENT_CHECK_SAME_VALUE = 2;
   protected String mName;
   private boolean mPidFiltering = false;
   private ArrayList<Integer> mPidFilterList = null;
   protected final ArrayList<EventDisplay.ValueDisplayDescriptor> mValueDescriptors = new ArrayList();
   private final ArrayList<EventDisplay.OccurrenceDisplayDescriptor> mOccurrenceDescriptors = new ArrayList();
   protected final HashMap<EventDisplay.ValueDisplayDescriptor, HashMap<Integer, TimeSeries>> mValueDescriptorSeriesMap = new HashMap();
   protected final HashMap<EventDisplay.OccurrenceDisplayDescriptor, HashMap<Integer, TimeSeries>> mOcurrenceDescriptorSeriesMap = new HashMap();
   protected final HashMap<ValueType, TimeSeriesCollection> mValueTypeDataSetMap = new HashMap();
   protected JFreeChart mChart;
   protected TimeSeriesCollection mOccurrenceDataSet;
   protected int mDataSetCount;
   private ChartComposite mChartComposite;
   protected long mMaximumChartItemAge = -1L;
   protected long mHistWidth = 1L;
   protected Table mLogTable;
   protected int mValueDescriptorCheck = 0;

   public static EventDisplay eventDisplayFactory(int type, String name) {
      switch(type) {
      case 0:
         return new DisplayLog(name);
      case 1:
         return new DisplayFilteredLog(name);
      case 2:
         return new DisplayGraph(name);
      case 3:
         return new DisplaySync(name);
      case 4:
         return new DisplaySyncHistogram(name);
      case 5:
         return new DisplaySyncPerf(name);
      default:
         throw new InvalidParameterException("Unknown Display Type " + type);
      }
   }

   abstract void newEvent(EventContainer var1, EventLogParser var2);

   abstract void resetUI();

   abstract int getDisplayType();

   abstract Control createComposite(Composite var1, EventLogParser var2, EventDisplay.ILogColumnListener var3);

   EventDisplay(String name) {
      this.mName = name;
   }

   static EventDisplay clone(EventDisplay from) {
      EventDisplay ed = eventDisplayFactory(from.getDisplayType(), from.getName());
      ed.mName = from.mName;
      ed.mPidFiltering = from.mPidFiltering;
      ed.mMaximumChartItemAge = from.mMaximumChartItemAge;
      ed.mHistWidth = from.mHistWidth;
      if(from.mPidFilterList != null) {
         ed.mPidFilterList = new ArrayList();
         ed.mPidFilterList.addAll(from.mPidFilterList);
      }

      Iterator i$ = from.mValueDescriptors.iterator();

      while(i$.hasNext()) {
         EventDisplay.ValueDisplayDescriptor desc = (EventDisplay.ValueDisplayDescriptor)i$.next();
         ed.mValueDescriptors.add(new EventDisplay.ValueDisplayDescriptor(desc));
      }

      ed.mValueDescriptorCheck = from.mValueDescriptorCheck;
      i$ = from.mOccurrenceDescriptors.iterator();

      while(i$.hasNext()) {
         EventDisplay.OccurrenceDisplayDescriptor desc1 = (EventDisplay.OccurrenceDisplayDescriptor)i$.next();
         ed.mOccurrenceDescriptors.add(new EventDisplay.OccurrenceDisplayDescriptor(desc1));
      }

      return ed;
   }

   String getStorageString() {
      StringBuilder sb = new StringBuilder();
      sb.append(this.mName);
      sb.append(":");
      sb.append(this.getDisplayType());
      sb.append(":");
      sb.append(Boolean.toString(this.mPidFiltering));
      sb.append(":");
      sb.append(this.getPidStorageString());
      sb.append(":");
      sb.append(this.getDescriptorStorageString(this.mValueDescriptors));
      sb.append(":");
      sb.append(this.getDescriptorStorageString(this.mOccurrenceDescriptors));
      sb.append(":");
      sb.append(this.mMaximumChartItemAge);
      sb.append(":");
      sb.append(this.mHistWidth);
      sb.append(":");
      return sb.toString();
   }

   void setName(String name) {
      this.mName = name;
   }

   String getName() {
      return this.mName;
   }

   void setPidFiltering(boolean filterByPid) {
      this.mPidFiltering = filterByPid;
   }

   boolean getPidFiltering() {
      return this.mPidFiltering;
   }

   void setPidFilterList(ArrayList<Integer> pids) {
      if(!this.mPidFiltering) {
         throw new InvalidParameterException();
      } else {
         this.mPidFilterList = pids;
      }
   }

   ArrayList<Integer> getPidFilterList() {
      return this.mPidFilterList;
   }

   void addPidFiler(int pid) {
      if(!this.mPidFiltering) {
         throw new InvalidParameterException();
      } else {
         if(this.mPidFilterList == null) {
            this.mPidFilterList = new ArrayList();
         }

         this.mPidFilterList.add(Integer.valueOf(pid));
      }
   }

   Iterator<EventDisplay.ValueDisplayDescriptor> getValueDescriptors() {
      return this.mValueDescriptors.iterator();
   }

   void updateValueDescriptorCheck() {
      this.mValueDescriptorCheck = this.checkDescriptors();
   }

   Iterator<EventDisplay.OccurrenceDisplayDescriptor> getOccurrenceDescriptors() {
      return this.mOccurrenceDescriptors.iterator();
   }

   void addDescriptor(EventDisplay.OccurrenceDisplayDescriptor descriptor) {
      if(descriptor instanceof EventDisplay.ValueDisplayDescriptor) {
         this.mValueDescriptors.add((EventDisplay.ValueDisplayDescriptor)descriptor);
         this.mValueDescriptorCheck = this.checkDescriptors();
      } else {
         this.mOccurrenceDescriptors.add(descriptor);
      }

   }

   EventDisplay.OccurrenceDisplayDescriptor getDescriptor(Class<? extends EventDisplay.OccurrenceDisplayDescriptor> descriptorClass, int index) {
      return descriptorClass == EventDisplay.OccurrenceDisplayDescriptor.class?(EventDisplay.OccurrenceDisplayDescriptor)this.mOccurrenceDescriptors.get(index):(descriptorClass == EventDisplay.ValueDisplayDescriptor.class?(EventDisplay.OccurrenceDisplayDescriptor)this.mValueDescriptors.get(index):null);
   }

   void removeDescriptor(Class<? extends EventDisplay.OccurrenceDisplayDescriptor> descriptorClass, int index) {
      if(descriptorClass == EventDisplay.OccurrenceDisplayDescriptor.class) {
         this.mOccurrenceDescriptors.remove(index);
      } else if(descriptorClass == EventDisplay.ValueDisplayDescriptor.class) {
         this.mValueDescriptors.remove(index);
         this.mValueDescriptorCheck = this.checkDescriptors();
      }

   }

   Control createCompositeChart(final Composite parent, EventLogParser logParser, String title) {
      this.mChart = ChartFactory.createTimeSeriesChart((String)null, (String)null, (String)null, (XYDataset)null, true, false, false);
      Font f = parent.getFont();
      FontData[] fData = f.getFontData();
      FontData firstFontData = fData[0];
      java.awt.Font awtFont = SWTUtils.toAwtFont(parent.getDisplay(), firstFontData, true);
      this.mChart.setTitle(new TextTitle(title, awtFont));
      final XYPlot xyPlot = this.mChart.getXYPlot();
      xyPlot.setRangeCrosshairVisible(true);
      xyPlot.setRangeCrosshairLockedOnData(true);
      xyPlot.setDomainCrosshairVisible(true);
      xyPlot.setDomainCrosshairLockedOnData(true);
      this.mChart.addChangeListener(new ChartChangeListener() {
         public void chartChanged(ChartChangeEvent event) {
            ChartChangeEventType type = event.getType();
            if(type == ChartChangeEventType.GENERAL) {
               parent.getDisplay().asyncExec(new Runnable() {
                  public void run() {
                     EventDisplay.this.processClick(xyPlot);
                  }
               });
            }

         }
      });
      this.mChartComposite = new ChartComposite(parent, 2048, this.mChart, 680, 420, 300, 200, 3000, 3000, true, true, true, true, true, true);
      this.mChartComposite.addDisposeListener(new DisposeListener() {
         public void widgetDisposed(DisposeEvent e) {
            EventDisplay.this.mValueTypeDataSetMap.clear();
            EventDisplay.this.mDataSetCount = 0;
            EventDisplay.this.mOccurrenceDataSet = null;
            EventDisplay.this.mChart = null;
            EventDisplay.this.mChartComposite = null;
            EventDisplay.this.mValueDescriptorSeriesMap.clear();
            EventDisplay.this.mOcurrenceDescriptorSeriesMap.clear();
         }
      });
      return this.mChartComposite;
   }

   private void processClick(XYPlot xyPlot) {
      double rangeValue = xyPlot.getRangeCrosshairValue();
      if(rangeValue != 0.0D) {
         double domainValue = xyPlot.getDomainCrosshairValue();
         Millisecond msec = new Millisecond(new Date((long)domainValue));
         Set descKeys = this.mValueDescriptorSeriesMap.keySet();
         Iterator i$ = descKeys.iterator();

         while(i$.hasNext()) {
            EventDisplay.ValueDisplayDescriptor descKey = (EventDisplay.ValueDisplayDescriptor)i$.next();
            HashMap map = (HashMap)this.mValueDescriptorSeriesMap.get(descKey);
            Set pidKeys = map.keySet();
            Iterator i$1 = pidKeys.iterator();

            while(i$1.hasNext()) {
               Integer pidKey = (Integer)i$1.next();
               TimeSeries series = (TimeSeries)map.get(pidKey);
               Number value = series.getValue(msec);
               if(value != null && value.doubleValue() == rangeValue) {
                  return;
               }
            }
         }
      }

   }

   void resizeColumn(int index, TableColumn sourceColumn) {
   }

   protected void setNewLogParser(EventLogParser logParser) {
   }

   void startMultiEventDisplay() {
      if(this.mLogTable != null) {
         this.mLogTable.setRedraw(false);
      }

   }

   void endMultiEventDisplay() {
      if(this.mLogTable != null) {
         this.mLogTable.setRedraw(true);
      }

   }

   Table getTable() {
      return this.mLogTable;
   }

   static EventDisplay load(String storageString) {
      if(storageString.length() > 0) {
         String[] values = storageString.split(Pattern.quote(":"));

         try {
            byte re = 0;
            int var8 = re + 1;
            String name = values[re];
            int displayType = Integer.parseInt(values[var8++]);
            boolean pidFiltering = Boolean.parseBoolean(values[var8++]);
            EventDisplay ed = eventDisplayFactory(displayType, name);
            ed.setPidFiltering(pidFiltering);
            if(var8 < values.length) {
               ed.loadPidFilters(values[var8++]);
            }

            if(var8 < values.length) {
               ed.loadValueDescriptors(values[var8++]);
            }

            if(var8 < values.length) {
               ed.loadOccurrenceDescriptors(values[var8++]);
            }

            ed.updateValueDescriptorCheck();
            if(var8 < values.length) {
               ed.mMaximumChartItemAge = Long.parseLong(values[var8++]);
            }

            if(var8 < values.length) {
               ed.mHistWidth = Long.parseLong(values[var8++]);
            }

            return ed;
         } catch (RuntimeException var7) {
            Log.e("ddms", var7);
         }
      }

      return null;
   }

   private String getPidStorageString() {
      if(this.mPidFilterList != null) {
         StringBuilder sb = new StringBuilder();
         boolean first = true;

         Integer i;
         for(Iterator i$ = this.mPidFilterList.iterator(); i$.hasNext(); sb.append(i)) {
            i = (Integer)i$.next();
            if(!first) {
               sb.append(",");
            } else {
               first = false;
            }
         }

         return sb.toString();
      } else {
         return "";
      }
   }

   private void loadPidFilters(String storageString) {
      if(storageString.length() > 0) {
         String[] values = storageString.split(Pattern.quote(","));
         String[] arr$ = values;
         int len$ = values.length;

         for(int i$ = 0; i$ < len$; ++i$) {
            String value = arr$[i$];
            if(this.mPidFilterList == null) {
               this.mPidFilterList = new ArrayList();
            }

            this.mPidFilterList.add(Integer.valueOf(Integer.parseInt(value)));
         }
      }

   }

   private String getDescriptorStorageString(ArrayList<? extends EventDisplay.OccurrenceDisplayDescriptor> descriptorList) {
      StringBuilder sb = new StringBuilder();
      boolean first = true;

      EventDisplay.OccurrenceDisplayDescriptor descriptor;
      for(Iterator i$ = descriptorList.iterator(); i$.hasNext(); sb.append(descriptor.getStorageString())) {
         descriptor = (EventDisplay.OccurrenceDisplayDescriptor)i$.next();
         if(!first) {
            sb.append("$");
         } else {
            first = false;
         }
      }

      return sb.toString();
   }

   private void loadOccurrenceDescriptors(String storageString) {
      if(storageString.length() != 0) {
         String[] values = storageString.split(Pattern.quote("$"));
         String[] arr$ = values;
         int len$ = values.length;

         for(int i$ = 0; i$ < len$; ++i$) {
            String value = arr$[i$];
            EventDisplay.OccurrenceDisplayDescriptor desc = new EventDisplay.OccurrenceDisplayDescriptor();
            desc.loadFrom(value);
            this.mOccurrenceDescriptors.add(desc);
         }

      }
   }

   private void loadValueDescriptors(String storageString) {
      if(storageString.length() != 0) {
         String[] values = storageString.split(Pattern.quote("$"));
         String[] arr$ = values;
         int len$ = values.length;

         for(int i$ = 0; i$ < len$; ++i$) {
            String value = arr$[i$];
            EventDisplay.ValueDisplayDescriptor desc = new EventDisplay.ValueDisplayDescriptor();
            desc.loadFrom(value);
            this.mValueDescriptors.add(desc);
         }

      }
   }

   private void getDescriptors(EventContainer event, ArrayList<? extends EventDisplay.OccurrenceDisplayDescriptor> fullList, ArrayList outList) {
      Iterator i$ = fullList.iterator();

      while(i$.hasNext()) {
         EventDisplay.OccurrenceDisplayDescriptor descriptor = (EventDisplay.OccurrenceDisplayDescriptor)i$.next();

         try {
            if(descriptor.eventTag == event.mTag && (descriptor.filterValueIndex == -1 || event.testValue(descriptor.filterValueIndex, descriptor.filterValue, descriptor.filterCompareMethod))) {
               outList.add(descriptor);
            }
         } catch (InvalidTypeException var7) {
            ;
         } catch (ArrayIndexOutOfBoundsException var8) {
            Log.e("Event Log", String.format("ArrayIndexOutOfBoundsException occured when checking %1$d-th value of event %2$d", new Object[]{Integer.valueOf(descriptor.filterValueIndex), Integer.valueOf(descriptor.eventTag)}));
         }
      }

   }

   protected boolean filterEvent(EventContainer event, ArrayList<EventDisplay.ValueDisplayDescriptor> valueDescriptors, ArrayList<EventDisplay.OccurrenceDisplayDescriptor> occurrenceDescriptors) {
      if(this.mPidFiltering && this.mPidFilterList != null) {
         boolean found = false;
         Iterator i$ = this.mPidFilterList.iterator();

         while(i$.hasNext()) {
            int pid = ((Integer)i$.next()).intValue();
            if(pid == event.pid) {
               found = true;
               break;
            }
         }

         if(!found) {
            return false;
         }
      }

      this.getDescriptors(event, this.mValueDescriptors, valueDescriptors);
      this.getDescriptors(event, this.mOccurrenceDescriptors, occurrenceDescriptors);
      return valueDescriptors.size() > 0 || occurrenceDescriptors.size() > 0;
   }

   private int checkDescriptors() {
      if(this.mValueDescriptors.size() < 2) {
         return 2;
      } else {
         int tag = -1;
         int index = -1;
         Iterator i$ = this.mValueDescriptors.iterator();

         while(i$.hasNext()) {
            EventDisplay.ValueDisplayDescriptor display = (EventDisplay.ValueDisplayDescriptor)i$.next();
            if(tag == -1) {
               tag = display.eventTag;
               index = display.valueIndex;
            } else {
               if(tag != display.eventTag) {
                  return 0;
               }

               if(index != -1 && index != display.valueIndex) {
                  index = -1;
               }
            }
         }

         if(index == -1) {
            return 1;
         } else {
            return 2;
         }
      }
   }

   void resetChartTimeLimit() {
      this.mMaximumChartItemAge = -1L;
   }

   void setChartTimeLimit(long timeLimit) {
      this.mMaximumChartItemAge = timeLimit;
   }

   long getChartTimeLimit() {
      return this.mMaximumChartItemAge;
   }

   void resetHistWidth() {
      this.mHistWidth = 1L;
   }

   void setHistWidth(long histWidth) {
      this.mHistWidth = histWidth;
   }

   long getHistWidth() {
      return this.mHistWidth;
   }

   static final class ValueDisplayDescriptor extends EventDisplay.OccurrenceDisplayDescriptor {
      String valueName;
      int valueIndex = -1;

      ValueDisplayDescriptor() {
      }

      ValueDisplayDescriptor(EventDisplay.ValueDisplayDescriptor descriptor) {
         this.replaceWith(descriptor);
      }

      ValueDisplayDescriptor(int eventTag, String valueName, int valueIndex) {
         super(eventTag);
         this.valueName = valueName;
         this.valueIndex = valueIndex;
      }

      ValueDisplayDescriptor(int eventTag, String valueName, int valueIndex, int seriesValueIndex) {
         super(eventTag, seriesValueIndex);
         this.valueName = valueName;
         this.valueIndex = valueIndex;
      }

      void replaceWith(EventDisplay.OccurrenceDisplayDescriptor descriptor) {
         super.replaceWith(descriptor);
         if(descriptor instanceof EventDisplay.ValueDisplayDescriptor) {
            EventDisplay.ValueDisplayDescriptor valueDescriptor = (EventDisplay.ValueDisplayDescriptor)descriptor;
            this.valueName = valueDescriptor.valueName;
            this.valueIndex = valueDescriptor.valueIndex;
         }

      }

      protected int loadFrom(String[] storageStrings, int index) {
         index = super.loadFrom(storageStrings, index);
         this.valueName = storageStrings[index++];
         this.valueIndex = Integer.parseInt(storageStrings[index++]);
         return index;
      }

      String getStorageString() {
         String superStorage = super.getStorageString();
         StringBuilder sb = new StringBuilder();
         sb.append(superStorage);
         sb.append("!");
         sb.append(this.valueName);
         sb.append("!");
         sb.append(this.valueIndex);
         return sb.toString();
      }
   }

   static class OccurrenceDisplayDescriptor {
      int eventTag = -1;
      int seriesValueIndex = -1;
      boolean includePid = false;
      int filterValueIndex = -1;
      CompareMethod filterCompareMethod;
      Object filterValue;

      OccurrenceDisplayDescriptor() {
         this.filterCompareMethod = CompareMethod.EQUAL_TO;
         this.filterValue = null;
      }

      OccurrenceDisplayDescriptor(EventDisplay.OccurrenceDisplayDescriptor descriptor) {
         this.filterCompareMethod = CompareMethod.EQUAL_TO;
         this.filterValue = null;
         this.replaceWith(descriptor);
      }

      OccurrenceDisplayDescriptor(int eventTag) {
         this.filterCompareMethod = CompareMethod.EQUAL_TO;
         this.filterValue = null;
         this.eventTag = eventTag;
      }

      OccurrenceDisplayDescriptor(int eventTag, int seriesValueIndex) {
         this.filterCompareMethod = CompareMethod.EQUAL_TO;
         this.filterValue = null;
         this.eventTag = eventTag;
         this.seriesValueIndex = seriesValueIndex;
      }

      void replaceWith(EventDisplay.OccurrenceDisplayDescriptor descriptor) {
         this.eventTag = descriptor.eventTag;
         this.seriesValueIndex = descriptor.seriesValueIndex;
         this.includePid = descriptor.includePid;
         this.filterValueIndex = descriptor.filterValueIndex;
         this.filterCompareMethod = descriptor.filterCompareMethod;
         this.filterValue = descriptor.filterValue;
      }

      final void loadFrom(String storageString) {
         String[] values = storageString.split(Pattern.quote("!"));
         this.loadFrom(values, 0);
      }

      protected int loadFrom(String[] storageStrings, int index) {
         this.eventTag = Integer.parseInt(storageStrings[index++]);
         this.seriesValueIndex = Integer.parseInt(storageStrings[index++]);
         this.includePid = Boolean.parseBoolean(storageStrings[index++]);
         this.filterValueIndex = Integer.parseInt(storageStrings[index++]);

         try {
            this.filterCompareMethod = CompareMethod.valueOf(storageStrings[index++]);
         } catch (IllegalArgumentException var4) {
            this.filterCompareMethod = CompareMethod.EQUAL_TO;
         }

         String value = storageStrings[index++];
         if(this.filterValueIndex != -1 && !"<null>".equals(value)) {
            this.filterValue = EventValueType.getObjectFromStorageString(value);
         }

         return index;
      }

      String getStorageString() {
         StringBuilder sb = new StringBuilder();
         sb.append(this.eventTag);
         sb.append("!");
         sb.append(this.seriesValueIndex);
         sb.append("!");
         sb.append(Boolean.toString(this.includePid));
         sb.append("!");
         sb.append(this.filterValueIndex);
         sb.append("!");
         sb.append(this.filterCompareMethod.name());
         sb.append("!");
         if(this.filterValue != null) {
            String value = EventValueType.getStorageString(this.filterValue);
            if(value != null) {
               sb.append(value);
            } else {
               sb.append("<null>");
            }
         } else {
            sb.append("<null>");
         }

         return sb.toString();
      }
   }

   interface ILogColumnListener {
      void columnResized(int var1, TableColumn var2);
   }
}
