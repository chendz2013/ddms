package com.android.ddmuilib.log.event;

import com.android.ddmlib.log.EventContainer;
import com.android.ddmlib.log.EventLogParser;
import com.android.ddmlib.log.EventValueDescription;
import com.android.ddmlib.log.InvalidTypeException;
import com.android.ddmlib.log.EventContainer.EventValueType;
import com.android.ddmlib.log.EventValueDescription.ValueType;
import com.android.ddmuilib.DdmUiPreferences;
import com.android.ddmuilib.TableHelper;
import com.android.ddmuilib.log.event.EventDisplay;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.ScrollBar;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

public class DisplayLog extends EventDisplay {
   private static final String PREFS_COL_DATE = "EventLogPanel.log.Col1";
   private static final String PREFS_COL_PID = "EventLogPanel.log.Col2";
   private static final String PREFS_COL_EVENTTAG = "EventLogPanel.log.Col3";
   private static final String PREFS_COL_VALUENAME = "EventLogPanel.log.Col4";
   private static final String PREFS_COL_VALUE = "EventLogPanel.log.Col5";
   private static final String PREFS_COL_TYPE = "EventLogPanel.log.Col6";

   public DisplayLog(String name) {
      super(name);
   }

   void resetUI() {
      this.mLogTable.removeAll();
   }

   void newEvent(EventContainer event, EventLogParser logParser) {
      this.addToLog(event, logParser);
   }

   Control createComposite(Composite parent, EventLogParser logParser, EventDisplay.ILogColumnListener listener) {
      return this.createLogUI(parent, listener);
   }

   private void addToLog(EventContainer event, EventLogParser logParser) {
      ScrollBar bar = this.mLogTable.getVerticalBar();
      boolean scroll = bar.getMaximum() == bar.getSelection() + bar.getThumb();
      Calendar c = Calendar.getInstance();
      long msec = (long)event.sec * 1000L;
      c.setTimeInMillis(msec);
      String date = String.format("%1$tF %1$tT", new Object[]{c});
      String eventName = (String)logParser.getTagMap().get(Integer.valueOf(event.mTag));
      String pidName = Integer.toString(event.pid);
      EventValueDescription[] valueDescription = (EventValueDescription[])logParser.getEventInfoMap().get(Integer.valueOf(event.mTag));
      if(valueDescription != null) {
         int itemCount;
         for(itemCount = 0; itemCount < valueDescription.length; ++itemCount) {
            EventValueDescription description = valueDescription[itemCount];

            try {
               String e = event.getValueAsString(itemCount);
               this.logValue(date, pidName, eventName, description.getName(), e, description.getEventValueType(), description.getValueType());
            } catch (InvalidTypeException var15) {
               this.logValue(date, pidName, eventName, description.getName(), var15.getMessage(), description.getEventValueType(), description.getValueType());
            }
         }

         if(scroll) {
            itemCount = this.mLogTable.getItemCount();
            if(itemCount > 0) {
               this.mLogTable.showItem(this.mLogTable.getItem(itemCount - 1));
            }
         }
      }

   }

   protected void addToLog(EventContainer event, EventLogParser logParser, ArrayList<EventDisplay.ValueDisplayDescriptor> valueDescriptors, ArrayList<EventDisplay.OccurrenceDisplayDescriptor> occurrenceDescriptors) {
      ScrollBar bar = this.mLogTable.getVerticalBar();
      boolean scroll = bar.getMaximum() == bar.getSelection() + bar.getThumb();
      Calendar c = Calendar.getInstance();
      long msec = (long)event.sec * 1000L;
      c.setTimeInMillis(msec);
      String date = String.format("%1$tF %1$tT", new Object[]{c});
      String eventName = (String)logParser.getTagMap().get(Integer.valueOf(event.mTag));
      String pidName = Integer.toString(event.pid);
      if(valueDescriptors.size() > 0) {
         Iterator itemCount = valueDescriptors.iterator();

         while(itemCount.hasNext()) {
            EventDisplay.ValueDisplayDescriptor descriptor = (EventDisplay.ValueDisplayDescriptor)itemCount.next();
            this.logDescriptor(event, descriptor, date, pidName, eventName, logParser);
         }
      }

      if(scroll) {
         int itemCount1 = this.mLogTable.getItemCount();
         if(itemCount1 > 0) {
            this.mLogTable.showItem(this.mLogTable.getItem(itemCount1 - 1));
         }
      }

   }

   private void logValue(String date, String pid, String event, String valueName, String value, EventValueType eventValueType, ValueType valueType) {
      TableItem item = new TableItem(this.mLogTable, 0);
      item.setText(0, date);
      item.setText(1, pid);
      item.setText(2, event);
      item.setText(3, valueName);
      item.setText(4, value);
      String type;
      if(valueType != ValueType.NOT_APPLICABLE) {
         type = String.format("%1$s, %2$s", new Object[]{eventValueType.toString(), valueType.toString()});
      } else {
         type = eventValueType.toString();
      }

      item.setText(5, type);
   }

   private void logDescriptor(EventContainer event, EventDisplay.ValueDisplayDescriptor descriptor, String date, String pidName, String eventName, EventLogParser logParser) {
      String value;
      try {
         value = event.getValueAsString(descriptor.valueIndex);
      } catch (InvalidTypeException var10) {
         value = var10.getMessage();
      }

      EventValueDescription[] values = (EventValueDescription[])logParser.getEventInfoMap().get(Integer.valueOf(event.mTag));
      EventValueDescription valueDescription = values[descriptor.valueIndex];
      this.logValue(date, pidName, eventName, descriptor.valueName, value, valueDescription.getEventValueType(), valueDescription.getValueType());
   }

   private Control createLogUI(Composite parent, final EventDisplay.ILogColumnListener listener) {
      Composite mainComp = new Composite(parent, 0);
      GridLayout gl;
      mainComp.setLayout(gl = new GridLayout(1, false));
      gl.marginHeight = gl.marginWidth = 0;
      mainComp.addDisposeListener(new DisposeListener() {
         public void widgetDisposed(DisposeEvent e) {
            DisplayLog.this.mLogTable = null;
         }
      });
      Label l = new Label(mainComp, 16777216);
      l.setText(this.mName);
      l.setLayoutData(new GridData(768));
      this.mLogTable = new Table(mainComp, 68098);
      this.mLogTable.setLayoutData(new GridData(1808));
      IPreferenceStore store = DdmUiPreferences.getStore();
      TableColumn col = TableHelper.createTableColumn(this.mLogTable, "Time", 16384, "0000-00-00 00:00:00", "EventLogPanel.log.Col1", store);
      col.addControlListener(new ControlAdapter() {
         public void controlResized(ControlEvent e) {
            Object source = e.getSource();
            if(source instanceof TableColumn) {
               listener.columnResized(0, (TableColumn)source);
            }

         }
      });
      col = TableHelper.createTableColumn(this.mLogTable, "pid", 16384, "0000", "EventLogPanel.log.Col2", store);
      col.addControlListener(new ControlAdapter() {
         public void controlResized(ControlEvent e) {
            Object source = e.getSource();
            if(source instanceof TableColumn) {
               listener.columnResized(1, (TableColumn)source);
            }

         }
      });
      col = TableHelper.createTableColumn(this.mLogTable, "Event", 16384, "abcdejghijklmno", "EventLogPanel.log.Col3", store);
      col.addControlListener(new ControlAdapter() {
         public void controlResized(ControlEvent e) {
            Object source = e.getSource();
            if(source instanceof TableColumn) {
               listener.columnResized(2, (TableColumn)source);
            }

         }
      });
      col = TableHelper.createTableColumn(this.mLogTable, "Name", 16384, "Process Name", "EventLogPanel.log.Col4", store);
      col.addControlListener(new ControlAdapter() {
         public void controlResized(ControlEvent e) {
            Object source = e.getSource();
            if(source instanceof TableColumn) {
               listener.columnResized(3, (TableColumn)source);
            }

         }
      });
      col = TableHelper.createTableColumn(this.mLogTable, "Value", 16384, "0000000", "EventLogPanel.log.Col5", store);
      col.addControlListener(new ControlAdapter() {
         public void controlResized(ControlEvent e) {
            Object source = e.getSource();
            if(source instanceof TableColumn) {
               listener.columnResized(4, (TableColumn)source);
            }

         }
      });
      col = TableHelper.createTableColumn(this.mLogTable, "Type", 16384, "long, seconds", "EventLogPanel.log.Col6", store);
      col.addControlListener(new ControlAdapter() {
         public void controlResized(ControlEvent e) {
            Object source = e.getSource();
            if(source instanceof TableColumn) {
               listener.columnResized(5, (TableColumn)source);
            }

         }
      });
      this.mLogTable.setHeaderVisible(true);
      this.mLogTable.setLinesVisible(true);
      return mainComp;
   }

   void resizeColumn(int index, TableColumn sourceColumn) {
      if(this.mLogTable != null) {
         TableColumn col = this.mLogTable.getColumn(index);
         if(col != sourceColumn) {
            col.setWidth(sourceColumn.getWidth());
         }
      }

   }

   int getDisplayType() {
      return 0;
   }
}
