package com.android.ddmuilib.log.event;

import com.android.ddmlib.log.EventContainer;
import com.android.ddmlib.log.EventLogParser;
import com.android.ddmlib.log.InvalidTypeException;
import com.android.ddmuilib.log.event.EventDisplay;
import java.awt.Color;

public abstract class SyncCommon extends EventDisplay {
   private int mLastState;
   private long mLastStartTime;
   private long mLastStopTime;
   private String mLastDetails;
   private int mLastSyncSource;
   protected static final int CALENDAR = 0;
   protected static final int GMAIL = 1;
   protected static final int FEEDS = 2;
   protected static final int CONTACTS = 3;
   protected static final int ERRORS = 4;
   protected static final int NUM_AUTHS = 4;
   protected static final String[] AUTH_NAMES = new String[]{"Calendar", "Gmail", "Feeds", "Contacts", "Errors"};
   protected static final Color[] AUTH_COLORS;
   final int EVENT_SYNC = 2720;
   final int EVENT_TICKLE = 2742;
   final int EVENT_SYNC_DETAILS = 2743;
   final int EVENT_CONTACTS_AGGREGATION = 2747;

   protected SyncCommon(String name) {
      super(name);
   }

   void resetUI() {
      this.mLastStartTime = 0L;
      this.mLastStopTime = 0L;
      this.mLastState = -1;
      this.mLastSyncSource = -1;
      this.mLastDetails = "";
   }

   void newEvent(EventContainer event, EventLogParser logParser) {
      try {
         if(event.mTag == 2720) {
            int e = Integer.parseInt(event.getValueAsString(1));
            if(e == 0) {
               this.mLastStartTime = (long)event.sec * 1000L + (long)event.nsec / 1000000L;
               this.mLastState = 0;
               this.mLastSyncSource = Integer.parseInt(event.getValueAsString(2));
               this.mLastDetails = "";
            } else if(e == 1 && this.mLastState == 0) {
               this.mLastStopTime = (long)event.sec * 1000L + (long)event.nsec / 1000000L;
               if(this.mLastStartTime == 0L) {
                  this.mLastStartTime = this.mLastStopTime;
               }

               int auth = this.getAuth(event.getValueAsString(0));
               this.processSyncEvent(event, auth, this.mLastStartTime, this.mLastStopTime, this.mLastDetails, true, this.mLastSyncSource);
               this.mLastState = 1;
            }
         } else {
            long e1;
            if(event.mTag == 2743) {
               this.mLastDetails = event.getValueAsString(3);
               if(this.mLastState != 0) {
                  e1 = (long)event.sec * 1000L + (long)event.nsec / 1000000L;
                  if(e1 - this.mLastStopTime <= 250L) {
                     int startTime = this.getAuth(event.getValueAsString(0));
                     this.processSyncEvent(event, startTime, this.mLastStartTime, this.mLastStopTime, this.mLastDetails, false, this.mLastSyncSource);
                  }
               }
            } else if(event.mTag == 2747) {
               e1 = (long)event.sec * 1000L + (long)event.nsec / 1000000L;
               long startTime1 = e1 - Long.parseLong(event.getValueAsString(0));
               int count = Integer.parseInt(event.getValueAsString(1));
               String details;
               if(count < 0) {
                  details = "g" + -count;
               } else {
                  details = "G" + count;
               }

               this.processSyncEvent(event, 3, startTime1, e1, details, true, this.mLastSyncSource);
            }
         }
      } catch (InvalidTypeException var9) {
         ;
      }

   }

   abstract void processSyncEvent(EventContainer var1, int var2, long var3, long var5, String var7, boolean var8, int var9);

   protected int getAuth(String authname) throws InvalidTypeException {
      if(!"calendar".equals(authname) && !"cl".equals(authname) && !"com.android.calendar".equals(authname)) {
         if(!"contacts".equals(authname) && !"cp".equals(authname) && !"com.android.contacts".equals(authname)) {
            if("subscribedfeeds".equals(authname)) {
               return 2;
            } else if(!"gmail-ls".equals(authname) && !"mail".equals(authname)) {
               if("gmail-live".equals(authname)) {
                  return 1;
               } else if("unknown".equals(authname)) {
                  return -1;
               } else {
                  throw new InvalidTypeException("Unknown authname " + authname);
               }
            } else {
               return 1;
            }
         } else {
            return 3;
         }
      } else {
         return 0;
      }
   }

   static {
      AUTH_COLORS = new Color[]{Color.MAGENTA, Color.GREEN, Color.BLUE, Color.ORANGE, Color.RED};
   }
}
