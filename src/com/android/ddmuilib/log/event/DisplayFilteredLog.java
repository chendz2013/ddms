package com.android.ddmuilib.log.event;

import com.android.ddmlib.log.EventContainer;
import com.android.ddmlib.log.EventLogParser;
import com.android.ddmuilib.log.event.DisplayLog;
import java.util.ArrayList;

public class DisplayFilteredLog extends DisplayLog {
   public DisplayFilteredLog(String name) {
      super(name);
   }

   void newEvent(EventContainer event, EventLogParser logParser) {
      ArrayList valueDescriptors = new ArrayList();
      ArrayList occurrenceDescriptors = new ArrayList();
      if(this.filterEvent(event, valueDescriptors, occurrenceDescriptors)) {
         this.addToLog(event, logParser, valueDescriptors, occurrenceDescriptors);
      }

   }

   int getDisplayType() {
      return 1;
   }
}
