package com.android.ddmuilib.log.event;

import com.android.ddmlib.log.EventContainer;
import com.android.ddmlib.log.EventLogParser;
import com.android.ddmlib.log.EventValueDescription;
import com.android.ddmlib.log.InvalidTypeException;
import com.android.ddmlib.log.EventValueDescription.ValueType;
import com.android.ddmuilib.log.event.EventDisplay;
import com.android.ddmuilib.log.event.OccurrenceRenderer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYAreaRenderer;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.Millisecond;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;

public class DisplayGraph extends EventDisplay {
   public DisplayGraph(String name) {
      super(name);
   }

   void resetUI() {
      Collection datasets = this.mValueTypeDataSetMap.values();
      Iterator i$ = datasets.iterator();

      while(i$.hasNext()) {
         TimeSeriesCollection dataset = (TimeSeriesCollection)i$.next();
         dataset.removeAllSeries();
      }

      if(this.mOccurrenceDataSet != null) {
         this.mOccurrenceDataSet.removeAllSeries();
      }

      this.mValueDescriptorSeriesMap.clear();
      this.mOcurrenceDescriptorSeriesMap.clear();
   }

   public Control createComposite(Composite parent, EventLogParser logParser, EventDisplay.ILogColumnListener listener) {
      String title = this.getChartTitle(logParser);
      return this.createCompositeChart(parent, logParser, title);
   }

   void newEvent(EventContainer event, EventLogParser logParser) {
      ArrayList valueDescriptors = new ArrayList();
      ArrayList occurrenceDescriptors = new ArrayList();
      if(this.filterEvent(event, valueDescriptors, occurrenceDescriptors)) {
         this.updateChart(event, logParser, valueDescriptors, occurrenceDescriptors);
      }

   }

   private void updateChart(EventContainer event, EventLogParser logParser, ArrayList<EventDisplay.ValueDisplayDescriptor> valueDescriptors, ArrayList<EventDisplay.OccurrenceDisplayDescriptor> occurrenceDescriptors) {
      Map tagMap = logParser.getTagMap();
      Millisecond millisecondTime = null;
      long msec = -1L;
      boolean accumulateValues = false;
      double accumulatedValue = 0.0D;
      Iterator pidMapValues;
      EventDisplay.ValueDisplayDescriptor i$;
      if(event.mTag == 2721) {
         accumulateValues = true;

         for(pidMapValues = valueDescriptors.iterator(); pidMapValues.hasNext(); accumulateValues &= i$.valueIndex != 0) {
            i$ = (EventDisplay.ValueDisplayDescriptor)pidMapValues.next();
         }
      }

      pidMapValues = valueDescriptors.iterator();

      HashMap pidMapValue;
      TimeSeries seriesCollection;
      String timeSeries;
      String i$1;
      while(pidMapValues.hasNext()) {
         i$ = (EventDisplay.ValueDisplayDescriptor)pidMapValues.next();

         try {
            pidMapValue = (HashMap)this.mValueDescriptorSeriesMap.get(i$);
            if(pidMapValue == null) {
               pidMapValue = new HashMap();
               this.mValueDescriptorSeriesMap.put(i$, pidMapValue);
            }

            seriesCollection = (TimeSeries)pidMapValue.get(Integer.valueOf(event.pid));
            if(seriesCollection == null) {
               i$1 = null;
               timeSeries = this.getSeriesLabel(event, i$);
               switch(this.mValueDescriptorCheck) {
               case 1:
                  i$1 = String.format("%1$s / %2$s", new Object[]{timeSeries, i$.valueName});
                  break;
               case 2:
                  i$1 = String.format("%1$s", new Object[]{timeSeries});
                  break;
               default:
                  i$1 = String.format("%1$s / %2$s: %3$s", new Object[]{timeSeries, tagMap.get(Integer.valueOf(i$.eventTag)), i$.valueName});
               }

               TimeSeriesCollection dataset = this.getValueDataset(((EventValueDescription[])logParser.getEventInfoMap().get(Integer.valueOf(event.mTag)))[i$.valueIndex].getValueType(), accumulateValues);
               seriesCollection = new TimeSeries(i$1, Millisecond.class);
               if(this.mMaximumChartItemAge != -1L) {
                  seriesCollection.setMaximumItemAge(this.mMaximumChartItemAge * 1000L);
               }

               dataset.addSeries(seriesCollection);
               pidMapValue.put(Integer.valueOf(event.pid), seriesCollection);
            }

            double i$4 = event.getValueAsDouble(i$.valueIndex);
            if(accumulateValues) {
               accumulatedValue += i$4;
               i$4 = accumulatedValue;
            }

            if(millisecondTime == null) {
               msec = (long)event.sec * 1000L + (long)event.nsec / 1000000L;
               millisecondTime = new Millisecond(new Date(msec));
            }

            seriesCollection.addOrUpdate(millisecondTime, i$4);
         } catch (InvalidTypeException var20) {
            ;
         }
      }

      pidMapValues = occurrenceDescriptors.iterator();

      while(pidMapValues.hasNext()) {
         EventDisplay.OccurrenceDisplayDescriptor i$2 = (EventDisplay.OccurrenceDisplayDescriptor)pidMapValues.next();

         try {
            pidMapValue = (HashMap)this.mOcurrenceDescriptorSeriesMap.get(i$2);
            if(pidMapValue == null) {
               pidMapValue = new HashMap();
               this.mOcurrenceDescriptorSeriesMap.put(i$2, pidMapValue);
            }

            seriesCollection = (TimeSeries)pidMapValue.get(Integer.valueOf(event.pid));
            if(seriesCollection == null) {
               i$1 = this.getSeriesLabel(event, i$2);
               timeSeries = String.format("[%1$s:%2$s]", new Object[]{tagMap.get(Integer.valueOf(i$2.eventTag)), i$1});
               seriesCollection = new TimeSeries(timeSeries, Millisecond.class);
               if(this.mMaximumChartItemAge != -1L) {
                  seriesCollection.setMaximumItemAge(this.mMaximumChartItemAge);
               }

               this.getOccurrenceDataSet().addSeries(seriesCollection);
               pidMapValue.put(Integer.valueOf(event.pid), seriesCollection);
            }

            if(millisecondTime == null) {
               msec = (long)event.sec * 1000L + (long)event.nsec / 1000000L;
               millisecondTime = new Millisecond(new Date(msec));
            }

            seriesCollection.addOrUpdate(millisecondTime, 0.0D);
         } catch (InvalidTypeException var19) {
            ;
         }
      }

      if(msec != -1L && this.mMaximumChartItemAge != -1L) {
         Collection pidMapValues1 = this.mValueDescriptorSeriesMap.values();
         Iterator i$3 = pidMapValues1.iterator();

         TimeSeries timeSeries1;
         Iterator i$5;
         Collection seriesCollection1;
         while(i$3.hasNext()) {
            pidMapValue = (HashMap)i$3.next();
            seriesCollection1 = pidMapValue.values();
            i$5 = seriesCollection1.iterator();

            while(i$5.hasNext()) {
               timeSeries1 = (TimeSeries)i$5.next();
               timeSeries1.removeAgedItems(msec, true);
            }
         }

         pidMapValues1 = this.mOcurrenceDescriptorSeriesMap.values();
         i$3 = pidMapValues1.iterator();

         while(i$3.hasNext()) {
            pidMapValue = (HashMap)i$3.next();
            seriesCollection1 = pidMapValue.values();
            i$5 = seriesCollection1.iterator();

            while(i$5.hasNext()) {
               timeSeries1 = (TimeSeries)i$5.next();
               timeSeries1.removeAgedItems(msec, true);
            }
         }
      }

   }

   private TimeSeriesCollection getValueDataset(ValueType type, boolean accumulateValues) {
      TimeSeriesCollection dataset = (TimeSeriesCollection)this.mValueTypeDataSetMap.get(type);
      if(dataset == null) {
         dataset = new TimeSeriesCollection();
         this.mValueTypeDataSetMap.put(type, dataset);
         Object renderer;
         if(type == ValueType.PERCENT && accumulateValues) {
            renderer = new XYAreaRenderer();
         } else {
            XYLineAndShapeRenderer xyPlot = new XYLineAndShapeRenderer();
            xyPlot.setBaseShapesVisible(type != ValueType.PERCENT);
            renderer = xyPlot;
         }

         XYPlot var8 = this.mChart.getXYPlot();
         var8.setDataset(this.mDataSetCount, dataset);
         var8.setRenderer(this.mDataSetCount, (XYItemRenderer)renderer);
         NumberAxis axis = new NumberAxis(type.toString());
         if(type == ValueType.PERCENT) {
            axis.setAutoRange(false);
            axis.setRange(0.0D, 100.0D);
         }

         int count = this.mDataSetCount;
         if(this.mOccurrenceDataSet != null) {
            --count;
         }

         var8.setRangeAxis(count, axis);
         if(count % 2 == 0) {
            var8.setRangeAxisLocation(count, AxisLocation.BOTTOM_OR_LEFT);
         } else {
            var8.setRangeAxisLocation(count, AxisLocation.TOP_OR_RIGHT);
         }

         var8.mapDatasetToRangeAxis(this.mDataSetCount, count);
         ++this.mDataSetCount;
      }

      return dataset;
   }

   private String getSeriesLabel(EventContainer event, EventDisplay.OccurrenceDisplayDescriptor descriptor) throws InvalidTypeException {
      return descriptor.seriesValueIndex != -1?(!descriptor.includePid?event.getValueAsString(descriptor.seriesValueIndex):String.format("%1$s (%2$d)", new Object[]{event.getValueAsString(descriptor.seriesValueIndex), Integer.valueOf(event.pid)})):Integer.toString(event.pid);
   }

   private TimeSeriesCollection getOccurrenceDataSet() {
      if(this.mOccurrenceDataSet == null) {
         this.mOccurrenceDataSet = new TimeSeriesCollection();
         XYPlot xyPlot = this.mChart.getXYPlot();
         xyPlot.setDataset(this.mDataSetCount, this.mOccurrenceDataSet);
         OccurrenceRenderer renderer = new OccurrenceRenderer();
         renderer.setBaseShapesVisible(false);
         xyPlot.setRenderer(this.mDataSetCount, renderer);
         ++this.mDataSetCount;
      }

      return this.mOccurrenceDataSet;
   }

   int getDisplayType() {
      return 2;
   }

   protected void setNewLogParser(EventLogParser logParser) {
      if(this.mChart != null) {
         this.mChart.setTitle(this.getChartTitle(logParser));
      }

   }

   private String getChartTitle(EventLogParser logParser) {
      if(this.mValueDescriptors.size() > 0) {
         String chartDesc = null;
         switch(this.mValueDescriptorCheck) {
         case 1:
            if(logParser != null) {
               chartDesc = (String)logParser.getTagMap().get(Integer.valueOf(((EventDisplay.ValueDisplayDescriptor)this.mValueDescriptors.get(0)).eventTag));
            }
            break;
         case 2:
            if(logParser != null) {
               chartDesc = String.format("%1$s / %2$s", new Object[]{logParser.getTagMap().get(Integer.valueOf(((EventDisplay.ValueDisplayDescriptor)this.mValueDescriptors.get(0)).eventTag)), ((EventDisplay.ValueDisplayDescriptor)this.mValueDescriptors.get(0)).valueName});
            }
         }

         if(chartDesc != null) {
            return String.format("%1$s - %2$s", new Object[]{this.mName, chartDesc});
         }
      }

      return this.mName;
   }
}
