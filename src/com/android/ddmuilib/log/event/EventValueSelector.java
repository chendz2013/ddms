package com.android.ddmuilib.log.event;

import com.android.ddmlib.log.EventLogParser;
import com.android.ddmlib.log.EventValueDescription;
import com.android.ddmlib.log.EventContainer.CompareMethod;
import com.android.ddmlib.log.EventContainer.EventValueType;
import com.android.ddmuilib.log.event.EventDisplay;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

final class EventValueSelector extends Dialog {
   private static final int DLG_WIDTH = 400;
   private static final int DLG_HEIGHT = 300;
   private Shell mParent;
   private Shell mShell;
   private boolean mEditStatus;
   private Combo mEventCombo;
   private Combo mValueCombo;
   private Combo mSeriesCombo;
   private Button mDisplayPidCheckBox;
   private Combo mFilterCombo;
   private Combo mFilterMethodCombo;
   private Text mFilterValue;
   private Button mOkButton;
   private EventLogParser mLogParser;
   private EventDisplay.OccurrenceDisplayDescriptor mDescriptor;
   private Integer[] mEventTags;
   private final ArrayList<Integer> mSeriesIndices = new ArrayList();

   public EventValueSelector(Shell parent) {
      super(parent, 67680);
   }

   boolean open(Class<? extends EventDisplay.OccurrenceDisplayDescriptor> descriptorClass, EventLogParser logParser) {
      try {
         EventDisplay.OccurrenceDisplayDescriptor e = (EventDisplay.OccurrenceDisplayDescriptor)descriptorClass.newInstance();
         this.setModified();
         return this.open(e, logParser);
      } catch (InstantiationException var4) {
         return false;
      } catch (IllegalAccessException var5) {
         return false;
      }
   }

   boolean open(EventDisplay.OccurrenceDisplayDescriptor descriptor, EventLogParser logParser) {
      if(descriptor instanceof EventDisplay.ValueDisplayDescriptor) {
         this.mDescriptor = new EventDisplay.ValueDisplayDescriptor((EventDisplay.ValueDisplayDescriptor)descriptor);
      } else {
         if(!(descriptor instanceof EventDisplay.OccurrenceDisplayDescriptor)) {
            return false;
         }

         this.mDescriptor = new EventDisplay.OccurrenceDisplayDescriptor(descriptor);
      }

      this.mLogParser = logParser;
      this.createUI();
      if(this.mParent != null && this.mShell != null) {
         this.loadValueDescriptor();
         this.checkValidity();

         try {
            this.mShell.setMinimumSize(400, 300);
            Rectangle display = this.mParent.getBounds();
            int cx = display.x + display.width / 2;
            int x = cx - 200;
            int cy = display.y + display.height / 2;
            int y = cy - 150;
            this.mShell.setBounds(x, y, 400, 300);
         } catch (Exception var8) {
            var8.printStackTrace();
         }

         this.mShell.layout();
         this.mShell.open();
         Display display1 = this.mParent.getDisplay();

         while(!this.mShell.isDisposed()) {
            if(!display1.readAndDispatch()) {
               display1.sleep();
            }
         }

         return this.mEditStatus;
      } else {
         return false;
      }
   }

   EventDisplay.OccurrenceDisplayDescriptor getDescriptor() {
      return this.mDescriptor;
   }

   private void createUI() {
      this.mParent = this.getParent();
      this.mShell = new Shell(this.mParent, this.getStyle());
      this.mShell.setText("Event Display Configuration");
      this.mShell.setLayout(new GridLayout(2, false));
      Label l = new Label(this.mShell, 0);
      l.setText("Event:");
      this.mEventCombo = new Combo(this.mShell, 12);
      this.mEventCombo.setLayoutData(new GridData(768));
      Map eventTagMap = this.mLogParser.getTagMap();
      Map eventInfoMap = this.mLogParser.getEventInfoMap();
      Set keys = eventTagMap.keySet();
      ArrayList list = new ArrayList();
      Iterator buttonComp = keys.iterator();

      while(buttonComp.hasNext()) {
         Integer gl = (Integer)buttonComp.next();
         if(eventInfoMap.get(gl) != null) {
            String padding = (String)eventTagMap.get(gl);
            this.mEventCombo.add(padding);
            list.add(gl);
         }
      }

      this.mEventTags = (Integer[])list.toArray(new Integer[list.size()]);
      this.mEventCombo.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            EventValueSelector.this.handleEventComboSelection();
            EventValueSelector.this.setModified();
         }
      });
      l = new Label(this.mShell, 0);
      l.setText("Value:");
      this.mValueCombo = new Combo(this.mShell, 12);
      this.mValueCombo.setLayoutData(new GridData(768));
      this.mValueCombo.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            EventValueSelector.this.handleValueComboSelection();
            EventValueSelector.this.setModified();
         }
      });
      l = new Label(this.mShell, 0);
      l.setText("Series Name:");
      this.mSeriesCombo = new Combo(this.mShell, 12);
      this.mSeriesCombo.setLayoutData(new GridData(768));
      this.mSeriesCombo.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            EventValueSelector.this.handleSeriesComboSelection();
            EventValueSelector.this.setModified();
         }
      });
      GridData gd;
      (new Composite(this.mShell, 0)).setLayoutData(gd = new GridData());
      gd.heightHint = gd.widthHint = 0;
      this.mDisplayPidCheckBox = new Button(this.mShell, 32);
      this.mDisplayPidCheckBox.setText("Also Show pid");
      this.mDisplayPidCheckBox.setEnabled(false);
      this.mDisplayPidCheckBox.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            EventValueSelector.this.mDescriptor.includePid = EventValueSelector.this.mDisplayPidCheckBox.getSelection();
            EventValueSelector.this.setModified();
         }
      });
      l = new Label(this.mShell, 0);
      l.setText("Filter By:");
      this.mFilterCombo = new Combo(this.mShell, 12);
      this.mFilterCombo.setLayoutData(new GridData(768));
      this.mFilterCombo.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            EventValueSelector.this.handleFilterComboSelection();
            EventValueSelector.this.setModified();
         }
      });
      l = new Label(this.mShell, 0);
      l.setText("Filter Method:");
      this.mFilterMethodCombo = new Combo(this.mShell, 12);
      this.mFilterMethodCombo.setLayoutData(new GridData(768));
      CompareMethod[] var11 = CompareMethod.values();
      int var14 = var11.length;

      for(int var15 = 0; var15 < var14; ++var15) {
         CompareMethod cancelButton = var11[var15];
         this.mFilterMethodCombo.add(cancelButton.toString());
      }

      this.mFilterMethodCombo.select(0);
      this.mFilterMethodCombo.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            EventValueSelector.this.handleFilterMethodComboSelection();
            EventValueSelector.this.setModified();
         }
      });
      l = new Label(this.mShell, 0);
      l.setText("Filter Value:");
      this.mFilterValue = new Text(this.mShell, 2052);
      this.mFilterValue.setLayoutData(new GridData(768));
      this.mFilterValue.addModifyListener(new ModifyListener() {
         public void modifyText(ModifyEvent e) {
            if(EventValueSelector.this.mDescriptor.filterValueIndex != -1) {
               int index = EventValueSelector.this.mEventCombo.getSelectionIndex();
               if(index != -1) {
                  int eventTag = EventValueSelector.this.mEventTags[index].intValue();
                  EventValueSelector.this.mDescriptor.eventTag = eventTag;
                  EventValueDescription valueDesc = ((EventValueDescription[])EventValueSelector.this.mLogParser.getEventInfoMap().get(Integer.valueOf(eventTag)))[EventValueSelector.this.mDescriptor.filterValueIndex];
                  EventValueSelector.this.mDescriptor.filterValue = valueDesc.getObjectFromString(EventValueSelector.this.mFilterValue.getText().trim());
                  EventValueSelector.this.setModified();
               }
            }

         }
      });
      l = new Label(this.mShell, 258);
      gd = new GridData(768);
      gd.horizontalSpan = 2;
      l.setLayoutData(gd);
      Composite var12 = new Composite(this.mShell, 0);
      gd = new GridData(768);
      gd.horizontalSpan = 2;
      var12.setLayoutData(gd);
      GridLayout var13;
      var12.setLayout(var13 = new GridLayout(6, true));
      var13.marginHeight = var13.marginWidth = 0;
      Composite var16 = new Composite(this.mShell, 0);
      var16.setLayoutData(new GridData(768));
      this.mOkButton = new Button(var12, 8);
      this.mOkButton.setText("OK");
      this.mOkButton.setLayoutData(new GridData(2));
      this.mOkButton.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            EventValueSelector.this.mShell.close();
         }
      });
      var16 = new Composite(this.mShell, 0);
      var16.setLayoutData(new GridData(768));
      var16 = new Composite(this.mShell, 0);
      var16.setLayoutData(new GridData(768));
      Button var17 = new Button(var12, 8);
      var17.setText("Cancel");
      var17.setLayoutData(new GridData(2));
      var17.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            EventValueSelector.this.mEditStatus = false;
            EventValueSelector.this.mShell.close();
         }
      });
      var16 = new Composite(this.mShell, 0);
      var16.setLayoutData(new GridData(768));
      this.mShell.addListener(21, new Listener() {
         public void handleEvent(Event event) {
            event.doit = true;
         }
      });
   }

   private void setModified() {
      this.mEditStatus = true;
   }

   private void handleEventComboSelection() {
      int index = this.mEventCombo.getSelectionIndex();
      if(index != -1) {
         int eventTag = this.mEventTags[index].intValue();
         this.mDescriptor.eventTag = eventTag;
         EventValueDescription[] values = (EventValueDescription[])this.mLogParser.getEventInfoMap().get(Integer.valueOf(eventTag));
         this.mValueCombo.removeAll();
         if(values != null) {
            int len$;
            if(!(this.mDescriptor instanceof EventDisplay.ValueDisplayDescriptor)) {
               this.mValueCombo.setEnabled(false);
            } else {
               EventDisplay.ValueDisplayDescriptor axisIndex = (EventDisplay.ValueDisplayDescriptor)this.mDescriptor;
               this.mValueCombo.setEnabled(true);
               EventValueDescription[] selectionIndex = values;
               int valueInfo = values.length;

               for(len$ = 0; len$ < valueInfo; ++len$) {
                  EventValueDescription i$ = selectionIndex[len$];
                  this.mValueCombo.add(i$.toString());
               }

               if(axisIndex.valueIndex != -1) {
                  this.mValueCombo.select(axisIndex.valueIndex);
               } else {
                  this.mValueCombo.clearSelection();
               }
            }

            this.mSeriesCombo.removeAll();
            this.mSeriesCombo.setEnabled(false);
            this.mSeriesIndices.clear();
            int var10 = 0;
            int var11 = -1;
            EventValueDescription[] var12 = values;
            len$ = values.length;

            EventValueDescription value;
            int var14;
            for(var14 = 0; var14 < len$; ++var14) {
               value = var12[var14];
               if(value.getEventValueType() == EventValueType.STRING) {
                  this.mSeriesCombo.add(value.getName());
                  this.mSeriesCombo.setEnabled(true);
                  this.mSeriesIndices.add(Integer.valueOf(var10));
                  if(this.mDescriptor.seriesValueIndex != -1 && this.mDescriptor.seriesValueIndex == var10) {
                     var11 = var10;
                  }
               }

               ++var10;
            }

            if(this.mSeriesCombo.isEnabled()) {
               this.mSeriesCombo.add("default (pid)", 0);
               this.mSeriesIndices.add(0, Integer.valueOf(-1));
               this.mSeriesCombo.select(var11 + 1);
               if(var11 >= 0) {
                  this.mDisplayPidCheckBox.setSelection(this.mDescriptor.includePid);
                  this.mDisplayPidCheckBox.setEnabled(true);
               } else {
                  this.mDisplayPidCheckBox.setEnabled(false);
                  this.mDisplayPidCheckBox.setSelection(false);
               }
            } else {
               this.mDisplayPidCheckBox.setSelection(false);
               this.mDisplayPidCheckBox.setEnabled(false);
            }

            this.mFilterCombo.setEnabled(true);
            this.mFilterCombo.removeAll();
            this.mFilterCombo.add("(no filter)");
            var12 = values;
            len$ = values.length;

            for(var14 = 0; var14 < len$; ++var14) {
               value = var12[var14];
               this.mFilterCombo.add(value.toString());
            }

            this.mFilterCombo.select(this.mDescriptor.filterValueIndex + 1);
            this.mFilterMethodCombo.select(this.getFilterMethodIndex(this.mDescriptor.filterCompareMethod));
            if(this.mDescriptor.filterValueIndex != -1) {
               EventValueDescription var13 = values[this.mDescriptor.filterValueIndex];
               if(var13.checkForType(this.mDescriptor.filterValue)) {
                  this.mFilterValue.setText(this.mDescriptor.filterValue.toString());
               } else {
                  this.mFilterValue.setText("");
               }
            } else {
               this.mFilterValue.setText("");
            }
         } else {
            this.disableSubCombos();
         }
      } else {
         this.disableSubCombos();
      }

      this.checkValidity();
   }

   private void disableSubCombos() {
      this.mValueCombo.removeAll();
      this.mValueCombo.clearSelection();
      this.mValueCombo.setEnabled(false);
      this.mSeriesCombo.removeAll();
      this.mSeriesCombo.clearSelection();
      this.mSeriesCombo.setEnabled(false);
      this.mDisplayPidCheckBox.setEnabled(false);
      this.mDisplayPidCheckBox.setSelection(false);
      this.mFilterCombo.removeAll();
      this.mFilterCombo.clearSelection();
      this.mFilterCombo.setEnabled(false);
      this.mFilterValue.setEnabled(false);
      this.mFilterValue.setText("");
      this.mFilterMethodCombo.setEnabled(false);
   }

   private void handleValueComboSelection() {
      EventDisplay.ValueDisplayDescriptor valueDescriptor = (EventDisplay.ValueDisplayDescriptor)this.mDescriptor;
      int index = this.mValueCombo.getSelectionIndex();
      valueDescriptor.valueIndex = index;
      int eventIndex = this.mEventCombo.getSelectionIndex();
      int eventTag = this.mEventTags[eventIndex].intValue();
      EventValueDescription[] values = (EventValueDescription[])this.mLogParser.getEventInfoMap().get(Integer.valueOf(eventTag));
      valueDescriptor.valueName = values[index].getName();
      this.checkValidity();
   }

   private void handleSeriesComboSelection() {
      int index = this.mSeriesCombo.getSelectionIndex();
      int valueIndex = ((Integer)this.mSeriesIndices.get(index)).intValue();
      this.mDescriptor.seriesValueIndex = valueIndex;
      if(index > 0) {
         this.mDisplayPidCheckBox.setEnabled(true);
         this.mDisplayPidCheckBox.setSelection(this.mDescriptor.includePid);
      } else {
         this.mDisplayPidCheckBox.setSelection(false);
         this.mDisplayPidCheckBox.setEnabled(false);
      }

   }

   private void handleFilterComboSelection() {
      int index = this.mFilterCombo.getSelectionIndex();
      --index;
      this.mDescriptor.filterValueIndex = index;
      if(index != -1) {
         this.mFilterValue.setEnabled(true);
         this.mFilterMethodCombo.setEnabled(true);
         if(this.mDescriptor.filterValue instanceof String) {
            this.mFilterValue.setText((String)this.mDescriptor.filterValue);
         }
      } else {
         this.mFilterValue.setText("");
         this.mFilterValue.setEnabled(false);
         this.mFilterMethodCombo.setEnabled(false);
      }

   }

   private void handleFilterMethodComboSelection() {
      int index = this.mFilterMethodCombo.getSelectionIndex();
      CompareMethod method = CompareMethod.values()[index];
      this.mDescriptor.filterCompareMethod = method;
   }

   private int getFilterMethodIndex(CompareMethod filterCompareMethod) {
      CompareMethod[] values = CompareMethod.values();

      for(int i = 0; i < values.length; ++i) {
         if(values[i] == filterCompareMethod) {
            return i;
         }
      }

      return -1;
   }

   private void loadValueDescriptor() {
      int eventIndex = 0;
      int comboIndex = -1;
      Integer[] arr$ = this.mEventTags;
      int len$ = arr$.length;

      for(int i$ = 0; i$ < len$; ++i$) {
         int i = arr$[i$].intValue();
         if(i == this.mDescriptor.eventTag) {
            comboIndex = eventIndex;
            break;
         }

         ++eventIndex;
      }

      if(comboIndex == -1) {
         this.mEventCombo.clearSelection();
      } else {
         this.mEventCombo.select(comboIndex);
      }

      this.handleEventComboSelection();
   }

   private void checkValidity() {
      this.mOkButton.setEnabled(this.mEventCombo.getSelectionIndex() != -1 && (!(this.mDescriptor instanceof EventDisplay.ValueDisplayDescriptor) || this.mValueCombo.getSelectionIndex() != -1));
   }
}
