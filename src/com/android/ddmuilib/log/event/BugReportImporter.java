package com.android.ddmuilib.log.event;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class BugReportImporter {
   private static final String TAG_HEADER = "------ EVENT LOG TAGS ------";
   private static final String LOG_HEADER = "------ EVENT LOG ------";
   private static final String HEADER_TAG = "------";
   private String[] mTags;
   private String[] mLog;

   public BugReportImporter(String filePath) throws FileNotFoundException {
      BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(filePath)));

      while(true) {
         try {
            String ignore;
            if((ignore = reader.readLine()) != null) {
               if(!"------ EVENT LOG TAGS ------".equals(ignore)) {
                  continue;
               }

               this.readTags(reader);
               return;
            }
         } catch (IOException var14) {
            ;
         } finally {
            if(reader != null) {
               try {
                  reader.close();
               } catch (IOException var13) {
                  ;
               }
            }

         }

         return;
      }
   }

   public String[] getTags() {
      return this.mTags;
   }

   public String[] getLog() {
      return this.mLog;
   }

   private void readTags(BufferedReader reader) throws IOException {
      ArrayList content = new ArrayList();

      String line;
      while((line = reader.readLine()) != null) {
         if("------ EVENT LOG ------".equals(line)) {
            this.mTags = (String[])content.toArray(new String[content.size()]);
            this.readLog(reader);
            return;
         }

         content.add(line);
      }

   }

   private void readLog(BufferedReader reader) throws IOException {
      ArrayList content = new ArrayList();

      String line;
      while((line = reader.readLine()) != null && !line.startsWith("------")) {
         content.add(line);
      }

      this.mLog = (String[])content.toArray(new String[content.size()]);
   }
}
