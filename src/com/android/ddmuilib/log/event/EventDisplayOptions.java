package com.android.ddmuilib.log.event;

import com.android.ddmlib.log.EventContainer;
import com.android.ddmlib.log.EventLogParser;
import com.android.ddmlib.log.EventValueDescription;
import com.android.ddmuilib.DdmUiPreferences;
import com.android.ddmuilib.ImageLoader;
import com.android.ddmuilib.log.event.EventDisplay;
import com.android.ddmuilib.log.event.EventValueSelector;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.List;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

class EventDisplayOptions extends Dialog {
   private static final int DLG_WIDTH = 700;
   private static final int DLG_HEIGHT = 700;
   private Shell mParent;
   private Shell mShell;
   private boolean mEditStatus = false;
   private final ArrayList<EventDisplay> mDisplayList = new ArrayList();
   private List mEventDisplayList;
   private Button mEventDisplayNewButton;
   private Button mEventDisplayDeleteButton;
   private Button mEventDisplayUpButton;
   private Button mEventDisplayDownButton;
   private Text mDisplayWidthText;
   private Text mDisplayHeightText;
   private Text mDisplayNameText;
   private Combo mDisplayTypeCombo;
   private Group mChartOptions;
   private Group mHistOptions;
   private Button mPidFilterCheckBox;
   private Text mPidText;
   private Map<Integer, String> mEventTagMap;
   private Map<Integer, EventValueDescription[]> mEventDescriptionMap;
   private ArrayList<Integer> mPidList;
   private EventLogParser mLogParser;
   private Group mInfoGroup;
   private EventDisplayOptions.SelectionWidgets mValueSelection;
   private EventDisplayOptions.SelectionWidgets mOccurrenceSelection;
   private boolean mProcessTextChanges = true;
   private Text mTimeLimitText;
   private Text mHistWidthText;

   EventDisplayOptions(Shell parent) {
      super(parent, 67680);
   }

   boolean open(EventLogParser logParser, ArrayList<EventDisplay> displayList, ArrayList<EventContainer> eventList) {
      this.mLogParser = logParser;
      if(logParser != null) {
         this.mEventTagMap = logParser.getTagMap();
         this.mEventDescriptionMap = logParser.getEventInfoMap();
      }

      this.duplicateEventDisplay(displayList);
      this.buildPidList(eventList);
      this.createUI();
      if(this.mParent != null && this.mShell != null) {
         this.mShell.setMinimumSize(700, 700);
         Rectangle r = this.mParent.getBounds();
         int cx = r.x + r.width / 2;
         int x = cx - 350;
         int cy = r.y + r.height / 2;
         int y = cy - 350;
         this.mShell.setBounds(x, y, 700, 700);
         this.mShell.layout();
         this.mShell.open();
         Display display = this.mParent.getDisplay();

         while(!this.mShell.isDisposed()) {
            if(!display.readAndDispatch()) {
               display.sleep();
            }
         }

         return this.mEditStatus;
      } else {
         return false;
      }
   }

   ArrayList<EventDisplay> getEventDisplays() {
      return this.mDisplayList;
   }

   private void createUI() {
      this.mParent = this.getParent();
      this.mShell = new Shell(this.mParent, this.getStyle());
      this.mShell.setText("Event Display Configuration");
      this.mShell.setLayout(new GridLayout(1, true));
      Composite topPanel = new Composite(this.mShell, 0);
      topPanel.setLayoutData(new GridData(1808));
      topPanel.setLayout(new GridLayout(2, false));
      Composite leftPanel = new Composite(topPanel, 0);
      Composite rightPanel = new Composite(topPanel, 0);
      this.createLeftPanel(leftPanel);
      this.createRightPanel(rightPanel);
      this.mShell.addListener(21, new Listener() {
         public void handleEvent(Event event) {
            event.doit = true;
         }
      });
      Label separator = new Label(this.mShell, 258);
      separator.setLayoutData(new GridData(768));
      Composite bottomButtons = new Composite(this.mShell, 0);
      bottomButtons.setLayoutData(new GridData(768));
      GridLayout gl;
      bottomButtons.setLayout(gl = new GridLayout(2, true));
      gl.marginHeight = gl.marginWidth = 0;
      Button okButton = new Button(bottomButtons, 8);
      okButton.setText("OK");
      okButton.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            EventDisplayOptions.this.mShell.close();
         }
      });
      Button cancelButton = new Button(bottomButtons, 8);
      cancelButton.setText("Cancel");
      cancelButton.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            EventDisplayOptions.this.mEditStatus = false;
            EventDisplayOptions.this.mShell.close();
         }
      });
      this.enable(false);
      this.fillEventDisplayList();
   }

   private void createLeftPanel(Composite leftPanel) {
      final IPreferenceStore store = DdmUiPreferences.getStore();
      leftPanel.setLayoutData(new GridData(1040));
      GridLayout gl;
      leftPanel.setLayout(gl = new GridLayout(1, false));
      gl.verticalSpacing = 1;
      this.mEventDisplayList = new List(leftPanel, 68100);
      this.mEventDisplayList.setLayoutData(new GridData(1808));
      this.mEventDisplayList.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            EventDisplayOptions.this.handleEventDisplaySelection();
         }
      });
      Composite bottomControls = new Composite(leftPanel, 0);
      bottomControls.setLayoutData(new GridData(768));
      bottomControls.setLayout(gl = new GridLayout(5, false));
      gl.marginHeight = gl.marginWidth = 0;
      gl.verticalSpacing = 0;
      gl.horizontalSpacing = 0;
      ImageLoader loader = ImageLoader.getDdmUiLibLoader();
      this.mEventDisplayNewButton = new Button(bottomControls, 8388616);
      this.mEventDisplayNewButton.setImage(loader.loadImage("add.png", leftPanel.getDisplay()));
      this.mEventDisplayNewButton.setToolTipText("Adds a new event display");
      this.mEventDisplayNewButton.setLayoutData(new GridData(64));
      this.mEventDisplayNewButton.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            EventDisplayOptions.this.createNewEventDisplay();
         }
      });
      this.mEventDisplayDeleteButton = new Button(bottomControls, 8388616);
      this.mEventDisplayDeleteButton.setImage(loader.loadImage("delete.png", leftPanel.getDisplay()));
      this.mEventDisplayDeleteButton.setToolTipText("Deletes the selected event display");
      this.mEventDisplayDeleteButton.setLayoutData(new GridData(64));
      this.mEventDisplayDeleteButton.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            EventDisplayOptions.this.deleteEventDisplay();
         }
      });
      this.mEventDisplayUpButton = new Button(bottomControls, 8388616);
      this.mEventDisplayUpButton.setImage(loader.loadImage("up.png", leftPanel.getDisplay()));
      this.mEventDisplayUpButton.setToolTipText("Moves the selected event display up");
      this.mEventDisplayUpButton.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            int selection = EventDisplayOptions.this.mEventDisplayList.getSelectionIndex();
            if(selection > 0) {
               EventDisplay display = (EventDisplay)EventDisplayOptions.this.mDisplayList.remove(selection);
               EventDisplayOptions.this.mDisplayList.add(selection - 1, display);
               EventDisplayOptions.this.mEventDisplayList.remove(selection);
               EventDisplayOptions.this.mEventDisplayList.add(display.getName(), selection - 1);
               EventDisplayOptions.this.mEventDisplayList.select(selection - 1);
               EventDisplayOptions.this.handleEventDisplaySelection();
               EventDisplayOptions.this.mEventDisplayList.showSelection();
               EventDisplayOptions.this.setModified();
            }

         }
      });
      this.mEventDisplayDownButton = new Button(bottomControls, 8388616);
      this.mEventDisplayDownButton.setImage(loader.loadImage("down.png", leftPanel.getDisplay()));
      this.mEventDisplayDownButton.setToolTipText("Moves the selected event display down");
      this.mEventDisplayDownButton.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            int selection = EventDisplayOptions.this.mEventDisplayList.getSelectionIndex();
            if(selection != -1 && selection < EventDisplayOptions.this.mEventDisplayList.getItemCount() - 1) {
               EventDisplay display = (EventDisplay)EventDisplayOptions.this.mDisplayList.remove(selection);
               EventDisplayOptions.this.mDisplayList.add(selection + 1, display);
               EventDisplayOptions.this.mEventDisplayList.remove(selection);
               EventDisplayOptions.this.mEventDisplayList.add(display.getName(), selection + 1);
               EventDisplayOptions.this.mEventDisplayList.select(selection + 1);
               EventDisplayOptions.this.handleEventDisplaySelection();
               EventDisplayOptions.this.mEventDisplayList.showSelection();
               EventDisplayOptions.this.setModified();
            }

         }
      });
      Group sizeGroup = new Group(leftPanel, 0);
      sizeGroup.setText("Display Size:");
      sizeGroup.setLayoutData(new GridData(768));
      sizeGroup.setLayout(new GridLayout(2, false));
      Label l = new Label(sizeGroup, 0);
      l.setText("Width:");
      this.mDisplayWidthText = new Text(sizeGroup, 18436);
      this.mDisplayWidthText.setLayoutData(new GridData(768));
      this.mDisplayWidthText.setText(Integer.toString(store.getInt("EventLogPanel.width")));
      this.mDisplayWidthText.addModifyListener(new ModifyListener() {
         public void modifyText(ModifyEvent e) {
            String text = EventDisplayOptions.this.mDisplayWidthText.getText().trim();

            try {
               store.setValue("EventLogPanel.width", Integer.parseInt(text));
               EventDisplayOptions.this.setModified();
            } catch (NumberFormatException var4) {
               ;
            }

         }
      });
      l = new Label(sizeGroup, 0);
      l.setText("Height:");
      this.mDisplayHeightText = new Text(sizeGroup, 18436);
      this.mDisplayHeightText.setLayoutData(new GridData(768));
      this.mDisplayHeightText.setText(Integer.toString(store.getInt("EventLogPanel.height")));
      this.mDisplayHeightText.addModifyListener(new ModifyListener() {
         public void modifyText(ModifyEvent e) {
            String text = EventDisplayOptions.this.mDisplayHeightText.getText().trim();

            try {
               store.setValue("EventLogPanel.height", Integer.parseInt(text));
               EventDisplayOptions.this.setModified();
            } catch (NumberFormatException var4) {
               ;
            }

         }
      });
   }

   private void createRightPanel(Composite rightPanel) {
      rightPanel.setLayout(new GridLayout(1, true));
      rightPanel.setLayoutData(new GridData(1808));
      this.mInfoGroup = new Group(rightPanel, 0);
      this.mInfoGroup.setLayoutData(new GridData(768));
      this.mInfoGroup.setLayout(new GridLayout(2, false));
      Label nameLabel = new Label(this.mInfoGroup, 16384);
      nameLabel.setText("Name:");
      this.mDisplayNameText = new Text(this.mInfoGroup, 18436);
      this.mDisplayNameText.setLayoutData(new GridData(768));
      this.mDisplayNameText.addModifyListener(new ModifyListener() {
         public void modifyText(ModifyEvent e) {
            if(EventDisplayOptions.this.mProcessTextChanges) {
               EventDisplay eventDisplay = EventDisplayOptions.this.getCurrentEventDisplay();
               if(eventDisplay != null) {
                  eventDisplay.setName(EventDisplayOptions.this.mDisplayNameText.getText());
                  int index = EventDisplayOptions.this.mEventDisplayList.getSelectionIndex();
                  EventDisplayOptions.this.mEventDisplayList.remove(index);
                  EventDisplayOptions.this.mEventDisplayList.add(eventDisplay.getName(), index);
                  EventDisplayOptions.this.mEventDisplayList.select(index);
                  EventDisplayOptions.this.handleEventDisplaySelection();
                  EventDisplayOptions.this.setModified();
               }
            }

         }
      });
      Label displayLabel = new Label(this.mInfoGroup, 16384);
      displayLabel.setText("Type:");
      this.mDisplayTypeCombo = new Combo(this.mInfoGroup, 12);
      this.mDisplayTypeCombo.setLayoutData(new GridData(768));
      this.mDisplayTypeCombo.add("Log All");
      this.mDisplayTypeCombo.add("Filtered Log");
      this.mDisplayTypeCombo.add("Graph");
      this.mDisplayTypeCombo.add("Sync");
      this.mDisplayTypeCombo.add("Sync Histogram");
      this.mDisplayTypeCombo.add("Sync Performance");
      this.mDisplayTypeCombo.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            EventDisplay eventDisplay = EventDisplayOptions.this.getCurrentEventDisplay();
            if(eventDisplay != null && eventDisplay.getDisplayType() != EventDisplayOptions.this.mDisplayTypeCombo.getSelectionIndex()) {
               EventDisplayOptions.this.setModified();
               String name = eventDisplay.getName();
               EventDisplay newEventDisplay = EventDisplay.eventDisplayFactory(EventDisplayOptions.this.mDisplayTypeCombo.getSelectionIndex(), name);
               EventDisplayOptions.this.setCurrentEventDisplay(newEventDisplay);
               EventDisplayOptions.this.fillUiWith(newEventDisplay);
            }

         }
      });
      this.mChartOptions = new Group(this.mInfoGroup, 0);
      this.mChartOptions.setText("Chart Options");
      GridData gd;
      this.mChartOptions.setLayoutData(gd = new GridData(768));
      gd.horizontalSpan = 2;
      this.mChartOptions.setLayout(new GridLayout(2, false));
      Label l = new Label(this.mChartOptions, 0);
      l.setText("Time Limit (seconds):");
      this.mTimeLimitText = new Text(this.mChartOptions, 2048);
      this.mTimeLimitText.setLayoutData(new GridData(768));
      this.mTimeLimitText.addModifyListener(new ModifyListener() {
         public void modifyText(ModifyEvent arg0) {
            String text = EventDisplayOptions.this.mTimeLimitText.getText().trim();
            EventDisplay eventDisplay = EventDisplayOptions.this.getCurrentEventDisplay();
            if(eventDisplay != null) {
               try {
                  if(text.length() == 0) {
                     eventDisplay.resetChartTimeLimit();
                  } else {
                     eventDisplay.setChartTimeLimit(Long.parseLong(text));
                  }
               } catch (NumberFormatException var8) {
                  eventDisplay.resetChartTimeLimit();
               } finally {
                  EventDisplayOptions.this.setModified();
               }
            }

         }
      });
      this.mHistOptions = new Group(this.mInfoGroup, 0);
      this.mHistOptions.setText("Histogram Options");
      GridData gdh;
      this.mHistOptions.setLayoutData(gdh = new GridData(768));
      gdh.horizontalSpan = 2;
      this.mHistOptions.setLayout(new GridLayout(2, false));
      Label lh = new Label(this.mHistOptions, 0);
      lh.setText("Histogram width (hours):");
      this.mHistWidthText = new Text(this.mHistOptions, 2048);
      this.mHistWidthText.setLayoutData(new GridData(768));
      this.mHistWidthText.addModifyListener(new ModifyListener() {
         public void modifyText(ModifyEvent arg0) {
            String text = EventDisplayOptions.this.mHistWidthText.getText().trim();
            EventDisplay eventDisplay = EventDisplayOptions.this.getCurrentEventDisplay();
            if(eventDisplay != null) {
               try {
                  if(text.length() == 0) {
                     eventDisplay.resetHistWidth();
                  } else {
                     eventDisplay.setHistWidth(Long.parseLong(text));
                  }
               } catch (NumberFormatException var8) {
                  eventDisplay.resetHistWidth();
               } finally {
                  EventDisplayOptions.this.setModified();
               }
            }

         }
      });
      this.mPidFilterCheckBox = new Button(this.mInfoGroup, 32);
      this.mPidFilterCheckBox.setText("Enable filtering by pid");
      this.mPidFilterCheckBox.setLayoutData(gd = new GridData(768));
      gd.horizontalSpan = 2;
      this.mPidFilterCheckBox.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            EventDisplay eventDisplay = EventDisplayOptions.this.getCurrentEventDisplay();
            if(eventDisplay != null) {
               eventDisplay.setPidFiltering(EventDisplayOptions.this.mPidFilterCheckBox.getSelection());
               EventDisplayOptions.this.mPidText.setEnabled(EventDisplayOptions.this.mPidFilterCheckBox.getSelection());
               EventDisplayOptions.this.setModified();
            }

         }
      });
      Label pidLabel = new Label(this.mInfoGroup, 0);
      pidLabel.setText("Pid Filter:");
      pidLabel.setToolTipText("Enter all pids, separated by commas");
      this.mPidText = new Text(this.mInfoGroup, 2048);
      this.mPidText.setLayoutData(new GridData(768));
      this.mPidText.addModifyListener(new ModifyListener() {
         public void modifyText(ModifyEvent e) {
            if(EventDisplayOptions.this.mProcessTextChanges) {
               EventDisplay eventDisplay = EventDisplayOptions.this.getCurrentEventDisplay();
               if(eventDisplay != null && eventDisplay.getPidFiltering()) {
                  String pidText = EventDisplayOptions.this.mPidText.getText().trim();
                  String[] pids = pidText.split("\\s*,\\s*");
                  ArrayList list = new ArrayList();
                  String[] arr$ = pids;
                  int len$ = pids.length;

                  for(int i$ = 0; i$ < len$; ++i$) {
                     String pid = arr$[i$];

                     try {
                        list.add(Integer.valueOf(pid));
                     } catch (NumberFormatException var11) {
                        ;
                     }
                  }

                  eventDisplay.setPidFilterList(list);
                  EventDisplayOptions.this.setModified();
               }
            }

         }
      });
      this.mValueSelection = this.createEventSelection(rightPanel, EventDisplay.ValueDisplayDescriptor.class, "Event Value Display");
      this.mOccurrenceSelection = this.createEventSelection(rightPanel, EventDisplay.OccurrenceDisplayDescriptor.class, "Event Occurrence Display");
   }

   private EventDisplayOptions.SelectionWidgets createEventSelection(Composite rightPanel, final Class<? extends EventDisplay.OccurrenceDisplayDescriptor> descriptorClass, String groupMessage) {
      Group eventSelectionPanel = new Group(rightPanel, 0);
      eventSelectionPanel.setLayoutData(new GridData(1808));
      GridLayout gl;
      eventSelectionPanel.setLayout(gl = new GridLayout(2, false));
      gl.marginHeight = gl.marginWidth = 0;
      eventSelectionPanel.setText(groupMessage);
      final EventDisplayOptions.SelectionWidgets widgets = new EventDisplayOptions.SelectionWidgets(null);
      widgets.mList = new List(eventSelectionPanel, 2564);
      widgets.mList.setLayoutData(new GridData(1808));
      widgets.mList.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            int index = widgets.mList.getSelectionIndex();
            if(index != -1) {
               widgets.mDeleteButton.setEnabled(true);
               widgets.mEditButton.setEnabled(true);
            } else {
               widgets.mDeleteButton.setEnabled(false);
               widgets.mEditButton.setEnabled(false);
            }

         }
      });
      Composite rightControls = new Composite(eventSelectionPanel, 0);
      rightControls.setLayoutData(new GridData(1040));
      rightControls.setLayout(gl = new GridLayout(1, false));
      gl.marginHeight = gl.marginWidth = 0;
      gl.verticalSpacing = 0;
      gl.horizontalSpacing = 0;
      widgets.mNewButton = new Button(rightControls, 8388616);
      widgets.mNewButton.setText("New...");
      widgets.mNewButton.setLayoutData(new GridData(768));
      widgets.mNewButton.setEnabled(false);
      widgets.mNewButton.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            try {
               EventDisplay e1 = EventDisplayOptions.this.getCurrentEventDisplay();
               if(e1 != null) {
                  EventValueSelector dialog = new EventValueSelector(EventDisplayOptions.this.mShell);
                  if(dialog.open(descriptorClass, EventDisplayOptions.this.mLogParser)) {
                     e1.addDescriptor(dialog.getDescriptor());
                     EventDisplayOptions.this.fillUiWith(e1);
                     EventDisplayOptions.this.setModified();
                  }
               }
            } catch (Exception var4) {
               var4.printStackTrace();
            }

         }
      });
      widgets.mEditButton = new Button(rightControls, 8388616);
      widgets.mEditButton.setText("Edit...");
      widgets.mEditButton.setLayoutData(new GridData(768));
      widgets.mEditButton.setEnabled(false);
      widgets.mEditButton.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            EventDisplay eventDisplay = EventDisplayOptions.this.getCurrentEventDisplay();
            if(eventDisplay != null) {
               int index = widgets.mList.getSelectionIndex();
               if(index != -1) {
                  EventDisplay.OccurrenceDisplayDescriptor descriptor = eventDisplay.getDescriptor(descriptorClass, index);
                  EventValueSelector dialog = new EventValueSelector(EventDisplayOptions.this.mShell);
                  if(dialog.open(descriptor, EventDisplayOptions.this.mLogParser)) {
                     descriptor.replaceWith(dialog.getDescriptor());
                     eventDisplay.updateValueDescriptorCheck();
                     EventDisplayOptions.this.fillUiWith(eventDisplay);
                     widgets.mList.select(index);
                     widgets.mList.notifyListeners(13, (Event)null);
                     EventDisplayOptions.this.setModified();
                  }
               }
            }

         }
      });
      widgets.mDeleteButton = new Button(rightControls, 8388616);
      widgets.mDeleteButton.setLayoutData(new GridData(768));
      widgets.mDeleteButton.setText("Delete");
      widgets.mDeleteButton.setEnabled(false);
      widgets.mDeleteButton.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            EventDisplay eventDisplay = EventDisplayOptions.this.getCurrentEventDisplay();
            if(eventDisplay != null) {
               int index = widgets.mList.getSelectionIndex();
               if(index != -1) {
                  eventDisplay.removeDescriptor(descriptorClass, index);
                  EventDisplayOptions.this.fillUiWith(eventDisplay);
                  EventDisplayOptions.this.setModified();
               }
            }

         }
      });
      return widgets;
   }

   private void duplicateEventDisplay(ArrayList<EventDisplay> displayList) {
      Iterator i$ = displayList.iterator();

      while(i$.hasNext()) {
         EventDisplay eventDisplay = (EventDisplay)i$.next();
         this.mDisplayList.add(EventDisplay.clone(eventDisplay));
      }

   }

   private void buildPidList(ArrayList<EventContainer> eventList) {
      this.mPidList = new ArrayList();
      Iterator i$ = eventList.iterator();

      while(i$.hasNext()) {
         EventContainer event = (EventContainer)i$.next();
         if(this.mPidList.indexOf(Integer.valueOf(event.pid)) == -1) {
            this.mPidList.add(Integer.valueOf(event.pid));
         }
      }

   }

   private void setModified() {
      this.mEditStatus = true;
   }

   private void enable(boolean status) {
      this.mEventDisplayDeleteButton.setEnabled(status);
      int selection = this.mEventDisplayList.getSelectionIndex();
      int count = this.mEventDisplayList.getItemCount();
      this.mEventDisplayUpButton.setEnabled(status && selection > 0);
      this.mEventDisplayDownButton.setEnabled(status && selection != -1 && selection < count - 1);
      this.mDisplayNameText.setEnabled(status);
      this.mDisplayTypeCombo.setEnabled(status);
      this.mPidFilterCheckBox.setEnabled(status);
      this.mValueSelection.setEnabled(status);
      this.mOccurrenceSelection.setEnabled(status);
      this.mValueSelection.mNewButton.setEnabled(status);
      this.mOccurrenceSelection.mNewButton.setEnabled(status);
      if(!status) {
         this.mPidText.setEnabled(false);
      }

   }

   private void fillEventDisplayList() {
      Iterator i$ = this.mDisplayList.iterator();

      while(i$.hasNext()) {
         EventDisplay eventDisplay = (EventDisplay)i$.next();
         this.mEventDisplayList.add(eventDisplay.getName());
      }

   }

   private void createNewEventDisplay() {
      int count = this.mDisplayList.size();
      String name = String.format("display %1$d", new Object[]{Integer.valueOf(count + 1)});
      EventDisplay eventDisplay = EventDisplay.eventDisplayFactory(0, name);
      this.mDisplayList.add(eventDisplay);
      this.mEventDisplayList.add(name);
      this.mEventDisplayList.select(count);
      this.handleEventDisplaySelection();
      this.mEventDisplayList.showSelection();
      this.setModified();
   }

   private void deleteEventDisplay() {
      int selection = this.mEventDisplayList.getSelectionIndex();
      if(selection != -1) {
         this.mDisplayList.remove(selection);
         this.mEventDisplayList.remove(selection);
         if(this.mDisplayList.size() < selection) {
            --selection;
         }

         this.mEventDisplayList.select(selection);
         this.handleEventDisplaySelection();
         this.setModified();
      }

   }

   private EventDisplay getCurrentEventDisplay() {
      int selection = this.mEventDisplayList.getSelectionIndex();
      return selection != -1?(EventDisplay)this.mDisplayList.get(selection):null;
   }

   private void setCurrentEventDisplay(EventDisplay eventDisplay) {
      int selection = this.mEventDisplayList.getSelectionIndex();
      if(selection != -1) {
         this.mDisplayList.set(selection, eventDisplay);
      }

   }

   private void handleEventDisplaySelection() {
      EventDisplay eventDisplay = this.getCurrentEventDisplay();
      if(eventDisplay != null) {
         this.enable(true);
         this.fillUiWith(eventDisplay);
      } else {
         this.enable(false);
         this.emptyUi();
      }

   }

   private void emptyUi() {
      this.mDisplayNameText.setText("");
      this.mDisplayTypeCombo.clearSelection();
      this.mValueSelection.mList.removeAll();
      this.mOccurrenceSelection.mList.removeAll();
   }

   private void fillUiWith(EventDisplay eventDisplay) {
      this.mProcessTextChanges = false;
      this.mDisplayNameText.setText(eventDisplay.getName());
      int displayMode = eventDisplay.getDisplayType();
      this.mDisplayTypeCombo.select(displayMode);
      GridData valueIterator;
      long occurrenceIterator;
      if(displayMode == 2) {
         valueIterator = (GridData)this.mChartOptions.getLayoutData();
         valueIterator.exclude = false;
         this.mChartOptions.setVisible(!valueIterator.exclude);
         occurrenceIterator = eventDisplay.getChartTimeLimit();
         if(occurrenceIterator != -1L) {
            this.mTimeLimitText.setText(Long.toString(occurrenceIterator));
         } else {
            this.mTimeLimitText.setText("");
         }
      } else {
         valueIterator = (GridData)this.mChartOptions.getLayoutData();
         valueIterator.exclude = true;
         this.mChartOptions.setVisible(!valueIterator.exclude);
         this.mTimeLimitText.setText("");
      }

      if(displayMode == 4) {
         valueIterator = (GridData)this.mHistOptions.getLayoutData();
         valueIterator.exclude = false;
         this.mHistOptions.setVisible(!valueIterator.exclude);
         occurrenceIterator = eventDisplay.getHistWidth();
         if(occurrenceIterator != -1L) {
            this.mHistWidthText.setText(Long.toString(occurrenceIterator));
         } else {
            this.mHistWidthText.setText("");
         }
      } else {
         valueIterator = (GridData)this.mHistOptions.getLayoutData();
         valueIterator.exclude = true;
         this.mHistOptions.setVisible(!valueIterator.exclude);
         this.mHistWidthText.setText("");
      }

      this.mInfoGroup.layout(true);
      this.mShell.layout(true);
      this.mShell.pack();
      if(eventDisplay.getPidFiltering()) {
         this.mPidFilterCheckBox.setSelection(true);
         this.mPidText.setEnabled(true);
         ArrayList var7 = eventDisplay.getPidFilterList();
         if(var7 != null) {
            StringBuilder var8 = new StringBuilder();
            int descriptor = var7.size();

            for(int i = 0; i < descriptor; ++i) {
               var8.append(var7.get(i));
               if(i < descriptor - 1) {
                  var8.append(", ");
               }
            }

            this.mPidText.setText(var8.toString());
         } else {
            this.mPidText.setText("");
         }
      } else {
         this.mPidFilterCheckBox.setSelection(false);
         this.mPidText.setEnabled(false);
         this.mPidText.setText("");
      }

      this.mProcessTextChanges = true;
      this.mValueSelection.mList.removeAll();
      this.mOccurrenceSelection.mList.removeAll();
      if(eventDisplay.getDisplayType() != 1 && eventDisplay.getDisplayType() != 2) {
         this.mOccurrenceSelection.setEnabled(false);
         this.mValueSelection.setEnabled(false);
      } else {
         this.mOccurrenceSelection.setEnabled(true);
         this.mValueSelection.setEnabled(true);
         Iterator var9 = eventDisplay.getValueDescriptors();

         while(var9.hasNext()) {
            EventDisplay.ValueDisplayDescriptor var12 = (EventDisplay.ValueDisplayDescriptor)var9.next();
            this.mValueSelection.mList.add(String.format("%1$s: %2$s [%3$s]%4$s", new Object[]{this.mEventTagMap.get(Integer.valueOf(var12.eventTag)), var12.valueName, this.getSeriesLabelDescription(var12), this.getFilterDescription(var12)}));
         }

         Iterator var10 = eventDisplay.getOccurrenceDescriptors();

         while(var10.hasNext()) {
            EventDisplay.OccurrenceDisplayDescriptor var11 = (EventDisplay.OccurrenceDisplayDescriptor)var10.next();
            this.mOccurrenceSelection.mList.add(String.format("%1$s [%2$s]%3$s", new Object[]{this.mEventTagMap.get(Integer.valueOf(var11.eventTag)), this.getSeriesLabelDescription(var11), this.getFilterDescription(var11)}));
         }

         this.mValueSelection.mList.notifyListeners(13, (Event)null);
         this.mOccurrenceSelection.mList.notifyListeners(13, (Event)null);
      }

   }

   private String getSeriesLabelDescription(EventDisplay.OccurrenceDisplayDescriptor descriptor) {
      return descriptor.seriesValueIndex != -1?(descriptor.includePid?String.format("%1$s + pid", new Object[]{((EventValueDescription[])this.mEventDescriptionMap.get(Integer.valueOf(descriptor.eventTag)))[descriptor.seriesValueIndex].getName()}):((EventValueDescription[])this.mEventDescriptionMap.get(Integer.valueOf(descriptor.eventTag)))[descriptor.seriesValueIndex].getName()):"pid";
   }

   private String getFilterDescription(EventDisplay.OccurrenceDisplayDescriptor descriptor) {
      return descriptor.filterValueIndex != -1?String.format(" [%1$s %2$s %3$s]", new Object[]{((EventValueDescription[])this.mEventDescriptionMap.get(Integer.valueOf(descriptor.eventTag)))[descriptor.filterValueIndex].getName(), descriptor.filterCompareMethod.testString(), descriptor.filterValue != null?descriptor.filterValue.toString():"?"}):"";
   }

   private static class SelectionWidgets {
      private List mList;
      private Button mNewButton;
      private Button mEditButton;
      private Button mDeleteButton;

      private SelectionWidgets() {
      }

      private void setEnabled(boolean enable) {
         this.mList.setEnabled(enable);
         this.mNewButton.setEnabled(enable);
         this.mEditButton.setEnabled(enable);
         this.mDeleteButton.setEnabled(enable);
      }

      // $FF: synthetic method
      SelectionWidgets(Object x0) {
         this();
      }
   }
}
