package com.android.ddmuilib.log.event;

import com.android.ddmlib.log.EventContainer;
import com.android.ddmlib.log.EventLogParser;
import com.android.ddmlib.log.InvalidTypeException;
import com.android.ddmuilib.log.event.EventDisplay;
import com.android.ddmuilib.log.event.SyncCommon;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.jfree.chart.labels.CustomXYToolTipGenerator;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.FixedMillisecond;
import org.jfree.data.time.SimpleTimePeriod;
import org.jfree.data.time.TimePeriodValues;
import org.jfree.data.time.TimePeriodValuesCollection;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.util.ShapeUtilities;

public class DisplaySync extends SyncCommon {
   private TimePeriodValues[] mDatasetsSync;
   private List<String>[] mTooltipsSync;
   private CustomXYToolTipGenerator[] mTooltipGenerators;
   private TimeSeries[] mDatasetsSyncTickle;
   private TimeSeries mDatasetError;

   public DisplaySync(String name) {
      super(name);
   }

   public Control createComposite(Composite parent, EventLogParser logParser, EventDisplay.ILogColumnListener listener) {
      Control composite = this.createCompositeChart(parent, logParser, "Sync Status");
      this.resetUI();
      return composite;
   }

   void resetUI() {
      super.resetUI();
      XYPlot xyPlot = this.mChart.getXYPlot();
      XYBarRenderer br = new XYBarRenderer();
      this.mDatasetsSync = new TimePeriodValues[4];
      List[] mTooltipsSyncTmp = new List[4];
      this.mTooltipsSync = mTooltipsSyncTmp;
      this.mTooltipGenerators = new CustomXYToolTipGenerator[4];
      TimePeriodValuesCollection tpvc = new TimePeriodValuesCollection();
      xyPlot.setDataset(tpvc);
      xyPlot.setRenderer(0, br);
      XYLineAndShapeRenderer ls = new XYLineAndShapeRenderer();
      ls.setBaseLinesVisible(false);
      this.mDatasetsSyncTickle = new TimeSeries[4];
      TimeSeriesCollection tsc = new TimeSeriesCollection();
      xyPlot.setDataset(1, tsc);
      xyPlot.setRenderer(1, ls);
      this.mDatasetError = new TimeSeries("Errors", FixedMillisecond.class);
      xyPlot.setDataset(2, new TimeSeriesCollection(this.mDatasetError));
      XYLineAndShapeRenderer errls = new XYLineAndShapeRenderer();
      errls.setBaseLinesVisible(false);
      errls.setSeriesPaint(0, Color.RED);
      xyPlot.setRenderer(2, errls);

      for(int i = 0; i < 4; ++i) {
         br.setSeriesPaint(i, AUTH_COLORS[i]);
         ls.setSeriesPaint(i, AUTH_COLORS[i]);
         this.mDatasetsSync[i] = new TimePeriodValues(AUTH_NAMES[i]);
         tpvc.addSeries(this.mDatasetsSync[i]);
         this.mTooltipsSync[i] = new ArrayList();
         this.mTooltipGenerators[i] = new CustomXYToolTipGenerator();
         br.setSeriesToolTipGenerator(i, this.mTooltipGenerators[i]);
         this.mTooltipGenerators[i].addToolTipSeries(this.mTooltipsSync[i]);
         this.mDatasetsSyncTickle[i] = new TimeSeries(AUTH_NAMES[i] + " tickle", FixedMillisecond.class);
         tsc.addSeries(this.mDatasetsSyncTickle[i]);
         ls.setSeriesShape(i, ShapeUtilities.createUpTriangle(2.5F));
      }

   }

   void newEvent(EventContainer event, EventLogParser logParser) {
      super.newEvent(event, logParser);

      try {
         if(event.mTag == 2742) {
            int e = this.getAuth(event.getValueAsString(0));
            if(e >= 0) {
               long msec = (long)event.sec * 1000L + (long)event.nsec / 1000000L;
               this.mDatasetsSyncTickle[e].addOrUpdate(new FixedMillisecond(msec), -1.0D);
            }
         }
      } catch (InvalidTypeException var6) {
         ;
      }

   }

   private int getHeightFromDetails(String details) {
      if(details == null) {
         return 1;
      } else {
         int total = 0;
         String[] parts = details.split("[a-zA-Z]");
         String[] arr$ = parts;
         int len$ = parts.length;

         for(int i$ = 0; i$ < len$; ++i$) {
            String part = arr$[i$];
            if(!"".equals(part)) {
               total += Integer.parseInt(part);
            }
         }

         if(total == 0) {
            total = 1;
         }

         return total;
      }
   }

   private String getTextFromDetails(int auth, String details, int eventSource) {
      StringBuffer sb = new StringBuffer();
      sb.append(AUTH_NAMES[auth]).append(": \n");
      Scanner scanner = new Scanner(details);
      Pattern charPat = Pattern.compile("[a-zA-Z]");
      Pattern numPat = Pattern.compile("[0-9]+");

      while(true) {
         while(scanner.hasNext()) {
            String key = scanner.findInLine(charPat);
            int val = Integer.parseInt(scanner.findInLine(numPat));
            if(auth == 1 && "M".equals(key)) {
               sb.append("messages from server: ").append(val).append("\n");
            } else if(auth == 1 && "L".equals(key)) {
               sb.append("labels from server: ").append(val).append("\n");
            } else if(auth == 1 && "C".equals(key)) {
               sb.append("check conversation requests from server: ").append(val).append("\n");
            } else if(auth == 1 && "A".equals(key)) {
               sb.append("attachments from server: ").append(val).append("\n");
            } else if(auth == 1 && "U".equals(key)) {
               sb.append("op updates from server: ").append(val).append("\n");
            } else if(auth == 1 && "u".equals(key)) {
               sb.append("op updates to server: ").append(val).append("\n");
            } else if(auth == 1 && "S".equals(key)) {
               sb.append("send/receive cycles: ").append(val).append("\n");
            } else if("Q".equals(key)) {
               sb.append("queries to server: ").append(val).append("\n");
            } else if("E".equals(key)) {
               sb.append("entries from server: ").append(val).append("\n");
            } else if("u".equals(key)) {
               sb.append("updates from client: ").append(val).append("\n");
            } else if("i".equals(key)) {
               sb.append("inserts from client: ").append(val).append("\n");
            } else if("d".equals(key)) {
               sb.append("deletes from client: ").append(val).append("\n");
            } else if("f".equals(key)) {
               sb.append("full sync requested\n");
            } else if("r".equals(key)) {
               sb.append("partial sync unavailable\n");
            } else if("X".equals(key)) {
               sb.append("hard error\n");
            } else if("e".equals(key)) {
               sb.append("number of parse exceptions: ").append(val).append("\n");
            } else if("c".equals(key)) {
               sb.append("number of conflicts: ").append(val).append("\n");
            } else if("a".equals(key)) {
               sb.append("number of auth exceptions: ").append(val).append("\n");
            } else if("D".equals(key)) {
               sb.append("too many deletions\n");
            } else if("R".equals(key)) {
               sb.append("too many retries: ").append(val).append("\n");
            } else if("b".equals(key)) {
               sb.append("database error\n");
            } else if("x".equals(key)) {
               sb.append("soft error\n");
            } else if("l".equals(key)) {
               sb.append("sync already in progress\n");
            } else if("I".equals(key)) {
               sb.append("io exception\n");
            } else if(auth == 3 && "g".equals(key)) {
               sb.append("aggregation query: ").append(val).append("\n");
            } else if(auth == 3 && "G".equals(key)) {
               sb.append("aggregation merge: ").append(val).append("\n");
            } else if(auth == 3 && "n".equals(key)) {
               sb.append("num entries: ").append(val).append("\n");
            } else if(auth == 3 && "p".equals(key)) {
               sb.append("photos uploaded from server: ").append(val).append("\n");
            } else if(auth == 3 && "P".equals(key)) {
               sb.append("photos downloaded from server: ").append(val).append("\n");
            } else if(auth == 0 && "F".equals(key)) {
               sb.append("server refresh\n");
            } else if(auth == 0 && "s".equals(key)) {
               sb.append("server diffs fetched\n");
            } else {
               sb.append(key).append("=").append(val);
            }
         }

         if(eventSource == 0) {
            sb.append("(server)");
         } else if(eventSource == 1) {
            sb.append("(local)");
         } else if(eventSource == 2) {
            sb.append("(poll)");
         } else if(eventSource == 3) {
            sb.append("(user)");
         }

         scanner.close();
         return sb.toString();
      }
   }

   void processSyncEvent(EventContainer event, int auth, long startTime, long stopTime, String details, boolean newEvent, int syncSource) {
      if(!newEvent) {
         int height = this.mDatasetsSync[auth].getItemCount();
         this.mDatasetsSync[auth].delete(height - 1, height - 1);
         this.mTooltipsSync[auth].remove(height - 1);
      }

      double height1 = (double)this.getHeightFromDetails(details);
      height1 = height1 / (double)(stopTime - startTime + 1L) * 10000.0D;
      if(height1 > 30.0D) {
         height1 = 30.0D;
      }

      this.mDatasetsSync[auth].add(new SimpleTimePeriod(startTime, stopTime), height1);
      this.mTooltipsSync[auth].add(this.getTextFromDetails(auth, details, syncSource));
      this.mTooltipGenerators[auth].addToolTipSeries(this.mTooltipsSync[auth]);
      if(details.indexOf(120) >= 0 || details.indexOf(88) >= 0) {
         long msec = (long)event.sec * 1000L + (long)event.nsec / 1000000L;
         this.mDatasetError.addOrUpdate(new FixedMillisecond(msec), -1.0D);
      }

   }

   int getDisplayType() {
      return 3;
   }
}
