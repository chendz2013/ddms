package com.android.ddmuilib.explorer;

import com.android.ddmlib.AdbCommandRejectedException;
import com.android.ddmlib.FileListingService;
import com.android.ddmlib.IDevice;
import com.android.ddmlib.IShellOutputReceiver;
import com.android.ddmlib.ShellCommandUnresponsiveException;
import com.android.ddmlib.SyncException;
import com.android.ddmlib.SyncService;
import com.android.ddmlib.TimeoutException;
import com.android.ddmlib.FileListingService.FileEntry;
import com.android.ddmlib.SyncService.ISyncProgressMonitor;
import com.android.ddmuilib.DdmUiPreferences;
import com.android.ddmuilib.ImageLoader;
import com.android.ddmuilib.Panel;
import com.android.ddmuilib.SyncProgressHelper;
import com.android.ddmuilib.TableHelper;
import com.android.ddmuilib.actions.ICommonAction;
import com.android.ddmuilib.console.DdmConsole;
import com.android.ddmuilib.explorer.DeviceContentProvider;
import com.android.ddmuilib.explorer.FileLabelProvider;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.ViewerDropAdapter;
import org.eclipse.swt.dnd.FileTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.dnd.TransferData;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

/**
 * 设备文件浏览器
 */
public class DeviceExplorer extends Panel {
   private static final String TRACE_KEY_EXT = ".key";
   private static final String TRACE_DATA_EXT = ".data";
   private static Pattern mKeyFilePattern = Pattern.compile("(.+)\\.key");
   private static Pattern mDataFilePattern = Pattern.compile("(.+)\\.data");
   public static String COLUMN_NAME = "android.explorer.name";
   public static String COLUMN_SIZE = "android.explorer.size";
   public static String COLUMN_DATE = "android.explorer.data";
   public static String COLUMN_TIME = "android.explorer.time";
   public static String COLUMN_PERMISSIONS = "android.explorer.permissions";
   public static String COLUMN_INFO = "android.explorer.info";
   private Composite mParent;
   private TreeViewer mTreeViewer;
   private Tree mTree;
   private DeviceContentProvider mContentProvider;
   private ICommonAction mPushAction;
   private ICommonAction mPullAction;
   private ICommonAction mDeleteAction;
   private ICommonAction mCreateNewFolderAction;
   private Image mFileImage;
   private Image mFolderImage;
   private Image mPackageImage;
   private Image mOtherImage;
   private IDevice mCurrentDevice;
   private String mDefaultSave;

   public void setCustomImages(Image fileImage, Image folderImage, Image packageImage, Image otherImage) {
      this.mFileImage = fileImage;
      this.mFolderImage = folderImage;
      this.mPackageImage = packageImage;
      this.mOtherImage = otherImage;
   }

   public void setActions(ICommonAction pushAction, ICommonAction pullAction, ICommonAction deleteAction, ICommonAction createNewFolderAction) {
      this.mPushAction = pushAction;
      this.mPullAction = pullAction;
      this.mDeleteAction = deleteAction;
      this.mCreateNewFolderAction = createNewFolderAction;
   }

   protected Control createControl(Composite parent) {
      this.mParent = parent;
      parent.setLayout(new FillLayout());
      ImageLoader loader = ImageLoader.getDdmUiLibLoader();
      if(this.mFileImage == null) {
         this.mFileImage = loader.loadImage("file.png", this.mParent.getDisplay());
      }

      if(this.mFolderImage == null) {
         this.mFolderImage = loader.loadImage("folder.png", this.mParent.getDisplay());
      }

      if(this.mPackageImage == null) {
         this.mPackageImage = loader.loadImage("android.png", this.mParent.getDisplay());
      }

      if(this.mOtherImage == null) {
         ;
      }

      this.mTree = new Tree(parent, 268500994);
      this.mTree.setHeaderVisible(true);
      IPreferenceStore store = DdmUiPreferences.getStore();
      TableHelper.createTreeColumn(this.mTree, "Name", 16384, "0000drwxrwxrwx", COLUMN_NAME, store);
      TableHelper.createTreeColumn(this.mTree, "Size", 131072, "000000", COLUMN_SIZE, store);
      TableHelper.createTreeColumn(this.mTree, "Date", 16384, "2007-08-14", COLUMN_DATE, store);
      TableHelper.createTreeColumn(this.mTree, "Time", 16384, "20:54", COLUMN_TIME, store);
      TableHelper.createTreeColumn(this.mTree, "Permissions", 16384, "drwxrwxrwx", COLUMN_PERMISSIONS, store);
      TableHelper.createTreeColumn(this.mTree, "Info", 16384, "drwxrwxrwx", COLUMN_INFO, store);
      this.mTreeViewer = new TreeViewer(this.mTree);
      this.mContentProvider = new DeviceContentProvider();
      this.mTreeViewer.setContentProvider(this.mContentProvider);
      this.mTreeViewer.setLabelProvider(new FileLabelProvider(this.mFileImage, this.mFolderImage, this.mPackageImage, this.mOtherImage));
      this.mTreeViewer.addSelectionChangedListener(new ISelectionChangedListener() {
         public void selectionChanged(SelectionChangedEvent event) {
            ISelection sel = event.getSelection();
            if(sel.isEmpty()) {
               DeviceExplorer.this.mPullAction.setEnabled(false);
               DeviceExplorer.this.mPushAction.setEnabled(false);
               DeviceExplorer.this.mDeleteAction.setEnabled(false);
               DeviceExplorer.this.mCreateNewFolderAction.setEnabled(false);
            } else {
               if(sel instanceof IStructuredSelection) {
                  IStructuredSelection selection = (IStructuredSelection)sel;
                  Object element = selection.getFirstElement();
                  if(element == null) {
                     return;
                  }

                  if(element instanceof FileEntry) {
                     DeviceExplorer.this.mPullAction.setEnabled(true);
                     DeviceExplorer.this.mPushAction.setEnabled(selection.size() == 1);
                     if(selection.size() == 1) {
                        FileEntry entry = (FileEntry)element;
                        DeviceExplorer.this.setDeleteEnabledState(entry);
                        DeviceExplorer.this.mCreateNewFolderAction.setEnabled(entry.isDirectory());
                     } else {
                        DeviceExplorer.this.mDeleteAction.setEnabled(false);
                     }
                  }
               }

            }
         }
      });
      this.mTreeViewer.addDoubleClickListener(new IDoubleClickListener() {
         public void doubleClick(DoubleClickEvent event) {
            ISelection sel = event.getSelection();
            if(sel instanceof IStructuredSelection) {
               IStructuredSelection selection = (IStructuredSelection)sel;
               if(selection.size() == 1) {
                  FileEntry entry = (FileEntry)selection.getFirstElement();
                  String name = entry.getName();
                  FileEntry parentEntry = entry.getParent();
                  if(parentEntry == null) {
                     return;
                  }

                  Matcher m = DeviceExplorer.mKeyFilePattern.matcher(name);
                  String baseName;
                  String keyName;
                  FileEntry keyEntry;
                  if(m.matches()) {
                     baseName = m.group(1);
                     keyName = baseName + ".data";
                     keyEntry = parentEntry.findChild(keyName);
                     DeviceExplorer.this.handleTraceDoubleClick(baseName, entry, keyEntry);
                  } else {
                     m = DeviceExplorer.mDataFilePattern.matcher(name);
                     if(m.matches()) {
                        baseName = m.group(1);
                        keyName = baseName + ".key";
                        keyEntry = parentEntry.findChild(keyName);
                        DeviceExplorer.this.handleTraceDoubleClick(baseName, keyEntry, entry);
                     }
                  }
               }
            }

         }
      });
      this.mTreeViewer.addDropSupport(3, new Transfer[]{FileTransfer.getInstance()}, new ViewerDropAdapter(this.mTreeViewer) {
         public boolean performDrop(Object data) {
            FileEntry target = (FileEntry)this.getCurrentTarget();
            if(target == null) {
               return false;
            } else {
               if(!target.isDirectory()) {
                  target = target.getParent();
               }

               if(target == null) {
                  return false;
               } else {
                  String[] files = (String[])((String[])data);
                  DeviceExplorer.this.pushFiles(files, target);
                  DeviceExplorer.this.refresh(target);
                  return true;
               }
            }
         }

         public boolean validateDrop(Object target, int operation, TransferData transferType) {
            if(target == null) {
               return false;
            } else {
               FileEntry targetEntry = (FileEntry)target;
               if(!targetEntry.isDirectory()) {
                  target = targetEntry.getParent();
               }

               return target != null;
            }
         }
      });
      (new Thread("Device Ls refresher") {
         public void run() {
            while(true) {
               try {
                  sleep(5000L);
               } catch (InterruptedException var2) {
                  return;
               }

               if(DeviceExplorer.this.mTree != null && !DeviceExplorer.this.mTree.isDisposed()) {
                  Display display = DeviceExplorer.this.mTree.getDisplay();
                  if(!display.isDisposed()) {
                     display.asyncExec(new Runnable() {
                        public void run() {
                           if(!DeviceExplorer.this.mTree.isDisposed()) {
                              DeviceExplorer.this.mTreeViewer.refresh(true);
                           }

                        }
                     });
                     continue;
                  }

                  return;
               }

               return;
            }
         }
      }).start();
      return this.mTree;
   }

   protected void postCreation() {
   }

   public void setFocus() {
      this.mTree.setFocus();
   }

   private void handleTraceDoubleClick(String baseName, FileEntry keyEntry, FileEntry dataEntry) {
      File keyFile;
      File dataFile;
      String path;
      try {
         File e = File.createTempFile(baseName, ".trace");
         e.delete();
         e.mkdir();
         path = e.getAbsolutePath();
         keyFile = new File(path + File.separator + keyEntry.getName());
         dataFile = new File(path + File.separator + dataEntry.getName());
      } catch (IOException var12) {
         return;
      }

      try {
         SyncService e2 = this.mCurrentDevice.getSyncService();
         if(e2 != null) {
            ISyncProgressMonitor monitor = SyncService.getNullProgressMonitor();
            e2.pullFile(keyEntry, keyFile.getAbsolutePath(), monitor);
            e2.pullFile(dataEntry, dataFile.getAbsolutePath(), monitor);
            String[] command = new String[]{DdmUiPreferences.getTraceview(), path + File.separator + baseName};

            try {
               final Process e1 = Runtime.getRuntime().exec(command);
               (new Thread("Traceview output") {
                  public void run() {
                     InputStreamReader is = new InputStreamReader(e1.getErrorStream());
                     BufferedReader resultReader = new BufferedReader(is);

                     try {
                        while(true) {
                           String e = resultReader.readLine();
                           if(e == null) {
                              e1.waitFor();
                              break;
                           }

                           DdmConsole.printErrorToConsole("Traceview: " + e);
                        }
                     } catch (IOException var4) {
                        ;
                     } catch (InterruptedException var5) {
                        ;
                     }

                  }
               }).start();
            } catch (IOException var11) {
               ;
            }
         }
      } catch (IOException var13) {
         DdmConsole.printErrorToConsole(String.format("Failed to pull %1$s: %2$s", new Object[]{keyEntry.getName(), var13.getMessage()}));
         return;
      } catch (SyncException var14) {
         if(!var14.wasCanceled()) {
            DdmConsole.printErrorToConsole(String.format("Failed to pull %1$s: %2$s", new Object[]{keyEntry.getName(), var14.getMessage()}));
            return;
         }
      } catch (TimeoutException var15) {
         DdmConsole.printErrorToConsole(String.format("Failed to pull %1$s: timeout", new Object[]{keyEntry.getName()}));
      } catch (AdbCommandRejectedException var16) {
         DdmConsole.printErrorToConsole(String.format("Failed to pull %1$s: %2$s", new Object[]{keyEntry.getName(), var16.getMessage()}));
      }

   }

   public void pullSelection() {
      TreeItem[] items = this.mTree.getSelection();
      String filePullName = null;
      FileEntry singleEntry = null;
      if(items.length == 1) {
         singleEntry = (FileEntry)items[0].getData();
         if(singleEntry.getType() == 0) {
            filePullName = singleEntry.getName();
         }
      }

      String defaultPath = this.mDefaultSave;
      if(defaultPath == null) {
         defaultPath = System.getProperty("user.home");
      }

      String directoryName;
      if(filePullName != null) {
         FileDialog directoryDialog = new FileDialog(this.mParent.getShell(), 8192);
         directoryDialog.setText("Get Device File");
         directoryDialog.setFileName(filePullName);
         directoryDialog.setFilterPath(defaultPath);
         directoryName = directoryDialog.open();
         if(directoryName != null) {
            this.mDefaultSave = directoryDialog.getFilterPath();
            this.pullFile(singleEntry, directoryName);
         }
      } else {
         DirectoryDialog directoryDialog1 = new DirectoryDialog(this.mParent.getShell(), 8192);
         directoryDialog1.setText("Get Device Files/Folders");
         directoryDialog1.setFilterPath(defaultPath);
         directoryName = directoryDialog1.open();
         if(directoryName != null) {
            this.pullSelection(items, directoryName);
         }
      }

   }

   public void pushIntoSelection() {
      TreeItem[] items = this.mTree.getSelection();
      if(items.length != 0) {
         FileDialog dlg = new FileDialog(this.mParent.getShell(), 4096);
         dlg.setText("Put File on Device");
         FileEntry entry = (FileEntry)items[0].getData();
         dlg.setFileName(entry.getName());
         String defaultPath = this.mDefaultSave;
         if(defaultPath == null) {
            defaultPath = System.getProperty("user.home");
         }

         dlg.setFilterPath(defaultPath);
         String fileName = dlg.open();
         if(fileName != null) {
            this.mDefaultSave = dlg.getFilterPath();
            FileEntry toRefresh = entry;
            String remotePath;
            if(entry.isDirectory()) {
               remotePath = entry.getFullPath();
            } else {
               toRefresh = entry.getParent();
               remotePath = toRefresh.getFullPath();
            }

            this.pushFile(fileName, remotePath);
            this.mTreeViewer.refresh(toRefresh);
         }

      }
   }

   public void deleteSelection() {
      TreeItem[] items = this.mTree.getSelection();
      if(items.length == 1) {
         FileEntry entry = (FileEntry)items[0].getData();
         final FileEntry parentEntry = entry.getParent();
         String command = "rm " + entry.getFullEscapedPath();

         try {
            this.mCurrentDevice.executeShellCommand(command, new IShellOutputReceiver() {
               public void addOutput(byte[] data, int offset, int length) {
               }

               public void flush() {
                  DeviceExplorer.this.mTreeViewer.refresh(parentEntry);
               }

               public boolean isCancelled() {
                  return false;
               }
            });
         } catch (IOException var6) {
            ;
         } catch (TimeoutException var7) {
            ;
         } catch (AdbCommandRejectedException var8) {
            ;
         } catch (ShellCommandUnresponsiveException var9) {
            ;
         }

      }
   }

   public void createNewFolderInSelection() {
      TreeItem[] items = this.mTree.getSelection();
      if(items.length == 1) {
         final FileEntry entry = (FileEntry)items[0].getData();
         if(entry.isDirectory()) {
            InputDialog inputDialog = new InputDialog(this.mTree.getShell(), "New Folder", "Please enter the new folder name", "New Folder", new IInputValidator() {
               public String isValid(String newText) {
                  return newText != null && newText.length() > 0 && newText.trim().length() > 0 && newText.indexOf(47) == -1 && newText.indexOf(92) == -1?null:"Invalid name";
               }
            });
            inputDialog.open();
            String value = inputDialog.getValue();
            if(value != null) {
               String command = "mkdir " + entry.getFullEscapedPath() + "/" + FileEntry.escape(value);

               try {
                  this.mCurrentDevice.executeShellCommand(command, new IShellOutputReceiver() {
                     public boolean isCancelled() {
                        return false;
                     }

                     public void flush() {
                        DeviceExplorer.this.mTreeViewer.refresh(entry);
                     }

                     public void addOutput(byte[] data, int offset, int length) {
                        String errorMessage;
                        if(data != null) {
                           errorMessage = new String(data);
                        } else {
                           errorMessage = "";
                        }

                        Status status = new Status(4, "DeviceExplorer", 0, errorMessage, (Throwable)null);
                        ErrorDialog.openError(DeviceExplorer.this.mTree.getShell(), "New Folder Error", "New Folder Error", status);
                     }
                  });
               } catch (TimeoutException var7) {
                  ;
               } catch (AdbCommandRejectedException var8) {
                  ;
               } catch (ShellCommandUnresponsiveException var9) {
                  ;
               } catch (IOException var10) {
                  ;
               }
            }
         }

      }
   }

   public void refresh() {
      this.mTreeViewer.refresh(true);
   }

   public void switchDevice(IDevice device) {
      if(device != this.mCurrentDevice) {
         this.mCurrentDevice = device;
         if(!this.mTree.isDisposed()) {
            Display d = this.mTree.getDisplay();
            d.asyncExec(new Runnable() {
               public void run() {
                  if(!DeviceExplorer.this.mTree.isDisposed() && DeviceExplorer.this.mCurrentDevice != null) {
                     FileListingService fls = DeviceExplorer.this.mCurrentDevice.getFileListingService();
                     DeviceExplorer.this.mContentProvider.setListingService(fls);
                     DeviceExplorer.this.mTreeViewer.setInput(fls.getRoot());
                  }

               }
            });
         }
      }

   }

   private void refresh(final FileEntry entry) {
      Display d = this.mTreeViewer.getTree().getDisplay();
      d.asyncExec(new Runnable() {
         public void run() {
            DeviceExplorer.this.mTreeViewer.refresh(entry);
         }
      });
   }

   private void pullSelection(TreeItem[] items, final String localDirectory) {
      try {
         final SyncService e = this.mCurrentDevice.getSyncService();
         if(e != null) {
            ArrayList entries = new ArrayList();
            TreeItem[] entryArray = items;
            int len$ = items.length;

            for(int i$ = 0; i$ < len$; ++i$) {
               TreeItem item = entryArray[i$];
               Object data = item.getData();
               if(data instanceof FileEntry) {
                  entries.add((FileEntry)data);
               }
            }

            final FileEntry[] var12 = (FileEntry[])entries.toArray(new FileEntry[entries.size()]);
            SyncProgressHelper.run(new SyncProgressHelper.SyncRunnable() {
               public void run(ISyncProgressMonitor monitor) throws SyncException, IOException, TimeoutException {
                  e.pull(var12, localDirectory, monitor);
               }

               public void close() {
                  e.close();
               }
            }, "Pulling file(s) from the device", this.mParent.getShell());
         }
      } catch (SyncException var10) {
         if(!var10.wasCanceled()) {
            DdmConsole.printErrorToConsole(String.format("Failed to pull selection: %1$s", new Object[]{var10.getMessage()}));
         }
      } catch (Exception var11) {
         DdmConsole.printErrorToConsole("Failed to pull selection");
         DdmConsole.printErrorToConsole(var11.getMessage());
      }

   }

   private void pullFile(final FileEntry remote, final String local) {
      try {
         final SyncService e = this.mCurrentDevice.getSyncService();
         if(e != null) {
            SyncProgressHelper.run(new SyncProgressHelper.SyncRunnable() {
               public void run(ISyncProgressMonitor monitor) throws SyncException, IOException, TimeoutException {
                  e.pullFile(remote, local, monitor);
               }

               public void close() {
                  e.close();
               }
            }, String.format("Pulling %1$s from the device", new Object[]{remote.getName()}), this.mParent.getShell());
         }
      } catch (SyncException var4) {
         if(!var4.wasCanceled()) {
            DdmConsole.printErrorToConsole(String.format("Failed to pull selection: %1$s", new Object[]{var4.getMessage()}));
         }
      } catch (Exception var5) {
         DdmConsole.printErrorToConsole("Failed to pull selection");
         DdmConsole.printErrorToConsole(var5.getMessage());
      }

   }

   private void pushFiles(final String[] localFiles, final FileEntry remoteDirectory) {
      try {
         final SyncService e = this.mCurrentDevice.getSyncService();
         if(e != null) {
            SyncProgressHelper.run(new SyncProgressHelper.SyncRunnable() {
               public void run(ISyncProgressMonitor monitor) throws SyncException, IOException, TimeoutException {
                  e.push(localFiles, remoteDirectory, monitor);
               }

               public void close() {
                  e.close();
               }
            }, "Pushing file(s) to the device", this.mParent.getShell());
         }
      } catch (SyncException var4) {
         if(!var4.wasCanceled()) {
            DdmConsole.printErrorToConsole(String.format("Failed to push selection: %1$s", new Object[]{var4.getMessage()}));
         }
      } catch (Exception var5) {
         DdmConsole.printErrorToConsole("Failed to push the items");
         DdmConsole.printErrorToConsole(var5.getMessage());
      }

   }

   private void pushFile(final String local, String remoteDirectory) {
      try {
         final SyncService e = this.mCurrentDevice.getSyncService();
         if(e != null) {
            String[] segs = local.split(Pattern.quote(File.separator));
            String name = segs[segs.length - 1];
            final String remoteFile = remoteDirectory + "/" + name;
            SyncProgressHelper.run(new SyncProgressHelper.SyncRunnable() {
               public void run(ISyncProgressMonitor monitor) throws SyncException, IOException, TimeoutException {
                  e.pushFile(local, remoteFile, monitor);
               }

               public void close() {
                  e.close();
               }
            }, String.format("Pushing %1$s to the device.", new Object[]{name}), this.mParent.getShell());
         }
      } catch (SyncException var7) {
         if(!var7.wasCanceled()) {
            DdmConsole.printErrorToConsole(String.format("Failed to push selection: %1$s", new Object[]{var7.getMessage()}));
         }
      } catch (Exception var8) {
         DdmConsole.printErrorToConsole("Failed to push the item(s).");
         DdmConsole.printErrorToConsole(var8.getMessage());
      }

   }

   protected void setDeleteEnabledState(FileEntry element) {
      this.mDeleteAction.setEnabled(element.getType() == 0);
   }
}
