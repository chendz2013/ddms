package com.android.ddmuilib.explorer;

import com.android.ddmlib.FileListingService;
import com.android.ddmlib.FileListingService.FileEntry;
import com.android.ddmlib.FileListingService.IListingReceiver;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Tree;

class DeviceContentProvider implements ITreeContentProvider {
   private TreeViewer mViewer;
   private FileListingService mFileListingService;
   private FileEntry mRootEntry;
   private IListingReceiver sListingReceiver = new IListingReceiver() {
      public void setChildren(final FileEntry entry, FileEntry[] children) {
         final Tree t = DeviceContentProvider.this.mViewer.getTree();
         if(t != null && !t.isDisposed()) {
            Display display = t.getDisplay();
            if(!display.isDisposed()) {
               display.asyncExec(new Runnable() {
                  public void run() {
                     if(!t.isDisposed()) {
                        DeviceContentProvider.this.mViewer.refresh(entry);
                        DeviceContentProvider.this.mViewer.setExpandedState(entry, true);
                     }

                  }
               });
            }
         }

      }

      public void refreshEntry(final FileEntry entry) {
         final Tree t = DeviceContentProvider.this.mViewer.getTree();
         if(t != null && !t.isDisposed()) {
            Display display = t.getDisplay();
            if(!display.isDisposed()) {
               display.asyncExec(new Runnable() {
                  public void run() {
                     if(!t.isDisposed()) {
                        DeviceContentProvider.this.mViewer.refresh(entry);
                     }

                  }
               });
            }
         }

      }
   };

   public void setListingService(FileListingService fls) {
      this.mFileListingService = fls;
   }

   public Object[] getChildren(Object parentElement) {
      if(parentElement instanceof FileEntry) {
         FileEntry parentEntry = (FileEntry)parentElement;
         FileEntry[] oldEntries = parentEntry.getCachedChildren();
         FileEntry[] newEntries = this.mFileListingService.getChildren(parentEntry, true, this.sListingReceiver);
         return newEntries != null?newEntries:oldEntries;
      } else {
         return new Object[0];
      }
   }

   public Object getParent(Object element) {
      if(element instanceof FileEntry) {
         FileEntry entry = (FileEntry)element;
         return entry.getParent();
      } else {
         return null;
      }
   }

   public boolean hasChildren(Object element) {
      if(element instanceof FileEntry) {
         FileEntry entry = (FileEntry)element;
         return entry.getType() == 1;
      } else {
         return false;
      }
   }

   public Object[] getElements(Object inputElement) {
      if(inputElement instanceof FileEntry) {
         FileEntry entry = (FileEntry)inputElement;
         if(entry.isRoot()) {
            return this.getChildren(this.mRootEntry);
         }
      }

      return null;
   }

   public void dispose() {
   }

   public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
      if(viewer instanceof TreeViewer) {
         this.mViewer = (TreeViewer)viewer;
      }

      if(newInput instanceof FileEntry) {
         this.mRootEntry = (FileEntry)newInput;
      }

   }
}
