package com.android.ddmuilib.explorer;

import com.android.ddmlib.FileListingService.FileEntry;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.swt.graphics.Image;

class FileLabelProvider implements ILabelProvider, ITableLabelProvider {
   private Image mFileImage;
   private Image mFolderImage;
   private Image mPackageImage;
   private Image mOtherImage;

   public FileLabelProvider(Image fileImage, Image folderImage, Image packageImage, Image otherImage) {
      this.mFileImage = fileImage;
      this.mFolderImage = folderImage;
      this.mOtherImage = otherImage;
      if(packageImage != null) {
         this.mPackageImage = packageImage;
      } else {
         this.mPackageImage = fileImage;
      }

   }

   public FileLabelProvider() {
   }

   public Image getImage(Object element) {
      return null;
   }

   public String getText(Object element) {
      return null;
   }

   public Image getColumnImage(Object element, int columnIndex) {
      if(columnIndex == 0) {
         if(element instanceof FileEntry) {
            FileEntry entry = (FileEntry)element;
            switch(entry.getType()) {
            case 0:
            case 5:
               if(entry.isApplicationPackage()) {
                  return this.mPackageImage;
               }

               return this.mFileImage;
            case 1:
            case 2:
               return this.mFolderImage;
            case 3:
            case 4:
            }
         }

         return this.mOtherImage;
      } else {
         return null;
      }
   }

   public String getColumnText(Object element, int columnIndex) {
      if(element instanceof FileEntry) {
         FileEntry entry = (FileEntry)element;
         switch(columnIndex) {
         case 0:
            return entry.getName();
         case 1:
            return entry.getSize();
         case 2:
            return entry.getDate();
         case 3:
            return entry.getTime();
         case 4:
            return entry.getPermissions();
         case 5:
            return entry.getInfo();
         }
      }

      return null;
   }

   public void addListener(ILabelProviderListener listener) {
   }

   public void dispose() {
   }

   public boolean isLabelProperty(Object element, String property) {
      return false;
   }

   public void removeListener(ILabelProviderListener listener) {
   }
}
