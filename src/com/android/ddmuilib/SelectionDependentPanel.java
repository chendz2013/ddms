package com.android.ddmuilib;

import com.android.ddmlib.Client;
import com.android.ddmlib.IDevice;
import com.android.ddmuilib.Panel;

public abstract class SelectionDependentPanel extends Panel {
   private IDevice mCurrentDevice = null;
   private Client mCurrentClient = null;

   protected final IDevice getCurrentDevice() {
      return this.mCurrentDevice;
   }

   protected final Client getCurrentClient() {
      return this.mCurrentClient;
   }

   public final void deviceSelected(IDevice selectedDevice) {
      if(selectedDevice != this.mCurrentDevice) {
         this.mCurrentDevice = selectedDevice;
         this.deviceSelected();
      }

   }

   public final void clientSelected(Client selectedClient) {
      if(selectedClient != this.mCurrentClient) {
         this.mCurrentClient = selectedClient;
         this.clientSelected();
      }

   }

   public abstract void deviceSelected();

   public abstract void clientSelected();
}
