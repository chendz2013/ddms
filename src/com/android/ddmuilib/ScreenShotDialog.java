package com.android.ddmuilib;

import com.android.ddmlib.AdbCommandRejectedException;
import com.android.ddmlib.IDevice;
import com.android.ddmlib.Log;
import com.android.ddmlib.RawImage;
import com.android.ddmlib.TimeoutException;
import com.android.ddmuilib.DdmUiPreferences;
import com.android.ddmuilib.ImageLoader;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.ImageTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.PaletteData;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

public class ScreenShotDialog extends Dialog {
   private Label mBusyLabel;
   private Label mImageLabel;
   private Button mSave;
   private IDevice mDevice;
   private RawImage mRawImage;
   private Clipboard mClipboard;
   private int mRotateCount;

   public ScreenShotDialog(Shell parent) {
      this(parent, 67680);
      this.mClipboard = new Clipboard(parent.getDisplay());
   }

   public ScreenShotDialog(Shell parent, int style) {
      super(parent, style);
      this.mRotateCount = 0;
   }

   public void open(IDevice device) {
      this.mDevice = device;
      Shell parent = this.getParent();
      Shell shell = new Shell(parent, this.getStyle());
      shell.setText("Device Screen Capture");
      this.createContents(shell);
      shell.pack();
      shell.open();
      this.updateDeviceImage(shell);
      Display display = parent.getDisplay();

      while(!shell.isDisposed()) {
         if(!display.readAndDispatch()) {
            display.sleep();
         }
      }

   }

   private void createContents(final Shell shell) {
      boolean colCount = true;
      shell.setLayout(new GridLayout(5, true));
      Button refresh = new Button(shell, 8);
      refresh.setText("Refresh");
      GridData data = new GridData(64);
      data.widthHint = 80;
      refresh.setLayoutData(data);
      refresh.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            ScreenShotDialog.this.updateDeviceImage(shell);
            if(ScreenShotDialog.this.mRawImage != null) {
               for(int i = 0; i < ScreenShotDialog.this.mRotateCount; ++i) {
                  ScreenShotDialog.this.mRawImage = ScreenShotDialog.this.mRawImage.getRotated();
               }

               ScreenShotDialog.this.updateImageDisplay(shell);
            }

         }
      });
      Button rotate = new Button(shell, 8);
      rotate.setText("Rotate");
      data = new GridData(64);
      data.widthHint = 80;
      rotate.setLayoutData(data);
      rotate.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            if(ScreenShotDialog.this.mRawImage != null) {
               ScreenShotDialog.this.mRotateCount = (ScreenShotDialog.this.mRotateCount + 1) % 4;
               ScreenShotDialog.this.mRawImage = ScreenShotDialog.this.mRawImage.getRotated();
               ScreenShotDialog.this.updateImageDisplay(shell);
            }

         }
      });
      this.mSave = new Button(shell, 8);
      this.mSave.setText("Save");
      data = new GridData(64);
      data.widthHint = 80;
      this.mSave.setLayoutData(data);
      this.mSave.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            ScreenShotDialog.this.saveImage(shell);
         }
      });
      Button copy = new Button(shell, 8);
      copy.setText("Copy");
      copy.setToolTipText("Copy the screenshot to the clipboard");
      data = new GridData(64);
      data.widthHint = 80;
      copy.setLayoutData(data);
      copy.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            ScreenShotDialog.this.copy();
         }
      });
      Button done = new Button(shell, 8);
      done.setText("Done");
      data = new GridData(64);
      data.widthHint = 80;
      done.setLayoutData(data);
      done.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            shell.close();
         }
      });
      this.mBusyLabel = new Label(shell, 0);
      this.mBusyLabel.setText("Preparing...");
      data = new GridData(32);
      data.horizontalSpan = 5;
      this.mBusyLabel.setLayoutData(data);
      this.mImageLabel = new Label(shell, 2048);
      data = new GridData(64);
      data.horizontalSpan = 5;
      this.mImageLabel.setLayoutData(data);
      Display display = shell.getDisplay();
      this.mImageLabel.setImage(ImageLoader.createPlaceHolderArt(display, 50, 50, display.getSystemColor(9)));
      shell.setDefaultButton(done);
   }

   private void copy() {
      this.mClipboard.setContents(new Object[]{this.mImageLabel.getImage().getImageData()}, new Transfer[]{ImageTransfer.getInstance()});
   }

   private void updateDeviceImage(Shell shell) {
      this.mBusyLabel.setText("Capturing...");
      shell.setCursor(shell.getDisplay().getSystemCursor(1));
      this.mRawImage = this.getDeviceImage();
      this.updateImageDisplay(shell);
   }

   private void updateImageDisplay(Shell shell) {
      Image image;
      if(this.mRawImage == null) {
         Display palette = shell.getDisplay();
         image = ImageLoader.createPlaceHolderArt(palette, 320, 240, palette.getSystemColor(9));
         this.mSave.setEnabled(false);
         this.mBusyLabel.setText("Screen not available");
      } else {
         PaletteData palette1 = new PaletteData(this.mRawImage.getRedMask(), this.mRawImage.getGreenMask(), this.mRawImage.getBlueMask());
         ImageData imageData = new ImageData(this.mRawImage.width, this.mRawImage.height, this.mRawImage.bpp, palette1, 1, this.mRawImage.data);
         image = new Image(this.getParent().getDisplay(), imageData);
         this.mSave.setEnabled(true);
         this.mBusyLabel.setText("Captured image:");
      }

      this.mImageLabel.setImage(image);
      this.mImageLabel.pack();
      shell.pack();
      shell.setCursor(shell.getDisplay().getSystemCursor(0));
   }

   private RawImage getDeviceImage() {
      try {
         return this.mDevice.getScreenshot();
      } catch (IOException var2) {
         Log.w("ddms", "Unable to get frame buffer: " + var2.getMessage());
         return null;
      } catch (TimeoutException var3) {
         Log.w("ddms", "Unable to get frame buffer: timeout ");
         return null;
      } catch (AdbCommandRejectedException var4) {
         Log.w("ddms", "Unable to get frame buffer: " + var4.getMessage());
         return null;
      }
   }

   private void saveImage(Shell shell) {
      FileDialog dlg = new FileDialog(shell, 8192);
      Calendar now = Calendar.getInstance();
      String fileName = String.format("device-%tF-%tH%tM%tS.png", new Object[]{now, now, now, now});
      dlg.setText("Save image...");
      dlg.setFileName(fileName);
      String lastDir = DdmUiPreferences.getStore().getString("lastImageSaveDir");
      if(lastDir.length() == 0) {
         lastDir = DdmUiPreferences.getStore().getString("imageSaveDir");
      }

      dlg.setFilterPath(lastDir);
      dlg.setFilterNames(new String[]{"PNG Files (*.png)"});
      dlg.setFilterExtensions(new String[]{"*.png"});
      fileName = dlg.open();
      if(fileName != null) {
         if(!fileName.endsWith(".png")) {
            fileName = fileName + ".png";
         }

         String saveDir = (new File(fileName)).getParent();
         if(saveDir != null) {
            DdmUiPreferences.getStore().setValue("lastImageSaveDir", saveDir);
         }

         Log.d("ddms", "Saving image to " + fileName);
         ImageData imageData = this.mImageLabel.getImage().getImageData();

         try {
            org.eclipse.swt.graphics.ImageLoader e = new org.eclipse.swt.graphics.ImageLoader();
            e.data = new ImageData[]{imageData};
            e.save(fileName, 5);
         } catch (SWTException var9) {
            Log.w("ddms", "Unable to save " + fileName + ": " + var9.getMessage());
         }
      }

   }
}
