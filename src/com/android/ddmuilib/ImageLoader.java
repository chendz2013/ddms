package com.android.ddmuilib;

import com.android.ddmlib.Log;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;

public class ImageLoader {
   private static final String PATH = "/images/";
   private final HashMap<String, Image> mLoadedImages = new HashMap();
   private static final HashMap<Class<?>, ImageLoader> mInstances = new HashMap();
   private final Class<?> mClass;

   private ImageLoader(Class<?> theClass) {
      if(theClass == null) {
         theClass = ImageLoader.class;
      }

      this.mClass = theClass;
   }

   public static ImageLoader getDdmUiLibLoader() {
      return getLoader((Class)null);
   }

   public static ImageLoader getLoader(Class<?> theClass) {
      ImageLoader instance = (ImageLoader)mInstances.get(theClass);
      if(instance == null) {
         instance = new ImageLoader(theClass);
         mInstances.put(theClass, instance);
      }

      return instance;
   }

   public static void dispose() {
      Iterator i$ = mInstances.values().iterator();

      while(i$.hasNext()) {
         ImageLoader loader = (ImageLoader)i$.next();
         loader.doDispose();
      }

   }

   private synchronized void doDispose() {
      Iterator i$ = this.mLoadedImages.values().iterator();

      while(i$.hasNext()) {
         Image image = (Image)i$.next();
         image.dispose();
      }

      this.mLoadedImages.clear();
   }

   public ImageDescriptor loadDescriptor(String filename) {
      URL url = this.mClass.getResource("/images/" + filename);
      return ImageDescriptor.createFromURL(url);
   }

   public synchronized Image loadImage(String filename, Display display) {
      Image img = (Image)this.mLoadedImages.get(filename);
      if(img == null) {
         String tmp = "/images/" + filename;
         InputStream imageStream = this.mClass.getResourceAsStream(tmp);
         if(imageStream != null) {
            img = new Image(display, imageStream);
            this.mLoadedImages.put(filename, img);
         }

         if(img == null) {
            throw new RuntimeException("Failed to load " + tmp);
         }
      }

      return img;
   }

   public Image loadImage(Display display, String fileName, int width, int height, Color phColor) {
      Image img = this.loadImage(fileName, display);
      if(img == null) {
         Log.w("ddms", "Couldn\'t load " + fileName);
         return width != -1 && height != -1?createPlaceHolderArt(display, width, height, phColor != null?phColor:display.getSystemColor(9)):null;
      } else {
         return img;
      }
   }

   public static Image createPlaceHolderArt(Display display, int width, int height, Color color) {
      Image img = new Image(display, width, height);
      GC gc = new GC(img);
      gc.setForeground(color);
      gc.drawLine(0, 0, width, height);
      gc.drawLine(0, height - 1, width, -1);
      gc.dispose();
      return img;
   }
}
