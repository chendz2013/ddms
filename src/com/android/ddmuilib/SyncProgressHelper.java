package com.android.ddmuilib;

import com.android.ddmlib.SyncException;
import com.android.ddmlib.TimeoutException;
import com.android.ddmlib.SyncService.ISyncProgressMonitor;
import com.android.ddmuilib.SyncProgressMonitor;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.swt.widgets.Shell;

public class SyncProgressHelper {
   public static void run(final SyncProgressHelper.SyncRunnable runnable, final String progressMessage, Shell parentShell) throws InvocationTargetException, InterruptedException, SyncException, IOException, TimeoutException {
      final Exception[] result = new Exception[1];
      (new ProgressMonitorDialog(parentShell)).run(true, true, new IRunnableWithProgress() {
         public void run(IProgressMonitor monitor) {
            try {
               runnable.run(new SyncProgressMonitor(monitor, progressMessage));
            } catch (Exception var6) {
               result[0] = var6;
            } finally {
               runnable.close();
            }

         }
      });
      if(result[0] instanceof SyncException) {
         SyncException se = (SyncException)result[0];
         if(!se.wasCanceled()) {
            throw se;
         }
      } else if(result[0] instanceof TimeoutException) {
         throw (TimeoutException)result[0];
      } else if(result[0] instanceof IOException) {
         throw (IOException)result[0];
      } else if(result[0] instanceof RuntimeException) {
         throw (RuntimeException)result[0];
      }
   }

   public interface SyncRunnable {
      void run(ISyncProgressMonitor var1) throws SyncException, IOException, TimeoutException;

      void close();
   }
}
