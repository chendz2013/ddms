package com.android.ddmuilib.heap;

import com.android.ddmlib.NativeAllocationInfo;
import com.android.ddmuilib.heap.NativeHeapSnapshot;
import java.util.List;
import org.eclipse.jface.viewers.ILazyTreeContentProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;

public final class NativeHeapProviderByAllocations implements ILazyTreeContentProvider {
   private TreeViewer mViewer;
   private boolean mDisplayZygoteMemory;
   private NativeHeapSnapshot mNativeHeapDump;

   public NativeHeapProviderByAllocations(TreeViewer viewer, boolean displayZygotes) {
      this.mViewer = viewer;
      this.mDisplayZygoteMemory = displayZygotes;
   }

   public void dispose() {
   }

   public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
      this.mNativeHeapDump = (NativeHeapSnapshot)newInput;
   }

   public Object getParent(Object arg0) {
      return null;
   }

   public void updateChildCount(Object element, int currentChildCount) {
      int childCount = 0;
      if(element == this.mNativeHeapDump) {
         childCount = this.getAllocations().size();
      }

      this.mViewer.setChildCount(element, childCount);
   }

   public void updateElement(Object parent, int index) {
      Object item = null;
      if(parent == this.mNativeHeapDump) {
         item = this.getAllocations().get(index);
      }

      this.mViewer.replace(parent, index, item);
      this.mViewer.setChildCount(item, 0);
   }

   public void displayZygoteMemory(boolean en) {
      this.mDisplayZygoteMemory = en;
   }

   private List<NativeAllocationInfo> getAllocations() {
      return this.mDisplayZygoteMemory?this.mNativeHeapDump.getAllocations():this.mNativeHeapDump.getNonZygoteAllocations();
   }
}
