package com.android.ddmuilib.heap;

import com.android.ddmlib.NativeAllocationInfo;
import com.android.ddmlib.NativeStackCallInfo;
import com.android.ddmuilib.heap.NativeHeapSnapshot;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.Reader;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.regex.Pattern;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.operation.IRunnableWithProgress;

public class NativeHeapDataImporter implements IRunnableWithProgress {
   private final LineNumberReader mReader;
   private int mStartLineNumber;
   private int mEndLineNumber;
   private NativeHeapSnapshot mSnapshot;

   public NativeHeapDataImporter(Reader stream) {
      this.mReader = new LineNumberReader(stream);
      this.mReader.setLineNumber(1);
   }

   public void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException {
      monitor.beginTask("Importing Heap Data", -1);
      ArrayList allocations = new ArrayList();

      try {
         String e1;
         try {
            do {
               StringBuilder sb = new StringBuilder();
               this.mStartLineNumber = this.mReader.getLineNumber();

               while((e1 = this.mReader.readLine()) != null && e1.trim().length() != 0) {
                  sb.append(e1);
                  sb.append('\n');
               }

               this.mEndLineNumber = this.mReader.getLineNumber();
               String allocationBlock = sb.toString();
               if(allocationBlock.trim().length() > 0) {
                  allocations.add(this.getNativeAllocation(allocationBlock));
               }
            } while(e1 != null);
         } catch (Exception var13) {
            Object e = var13;
            if(var13.getMessage() == null) {
               e = new RuntimeException(this.genericErrorMessage("Unexpected Parse error"));
            }

            throw new InvocationTargetException((Throwable)e);
         }
      } finally {
         try {
            this.mReader.close();
         } catch (IOException var12) {
            ;
         }

         monitor.done();
      }

      this.mSnapshot = new NativeHeapSnapshot(allocations);
   }

   private NativeAllocationInfo getNativeAllocation(String block) {
      Scanner sc = new Scanner(block);

      try {
         String kw = sc.next();
         if(!"Allocations:".equals(kw)) {
            throw new InputMismatchException(this.expectedKeywordErrorMessage("Allocations:", kw));
         } else {
            int allocations = sc.nextInt();
            kw = sc.next();
            if(!"Size:".equals(kw)) {
               throw new InputMismatchException(this.expectedKeywordErrorMessage("Size:", kw));
            } else {
               int size = sc.nextInt();
               kw = sc.next();
               if(!"TotalSize:".equals(kw)) {
                  throw new InputMismatchException(this.expectedKeywordErrorMessage("TotalSize:", kw));
               } else {
                  int totalSize = sc.nextInt();
                  if(totalSize != size * allocations) {
                     throw new InputMismatchException(this.genericErrorMessage("Total Size does not match size * # of allocations"));
                  } else {
                     NativeAllocationInfo info = new NativeAllocationInfo(size, allocations);
                     kw = sc.next();
                     if(!"BeginStacktrace:".equals(kw)) {
                        throw new InputMismatchException(this.expectedKeywordErrorMessage("BeginStacktrace:", kw));
                     } else {
                        ArrayList stackInfo = new ArrayList();
                        Pattern endTracePattern = Pattern.compile("EndStacktrace");

                        do {
                           long address = sc.nextLong(16);
                           info.addStackCallAddress(address);
                           String library = sc.next();
                           sc.next();
                           String method = this.scanTillSeparator(sc, "---");
                           String filename = "";
                           if(!this.isUnresolved(method, address)) {
                              filename = sc.next();
                           }

                           stackInfo.add(new NativeStackCallInfo(address, library, method, filename));
                        } while(!sc.hasNext(endTracePattern));

                        info.setResolvedStackCall(stackInfo);
                        NativeAllocationInfo address1 = info;
                        return address1;
                     }
                  }
               }
            }
         }
      } finally {
         sc.close();
      }
   }

   private String scanTillSeparator(Scanner sc, String separator) {
      StringBuilder sb = new StringBuilder();

      while(true) {
         String token = sc.next();
         if(token.equals(separator)) {
            return sb.toString().trim();
         }

         sb.append(token);
         sb.append(' ');
      }
   }

   private boolean isUnresolved(String method, long address) {
      return Long.toString(address, 16).equals(method);
   }

   private String genericErrorMessage(String message) {
      return String.format("%1$s between lines %2$d and %3$d", new Object[]{message, Integer.valueOf(this.mStartLineNumber), Integer.valueOf(this.mEndLineNumber)});
   }

   private String expectedKeywordErrorMessage(String expected, String actual) {
      return String.format("Expected keyword \'%1$s\', saw \'%2$s\' between lines %3$d to %4$d.", new Object[]{expected, actual, Integer.valueOf(this.mStartLineNumber), Integer.valueOf(this.mEndLineNumber)});
   }

   public NativeHeapSnapshot getImportedSnapshot() {
      return this.mSnapshot;
   }
}
