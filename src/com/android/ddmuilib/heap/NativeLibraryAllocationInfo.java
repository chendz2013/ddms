package com.android.ddmuilib.heap;

import com.android.ddmlib.NativeAllocationInfo;
import com.android.ddmlib.NativeStackCallInfo;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public final class NativeLibraryAllocationInfo {
   public static final String UNRESOLVED_LIBRARY_NAME = "Resolving..";
   private static final String UNKNOWN_LIBRARY_NAME = "unknown";
   private final String mLibraryName;
   private final List<NativeAllocationInfo> mHeapAllocations;
   private int mTotalSize;

   private NativeLibraryAllocationInfo(String libraryName) {
      this.mLibraryName = libraryName;
      this.mHeapAllocations = new ArrayList();
   }

   private void addAllocation(NativeAllocationInfo info) {
      this.mHeapAllocations.add(info);
   }

   private void updateTotalSize() {
      this.mTotalSize = 0;

      NativeAllocationInfo i;
      for(Iterator i$ = this.mHeapAllocations.iterator(); i$.hasNext(); this.mTotalSize += i.getAllocationCount() * i.getSize()) {
         i = (NativeAllocationInfo)i$.next();
      }

   }

   public String getLibraryName() {
      return this.mLibraryName;
   }

   public long getTotalSize() {
      return (long)this.mTotalSize;
   }

   public List<NativeAllocationInfo> getAllocations() {
      return this.mHeapAllocations;
   }

   public static List<NativeLibraryAllocationInfo> constructFrom(List<NativeAllocationInfo> allocations) {
      if(allocations == null) {
         return null;
      } else {
         HashMap allocationsByLibrary = new HashMap();

         NativeAllocationInfo i$;
         String l;
         for(Iterator libraryAllocations = allocations.iterator(); libraryAllocations.hasNext(); addtoLibrary(allocationsByLibrary, l, i$)) {
            i$ = (NativeAllocationInfo)libraryAllocations.next();
            l = "Resolving..";
            if(i$.isStackCallResolved()) {
               NativeStackCallInfo relevantStackCall = i$.getRelevantStackCallInfo();
               if(relevantStackCall != null) {
                  l = relevantStackCall.getLibraryName();
               } else {
                  l = "unknown";
               }
            }
         }

         ArrayList libraryAllocations1 = new ArrayList(allocationsByLibrary.values());
         Iterator i$1 = libraryAllocations1.iterator();

         while(i$1.hasNext()) {
            NativeLibraryAllocationInfo l1 = (NativeLibraryAllocationInfo)i$1.next();
            l1.updateTotalSize();
         }

         Collections.sort(libraryAllocations1, new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
               return compare( (NativeLibraryAllocationInfo)o1,(NativeLibraryAllocationInfo)o2);
            }

            public int compare(NativeLibraryAllocationInfo o1, NativeLibraryAllocationInfo o2) {
               return (int)(o2.getTotalSize() - o1.getTotalSize());
            }
         });
         return libraryAllocations1;
      }
   }

   private static void addtoLibrary(Map<String, NativeLibraryAllocationInfo> libraryAllocations, String libName, NativeAllocationInfo info) {
      NativeLibraryAllocationInfo libAllocationInfo = (NativeLibraryAllocationInfo)libraryAllocations.get(libName);
      if(libAllocationInfo == null) {
         libAllocationInfo = new NativeLibraryAllocationInfo(libName);
         libraryAllocations.put(libName, libAllocationInfo);
      }

      libAllocationInfo.addAllocation(info);
   }
}
