package com.android.ddmuilib.heap;

import com.android.ddmlib.NativeAllocationInfo;
import com.android.ddmuilib.heap.NativeDiffAllocationInfo;
import com.android.ddmuilib.heap.NativeHeapSnapshot;
import com.google.common.collect.Sets;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class NativeHeapDiffSnapshot extends NativeHeapSnapshot {
   private long mCommonAllocationsTotalMemory;

   public NativeHeapDiffSnapshot(NativeHeapSnapshot newSnapshot, NativeHeapSnapshot oldSnapshot) {
      super(getNewAllocations(newSnapshot, oldSnapshot));
      HashSet commonAllocations = new HashSet(oldSnapshot.getAllocations());
      commonAllocations.retainAll(newSnapshot.getAllocations());
      this.mCommonAllocationsTotalMemory = this.getTotalMemory(commonAllocations);
   }

   private static List<NativeAllocationInfo> getNewAllocations(NativeHeapSnapshot newSnapshot, NativeHeapSnapshot oldSnapshot) {
      HashSet allocations = new HashSet(newSnapshot.getAllocations());
      allocations.removeAll(oldSnapshot.getAllocations());
      HashSet onlyInPrevious = new HashSet(oldSnapshot.getAllocations());
      HashSet newAllocations = Sets.newHashSetWithExpectedSize(allocations.size());
      onlyInPrevious.removeAll(newSnapshot.getAllocations());
      Iterator i$ = allocations.iterator();

      while(i$.hasNext()) {
         NativeAllocationInfo current = (NativeAllocationInfo)i$.next();
         NativeAllocationInfo old = getOldAllocationWithSameStack(current, onlyInPrevious);
         if(old == null) {
            newAllocations.add(current);
         } else if(current.getAllocationCount() > old.getAllocationCount()) {
            newAllocations.add(new NativeDiffAllocationInfo(current, old));
         }
      }

      return new ArrayList(newAllocations);
   }

   private static NativeAllocationInfo getOldAllocationWithSameStack(NativeAllocationInfo info, Set<NativeAllocationInfo> allocations) {
      Iterator i$ = allocations.iterator();

      NativeAllocationInfo a;
      do {
         if(!i$.hasNext()) {
            return null;
         }

         a = (NativeAllocationInfo)i$.next();
      } while(info.getSize() != a.getSize() || !info.stackEquals(a));

      return a;
   }

   public String getFormattedMemorySize() {
      long newAllocations = this.getTotalSize();
      return String.format("%s bytes new + %s bytes retained = %s bytes total", new Object[]{this.formatMemorySize(newAllocations), this.formatMemorySize(this.mCommonAllocationsTotalMemory), this.formatMemorySize(newAllocations + this.mCommonAllocationsTotalMemory)});
   }
}
