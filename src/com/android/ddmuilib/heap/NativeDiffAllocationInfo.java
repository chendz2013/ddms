package com.android.ddmuilib.heap;

import com.android.ddmlib.NativeAllocationInfo;
import com.android.ddmlib.NativeStackCallInfo;
import java.util.List;

public class NativeDiffAllocationInfo extends NativeAllocationInfo {
   private final NativeAllocationInfo info;

   public NativeDiffAllocationInfo(NativeAllocationInfo cur, NativeAllocationInfo prev) {
      super(cur.getSize(), getNewAllocations(cur, prev));
      this.info = cur;
   }

   private static int getNewAllocations(NativeAllocationInfo n1, NativeAllocationInfo n2) {
      return n1.getAllocationCount() - n2.getAllocationCount();
   }

   public boolean isStackCallResolved() {
      return this.info.isStackCallResolved();
   }

   public List<Long> getStackCallAddresses() {
      return this.info.getStackCallAddresses();
   }

   public synchronized List<NativeStackCallInfo> getResolvedStackCall() {
      return this.info.getResolvedStackCall();
   }

   public synchronized NativeStackCallInfo getRelevantStackCallInfo() {
      return this.info.getRelevantStackCallInfo();
   }

   public boolean isZygoteChild() {
      return this.info.isZygoteChild();
   }
}
