package com.android.ddmuilib.heap;

import com.android.ddmlib.NativeAllocationInfo;
import com.android.ddmlib.NativeStackCallInfo;
import com.android.ddmuilib.heap.NativeLibraryAllocationInfo;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

public class NativeHeapLabelProvider extends LabelProvider implements ITableLabelProvider {
   private long mTotalSize;

   public Image getColumnImage(Object arg0, int arg1) {
      return null;
   }

   public String getColumnText(Object element, int index) {
      return element instanceof NativeAllocationInfo?this.getColumnTextForNativeAllocation((NativeAllocationInfo)element, index):(element instanceof NativeLibraryAllocationInfo?this.getColumnTextForNativeLibrary((NativeLibraryAllocationInfo)element, index):null);
   }

   private String getColumnTextForNativeAllocation(NativeAllocationInfo info, int index) {
      NativeStackCallInfo stackInfo = info.getRelevantStackCallInfo();
      switch(index) {
      case 0:
         return stackInfo == null?this.stackResolutionStatus(info):stackInfo.getLibraryName();
      case 1:
         return Integer.toString(info.getSize() * info.getAllocationCount());
      case 2:
         return this.getPercentageString((long)(info.getSize() * info.getAllocationCount()), this.mTotalSize);
      case 3:
         String prefix = "";
         if(!info.isZygoteChild()) {
            prefix = "Z ";
         }

         return prefix + Integer.toString(info.getAllocationCount());
      case 4:
         return Integer.toString(info.getSize());
      case 5:
         return stackInfo == null?this.stackResolutionStatus(info):stackInfo.getMethodName();
      default:
         return null;
      }
   }

   private String getColumnTextForNativeLibrary(NativeLibraryAllocationInfo info, int index) {
      switch(index) {
      case 0:
         return info.getLibraryName();
      case 1:
         return Long.toString(info.getTotalSize());
      case 2:
         return this.getPercentageString(info.getTotalSize(), this.mTotalSize);
      default:
         return null;
      }
   }

   private String getPercentageString(long size, long total) {
      return total == 0L?"":String.format("%.1f%%", new Object[]{Float.valueOf((float)(size * 100L) / (float)total)});
   }

   private String stackResolutionStatus(NativeAllocationInfo info) {
      return info.isStackCallResolved()?"?":"Resolving...";
   }

   public void setTotalSize(long totalSize) {
      this.mTotalSize = totalSize;
   }
}
