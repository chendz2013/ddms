package com.android.ddmuilib.heap;

import com.android.ddmlib.NativeStackCallInfo;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

public class NativeStackLabelProvider extends LabelProvider implements ITableLabelProvider {
   public Image getColumnImage(Object arg0, int arg1) {
      return null;
   }

   public String getColumnText(Object element, int index) {
      return element instanceof NativeStackCallInfo?this.getResolvedStackTraceColumnText((NativeStackCallInfo)element, index):(element instanceof Long?this.getStackAddressColumnText((Long)element, index):null);
   }

   public String getResolvedStackTraceColumnText(NativeStackCallInfo info, int index) {
      switch(index) {
      case 0:
         return String.format("0x%08x", new Object[]{Long.valueOf(info.getAddress())});
      case 1:
         return info.getLibraryName();
      case 2:
         return info.getMethodName();
      case 3:
         return info.getSourceFile();
      case 4:
         int l = info.getLineNumber();
         return l == -1?"":Integer.toString(l);
      default:
         return null;
      }
   }

   private String getStackAddressColumnText(Long address, int index) {
      return index == 0?String.format("0x%08x", new Object[]{address}):null;
   }
}
