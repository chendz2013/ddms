package com.android.ddmuilib.heap;

import java.util.List;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;

public class NativeStackContentProvider implements ITreeContentProvider {
   public Object[] getElements(Object arg0) {
      return this.getChildren(arg0);
   }

   public void dispose() {
   }

   public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
   }

   public Object[] getChildren(Object parentElement) {
      return parentElement instanceof List?((List)parentElement).toArray():null;
   }

   public Object getParent(Object element) {
      return null;
   }

   public boolean hasChildren(Object element) {
      return false;
   }
}
