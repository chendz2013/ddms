package com.android.ddmuilib.heap;

import com.android.ddmuilib.heap.NativeHeapSnapshot;
import com.android.ddmuilib.heap.NativeLibraryAllocationInfo;
import java.util.List;
import org.eclipse.jface.viewers.ILazyTreeContentProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;

public class NativeHeapProviderByLibrary implements ILazyTreeContentProvider {
   private TreeViewer mViewer;
   private boolean mDisplayZygoteMemory;

   public NativeHeapProviderByLibrary(TreeViewer viewer, boolean displayZygotes) {
      this.mViewer = viewer;
      this.mDisplayZygoteMemory = displayZygotes;
   }

   public void dispose() {
   }

   public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
   }

   public Object getParent(Object element) {
      return null;
   }

   public void updateChildCount(Object element, int currentChildCount) {
      int childCount = 0;
      if(element instanceof NativeHeapSnapshot) {
         NativeHeapSnapshot info = (NativeHeapSnapshot)element;
         childCount = this.getLibraryAllocations(info).size();
      } else if(element instanceof NativeLibraryAllocationInfo) {
         NativeLibraryAllocationInfo info1 = (NativeLibraryAllocationInfo)element;
         childCount = info1.getAllocations().size();
      }

      this.mViewer.setChildCount(element, childCount);
   }

   public void updateElement(Object parent, int index) {
      Object item = null;
      int childCount = 0;
      if(parent instanceof NativeHeapSnapshot) {
         NativeHeapSnapshot snapshot = (NativeHeapSnapshot)parent;
         item = this.getLibraryAllocations(snapshot).get(index);
         childCount = ((NativeLibraryAllocationInfo)item).getAllocations().size();
      } else if(parent instanceof NativeLibraryAllocationInfo) {
         item = ((NativeLibraryAllocationInfo)parent).getAllocations().get(index);
      }

      this.mViewer.replace(parent, index, item);
      this.mViewer.setChildCount(item, childCount);
   }

   public void displayZygoteMemory(boolean en) {
      this.mDisplayZygoteMemory = en;
   }

   private List<NativeLibraryAllocationInfo> getLibraryAllocations(NativeHeapSnapshot snapshot) {
      return this.mDisplayZygoteMemory?snapshot.getAllocationsByLibrary():snapshot.getNonZygoteAllocationsByLibrary();
   }
}
