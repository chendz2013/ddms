package com.android.ddmuilib.heap;

import com.android.ddmlib.NativeAllocationInfo;
import com.android.ddmuilib.heap.NativeLibraryAllocationInfo;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class NativeHeapSnapshot {
   private static final NumberFormat NUMBER_FORMATTER = NumberFormat.getInstance();
   private List<NativeAllocationInfo> mHeapAllocations;
   private List<NativeLibraryAllocationInfo> mHeapAllocationsByLibrary;
   private List<NativeAllocationInfo> mNonZygoteHeapAllocations;
   private List<NativeLibraryAllocationInfo> mNonZygoteHeapAllocationsByLibrary;
   private long mTotalSize;

   public NativeHeapSnapshot(List<NativeAllocationInfo> heapAllocations) {
      this.mHeapAllocations = heapAllocations;
      this.mTotalSize = this.getTotalMemory(heapAllocations);
   }

   protected long getTotalMemory(Collection<NativeAllocationInfo> heapSnapshot) {
      long total = 0L;

      NativeAllocationInfo info;
      for(Iterator i$ = heapSnapshot.iterator(); i$.hasNext(); total += (long)(info.getAllocationCount() * info.getSize())) {
         info = (NativeAllocationInfo)i$.next();
      }

      return total;
   }

   public List<NativeAllocationInfo> getAllocations() {
      return this.mHeapAllocations;
   }

   public List<NativeLibraryAllocationInfo> getAllocationsByLibrary() {
      if(this.mHeapAllocationsByLibrary != null) {
         return this.mHeapAllocationsByLibrary;
      } else {
         List heapAllocations = NativeLibraryAllocationInfo.constructFrom(this.mHeapAllocations);
         if(this.isFullyResolved(heapAllocations)) {
            this.mHeapAllocationsByLibrary = heapAllocations;
         }

         return heapAllocations;
      }
   }

   private boolean isFullyResolved(List<NativeLibraryAllocationInfo> heapAllocations) {
      Iterator i$ = heapAllocations.iterator();

      NativeLibraryAllocationInfo info;
      do {
         if(!i$.hasNext()) {
            return true;
         }

         info = (NativeLibraryAllocationInfo)i$.next();
      } while(!info.getLibraryName().equals("Resolving.."));

      return false;
   }

   public long getTotalSize() {
      return this.mTotalSize;
   }

   public String getFormattedMemorySize() {
      return String.format("%s bytes", new Object[]{this.formatMemorySize(this.getTotalSize())});
   }

   protected String formatMemorySize(long memSize) {
      return NUMBER_FORMATTER.format(memSize);
   }

   public List<NativeAllocationInfo> getNonZygoteAllocations() {
      if(this.mNonZygoteHeapAllocations != null) {
         return this.mNonZygoteHeapAllocations;
      } else {
         this.mNonZygoteHeapAllocations = new ArrayList();
         Iterator i$ = this.mHeapAllocations.iterator();

         while(i$.hasNext()) {
            NativeAllocationInfo info = (NativeAllocationInfo)i$.next();
            if(info.isZygoteChild()) {
               this.mNonZygoteHeapAllocations.add(info);
            }
         }

         return this.mNonZygoteHeapAllocations;
      }
   }

   public List<NativeLibraryAllocationInfo> getNonZygoteAllocationsByLibrary() {
      if(this.mNonZygoteHeapAllocationsByLibrary != null) {
         return this.mNonZygoteHeapAllocationsByLibrary;
      } else {
         List heapAllocations = NativeLibraryAllocationInfo.constructFrom(this.getNonZygoteAllocations());
         if(this.isFullyResolved(heapAllocations)) {
            this.mNonZygoteHeapAllocationsByLibrary = heapAllocations;
         }

         return heapAllocations;
      }
   }
}
