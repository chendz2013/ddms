package com.android.ddmuilib.heap;

import com.android.ddmlib.NativeAllocationInfo;
import com.android.ddmlib.NativeLibraryMapInfo;
import com.android.ddmlib.NativeStackCallInfo;
import com.android.ddmuilib.DdmUiPreferences;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.RandomAccessFile;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.operation.IRunnableWithProgress;

public class NativeSymbolResolverTask implements IRunnableWithProgress {
   private static final String ADDR2LINE;
   private static final String ADDR2LINE64;
   private static final String DEFAULT_SYMBOLS_FOLDER;
   private static final int ELF_CLASS32 = 1;
   private static final int ELF_CLASS64 = 2;
   private static final int ELF_DATA2LSB = 1;
   private static final int ELF_PT_LOAD = 1;
   private List<NativeAllocationInfo> mCallSites;
   private List<NativeLibraryMapInfo> mMappedLibraries;
   private List<String> mSymbolSearchFolders;
   private SortedSet<Long> mUnresolvedAddresses;
   private Set<Long> mUnresolvableAddresses;
   private Map<NativeLibraryMapInfo, Set<Long>> mUnresolvedAddressesPerLibrary;
   private Set<Long> mUnmappedAddresses;
   private Map<Long, NativeStackCallInfo> mAddressResolution;
   private Set<String> mNotFoundLibraries;
   private String mAddr2LineErrorMessage = null;
   private String mAddr2LineCmd;

   public NativeSymbolResolverTask(List<NativeAllocationInfo> callSites, List<NativeLibraryMapInfo> mappedLibraries, String symbolSearchPath, String abi) {
      this.mCallSites = callSites;
      this.mMappedLibraries = mappedLibraries;
      this.mSymbolSearchFolders = new ArrayList();
      this.mSymbolSearchFolders.add(DEFAULT_SYMBOLS_FOLDER);
      this.mSymbolSearchFolders.addAll(Arrays.asList(symbolSearchPath.split(":")));
      this.mUnresolvedAddresses = new TreeSet();
      this.mUnresolvableAddresses = new HashSet();
      this.mUnresolvedAddressesPerLibrary = new HashMap();
      this.mUnmappedAddresses = new HashSet();
      this.mAddressResolution = new HashMap();
      this.mNotFoundLibraries = new HashSet();
      if(abi != null && !abi.startsWith("32")) {
         this.mAddr2LineCmd = ADDR2LINE64;
      } else {
         this.mAddr2LineCmd = ADDR2LINE;
      }

   }

   public void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException {
      monitor.beginTask("Resolving symbols", -1);
      this.collectAllUnresolvedAddresses();
      this.checkCancellation(monitor);
      this.mapUnresolvedAddressesToLibrary();
      this.checkCancellation(monitor);
      this.resolveLibraryAddresses(monitor);
      this.checkCancellation(monitor);
      this.resolveCallSites(this.mCallSites);
      monitor.done();
   }

   private void collectAllUnresolvedAddresses() {
      Iterator i$ = this.mCallSites.iterator();

      while(i$.hasNext()) {
         NativeAllocationInfo callSite = (NativeAllocationInfo)i$.next();
         this.mUnresolvedAddresses.addAll(callSite.getStackCallAddresses());
      }

   }

   private void mapUnresolvedAddressesToLibrary() {
      HashSet mappedAddresses = new HashSet();
      Iterator i$ = this.mMappedLibraries.iterator();

      while(i$.hasNext()) {
         NativeLibraryMapInfo lib = (NativeLibraryMapInfo)i$.next();
         SortedSet addressesInLibrary = this.mUnresolvedAddresses.subSet(Long.valueOf(lib.getStartAddress()), Long.valueOf(lib.getEndAddress() + 1L));
         if(addressesInLibrary.size() > 0) {
            this.mUnresolvedAddressesPerLibrary.put(lib, addressesInLibrary);
            mappedAddresses.addAll(addressesInLibrary);
         }
      }

      this.mUnmappedAddresses.addAll(this.mUnresolvedAddresses);
      this.mUnmappedAddresses.removeAll(mappedAddresses);
   }

   private void resolveLibraryAddresses(IProgressMonitor monitor) throws InterruptedException {
      for(Iterator i$ = this.mUnresolvedAddressesPerLibrary.keySet().iterator(); i$.hasNext(); this.checkCancellation(monitor)) {
         NativeLibraryMapInfo lib = (NativeLibraryMapInfo)i$.next();
         String libPath = this.getLibraryLocation(lib);
         Set addressesToResolve = (Set)this.mUnresolvedAddressesPerLibrary.get(lib);
         if(libPath == null) {
            this.mNotFoundLibraries.add(lib.getLibraryName());
            this.markAddressesNotResolvable(addressesToResolve, lib);
         } else {
            monitor.subTask(String.format("Resolving addresses mapped to %s.", new Object[]{libPath}));
            this.resolveAddresses(lib, libPath, addressesToResolve);
         }
      }

   }

   private long unsigned(byte value, long shift) {
      return ((long)value & 255L) << (int)shift;
   }

   private short elfGetHalfWord(RandomAccessFile file, long offset) throws IOException {
      byte[] buf = new byte[2];
      file.seek(offset);
      file.readFully(buf, 0, 2);
      return (short)((int)(this.unsigned(buf[0], 0L) | this.unsigned(buf[1], 8L)));
   }

   private int elfGetWord(RandomAccessFile file, long offset) throws IOException {
      byte[] buf = new byte[4];
      file.seek(offset);
      file.readFully(buf, 0, 4);
      return (int)(this.unsigned(buf[0], 0L) | this.unsigned(buf[1], 8L) | this.unsigned(buf[2], 16L) | this.unsigned(buf[3], 24L));
   }

   private long elfGetDoubleWord(RandomAccessFile file, long offset) throws IOException {
      byte[] buf = new byte[8];
      file.seek(offset);
      file.readFully(buf, 0, 8);
      return this.unsigned(buf[0], 0L) | this.unsigned(buf[1], 8L) | this.unsigned(buf[2], 16L) | this.unsigned(buf[3], 24L) | this.unsigned(buf[4], 32L) | this.unsigned(buf[5], 40L) | this.unsigned(buf[6], 48L) | this.unsigned(buf[7], 56L);
   }

   private long getLoadBase(String libPath) {
      RandomAccessFile file;
      try {
         file = new RandomAccessFile(libPath, "r");
      } catch (FileNotFoundException var28) {
         return 0L;
      }

      byte[] buffer = new byte[8];

      try {
         file.readFully(buffer, 0, 6);
      } catch (IOException var27) {
         return 0L;
      }

      if(buffer[0] == 127 && buffer[1] == 69 && buffer[2] == 76 && buffer[3] == 70 && buffer[5] == 1) {
         boolean elf32;
         long elfPhdrSize;
         long ePhnumOffset;
         long ePhoffOffset;
         long pTypeOffset;
         long pOffsetOffset;
         long pVaddrOffset;
         if(buffer[4] == 1) {
            elf32 = true;
            elfPhdrSize = 32L;
            ePhnumOffset = 44L;
            ePhoffOffset = 28L;
            pTypeOffset = 0L;
            pOffsetOffset = 4L;
            pVaddrOffset = 8L;
         } else {
            if(buffer[4] != 2) {
               return 0L;
            }

            elf32 = false;
            elfPhdrSize = 56L;
            ePhnumOffset = 56L;
            ePhoffOffset = 32L;
            pTypeOffset = 0L;
            pOffsetOffset = 8L;
            pVaddrOffset = 16L;
         }

         try {
            short e = this.elfGetHalfWord(file, ePhnumOffset);
            long offset;
            if(elf32) {
               offset = (long)this.elfGetWord(file, ePhoffOffset);
            } else {
               offset = this.elfGetDoubleWord(file, ePhoffOffset);
            }

            for(int i = 0; i < e; ++i) {
               int pType = this.elfGetWord(file, offset + pTypeOffset);
               long pOffset;
               if(elf32) {
                  pOffset = (long)this.elfGetWord(file, offset + pOffsetOffset);
               } else {
                  pOffset = this.elfGetDoubleWord(file, offset + pOffsetOffset);
               }

               if(pType == 1 && pOffset == 0L) {
                  long pVaddr;
                  if(elf32) {
                     pVaddr = (long)this.elfGetWord(file, offset + pVaddrOffset);
                  } else {
                     pVaddr = this.elfGetDoubleWord(file, offset + pVaddrOffset);
                  }

                  return pVaddr;
               }

               offset += elfPhdrSize;
            }

            return 0L;
         } catch (IOException var26) {
            return 0L;
         }
      } else {
         return 0L;
      }
   }

   private void resolveAddresses(NativeLibraryMapInfo lib, String libPath, Set<Long> addressesToResolve) {
      Process addr2line;
      try {
         addr2line = (new ProcessBuilder(new String[]{this.mAddr2LineCmd, "-C", "-f", "-e", libPath})).start();
      } catch (IOException var18) {
         this.mAddr2LineErrorMessage = var18.getMessage();
         this.markAddressesNotResolvable(addressesToResolve, lib);
         return;
      }

      BufferedReader resultReader = new BufferedReader(new InputStreamReader(addr2line.getInputStream()));
      BufferedWriter addressWriter = new BufferedWriter(new OutputStreamWriter(addr2line.getOutputStream()));
      long libStartAddress = this.isExecutable(lib)?0L:lib.getStartAddress();
      long libLoadBase = this.isExecutable(lib)?0L:this.getLoadBase(libPath);

      try {
         Iterator e = addressesToResolve.iterator();

         while(e.hasNext()) {
            Long i$1 = (Long)e.next();
            long addr1 = i$1.longValue() - libStartAddress + libLoadBase;
            addressWriter.write(Long.toHexString(addr1));
            addressWriter.newLine();
            addressWriter.flush();
            String method = resultReader.readLine();
            String sourceFile = resultReader.readLine();
            this.mAddressResolution.put(i$1, new NativeStackCallInfo(i$1.longValue(), lib.getLibraryName(), method, sourceFile));
         }
      } catch (IOException var19) {
         Iterator i$ = addressesToResolve.iterator();

         while(i$.hasNext()) {
            Long addr = (Long)i$.next();
            if(this.mAddressResolution.get(addr) == null) {
               this.markAddressNotResolvable(lib, addr);
            }
         }
      }

      try {
         resultReader.close();
         addressWriter.close();
      } catch (IOException var17) {
         ;
      }

      addr2line.destroy();
   }

   private boolean isExecutable(NativeLibraryMapInfo object) {
      String devicePath = object.getLibraryName();
      return devicePath.contains("/bin/");
   }

   private void markAddressesNotResolvable(Set<Long> addressesToResolve, NativeLibraryMapInfo lib) {
      Iterator i$ = addressesToResolve.iterator();

      while(i$.hasNext()) {
         Long addr = (Long)i$.next();
         this.markAddressNotResolvable(lib, addr);
      }

   }

   private void markAddressNotResolvable(NativeLibraryMapInfo lib, Long addr) {
      this.mAddressResolution.put(addr, new NativeStackCallInfo(addr.longValue(), lib.getLibraryName(), Long.toHexString(addr.longValue()), ""));
      this.mUnresolvableAddresses.add(addr);
   }

   private String getLibraryLocation(NativeLibraryMapInfo lib) {
      String pathOnDevice = lib.getLibraryName();
      String libName = (new File(pathOnDevice)).getName();
      Iterator i$ = this.mSymbolSearchFolders.iterator();

      String fullPath;
      do {
         if(!i$.hasNext()) {
            return null;
         }

         String p = (String)i$.next();
         fullPath = p + File.separator + pathOnDevice;
         if((new File(fullPath)).exists()) {
            return fullPath;
         }

         fullPath = p + File.separator + libName;
      } while(!(new File(fullPath)).exists());

      return fullPath;
   }

   private void resolveCallSites(List<NativeAllocationInfo> callSites) {
      Iterator i$ = callSites.iterator();

      while(i$.hasNext()) {
         NativeAllocationInfo callSite = (NativeAllocationInfo)i$.next();
         ArrayList stackInfo = new ArrayList();
         Iterator i$1 = callSite.getStackCallAddresses().iterator();

         while(i$1.hasNext()) {
            Long addr = (Long)i$1.next();
            NativeStackCallInfo info = (NativeStackCallInfo)this.mAddressResolution.get(addr);
            if(info != null) {
               stackInfo.add(info);
            }
         }

         callSite.setResolvedStackCall(stackInfo);
      }

   }

   private void checkCancellation(IProgressMonitor monitor) throws InterruptedException {
      if(monitor.isCanceled()) {
         throw new InterruptedException();
      }
   }

   public String getAddr2LineErrorMessage() {
      return this.mAddr2LineErrorMessage;
   }

   public Set<Long> getUnmappedAddresses() {
      return this.mUnmappedAddresses;
   }

   public Set<Long> getUnresolvableAddresses() {
      return this.mUnresolvableAddresses;
   }

   public Set<String> getNotFoundLibraries() {
      return this.mNotFoundLibraries;
   }

   static {
      String addr2lineEnv = System.getenv("ANDROID_ADDR2LINE");
      String addr2line64Env = System.getenv("ANDROID_ADDR2LINE64");
      ADDR2LINE = addr2lineEnv != null?addr2lineEnv:DdmUiPreferences.getAddr2Line();
      ADDR2LINE64 = addr2line64Env != null?addr2line64Env:DdmUiPreferences.getAddr2Line64();
      String symbols = System.getenv("ANDROID_SYMBOLS");
      DEFAULT_SYMBOLS_FOLDER = symbols != null?symbols:DdmUiPreferences.getSymbolDirectory();
   }
}
