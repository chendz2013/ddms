package com.android.ddmuilib.heap;

import com.android.ddmlib.Client;
import com.android.ddmlib.Log;
import com.android.ddmlib.NativeAllocationInfo;
import com.android.ddmlib.NativeLibraryMapInfo;
import com.android.ddmlib.NativeStackCallInfo;
import com.android.ddmuilib.Addr2Line;
import com.android.ddmuilib.BaseHeapPanel;
import com.android.ddmuilib.ITableFocusListener;
import com.android.ddmuilib.ImageLoader;
import com.android.ddmuilib.TableHelper;
import com.android.ddmuilib.heap.NativeHeapDataImporter;
import com.android.ddmuilib.heap.NativeHeapDiffSnapshot;
import com.android.ddmuilib.heap.NativeHeapLabelProvider;
import com.android.ddmuilib.heap.NativeHeapProviderByAllocations;
import com.android.ddmuilib.heap.NativeHeapProviderByLibrary;
import com.android.ddmuilib.heap.NativeHeapSnapshot;
import com.android.ddmuilib.heap.NativeStackContentProvider;
import com.android.ddmuilib.heap.NativeStackLabelProvider;
import com.android.ddmuilib.heap.NativeSymbolResolverTask;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Sash;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

public class NativeHeapPanel extends BaseHeapPanel {
   private static final boolean USE_OLD_RESOLVER;
   private final int MAX_DISPLAYED_ERROR_ITEMS = 5;
   private static final String TOOLTIP_EXPORT_DATA = "Export Heap Data";
   private static final String TOOLTIP_ZYGOTE_ALLOCATIONS = "Show Zygote Allocations";
   private static final String TOOLTIP_DIFFS_ONLY = "Only show new allocations not present in previous snapshot";
   private static final String TOOLTIP_GROUPBY = "Group allocations by library.";
   private static final String EXPORT_DATA_IMAGE = "save.png";
   private static final String ZYGOTE_IMAGE = "zygote.png";
   private static final String DIFFS_ONLY_IMAGE = "diff.png";
   private static final String GROUPBY_IMAGE = "groupby.png";
   private static final String SNAPSHOT_HEAP_BUTTON_TEXT = "Snapshot Current Native Heap Usage";
   private static final String LOAD_HEAP_DATA_BUTTON_TEXT = "Import Heap Data";
   private static final String SYMBOL_SEARCH_PATH_LABEL_TEXT = "Symbol Search Path:";
   private static final String SYMBOL_SEARCH_PATH_TEXT_MESSAGE = "List of colon separated paths to search for symbol debug information. See tooltip for examples.";
   private static final String SYMBOL_SEARCH_PATH_TOOLTIP_TEXT = "Colon separated paths that contain unstripped libraries with debug symbols.\ne.g.: <android-src>/out/target/product/generic/symbols/system/lib:/path/to/my/app/obj/local/armeabi";
   private static final String PREFS_SHOW_DIFFS_ONLY = "nativeheap.show.diffs.only";
   private static final String PREFS_SHOW_ZYGOTE_ALLOCATIONS = "nativeheap.show.zygote";
   private static final String PREFS_GROUP_BY_LIBRARY = "nativeheap.grouby.library";
   private static final String PREFS_SYMBOL_SEARCH_PATH = "nativeheap.search.path";
   private static final String PREFS_SASH_HEIGHT_PERCENT = "nativeheap.sash.percent";
   private static final String PREFS_LAST_IMPORTED_HEAPPATH = "nativeheap.last.import.path";
   private IPreferenceStore mPrefStore;
   private List<NativeHeapSnapshot> mNativeHeapSnapshots;
   private List<NativeHeapSnapshot> mDiffSnapshots;
   private Map<Integer, List<NativeHeapSnapshot>> mImportedSnapshotsPerPid;
   private Button mSnapshotHeapButton;
   private Button mLoadHeapDataButton;
   private Text mSymbolSearchPathText;
   private Combo mSnapshotIndexCombo;
   private Label mMemoryAllocatedText;
   private TreeViewer mDetailsTreeViewer;
   private TreeViewer mStackTraceTreeViewer;
   private NativeHeapProviderByAllocations mContentProviderByAllocations;
   private NativeHeapProviderByLibrary mContentProviderByLibrary;
   private NativeHeapLabelProvider mDetailsTreeLabelProvider;
   private ToolItem mGroupByButton;
   private ToolItem mDiffsOnlyButton;
   private ToolItem mShowZygoteAllocationsButton;
   private ToolItem mExportHeapDataButton;
   private ITableFocusListener mTableFocusListener;

   public NativeHeapPanel(IPreferenceStore prefStore) {
      this.mPrefStore = prefStore;
      this.mPrefStore.setDefault("nativeheap.sash.percent", 75);
      this.mPrefStore.setDefault("nativeheap.search.path", "");
      this.mPrefStore.setDefault("nativeheap.grouby.library", false);
      this.mPrefStore.setDefault("nativeheap.show.zygote", true);
      this.mPrefStore.setDefault("nativeheap.show.diffs.only", false);
      this.mNativeHeapSnapshots = new ArrayList();
      this.mDiffSnapshots = new ArrayList();
      this.mImportedSnapshotsPerPid = new HashMap();
   }

   public void clientChanged(final Client client, int changeMask) {
      if(client == this.getCurrentClient()) {
         if((changeMask & 128) == 128) {
            List allocations = client.getClientData().getNativeAllocationList();
            if(allocations.size() != 0) {
               final List nativeAllocations = this.shallowCloneList(allocations);
               this.addNativeHeapSnapshot(new NativeHeapSnapshot(nativeAllocations));
               this.updateDisplay();
               if(USE_OLD_RESOLVER) {
                  Thread t = new Thread(new NativeHeapPanel.SymbolResolverTask(nativeAllocations, client.getClientData().getMappedNativeLibraries()));
                  t.setName("Address to Symbol Resolver");
                  t.start();
               } else {
                  Display.getDefault().asyncExec(new Runnable() {
                     public void run() {
                        this.resolveSymbols();
                        NativeHeapPanel.this.mDetailsTreeViewer.refresh();
                        NativeHeapPanel.this.mStackTraceTreeViewer.refresh();
                     }

                     public void resolveSymbols() {
                        Shell shell = Display.getDefault().getActiveShell();
                        ProgressMonitorDialog d = new ProgressMonitorDialog(shell);
                        NativeSymbolResolverTask resolver = new NativeSymbolResolverTask(nativeAllocations, client.getClientData().getMappedNativeLibraries(), NativeHeapPanel.this.mSymbolSearchPathText.getText(), client.getClientData().getAbi());

                        try {
                           d.run(true, true, resolver);
                        } catch (InvocationTargetException var5) {
                           MessageDialog.openError(shell, "Error Resolving Symbols", var5.getCause().getMessage());
                           return;
                        } catch (InterruptedException var6) {
                           return;
                        }

                        MessageDialog.openInformation(shell, "Symbol Resolution Status", NativeHeapPanel.this.getResolutionStatusMessage(resolver));
                     }
                  });
               }

            }
         }
      }
   }

   private String getResolutionStatusMessage(NativeSymbolResolverTask resolver) {
      StringBuilder sb = new StringBuilder();
      sb.append("Symbol Resolution Complete.\n\n");
      Set unmappedAddresses = resolver.getUnmappedAddresses();
      if(unmappedAddresses.size() > 0) {
         sb.append(String.format("Unmapped addresses (%d): ", new Object[]{Integer.valueOf(unmappedAddresses.size())}));
         sb.append(this.getSampleForDisplay(unmappedAddresses));
         sb.append('\n');
      }

      Set notFoundLibraries = resolver.getNotFoundLibraries();
      if(notFoundLibraries.size() > 0) {
         sb.append(String.format("Libraries not found on disk (%d): ", new Object[]{Integer.valueOf(notFoundLibraries.size())}));
         sb.append(this.getSampleForDisplay(notFoundLibraries));
         sb.append('\n');
      }

      Set unresolvableAddresses = resolver.getUnresolvableAddresses();
      if(unresolvableAddresses.size() > 0) {
         sb.append(String.format("Unresolved addresses (%d): ", new Object[]{Integer.valueOf(unresolvableAddresses.size())}));
         sb.append(this.getSampleForDisplay(unresolvableAddresses));
         sb.append('\n');
      }

      if(resolver.getAddr2LineErrorMessage() != null) {
         sb.append("Error launching addr2line: ");
         sb.append(resolver.getAddr2LineErrorMessage());
      }

      return sb.toString();
   }

   private String getSampleForDisplay(Collection<?> items) {
      StringBuilder sb = new StringBuilder();
      int c = 1;

      for(Iterator it = items.iterator(); it.hasNext(); ++c) {
         Object item = it.next();
         if(item instanceof Long) {
            sb.append(String.format("0x%x", new Object[]{item}));
         } else {
            sb.append(item);
         }

         if(c == 5 && it.hasNext()) {
            sb.append(", ...");
            break;
         }

         if(it.hasNext()) {
            sb.append(", ");
         }
      }

      return sb.toString();
   }

   private void addNativeHeapSnapshot(NativeHeapSnapshot snapshot) {
      this.mNativeHeapSnapshots.add(snapshot);
      this.mDiffSnapshots.add(null);
   }

   private List<NativeAllocationInfo> shallowCloneList(List<NativeAllocationInfo> allocations) {
      ArrayList clonedList = new ArrayList(allocations.size());
      Iterator i$ = allocations.iterator();

      while(i$.hasNext()) {
         NativeAllocationInfo i = (NativeAllocationInfo)i$.next();
         clonedList.add(i);
      }

      return clonedList;
   }

   public void deviceSelected() {
   }

   public void clientSelected() {
      Client c = this.getCurrentClient();
      if(c == null) {
         this.mSnapshotHeapButton.setEnabled(false);
         this.mLoadHeapDataButton.setEnabled(false);
      } else {
         this.mNativeHeapSnapshots = new ArrayList();
         this.mDiffSnapshots = new ArrayList();
         this.mSnapshotHeapButton.setEnabled(true);
         this.mLoadHeapDataButton.setEnabled(true);
         List importedSnapshots = (List)this.mImportedSnapshotsPerPid.get(Integer.valueOf(c.getClientData().getPid()));
         if(importedSnapshots != null) {
            Iterator allocations = importedSnapshots.iterator();

            while(allocations.hasNext()) {
               NativeHeapSnapshot n = (NativeHeapSnapshot)allocations.next();
               this.addNativeHeapSnapshot(n);
            }
         }

         List allocations1 = c.getClientData().getNativeAllocationList();
         allocations1 = this.shallowCloneList(allocations1);
         if(allocations1.size() > 0) {
            this.addNativeHeapSnapshot(new NativeHeapSnapshot(allocations1));
         }

         this.updateDisplay();
      }
   }

   private void updateDisplay() {
      Display.getDefault().syncExec(new Runnable() {
         public void run() {
            NativeHeapPanel.this.updateSnapshotIndexCombo();
            NativeHeapPanel.this.updateToolbars();
            int lastSnapshotIndex = NativeHeapPanel.this.mNativeHeapSnapshots.size() - 1;
            NativeHeapPanel.this.displaySnapshot(lastSnapshotIndex);
            NativeHeapPanel.this.displayStackTraceForSelection();
         }
      });
   }

   private void displaySelectedSnapshot() {
      Display.getDefault().syncExec(new Runnable() {
         public void run() {
            int idx = NativeHeapPanel.this.mSnapshotIndexCombo.getSelectionIndex();
            NativeHeapPanel.this.displaySnapshot(idx);
         }
      });
   }

   private void displaySnapshot(int index) {
      if(index >= 0 && this.mNativeHeapSnapshots.size() != 0) {
         assert index < this.mNativeHeapSnapshots.size() : "Invalid snapshot index";

         NativeHeapSnapshot snapshot = (NativeHeapSnapshot)this.mNativeHeapSnapshots.get(index);
         if(this.mDiffsOnlyButton.getSelection() && index > 0) {
            snapshot = this.getDiffSnapshot(index);
         }

         this.mMemoryAllocatedText.setText(snapshot.getFormattedMemorySize());
         this.mMemoryAllocatedText.pack();
         this.mDetailsTreeLabelProvider.setTotalSize(snapshot.getTotalSize());
         this.mDetailsTreeViewer.setInput(snapshot);
         this.mDetailsTreeViewer.refresh();
      } else {
         this.mDetailsTreeViewer.setInput((Object)null);
         this.mMemoryAllocatedText.setText("");
      }
   }

   private NativeHeapSnapshot getDiffSnapshot(int index) {
      NativeHeapSnapshot diffSnapshot = (NativeHeapSnapshot)this.mDiffSnapshots.get(index);
      if(diffSnapshot != null) {
         return diffSnapshot;
      } else {
         NativeHeapSnapshot cur = (NativeHeapSnapshot)this.mNativeHeapSnapshots.get(index);
         NativeHeapSnapshot prev = (NativeHeapSnapshot)this.mNativeHeapSnapshots.get(index - 1);
         NativeHeapDiffSnapshot diffSnapshot1 = new NativeHeapDiffSnapshot(cur, prev);
         this.mDiffSnapshots.set(index, diffSnapshot1);
         return diffSnapshot1;
      }
   }

   private void updateDisplayGrouping() {
      boolean groupByLibrary = this.mGroupByButton.getSelection();
      this.mPrefStore.setValue("nativeheap.grouby.library", groupByLibrary);
      if(groupByLibrary) {
         this.mDetailsTreeViewer.setContentProvider(this.mContentProviderByLibrary);
      } else {
         this.mDetailsTreeViewer.setContentProvider(this.mContentProviderByAllocations);
      }

   }

   private void updateDisplayForZygotes() {
      boolean displayZygoteMemory = this.mShowZygoteAllocationsButton.getSelection();
      this.mPrefStore.setValue("nativeheap.show.zygote", displayZygoteMemory);
      this.mContentProviderByLibrary.displayZygoteMemory(displayZygoteMemory);
      this.mContentProviderByAllocations.displayZygoteMemory(displayZygoteMemory);
      this.mDetailsTreeViewer.refresh();
   }

   private void updateSnapshotIndexCombo() {
      ArrayList items = new ArrayList();
      int numSnapshots = this.mNativeHeapSnapshots.size();

      for(int i = 0; i < numSnapshots; ++i) {
         items.add("Snapshot " + (i + 1));
      }

      this.mSnapshotIndexCombo.setItems((String[])items.toArray(new String[items.size()]));
      if(numSnapshots > 0) {
         this.mSnapshotIndexCombo.setEnabled(true);
         this.mSnapshotIndexCombo.select(numSnapshots - 1);
      } else {
         this.mSnapshotIndexCombo.setEnabled(false);
      }

   }

   private void updateToolbars() {
      int numSnapshots = this.mNativeHeapSnapshots.size();
      this.mExportHeapDataButton.setEnabled(numSnapshots > 0);
   }

   protected Control createControl(Composite parent) {
      Composite c = new Composite(parent, 0);
      c.setLayout(new GridLayout(1, false));
      c.setLayoutData(new GridData(1808));
      this.createControlsSection(c);
      this.createDetailsSection(c);
      this.clientSelected();
      return c;
   }

   private void createControlsSection(Composite parent) {
      Composite c = new Composite(parent, 0);
      c.setLayout(new GridLayout(3, false));
      c.setLayoutData(new GridData(768));
      this.createGetHeapDataSection(c);
      Label l = new Label(c, 514);
      l.setLayoutData(new GridData(1040));
      this.createDisplaySection(c);
   }

   private void createGetHeapDataSection(Composite parent) {
      Composite c = new Composite(parent, 0);
      c.setLayout(new GridLayout(1, false));
      this.createTakeHeapSnapshotButton(c);
      Label l = new Label(c, 258);
      l.setLayoutData(new GridData(768));
      this.createLoadHeapDataButton(c);
   }

   private void createTakeHeapSnapshotButton(Composite parent) {
      this.mSnapshotHeapButton = new Button(parent, 2056);
      this.mSnapshotHeapButton.setText("Snapshot Current Native Heap Usage");
      this.mSnapshotHeapButton.setLayoutData(new GridData());
      this.mSnapshotHeapButton.setEnabled(false);
      this.mSnapshotHeapButton.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent evt) {
            NativeHeapPanel.this.snapshotHeap();
         }
      });
   }

   private void snapshotHeap() {
      Client c = this.getCurrentClient();

      assert c != null : "Snapshot Heap could not have been enabled w/o a selected client.";

      c.requestNativeHeapInformation();
   }

   private void createLoadHeapDataButton(Composite parent) {
      this.mLoadHeapDataButton = new Button(parent, 2056);
      this.mLoadHeapDataButton.setText("Import Heap Data");
      this.mLoadHeapDataButton.setLayoutData(new GridData());
      this.mLoadHeapDataButton.setEnabled(false);
      this.mLoadHeapDataButton.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent evt) {
            NativeHeapPanel.this.loadHeapDataFromFile();
         }
      });
   }

   private void loadHeapDataFromFile() {
      String path = this.getHeapDumpToImport();
      if(path != null) {
         FileReader reader = null;

         try {
            reader = new FileReader(path);
         } catch (FileNotFoundException var9) {
            ;
         }

         Shell shell = Display.getDefault().getActiveShell();
         ProgressMonitorDialog d = new ProgressMonitorDialog(shell);
         NativeHeapDataImporter importer = new NativeHeapDataImporter(reader);

         try {
            d.run(true, true, importer);
         } catch (InvocationTargetException var7) {
            MessageDialog.openError(shell, "Error Importing Heap Data", var7.getCause().getMessage());
            return;
         } catch (InterruptedException var8) {
            return;
         }

         NativeHeapSnapshot snapshot = importer.getImportedSnapshot();
         this.addToImportedSnapshots(snapshot);
         this.addNativeHeapSnapshot(snapshot);
         this.updateDisplay();
      }
   }

   private void addToImportedSnapshots(NativeHeapSnapshot snapshot) {
      Client c = this.getCurrentClient();
      if(c != null) {
         Integer pid = Integer.valueOf(c.getClientData().getPid());
         List importedSnapshots = (List)this.mImportedSnapshotsPerPid.get(pid);
         if(importedSnapshots == null) {
            importedSnapshots = new ArrayList();
         }

         ((List)importedSnapshots).add(snapshot);
         this.mImportedSnapshotsPerPid.put(pid, importedSnapshots);
      }
   }

   private String getHeapDumpToImport() {
      FileDialog fileDialog = new FileDialog(Display.getDefault().getActiveShell(), 4096);
      fileDialog.setText("Import Heap Dump");
      fileDialog.setFilterExtensions(new String[]{"*.txt"});
      fileDialog.setFilterPath(this.mPrefStore.getString("nativeheap.last.import.path"));
      String selectedFile = fileDialog.open();
      if(selectedFile != null) {
         this.mPrefStore.setValue("nativeheap.last.import.path", (new File(selectedFile)).getParent());
      }

      return selectedFile;
   }

   private void createDisplaySection(Composite parent) {
      Composite c = new Composite(parent, 0);
      c.setLayout(new GridLayout(2, false));
      c.setLayoutData(new GridData(768));
      this.createLabel(c, "Display:");
      this.mSnapshotIndexCombo = new Combo(c, 8);
      this.mSnapshotIndexCombo.setItems(new String[]{"No heap snapshots available."});
      this.mSnapshotIndexCombo.setEnabled(false);
      this.mSnapshotIndexCombo.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent arg0) {
            NativeHeapPanel.this.displaySelectedSnapshot();
         }
      });
      this.createLabel(c, "Memory Allocated:");
      this.mMemoryAllocatedText = new Label(c, 0);
      GridData gd = new GridData();
      gd.widthHint = 100;
      this.mMemoryAllocatedText.setLayoutData(gd);
      this.createLabel(c, "Symbol Search Path:");
      this.mSymbolSearchPathText = new Text(c, 2048);
      this.mSymbolSearchPathText.setMessage("List of colon separated paths to search for symbol debug information. See tooltip for examples.");
      this.mSymbolSearchPathText.setToolTipText("Colon separated paths that contain unstripped libraries with debug symbols.\ne.g.: <android-src>/out/target/product/generic/symbols/system/lib:/path/to/my/app/obj/local/armeabi");
      this.mSymbolSearchPathText.addModifyListener(new ModifyListener() {
         public void modifyText(ModifyEvent arg0) {
            String path = NativeHeapPanel.this.mSymbolSearchPathText.getText();
            NativeHeapPanel.this.updateSearchPath(path);
            NativeHeapPanel.this.mPrefStore.setValue("nativeheap.search.path", path);
         }
      });
      this.mSymbolSearchPathText.setText(this.mPrefStore.getString("nativeheap.search.path"));
      this.mSymbolSearchPathText.setLayoutData(new GridData(768));
   }

   private void updateSearchPath(String path) {
      Addr2Line.setSearchPath(path);
   }

   private void createLabel(Composite parent, String text) {
      Label l = new Label(parent, 0);
      l.setText(text);
      GridData gd = new GridData();
      gd.horizontalAlignment = 131072;
      l.setLayoutData(gd);
   }

   private void createDetailsSection(Composite parent) {
      final Composite c = new Composite(parent, 0);
      c.setLayout(new FormLayout());
      c.setLayoutData(new GridData(1808));
      ToolBar detailsToolBar = new ToolBar(c, 8390656);
      this.initializeDetailsToolBar(detailsToolBar);
      Tree detailsTree = new Tree(c, 268437506);
      this.initializeDetailsTree(detailsTree);
      final Sash sash = new Sash(c, 2304);
      Label stackTraceLabel = new Label(c, 0);
      stackTraceLabel.setText("Stack Trace:");
      Tree stackTraceTree = new Tree(c, 2050);
      this.initializeStackTraceTree(stackTraceTree);
      FormData data = new FormData();
      data.top = new FormAttachment(0, 0);
      data.left = new FormAttachment(0, 0);
      data.right = new FormAttachment(100, 0);
      detailsToolBar.setLayoutData(data);
      data = new FormData();
      data.top = new FormAttachment(detailsToolBar, 0);
      data.bottom = new FormAttachment(sash, 0);
      data.left = new FormAttachment(0, 0);
      data.right = new FormAttachment(100, 0);
      detailsTree.setLayoutData(data);
      final FormData sashData = new FormData();
      sashData.top = new FormAttachment(this.mPrefStore.getInt("nativeheap.sash.percent"), 0);
      sashData.left = new FormAttachment(0, 0);
      sashData.right = new FormAttachment(100, 0);
      sash.setLayoutData(sashData);
      data = new FormData();
      data.top = new FormAttachment(sash, 0);
      data.left = new FormAttachment(0, 0);
      data.right = new FormAttachment(100, 0);
      stackTraceLabel.setLayoutData(data);
      data = new FormData();
      data.top = new FormAttachment(stackTraceLabel, 0);
      data.left = new FormAttachment(0, 0);
      data.bottom = new FormAttachment(100, 0);
      data.right = new FormAttachment(100, 0);
      stackTraceTree.setLayoutData(data);
      sash.addListener(13, new Listener() {
         public void handleEvent(Event e) {
            Rectangle sashRect = sash.getBounds();
            Rectangle panelRect = c.getClientArea();
            int sashPercent = sashRect.y * 100 / panelRect.height;
            NativeHeapPanel.this.mPrefStore.setValue("nativeheap.sash.percent", sashPercent);
            sashData.top = new FormAttachment(0, e.y);
            c.layout();
         }
      });
   }

   private void initializeDetailsToolBar(ToolBar toolbar) {
      this.mGroupByButton = new ToolItem(toolbar, 32);
      this.mGroupByButton.setImage(ImageLoader.getDdmUiLibLoader().loadImage("groupby.png", toolbar.getDisplay()));
      this.mGroupByButton.setToolTipText("Group allocations by library.");
      this.mGroupByButton.setSelection(this.mPrefStore.getBoolean("nativeheap.grouby.library"));
      this.mGroupByButton.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent arg0) {
            NativeHeapPanel.this.updateDisplayGrouping();
         }
      });
      this.mDiffsOnlyButton = new ToolItem(toolbar, 32);
      this.mDiffsOnlyButton.setImage(ImageLoader.getDdmUiLibLoader().loadImage("diff.png", toolbar.getDisplay()));
      this.mDiffsOnlyButton.setToolTipText("Only show new allocations not present in previous snapshot");
      this.mDiffsOnlyButton.setSelection(this.mPrefStore.getBoolean("nativeheap.show.diffs.only"));
      this.mDiffsOnlyButton.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent arg0) {
            int idx = NativeHeapPanel.this.mSnapshotIndexCombo.getSelectionIndex();
            NativeHeapPanel.this.displaySnapshot(idx);
         }
      });
      this.mShowZygoteAllocationsButton = new ToolItem(toolbar, 32);
      this.mShowZygoteAllocationsButton.setImage(ImageLoader.getDdmUiLibLoader().loadImage("zygote.png", toolbar.getDisplay()));
      this.mShowZygoteAllocationsButton.setToolTipText("Show Zygote Allocations");
      this.mShowZygoteAllocationsButton.setSelection(this.mPrefStore.getBoolean("nativeheap.show.zygote"));
      this.mShowZygoteAllocationsButton.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent arg0) {
            NativeHeapPanel.this.updateDisplayForZygotes();
         }
      });
      this.mExportHeapDataButton = new ToolItem(toolbar, 8);
      this.mExportHeapDataButton.setImage(ImageLoader.getDdmUiLibLoader().loadImage("save.png", toolbar.getDisplay()));
      this.mExportHeapDataButton.setToolTipText("Export Heap Data");
      this.mExportHeapDataButton.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent arg0) {
            NativeHeapPanel.this.exportSnapshot();
         }
      });
   }

   private void exportSnapshot() {
      int idx = this.mSnapshotIndexCombo.getSelectionIndex();
      String snapshotName = this.mSnapshotIndexCombo.getItem(idx);
      FileDialog fileDialog = new FileDialog(Display.getDefault().getActiveShell(), 8192);
      fileDialog.setText("Save " + snapshotName);
      fileDialog.setFileName("allocations.txt");
      final String fileName = fileDialog.open();
      if(fileName != null) {
         final NativeHeapSnapshot snapshot = (NativeHeapSnapshot)this.mNativeHeapSnapshots.get(idx);
         Thread t = new Thread(new Runnable() {
            public void run() {
               PrintWriter out;
               try {
                  out = new PrintWriter(new BufferedWriter(new FileWriter(fileName)));
               } catch (IOException var4) {
                  this.displayErrorMessage(var4.getMessage());
                  return;
               }

               Iterator i$ = snapshot.getAllocations().iterator();

               while(i$.hasNext()) {
                  NativeAllocationInfo alloc = (NativeAllocationInfo)i$.next();
                  out.println(alloc.toString());
               }

               out.close();
            }

            private void displayErrorMessage(final String message) {
               Display.getDefault().syncExec(new Runnable() {
                  public void run() {
                     MessageDialog.openError(Display.getDefault().getActiveShell(), "Failed to export heap data", message);
                  }
               });
            }
         });
         t.setName("Saving Heap Data to File...");
         t.start();
      }
   }

   private void initializeDetailsTree(Tree tree) {
      tree.setHeaderVisible(true);
      tree.setLinesVisible(true);
      List properties = Arrays.asList(new String[]{"Library", "Total", "Percentage", "Count", "Size", "Method"});
      List sampleValues = Arrays.asList(new String[]{"/path/in/device/to/system/library.so", "123456789", " 100%", "123456789", "123456789", "PossiblyLongDemangledMethodName"});
      List swtFlags = Arrays.asList(new Integer[]{Integer.valueOf(16384), Integer.valueOf(131072), Integer.valueOf(131072), Integer.valueOf(131072), Integer.valueOf(131072), Integer.valueOf(16384)});

      for(int displayZygotes = 0; displayZygotes < properties.size(); ++displayZygotes) {
         String p = (String)properties.get(displayZygotes);
         String v = (String)sampleValues.get(displayZygotes);
         int flags = ((Integer)swtFlags.get(displayZygotes)).intValue();
         TableHelper.createTreeColumn(tree, p, flags, v, this.getPref("details", p), this.mPrefStore);
      }

      this.mDetailsTreeViewer = new TreeViewer(tree);
      this.mDetailsTreeViewer.setUseHashlookup(true);
      boolean var9 = this.mPrefStore.getBoolean("nativeheap.show.zygote");
      this.mContentProviderByAllocations = new NativeHeapProviderByAllocations(this.mDetailsTreeViewer, var9);
      this.mContentProviderByLibrary = new NativeHeapProviderByLibrary(this.mDetailsTreeViewer, var9);
      if(this.mPrefStore.getBoolean("nativeheap.grouby.library")) {
         this.mDetailsTreeViewer.setContentProvider(this.mContentProviderByLibrary);
      } else {
         this.mDetailsTreeViewer.setContentProvider(this.mContentProviderByAllocations);
      }

      this.mDetailsTreeLabelProvider = new NativeHeapLabelProvider();
      this.mDetailsTreeViewer.setLabelProvider(this.mDetailsTreeLabelProvider);
      this.mDetailsTreeViewer.setInput((Object)null);
      tree.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent event) {
            NativeHeapPanel.this.displayStackTraceForSelection();
         }
      });
   }

   private void initializeStackTraceTree(Tree tree) {
      tree.setHeaderVisible(true);
      tree.setLinesVisible(true);
      List properties = Arrays.asList(new String[]{"Address", "Library", "Method", "File", "Line"});
      List sampleValues = Arrays.asList(new String[]{"0x1234_5678", "/path/in/device/to/system/library.so", "PossiblyLongDemangledMethodName", "/android/out/prefix/in/home/directory/to/path/in/device/to/system/library.so", "2000"});

      for(int i = 0; i < properties.size(); ++i) {
         String p = (String)properties.get(i);
         String v = (String)sampleValues.get(i);
         TableHelper.createTreeColumn(tree, p, 16384, v, this.getPref("stack", p), this.mPrefStore);
      }

      this.mStackTraceTreeViewer = new TreeViewer(tree);
      this.mStackTraceTreeViewer.setContentProvider(new NativeStackContentProvider());
      this.mStackTraceTreeViewer.setLabelProvider(new NativeStackLabelProvider());
      this.mStackTraceTreeViewer.setInput((Object)null);
   }

   private void displayStackTraceForSelection() {
      TreeItem[] items = this.mDetailsTreeViewer.getTree().getSelection();
      if(items.length == 0) {
         this.mStackTraceTreeViewer.setInput((Object)null);
      } else {
         Object data = items[0].getData();
         if(!(data instanceof NativeAllocationInfo)) {
            this.mStackTraceTreeViewer.setInput((Object)null);
         } else {
            NativeAllocationInfo info = (NativeAllocationInfo)data;
            if(info.isStackCallResolved()) {
               this.mStackTraceTreeViewer.setInput(info.getResolvedStackCall());
            } else {
               this.mStackTraceTreeViewer.setInput(info.getStackCallAddresses());
            }

         }
      }
   }

   private String getPref(String prefix, String s) {
      return "nativeheap.tree." + prefix + "." + s;
   }

   public void setFocus() {
   }

   public void setTableFocusListener(ITableFocusListener listener) {
      this.mTableFocusListener = listener;
      final Tree heapSitesTree = this.mDetailsTreeViewer.getTree();
      final ITableFocusListener.IFocusedTableActivator heapSitesActivator = new ITableFocusListener.IFocusedTableActivator() {
         public void copy(Clipboard clipboard) {
            TreeItem[] items = heapSitesTree.getSelection();
            NativeHeapPanel.this.copyToClipboard(items, clipboard);
         }

         public void selectAll() {
            heapSitesTree.selectAll();
         }
      };
      heapSitesTree.addFocusListener(new FocusListener() {
         public void focusLost(FocusEvent arg0) {
            NativeHeapPanel.this.mTableFocusListener.focusLost(heapSitesActivator);
         }

         public void focusGained(FocusEvent arg0) {
            NativeHeapPanel.this.mTableFocusListener.focusGained(heapSitesActivator);
         }
      });
      final Tree stackTraceTree = this.mStackTraceTreeViewer.getTree();
      final ITableFocusListener.IFocusedTableActivator stackTraceActivator = new ITableFocusListener.IFocusedTableActivator() {
         public void copy(Clipboard clipboard) {
            TreeItem[] items = stackTraceTree.getSelection();
            NativeHeapPanel.this.copyToClipboard(items, clipboard);
         }

         public void selectAll() {
            stackTraceTree.selectAll();
         }
      };
      stackTraceTree.addFocusListener(new FocusListener() {
         public void focusLost(FocusEvent arg0) {
            NativeHeapPanel.this.mTableFocusListener.focusLost(stackTraceActivator);
         }

         public void focusGained(FocusEvent arg0) {
            NativeHeapPanel.this.mTableFocusListener.focusGained(stackTraceActivator);
         }
      });
   }

   private void copyToClipboard(TreeItem[] items, Clipboard clipboard) {
      StringBuilder sb = new StringBuilder();
      TreeItem[] content = items;
      int len$ = items.length;

      for(int i$ = 0; i$ < len$; ++i$) {
         TreeItem item = content[i$];
         Object data = item.getData();
         if(data != null) {
            sb.append(data.toString());
            sb.append('\n');
         }
      }

      String var9 = sb.toString();
      if(var9.length() > 0) {
         clipboard.setContents(new Object[]{sb.toString()}, new Transfer[]{TextTransfer.getInstance()});
      }

   }

   static {
      String useOldResolver = System.getenv("ANDROID_DDMS_OLD_SYMRESOLVER");
      USE_OLD_RESOLVER = useOldResolver != null && useOldResolver.equalsIgnoreCase("true");
   }

   private class SymbolResolverTask implements Runnable {
      private List<NativeAllocationInfo> mCallSites;
      private List<NativeLibraryMapInfo> mMappedLibraries;
      private Map<Long, NativeStackCallInfo> mResolvedSymbolCache;

      public SymbolResolverTask(List<NativeAllocationInfo> var1, List<NativeLibraryMapInfo> var2) {
         this.mCallSites = var1;
         this.mMappedLibraries = var2;
         this.mResolvedSymbolCache = new HashMap();
      }

      public void run() {
         Iterator i$ = this.mCallSites.iterator();

         while(true) {
            NativeAllocationInfo callSite;
            do {
               if(!i$.hasNext()) {
                  Display.getDefault().asyncExec(new Runnable() {
                     public void run() {
                        NativeHeapPanel.this.mDetailsTreeViewer.refresh();
                        NativeHeapPanel.this.mStackTraceTreeViewer.refresh();
                     }
                  });
                  return;
               }

               callSite = (NativeAllocationInfo)i$.next();
            } while(callSite.isStackCallResolved());

            List addresses = callSite.getStackCallAddresses();
            ArrayList resolvedStackInfo = new ArrayList(addresses.size());
            Iterator i$1 = addresses.iterator();

            while(i$1.hasNext()) {
               Long address = (Long)i$1.next();
               NativeStackCallInfo info = (NativeStackCallInfo)this.mResolvedSymbolCache.get(address);
               if(info != null) {
                  resolvedStackInfo.add(info);
               } else {
                  info = this.resolveAddress(address.longValue());
                  resolvedStackInfo.add(info);
                  this.mResolvedSymbolCache.put(address, info);
               }
            }

            callSite.setResolvedStackCall(resolvedStackInfo);
         }
      }

      private NativeStackCallInfo resolveAddress(long addr) {
         NativeLibraryMapInfo library = this.getLibraryFor(addr);
         if(library != null) {
            Client c = NativeHeapPanel.this.getCurrentClient();
            Addr2Line process = Addr2Line.getProcess(library, c.getClientData().getAbi());
            if(process != null) {
               NativeStackCallInfo info = process.getAddress(addr);
               if(info != null) {
                  return info;
               }
            }
         }

         return new NativeStackCallInfo(addr, library != null?library.getLibraryName():null, Long.toHexString(addr), "");
      }

      private NativeLibraryMapInfo getLibraryFor(long addr) {
         Iterator i$ = this.mMappedLibraries.iterator();

         NativeLibraryMapInfo info;
         do {
            if(!i$.hasNext()) {
               Log.d("ddm-nativeheap", "Failed finding Library for " + Long.toHexString(addr));
               return null;
            }

            info = (NativeLibraryMapInfo)i$.next();
         } while(!info.isWithinLibrary(addr));

         return info;
      }
   }
}
