package com.android.ddmuilib.console;

public interface IDdmConsole {
   void printErrorToConsole(String var1);

   void printErrorToConsole(String[] var1);

   void printToConsole(String var1);

   void printToConsole(String[] var1);
}
