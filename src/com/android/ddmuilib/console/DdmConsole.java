package com.android.ddmuilib.console;

import com.android.ddmuilib.console.IDdmConsole;

public class DdmConsole {
   private static IDdmConsole mConsole;

   public static void printErrorToConsole(String message) {
      if(mConsole != null) {
         mConsole.printErrorToConsole(message);
      } else {
         System.err.println(message);
      }

   }

   public static void printErrorToConsole(String[] messages) {
      if(mConsole != null) {
         mConsole.printErrorToConsole(messages);
      } else {
         String[] arr$ = messages;
         int len$ = messages.length;

         for(int i$ = 0; i$ < len$; ++i$) {
            String message = arr$[i$];
            System.err.println(message);
         }
      }

   }

   public static void printToConsole(String message) {
      if(mConsole != null) {
         mConsole.printToConsole(message);
      } else {
         System.out.println(message);
      }

   }

   public static void printToConsole(String[] messages) {
      if(mConsole != null) {
         mConsole.printToConsole(messages);
      } else {
         String[] arr$ = messages;
         int len$ = messages.length;

         for(int i$ = 0; i$ < len$; ++i$) {
            String message = arr$[i$];
            System.out.println(message);
         }
      }

   }

   public static void setConsole(IDdmConsole console) {
      mConsole = console;
   }
}
