package com.android.ddmuilib;

public interface IFindTarget {
   boolean findAndSelect(String var1, boolean var2, boolean var3);
}
