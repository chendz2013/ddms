package com.android.ddmuilib;

import com.android.ddmlib.HeapSegment;
import com.android.ddmlib.ClientData.HeapData;
import com.android.ddmlib.HeapSegment.HeapSegmentElement;
import com.android.ddmuilib.TablePanel;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.PaletteData;

public abstract class BaseHeapPanel extends TablePanel {
   protected byte[] mProcessedHeapData;
   private Map<Integer, ArrayList<HeapSegmentElement>> mHeapMap;

   protected boolean serializeHeapData(HeapData heapData) {
      synchronized(heapData) {
         Collection heapSegments = heapData.getHeapSegments();
         if(heapSegments != null) {
            heapData.clearHeapData();
            this.doSerializeHeapData(heapSegments);
            heapData.setProcessedHeapData(this.mProcessedHeapData);
            heapData.setProcessedHeapMap(this.mHeapMap);
         } else {
            byte[] pixData = heapData.getProcessedHeapData();
            if(pixData == this.mProcessedHeapData) {
               return false;
            }

            this.mProcessedHeapData = pixData;
            Map heapMap = heapData.getProcessedHeapMap();
            this.mHeapMap = heapMap;
         }

         return true;
      }
   }

   protected byte[] getSerializedData() {
      return this.mProcessedHeapData;
   }

   private void doSerializeHeapData(Collection<HeapSegment> heapData) {
      this.mHeapMap = new TreeMap();
      ByteArrayOutputStream out = new ByteArrayOutputStream(4096);
      Iterator iterator = heapData.iterator();

      while(iterator.hasNext()) {
         HeapSegment elementLists = (HeapSegment)iterator.next();
         HeapSegmentElement i$ = null;

         while(true) {
            i$ = elementLists.getNextElement((HeapSegmentElement)null);
            if(i$ == null) {
               break;
            }

            int elementList;
            if(i$.getSolidity() == 0) {
               elementList = 1;
            } else {
               elementList = i$.getKind() + 2;
            }

            ArrayList elementList1 = (ArrayList)this.mHeapMap.get(Integer.valueOf(elementList));
            if(elementList1 == null) {
               elementList1 = new ArrayList();
               this.mHeapMap.put(Integer.valueOf(elementList), elementList1);
            }

            elementList1.add(i$);

            for(int len = i$.getLength() / 8; len > 0; --len) {
               out.write(elementList);
            }
         }
      }

      this.mProcessedHeapData = out.toByteArray();
      Collection var9 = this.mHeapMap.values();
      Iterator var10 = var9.iterator();

      while(var10.hasNext()) {
         ArrayList var11 = (ArrayList)var10.next();
         Collections.sort(var11);
      }

   }

   protected ImageData createLinearHeapImage(byte[] pixData, int h, PaletteData palette) {
      int w = pixData.length / h;
      if(pixData.length % h != 0) {
         ++w;
      }

      ImageData id = new ImageData(w, h, 8, palette);
      int x = 0;
      int y = 0;
      byte[] arr$ = pixData;
      int len$ = pixData.length;

      for(int i$ = 0; i$ < len$; ++i$) {
         byte b = arr$[i$];
         if(b >= 0) {
            id.setPixel(x, y, b);
         }

         ++y;
         if(y >= h) {
            y = 0;
            ++x;
         }
      }

      return id;
   }
}
