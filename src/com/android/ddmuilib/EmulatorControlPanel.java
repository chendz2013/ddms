package com.android.ddmuilib;

import com.android.ddmlib.EmulatorConsole;
import com.android.ddmlib.IDevice;
import com.android.ddmlib.EmulatorConsole.GsmMode;
import com.android.ddmlib.EmulatorConsole.GsmStatus;
import com.android.ddmlib.EmulatorConsole.NetworkStatus;
import com.android.ddmuilib.DdmUiPreferences;
import com.android.ddmuilib.ImageLoader;
import com.android.ddmuilib.SelectionDependentPanel;
import com.android.ddmuilib.TableHelper;
import com.android.ddmuilib.location.CoordinateControls;
import com.android.ddmuilib.location.GpxParser;
import com.android.ddmuilib.location.KmlParser;
import com.android.ddmuilib.location.TrackContentProvider;
import com.android.ddmuilib.location.TrackLabelProvider;
import com.android.ddmuilib.location.TrackPoint;
import com.android.ddmuilib.location.WayPoint;
import com.android.ddmuilib.location.WayPointContentProvider;
import com.android.ddmuilib.location.WayPointLabelProvider;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Text;

public class EmulatorControlPanel extends SelectionDependentPanel {
   private static final double DEFAULT_LONGITUDE = -122.084095D;
   private static final double DEFAULT_LATITUDE = 37.422006D;
   private static final String SPEED_FORMAT = "Speed: %1$dX";
   private static final String[][] GSM_MODES;
   private static final String[] NETWORK_SPEEDS;
   private static final String[] NETWORK_LATENCIES;
   private static final int[] PLAY_SPEEDS;
   private static final String RE_PHONE_NUMBER = "^[+#0-9]+$";
   private static final String PREFS_WAYPOINT_COL_NAME = "emulatorControl.waypoint.name";
   private static final String PREFS_WAYPOINT_COL_LONGITUDE = "emulatorControl.waypoint.longitude";
   private static final String PREFS_WAYPOINT_COL_LATITUDE = "emulatorControl.waypoint.latitude";
   private static final String PREFS_WAYPOINT_COL_ELEVATION = "emulatorControl.waypoint.elevation";
   private static final String PREFS_WAYPOINT_COL_DESCRIPTION = "emulatorControl.waypoint.desc";
   private static final String PREFS_TRACK_COL_NAME = "emulatorControl.track.name";
   private static final String PREFS_TRACK_COL_COUNT = "emulatorControl.track.count";
   private static final String PREFS_TRACK_COL_FIRST = "emulatorControl.track.first";
   private static final String PREFS_TRACK_COL_LAST = "emulatorControl.track.last";
   private static final String PREFS_TRACK_COL_COMMENT = "emulatorControl.track.comment";
   private EmulatorConsole mEmulatorConsole;
   private Composite mParent;
   private Label mVoiceLabel;
   private Combo mVoiceMode;
   private Label mDataLabel;
   private Combo mDataMode;
   private Label mSpeedLabel;
   private Combo mNetworkSpeed;
   private Label mLatencyLabel;
   private Combo mNetworkLatency;
   private Label mNumberLabel;
   private Text mPhoneNumber;
   private Button mVoiceButton;
   private Button mSmsButton;
   private Label mMessageLabel;
   private Text mSmsMessage;
   private Button mCallButton;
   private Button mCancelButton;
   private TabFolder mLocationFolders;
   private Button mDecimalButton;
   private Button mSexagesimalButton;
   private CoordinateControls mLongitudeControls;
   private CoordinateControls mLatitudeControls;
   private Button mGpxUploadButton;
   private Table mGpxWayPointTable;
   private Table mGpxTrackTable;
   private Button mKmlUploadButton;
   private Table mKmlWayPointTable;
   private Button mPlayGpxButton;
   private Button mGpxBackwardButton;
   private Button mGpxForwardButton;
   private Button mGpxSpeedButton;
   private Button mPlayKmlButton;
   private Button mKmlBackwardButton;
   private Button mKmlForwardButton;
   private Button mKmlSpeedButton;
   private Image mPlayImage;
   private Image mPauseImage;
   private Thread mPlayingThread;
   private boolean mPlayingTrack;
   private int mPlayDirection = 1;
   private int mSpeed;
   private int mSpeedIndex;
   private final SelectionAdapter mDirectionButtonAdapter = new SelectionAdapter() {
      public void widgetSelected(SelectionEvent e) {
         Button b = (Button)e.getSource();
         if(!b.getSelection()) {
            b.setSelection(true);
         } else {
            if(b != EmulatorControlPanel.this.mGpxForwardButton && b != EmulatorControlPanel.this.mKmlForwardButton) {
               EmulatorControlPanel.this.mGpxBackwardButton.setSelection(true);
               EmulatorControlPanel.this.mGpxForwardButton.setSelection(false);
               EmulatorControlPanel.this.mKmlBackwardButton.setSelection(true);
               EmulatorControlPanel.this.mKmlForwardButton.setSelection(false);
               EmulatorControlPanel.this.mPlayDirection = -1;
            } else {
               EmulatorControlPanel.this.mGpxBackwardButton.setSelection(false);
               EmulatorControlPanel.this.mGpxForwardButton.setSelection(true);
               EmulatorControlPanel.this.mKmlBackwardButton.setSelection(false);
               EmulatorControlPanel.this.mKmlForwardButton.setSelection(true);
               EmulatorControlPanel.this.mPlayDirection = 1;
            }

         }
      }
   };
   private final SelectionAdapter mSpeedButtonAdapter = new SelectionAdapter() {
      public void widgetSelected(SelectionEvent e) {
         EmulatorControlPanel.this.mSpeedIndex = (EmulatorControlPanel.this.mSpeedIndex + 1) % EmulatorControlPanel.PLAY_SPEEDS.length;
         EmulatorControlPanel.this.mSpeed = EmulatorControlPanel.PLAY_SPEEDS[EmulatorControlPanel.this.mSpeedIndex];
         EmulatorControlPanel.this.mGpxSpeedButton.setText(String.format("Speed: %1$dX", new Object[]{Integer.valueOf(EmulatorControlPanel.this.mSpeed)}));
         EmulatorControlPanel.this.mGpxPlayControls.pack();
         EmulatorControlPanel.this.mKmlSpeedButton.setText(String.format("Speed: %1$dX", new Object[]{Integer.valueOf(EmulatorControlPanel.this.mSpeed)}));
         EmulatorControlPanel.this.mKmlPlayControls.pack();
         if(EmulatorControlPanel.this.mPlayingThread != null) {
            EmulatorControlPanel.this.mPlayingThread.interrupt();
         }

      }
   };
   private Composite mKmlPlayControls;
   private Composite mGpxPlayControls;

   public void deviceSelected() {
      this.handleNewDevice(this.getCurrentDevice());
   }

   public void clientSelected() {
   }

   protected Control createControl(Composite parent) {
      this.mParent = parent;
      final ScrolledComposite scollingParent = new ScrolledComposite(parent, 512);
      scollingParent.setExpandVertical(true);
      scollingParent.setExpandHorizontal(true);
      scollingParent.setLayoutData(new GridData(1808));
      final Composite top = new Composite(scollingParent, 0);
      scollingParent.setContent(top);
      top.setLayout(new GridLayout(1, false));
      scollingParent.addControlListener(new ControlAdapter() {
         public void controlResized(ControlEvent e) {
            Rectangle r = scollingParent.getClientArea();
            scollingParent.setMinSize(top.computeSize(r.width, -1));
         }
      });
      this.createRadioControls(top);
      this.createCallControls(top);
      this.createLocationControls(top);
      //设置enable
      this.doEnable(false);
      top.layout();
      Rectangle r = scollingParent.getClientArea();
      scollingParent.setMinSize(top.computeSize(r.width, -1));
      return scollingParent;
   }

   private void createRadioControls(Composite top) {
      Group g1 = new Group(top, 0);
      g1.setLayoutData(new GridData(768));
      g1.setLayout(new GridLayout(2, false));
      g1.setText("Telephony Status");
      Composite insideGroup = new Composite(g1, 0);
      GridLayout gl = new GridLayout(4, false);
      gl.marginBottom = gl.marginHeight = gl.marginLeft = gl.marginRight = 0;
      insideGroup.setLayout(gl);
      this.mVoiceLabel = new Label(insideGroup, 0);
      this.mVoiceLabel.setText("Voice:");
      this.mVoiceLabel.setAlignment(131072);
      this.mVoiceMode = new Combo(insideGroup, 8);
      this.mVoiceMode.setLayoutData(new GridData(768));
      String[][] l = GSM_MODES;
      int len$ = l.length;

      int i$;
      String[] mode;
      for(i$ = 0; i$ < len$; ++i$) {
         mode = l[i$];
         this.mVoiceMode.add(mode[0]);
      }

      this.mVoiceMode.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            EmulatorControlPanel.this.setVoiceMode(EmulatorControlPanel.this.mVoiceMode.getSelectionIndex());
         }
      });
      this.mSpeedLabel = new Label(insideGroup, 0);
      this.mSpeedLabel.setText("Speed:");
      this.mSpeedLabel.setAlignment(131072);
      this.mNetworkSpeed = new Combo(insideGroup, 8);
      this.mNetworkSpeed.setLayoutData(new GridData(768));
      String[] var9 = NETWORK_SPEEDS;
      len$ = var9.length;

      String var11;
      for(i$ = 0; i$ < len$; ++i$) {
         var11 = var9[i$];
         this.mNetworkSpeed.add(var11);
      }

      this.mNetworkSpeed.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            EmulatorControlPanel.this.setNetworkSpeed(EmulatorControlPanel.this.mNetworkSpeed.getSelectionIndex());
         }
      });
      this.mDataLabel = new Label(insideGroup, 0);
      this.mDataLabel.setText("Data:");
      this.mDataLabel.setAlignment(131072);
      this.mDataMode = new Combo(insideGroup, 8);
      this.mDataMode.setLayoutData(new GridData(768));
      l = GSM_MODES;
      len$ = l.length;

      for(i$ = 0; i$ < len$; ++i$) {
         mode = l[i$];
         this.mDataMode.add(mode[0]);
      }

      this.mDataMode.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            EmulatorControlPanel.this.setDataMode(EmulatorControlPanel.this.mDataMode.getSelectionIndex());
         }
      });
      this.mLatencyLabel = new Label(insideGroup, 0);
      this.mLatencyLabel.setText("Latency:");
      this.mLatencyLabel.setAlignment(131072);
      this.mNetworkLatency = new Combo(insideGroup, 8);
      this.mNetworkLatency.setLayoutData(new GridData(768));
      var9 = NETWORK_LATENCIES;
      len$ = var9.length;

      for(i$ = 0; i$ < len$; ++i$) {
         var11 = var9[i$];
         this.mNetworkLatency.add(var11);
      }

      this.mNetworkLatency.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            EmulatorControlPanel.this.setNetworkLatency(EmulatorControlPanel.this.mNetworkLatency.getSelectionIndex());
         }
      });
      Label var10 = new Label(g1, 0);
      var10.setLayoutData(new GridData(768));
   }

   private void createCallControls(Composite top) {
      Group g2 = new Group(top, 0);
      g2.setLayoutData(new GridData(768));
      g2.setLayout(new GridLayout(1, false));
      g2.setText("Telephony Actions");
      Composite phoneComp = new Composite(g2, 0);
      phoneComp.setLayoutData(new GridData(1808));
      GridLayout gl = new GridLayout(2, false);
      gl.marginBottom = gl.marginHeight = gl.marginLeft = gl.marginRight = 0;
      phoneComp.setLayout(gl);
      this.mNumberLabel = new Label(phoneComp, 0);
      this.mNumberLabel.setText("Incoming number:");
      this.mPhoneNumber = new Text(phoneComp, 18436);
      this.mPhoneNumber.setLayoutData(new GridData(768));
      this.mPhoneNumber.addModifyListener(new ModifyListener() {
         public void modifyText(ModifyEvent e) {
            EmulatorControlPanel.this.doEnable(EmulatorControlPanel.this.mEmulatorConsole != null);
         }
      });
      this.mVoiceButton = new Button(phoneComp, 16);
      GridData gd = new GridData();
      gd.horizontalSpan = 2;
      this.mVoiceButton.setText("Voice");
      this.mVoiceButton.setLayoutData(gd);
      this.mVoiceButton.setEnabled(false);
      this.mVoiceButton.setSelection(true);
      this.mVoiceButton.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            //设置可以
            EmulatorControlPanel.this.doEnable(true);
            if(EmulatorControlPanel.this.mVoiceButton.getSelection()) {
               EmulatorControlPanel.this.mCallButton.setText("Call");
            } else {
               EmulatorControlPanel.this.mCallButton.setText("Send");
            }

         }
      });
      this.mSmsButton = new Button(phoneComp, 16);
      this.mSmsButton.setText("SMS");
      gd = new GridData();
      gd.horizontalSpan = 2;
      this.mSmsButton.setLayoutData(gd);
      this.mSmsButton.setEnabled(false);
      this.mMessageLabel = new Label(phoneComp, 0);
      gd = new GridData();
      gd.verticalAlignment = 128;
      this.mMessageLabel.setLayoutData(gd);
      this.mMessageLabel.setText("Message:");
      this.mMessageLabel.setEnabled(false);
      this.mSmsMessage = new Text(phoneComp, 19010);
      this.mSmsMessage.setLayoutData(gd = new GridData(768));
      gd.heightHint = 70;
      this.mSmsMessage.setEnabled(false);
      Composite g2ButtonComp = new Composite(g2, 0);
      g2ButtonComp.setLayoutData(new GridData(768));
      gl = new GridLayout(2, false);
      gl.marginWidth = gl.marginHeight = 0;
      g2ButtonComp.setLayout(gl);
      this.mCallButton = new Button(g2ButtonComp, 8);
      this.mCallButton.setText("Call");
      this.mCallButton.setEnabled(false);
      this.mCallButton.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            if(EmulatorControlPanel.this.mEmulatorConsole != null) {
               if(EmulatorControlPanel.this.mVoiceButton.getSelection()) {
                  EmulatorControlPanel.this.processCommandResult(EmulatorControlPanel.this.mEmulatorConsole.call(EmulatorControlPanel.this.mPhoneNumber.getText().trim()));
               } else {
                  String message = EmulatorControlPanel.this.mSmsMessage.getText();
                  message = message.replaceAll("\\\\", "\\\\\\\\");
                  message = message.replaceAll("\r\n", "\\\\n");
                  message = message.replaceAll("\n", "\\\\n");
                  message = message.replaceAll("\r", "\\\\n");
                  EmulatorControlPanel.this.processCommandResult(EmulatorControlPanel.this.mEmulatorConsole.sendSms(EmulatorControlPanel.this.mPhoneNumber.getText().trim(), message));
               }
            }

         }
      });
      this.mCancelButton = new Button(g2ButtonComp, 8);
      this.mCancelButton.setText("Hang Up");
      this.mCancelButton.setEnabled(false);
      this.mCancelButton.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            if(EmulatorControlPanel.this.mEmulatorConsole != null && EmulatorControlPanel.this.mVoiceButton.getSelection()) {
               EmulatorControlPanel.this.processCommandResult(EmulatorControlPanel.this.mEmulatorConsole.cancelCall(EmulatorControlPanel.this.mPhoneNumber.getText().trim()));
            }

         }
      });
   }

   private void createLocationControls(Composite top) {
      Label l = new Label(top, 0);
      l.setLayoutData(new GridData(768));
      l.setText("Location Controls");
      this.mLocationFolders = new TabFolder(top, 0);
      this.mLocationFolders.setLayoutData(new GridData(768));
      Composite manualLocationComp = new Composite(this.mLocationFolders, 0);
      TabItem item = new TabItem(this.mLocationFolders, 0);
      item.setText("Manual");
      item.setControl(manualLocationComp);
      this.createManualLocationControl(manualLocationComp);
      ImageLoader loader = ImageLoader.getDdmUiLibLoader();
      this.mPlayImage = loader.loadImage("play.png", this.mParent.getDisplay());
      this.mPauseImage = loader.loadImage("pause.png", this.mParent.getDisplay());
      Composite gpxLocationComp = new Composite(this.mLocationFolders, 0);
      item = new TabItem(this.mLocationFolders, 0);
      item.setText("GPX");
      item.setControl(gpxLocationComp);
      this.createGpxLocationControl(gpxLocationComp);
      Composite kmlLocationComp = new Composite(this.mLocationFolders, 0);
      kmlLocationComp.setLayout(new FillLayout());
      item = new TabItem(this.mLocationFolders, 0);
      item.setText("KML");
      item.setControl(kmlLocationComp);
      this.createKmlLocationControl(kmlLocationComp);
   }

   private void createManualLocationControl(Composite manualLocationComp) {
      manualLocationComp.setLayout(new GridLayout(1, false));
      this.mDecimalButton = new Button(manualLocationComp, 16);
      this.mDecimalButton.setLayoutData(new GridData(768));
      this.mDecimalButton.setText("Decimal");
      this.mSexagesimalButton = new Button(manualLocationComp, 16);
      this.mSexagesimalButton.setLayoutData(new GridData(768));
      this.mSexagesimalButton.setText("Sexagesimal");
      final Composite content = new Composite(manualLocationComp, 0);
      final StackLayout sl;
      content.setLayout(sl = new StackLayout());
      final Composite decimalContent = new Composite(content, 0);
      GridLayout gl;
      decimalContent.setLayout(gl = new GridLayout(2, false));
      gl.marginHeight = gl.marginWidth = 0;
      this.mLongitudeControls = new CoordinateControls();
      this.mLatitudeControls = new CoordinateControls();
      Label label = new Label(decimalContent, 0);
      label.setText("Longitude");
      this.mLongitudeControls.createDecimalText(decimalContent);
      label = new Label(decimalContent, 0);
      label.setText("Latitude");
      this.mLatitudeControls.createDecimalText(decimalContent);
      final Composite sexagesimalContent = new Composite(content, 0);
      sexagesimalContent.setLayout(gl = new GridLayout(7, false));
      gl.marginHeight = gl.marginWidth = 0;
      label = new Label(sexagesimalContent, 0);
      label.setText("Longitude");
      this.mLongitudeControls.createSexagesimalDegreeText(sexagesimalContent);
      label = new Label(sexagesimalContent, 0);
      label.setText("°");
      this.mLongitudeControls.createSexagesimalMinuteText(sexagesimalContent);
      label = new Label(sexagesimalContent, 0);
      label.setText("\'");
      this.mLongitudeControls.createSexagesimalSecondText(sexagesimalContent);
      label = new Label(sexagesimalContent, 0);
      label.setText("\"");
      label = new Label(sexagesimalContent, 0);
      label.setText("Latitude");
      this.mLatitudeControls.createSexagesimalDegreeText(sexagesimalContent);
      label = new Label(sexagesimalContent, 0);
      label.setText("°");
      this.mLatitudeControls.createSexagesimalMinuteText(sexagesimalContent);
      label = new Label(sexagesimalContent, 0);
      label.setText("\'");
      this.mLatitudeControls.createSexagesimalSecondText(sexagesimalContent);
      label = new Label(sexagesimalContent, 0);
      label.setText("\"");
      sl.topControl = decimalContent;
      this.mDecimalButton.setSelection(true);
      this.mDecimalButton.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            if(EmulatorControlPanel.this.mDecimalButton.getSelection()) {
               sl.topControl = decimalContent;
            } else {
               sl.topControl = sexagesimalContent;
            }

            content.layout();
         }
      });
      Button sendButton = new Button(manualLocationComp, 8);
      sendButton.setText("Send");
      sendButton.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            if(EmulatorControlPanel.this.mEmulatorConsole != null) {
               EmulatorControlPanel.this.processCommandResult(EmulatorControlPanel.this.mEmulatorConsole.sendLocation(EmulatorControlPanel.this.mLongitudeControls.getValue(), EmulatorControlPanel.this.mLatitudeControls.getValue(), 0.0D));
            }

         }
      });
      this.mLongitudeControls.setValue(-122.084095D);
      this.mLatitudeControls.setValue(37.422006D);
   }

   private void createGpxLocationControl(Composite gpxLocationComp) {
      IPreferenceStore store = DdmUiPreferences.getStore();
      gpxLocationComp.setLayout(new GridLayout(1, false));
      this.mGpxUploadButton = new Button(gpxLocationComp, 8);
      this.mGpxUploadButton.setText("Load GPX...");
      this.mGpxWayPointTable = new Table(gpxLocationComp, 66304);
      GridData gd;
      this.mGpxWayPointTable.setLayoutData(gd = new GridData(768));
      gd.heightHint = 100;
      this.mGpxWayPointTable.setHeaderVisible(true);
      this.mGpxWayPointTable.setLinesVisible(true);
      TableHelper.createTableColumn(this.mGpxWayPointTable, "Name", 16384, "Some Name", "emulatorControl.waypoint.name", store);
      TableHelper.createTableColumn(this.mGpxWayPointTable, "Longitude", 16384, "-199.999999", "emulatorControl.waypoint.longitude", store);
      TableHelper.createTableColumn(this.mGpxWayPointTable, "Latitude", 16384, "-199.999999", "emulatorControl.waypoint.latitude", store);
      TableHelper.createTableColumn(this.mGpxWayPointTable, "Elevation", 16384, "99999.9", "emulatorControl.waypoint.elevation", store);
      TableHelper.createTableColumn(this.mGpxWayPointTable, "Description", 16384, "Some Description", "emulatorControl.waypoint.desc", store);
      final TableViewer gpxWayPointViewer = new TableViewer(this.mGpxWayPointTable);
      gpxWayPointViewer.setContentProvider(new WayPointContentProvider());
      gpxWayPointViewer.setLabelProvider(new WayPointLabelProvider());
      gpxWayPointViewer.addSelectionChangedListener(new ISelectionChangedListener() {
         public void selectionChanged(SelectionChangedEvent event) {
            ISelection selection = event.getSelection();
            if(selection instanceof IStructuredSelection) {
               IStructuredSelection structuredSelection = (IStructuredSelection)selection;
               Object selectedObject = structuredSelection.getFirstElement();
               if(selectedObject instanceof WayPoint) {
                  WayPoint wayPoint = (WayPoint)selectedObject;
                  if(EmulatorControlPanel.this.mEmulatorConsole != null && !EmulatorControlPanel.this.mPlayingTrack) {
                     EmulatorControlPanel.this.processCommandResult(EmulatorControlPanel.this.mEmulatorConsole.sendLocation(wayPoint.getLongitude(), wayPoint.getLatitude(), wayPoint.getElevation()));
                  }
               }
            }

         }
      });
      this.mGpxTrackTable = new Table(gpxLocationComp, 66304);
      this.mGpxTrackTable.setLayoutData(gd = new GridData(768));
      gd.heightHint = 100;
      this.mGpxTrackTable.setHeaderVisible(true);
      this.mGpxTrackTable.setLinesVisible(true);
      TableHelper.createTableColumn(this.mGpxTrackTable, "Name", 16384, "Some very long name", "emulatorControl.track.name", store);
      TableHelper.createTableColumn(this.mGpxTrackTable, "Point Count", 131072, "9999", "emulatorControl.track.count", store);
      TableHelper.createTableColumn(this.mGpxTrackTable, "First Point Time", 16384, "999-99-99T99:99:99Z", "emulatorControl.track.first", store);
      TableHelper.createTableColumn(this.mGpxTrackTable, "Last Point Time", 16384, "999-99-99T99:99:99Z", "emulatorControl.track.last", store);
      TableHelper.createTableColumn(this.mGpxTrackTable, "Comment", 16384, "-199.999999", "emulatorControl.track.comment", store);
      final TableViewer gpxTrackViewer = new TableViewer(this.mGpxTrackTable);
      gpxTrackViewer.setContentProvider(new TrackContentProvider());
      gpxTrackViewer.setLabelProvider(new TrackLabelProvider());
      gpxTrackViewer.addSelectionChangedListener(new ISelectionChangedListener() {
         public void selectionChanged(SelectionChangedEvent event) {
            ISelection selection = event.getSelection();
            if(selection instanceof IStructuredSelection) {
               IStructuredSelection structuredSelection = (IStructuredSelection)selection;
               Object selectedObject = structuredSelection.getFirstElement();
               if(selectedObject instanceof GpxParser.Track) {
                  GpxParser.Track track = (GpxParser.Track)selectedObject;
                  if(EmulatorControlPanel.this.mEmulatorConsole != null && !EmulatorControlPanel.this.mPlayingTrack) {
                     TrackPoint[] points = track.getPoints();
                     EmulatorControlPanel.this.processCommandResult(EmulatorControlPanel.this.mEmulatorConsole.sendLocation(points[0].getLongitude(), points[0].getLatitude(), points[0].getElevation()));
                  }

                  EmulatorControlPanel.this.mPlayGpxButton.setEnabled(true);
                  EmulatorControlPanel.this.mGpxBackwardButton.setEnabled(true);
                  EmulatorControlPanel.this.mGpxForwardButton.setEnabled(true);
                  EmulatorControlPanel.this.mGpxSpeedButton.setEnabled(true);
                  return;
               }
            }

            EmulatorControlPanel.this.mPlayGpxButton.setEnabled(false);
            EmulatorControlPanel.this.mGpxBackwardButton.setEnabled(false);
            EmulatorControlPanel.this.mGpxForwardButton.setEnabled(false);
            EmulatorControlPanel.this.mGpxSpeedButton.setEnabled(false);
         }
      });
      this.mGpxUploadButton.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            FileDialog fileDialog = new FileDialog(EmulatorControlPanel.this.mParent.getShell(), 4096);
            fileDialog.setText("Load GPX File");
            fileDialog.setFilterExtensions(new String[]{"*.gpx"});
            String fileName = fileDialog.open();
            if(fileName != null) {
               GpxParser parser = new GpxParser(fileName);
               if(parser.parse()) {
                  gpxWayPointViewer.setInput(parser.getWayPoints());
                  gpxTrackViewer.setInput(parser.getTracks());
               }
            }

         }
      });
      this.mGpxPlayControls = new Composite(gpxLocationComp, 0);
      GridLayout gl;
      this.mGpxPlayControls.setLayout(gl = new GridLayout(5, false));
      gl.marginHeight = gl.marginWidth = 0;
      this.mGpxPlayControls.setLayoutData(new GridData(768));
      this.mPlayGpxButton = new Button(this.mGpxPlayControls, 8388616);
      this.mPlayGpxButton.setImage(this.mPlayImage);
      this.mPlayGpxButton.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            if(!EmulatorControlPanel.this.mPlayingTrack) {
               ISelection selection = gpxTrackViewer.getSelection();
               if(!selection.isEmpty() && selection instanceof IStructuredSelection) {
                  IStructuredSelection structuredSelection = (IStructuredSelection)selection;
                  Object selectedObject = structuredSelection.getFirstElement();
                  if(selectedObject instanceof GpxParser.Track) {
                     GpxParser.Track track = (GpxParser.Track)selectedObject;
                     EmulatorControlPanel.this.playTrack(track);
                  }
               }
            } else {
               EmulatorControlPanel.this.mPlayingTrack = false;
               if(EmulatorControlPanel.this.mPlayingThread != null) {
                  EmulatorControlPanel.this.mPlayingThread.interrupt();
               }
            }

         }
      });
      Label separator = new Label(this.mGpxPlayControls, 514);
      separator.setLayoutData(gd = new GridData(1040));
      gd.heightHint = 0;
      ImageLoader loader = ImageLoader.getDdmUiLibLoader();
      this.mGpxBackwardButton = new Button(this.mGpxPlayControls, 8388610);
      this.mGpxBackwardButton.setImage(loader.loadImage("backward.png", this.mParent.getDisplay()));
      this.mGpxBackwardButton.setSelection(false);
      this.mGpxBackwardButton.addSelectionListener(this.mDirectionButtonAdapter);
      this.mGpxForwardButton = new Button(this.mGpxPlayControls, 8388610);
      this.mGpxForwardButton.setImage(loader.loadImage("forward.png", this.mParent.getDisplay()));
      this.mGpxForwardButton.setSelection(true);
      this.mGpxForwardButton.addSelectionListener(this.mDirectionButtonAdapter);
      this.mGpxSpeedButton = new Button(this.mGpxPlayControls, 8388616);
      this.mSpeedIndex = 0;
      this.mSpeed = PLAY_SPEEDS[this.mSpeedIndex];
      this.mGpxSpeedButton.setText(String.format("Speed: %1$dX", new Object[]{Integer.valueOf(this.mSpeed)}));
      this.mGpxSpeedButton.addSelectionListener(this.mSpeedButtonAdapter);
      this.mPlayGpxButton.setEnabled(false);
      this.mGpxBackwardButton.setEnabled(false);
      this.mGpxForwardButton.setEnabled(false);
      this.mGpxSpeedButton.setEnabled(false);
   }

   private void createKmlLocationControl(Composite kmlLocationComp) {
      IPreferenceStore store = DdmUiPreferences.getStore();
      kmlLocationComp.setLayout(new GridLayout(1, false));
      this.mKmlUploadButton = new Button(kmlLocationComp, 8);
      this.mKmlUploadButton.setText("Load KML...");
      this.mKmlWayPointTable = new Table(kmlLocationComp, 66304);
      GridData gd;
      this.mKmlWayPointTable.setLayoutData(gd = new GridData(768));
      gd.heightHint = 200;
      this.mKmlWayPointTable.setHeaderVisible(true);
      this.mKmlWayPointTable.setLinesVisible(true);
      TableHelper.createTableColumn(this.mKmlWayPointTable, "Name", 16384, "Some Name", "emulatorControl.waypoint.name", store);
      TableHelper.createTableColumn(this.mKmlWayPointTable, "Longitude", 16384, "-199.999999", "emulatorControl.waypoint.longitude", store);
      TableHelper.createTableColumn(this.mKmlWayPointTable, "Latitude", 16384, "-199.999999", "emulatorControl.waypoint.latitude", store);
      TableHelper.createTableColumn(this.mKmlWayPointTable, "Elevation", 16384, "99999.9", "emulatorControl.waypoint.elevation", store);
      TableHelper.createTableColumn(this.mKmlWayPointTable, "Description", 16384, "Some Description", "emulatorControl.waypoint.desc", store);
      final TableViewer kmlWayPointViewer = new TableViewer(this.mKmlWayPointTable);
      kmlWayPointViewer.setContentProvider(new WayPointContentProvider());
      kmlWayPointViewer.setLabelProvider(new WayPointLabelProvider());
      this.mKmlUploadButton.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            FileDialog fileDialog = new FileDialog(EmulatorControlPanel.this.mParent.getShell(), 4096);
            fileDialog.setText("Load KML File");
            fileDialog.setFilterExtensions(new String[]{"*.kml"});
            String fileName = fileDialog.open();
            if(fileName != null) {
               KmlParser parser = new KmlParser(fileName);
               if(parser.parse()) {
                  kmlWayPointViewer.setInput(parser.getWayPoints());
                  EmulatorControlPanel.this.mPlayKmlButton.setEnabled(true);
                  EmulatorControlPanel.this.mKmlBackwardButton.setEnabled(true);
                  EmulatorControlPanel.this.mKmlForwardButton.setEnabled(true);
                  EmulatorControlPanel.this.mKmlSpeedButton.setEnabled(true);
               }
            }

         }
      });
      kmlWayPointViewer.addSelectionChangedListener(new ISelectionChangedListener() {
         public void selectionChanged(SelectionChangedEvent event) {
            ISelection selection = event.getSelection();
            if(selection instanceof IStructuredSelection) {
               IStructuredSelection structuredSelection = (IStructuredSelection)selection;
               Object selectedObject = structuredSelection.getFirstElement();
               if(selectedObject instanceof WayPoint) {
                  WayPoint wayPoint = (WayPoint)selectedObject;
                  if(EmulatorControlPanel.this.mEmulatorConsole != null && !EmulatorControlPanel.this.mPlayingTrack) {
                     EmulatorControlPanel.this.processCommandResult(EmulatorControlPanel.this.mEmulatorConsole.sendLocation(wayPoint.getLongitude(), wayPoint.getLatitude(), wayPoint.getElevation()));
                  }
               }
            }

         }
      });
      this.mKmlPlayControls = new Composite(kmlLocationComp, 0);
      GridLayout gl;
      this.mKmlPlayControls.setLayout(gl = new GridLayout(5, false));
      gl.marginHeight = gl.marginWidth = 0;
      this.mKmlPlayControls.setLayoutData(new GridData(768));
      this.mPlayKmlButton = new Button(this.mKmlPlayControls, 8388616);
      this.mPlayKmlButton.setImage(this.mPlayImage);
      this.mPlayKmlButton.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            if(!EmulatorControlPanel.this.mPlayingTrack) {
               Object input = kmlWayPointViewer.getInput();
               if(input instanceof WayPoint[]) {
                  EmulatorControlPanel.this.playKml((WayPoint[])((WayPoint[])input));
               }
            } else {
               EmulatorControlPanel.this.mPlayingTrack = false;
               if(EmulatorControlPanel.this.mPlayingThread != null) {
                  EmulatorControlPanel.this.mPlayingThread.interrupt();
               }
            }

         }
      });
      Label separator = new Label(this.mKmlPlayControls, 514);
      separator.setLayoutData(gd = new GridData(1040));
      gd.heightHint = 0;
      ImageLoader loader = ImageLoader.getDdmUiLibLoader();
      this.mKmlBackwardButton = new Button(this.mKmlPlayControls, 8388610);
      this.mKmlBackwardButton.setImage(loader.loadImage("backward.png", this.mParent.getDisplay()));
      this.mKmlBackwardButton.setSelection(false);
      this.mKmlBackwardButton.addSelectionListener(this.mDirectionButtonAdapter);
      this.mKmlForwardButton = new Button(this.mKmlPlayControls, 8388610);
      this.mKmlForwardButton.setImage(loader.loadImage("forward.png", this.mParent.getDisplay()));
      this.mKmlForwardButton.setSelection(true);
      this.mKmlForwardButton.addSelectionListener(this.mDirectionButtonAdapter);
      this.mKmlSpeedButton = new Button(this.mKmlPlayControls, 8388616);
      this.mSpeedIndex = 0;
      this.mSpeed = PLAY_SPEEDS[this.mSpeedIndex];
      this.mKmlSpeedButton.setText(String.format("Speed: %1$dX", new Object[]{Integer.valueOf(this.mSpeed)}));
      this.mKmlSpeedButton.addSelectionListener(this.mSpeedButtonAdapter);
      this.mPlayKmlButton.setEnabled(false);
      this.mKmlBackwardButton.setEnabled(false);
      this.mKmlForwardButton.setEnabled(false);
      this.mKmlSpeedButton.setEnabled(false);
   }

   public void setFocus() {
   }

   protected void postCreation() {
   }

   private synchronized void setDataMode(int selectionIndex) {
      if(this.mEmulatorConsole != null) {
         this.processCommandResult(this.mEmulatorConsole.setGsmDataMode(GsmMode.getEnum(GSM_MODES[selectionIndex][1])));
      }

   }

   private synchronized void setVoiceMode(int selectionIndex) {
      if(this.mEmulatorConsole != null) {
         this.processCommandResult(this.mEmulatorConsole.setGsmVoiceMode(GsmMode.getEnum(GSM_MODES[selectionIndex][1])));
      }

   }

   private synchronized void setNetworkLatency(int selectionIndex) {
      if(this.mEmulatorConsole != null) {
         this.processCommandResult(this.mEmulatorConsole.setNetworkLatency(selectionIndex));
      }

   }

   private synchronized void setNetworkSpeed(int selectionIndex) {
      if(this.mEmulatorConsole != null) {
         this.processCommandResult(this.mEmulatorConsole.setNetworkSpeed(selectionIndex));
      }

   }

   /**
    * 选中了设备之后进行处理
    * @param device
     */
   public void handleNewDevice(IDevice device) {
      if(!this.mParent.isDisposed()) {
         synchronized(this) {
            this.mEmulatorConsole = null;
         }

         boolean var17 = false;

         try {
            var17 = true;
            if(device != null) {
                GsmStatus enable2 = null;
                NetworkStatus netstatus2 = null;
               synchronized(this) {
                  this.mEmulatorConsole = EmulatorConsole.getConsole(device);
                  if(this.mEmulatorConsole != null) {
                     enable2 = this.mEmulatorConsole.getGsmStatus();
                     netstatus2 = this.mEmulatorConsole.getNetworkStatus();
                     if(enable2 == null || netstatus2 == null) {
                        this.mEmulatorConsole = null;
                     }
                  }
               }
               final GsmStatus enable = enable2;
               final NetworkStatus netstatus = netstatus2;
               if(enable != null) {
                  if(netstatus != null) {
                     Display d = this.mParent.getDisplay();
                     if(!d.isDisposed()) {
                        d.asyncExec(new Runnable() {
                           public void run() {
                              if(enable.voice != GsmMode.UNKNOWN) {
                                 EmulatorControlPanel.this.mVoiceMode.select(EmulatorControlPanel.this.getGsmComboIndex(enable.voice));
                              } else {
                                 EmulatorControlPanel.this.mVoiceMode.clearSelection();
                              }

                              if(enable.data != GsmMode.UNKNOWN) {
                                 EmulatorControlPanel.this.mDataMode.select(EmulatorControlPanel.this.getGsmComboIndex(enable.data));
                              } else {
                                 EmulatorControlPanel.this.mDataMode.clearSelection();
                              }

                              if(netstatus.speed != -1) {
                                 EmulatorControlPanel.this.mNetworkSpeed.select(netstatus.speed);
                              } else {
                                 EmulatorControlPanel.this.mNetworkSpeed.clearSelection();
                              }

                              if(netstatus.latency != -1) {
                                 EmulatorControlPanel.this.mNetworkLatency.select(netstatus.latency);
                              } else {
                                 EmulatorControlPanel.this.mNetworkLatency.clearSelection();
                              }

                           }
                        });
                        var17 = false;
                     } else {
                        var17 = false;
                     }
                  } else {
                     var17 = false;
                  }
               } else {
                  var17 = false;
               }
            } else {
               var17 = false;
            }
         } finally {
            if(var17) {
               boolean enable1 = false;
               synchronized(this) {
                  enable1 = this.mEmulatorConsole != null;
               }

               this.enable(enable1);
            }
         }

         boolean enable2 = false;
         synchronized(this) {
            enable2 = this.mEmulatorConsole != null;
         }
         //开启各项的功能
         this.enable(enable2);
      }
   }

   /**
    * 开启各项的功能
    * @param enabled
     */
   private void enable(final boolean enabled) {
      try {
         Display e = this.mParent.getDisplay();
         e.asyncExec(new Runnable() {
            public void run() {
               if(!EmulatorControlPanel.this.mParent.isDisposed()) {
                  EmulatorControlPanel.this.doEnable(enabled);
               }

            }
         });
      } catch (SWTException var3) {
         ;
      }

   }

   private boolean isValidPhoneNumber() {
      String number = this.mPhoneNumber.getText().trim();
      return number.matches("^[+#0-9]+$");
   }

   /**
    * 是否开启功能
    * @param enabled
     */
   protected void doEnable(boolean enabled) {
      this.mVoiceLabel.setEnabled(enabled);
      this.mVoiceMode.setEnabled(enabled);
      this.mDataLabel.setEnabled(enabled);
      this.mDataMode.setEnabled(enabled);
      this.mSpeedLabel.setEnabled(enabled);
      this.mNetworkSpeed.setEnabled(enabled);
      this.mLatencyLabel.setEnabled(enabled);
      this.mNetworkLatency.setEnabled(enabled);
      if(this.mPhoneNumber.isEnabled() != enabled) {
         this.mNumberLabel.setEnabled(enabled);
         this.mPhoneNumber.setEnabled(enabled);
      }

      boolean valid = this.isValidPhoneNumber();
      this.mVoiceButton.setEnabled(enabled && valid);
      this.mSmsButton.setEnabled(enabled && valid);
      boolean smsValid = enabled && valid && this.mSmsButton.getSelection();
      if(this.mSmsMessage.isEnabled() != smsValid) {
         this.mMessageLabel.setEnabled(smsValid);
         this.mSmsMessage.setEnabled(smsValid);
      }

      if(!enabled) {
         this.mSmsMessage.setText("");
      }

      this.mCallButton.setEnabled(enabled && valid);
      this.mCancelButton.setEnabled(enabled && valid && this.mVoiceButton.getSelection());
      if(!enabled) {
         this.mVoiceMode.clearSelection();
         this.mDataMode.clearSelection();
         this.mNetworkSpeed.clearSelection();
         this.mNetworkLatency.clearSelection();
         if(this.mPhoneNumber.getText().length() > 0) {
            this.mPhoneNumber.setText("");
         }
      }

      this.mLocationFolders.setEnabled(enabled);
      this.mDecimalButton.setEnabled(enabled);
      this.mSexagesimalButton.setEnabled(enabled);
      this.mLongitudeControls.setEnabled(enabled);
      this.mLatitudeControls.setEnabled(enabled);
      this.mGpxUploadButton.setEnabled(enabled);
      this.mGpxWayPointTable.setEnabled(enabled);
      this.mGpxTrackTable.setEnabled(enabled);
      this.mKmlUploadButton.setEnabled(enabled);
      this.mKmlWayPointTable.setEnabled(enabled);
   }

   private int getGsmComboIndex(GsmMode mode) {
      for(int i = 0; i < GSM_MODES.length; ++i) {
         String[] modes = GSM_MODES[i];
         if(mode.getTag().equals(modes[1])) {
            return i;
         }
      }

      return -1;
   }

   private boolean processCommandResult(final String result) {
      if(result != EmulatorConsole.RESULT_OK) {
         try {
            this.mParent.getDisplay().asyncExec(new Runnable() {
               public void run() {
                  if(!EmulatorControlPanel.this.mParent.isDisposed()) {
                     MessageDialog.openError(EmulatorControlPanel.this.mParent.getShell(), "Emulator Console", result);
                  }

               }
            });
         } catch (SWTException var3) {
            ;
         }

         return false;
      } else {
         return true;
      }
   }

   private void playTrack(final GpxParser.Track track) {
      if(this.mEmulatorConsole != null) {
         this.mPlayGpxButton.setImage(this.mPauseImage);
         this.mPlayKmlButton.setImage(this.mPauseImage);
         this.mPlayingTrack = true;
         this.mPlayingThread = new Thread() {
            public void run() {
               try {
                  TrackPoint[] e = track.getPoints();
                  int count = e.length;
                  int start = 0;
                  if(EmulatorControlPanel.this.mPlayDirection == -1) {
                     start = count - 1;
                  }

                  for(int p = start; p >= 0 && p < count; p += EmulatorControlPanel.this.mPlayDirection) {
                     if(!EmulatorControlPanel.this.mPlayingTrack) {
                        return;
                     }

                     TrackPoint trackPoint = e[p];
                     EmulatorControlPanel nextIndex = EmulatorControlPanel.this;
                     synchronized(EmulatorControlPanel.this) {
                        if(EmulatorControlPanel.this.mEmulatorConsole == null || !EmulatorControlPanel.this.processCommandResult(EmulatorControlPanel.this.mEmulatorConsole.sendLocation(trackPoint.getLongitude(), trackPoint.getLatitude(), trackPoint.getElevation()))) {
                           return;
                        }
                     }

                     int nextIndex1 = p + EmulatorControlPanel.this.mPlayDirection;
                     if(nextIndex1 >= 0 && nextIndex1 < count) {
                        TrackPoint nextPoint = e[nextIndex1];
                        long delta = nextPoint.getTime() - trackPoint.getTime();
                        if(delta < 0L) {
                           delta = -delta;
                        }

                        long startTime = System.currentTimeMillis();

                        try {
                           sleep(delta / (long)EmulatorControlPanel.this.mSpeed);
                        } catch (InterruptedException var33) {
                           if(!EmulatorControlPanel.this.mPlayingTrack) {
                              return;
                           }

                           while(true) {
                              long waited = System.currentTimeMillis() - startTime;
                              long needToWait = delta / (long)EmulatorControlPanel.this.mSpeed;
                              if(waited >= needToWait) {
                                 break;
                              }

                              try {
                                 sleep(needToWait - waited);
                              } catch (InterruptedException var32) {
                                 if(!EmulatorControlPanel.this.mPlayingTrack) {
                                    return;
                                 }
                              }
                           }
                        }
                     }
                  }

               } finally {
                  EmulatorControlPanel.this.mPlayingTrack = false;

                  try {
                     EmulatorControlPanel.this.mParent.getDisplay().asyncExec(new Runnable() {
                        public void run() {
                           if(!EmulatorControlPanel.this.mPlayGpxButton.isDisposed()) {
                              EmulatorControlPanel.this.mPlayGpxButton.setImage(EmulatorControlPanel.this.mPlayImage);
                              EmulatorControlPanel.this.mPlayKmlButton.setImage(EmulatorControlPanel.this.mPlayImage);
                           }

                        }
                     });
                  } catch (SWTException var31) {
                     ;
                  }

               }
            }
         };
         this.mPlayingThread.start();
      }

   }

   private void playKml(final WayPoint[] trackPoints) {
      if(this.mEmulatorConsole != null) {
         this.mPlayGpxButton.setImage(this.mPauseImage);
         this.mPlayKmlButton.setImage(this.mPauseImage);
         this.mPlayingTrack = true;
         this.mPlayingThread = new Thread() {
            public void run() {
               try {
                  int e = trackPoints.length;
                  int start = 0;
                  if(EmulatorControlPanel.this.mPlayDirection == -1) {
                     start = e - 1;
                  }

                  for(int p = start; p >= 0 && p < e; p += EmulatorControlPanel.this.mPlayDirection) {
                     if(!EmulatorControlPanel.this.mPlayingTrack) {
                        return;
                     }

                     WayPoint trackPoint = trackPoints[p];
                     EmulatorControlPanel nextIndex = EmulatorControlPanel.this;
                     synchronized(EmulatorControlPanel.this) {
                        if(EmulatorControlPanel.this.mEmulatorConsole == null || !EmulatorControlPanel.this.processCommandResult(EmulatorControlPanel.this.mEmulatorConsole.sendLocation(trackPoint.getLongitude(), trackPoint.getLatitude(), trackPoint.getElevation()))) {
                           return;
                        }
                     }

                     int nextIndex1 = p + EmulatorControlPanel.this.mPlayDirection;
                     if(nextIndex1 >= 0 && nextIndex1 < e) {
                        long delta = 1000L;
                        if(delta < 0L) {
                           delta = -delta;
                        }

                        long startTime = System.currentTimeMillis();

                        try {
                           sleep(delta / (long)EmulatorControlPanel.this.mSpeed);
                        } catch (InterruptedException var31) {
                           if(!EmulatorControlPanel.this.mPlayingTrack) {
                              return;
                           }

                           while(true) {
                              long waited = System.currentTimeMillis() - startTime;
                              long needToWait = delta / (long)EmulatorControlPanel.this.mSpeed;
                              if(waited >= needToWait) {
                                 break;
                              }

                              try {
                                 sleep(needToWait - waited);
                              } catch (InterruptedException var30) {
                                 if(!EmulatorControlPanel.this.mPlayingTrack) {
                                    return;
                                 }
                              }
                           }
                        }
                     }
                  }

               } finally {
                  EmulatorControlPanel.this.mPlayingTrack = false;

                  try {
                     EmulatorControlPanel.this.mParent.getDisplay().asyncExec(new Runnable() {
                        public void run() {
                           if(!EmulatorControlPanel.this.mPlayGpxButton.isDisposed()) {
                              EmulatorControlPanel.this.mPlayGpxButton.setImage(EmulatorControlPanel.this.mPlayImage);
                              EmulatorControlPanel.this.mPlayKmlButton.setImage(EmulatorControlPanel.this.mPlayImage);
                           }

                        }
                     });
                  } catch (SWTException var29) {
                     ;
                  }

               }
            }
         };
         this.mPlayingThread.start();
      }

   }

   static {
      GSM_MODES = new String[][]{{"unregistered", GsmMode.UNREGISTERED.getTag()}, {"home", GsmMode.HOME.getTag()}, {"roaming", GsmMode.ROAMING.getTag()}, {"searching", GsmMode.SEARCHING.getTag()}, {"denied", GsmMode.DENIED.getTag()}};
      NETWORK_SPEEDS = new String[]{"Full", "GSM", "HSCSD", "GPRS", "EDGE", "UMTS", "HSDPA"};
      NETWORK_LATENCIES = new String[]{"None", "GPRS", "EDGE", "UMTS"};
      PLAY_SPEEDS = new int[]{1, 2, 5, 10, 20, 50};
   }
}
