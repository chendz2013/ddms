package com.android.ddmuilib.handler;

import com.android.ddmlib.Client;
import com.android.ddmlib.IDevice;
import com.android.ddmlib.Log;
import com.android.ddmlib.SyncException;
import com.android.ddmlib.SyncService;
import com.android.ddmlib.TimeoutException;
import com.android.ddmlib.ClientData.IMethodProfilingHandler;
import com.android.ddmlib.SyncService.ISyncProgressMonitor;
import com.android.ddmuilib.DdmUiPreferences;
import com.android.ddmuilib.SyncProgressHelper;
import com.android.ddmuilib.console.DdmConsole;
import com.android.ddmuilib.handler.BaseFileHandler;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;

import com.android.traceview.DmTraceReader;
import com.android.traceview.MainWindow;
import com.android.traceview.TraceUnits;
import org.eclipse.swt.widgets.Shell;

public class MethodProfilingHandler extends BaseFileHandler implements IMethodProfilingHandler {
   public MethodProfilingHandler(Shell parentShell) {
      super(parentShell);
   }

   protected String getDialogTitle() {
      return "Method Profiling Error";
   }

   public void onStartFailure(Client client, String message) {
      this.displayErrorInUiThread("Unable to create Method Profiling file for application \'%1$s\'\n\n%2$sCheck logcat for more information.", new Object[]{client.getClientData().getClientDescription(), message != null?message + "\n\n":""});
   }

   public void onEndFailure(Client client, String message) {
      this.displayErrorInUiThread("Unable to finish Method Profiling for application \'%1$s\'\n\n%2$sCheck logcat for more information.", new Object[]{client.getClientData().getClientDescription(), message != null?message + "\n\n":""});
   }

   public void onSuccess(final String remoteFilePath, final Client client) {
      this.mParentShell.getDisplay().asyncExec(new Runnable() {
         public void run() {
            if(remoteFilePath == null) {
               MethodProfilingHandler.this.displayErrorFromUiThread("Unable to download trace file: unknown file name.\nThis can happen if you disconnected the device while recording the trace.", new Object[0]);
            } else {
               IDevice device = client.getDevice();

               try {
                  SyncService e = client.getDevice().getSyncService();
                  if(e != null) {
                     MethodProfilingHandler.this.pullAndOpen(e, remoteFilePath);
                  } else {
                     MethodProfilingHandler.this.displayErrorFromUiThread("Unable to download trace file from device \'%1$s\'.", new Object[]{device.getSerialNumber()});
                  }
               } catch (Exception var3) {
                  MethodProfilingHandler.this.displayErrorFromUiThread("Unable to download trace file from device \'%1$s\'.", new Object[]{device.getSerialNumber()});
               }

            }
         }
      });
   }

   public void onSuccess(byte[] data, Client client) {
      try {
         File e = this.saveTempFile(data, ".trace");
         this.open(e.getAbsolutePath());
      } catch (IOException var5) {
         String errorMsg = var5.getMessage();
         this.displayErrorInUiThread("Failed to save trace data into temp file%1$s", new Object[]{errorMsg != null?":\n" + errorMsg:"."});
      }

   }

   private void pullAndOpen(final SyncService sync, final String remoteFilePath) throws InvocationTargetException, InterruptedException, IOException {
      File temp = File.createTempFile("android", ".trace");
      final String tempPath = temp.getAbsolutePath();

      try {
         SyncProgressHelper.run(new SyncProgressHelper.SyncRunnable() {
            public void run(ISyncProgressMonitor monitor) throws SyncException, IOException, TimeoutException {
               sync.pullFile(remoteFilePath, tempPath, monitor);
            }

            public void close() {
               sync.close();
            }
         }, String.format("Pulling %1$s from the device", new Object[]{remoteFilePath}), this.mParentShell);
         this.open(tempPath);
      } catch (SyncException var6) {
         if(!var6.wasCanceled()) {
            this.displayErrorFromUiThread("Unable to download trace file:\n\n%1$s", new Object[]{var6.getMessage()});
         }
      } catch (TimeoutException var7) {
         this.displayErrorFromUiThread("Unable to download trace file:\n\ntimeout", new Object[0]);
      }

   }

   protected void open(final String tempPath) {
      this.mParentShell.getDisplay().asyncExec(new Runnable() {
                                                  public void run() {
                                                     String traceName = tempPath;
                                                     boolean regression = false;
                                                     DmTraceReader reader = null;
                                                     try {
                                                        reader = new DmTraceReader(traceName, regression);
                                                     }catch (IOException ex){
                                                        ex.printStackTrace();
                                                     }
                                                     reader.getTraceUnits().setTimeScale(TraceUnits.TimeScale.MilliSeconds);
                                                     MainWindow win = new MainWindow(traceName, reader);
                                                     win.open();
                                                  }
                                               }
      );
//      String[] command = new String[]{DdmUiPreferences.getTraceview(), tempPath};
//
//      try {
//         final Process e = Runtime.getRuntime().exec(command);
//         (new Thread("Traceview output") {
//            public void run() {
//               InputStreamReader is = new InputStreamReader(e.getErrorStream());
//               BufferedReader resultReader = new BufferedReader(is);
//
//               try {
//                  while(true) {
//                     String ex = resultReader.readLine();
//                     if(ex == null) {
//                        e.waitFor();
//                        break;
//                     }
//
//                     DdmConsole.printErrorToConsole("Traceview: " + ex);
//                  }
//               } catch (Exception var4) {
//                  Log.e("traceview", var4);
//               }
//
//            }
//         }).start();
//      } catch (IOException var4) {
//         Log.e("traceview", var4);
//      }

   }
}
