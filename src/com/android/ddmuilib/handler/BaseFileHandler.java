package com.android.ddmuilib.handler;

import com.android.ddmlib.SyncException;
import com.android.ddmlib.SyncService;
import com.android.ddmlib.TimeoutException;
import com.android.ddmlib.SyncService.ISyncProgressMonitor;
import com.android.ddmuilib.SyncProgressHelper;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;

public abstract class BaseFileHandler {
   protected final Shell mParentShell;

   public BaseFileHandler(Shell parentShell) {
      this.mParentShell = parentShell;
   }

   protected abstract String getDialogTitle();

   protected void promptAndPull(final SyncService sync, String localFileName, final String remoteFilePath, String title) throws InvocationTargetException, InterruptedException, SyncException, TimeoutException, IOException {
      FileDialog fileDialog = new FileDialog(this.mParentShell, 8192);
      fileDialog.setText(title);
      fileDialog.setFileName(localFileName);
      final String localFilePath = fileDialog.open();
      if(localFilePath != null) {
         SyncProgressHelper.run(new SyncProgressHelper.SyncRunnable() {
            public void run(ISyncProgressMonitor monitor) throws SyncException, IOException, TimeoutException {
               sync.pullFile(remoteFilePath, localFilePath, monitor);
            }

            public void close() {
               sync.close();
            }
         }, String.format("Pulling %1$s from the device", new Object[]{remoteFilePath}), this.mParentShell);
      }

   }

   protected boolean promptAndSave(String localFileName, byte[] data, String title) {
      FileDialog fileDialog = new FileDialog(this.mParentShell, 8192);
      fileDialog.setText(title);
      fileDialog.setFileName(localFileName);
      String localFilePath = fileDialog.open();
      if(localFilePath != null) {
         try {
            this.saveFile(data, new File(localFilePath));
            return true;
         } catch (IOException var8) {
            String errorMsg = var8.getMessage();
            this.displayErrorInUiThread("Failed to save file \'%1$s\'%2$s", new Object[]{localFilePath, errorMsg != null?":\n" + errorMsg:"."});
         }
      }

      return false;
   }

   protected void displayErrorInUiThread(final String format, final Object... args) {
      this.mParentShell.getDisplay().asyncExec(new Runnable() {
         public void run() {
            MessageDialog.openError(BaseFileHandler.this.mParentShell, BaseFileHandler.this.getDialogTitle(), String.format(format, args));
         }
      });
   }

   protected void displayErrorFromUiThread(String format, Object... args) {
      MessageDialog.openError(this.mParentShell, this.getDialogTitle(), String.format(format, args));
   }

   protected File saveTempFile(byte[] data, String extension) throws IOException {
      File f = File.createTempFile("ddms", extension);
      this.saveFile(data, f);
      return f;
   }

   protected void saveFile(byte[] data, File output) throws IOException {
      FileOutputStream fos = null;

      try {
         fos = new FileOutputStream(output);
         fos.write(data);
      } finally {
         if(fos != null) {
            fos.close();
         }

      }

   }
}
