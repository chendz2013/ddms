package com.android.ddmuilib;

import com.android.ddmlib.Log;
import com.android.ddmlib.NativeLibraryMapInfo;
import com.android.ddmlib.NativeStackCallInfo;
import com.android.ddmuilib.DdmUiPreferences;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class Addr2Line {
   private static final String ANDROID_SYMBOLS_ENVVAR = "ANDROID_SYMBOLS";
   private static final String LIBRARY_NOT_FOUND_MESSAGE_FORMAT = "Unable to locate library %s on disk. Addresses mapping to this library will not be resolved. In order to fix this, set the the library search path in the UI, or set the environment variable ANDROID_SYMBOLS.";
   private static final HashMap<String, Addr2Line> sProcessCache = new HashMap();
   private static final byte[] sCrLf = new byte[]{(byte)10};
   private NativeLibraryMapInfo mLibrary;
   private String mAddr2LineCmd;
   private Process mProcess;
   private BufferedReader mResultReader;
   private BufferedOutputStream mAddressWriter;
   private static final String DEFAULT_LIBRARY_SYMBOLS_FOLDER;
   private static List<String> mLibrarySearchPaths;

   public static void setSearchPath(String path) {
      mLibrarySearchPaths.clear();
      mLibrarySearchPaths.addAll(Arrays.asList(path.split(":")));
   }

   public static Addr2Line getProcess(NativeLibraryMapInfo library, String abi) {
      String libName = library.getLibraryName();
      if(libName != null) {
         HashMap var3 = sProcessCache;
         synchronized(sProcessCache) {
            Addr2Line process = (Addr2Line)sProcessCache.get(libName);
            if(process == null) {
               process = new Addr2Line(library, abi);
               boolean status = process.start();
               if(status) {
                  sProcessCache.put(libName, process);
               } else {
                  process = null;
               }
            }

            return process;
         }
      } else {
         return null;
      }
   }

   private Addr2Line(NativeLibraryMapInfo library, String abi) {
      this.mLibrary = library;
      if(abi != null && !abi.startsWith("32")) {
         Log.d("ddm-Addr2Line", "Using 64 bit addr2line command");
         this.mAddr2LineCmd = System.getenv("ANDROID_ADDR2LINE64");
         if(this.mAddr2LineCmd == null) {
            this.mAddr2LineCmd = DdmUiPreferences.getAddr2Line64();
         }
      } else {
         Log.d("ddm-Addr2Line", "Using 32 bit addr2line command");
         this.mAddr2LineCmd = System.getenv("ANDROID_ADDR2LINE");
         if(this.mAddr2LineCmd == null) {
            this.mAddr2LineCmd = DdmUiPreferences.getAddr2Line();
         }
      }

   }

   private String getLibraryPath(String library) {
      String path = DEFAULT_LIBRARY_SYMBOLS_FOLDER + library;
      if((new File(path)).exists()) {
         return path;
      } else {
         Iterator i$ = mLibrarySearchPaths.iterator();

         String fullPath;
         do {
            if(!i$.hasNext()) {
               return null;
            }

            String p = (String)i$.next();
            fullPath = p + "/" + library;
            if((new File(fullPath)).exists()) {
               return fullPath;
            }

            fullPath = p + "/" + (new File(library)).getName();
         } while(!(new File(fullPath)).exists());

         return fullPath;
      }
   }

   private boolean start() {
      String[] command = new String[]{this.mAddr2LineCmd, "-C", "-f", "-e", null};
      String fullPath = this.getLibraryPath(this.mLibrary.getLibraryName());
      if(fullPath == null) {
         String e1 = String.format("Unable to locate library %s on disk. Addresses mapping to this library will not be resolved. In order to fix this, set the the library search path in the UI, or set the environment variable ANDROID_SYMBOLS.", new Object[]{this.mLibrary.getLibraryName()});
         Log.e("ddm-Addr2Line", e1);
         return false;
      } else {
         command[4] = fullPath;

         try {
            this.mProcess = Runtime.getRuntime().exec(command);
            if(this.mProcess != null) {
               InputStreamReader e = new InputStreamReader(this.mProcess.getInputStream());
               this.mResultReader = new BufferedReader(e);
               this.mAddressWriter = new BufferedOutputStream(this.mProcess.getOutputStream());
               if(this.mResultReader != null && this.mAddressWriter != null) {
                  return true;
               }

               this.mProcess.destroy();
               this.mProcess = null;
               return false;
            }
         } catch (IOException var5) {
            String msg = String.format("Error while trying to start %1$s process for library %2$s", new Object[]{this.mAddr2LineCmd, this.mLibrary});
            Log.e("ddm-Addr2Line", msg);
            if(this.mProcess != null) {
               this.mProcess.destroy();
               this.mProcess = null;
            }
         }

         return false;
      }
   }

   public void stop() {
      HashMap var1 = sProcessCache;
      synchronized(sProcessCache) {
         if(this.mProcess != null) {
            sProcessCache.remove(this.mLibrary);
            this.mProcess.destroy();
            this.mProcess = null;
         }

      }
   }

   public static void stopAll() {
      HashMap var0 = sProcessCache;
      synchronized(sProcessCache) {
         Collection col = sProcessCache.values();
         Iterator i$ = col.iterator();

         while(i$.hasNext()) {
            Addr2Line a2l = (Addr2Line)i$.next();
            a2l.stop();
         }

      }
   }

   public NativeStackCallInfo getAddress(long addr) {
      long offset = addr - this.mLibrary.getStartAddress();
      HashMap var5 = sProcessCache;
      synchronized(sProcessCache) {
         if(this.mProcess != null) {
            String tmp = Long.toString(offset, 16);

            try {
               this.mAddressWriter.write(tmp.getBytes());
               this.mAddressWriter.write(sCrLf);
               this.mAddressWriter.flush();
               String e = this.mResultReader.readLine();
               String source = this.mResultReader.readLine();
               if(e != null && source != null) {
                  NativeStackCallInfo var10000 = new NativeStackCallInfo(addr, this.mLibrary.getLibraryName(), e, source);
                  return var10000;
               }
            } catch (IOException var10) {
               Log.e("ddms", "Error while trying to get information for addr: " + tmp + " in library: " + this.mLibrary);
            }
         }

         return null;
      }
   }

   static {
      String symbols = System.getenv("ANDROID_SYMBOLS");
      if(symbols == null) {
         DEFAULT_LIBRARY_SYMBOLS_FOLDER = DdmUiPreferences.getSymbolDirectory();
      } else {
         DEFAULT_LIBRARY_SYMBOLS_FOLDER = symbols;
      }

      mLibrarySearchPaths = new ArrayList();
   }
}
