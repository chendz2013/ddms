package com.android.ddmuilib.net;

import com.android.ddmlib.AdbCommandRejectedException;
import com.android.ddmlib.Client;
import com.android.ddmlib.IDevice;
import com.android.ddmlib.MultiLineReceiver;
import com.android.ddmlib.ShellCommandUnresponsiveException;
import com.android.ddmlib.TimeoutException;
import com.android.ddmuilib.DdmUiPreferences;
import com.android.ddmuilib.TableHelper;
import com.android.ddmuilib.TablePanel;
import java.awt.BasicStroke;
import java.awt.Color;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.FieldPosition;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.util.ArrayList;
import java.util.Date;
import java.util.Formatter;
import java.util.Iterator;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.DatasetRenderingOrder;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StackedXYAreaRenderer2;
import org.jfree.chart.renderer.xy.XYAreaRenderer;
import org.jfree.data.DefaultKeyedValues2D;
import org.jfree.data.time.Millisecond;
import org.jfree.data.time.TimePeriod;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.AbstractIntervalXYDataset;
import org.jfree.data.xy.TableXYDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.experimental.chart.swt.ChartComposite;
import org.jfree.ui.RectangleAnchor;
import org.jfree.ui.TextAnchor;

/**
 * 通过读取/proc/net/xt_qtaguid/stats的文件来实现的
 */
public class NetworkPanel extends TablePanel {
   private static final long HISTORY_MILLIS = 30000L;
   private static final String PREFS_NETWORK_COL_TITLE = "networkPanel.title";
   private static final String PREFS_NETWORK_COL_RX_BYTES = "networkPanel.rxBytes";
   private static final String PREFS_NETWORK_COL_RX_PACKETS = "networkPanel.rxPackets";
   private static final String PREFS_NETWORK_COL_TX_BYTES = "networkPanel.txBytes";
   private static final String PREFS_NETWORK_COL_TX_PACKETS = "networkPanel.txPackets";
   private static final String PROC_XT_QTAGUID = "/proc/net/xt_qtaguid/stats";
   private static final Color TOTAL_COLOR;
   private static final Color[] SERIES_COLORS;
   private Display mDisplay;
   private Composite mPanel;
   private Composite mHeader;
   private Label mSpeedLabel;
   private Combo mSpeedCombo;
   private long mSpeedMillis;
   private Button mRunningButton;
   private Button mResetButton;
   private JFreeChart mChart;
   private ChartComposite mChartComposite;
   private ValueAxis mDomainAxis;
   private TimeSeriesCollection mTotalCollection;
   private TimeSeries mRxTotalSeries;
   private TimeSeries mTxTotalSeries;
   private NetworkPanel.LiveTimeTableXYDataset mRxDetailDataset;
   private NetworkPanel.LiveTimeTableXYDataset mTxDetailDataset;
   private XYAreaRenderer mTotalRenderer;
   private StackedXYAreaRenderer2 mRenderer;
   private Table mTable;
   private TableViewer mTableViewer;
   private int mActiveUid = -1;
   private ArrayList<NetworkPanel.TrackedItem> mTrackedItems = new ArrayList();
   private NetworkPanel.SampleThread mSampleThread;
   private NetworkPanel.NetworkSnapshot mLastSnapshot;

   protected Control createControl(Composite parent) {
      this.mDisplay = parent.getDisplay();
      this.mPanel = new Composite(parent, 0);
      FormLayout formLayout = new FormLayout();
      this.mPanel.setLayout(formLayout);
      this.createHeader();
      this.createChart();
      this.createTable();
      return this.mPanel;
   }

   private void createHeader() {
      this.mHeader = new Composite(this.mPanel, 0);
      RowLayout layout = new RowLayout();
      layout.center = true;
      this.mHeader.setLayout(layout);
      this.mSpeedLabel = new Label(this.mHeader, 0);
      this.mSpeedLabel.setText("速度:");
      this.mSpeedCombo = new Combo(this.mHeader, 8);
      this.mSpeedCombo.add("快 (100ms)");
      this.mSpeedCombo.add("中 (250ms)");
      this.mSpeedCombo.add("慢 (500ms)");
      this.mSpeedCombo.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            NetworkPanel.this.updateSpeed();
         }
      });
      this.mSpeedCombo.select(1);
      this.updateSpeed();
      this.mRunningButton = new Button(this.mHeader, 8);
      this.mRunningButton.setText("开始");
      this.mRunningButton.setEnabled(false);
      this.mRunningButton.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            boolean alreadyRunning = NetworkPanel.this.mSampleThread != null;
            NetworkPanel.this.updateRunning(!alreadyRunning);
         }
      });
      this.mResetButton = new Button(this.mHeader, 8);
      this.mResetButton.setText("恢复");
      this.mResetButton.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            NetworkPanel.this.clearTrackedItems();
         }
      });
      FormData data = new FormData();
      data.top = new FormAttachment(0);
      data.left = new FormAttachment(0);
      data.right = new FormAttachment(100);
      this.mHeader.setLayoutData(data);
   }

   private void createChart() {
      this.mChart = ChartFactory.createTimeSeriesChart((String)null, (String)null, (String)null, (XYDataset)null, false, false, false);
      this.mRxTotalSeries = new TimeSeries("RX total");
      this.mTxTotalSeries = new TimeSeries("TX total");
      this.mRxTotalSeries.setMaximumItemAge(30000L);
      this.mTxTotalSeries.setMaximumItemAge(30000L);
      this.mTotalCollection = new TimeSeriesCollection();
      this.mTotalCollection.addSeries(this.mRxTotalSeries);
      this.mTotalCollection.addSeries(this.mTxTotalSeries);
      this.mRxDetailDataset = new NetworkPanel.LiveTimeTableXYDataset();
      this.mTxDetailDataset = new NetworkPanel.LiveTimeTableXYDataset();
      this.mTotalRenderer = new XYAreaRenderer(4);
      this.mRenderer = new StackedXYAreaRenderer2();
      XYPlot xyPlot = this.mChart.getXYPlot();
      xyPlot.setDatasetRenderingOrder(DatasetRenderingOrder.FORWARD);
      xyPlot.setDataset(0, this.mTotalCollection);
      xyPlot.setDataset(1, this.mRxDetailDataset);
      xyPlot.setDataset(2, this.mTxDetailDataset);
      xyPlot.setRenderer(0, this.mTotalRenderer);
      xyPlot.setRenderer(1, this.mRenderer);
      xyPlot.setRenderer(2, this.mRenderer);
      this.mDomainAxis = xyPlot.getDomainAxis();
      this.mDomainAxis.setAutoRange(false);
      NumberAxis axis = new NumberAxis();
      axis.setNumberFormatOverride(new NetworkPanel.BytesFormat(true));
      axis.setAutoRangeMinimumSize(50.0D);
      xyPlot.setRangeAxis(axis);
      xyPlot.setRangeAxisLocation(AxisLocation.BOTTOM_OR_RIGHT);
      xyPlot.addRangeMarker(new ValueMarker(0.0D, Color.BLACK, new BasicStroke(2.0F)));
      ValueMarker rxMarker = new ValueMarker(0.0D);
      rxMarker.setStroke(new BasicStroke(0.0F));
      rxMarker.setLabel("RX");
      rxMarker.setLabelFont(rxMarker.getLabelFont().deriveFont(30.0F));
      rxMarker.setLabelPaint(Color.LIGHT_GRAY);
      rxMarker.setLabelAnchor(RectangleAnchor.TOP_RIGHT);
      rxMarker.setLabelTextAnchor(TextAnchor.BOTTOM_RIGHT);
      xyPlot.addRangeMarker(rxMarker);
      ValueMarker txMarker = new ValueMarker(0.0D);
      txMarker.setStroke(new BasicStroke(0.0F));
      txMarker.setLabel("TX");
      txMarker.setLabelFont(txMarker.getLabelFont().deriveFont(30.0F));
      txMarker.setLabelPaint(Color.LIGHT_GRAY);
      txMarker.setLabelAnchor(RectangleAnchor.BOTTOM_RIGHT);
      txMarker.setLabelTextAnchor(TextAnchor.TOP_RIGHT);
      xyPlot.addRangeMarker(txMarker);
      this.mChartComposite = new ChartComposite(this.mPanel, 2048, this.mChart, 680, 420, 300, 200, 4096, 4096, true, true, true, true, false, true);
      FormData data = new FormData();
      data.top = new FormAttachment(this.mHeader);
      data.left = new FormAttachment(0);
      data.bottom = new FormAttachment(70);
      data.right = new FormAttachment(100);
      this.mChartComposite.setLayoutData(data);
   }

   private void createTable() {
      this.mTable = new Table(this.mPanel, 67586);
      FormData data = new FormData();
      data.top = new FormAttachment(this.mChartComposite);
      data.left = new FormAttachment(this.mChartComposite, 0, 16777216);
      data.bottom = new FormAttachment(100);
      this.mTable.setLayoutData(data);
      this.mTable.setHeaderVisible(true);
      this.mTable.setLinesVisible(true);
      IPreferenceStore store = DdmUiPreferences.getStore();
      TableHelper.createTableColumn(this.mTable, "", 16777216, buildSampleText(2), (String)null, (IPreferenceStore)null);
      TableHelper.createTableColumn(this.mTable, "Tag", 16384, buildSampleText(32), "networkPanel.title", store);
      TableHelper.createTableColumn(this.mTable, "RX bytes", 131072, buildSampleText(12), "networkPanel.rxBytes", store);
      TableHelper.createTableColumn(this.mTable, "RX packets", 131072, buildSampleText(12), "networkPanel.rxPackets", store);
      TableHelper.createTableColumn(this.mTable, "TX bytes", 131072, buildSampleText(12), "networkPanel.txBytes", store);
      TableHelper.createTableColumn(this.mTable, "TX packets", 131072, buildSampleText(12), "networkPanel.txPackets", store);
      this.mTableViewer = new TableViewer(this.mTable);
      this.mTableViewer.setContentProvider(new NetworkPanel.ContentProvider(null));
      this.mTableViewer.setLabelProvider(new NetworkPanel.LabelProvider(null));
   }

   private void updateSpeed() {
      switch(this.mSpeedCombo.getSelectionIndex()) {
      case 0:
         this.mSpeedMillis = 100L;
         break;
      case 1:
         this.mSpeedMillis = 250L;
         break;
      case 2:
         this.mSpeedMillis = 500L;
      }

   }

   private void updateRunning(boolean shouldRun) {
      boolean alreadyRunning = this.mSampleThread != null;
      if(alreadyRunning && !shouldRun) {
         this.mSampleThread.finish();
         this.mSampleThread = null;
         this.mRunningButton.setText("开始");
         this.mHeader.pack();
      } else if(!alreadyRunning && shouldRun) {
         this.mSampleThread = new NetworkPanel.SampleThread(null);
         this.mSampleThread.start();
         this.mRunningButton.setText("停止");
         this.mHeader.pack();
      }

   }

   public void setFocus() {
      this.mPanel.setFocus();
   }

   private static Color nextSeriesColor(int index) {
      return SERIES_COLORS[index % SERIES_COLORS.length];
   }

   public NetworkPanel.TrackedItem findOrCreateTrackedItem(int uid, int tag) {
      Iterator item = this.mTrackedItems.iterator();

      NetworkPanel.TrackedItem gc;
      do {
         if(!item.hasNext()) {
            NetworkPanel.TrackedItem item1 = new NetworkPanel.TrackedItem(uid, tag);
            if(item1.isTotal()) {
               item1.color = TOTAL_COLOR;
               item1.label = "Total";
            } else {
               int gc1 = this.mTrackedItems.size();
               item1.color = nextSeriesColor(gc1);
               Formatter formatter = new Formatter();
               item1.label = "0x" + formatter.format("%08x", new Object[]{Integer.valueOf(tag)});
               formatter.close();
            }

            item1.colorImage = new Image(this.mDisplay, 20, 20);
            GC gc2 = new GC(item1.colorImage);
            gc2.setBackground(new org.eclipse.swt.graphics.Color(this.mDisplay, item1.color.getRed(), item1.color.getGreen(), item1.color.getBlue()));
            gc2.fillRectangle(item1.colorImage.getBounds());
            gc2.dispose();
            this.mTrackedItems.add(item1);
            return item1;
         }

         gc = (NetworkPanel.TrackedItem)item.next();
      } while(gc.uid != uid || gc.tag != tag);

      return gc;
   }

   public void clearTrackedItems() {
      this.mRxTotalSeries.clear();
      this.mTxTotalSeries.clear();
      this.mRxDetailDataset.clear();
      this.mTxDetailDataset.clear();
      this.mTrackedItems.clear();
      this.mTableViewer.setInput(this.mTrackedItems);
   }

   private void updateSeriesPaint() {
      Iterator count = this.mTrackedItems.iterator();

      while(count.hasNext()) {
         NetworkPanel.TrackedItem i = (NetworkPanel.TrackedItem)count.next();
         int seriesIndex = this.mRxDetailDataset.getColumnIndex(i.label);
         if(seriesIndex >= 0) {
            this.mRenderer.setSeriesPaint(seriesIndex, i.color);
            this.mRenderer.setSeriesFillPaint(seriesIndex, i.color);
         }
      }

      int var4 = this.mTotalCollection.getSeriesCount();

      for(int var5 = 0; var5 < var4; ++var5) {
         this.mTotalRenderer.setSeriesPaint(var5, TOTAL_COLOR);
         this.mTotalRenderer.setSeriesFillPaint(var5, TOTAL_COLOR);
      }

   }

   public void deviceSelected() {
      this.clientSelected();
   }

   public void clientSelected() {
      this.mActiveUid = -1;
      Client client = this.getCurrentClient();
      if(client != null) {
         int validUid = client.getClientData().getPid();

         try {
            NetworkPanel.UidParser e = new NetworkPanel.UidParser(null);
            this.getCurrentDevice().executeShellCommand("cat /proc/" + validUid + "/status", e);
            this.mActiveUid = e.uid;
         } catch (TimeoutException var4) {
            var4.printStackTrace();
         } catch (AdbCommandRejectedException var5) {
            var5.printStackTrace();
         } catch (ShellCommandUnresponsiveException var6) {
            var6.printStackTrace();
         } catch (IOException var7) {
            var7.printStackTrace();
         }
      }

      this.clearTrackedItems();
      this.updateRunning(false);
      boolean validUid1 = this.mActiveUid != -1;
      this.mRunningButton.setEnabled(validUid1);
   }

   public void clientChanged(Client client, int changeMask) {
   }

   /**
    * 执行采样
    */
   public void performSample() {
      IDevice device = this.getCurrentDevice();
      if(device != null) {
         try {
            NetworkPanel.NetworkSnapshotParser e = new NetworkPanel.NetworkSnapshotParser();
            device.executeShellCommand("cat /proc/net/xt_qtaguid/stats", e);
            if(e.isError()) {
               this.mDisplay.asyncExec(new Runnable() {
                  public void run() {
                     NetworkPanel.this.updateRunning(false);
                     String title = "Problem reading stats";
                     String message = "Problem reading xt_qtaguid network statistics from selected device.";
                     Status status = new Status(4, "NetworkPanel", 0, "Problem reading xt_qtaguid network statistics from selected device.", (Throwable)null);
                     ErrorDialog.openError(NetworkPanel.this.mPanel.getShell(), "Problem reading stats", "Problem reading stats", status);
                  }
               });
               return;
            }

            NetworkPanel.NetworkSnapshot snapshot = e.getParsedSnapshot();
            if(this.mLastSnapshot == null) {
               this.mLastSnapshot = snapshot;
               return;
            }

            NetworkPanel.NetworkSnapshot delta = NetworkPanel.NetworkSnapshot.subtract(snapshot, this.mLastSnapshot);
            this.mLastSnapshot = snapshot;
            if(!this.mDisplay.isDisposed()) {
               this.mDisplay.syncExec(new NetworkPanel.UpdateDeltaRunnable(delta, snapshot.timestamp));
            }
         } catch (TimeoutException var5) {
            var5.printStackTrace();
         } catch (AdbCommandRejectedException var6) {
            var6.printStackTrace();
         } catch (ShellCommandUnresponsiveException var7) {
            var7.printStackTrace();
         } catch (IOException var8) {
            var8.printStackTrace();
         }

      }
   }

   public static boolean equal(Object a, Object b) {
      return a == b || a != null && a.equals(b);
   }

   private static String buildSampleText(int length) {
      StringBuilder builder = new StringBuilder(length);

      for(int i = 0; i < length; ++i) {
         builder.append("X");
      }

      return builder.toString();
   }

   static {
      TOTAL_COLOR = Color.GRAY;
      SERIES_COLORS = new Color[]{Color.decode("0x2bc4c1"), Color.decode("0xD50F25"), Color.decode("0x3369E8"), Color.decode("0xEEB211"), Color.decode("0x00bd2e"), Color.decode("0xae26ae")};
   }

   public static class LiveTimeTableXYDataset extends AbstractIntervalXYDataset implements TableXYDataset {
      private DefaultKeyedValues2D mValues = new DefaultKeyedValues2D(true);

      public void addValue(Number value, TimePeriod rowKey, String columnKey) {
         this.mValues.addValue(value, rowKey, columnKey);
      }

      public void removeBefore(long beforeMillis) {
         while(true) {
            if(this.mValues.getRowCount() > 0) {
               TimePeriod period = (TimePeriod)this.mValues.getRowKey(0);
               if(period.getEnd().getTime() < beforeMillis) {
                  this.mValues.removeRow(0);
                  continue;
               }
            }

            return;
         }
      }

      public int getColumnIndex(String key) {
         return this.mValues.getColumnIndex(key);
      }

      public void clear() {
         this.mValues.clear();
         this.fireDatasetChanged();
      }

      public void fireDatasetChanged() {
         super.fireDatasetChanged();
      }

      public int getItemCount() {
         return this.mValues.getRowCount();
      }

      public int getItemCount(int series) {
         return this.mValues.getRowCount();
      }

      public int getSeriesCount() {
         return this.mValues.getColumnCount();
      }

      public Comparable getSeriesKey(int series) {
         return this.mValues.getColumnKey(series);
      }

      public double getXValue(int series, int item) {
         TimePeriod period = (TimePeriod)this.mValues.getRowKey(item);
         return (double)period.getStart().getTime();
      }

      public double getStartXValue(int series, int item) {
         return this.getXValue(series, item);
      }

      public double getEndXValue(int series, int item) {
         return this.getXValue(series, item);
      }

      public Number getX(int series, int item) {
         return Double.valueOf(this.getXValue(series, item));
      }

      public Number getStartX(int series, int item) {
         return Double.valueOf(this.getXValue(series, item));
      }

      public Number getEndX(int series, int item) {
         return Double.valueOf(this.getXValue(series, item));
      }

      public Number getY(int series, int item) {
         return this.mValues.getValue(item, series);
      }

      public Number getStartY(int series, int item) {
         return this.getY(series, item);
      }

      public Number getEndY(int series, int item) {
         return this.getY(series, item);
      }
   }

   private static class BytesFormat extends NumberFormat {
      private final String[] mUnits;
      private final DecimalFormat mFormat = new DecimalFormat("#.#");

      public BytesFormat(boolean perSecond) {
         if(perSecond) {
            this.mUnits = new String[]{"B/s", "KB/s", "MB/s"};
         } else {
            this.mUnits = new String[]{"B", "KB", "MB"};
         }

      }

      public StringBuffer format(long number, StringBuffer toAppendTo, FieldPosition pos) {
         double value = (double)Math.abs(number);

         int i;
         for(i = 0; value > 1024.0D && i < this.mUnits.length - 1; ++i) {
            value /= 1024.0D;
         }

         toAppendTo.append(this.mFormat.format(value));
         toAppendTo.append(this.mUnits[i]);
         return toAppendTo;
      }

      public StringBuffer format(double number, StringBuffer toAppendTo, FieldPosition pos) {
         return this.format((long)number, toAppendTo, pos);
      }

      public Number parse(String source, ParsePosition parsePosition) {
         return null;
      }
   }

   private static class LabelProvider implements ITableLabelProvider {
      private final DecimalFormat mFormat;

      private LabelProvider() {
         this.mFormat = new DecimalFormat("#,###");
      }

      public Image getColumnImage(Object element, int columnIndex) {
         if(element instanceof NetworkPanel.TrackedItem) {
            NetworkPanel.TrackedItem item = (NetworkPanel.TrackedItem)element;
            switch(columnIndex) {
            case 0:
               return item.colorImage;
            }
         }

         return null;
      }

      public String getColumnText(Object element, int columnIndex) {
         if(element instanceof NetworkPanel.TrackedItem) {
            NetworkPanel.TrackedItem item = (NetworkPanel.TrackedItem)element;
            switch(columnIndex) {
            case 0:
               return null;
            case 1:
               return item.label;
            case 2:
               return this.mFormat.format(item.rxBytes);
            case 3:
               return this.mFormat.format(item.rxPackets);
            case 4:
               return this.mFormat.format(item.txBytes);
            case 5:
               return this.mFormat.format(item.txPackets);
            }
         }

         return null;
      }

      public void addListener(ILabelProviderListener listener) {
      }

      public void dispose() {
      }

      public boolean isLabelProperty(Object element, String property) {
         return false;
      }

      public void removeListener(ILabelProviderListener listener) {
      }

      // $FF: synthetic method
      LabelProvider(Object x0) {
         this();
      }
   }

   private class ContentProvider implements IStructuredContentProvider {
      private ContentProvider() {
      }

      public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
      }

      public void dispose() {
      }

      public Object[] getElements(Object inputElement) {
         return NetworkPanel.this.mTrackedItems.toArray();
      }

      // $FF: synthetic method
      ContentProvider(Object x1) {
         this();
      }
   }

   private static class NetworkSnapshot implements Iterable<NetworkPanel.NetworkSnapshot.Entry> {
      private ArrayList<NetworkPanel.NetworkSnapshot.Entry> mStats = new ArrayList();
      public final long timestamp;

      public NetworkSnapshot(long timestamp) {
         this.timestamp = timestamp;
      }

      public void clear() {
         this.mStats.clear();
      }

      public void combine(NetworkPanel.NetworkSnapshot.Entry entry) {
         NetworkPanel.NetworkSnapshot.Entry existing = this.findEntry(entry.iface, entry.uid, entry.set, entry.tag);
         if(existing != null) {
            existing.rxBytes += entry.rxBytes;
            existing.rxPackets += entry.rxPackets;
            existing.txBytes += entry.txBytes;
            existing.txPackets += entry.txPackets;
         } else {
            this.mStats.add(entry);
         }

      }

      public Iterator<NetworkPanel.NetworkSnapshot.Entry> iterator() {
         return this.mStats.iterator();
      }

      public NetworkPanel.NetworkSnapshot.Entry findEntry(String iface, int uid, int set, int tag) {
         Iterator i$ = this.mStats.iterator();

         NetworkPanel.NetworkSnapshot.Entry entry;
         do {
            if(!i$.hasNext()) {
               return null;
            }

            entry = (NetworkPanel.NetworkSnapshot.Entry)i$.next();
         } while(entry.uid != uid || entry.set != set || entry.tag != tag || !NetworkPanel.equal(entry.iface, iface));

         return entry;
      }

      public static NetworkPanel.NetworkSnapshot subtract(NetworkPanel.NetworkSnapshot left, NetworkPanel.NetworkSnapshot right) {
         NetworkPanel.NetworkSnapshot result = new NetworkPanel.NetworkSnapshot(left.timestamp - right.timestamp);
         Iterator i$ = left.iterator();

         while(i$.hasNext()) {
            NetworkPanel.NetworkSnapshot.Entry leftEntry = (NetworkPanel.NetworkSnapshot.Entry)i$.next();
            NetworkPanel.NetworkSnapshot.Entry rightEntry = right.findEntry(leftEntry.iface, leftEntry.uid, leftEntry.set, leftEntry.tag);
            if(rightEntry != null) {
               NetworkPanel.NetworkSnapshot.Entry resultEntry = new NetworkPanel.NetworkSnapshot.Entry();
               resultEntry.iface = leftEntry.iface;
               resultEntry.uid = leftEntry.uid;
               resultEntry.set = leftEntry.set;
               resultEntry.tag = leftEntry.tag;
               resultEntry.rxBytes = leftEntry.rxBytes - rightEntry.rxBytes;
               resultEntry.rxPackets = leftEntry.rxPackets - rightEntry.rxPackets;
               resultEntry.txBytes = leftEntry.txBytes - rightEntry.txBytes;
               resultEntry.txPackets = leftEntry.txPackets - rightEntry.txPackets;
               result.combine(resultEntry);
            }
         }

         return result;
      }

      public static class Entry {
         public String iface;
         public int uid;
         public int set;
         public int tag;
         public long rxBytes;
         public long rxPackets;
         public long txBytes;
         public long txPackets;

         public boolean isEmpty() {
            return this.rxBytes == 0L && this.rxPackets == 0L && this.txBytes == 0L && this.txPackets == 0L;
         }
      }
   }

   private static class NetworkSnapshotParser extends MultiLineReceiver {
      private NetworkPanel.NetworkSnapshot mSnapshot = new NetworkPanel.NetworkSnapshot(System.currentTimeMillis());

      public boolean isError() {
         return this.mSnapshot == null;
      }

      public NetworkPanel.NetworkSnapshot getParsedSnapshot() {
         return this.mSnapshot;
      }

      public boolean isCancelled() {
         return false;
      }

      public void processNewLines(String[] lines) {
         String[] arr$ = lines;
         int len$ = lines.length;

         for(int i$ = 0; i$ < len$; ++i$) {
            String line = arr$[i$];
            if(line.endsWith("No such file or directory")) {
               this.mSnapshot = null;
               return;
            }

            if(!line.startsWith("idx")) {
               String[] cols = line.split(" ");
               if(cols.length >= 9) {
                  NetworkPanel.NetworkSnapshot.Entry entry = new NetworkPanel.NetworkSnapshot.Entry();
                  entry.iface = null;
                  entry.uid = Integer.parseInt(cols[3]);
                  entry.set = -1;
                  entry.tag = kernelToTag(cols[2]);
                  entry.rxBytes = Long.parseLong(cols[5]);
                  entry.rxPackets = Long.parseLong(cols[6]);
                  entry.txBytes = Long.parseLong(cols[7]);
                  entry.txPackets = Long.parseLong(cols[8]);
                  this.mSnapshot.combine(entry);
               }
            }
         }

      }

      public static int kernelToTag(String string) {
         int length = string.length();
         return length > 10?Long.decode(string.substring(0, length - 8)).intValue():0;
      }
   }

   private static class UidParser extends MultiLineReceiver {
      public int uid;

      private UidParser() {
         this.uid = -1;
      }

      public boolean isCancelled() {
         return false;
      }

      public void processNewLines(String[] lines) {
         String[] arr$ = lines;
         int len$ = lines.length;

         for(int i$ = 0; i$ < len$; ++i$) {
            String line = arr$[i$];
            if(line.startsWith("Uid:")) {
               String[] cols = line.split("\t");
               this.uid = Integer.parseInt(cols[1]);
            }
         }

      }

      // $FF: synthetic method
      UidParser(Object x0) {
         this();
      }
   }

   private class UpdateDeltaRunnable implements Runnable {
      private final NetworkPanel.NetworkSnapshot mDelta;
      private final long mEndTime;

      public UpdateDeltaRunnable(NetworkPanel.NetworkSnapshot delta, long endTime) {
         this.mDelta = delta;
         this.mEndTime = endTime;
      }

      public void run() {
         if(!NetworkPanel.this.mDisplay.isDisposed()) {
            Millisecond time = new Millisecond(new Date(this.mEndTime));
            Iterator beforeMillis = this.mDelta.iterator();

            while(beforeMillis.hasNext()) {
               NetworkPanel.NetworkSnapshot.Entry entry = (NetworkPanel.NetworkSnapshot.Entry)beforeMillis.next();
               if(NetworkPanel.this.mActiveUid == entry.uid) {
                  NetworkPanel.TrackedItem item = NetworkPanel.this.findOrCreateTrackedItem(entry.uid, entry.tag);
                  item.recordDelta(time, this.mDelta.timestamp, entry);
               }
            }

            long beforeMillis1 = this.mEndTime - 30000L;
            NetworkPanel.this.mRxDetailDataset.removeBefore(beforeMillis1);
            NetworkPanel.this.mTxDetailDataset.removeBefore(beforeMillis1);
            NetworkPanel.this.mRxDetailDataset.fireDatasetChanged();
            NetworkPanel.this.mTxDetailDataset.fireDatasetChanged();
            NetworkPanel.this.mDomainAxis.setRange((double)(this.mEndTime - 30000L), (double)this.mEndTime);
            NetworkPanel.this.updateSeriesPaint();
            NetworkPanel.this.mTableViewer.setInput(NetworkPanel.this.mTrackedItems);
         }
      }
   }

   private class TrackedItem {
      public final int uid;
      public final int tag;
      public Color color;
      public Image colorImage;
      public String label;
      public long rxBytes;
      public long rxPackets;
      public long txBytes;
      public long txPackets;

      public TrackedItem(int uid, int tag) {
         this.uid = uid;
         this.tag = tag;
      }

      public boolean isTotal() {
         return this.tag == 0;
      }

      public void recordDelta(Millisecond time, long deltaMillis, NetworkPanel.NetworkSnapshot.Entry delta) {
         long rxBytesPerSecond = delta.rxBytes * 1000L / deltaMillis;
         long txBytesPerSecond = delta.txBytes * 1000L / deltaMillis;
         if(this.isTotal()) {
            NetworkPanel.this.mRxTotalSeries.addOrUpdate(time, (double)rxBytesPerSecond);
            NetworkPanel.this.mTxTotalSeries.addOrUpdate(time, (double)(-txBytesPerSecond));
         } else {
            NetworkPanel.this.mRxDetailDataset.addValue(Long.valueOf(rxBytesPerSecond), time, this.label);
            NetworkPanel.this.mTxDetailDataset.addValue(Long.valueOf(-txBytesPerSecond), time, this.label);
         }

         this.rxBytes += delta.rxBytes;
         this.rxPackets += delta.rxPackets;
         this.txBytes += delta.txBytes;
         this.txPackets += delta.txPackets;
      }
   }

   private class SampleThread extends Thread {
      private volatile boolean mFinish;

      private SampleThread() {
      }

      public void finish() {
         this.mFinish = true;
         this.interrupt();
      }

      public void run() {
         while(!this.mFinish && !NetworkPanel.this.mDisplay.isDisposed()) {
            NetworkPanel.this.performSample();

            try {
               Thread.sleep(NetworkPanel.this.mSpeedMillis);
            } catch (InterruptedException var2) {
               ;
            }
         }

      }

      // $FF: synthetic method
      SampleThread(Object x1) {
         this();
      }
   }
}
