package com.android.ddmuilib;

import com.android.ddmlib.AndroidDebugBridge;
import com.android.ddmlib.Client;
import com.android.ddmlib.ClientData;
import com.android.ddmlib.DdmPreferences;
import com.android.ddmlib.IDevice;
import com.android.ddmlib.AndroidDebugBridge.IClientChangeListener;
import com.android.ddmlib.AndroidDebugBridge.IDebugBridgeChangeListener;
import com.android.ddmlib.AndroidDebugBridge.IDeviceChangeListener;
import com.android.ddmlib.ClientData.DebuggerStatus;
import com.android.ddmlib.ClientData.MethodProfilingStatus;
import com.android.ddmlib.IDevice.DeviceState;
import com.android.ddmuilib.DdmUiPreferences;
import com.android.ddmuilib.ImageLoader;
import com.android.ddmuilib.Panel;
import com.android.ddmuilib.TableHelper;
import com.android.ddmuilib.vmtrace.VmTraceOptionsDialog;
import com.google.common.base.Throwables;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.swt.widgets.TreeItem;

public final class DevicePanel extends Panel implements IDebugBridgeChangeListener, IDeviceChangeListener, IClientChangeListener {
   private static final String PREFS_COL_NAME_SERIAL = "devicePanel.Col0";
   private static final String PREFS_COL_PID_STATE = "devicePanel.Col1";
   private static final String PREFS_COL_PORT_BUILD = "devicePanel.Col4";
   private static final int DEVICE_COL_SERIAL = 0;
   private static final int DEVICE_COL_STATE = 1;
   private static final int DEVICE_COL_BUILD = 4;
   private static final int CLIENT_COL_NAME = 0;
   private static final int CLIENT_COL_PID = 1;
   private static final int CLIENT_COL_THREAD = 2;
   private static final int CLIENT_COL_HEAP = 3;
   private static final int CLIENT_COL_PORT = 4;
   public static final int ICON_WIDTH = 16;
   public static final String ICON_THREAD = "thread.png";
   public static final String ICON_HEAP = "heap.png";
   public static final String ICON_HALT = "halt.png";
   public static final String ICON_GC = "gc.png";
   public static final String ICON_HPROF = "hprof.png";
   public static final String ICON_TRACING_START = "tracing_start.png";
   public static final String ICON_TRACING_STOP = "tracing_stop.png";
   private IDevice mCurrentDevice;
   private Client mCurrentClient;
   private Tree mTree;
   private TreeViewer mTreeViewer;
   private Image mDeviceImage;
   private Image mEmulatorImage;
   private Image mThreadImage;
   private Image mHeapImage;
   private Image mWaitingImage;
   private Image mDebuggerImage;
   private Image mDebugErrorImage;
   private final ArrayList<DevicePanel.IUiSelectionListener> mListeners = new ArrayList();
   private final ArrayList<IDevice> mDevicesToExpand = new ArrayList();
   private boolean mAdvancedPortSupport;

   public DevicePanel(boolean advancedPortSupport) {
      this.mAdvancedPortSupport = advancedPortSupport;
   }

   public void addSelectionListener(DevicePanel.IUiSelectionListener listener) {
      this.mListeners.add(listener);
   }

   public void removeSelectionListener(DevicePanel.IUiSelectionListener listener) {
      this.mListeners.remove(listener);
   }

   protected Control createControl(Composite parent) {
      this.loadImages(parent.getDisplay());
      parent.setLayout(new FillLayout());
      this.mTree = new Tree(parent, 65540);
      this.mTree.setHeaderVisible(true);
      this.mTree.setLinesVisible(true);
      IPreferenceStore store = DdmUiPreferences.getStore();
      TableHelper.createTreeColumn(this.mTree, "进程名称", 16384, "com.android.home", "devicePanel.Col0", store);
      TableHelper.createTreeColumn(this.mTree, "进程ID", 16384, "Offline", "devicePanel.Col1", store);
      TreeColumn col = new TreeColumn(this.mTree, 0);
      col.setWidth(24);
      col.setResizable(false);
      col = new TreeColumn(this.mTree, 0);
      col.setWidth(24);
      col.setResizable(false);
      TableHelper.createTreeColumn(this.mTree, "端口", 16384, "9999-9999", "devicePanel.Col4", store);
      this.mTreeViewer = new TreeViewer(this.mTree);
      this.mTreeViewer.setAutoExpandLevel(-1);
      this.mTreeViewer.setContentProvider(new DevicePanel.ContentProvider(null));
      this.mTreeViewer.setLabelProvider(new DevicePanel.LabelProvider(null));
      this.mTree.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            DevicePanel.this.notifyListeners();
         }
      });
      return this.mTree;
   }

   public void setFocus() {
      this.mTree.setFocus();
   }

   protected void postCreation() {
      AndroidDebugBridge.addDebugBridgeChangeListener(this);
      AndroidDebugBridge.addDeviceChangeListener(this);
      AndroidDebugBridge.addClientChangeListener(this);
   }

   public void dispose() {
      AndroidDebugBridge.removeDebugBridgeChangeListener(this);
      AndroidDebugBridge.removeDeviceChangeListener(this);
      AndroidDebugBridge.removeClientChangeListener(this);
   }

   public Client getSelectedClient() {
      return this.mCurrentClient;
   }

   public IDevice getSelectedDevice() {
      return this.mCurrentDevice;
   }

   public void killSelectedClient() {
      if(this.mCurrentClient != null) {
         Client client = this.mCurrentClient;
         TreePath treePath = new TreePath(new Object[]{this.mCurrentDevice});
         TreeSelection treeSelection = new TreeSelection(treePath);
         this.mTreeViewer.setSelection(treeSelection);
         client.kill();
      }

   }

   public void forceGcOnSelectedClient() {
      if(this.mCurrentClient != null) {
         this.mCurrentClient.executeGarbageCollector();
      }

   }

   public void dumpHprof() {
      if(this.mCurrentClient != null) {
         this.mCurrentClient.dumpHprof();
      }

   }

   public void toggleMethodProfiling() {
      if(this.mCurrentClient != null) {
         try {
            this.toggleMethodProfiling(this.mCurrentClient);
         } catch (IOException var2) {
            MessageDialog.openError(this.mTree.getShell(), "Method Profiling", "Unexpected I/O error while starting/stopping profiling: " + Throwables.getRootCause(var2).getMessage());
         }

      }
   }

   /**
    * 切换profile
    * @param client
    * @throws IOException
     */
   private void toggleMethodProfiling(Client client) throws IOException {
      ClientData cd = this.mCurrentClient.getClientData();
      if(cd.getMethodProfilingStatus() == MethodProfilingStatus.TRACER_ON) {
         this.mCurrentClient.stopMethodTracer();
      } else if(cd.getMethodProfilingStatus() == MethodProfilingStatus.SAMPLER_ON) {
         this.mCurrentClient.stopSamplingProfiler();
      } else {
         boolean supportsSampling = cd.hasFeature("method-sample-profiling");
         boolean shouldUseTracing = true;
         int samplingIntervalMicros = 1;
         //支持采样的方式
         if(supportsSampling) {
            VmTraceOptionsDialog dialog = new VmTraceOptionsDialog(this.mTree.getShell());
            if(dialog.open() == 1) {
               return;
            }

            shouldUseTracing = dialog.shouldUseTracing();
            if(!shouldUseTracing) {
               samplingIntervalMicros = dialog.getSamplingIntervalMicros();
            }
         }

         if(shouldUseTracing) {
            this.mCurrentClient.startMethodTracer();
         } else {
            this.mCurrentClient.startSamplingProfiler(samplingIntervalMicros, TimeUnit.MICROSECONDS);
         }
      }

   }

   public void setEnabledHeapOnSelectedClient(boolean enable) {
      if(this.mCurrentClient != null) {
         this.mCurrentClient.setHeapUpdateEnabled(enable);
      }

   }

   public void setEnabledThreadOnSelectedClient(boolean enable) {
      if(this.mCurrentClient != null) {
         this.mCurrentClient.setThreadUpdateEnabled(enable);
      }

   }

   public void bridgeChanged(final AndroidDebugBridge bridge) {
      if(!this.mTree.isDisposed()) {
         this.exec(new Runnable() {
            public void run() {
               if(!DevicePanel.this.mTree.isDisposed()) {
                  DevicePanel.this.mTreeViewer.setInput(bridge);
                  DevicePanel.this.notifyListeners();
               } else {
                  AndroidDebugBridge.removeDebugBridgeChangeListener(DevicePanel.this);
                  AndroidDebugBridge.removeDeviceChangeListener(DevicePanel.this);
                  AndroidDebugBridge.removeClientChangeListener(DevicePanel.this);
               }

            }
         });
      }

      ArrayList var2 = this.mDevicesToExpand;
      synchronized(this.mDevicesToExpand) {
         this.mDevicesToExpand.clear();
      }
   }

   public void deviceConnected(IDevice device) {
      this.exec(new Runnable() {
         public void run() {
            if(!DevicePanel.this.mTree.isDisposed()) {
               DevicePanel.this.mTreeViewer.refresh();
               DevicePanel.this.notifyListeners();
            } else {
               AndroidDebugBridge.removeDebugBridgeChangeListener(DevicePanel.this);
               AndroidDebugBridge.removeDeviceChangeListener(DevicePanel.this);
               AndroidDebugBridge.removeClientChangeListener(DevicePanel.this);
            }

         }
      });
      if(!device.hasClients()) {
         ArrayList var2 = this.mDevicesToExpand;
         synchronized(this.mDevicesToExpand) {
            this.mDevicesToExpand.add(device);
         }
      }

   }

   public void deviceDisconnected(IDevice device) {
      this.deviceConnected(device);
      ArrayList var2 = this.mDevicesToExpand;
      synchronized(this.mDevicesToExpand) {
         this.mDevicesToExpand.remove(device);
      }
   }

   public void deviceChanged(final IDevice device, int changeMask) {
      boolean expand = false;
      ArrayList finalExpand = this.mDevicesToExpand;
      synchronized(this.mDevicesToExpand) {
         int index = this.mDevicesToExpand.indexOf(device);
         if(device.hasClients() && index != -1) {
            this.mDevicesToExpand.remove(index);
            expand = true;
         }
      }
      final boolean blnExpand = expand;
      this.exec(new Runnable() {
         public void run() {
            if(!DevicePanel.this.mTree.isDisposed()) {
               IDevice selectedDevice = DevicePanel.this.getSelectedDevice();
               DevicePanel.this.mTreeViewer.refresh(device);
               if(selectedDevice == device && DevicePanel.this.mTreeViewer.getSelection().isEmpty()) {
                  DevicePanel.this.mTreeViewer.setSelection(new TreeSelection(new TreePath(new Object[]{device})));
               }

               DevicePanel.this.notifyListeners();
               if(blnExpand) {
                  DevicePanel.this.mTreeViewer.setExpandedState(device, true);
               }
            } else {
               AndroidDebugBridge.removeDebugBridgeChangeListener(DevicePanel.this);
               AndroidDebugBridge.removeDeviceChangeListener(DevicePanel.this);
               AndroidDebugBridge.removeClientChangeListener(DevicePanel.this);
            }

         }
      });
   }

   public void clientChanged(final Client client, final int changeMask) {
      this.exec(new Runnable() {
         public void run() {
            if(!DevicePanel.this.mTree.isDisposed()) {
               DevicePanel.this.mTreeViewer.refresh(client);
               if((changeMask & 2) == 2 && client.getClientData().getDebuggerConnectionStatus() == DebuggerStatus.WAITING) {
                  IDevice device = client.getDevice();
                  if(!DevicePanel.this.mTreeViewer.getExpandedState(device)) {
                     DevicePanel.this.mTreeViewer.setExpandedState(device, true);
                  }

                  TreePath treePath = new TreePath(new Object[]{device, client});
                  TreeSelection treeSelection = new TreeSelection(treePath);
                  DevicePanel.this.mTreeViewer.setSelection(treeSelection);
                  if(DevicePanel.this.mAdvancedPortSupport) {
                     client.setAsSelectedClient();
                  }

                  DevicePanel.this.notifyListeners(device, client);
               }
            } else {
               AndroidDebugBridge.removeDebugBridgeChangeListener(DevicePanel.this);
               AndroidDebugBridge.removeDeviceChangeListener(DevicePanel.this);
               AndroidDebugBridge.removeClientChangeListener(DevicePanel.this);
            }

         }
      });
   }

   private void loadImages(Display display) {
      ImageLoader loader = ImageLoader.getDdmUiLibLoader();
      if(this.mDeviceImage == null) {
         this.mDeviceImage = loader.loadImage(display, "device.png", 16, 16, display.getSystemColor(3));
      }

      if(this.mEmulatorImage == null) {
         this.mEmulatorImage = loader.loadImage(display, "emulator.png", 16, 16, display.getSystemColor(9));
      }

      if(this.mThreadImage == null) {
         this.mThreadImage = loader.loadImage(display, "thread.png", 16, 16, display.getSystemColor(7));
      }

      if(this.mHeapImage == null) {
         this.mHeapImage = loader.loadImage(display, "heap.png", 16, 16, display.getSystemColor(9));
      }

      if(this.mWaitingImage == null) {
         this.mWaitingImage = loader.loadImage(display, "debug-wait.png", 16, 16, display.getSystemColor(3));
      }

      if(this.mDebuggerImage == null) {
         this.mDebuggerImage = loader.loadImage(display, "debug-attach.png", 16, 16, display.getSystemColor(5));
      }

      if(this.mDebugErrorImage == null) {
         this.mDebugErrorImage = loader.loadImage(display, "debug-error.png", 16, 16, display.getSystemColor(3));
      }

   }

   private static String getStateString(IDevice d) {
      DeviceState deviceState = d.getState();
      return deviceState == DeviceState.ONLINE?"Online":(deviceState == DeviceState.OFFLINE?"Offline":(deviceState == DeviceState.BOOTLOADER?"Bootloader":"??"));
   }

   private void exec(Runnable runnable) {
      try {
         Display e = this.mTree.getDisplay();
         e.asyncExec(runnable);
      } catch (SWTException var3) {
         AndroidDebugBridge.removeDebugBridgeChangeListener(this);
         AndroidDebugBridge.removeDeviceChangeListener(this);
         AndroidDebugBridge.removeClientChangeListener(this);
      }

   }

   private void notifyListeners() {
      TreeItem[] items = this.mTree.getSelection();
      Client client = null;
      IDevice device = null;
      if(items.length == 1) {
         Object object = items[0].getData();
         if(object instanceof Client) {
            client = (Client)object;
            device = client.getDevice();
         } else if(object instanceof IDevice) {
            device = (IDevice)object;
         }
      }

      this.notifyListeners(device, client);
   }

   private void notifyListeners(IDevice selectedDevice, Client selectedClient) {
      if(selectedDevice != this.mCurrentDevice || selectedClient != this.mCurrentClient) {
         this.mCurrentDevice = selectedDevice;
         this.mCurrentClient = selectedClient;
         Iterator i$ = this.mListeners.iterator();

         while(i$.hasNext()) {
            DevicePanel.IUiSelectionListener listener = (DevicePanel.IUiSelectionListener)i$.next();

            try {
               listener.selectionChanged(selectedDevice, selectedClient);
            } catch (Exception var6) {
               ;
            }
         }
      }

   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] $SwitchMap$com$android$ddmlib$ClientData$DebuggerStatus = new int[DebuggerStatus.values().length];

      static {
         try {
            $SwitchMap$com$android$ddmlib$ClientData$DebuggerStatus[DebuggerStatus.DEFAULT.ordinal()] = 1;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            $SwitchMap$com$android$ddmlib$ClientData$DebuggerStatus[DebuggerStatus.WAITING.ordinal()] = 2;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            $SwitchMap$com$android$ddmlib$ClientData$DebuggerStatus[DebuggerStatus.ATTACHED.ordinal()] = 3;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            $SwitchMap$com$android$ddmlib$ClientData$DebuggerStatus[DebuggerStatus.ERROR.ordinal()] = 4;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }

   public interface IUiSelectionListener {
      void selectionChanged(IDevice var1, Client var2);
   }

   private class LabelProvider implements ITableLabelProvider {
      private LabelProvider() {
      }

      public Image getColumnImage(Object element, int columnIndex) {
         if(columnIndex == 0 && element instanceof IDevice) {
            IDevice client1 = (IDevice)element;
            return client1.isEmulator()?DevicePanel.this.mEmulatorImage:DevicePanel.this.mDeviceImage;
         } else {
            if(element instanceof Client) {
               Client client = (Client)element;
               ClientData cd = client.getClientData();
               switch(columnIndex) {
               case 0:
                  switch(DevicePanel.SyntheticClass_1.$SwitchMap$com$android$ddmlib$ClientData$DebuggerStatus[cd.getDebuggerConnectionStatus().ordinal()]) {
                  case 1:
                     return null;
                  case 2:
                     return DevicePanel.this.mWaitingImage;
                  case 3:
                     return DevicePanel.this.mDebuggerImage;
                  case 4:
                     return DevicePanel.this.mDebugErrorImage;
                  default:
                     return null;
                  }
               case 1:
               default:
                  break;
               case 2:
                  if(client.isThreadUpdateEnabled()) {
                     return DevicePanel.this.mThreadImage;
                  }

                  return null;
               case 3:
                  if(client.isHeapUpdateEnabled()) {
                     return DevicePanel.this.mHeapImage;
                  }

                  return null;
               }
            }

            return null;
         }
      }

      public String getColumnText(Object element, int columnIndex) {
         String name;
         if(element instanceof IDevice) {
            IDevice client = (IDevice)element;
            switch(columnIndex) {
            case 0:
               return client.getName();
            case 1:
               return DevicePanel.getStateString(client);
            case 2:
            case 3:
            default:
               break;
            case 4:
               String cd = client.getProperty("ro.build.version.release");
               if(cd != null) {
                  name = client.getProperty("ro.debuggable");
                  if(client.isEmulator()) {
                     String port = client.getAvdName();
                     if(port == null) {
                        port = "?";
                     }

                     if(name != null && name.equals("1")) {
                        return String.format("%1$s [%2$s, debug]", new Object[]{port, cd});
                     }

                     return String.format("%1$s [%2$s]", new Object[]{port, cd});
                  }

                  if(name != null && name.equals("1")) {
                     return String.format("%1$s, debug", new Object[]{cd});
                  }

                  return String.format("%1$s", new Object[]{cd});
               }

               return "unknown";
            }
         } else if(element instanceof Client) {
            Client client1 = (Client)element;
            ClientData cd1 = client1.getClientData();
            switch(columnIndex) {
            case 0:
               name = cd1.getClientDescription();
               if(name != null) {
                  if(cd1.isValidUserId() && cd1.getUserId() != 0) {
                     return String.format(Locale.US, "%s (%d)", new Object[]{name, Integer.valueOf(cd1.getUserId())});
                  }else{
                     return name;
                  }
               }

               return "?";
            case 1:
               return Integer.toString(cd1.getPid());
            case 2:
            case 3:
            default:
               break;
            case 4:
               if(DevicePanel.this.mAdvancedPortSupport) {
                  int port1 = client1.getDebuggerListenPort();
                  String portString = "?";
                  if(port1 != 0) {
                     portString = Integer.toString(port1);
                  }

                  if(client1.isSelectedClient()) {
                     return String.format("%1$s / %2$d", new Object[]{portString, Integer.valueOf(DdmPreferences.getSelectedDebugPort())});
                  }

                  return portString;
               }
            }
         }

         return null;
      }

      public void addListener(ILabelProviderListener listener) {
      }

      public void dispose() {
      }

      public boolean isLabelProperty(Object element, String property) {
         return false;
      }

      public void removeListener(ILabelProviderListener listener) {
      }

      // $FF: synthetic method
      LabelProvider(Object x1) {
         this();
      }
   }

   private class ContentProvider implements ITreeContentProvider {
      private ContentProvider() {
      }

      public Object[] getChildren(Object parentElement) {
         return (Object[])(parentElement instanceof IDevice?((IDevice)parentElement).getClients():new Object[0]);
      }

      public Object getParent(Object element) {
         return element instanceof Client?((Client)element).getDevice():null;
      }

      public boolean hasChildren(Object element) {
         return element instanceof IDevice?((IDevice)element).hasClients():false;
      }

      public Object[] getElements(Object inputElement) {
         return (Object[])(inputElement instanceof AndroidDebugBridge?((AndroidDebugBridge)inputElement).getDevices():new Object[0]);
      }

      public void dispose() {
      }

      public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
      }

      // $FF: synthetic method
      ContentProvider(Object x1) {
         this();
      }
   }
}
