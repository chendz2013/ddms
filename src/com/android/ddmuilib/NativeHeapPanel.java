package com.android.ddmuilib;

import com.android.ddmlib.Client;
import com.android.ddmlib.ClientData;
import com.android.ddmlib.Log;
import com.android.ddmlib.NativeAllocationInfo;
import com.android.ddmlib.NativeLibraryMapInfo;
import com.android.ddmlib.NativeStackCallInfo;
import com.android.ddmuilib.Addr2Line;
import com.android.ddmuilib.BackgroundThread;
import com.android.ddmuilib.BaseHeapPanel;
import com.android.ddmuilib.DdmUiPreferences;
import com.android.ddmuilib.TableHelper;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.PaletteData;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Sash;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;

public final class NativeHeapPanel extends BaseHeapPanel {
   private static final int NUM_PALETTE_ENTRIES = 10;
   private static final String[] mMapLegend = new String[10];
   private static final PaletteData mMapPalette = createPalette();
   private static final int ALLOC_DISPLAY_ALL = 0;
   private static final int ALLOC_DISPLAY_PRE_ZYGOTE = 1;
   private static final int ALLOC_DISPLAY_POST_ZYGOTE = 2;
   private Display mDisplay;
   private Composite mBase;
   private Label mUpdateStatus;
   private Combo mAllocDisplayCombo;
   private Button mFullUpdateButton;
   private Combo mDisplayModeCombo;
   private Composite mTopStackComposite;
   private StackLayout mTopStackLayout;
   private Composite mAllocationStackComposite;
   private StackLayout mAllocationStackLayout;
   private Composite mTableModeControl;
   private Control mAllocationModeTop;
   private Control mLibraryModeTopControl;
   private Composite mPageUIComposite;
   private Label mTotalMemoryLabel;
   private Label mPageLabel;
   private Button mPageNextButton;
   private Button mPagePreviousButton;
   private Table mAllocationTable;
   private Table mLibraryTable;
   private Table mLibraryAllocationTable;
   private Table mDetailTable;
   private Label mImage;
   private int mAllocDisplayMode = 0;
   private NativeHeapPanel.StackCallThread mStackCallThread;
   private NativeHeapPanel.FillTableThread mFillTableThread;
   private ClientData mClientData;
   private ClientData mBackUpClientData;
   private final ArrayList<NativeAllocationInfo> mAllocations = new ArrayList();
   private final ArrayList<NativeAllocationInfo> mDisplayedAllocations = new ArrayList();
   private final ArrayList<NativeAllocationInfo> mBackUpAllocations = new ArrayList();
   private int mBackUpTotalMemory;
   private int mCurrentPage = 0;
   private int mPageCount = 0;
   private final ArrayList<NativeHeapPanel.LibraryAllocations> mLibraryAllocations = new ArrayList();
   private static final int NOT_SELECTED = 0;
   private static final int NOT_ENABLED = 1;
   private static final int ENABLED = 2;
   private static final int DISPLAY_PER_PAGE = 20;
   private static final String PREFS_ALLOCATION_SASH = "NHallocSash";
   private static final String PREFS_LIBRARY_SASH = "NHlibrarySash";
   private static final String PREFS_DETAIL_ADDRESS = "NHdetailAddress";
   private static final String PREFS_DETAIL_LIBRARY = "NHdetailLibrary";
   private static final String PREFS_DETAIL_METHOD = "NHdetailMethod";
   private static final String PREFS_DETAIL_FILE = "NHdetailFile";
   private static final String PREFS_DETAIL_LINE = "NHdetailLine";
   private static final String PREFS_ALLOC_TOTAL = "NHallocTotal";
   private static final String PREFS_ALLOC_COUNT = "NHallocCount";
   private static final String PREFS_ALLOC_SIZE = "NHallocSize";
   private static final String PREFS_ALLOC_LIBRARY = "NHallocLib";
   private static final String PREFS_ALLOC_METHOD = "NHallocMethod";
   private static final String PREFS_ALLOC_FILE = "NHallocFile";
   private static final String PREFS_LIB_LIBRARY = "NHlibLibrary";
   private static final String PREFS_LIB_SIZE = "NHlibSize";
   private static final String PREFS_LIB_COUNT = "NHlibCount";
   private static final String PREFS_LIBALLOC_TOTAL = "NHlibAllocTotal";
   private static final String PREFS_LIBALLOC_COUNT = "NHlibAllocCount";
   private static final String PREFS_LIBALLOC_SIZE = "NHlibAllocSize";
   private static final String PREFS_LIBALLOC_METHOD = "NHlibAllocMethod";
   private static DecimalFormat sFormatter = (DecimalFormat)NumberFormat.getInstance();
   private HashMap<Long, NativeStackCallInfo> mSourceCache = new HashMap();
   private long mTotalSize;
   private Button mSaveButton;
   private Button mSymbolsButton;

   protected Control createControl(Composite parent) {
      this.mDisplay = parent.getDisplay();
      this.mBase = new Composite(parent, 0);
      GridLayout gl = new GridLayout(1, false);
      gl.horizontalSpacing = 0;
      gl.verticalSpacing = 0;
      this.mBase.setLayout(gl);
      this.mBase.setLayoutData(new GridData(1808));
      Composite tmp = new Composite(this.mBase, 0);
      tmp.setLayoutData(new GridData(768));
      tmp.setLayout(gl = new GridLayout(2, false));
      gl.marginWidth = gl.marginHeight = 0;
      this.mFullUpdateButton = new Button(tmp, 0);
      this.mFullUpdateButton.setText("Full Update");
      this.mFullUpdateButton.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            NativeHeapPanel.this.mBackUpClientData = null;
            NativeHeapPanel.this.mDisplayModeCombo.setEnabled(false);
            NativeHeapPanel.this.mSaveButton.setEnabled(false);
            NativeHeapPanel.this.emptyTables();
            if(NativeHeapPanel.this.mStackCallThread != null && NativeHeapPanel.this.mStackCallThread.getClientData() == NativeHeapPanel.this.mClientData) {
               NativeHeapPanel.this.mStackCallThread.quit();
               NativeHeapPanel.this.mStackCallThread = null;
            }

            NativeHeapPanel.this.mLibraryAllocations.clear();
            Client client = NativeHeapPanel.this.getCurrentClient();
            if(client != null) {
               client.requestNativeHeapInformation();
            }

         }
      });
      this.mUpdateStatus = new Label(tmp, 0);
      this.mUpdateStatus.setLayoutData(new GridData(768));
      Composite top_layout = new Composite(this.mBase, 0);
      top_layout.setLayout(gl = new GridLayout(4, false));
      gl.marginWidth = gl.marginHeight = 0;
      (new Label(top_layout, 0)).setText("Show:");
      this.mAllocDisplayCombo = new Combo(top_layout, 12);
      this.mAllocDisplayCombo.setLayoutData(new GridData(768));
      this.mAllocDisplayCombo.add("All Allocations");
      this.mAllocDisplayCombo.add("Pre-Zygote Allocations");
      this.mAllocDisplayCombo.add("Zygote Child Allocations (Z)");
      this.mAllocDisplayCombo.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            NativeHeapPanel.this.onAllocDisplayChange();
         }
      });
      this.mAllocDisplayCombo.select(0);
      Label separator = new Label(top_layout, 514);
      GridData gd;
      separator.setLayoutData(gd = new GridData(1040));
      gd.heightHint = 0;
      gd.verticalSpan = 2;
      this.mSaveButton = new Button(top_layout, 8);
      this.mSaveButton.setText("Save...");
      this.mSaveButton.setEnabled(false);
      this.mSaveButton.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            FileDialog fileDialog = new FileDialog(NativeHeapPanel.this.mBase.getShell(), 8192);
            fileDialog.setText("Save Allocations");
            fileDialog.setFileName("allocations.txt");
            String fileName = fileDialog.open();
            if(fileName != null) {
               NativeHeapPanel.this.saveAllocations(fileName);
            }

         }
      });
      Label l = new Label(top_layout, 0);
      l.setText("Display:");
      this.mDisplayModeCombo = new Combo(top_layout, 12);
      this.mDisplayModeCombo.setLayoutData(new GridData(768));
      this.mDisplayModeCombo.setItems(new String[]{"Allocation List", "By Libraries"});
      this.mDisplayModeCombo.select(0);
      this.mDisplayModeCombo.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            NativeHeapPanel.this.switchDisplayMode();
         }
      });
      this.mDisplayModeCombo.setEnabled(false);
      this.mSymbolsButton = new Button(top_layout, 8);
      this.mSymbolsButton.setText("Load Symbols");
      this.mSymbolsButton.setEnabled(false);
      this.mTopStackComposite = new Composite(this.mBase, 0);
      this.mTopStackComposite.setLayout(this.mTopStackLayout = new StackLayout());
      this.mTopStackComposite.setLayoutData(new GridData(1808));
      this.createTableDisplay(this.mTopStackComposite);
      this.mTopStackLayout.topControl = this.mTableModeControl;
      this.mTopStackComposite.layout();
      this.setUpdateStatus(0);
      this.mBase.pack();
      return this.mBase;
   }

   public void setFocus() {
   }

   public void clientChanged(Client client, int changeMask) {
      if(client == this.getCurrentClient() && (changeMask & 128) == 128) {
         if(this.mBase.isDisposed()) {
            return;
         }

         this.mBase.getDisplay().asyncExec(new Runnable() {
            public void run() {
               NativeHeapPanel.this.clientSelected();
            }
         });
      }

   }

   public void deviceSelected() {
   }

   public void clientSelected() {
      if(!this.mBase.isDisposed()) {
         Client client = this.getCurrentClient();
         this.mDisplayModeCombo.setEnabled(false);
         this.emptyTables();
         Log.d("ddms", "NativeHeapPanel: changed " + client);
         if(client != null) {
            ClientData cd = client.getClientData();
            this.mClientData = cd;
            this.setUpdateStatus(2);
            this.initAllocationDisplay();
         } else {
            this.mClientData = null;
            this.setUpdateStatus(0);
         }

         this.mBase.pack();
      }
   }

   public void updateAllocationStackCalls(ClientData cd, int count) {
      if(cd == this.mClientData) {
         int total = this.mAllocations.size();
         if(count == total) {
            this.mDisplayModeCombo.setEnabled(true);
            this.mSaveButton.setEnabled(true);
            this.mStackCallThread = null;
         }

         try {
            int e = this.mAllocationTable.getSelectionIndex();
            NativeAllocationInfo info = null;
            if(e != -1) {
               info = (NativeAllocationInfo)this.mAllocationTable.getItem(e).getData();
            }

            this.emptyTables();
            this.fillAllocationTable();
            this.mAllocationTable.setSelection(e);
            if(info != null) {
               this.fillDetailTable(info);
            }
         } catch (SWTException var6) {
            if(!this.mAllocationTable.isDisposed()) {
               throw var6;
            }
         }
      }

   }

   protected void setTableFocusListener() {
      this.addTableToFocusListener(this.mAllocationTable);
      this.addTableToFocusListener(this.mLibraryTable);
      this.addTableToFocusListener(this.mLibraryAllocationTable);
      this.addTableToFocusListener(this.mDetailTable);
   }

   protected void onAllocDisplayChange() {
      this.mAllocDisplayMode = this.mAllocDisplayCombo.getSelectionIndex();
      this.updateAllocDisplayList();
      this.updateTotalMemoryDisplay();
      this.mCurrentPage = 0;
      this.updatePageUI();
      this.switchDisplayMode();
   }

   private void updateAllocDisplayList() {
      this.mTotalSize = 0L;
      this.mDisplayedAllocations.clear();
      Iterator count = this.mAllocations.iterator();

      while(true) {
         NativeAllocationInfo info;
         do {
            if(!count.hasNext()) {
               int var3 = this.mDisplayedAllocations.size();
               this.mPageCount = var3 / 20;
               if(var3 % 20 > 0) {
                  ++this.mPageCount;
               }

               return;
            }

            info = (NativeAllocationInfo)count.next();
         } while(this.mAllocDisplayMode != 0 && !(this.mAllocDisplayMode == 1 ^ info.isZygoteChild()));

         this.mDisplayedAllocations.add(info);
         this.mTotalSize += (long)(info.getSize() * info.getAllocationCount());
      }
   }

   private void updateTotalMemoryDisplay() {
      switch(this.mAllocDisplayMode) {
      case 0:
         this.mTotalMemoryLabel.setText(String.format("Total Memory: %1$s Bytes", new Object[]{sFormatter.format(this.mTotalSize)}));
         break;
      case 1:
         this.mTotalMemoryLabel.setText(String.format("Zygote Memory: %1$s Bytes", new Object[]{sFormatter.format(this.mTotalSize)}));
         break;
      case 2:
         this.mTotalMemoryLabel.setText(String.format("Post-zygote Memory: %1$s Bytes", new Object[]{sFormatter.format(this.mTotalSize)}));
      }

   }

   private void switchDisplayMode() {
      switch(this.mDisplayModeCombo.getSelectionIndex()) {
      case 0:
         this.mTopStackLayout.topControl = this.mTableModeControl;
         this.mAllocationStackLayout.topControl = this.mAllocationModeTop;
         this.mAllocationStackComposite.layout();
         this.mTopStackComposite.layout();
         this.emptyTables();
         this.fillAllocationTable();
         break;
      case 1:
         this.mTopStackLayout.topControl = this.mTableModeControl;
         this.mAllocationStackLayout.topControl = this.mLibraryModeTopControl;
         this.mAllocationStackComposite.layout();
         this.mTopStackComposite.layout();
         this.emptyTables();
         this.fillLibraryTable();
      }

   }

   private void initAllocationDisplay() {
      if(this.mStackCallThread != null) {
         this.mStackCallThread.quit();
      }

      this.mAllocations.clear();
      this.mAllocations.addAll(this.mClientData.getNativeAllocationList());
      this.updateAllocDisplayList();
      if(this.mBackUpClientData != null && this.mBackUpClientData == this.mClientData) {
         ArrayList add = new ArrayList();
         Iterator count = this.mAllocations.iterator();

         while(count.hasNext()) {
            NativeAllocationInfo i$ = (NativeAllocationInfo)count.next();
            boolean allocInfo = false;
            Iterator i$1 = this.mBackUpAllocations.iterator();

            while(i$1.hasNext()) {
               NativeAllocationInfo old_mi = (NativeAllocationInfo)i$1.next();
               if(i$.equals(old_mi)) {
                  allocInfo = true;
                  break;
               }
            }

            if(!allocInfo) {
               add.add(i$);
            }
         }

         this.mAllocations.clear();
         this.mAllocations.addAll(add);
         int count1 = 0;

         NativeAllocationInfo allocInfo1;
         for(Iterator i$2 = this.mAllocations.iterator(); i$2.hasNext(); count1 += allocInfo1.getSize() * allocInfo1.getAllocationCount()) {
            allocInfo1 = (NativeAllocationInfo)i$2.next();
         }

         this.mTotalMemoryLabel.setText(String.format("Memory Difference: %1$s Bytes", new Object[]{sFormatter.format((long)count1)}));
      } else {
         this.updateTotalMemoryDisplay();
      }

      this.mTotalMemoryLabel.pack();
      this.mDisplayModeCombo.select(0);
      this.mLibraryAllocations.clear();
      this.mCurrentPage = 0;
      this.updatePageUI();
      this.switchDisplayMode();
      if(this.mAllocations.size() > 0) {
         this.mStackCallThread = new NativeHeapPanel.StackCallThread(this.mClientData);
         this.mStackCallThread.start();
      }

   }

   private void updatePageUI() {
      if(this.mPageCount == 0) {
         this.mPageLabel.setText("0 of 0 allocations.");
      } else {
         StringBuffer buffer = new StringBuffer();
         int start = this.mCurrentPage * 20 + 1;
         int count = this.mDisplayedAllocations.size();
         int end = Math.min(start + 20 - 1, count);
         buffer.append(sFormatter.format((long)start));
         buffer.append(" - ");
         buffer.append(sFormatter.format((long)end));
         buffer.append(" of ");
         buffer.append(sFormatter.format((long)count));
         buffer.append(" allocations.");
         this.mPageLabel.setText(buffer.toString());
      }

      this.mPagePreviousButton.setEnabled(this.mCurrentPage > 0);
      this.mPageNextButton.setEnabled(this.mCurrentPage < this.mPageCount - 1);
      this.mPageLabel.pack();
      this.mPageUIComposite.pack();
   }

   private void fillAllocationTable() {
      int count = this.mDisplayedAllocations.size();
      int start = this.mCurrentPage * 20;
      int end = start + 20;

      for(int i = start; i < end && i < count; ++i) {
         NativeAllocationInfo info = (NativeAllocationInfo)this.mDisplayedAllocations.get(i);
         TableItem item = null;
         if(this.mAllocDisplayMode == 0) {
            item = new TableItem(this.mAllocationTable, 0);
            item.setText(0, (info.isZygoteChild()?"Z ":"") + sFormatter.format((long)(info.getSize() * info.getAllocationCount())));
            item.setText(1, sFormatter.format((long)info.getAllocationCount()));
            item.setText(2, sFormatter.format((long)info.getSize()));
         } else {
            if(!(this.mAllocDisplayMode == 1 ^ info.isZygoteChild())) {
               continue;
            }

            item = new TableItem(this.mAllocationTable, 0);
            item.setText(0, sFormatter.format((long)(info.getSize() * info.getAllocationCount())));
            item.setText(1, sFormatter.format((long)info.getAllocationCount()));
            item.setText(2, sFormatter.format((long)info.getSize()));
         }

         item.setData(info);
         NativeStackCallInfo bti = info.getRelevantStackCallInfo();
         if(bti != null) {
            String lib = bti.getLibraryName();
            String method = bti.getMethodName();
            String source = bti.getSourceFile();
            if(lib != null) {
               item.setText(3, lib);
            }

            if(method != null) {
               item.setText(4, method);
            }

            if(source != null) {
               item.setText(5, source);
            }
         }
      }

   }

   private void fillLibraryTable() {
      this.sortAllocationsPerLibrary();
      Iterator i$ = this.mLibraryAllocations.iterator();

      while(i$.hasNext()) {
         NativeHeapPanel.LibraryAllocations liballoc = (NativeHeapPanel.LibraryAllocations)i$.next();
         if(liballoc != null) {
            TableItem item = new TableItem(this.mLibraryTable, 0);
            String lib = liballoc.getLibrary();
            item.setText(0, lib != null?lib:"");
            item.setText(1, sFormatter.format((long)liballoc.getSize()));
            item.setText(2, sFormatter.format((long)liballoc.getCount()));
         }
      }

   }

   private void fillLibraryAllocationTable() {
      this.mLibraryAllocationTable.removeAll();
      this.mDetailTable.removeAll();
      int index = this.mLibraryTable.getSelectionIndex();
      if(index != -1) {
         NativeHeapPanel.LibraryAllocations liballoc = (NativeHeapPanel.LibraryAllocations)this.mLibraryAllocations.get(index);
         if(this.mFillTableThread != null) {
            this.mFillTableThread.quit();
         }

         this.mFillTableThread = new NativeHeapPanel.FillTableThread(liballoc, liballoc.getAllocationSize());
         this.mFillTableThread.start();
      }

   }

   public void updateLibraryAllocationTable(NativeHeapPanel.LibraryAllocations liballoc, int start, int end) {
      try {
         if(!this.mLibraryTable.isDisposed()) {
            int e = this.mLibraryTable.getSelectionIndex();
            if(e != -1) {
               NativeHeapPanel.LibraryAllocations newliballoc = (NativeHeapPanel.LibraryAllocations)this.mLibraryAllocations.get(e);
               if(newliballoc == liballoc) {
                  int count = liballoc.getAllocationSize();

                  for(int i = start; i < end && i < count; ++i) {
                     NativeAllocationInfo info = liballoc.getAllocation(i);
                     TableItem item = new TableItem(this.mLibraryAllocationTable, 0);
                     item.setText(0, sFormatter.format((long)(info.getSize() * info.getAllocationCount())));
                     item.setText(1, sFormatter.format((long)info.getAllocationCount()));
                     item.setText(2, sFormatter.format((long)info.getSize()));
                     NativeStackCallInfo stackCallInfo = info.getRelevantStackCallInfo();
                     if(stackCallInfo != null) {
                        item.setText(3, stackCallInfo.getMethodName());
                     }
                  }
               } else if(this.mFillTableThread != null) {
                  this.mFillTableThread.quit();
                  this.mFillTableThread = null;
               }
            }
         }
      } catch (SWTException var11) {
         Log.e("ddms", "error when updating the library allocation table");
      }

   }

   private void fillDetailTable(NativeAllocationInfo mi) {
      this.mDetailTable.removeAll();
      this.mDetailTable.setRedraw(false);

      try {
         List addresses = mi.getStackCallAddresses();
         List resolvedStackCall = mi.getResolvedStackCall();
         if(resolvedStackCall != null) {
            for(int i = 0; i < resolvedStackCall.size(); ++i) {
               if(addresses.get(i) != null && ((Long)addresses.get(i)).longValue() != 0L) {
                  long addr = ((Long)addresses.get(i)).longValue();
                  NativeStackCallInfo source = (NativeStackCallInfo)resolvedStackCall.get(i);
                  TableItem item = new TableItem(this.mDetailTable, 0);
                  item.setText(0, String.format("%08x", new Object[]{Long.valueOf(addr)}));
                  String libraryName = source.getLibraryName();
                  String methodName = source.getMethodName();
                  String sourceFile = source.getSourceFile();
                  int lineNumber = source.getLineNumber();
                  if(libraryName != null) {
                     item.setText(1, libraryName);
                  }

                  if(methodName != null) {
                     item.setText(2, methodName);
                  }

                  if(sourceFile != null) {
                     item.setText(3, sourceFile);
                  }

                  if(lineNumber != -1) {
                     item.setText(4, Integer.toString(lineNumber));
                  }
               }
            }

            return;
         }
      } finally {
         this.mDetailTable.setRedraw(true);
      }

   }

   private void setUpdateStatus(int status) {
      switch(status) {
      case 0:
         this.mUpdateStatus.setText("Select a client to see heap info");
         this.mAllocDisplayCombo.setEnabled(false);
         this.mFullUpdateButton.setEnabled(false);
         break;
      case 1:
         this.mUpdateStatus.setText("Heap updates are NOT ENABLED for this client");
         this.mAllocDisplayCombo.setEnabled(false);
         this.mFullUpdateButton.setEnabled(false);
         break;
      case 2:
         this.mUpdateStatus.setText("Press \'Full Update\' to retrieve latest data");
         this.mAllocDisplayCombo.setEnabled(true);
         this.mFullUpdateButton.setEnabled(true);
         break;
      default:
         throw new RuntimeException();
      }

      this.mUpdateStatus.pack();
   }

   private void createTableDisplay(Composite base) {
      boolean minPanelWidth = true;
      final IPreferenceStore prefs = DdmUiPreferences.getStore();
      this.mTableModeControl = new Composite(base, 0);
      GridLayout gl = new GridLayout(1, false);
      gl.marginLeft = gl.marginRight = gl.marginTop = gl.marginBottom = 0;
      this.mTableModeControl.setLayout(gl);
      this.mTableModeControl.setLayoutData(new GridData(1808));
      this.mTotalMemoryLabel = new Label(this.mTableModeControl, 0);
      this.mTotalMemoryLabel.setLayoutData(new GridData(768));
      this.mTotalMemoryLabel.setText("Total Memory: 0 Bytes");
      final Composite sash_composite = new Composite(this.mTableModeControl, 0);
      sash_composite.setLayout(new FormLayout());
      sash_composite.setLayoutData(new GridData(1808));
      this.mAllocationStackComposite = new Composite(sash_composite, 0);
      this.mAllocationStackLayout = new StackLayout();
      this.mAllocationStackComposite.setLayout(this.mAllocationStackLayout);
      this.mAllocationStackComposite.setLayoutData(new GridData(1808));
      this.createAllocationTopHalf(this.mAllocationStackComposite);
      this.createLibraryTopHalf(this.mAllocationStackComposite);
      final Sash sash = new Sash(sash_composite, 256);
      this.createDetailTable(sash_composite);
      this.mAllocationStackLayout.topControl = this.mAllocationModeTop;
      FormData data = new FormData();
      data.top = new FormAttachment(this.mTotalMemoryLabel, 0);
      data.bottom = new FormAttachment(sash, 0);
      data.left = new FormAttachment(0, 0);
      data.right = new FormAttachment(100, 0);
      this.mAllocationStackComposite.setLayoutData(data);
      final FormData sashData = new FormData();
      if(prefs != null && prefs.contains("NHallocSash")) {
         sashData.top = new FormAttachment(0, prefs.getInt("NHallocSash"));
      } else {
         sashData.top = new FormAttachment(50, 0);
      }

      sashData.left = new FormAttachment(0, 0);
      sashData.right = new FormAttachment(100, 0);
      sash.setLayoutData(sashData);
      data = new FormData();
      data.top = new FormAttachment(sash, 0);
      data.bottom = new FormAttachment(100, 0);
      data.left = new FormAttachment(0, 0);
      data.right = new FormAttachment(100, 0);
      this.mDetailTable.setLayoutData(data);
      sash.addListener(13, new Listener() {
         public void handleEvent(Event e) {
            Rectangle sashRect = sash.getBounds();
            Rectangle panelRect = sash_composite.getClientArea();
            int bottom = panelRect.height - sashRect.height - 60;
            e.y = Math.max(Math.min(e.y, bottom), 60);
            if(e.y != sashRect.y) {
               sashData.top = new FormAttachment(0, e.y);
               prefs.setValue("NHallocSash", e.y);
               sash_composite.layout();
            }

         }
      });
   }

   private void createDetailTable(Composite base) {
      IPreferenceStore prefs = DdmUiPreferences.getStore();
      this.mDetailTable = new Table(base, 65538);
      this.mDetailTable.setLayoutData(new GridData(1808));
      this.mDetailTable.setHeaderVisible(true);
      this.mDetailTable.setLinesVisible(true);
      TableHelper.createTableColumn(this.mDetailTable, "Address", 131072, "00000000", "NHdetailAddress", prefs);
      TableHelper.createTableColumn(this.mDetailTable, "Library", 16384, "abcdefghijklmnopqrst", "NHdetailLibrary", prefs);
      TableHelper.createTableColumn(this.mDetailTable, "Method", 16384, "abcdefghijklmnopqrst", "NHdetailMethod", prefs);
      TableHelper.createTableColumn(this.mDetailTable, "File", 16384, "abcdefghijklmnopqrstuvwxyz", "NHdetailFile", prefs);
      TableHelper.createTableColumn(this.mDetailTable, "Line", 131072, "9,999", "NHdetailLine", prefs);
   }

   private void createAllocationTopHalf(Composite b) {
      IPreferenceStore prefs = DdmUiPreferences.getStore();
      Composite base = new Composite(b, 0);
      this.mAllocationModeTop = base;
      GridLayout gl = new GridLayout(1, false);
      gl.marginLeft = gl.marginRight = gl.marginTop = gl.marginBottom = 0;
      gl.verticalSpacing = 0;
      base.setLayout(gl);
      base.setLayoutData(new GridData(1808));
      this.mPageUIComposite = new Composite(base, 0);
      this.mPageUIComposite.setLayoutData(new GridData(32));
      gl = new GridLayout(3, false);
      gl.marginLeft = gl.marginRight = gl.marginTop = gl.marginBottom = 0;
      gl.horizontalSpacing = 0;
      this.mPageUIComposite.setLayout(gl);
      this.mPagePreviousButton = new Button(this.mPageUIComposite, 0);
      this.mPagePreviousButton.setText("<");
      this.mPagePreviousButton.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            NativeHeapPanel.access$1310(NativeHeapPanel.this);
            NativeHeapPanel.this.updatePageUI();
            NativeHeapPanel.this.emptyTables();
            NativeHeapPanel.this.fillAllocationTable();
         }
      });
      this.mPageNextButton = new Button(this.mPageUIComposite, 0);
      this.mPageNextButton.setText(">");
      this.mPageNextButton.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            NativeHeapPanel.access$1308(NativeHeapPanel.this);
            NativeHeapPanel.this.updatePageUI();
            NativeHeapPanel.this.emptyTables();
            NativeHeapPanel.this.fillAllocationTable();
         }
      });
      this.mPageLabel = new Label(this.mPageUIComposite, 0);
      this.mPageLabel.setLayoutData(new GridData(768));
      this.updatePageUI();
      this.mAllocationTable = new Table(base, 65538);
      this.mAllocationTable.setLayoutData(new GridData(1808));
      this.mAllocationTable.setHeaderVisible(true);
      this.mAllocationTable.setLinesVisible(true);
      TableHelper.createTableColumn(this.mAllocationTable, "Total", 131072, "9,999,999", "NHallocTotal", prefs);
      TableHelper.createTableColumn(this.mAllocationTable, "Count", 131072, "9,999", "NHallocCount", prefs);
      TableHelper.createTableColumn(this.mAllocationTable, "Size", 131072, "999,999", "NHallocSize", prefs);
      TableHelper.createTableColumn(this.mAllocationTable, "Library", 16384, "abcdefghijklmnopqrst", "NHallocLib", prefs);
      TableHelper.createTableColumn(this.mAllocationTable, "Method", 16384, "abcdefghijklmnopqrst", "NHallocMethod", prefs);
      TableHelper.createTableColumn(this.mAllocationTable, "File", 16384, "abcdefghijklmnopqrstuvwxyz", "NHallocFile", prefs);
      this.mAllocationTable.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            int index = NativeHeapPanel.this.mAllocationTable.getSelectionIndex();
            if(index >= 0 && index < NativeHeapPanel.this.mAllocationTable.getItemCount()) {
               TableItem item = NativeHeapPanel.this.mAllocationTable.getItem(index);
               if(item != null && item.getData() instanceof NativeAllocationInfo) {
                  NativeHeapPanel.this.fillDetailTable((NativeAllocationInfo)item.getData());
               }
            }

         }
      });
   }

   private void createLibraryTopHalf(Composite base) {
      boolean minPanelWidth = true;
      final IPreferenceStore prefs = DdmUiPreferences.getStore();
      final Composite top = new Composite(base, 0);
      this.mLibraryModeTopControl = top;
      top.setLayout(new FormLayout());
      top.setLayoutData(new GridData(1808));
      this.mLibraryTable = new Table(top, 65538);
      this.mLibraryTable.setLayoutData(new GridData(1808));
      this.mLibraryTable.setHeaderVisible(true);
      this.mLibraryTable.setLinesVisible(true);
      TableHelper.createTableColumn(this.mLibraryTable, "Library", 16384, "abcdefghijklmnopqrstuvwxyz", "NHlibLibrary", prefs);
      TableHelper.createTableColumn(this.mLibraryTable, "Size", 131072, "9,999,999", "NHlibSize", prefs);
      TableHelper.createTableColumn(this.mLibraryTable, "Count", 131072, "9,999", "NHlibCount", prefs);
      this.mLibraryTable.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            NativeHeapPanel.this.fillLibraryAllocationTable();
         }
      });
      final Sash sash = new Sash(top, 512);
      this.mLibraryAllocationTable = new Table(top, 65538);
      this.mLibraryAllocationTable.setLayoutData(new GridData(1808));
      this.mLibraryAllocationTable.setHeaderVisible(true);
      this.mLibraryAllocationTable.setLinesVisible(true);
      TableHelper.createTableColumn(this.mLibraryAllocationTable, "Total", 131072, "9,999,999", "NHlibAllocTotal", prefs);
      TableHelper.createTableColumn(this.mLibraryAllocationTable, "Count", 131072, "9,999", "NHlibAllocCount", prefs);
      TableHelper.createTableColumn(this.mLibraryAllocationTable, "Size", 131072, "999,999", "NHlibAllocSize", prefs);
      TableHelper.createTableColumn(this.mLibraryAllocationTable, "Method", 16384, "abcdefghijklmnopqrst", "NHlibAllocMethod", prefs);
      this.mLibraryAllocationTable.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            int index1 = NativeHeapPanel.this.mLibraryTable.getSelectionIndex();
            int index2 = NativeHeapPanel.this.mLibraryAllocationTable.getSelectionIndex();
            if(index1 != -1 && index2 != -1) {
               NativeHeapPanel.LibraryAllocations liballoc = (NativeHeapPanel.LibraryAllocations)NativeHeapPanel.this.mLibraryAllocations.get(index1);
               NativeAllocationInfo info = liballoc.getAllocation(index2);
               NativeHeapPanel.this.fillDetailTable(info);
            }

         }
      });
      FormData data = new FormData();
      data.top = new FormAttachment(0, 0);
      data.bottom = new FormAttachment(100, 0);
      data.left = new FormAttachment(0, 0);
      data.right = new FormAttachment(sash, 0);
      this.mLibraryTable.setLayoutData(data);
      final FormData sashData = new FormData();
      if(prefs != null && prefs.contains("NHlibrarySash")) {
         sashData.left = new FormAttachment(0, prefs.getInt("NHlibrarySash"));
      } else {
         sashData.left = new FormAttachment(50, 0);
      }

      sashData.bottom = new FormAttachment(100, 0);
      sashData.top = new FormAttachment(0, 0);
      sash.setLayoutData(sashData);
      data = new FormData();
      data.top = new FormAttachment(0, 0);
      data.bottom = new FormAttachment(100, 0);
      data.left = new FormAttachment(sash, 0);
      data.right = new FormAttachment(100, 0);
      this.mLibraryAllocationTable.setLayoutData(data);
      sash.addListener(13, new Listener() {
         public void handleEvent(Event e) {
            Rectangle sashRect = sash.getBounds();
            Rectangle panelRect = top.getClientArea();
            int right = panelRect.width - sashRect.width - 60;
            e.x = Math.max(Math.min(e.x, right), 60);
            if(e.x != sashRect.x) {
               sashData.left = new FormAttachment(0, e.x);
               prefs.setValue("NHlibrarySash", e.y);
               top.layout();
            }

         }
      });
   }

   private void emptyTables() {
      this.mAllocationTable.removeAll();
      this.mLibraryTable.removeAll();
      this.mLibraryAllocationTable.removeAll();
      this.mDetailTable.removeAll();
   }

   private void sortAllocationsPerLibrary() {
      if(this.mClientData != null) {
         this.mLibraryAllocations.clear();
         HashMap libcache = new HashMap();
         int count = this.mDisplayedAllocations.size();

         for(int i$ = 0; i$ < count; ++i$) {
            NativeAllocationInfo liballoc = (NativeAllocationInfo)this.mDisplayedAllocations.get(i$);
            NativeStackCallInfo stackCallInfo = liballoc.getRelevantStackCallInfo();
            if(stackCallInfo != null) {
               String libraryName = stackCallInfo.getLibraryName();
               NativeHeapPanel.LibraryAllocations liballoc1 = (NativeHeapPanel.LibraryAllocations)libcache.get(libraryName);
               if(liballoc1 == null) {
                  liballoc1 = new NativeHeapPanel.LibraryAllocations(libraryName);
                  libcache.put(libraryName, liballoc1);
                  this.mLibraryAllocations.add(liballoc1);
               }

               liballoc1.addAllocation(liballoc);
            }
         }

         Iterator var8 = this.mLibraryAllocations.iterator();

         while(var8.hasNext()) {
            NativeHeapPanel.LibraryAllocations var9 = (NativeHeapPanel.LibraryAllocations)var8.next();
            var9.computeAllocationSizeAndCount();
         }

         Collections.sort(this.mLibraryAllocations, new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
               return compare((NativeHeapPanel.LibraryAllocations)o1, (NativeHeapPanel.LibraryAllocations)o2);
            }

            public int compare(NativeHeapPanel.LibraryAllocations o1, NativeHeapPanel.LibraryAllocations o2) {
               return o2.getSize() - o1.getSize();
            }
         });
      }

   }

   private void renderBitmap(ClientData cd) {
      synchronized(cd) {
         if(this.serializeHeapData(cd.getVmHeapData())) {
            byte[] pixData = this.getSerializedData();
            ImageData id = this.createLinearHeapImage(pixData, 200, mMapPalette);
            Image image = new Image(this.mBase.getDisplay(), id);
            this.mImage.setImage(image);
            this.mImage.pack(true);
         }
      }
   }

   private static PaletteData createPalette() {
      RGB[] colors = new RGB[10];
      colors[0] = new RGB(192, 192, 192);
      mMapLegend[0] = "(heap expansion area)";
      colors[1] = new RGB(0, 0, 0);
      mMapLegend[1] = "free";
      colors[2] = new RGB(0, 0, 255);
      mMapLegend[2] = "data object";
      colors[3] = new RGB(0, 255, 0);
      mMapLegend[3] = "class object";
      colors[4] = new RGB(255, 0, 0);
      mMapLegend[4] = "1-byte array (byte[], boolean[])";
      colors[5] = new RGB(255, 128, 0);
      mMapLegend[5] = "2-byte array (short[], char[])";
      colors[6] = new RGB(255, 255, 0);
      mMapLegend[6] = "4-byte array (object[], int[], float[])";
      colors[7] = new RGB(255, 128, 128);
      mMapLegend[7] = "8-byte array (long[], double[])";
      colors[8] = new RGB(255, 0, 255);
      mMapLegend[8] = "unknown object";
      colors[9] = new RGB(64, 64, 64);
      mMapLegend[9] = "non-Java object";
      return new PaletteData(colors);
   }

   private void saveAllocations(String fileName) {
      try {
         PrintWriter e = new PrintWriter(new BufferedWriter(new FileWriter(fileName)));
         Iterator i$ = this.mAllocations.iterator();

         while(i$.hasNext()) {
            NativeAllocationInfo alloc = (NativeAllocationInfo)i$.next();
            e.println(alloc.toString());
         }

         e.close();
      } catch (IOException var5) {
         Log.e("Native", var5);
      }

   }

   // $FF: synthetic method
   static int access$1310(NativeHeapPanel x0) {
      return x0.mCurrentPage--;
   }

   // $FF: synthetic method
   static int access$1308(NativeHeapPanel x0) {
      return x0.mCurrentPage++;
   }

   static {
      if(sFormatter == null) {
         sFormatter = new DecimalFormat("#,###");
      } else {
         sFormatter.applyPattern("#,###");
      }

   }

   public static class LibraryAllocations {
      private String mLibrary;
      private final ArrayList<NativeAllocationInfo> mLibAllocations = new ArrayList();
      private int mSize;
      private int mCount;

      public LibraryAllocations(String lib) {
         this.mLibrary = lib;
      }

      public String getLibrary() {
         return this.mLibrary;
      }

      public void addAllocation(NativeAllocationInfo info) {
         this.mLibAllocations.add(info);
      }

      public Iterator<NativeAllocationInfo> getAllocations() {
         return this.mLibAllocations.iterator();
      }

      public NativeAllocationInfo getAllocation(int index) {
         return (NativeAllocationInfo)this.mLibAllocations.get(index);
      }

      public int getAllocationSize() {
         return this.mLibAllocations.size();
      }

      public int getSize() {
         return this.mSize;
      }

      public int getCount() {
         return this.mCount;
      }

      public void computeAllocationSizeAndCount() {
         this.mSize = 0;
         this.mCount = 0;

         NativeAllocationInfo info;
         for(Iterator i$ = this.mLibAllocations.iterator(); i$.hasNext(); this.mSize += info.getAllocationCount() * info.getSize()) {
            info = (NativeAllocationInfo)i$.next();
            this.mCount += info.getAllocationCount();
         }

         Collections.sort(this.mLibAllocations, new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
               return compare((NativeAllocationInfo)o1,(NativeAllocationInfo)o2);
            }

            public int compare(NativeAllocationInfo o1, NativeAllocationInfo o2) {
               return o2.getAllocationCount() * o2.getSize() - o1.getAllocationCount() * o1.getSize();
            }
         });
      }
   }

   private class FillTableThread extends BackgroundThread {
      private NativeHeapPanel.LibraryAllocations mLibAlloc;
      private int mMax;

      public FillTableThread(NativeHeapPanel.LibraryAllocations liballoc, int m) {
         this.mLibAlloc = liballoc;
         this.mMax = m;
      }

      public void run() {
         for(int i = this.mMax; i > 0 && !this.isQuitting(); i -= 10) {
            this.updateNHLibraryAllocationTable(this.mLibAlloc, this.mMax - i, this.mMax - i + 10);
         }

      }

      private void updateNHLibraryAllocationTable(final NativeHeapPanel.LibraryAllocations libAlloc, final int start, final int end) {
         if(!NativeHeapPanel.this.mDisplay.isDisposed()) {
            NativeHeapPanel.this.mDisplay.asyncExec(new Runnable() {
               public void run() {
                  NativeHeapPanel.this.updateLibraryAllocationTable(libAlloc, start, end);
               }
            });
         }

      }
   }

   private class StackCallThread extends BackgroundThread {
      private ClientData mClientData;

      public StackCallThread(ClientData cd) {
         this.mClientData = cd;
      }

      public ClientData getClientData() {
         return this.mClientData;
      }

      public void run() {
         Iterator iter = NativeHeapPanel.this.mAllocations.iterator();
         int total = NativeHeapPanel.this.mAllocations.size();
         int count = 0;

         do {
            if(!iter.hasNext()) {
               this.updateNHAllocationStackCalls(this.mClientData, count);
               return;
            }

            if(this.isQuitting()) {
               return;
            }

            NativeAllocationInfo info = (NativeAllocationInfo)iter.next();
            if(!info.isStackCallResolved()) {
               List list = info.getStackCallAddresses();
               int size = list.size();
               ArrayList resolvedStackCall = new ArrayList();

               for(int i = 0; i < size; ++i) {
                  long addr = ((Long)list.get(i)).longValue();
                  NativeStackCallInfo source = (NativeStackCallInfo)NativeHeapPanel.this.mSourceCache.get(Long.valueOf(addr));
                  if(source == null) {
                     source = this.sourceForAddr(addr);
                     NativeHeapPanel.this.mSourceCache.put(Long.valueOf(addr), source);
                  }

                  resolvedStackCall.add(source);
               }

               info.setResolvedStackCall(resolvedStackCall);
            }

            ++count;
         } while(count % 20 != 0 || count == total || this.updateNHAllocationStackCalls(this.mClientData, count));

      }

      private NativeStackCallInfo sourceForAddr(long addr) {
         NativeLibraryMapInfo library = this.getLibraryFor(addr);
         if(library != null) {
            Addr2Line process = Addr2Line.getProcess(library, this.mClientData.getAbi());
            if(process != null) {
               NativeStackCallInfo info = process.getAddress(addr);
               if(info != null) {
                  return info;
               }
            }
         }

         return new NativeStackCallInfo(addr, library != null?library.getLibraryName():null, Long.toHexString(addr), "");
      }

      private NativeLibraryMapInfo getLibraryFor(long addr) {
         Iterator i$ = this.mClientData.getMappedNativeLibraries().iterator();

         NativeLibraryMapInfo info;
         do {
            if(!i$.hasNext()) {
               Log.d("ddm-nativeheap", "Failed finding Library for " + Long.toHexString(addr));
               return null;
            }

            info = (NativeLibraryMapInfo)i$.next();
         } while(!info.isWithinLibrary(addr));

         return info;
      }

      private boolean updateNHAllocationStackCalls(final ClientData clientData, final int count) {
         if(!NativeHeapPanel.this.mDisplay.isDisposed()) {
            NativeHeapPanel.this.mDisplay.asyncExec(new Runnable() {
               public void run() {
                  NativeHeapPanel.this.updateAllocationStackCalls(clientData, count);
               }
            });
            return true;
         } else {
            return false;
         }
      }
   }
}
