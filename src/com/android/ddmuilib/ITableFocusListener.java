package com.android.ddmuilib;

import org.eclipse.swt.dnd.Clipboard;

public interface ITableFocusListener {
   void focusGained(ITableFocusListener.IFocusedTableActivator var1);

   void focusLost(ITableFocusListener.IFocusedTableActivator var1);

   public interface IFocusedTableActivator {
      void copy(Clipboard var1);

      void selectAll();
   }
}
