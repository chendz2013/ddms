package com.android.ddmuilib.vmtrace;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class VmTraceOptionsDialog extends Dialog {
   private static final int DEFAULT_SAMPLING_INTERVAL_US = 1000;
   private static boolean sTracingEnabled = false;
   private static int sSamplingIntervalUs = 1000;

   public VmTraceOptionsDialog(Shell parentShell) {
      super(parentShell);
   }

   protected void configureShell(Shell newShell) {
      super.configureShell(newShell);
      newShell.setText("Profiling设置");
   }

   protected Control createDialogArea(Composite shell) {
      byte horizontalIndent = 30;
      Composite parent = (Composite)super.createDialogArea(shell);
      Composite c = new Composite(parent, 0);
      c.setLayout(new GridLayout(2, false));
      c.setLayoutData(new GridData(1808));
      Button useSamplingButton = new Button(c, 16);
      useSamplingButton.setText("Sample based profiling");
      useSamplingButton.setSelection(!sTracingEnabled);
      GridData gd = new GridData(32, 4, true, true, 2, 1);
      useSamplingButton.setLayoutData(gd);
      Label l = new Label(c, 0);
      l.setText("Sample based profiling works by interrupting the VM at a given frequency and \ncollecting the call stacks at that time. The overhead is proportional to the \nsampling frequency.");
      gd = new GridData(32, 4, true, true, 2, 1);
      gd.horizontalIndent = horizontalIndent;
      l.setLayoutData(gd);
      l = new Label(c, 0);
      l.setText("Sampling frequency (microseconds): ");
      gd = new GridData(32, 8, false, true);
      gd.horizontalIndent = horizontalIndent;
      l.setLayoutData(gd);
      final Text samplingIntervalTextField = new Text(c, 2048);
      gd = new GridData(32, 4, true, true);
      gd.widthHint = 100;
      samplingIntervalTextField.setLayoutData(gd);
      samplingIntervalTextField.setEnabled(!sTracingEnabled);
      samplingIntervalTextField.setText(Integer.toString(sSamplingIntervalUs));
      samplingIntervalTextField.addModifyListener(new ModifyListener() {
         public void modifyText(ModifyEvent modifyEvent) {
            int v = this.getIntegerValue(samplingIntervalTextField.getText());
            VmTraceOptionsDialog.this.getButton(0).setEnabled(v > 0);
            VmTraceOptionsDialog.sSamplingIntervalUs = v > 0?v:1000;
         }

         private int getIntegerValue(String text) {
            try {
               return Integer.parseInt(text);
            } catch (NumberFormatException var3) {
               return -1;
            }
         }
      });
      final Button useTracingButton = new Button(c, 16);
      useTracingButton.setText("Trace based profiling");
      useTracingButton.setSelection(sTracingEnabled);
      gd = new GridData(32, 4, true, true, 2, 1);
      useTracingButton.setLayoutData(gd);
      l = new Label(c, 0);
      l.setText("Trace based profiling works by tracing the entry and exit of every method.\nThis captures the execution of all methods, no matter how small, and hence\nhas a high overhead.");
      gd = new GridData(32, 4, true, true, 2, 1);
      gd.horizontalIndent = horizontalIndent;
      l.setLayoutData(gd);
      SelectionAdapter selectionAdapter = new SelectionAdapter() {
         public void widgetSelected(SelectionEvent event) {
            VmTraceOptionsDialog.sTracingEnabled = useTracingButton.getSelection();
            samplingIntervalTextField.setEnabled(!VmTraceOptionsDialog.sTracingEnabled);
         }
      };
      useTracingButton.addSelectionListener(selectionAdapter);
      useSamplingButton.addSelectionListener(selectionAdapter);
      return c;
   }

   public boolean shouldUseTracing() {
      return sTracingEnabled;
   }

   public int getSamplingIntervalMicros() {
      return sSamplingIntervalUs;
   }
}
