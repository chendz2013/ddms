package com.android.ddmuilib;

import com.android.ddmlib.AllocationInfo;
import com.android.ddmlib.Client;
import com.android.ddmlib.IStackTraceInfo;
import com.android.ddmlib.AllocationInfo.AllocationSorter;
import com.android.ddmlib.AllocationInfo.SortMode;
import com.android.ddmlib.ClientData.AllocationTrackingStatus;
import com.android.ddmuilib.DdmUiPreferences;
import com.android.ddmuilib.ImageLoader;
import com.android.ddmuilib.StackTracePanel;
import com.android.ddmuilib.TableHelper;
import com.android.ddmuilib.TablePanel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Sash;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.Text;

/**
 * 分配跟踪
 */
public class AllocationPanel extends TablePanel {
   private static final String PREFS_ALLOC_COL_NUMBER = "allocPanel.Col00";
   private static final String PREFS_ALLOC_COL_SIZE = "allocPanel.Col0";
   private static final String PREFS_ALLOC_COL_CLASS = "allocPanel.Col1";
   private static final String PREFS_ALLOC_COL_THREAD = "allocPanel.Col2";
   private static final String PREFS_ALLOC_COL_TRACE_CLASS = "allocPanel.Col3";
   private static final String PREFS_ALLOC_COL_TRACE_METHOD = "allocPanel.Col4";
   private static final String PREFS_ALLOC_SASH = "allocPanel.sash";
   private static final String PREFS_STACK_COLUMN = "allocPanel.stack.col0";
   private Composite mAllocationBase;
   private Table mAllocationTable;
   private TableViewer mAllocationViewer;
   private StackTracePanel mStackTracePanel;
   private Table mStackTraceTable;
   private Button mEnableButton;
   private Button mRequestButton;
   private Button mTraceFilterCheck;
   private final AllocationSorter mSorter = new AllocationSorter();
   private TableColumn mSortColumn;
   private Image mSortUpImg;
   private Image mSortDownImg;
   private String mFilterText = null;

   protected Control createControl(Composite parent) {
      final IPreferenceStore store = DdmUiPreferences.getStore();
      Display display = parent.getDisplay();
      this.mSortUpImg = ImageLoader.getDdmUiLibLoader().loadImage("sort_up.png", display);
      this.mSortDownImg = ImageLoader.getDdmUiLibLoader().loadImage("sort_down.png", display);
      this.mAllocationBase = new Composite(parent, 0);
      this.mAllocationBase.setLayout(new FormLayout());
      Composite topParent = new Composite(this.mAllocationBase, 0);
      topParent.setLayout(new GridLayout(6, false));
      this.mEnableButton = new Button(topParent, 8);
      //开启分配
      this.mEnableButton.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            Client current = AllocationPanel.this.getCurrentClient();
            AllocationTrackingStatus status = current.getClientData().getAllocationStatus();
            if(status == AllocationTrackingStatus.ON) {
               current.enableAllocationTracker(false);
            } else {
               current.enableAllocationTracker(true);
            }

            current.requestAllocationStatus();
         }
      });
      this.mRequestButton = new Button(topParent, 8);
      this.mRequestButton.setText("获取分配");
      this.mRequestButton.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            AllocationPanel.this.getCurrentClient().requestAllocationDetails();
         }
      });
      this.setUpButtons(false, AllocationTrackingStatus.OFF);
      Composite spacer = new Composite(topParent, 0);
      spacer.setLayoutData(new GridData(768));
      (new Label(topParent, 0)).setText("过滤:");
      final Text filterText = new Text(topParent, 2048);
      GridData gridData;
      filterText.setLayoutData(gridData = new GridData(768));
      gridData.widthHint = 200;
      filterText.addModifyListener(new ModifyListener() {
         public void modifyText(ModifyEvent arg0) {
            AllocationPanel.this.mFilterText = filterText.getText().trim();
            AllocationPanel.this.mAllocationViewer.refresh();
         }
      });
      this.mTraceFilterCheck = new Button(topParent, 32);
      this.mTraceFilterCheck.setText("增量跟踪");
      //开启增量跟踪
      this.mTraceFilterCheck.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent arg0) {
            AllocationPanel.this.mAllocationViewer.refresh();
         }
      });
      this.mAllocationTable = new Table(topParent, 65538);
      this.mAllocationTable.setLayoutData(gridData = new GridData(1808));
      gridData.horizontalSpan = 6;
      this.mAllocationTable.setHeaderVisible(true);
      this.mAllocationTable.setLinesVisible(true);
      final TableColumn numberCol = TableHelper.createTableColumn(this.mAllocationTable, "分配顺序", 131072, "Alloc Order", "allocPanel.Col00", store);
      numberCol.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent arg0) {
            AllocationPanel.this.setSortColumn(numberCol, SortMode.NUMBER);
         }
      });
      final TableColumn sizeCol = TableHelper.createTableColumn(this.mAllocationTable, "分配大小", 131072, "888", "allocPanel.Col0", store);
      sizeCol.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent arg0) {
            AllocationPanel.this.setSortColumn(sizeCol, SortMode.SIZE);
         }
      });
      final TableColumn classCol = TableHelper.createTableColumn(this.mAllocationTable, "已分配的类", 16384, "Allocated Class", "allocPanel.Col1", store);
      classCol.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent arg0) {
            AllocationPanel.this.setSortColumn(classCol, SortMode.CLASS);
         }
      });
      final TableColumn threadCol = TableHelper.createTableColumn(this.mAllocationTable, "线程Id", 16384, "999", "allocPanel.Col2", store);
      threadCol.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent arg0) {
            AllocationPanel.this.setSortColumn(threadCol, SortMode.THREAD);
         }
      });
      final TableColumn inClassCol = TableHelper.createTableColumn(this.mAllocationTable, "分配在(类)", 16384, "utime", "allocPanel.Col3", store);
      inClassCol.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent arg0) {
            AllocationPanel.this.setSortColumn(inClassCol, SortMode.IN_CLASS);
         }
      });
      final TableColumn inMethodCol = TableHelper.createTableColumn(this.mAllocationTable, "分配在(方法)", 16384, "utime", "allocPanel.Col4", store);
      inMethodCol.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent arg0) {
            AllocationPanel.this.setSortColumn(inMethodCol, SortMode.IN_METHOD);
         }
      });
      switch(AllocationPanel.SyntheticClass_1.$SwitchMap$com$android$ddmlib$AllocationInfo$SortMode[this.mSorter.getSortMode().ordinal()]) {
      case 1:
         this.mSortColumn = sizeCol;
         break;
      case 2:
         this.mSortColumn = classCol;
         break;
      case 3:
         this.mSortColumn = threadCol;
         break;
      case 4:
         this.mSortColumn = inClassCol;
         break;
      case 5:
         this.mSortColumn = inMethodCol;
      }

      this.mSortColumn.setImage(this.mSorter.isDescending()?this.mSortDownImg:this.mSortUpImg);
      this.mAllocationViewer = new TableViewer(this.mAllocationTable);
      this.mAllocationViewer.setContentProvider(new AllocationPanel.AllocationContentProvider(null));
      this.mAllocationViewer.setLabelProvider(new AllocationPanel.AllocationLabelProvider(null));
      //更新堆栈
      this.mAllocationViewer.addSelectionChangedListener(new ISelectionChangedListener() {
         public void selectionChanged(SelectionChangedEvent event) {
            AllocationInfo selectedAlloc = AllocationPanel.this.getAllocationSelection(event.getSelection());
            AllocationPanel.this.updateAllocationStackTrace(selectedAlloc);
         }
      });
      final Sash sash = new Sash(this.mAllocationBase, 256);
      Color darkGray = parent.getDisplay().getSystemColor(16);
      sash.setBackground(darkGray);
      //创建StackTrace堆栈Panel
      this.mStackTracePanel = new StackTracePanel();
      this.mStackTraceTable = this.mStackTracePanel.createPanel(this.mAllocationBase, "allocPanel.stack.col0", store);
      FormData data = new FormData();
      data.top = new FormAttachment(0, 0);
      data.bottom = new FormAttachment(sash, 0);
      data.left = new FormAttachment(0, 0);
      data.right = new FormAttachment(100, 0);
      topParent.setLayoutData(data);
      final FormData sashData = new FormData();
      if(store != null && store.contains("allocPanel.sash")) {
         sashData.top = new FormAttachment(0, store.getInt("allocPanel.sash"));
      } else {
         sashData.top = new FormAttachment(50, 0);
      }

      sashData.left = new FormAttachment(0, 0);
      sashData.right = new FormAttachment(100, 0);
      sash.setLayoutData(sashData);
      data = new FormData();
      data.top = new FormAttachment(sash, 0);
      data.bottom = new FormAttachment(100, 0);
      data.left = new FormAttachment(0, 0);
      data.right = new FormAttachment(100, 0);
      this.mStackTraceTable.setLayoutData(data);
      sash.addListener(13, new Listener() {
         public void handleEvent(Event e) {
            Rectangle sashRect = sash.getBounds();
            Rectangle panelRect = AllocationPanel.this.mAllocationBase.getClientArea();
            int bottom = panelRect.height - sashRect.height - 100;
            e.y = Math.max(Math.min(e.y, bottom), 100);
            if(e.y != sashRect.y) {
               sashData.top = new FormAttachment(0, e.y);
               store.setValue("allocPanel.sash", e.y);
               AllocationPanel.this.mAllocationBase.layout();
            }

         }
      });
      return this.mAllocationBase;
   }

   public void dispose() {
      this.mSortUpImg.dispose();
      this.mSortDownImg.dispose();
      super.dispose();
   }

   public void setFocus() {
      this.mAllocationTable.setFocus();
   }

   public void clientChanged(final Client client, int changeMask) {
      if(client == this.getCurrentClient()) {
         if((changeMask & 512) != 0) {
            try {
               this.mAllocationTable.getDisplay().asyncExec(new Runnable() {
                  public void run() {
                     AllocationPanel.this.mAllocationViewer.refresh();
                     AllocationPanel.this.updateAllocationStackCall();
                  }
               });
            } catch (SWTException var5) {
               ;
            }
         } else if((changeMask & 1024) != 0) {
            try {
               this.mAllocationTable.getDisplay().asyncExec(new Runnable() {
                  public void run() {
                     AllocationPanel.this.setUpButtons(true, client.getClientData().getAllocationStatus());
                  }
               });
            } catch (SWTException var4) {
               ;
            }
         }
      }

   }

   public void deviceSelected() {
   }

   public void clientSelected() {
      if(!this.mAllocationTable.isDisposed()) {
         Client client = this.getCurrentClient();
         this.mStackTracePanel.setCurrentClient(client);
         this.mStackTracePanel.setViewerInput((IStackTraceInfo)null);
         if(client != null) {
            this.setUpButtons(true, client.getClientData().getAllocationStatus());
         } else {
            this.setUpButtons(false, AllocationTrackingStatus.OFF);
         }

         this.mAllocationViewer.setInput(client);
      }
   }

   private void updateAllocationStackCall() {
      Client client = this.getCurrentClient();
      if(client != null) {
         AllocationInfo selectedAlloc = this.getAllocationSelection((ISelection)null);
         if(selectedAlloc != null) {
            this.updateAllocationStackTrace(selectedAlloc);
         } else {
            this.updateAllocationStackTrace((AllocationInfo)null);
         }
      }

   }

   /**
    * 更新堆栈
    * @param alloc
     */
   private void updateAllocationStackTrace(AllocationInfo alloc) {
      this.mStackTracePanel.setViewerInput(alloc);
   }

   protected void setTableFocusListener() {
      this.addTableToFocusListener(this.mAllocationTable);
      this.addTableToFocusListener(this.mStackTraceTable);
   }

   private AllocationInfo getAllocationSelection(ISelection selection) {
      if(selection == null) {
         selection = this.mAllocationViewer.getSelection();
      }

      if(selection instanceof IStructuredSelection) {
         IStructuredSelection structuredSelection = (IStructuredSelection)selection;
         Object object = structuredSelection.getFirstElement();
         if(object instanceof AllocationInfo) {
            return (AllocationInfo)object;
         }
      }

      return null;
   }

   private void setUpButtons(boolean enabled, AllocationTrackingStatus trackingStatus) {
      if(enabled) {
         switch(AllocationPanel.SyntheticClass_1.$SwitchMap$com$android$ddmlib$ClientData$AllocationTrackingStatus[trackingStatus.ordinal()]) {
         case 1:
            this.mEnableButton.setText("?");
            this.mEnableButton.setEnabled(false);
            this.mRequestButton.setEnabled(false);
            break;
         case 2:
            this.mEnableButton.setText("开始跟踪");
            this.mEnableButton.setEnabled(true);
            this.mRequestButton.setEnabled(false);
            break;
         case 3:
            this.mEnableButton.setText("停止跟踪");
            this.mEnableButton.setEnabled(true);
            this.mRequestButton.setEnabled(true);
         }
      } else {
         this.mEnableButton.setEnabled(false);
         this.mRequestButton.setEnabled(false);
         this.mEnableButton.setText("开始跟踪");
      }

   }

   private void setSortColumn(TableColumn column, SortMode sortMode) {
      this.mSorter.setSortMode(sortMode);
      this.mAllocationTable.setRedraw(false);
      if(this.mSortColumn != column) {
         this.mSortColumn.setImage((Image)null);
      }

      this.mSortColumn = column;
      if(this.mSorter.isDescending()) {
         this.mSortColumn.setImage(this.mSortDownImg);
      } else {
         this.mSortColumn.setImage(this.mSortUpImg);
      }

      this.mAllocationTable.setRedraw(true);
      this.mAllocationViewer.refresh();
   }

   private AllocationInfo[] getFilteredAllocations(AllocationInfo[] allocations, String filterText) {
      ArrayList results = new ArrayList();
      Locale locale = Locale.getDefault();
      filterText = filterText.toLowerCase(locale);
      boolean fullTrace = this.mTraceFilterCheck.getSelection();
      AllocationInfo[] arr$ = allocations;
      int len$ = allocations.length;

      for(int i$ = 0; i$ < len$; ++i$) {
         AllocationInfo info = arr$[i$];
         if(info.filter(filterText, fullTrace, locale)) {
            results.add(info);
         }
      }

      return (AllocationInfo[])results.toArray(new AllocationInfo[results.size()]);
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] $SwitchMap$com$android$ddmlib$AllocationInfo$SortMode;
      // $FF: synthetic field
      static final int[] $SwitchMap$com$android$ddmlib$ClientData$AllocationTrackingStatus = new int[AllocationTrackingStatus.values().length];

      static {
         try {
            $SwitchMap$com$android$ddmlib$ClientData$AllocationTrackingStatus[AllocationTrackingStatus.UNKNOWN.ordinal()] = 1;
         } catch (NoSuchFieldError var8) {
            ;
         }

         try {
            $SwitchMap$com$android$ddmlib$ClientData$AllocationTrackingStatus[AllocationTrackingStatus.OFF.ordinal()] = 2;
         } catch (NoSuchFieldError var7) {
            ;
         }

         try {
            $SwitchMap$com$android$ddmlib$ClientData$AllocationTrackingStatus[AllocationTrackingStatus.ON.ordinal()] = 3;
         } catch (NoSuchFieldError var6) {
            ;
         }

         $SwitchMap$com$android$ddmlib$AllocationInfo$SortMode = new int[SortMode.values().length];

         try {
            $SwitchMap$com$android$ddmlib$AllocationInfo$SortMode[SortMode.SIZE.ordinal()] = 1;
         } catch (NoSuchFieldError var5) {
            ;
         }

         try {
            $SwitchMap$com$android$ddmlib$AllocationInfo$SortMode[SortMode.CLASS.ordinal()] = 2;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            $SwitchMap$com$android$ddmlib$AllocationInfo$SortMode[SortMode.THREAD.ordinal()] = 3;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            $SwitchMap$com$android$ddmlib$AllocationInfo$SortMode[SortMode.IN_CLASS.ordinal()] = 4;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            $SwitchMap$com$android$ddmlib$AllocationInfo$SortMode[SortMode.IN_METHOD.ordinal()] = 5;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }

   private static class AllocationLabelProvider implements ITableLabelProvider {
      private AllocationLabelProvider() {
      }

      public Image getColumnImage(Object element, int columnIndex) {
         return null;
      }

      public String getColumnText(Object element, int columnIndex) {
         if(element instanceof AllocationInfo) {
            AllocationInfo alloc = (AllocationInfo)element;
            switch(columnIndex) {
            case 0:
               return Integer.toString(alloc.getAllocNumber());
            case 1:
               return Integer.toString(alloc.getSize());
            case 2:
               return alloc.getAllocatedClass();
            case 3:
               return Short.toString(alloc.getThreadId());
            case 4:
               return alloc.getFirstTraceClassName();
            case 5:
               return alloc.getFirstTraceMethodName();
            }
         }

         return null;
      }

      public void addListener(ILabelProviderListener listener) {
      }

      public void dispose() {
      }

      public boolean isLabelProperty(Object element, String property) {
         return false;
      }

      public void removeListener(ILabelProviderListener listener) {
      }

      // $FF: synthetic method
      AllocationLabelProvider(Object x0) {
         this();
      }
   }

   private class AllocationContentProvider implements IStructuredContentProvider {
      private AllocationContentProvider() {
      }

      public Object[] getElements(Object inputElement) {
         if(inputElement instanceof Client) {
            AllocationInfo[] allocs = ((Client)inputElement).getClientData().getAllocations();
            if(allocs != null) {
               if(AllocationPanel.this.mFilterText != null && AllocationPanel.this.mFilterText.length() > 0) {
                  allocs = AllocationPanel.this.getFilteredAllocations(allocs, AllocationPanel.this.mFilterText);
               }

               Arrays.sort(allocs, AllocationPanel.this.mSorter);
               return allocs;
            }
         }

         return new Object[0];
      }

      public void dispose() {
      }

      public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
      }

      // $FF: synthetic method
      AllocationContentProvider(Object x1) {
         this();
      }
   }
}
