package com.android.ddmuilib;

import com.android.ddmlib.Client;
import com.android.ddmlib.IStackTraceInfo;
import com.android.ddmuilib.TableHelper;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;

/**
 * 显示线程的StackTrace堆栈跟踪
 */
public final class StackTracePanel {
   private static StackTracePanel.ISourceRevealer sSourceRevealer;
   private Table mStackTraceTable;
   private TableViewer mStackTraceViewer;
   private Client mCurrentClient;

   public static void setSourceRevealer(StackTracePanel.ISourceRevealer revealer) {
      sSourceRevealer = revealer;
   }

   public Table createPanel(Composite parent, String prefs_stack_column, IPreferenceStore store) {
      this.mStackTraceTable = new Table(parent, 65538);
      this.mStackTraceTable.setHeaderVisible(false);
      this.mStackTraceTable.setLinesVisible(false);
      TableHelper.createTableColumn(this.mStackTraceTable, "信息", 16384, "SomeLongClassName.method(android/somepackage/someotherpackage/somefile.java:99999)", prefs_stack_column, store);
      this.mStackTraceViewer = new TableViewer(this.mStackTraceTable);
      this.mStackTraceViewer.setContentProvider(new StackTracePanel.StackTraceContentProvider(null));
      this.mStackTraceViewer.setLabelProvider(new StackTracePanel.StackTraceLabelProvider(null));
      this.mStackTraceViewer.addDoubleClickListener(new IDoubleClickListener() {
         public void doubleClick(DoubleClickEvent event) {
            if(StackTracePanel.sSourceRevealer != null && StackTracePanel.this.mCurrentClient != null) {
               ISelection selection = StackTracePanel.this.mStackTraceViewer.getSelection();
               if(selection instanceof IStructuredSelection) {
                  IStructuredSelection structuredSelection = (IStructuredSelection)selection;
                  Object object = structuredSelection.getFirstElement();
                  if(object instanceof StackTraceElement) {
                     StackTraceElement traceElement = (StackTraceElement)object;
                     if(!traceElement.isNativeMethod()) {
                        StackTracePanel.sSourceRevealer.reveal(StackTracePanel.this.mCurrentClient.getClientData().getClientDescription(), traceElement.getClassName(), traceElement.getLineNumber());
                     }
                  }
               }
            }

         }
      });
      return this.mStackTraceTable;
   }

   public void setViewerInput(IStackTraceInfo input) {
      this.mStackTraceViewer.setInput(input);
      this.mStackTraceViewer.refresh();
   }

   public void setCurrentClient(Client currentClient) {
      this.mCurrentClient = currentClient;
   }

   public interface ISourceRevealer {
      void reveal(String var1, String var2, int var3);
   }

   private static class StackTraceLabelProvider implements ITableLabelProvider {
      private StackTraceLabelProvider() {
      }

      public Image getColumnImage(Object element, int columnIndex) {
         return null;
      }

      public String getColumnText(Object element, int columnIndex) {
         if(element instanceof StackTraceElement && columnIndex == 0) {
            StackTraceElement traceElement = (StackTraceElement)element;
            return "  在 " + traceElement.toString();
         } else {
            return null;
         }
      }

      public void addListener(ILabelProviderListener listener) {
      }

      public void dispose() {
      }

      public boolean isLabelProperty(Object element, String property) {
         return false;
      }

      public void removeListener(ILabelProviderListener listener) {
      }

      // $FF: synthetic method
      StackTraceLabelProvider(Object x0) {
         this();
      }
   }

   private static class StackTraceContentProvider implements IStructuredContentProvider {
      private StackTraceContentProvider() {
      }

      public Object[] getElements(Object inputElement) {
         if(inputElement instanceof IStackTraceInfo) {
            StackTraceElement[] trace = ((IStackTraceInfo)inputElement).getStackTrace();
            if(trace != null) {
               return trace;
            }
         }

         return new Object[0];
      }

      public void dispose() {
      }

      public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
      }

      // $FF: synthetic method
      StackTraceContentProvider(Object x0) {
         this();
      }
   }
}
