package com.android.ddmuilib.screenrecord;

import com.android.ddmlib.CollectingOutputReceiver;
import com.android.ddmlib.IDevice;
import com.android.ddmlib.ScreenRecorderOptions;
import com.android.ddmlib.ScreenRecorderOptions.Builder;
import com.android.ddmuilib.screenrecord.ScreenRecorderOptionsDialog;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.swt.widgets.Shell;

public class ScreenRecorderAction {
   private static final String TITLE = "Screen Recorder";
   private static final String REMOTE_PATH = "/sdcard/ddmsrec.mp4";
   private final Shell mParentShell;
   private final IDevice mDevice;

   public ScreenRecorderAction(Shell parent, IDevice device) {
      this.mParentShell = parent;
      this.mDevice = device;
   }

   public void performAction() {
      ScreenRecorderOptionsDialog optionsDialog = new ScreenRecorderOptionsDialog(this.mParentShell);
      if(optionsDialog.open() != 1) {
         final ScreenRecorderOptions options = (new Builder()).setBitRate(optionsDialog.getBitRate()).setSize(optionsDialog.getWidth(), optionsDialog.getHeight()).build();
         final CountDownLatch latch = new CountDownLatch(1);
         final CollectingOutputReceiver receiver = new CollectingOutputReceiver(latch);
         (new Thread(new Runnable() {
            public void run() {
               try {
                  ScreenRecorderAction.this.mDevice.startScreenRecorder("/sdcard/ddmsrec.mp4", options, receiver);
               } catch (Exception var2) {
                  ScreenRecorderAction.this.showError("Unexpected error while launching screenrecorder", var2);
                  latch.countDown();
               }

            }
         }, "Screen Recorder")).start();

         try {
            (new ProgressMonitorDialog(this.mParentShell)).run(true, true, new IRunnableWithProgress() {
               public void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException {
                  int timeInSecond = 0;
                  monitor.beginTask("Recording...", -1);

                  while(!latch.await(1L, TimeUnit.SECONDS)) {
                     monitor.subTask(String.format("Recording...%d seconds elapsed", new Object[]{Integer.valueOf(timeInSecond++)}));
                     if(monitor.isCanceled()) {
                        receiver.cancel();
                        monitor.subTask("Stopping...");
                        latch.await(1L, TimeUnit.SECONDS);
                        break;
                     }
                  }

               }
            });
         } catch (InvocationTargetException var7) {
            this.showError("Unexpected error while recording: ", var7.getTargetException());
            return;
         } catch (InterruptedException var8) {
            ;
         }

         try {
            this.mDevice.pullFile("/sdcard/ddmsrec.mp4", optionsDialog.getDestination().getAbsolutePath());
         } catch (Exception var6) {
            this.showError("Unexpected error while copying video recording from device", var6);
         }

         MessageDialog.openInformation(this.mParentShell, "Screen Recorder", "Screen recording saved at " + optionsDialog.getDestination().getAbsolutePath());
      }
   }

   private void showError(final String message, final Throwable e) {
      this.mParentShell.getDisplay().asyncExec(new Runnable() {
         public void run() {
            String msg = message;
            if(e != null) {
               msg = msg + (e.getLocalizedMessage() != null?": " + e.getLocalizedMessage():"");
            }

            MessageDialog.openError(ScreenRecorderAction.this.mParentShell, "Screen Recorder", msg);
         }
      });
   }
}
