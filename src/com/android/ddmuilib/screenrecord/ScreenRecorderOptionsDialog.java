package com.android.ddmuilib.screenrecord;

import java.io.File;
import java.util.Calendar;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class ScreenRecorderOptionsDialog extends TitleAreaDialog {
   private static final int DEFAULT_BITRATE_MBPS = 4;
   private static String sLastSavedFolder = System.getProperty("user.home");
   private static String sLastFileName = suggestFileName();
   private static int sBitRateMbps = 4;
   private static int sWidth = 0;
   private static int sHeight = 0;
   private Text mBitRateText;
   private Text mWidthText;
   private Text mHeightText;
   private Text mDestinationText;

   public ScreenRecorderOptionsDialog(Shell parentShell) {
      super(parentShell);
      this.setShellStyle(this.getShellStyle() | 16);
   }

   protected Control createDialogArea(Composite shell) {
      this.setTitle("Screen Recorder Options");
      this.setMessage("Provide screen recorder options. Leave empty to use defaults.");
      Composite parent = (Composite)super.createDialogArea(shell);
      Composite c = new Composite(parent, 2048);
      c.setLayout(new GridLayout(3, false));
      c.setLayoutData(new GridData(1808));
      this.createLabel(c, "Bit Rate (in Mbps)");
      this.mBitRateText = new Text(c, 2048);
      this.mBitRateText.setText(Integer.toString(sBitRateMbps));
      this.mBitRateText.setLayoutData(new GridData(768));
      this.createLabel(c, "");
      this.createLabel(c, "Video width (in px, defaults to screen width)");
      this.mWidthText = new Text(c, 2048);
      this.mWidthText.setLayoutData(new GridData(768));
      if(sWidth > 0) {
         this.mWidthText.setText(Integer.toString(sWidth));
      }

      this.createLabel(c, "");
      this.createLabel(c, "Video height (in px, defaults to screen height)");
      this.mHeightText = new Text(c, 2048);
      this.mHeightText.setLayoutData(new GridData(768));
      if(sHeight > 0) {
         this.mHeightText.setText(Integer.toString(sHeight));
      }

      this.createLabel(c, "");
      ModifyListener m = new ModifyListener() {
         public void modifyText(ModifyEvent modifyEvent) {
            ScreenRecorderOptionsDialog.this.validateAndUpdateState();
         }
      };
      this.mBitRateText.addModifyListener(m);
      this.mWidthText.addModifyListener(m);
      this.mHeightText.addModifyListener(m);
      this.createLabel(c, "Save Video as: ");
      this.mDestinationText = new Text(c, 2048);
      this.mDestinationText.setLayoutData(new GridData(768));
      this.mDestinationText.setText(getFilePath());
      Button browseButton = new Button(c, 8);
      browseButton.setText("Browse");
      browseButton.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent selectionEvent) {
            FileDialog dlg = new FileDialog(ScreenRecorderOptionsDialog.this.getShell(), 8192);
            dlg.setText("Save Video...");
            dlg.setFileName(ScreenRecorderOptionsDialog.sLastFileName != null?ScreenRecorderOptionsDialog.sLastFileName:ScreenRecorderOptionsDialog.suggestFileName());
            if(ScreenRecorderOptionsDialog.sLastSavedFolder != null) {
               dlg.setFilterPath(ScreenRecorderOptionsDialog.sLastSavedFolder);
            }

            dlg.setFilterNames(new String[]{"MP4 files (*.mp4)"});
            dlg.setFilterExtensions(new String[]{"*.mp4"});
            String filePath = dlg.open();
            if(filePath != null) {
               if(!filePath.endsWith(".mp4")) {
                  filePath = filePath + ".mp4";
               }

               ScreenRecorderOptionsDialog.this.mDestinationText.setText(filePath);
               ScreenRecorderOptionsDialog.this.validateAndUpdateState();
            }

         }
      });
      return c;
   }

   private static String getFilePath() {
      return sLastSavedFolder + File.separatorChar + sLastFileName;
   }

   private static String suggestFileName() {
      Calendar now = Calendar.getInstance();
      return String.format("device-%tF-%tH%tM%tS.mp4", new Object[]{now, now, now, now});
   }

   private void createLabel(Composite c, String text) {
      Label l = new Label(c, 0);
      l.setText(text);
      GridData gd = new GridData();
      gd.horizontalAlignment = 131072;
      l.setLayoutData(gd);
   }

   private void validateAndUpdateState() {
      int intValue;
      if((intValue = this.validateInteger(this.mBitRateText.getText().trim(), "Bit Rate has to be an integer")) >= 0) {
         sBitRateMbps = intValue > 0?intValue:4;
         if((intValue = this.validateInteger(this.mWidthText.getText().trim(), "Recorded video resolution width has to be a valid integer.")) >= 0) {
            if(intValue % 16 != 0) {
               this.setErrorMessage("Width must be a multiple of 16");
               this.setOkButtonEnabled(false);
            } else {
               sWidth = intValue;
               if((intValue = this.validateInteger(this.mHeightText.getText().trim(), "Recorded video resolution height has to be a valid integer.")) >= 0) {
                  if(intValue % 16 != 0) {
                     this.setErrorMessage("Height must be a multiple of 16");
                     this.setOkButtonEnabled(false);
                  } else {
                     sHeight = intValue;
                     String filePath = this.mDestinationText.getText();
                     File f = new File(filePath);
                     if(!f.getParentFile().isDirectory()) {
                        this.setErrorMessage("The path \'" + f.getParentFile().getAbsolutePath() + "\' is not a valid directory.");
                        this.setOkButtonEnabled(false);
                     } else {
                        sLastFileName = f.getName();
                        sLastSavedFolder = f.getParentFile().getAbsolutePath();
                        this.setErrorMessage((String)null);
                        this.setOkButtonEnabled(true);
                     }
                  }
               }
            }
         }
      }
   }

   private int validateInteger(String s, String errorMessage) {
      if(!s.isEmpty()) {
         try {
            return Integer.parseInt(s);
         } catch (NumberFormatException var4) {
            this.setErrorMessage(errorMessage);
            this.setOkButtonEnabled(false);
            return -1;
         }
      } else {
         return 0;
      }
   }

   private void setOkButtonEnabled(boolean en) {
      this.getButton(0).setEnabled(en);
   }

   public int getBitRate() {
      return sBitRateMbps;
   }

   public int getWidth() {
      return sWidth;
   }

   public int getHeight() {
      return sHeight;
   }

   public File getDestination() {
      return new File(sLastSavedFolder, sLastFileName);
   }
}
