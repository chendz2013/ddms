package com.android.ddmuilib.logcat;

import com.android.ddmlib.Log.LogLevel;
import com.android.ddmlib.logcat.LogCatFilter;
import com.android.ddmuilib.logcat.LogCatFilterData;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public final class LogCatFilterSettingsSerializer {
   private static final char SINGLE_QUOTE = '\'';
   private static final char ESCAPE_CHAR = '\\';
   private static final String ATTR_DELIM = ", ";
   private static final String KW_DELIM = ": ";
   private static final String KW_NAME = "name";
   private static final String KW_TAG = "tag";
   private static final String KW_TEXT = "text";
   private static final String KW_PID = "pid";
   private static final String KW_APP = "app";
   private static final String KW_LOGLEVEL = "level";

   public String encodeToPreferenceString(List<LogCatFilter> filters, Map<LogCatFilter, LogCatFilterData> filterData) {
      StringBuffer sb = new StringBuffer();
      Iterator i$ = filters.iterator();

      while(true) {
         LogCatFilter f;
         LogCatFilterData fd;
         do {
            if(!i$.hasNext()) {
               return sb.toString();
            }

            f = (LogCatFilter)i$.next();
            fd = (LogCatFilterData)filterData.get(f);
         } while(fd != null && fd.isTransient());

         sb.append("name");
         sb.append(": ");
         sb.append(this.quoteString(f.getName()));
         sb.append(", ");
         sb.append("tag");
         sb.append(": ");
         sb.append(this.quoteString(f.getTag()));
         sb.append(", ");
         sb.append("text");
         sb.append(": ");
         sb.append(this.quoteString(f.getText()));
         sb.append(", ");
         sb.append("pid");
         sb.append(": ");
         sb.append(this.quoteString(f.getPid()));
         sb.append(", ");
         sb.append("app");
         sb.append(": ");
         sb.append(this.quoteString(f.getAppName()));
         sb.append(", ");
         sb.append("level");
         sb.append(": ");
         sb.append(this.quoteString(f.getLogLevel().getStringValue()));
         sb.append(", ");
      }
   }

   public List<LogCatFilter> decodeFromPreferenceString(String pref) {
      ArrayList fs = new ArrayList();
      List kv = this.getKeyValues(pref);
      if(kv.size() == 0) {
         return fs;
      } else {
         int index = 0;

         while(index < kv.size()) {
            String name = "";
            String tag = "";
            String pid = "";
            String app = "";
            String text = "";
            LogLevel level = LogLevel.VERBOSE;

            assert ((String)kv.get(index)).equals("name");

            name = (String)kv.get(index + 1);
            index += 2;

            while(index < kv.size() && !((String)kv.get(index)).equals("name")) {
               String key = (String)kv.get(index);
               String value = (String)kv.get(index + 1);
               index += 2;
               if(key.equals("tag")) {
                  tag = value;
               } else if(key.equals("text")) {
                  text = value;
               } else if(key.equals("pid")) {
                  pid = value;
               } else if(key.equals("app")) {
                  app = value;
               } else if(key.equals("level")) {
                  level = LogLevel.getByString(value);
               }
            }

            fs.add(new LogCatFilter(name, tag, text, pid, app, level));
         }

         return fs;
      }
   }

   private List<String> getKeyValues(String pref) {
      ArrayList kv = new ArrayList();
      int index = 0;

      while(index < pref.length()) {
         String kw = this.getKeyword(pref.substring(index));
         if(kw == null) {
            break;
         }

         index += kw.length() + ": ".length();
         String value = this.getNextString(pref.substring(index));
         index += value.length() + ", ".length();
         value = this.unquoteString(value);
         kv.add(kw);
         kv.add(value);
      }

      return kv;
   }

   private String quoteString(String s) {
      return '\'' + s.replace(Character.toString('\''), "\\\'") + '\'';
   }

   private String unquoteString(String s) {
      s = s.substring(1, s.length() - 1);
      return s.replace("\\\'", Character.toString('\''));
   }

   private String getKeyword(String pref) {
      int kwlen = pref.indexOf(": ");
      return kwlen == -1?null:pref.substring(0, kwlen);
   }

   private String getNextString(String s) {
      assert s.charAt(0) == 39;

      StringBuffer sb = new StringBuffer();

      for(int index = 0; index < s.length(); ++index) {
         sb.append(s.charAt(index));
         if(index > 0 && s.charAt(index) == 39 && s.charAt(index - 1) != 92) {
            break;
         }
      }

      return sb.toString();
   }
}
