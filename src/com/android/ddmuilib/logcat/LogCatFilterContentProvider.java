package com.android.ddmuilib.logcat;

import java.util.List;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;

public final class LogCatFilterContentProvider implements IStructuredContentProvider {
   public void dispose() {
   }

   public void inputChanged(Viewer arg0, Object arg1, Object arg2) {
   }

   public Object[] getElements(Object model) {
      return ((List)model).toArray();
   }
}
