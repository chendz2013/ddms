package com.android.ddmuilib.logcat;

import com.android.ddmlib.logcat.LogCatMessage;

public interface ILogCatMessageSelectionListener {
   void messageDoubleClicked(LogCatMessage var1);
}
