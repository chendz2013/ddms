package com.android.ddmuilib.logcat;

import com.android.ddmlib.IDevice;
import com.android.ddmlib.Log.LogLevel;
import com.android.ddmlib.logcat.LogCatListener;
import com.android.ddmlib.logcat.LogCatMessage;
import com.android.ddmlib.logcat.LogCatReceiverTask;
import com.android.ddmuilib.logcat.ILogCatBufferChangeListener;
import com.android.ddmuilib.logcat.LogCatMessageList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.eclipse.jface.preference.IPreferenceStore;

public final class LogCatReceiver implements LogCatListener {
   private static LogCatMessage DEVICE_DISCONNECTED_MESSAGE;
   private LogCatMessageList mLogMessages;
   private IDevice mCurrentDevice;
   private LogCatReceiverTask mLogCatReceiverTask;
   private Set<ILogCatBufferChangeListener> mLogCatMessageListeners;
   private IPreferenceStore mPrefStore;

   public LogCatReceiver(IDevice device, IPreferenceStore prefStore) {
      this.mCurrentDevice = device;
      this.mPrefStore = prefStore;
      this.mLogCatMessageListeners = new HashSet();
      this.mLogMessages = new LogCatMessageList(this.getFifoSize());
      this.startReceiverThread();
   }

   public void stop() {
      if(this.mLogCatReceiverTask != null) {
         this.mLogCatReceiverTask.removeLogCatListener(this);
         this.mLogCatReceiverTask.stop();
         this.mLogCatReceiverTask = null;
         this.log(Collections.singletonList(DEVICE_DISCONNECTED_MESSAGE));
      }

      this.mCurrentDevice = null;
   }

   private int getFifoSize() {
      int n = this.mPrefStore.getInt("logcat.messagelist.max.size");
      return n == 0?5000:n;
   }

   private void startReceiverThread() {
      if(this.mCurrentDevice != null) {
         this.mLogCatReceiverTask = new LogCatReceiverTask(this.mCurrentDevice);
         this.mLogCatReceiverTask.addLogCatListener(this);
         Thread t = new Thread(this.mLogCatReceiverTask);
         t.setName("LogCat output receiver for " + this.mCurrentDevice.getSerialNumber());
         t.start();
      }
   }

   public void log(List<LogCatMessage> newMessages) {
      LogCatMessageList var3 = this.mLogMessages;
      List deletedMessages;
      synchronized(this.mLogMessages) {
         deletedMessages = this.mLogMessages.ensureSpace(newMessages.size());
         this.mLogMessages.appendMessages(newMessages);
      }

      this.sendLogChangedEvent(newMessages, deletedMessages);
   }

   public LogCatMessageList getMessages() {
      return this.mLogMessages;
   }

   public void clearMessages() {
      this.mLogMessages.clear();
   }

   public void addMessageReceivedEventListener(ILogCatBufferChangeListener l) {
      this.mLogCatMessageListeners.add(l);
   }

   public void removeMessageReceivedEventListener(ILogCatBufferChangeListener l) {
      this.mLogCatMessageListeners.remove(l);
   }

   private void sendLogChangedEvent(List<LogCatMessage> addedMessages, List<LogCatMessage> deletedMessages) {
      Iterator i$ = this.mLogCatMessageListeners.iterator();

      while(i$.hasNext()) {
         ILogCatBufferChangeListener l = (ILogCatBufferChangeListener)i$.next();
         l.bufferChanged(addedMessages, deletedMessages);
      }

   }

   public void resizeFifo(int size) {
      this.mLogMessages.resize(size);
   }

   static {
      DEVICE_DISCONNECTED_MESSAGE = new LogCatMessage(LogLevel.ERROR, "", "", "", "", "", "Device disconnected");
   }
}
