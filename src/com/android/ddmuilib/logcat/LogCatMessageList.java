package com.android.ddmuilib.logcat;

import com.android.ddmlib.logcat.LogCatMessage;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public final class LogCatMessageList {
   public static final String MAX_MESSAGES_PREFKEY = "logcat.messagelist.max.size";
   public static final int MAX_MESSAGES_DEFAULT = 5000;
   private int mFifoSize;
   private BlockingQueue<LogCatMessage> mQ;

   public LogCatMessageList(int maxMessages) {
      this.mFifoSize = maxMessages;
      this.mQ = new ArrayBlockingQueue(this.mFifoSize);
   }

   public synchronized void resize(int n) {
      this.mFifoSize = n;
      if(this.mFifoSize > this.mQ.size()) {
         this.mQ = new ArrayBlockingQueue(this.mFifoSize, true, this.mQ);
      } else {
         LogCatMessage[] curMessages = (LogCatMessage[])this.mQ.toArray(new LogCatMessage[this.mQ.size()]);
         this.mQ = new ArrayBlockingQueue(this.mFifoSize);

         for(int i = curMessages.length - this.mFifoSize; i < curMessages.length; ++i) {
            this.mQ.offer(curMessages[i]);
         }
      }

   }

   public synchronized void appendMessages(List<LogCatMessage> messages) {
      this.ensureSpace(messages.size());
      Iterator i$ = messages.iterator();

      while(i$.hasNext()) {
         LogCatMessage m = (LogCatMessage)i$.next();
         this.mQ.offer(m);
      }

   }

   public synchronized List<LogCatMessage> ensureSpace(int messageCount) {
      ArrayList l = new ArrayList(messageCount);

      while(this.mQ.remainingCapacity() < messageCount) {
         l.add(this.mQ.poll());
      }

      return l;
   }

   public synchronized int remainingCapacity() {
      return this.mQ.remainingCapacity();
   }

   public synchronized void clear() {
      this.mQ.clear();
   }

   public synchronized List<LogCatMessage> getAllMessages() {
      return new ArrayList(this.mQ);
   }
}
