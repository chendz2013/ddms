package com.android.ddmuilib.logcat;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class LogCatStackTraceParser {
   private static final String EXCEPTION_LINE_REGEX = "\\s*at\\ (.*)\\((.*)\\..*\\:(\\d+)\\)";
   private static final Pattern EXCEPTION_LINE_PATTERN = Pattern.compile("\\s*at\\ (.*)\\((.*)\\..*\\:(\\d+)\\)");

   public boolean isValidExceptionTrace(String line) {
      return EXCEPTION_LINE_PATTERN.matcher(line).find();
   }

   public String getMethodName(String line) {
      Matcher m = EXCEPTION_LINE_PATTERN.matcher(line);
      m.find();
      return m.group(1);
   }

   public String getFileName(String line) {
      Matcher m = EXCEPTION_LINE_PATTERN.matcher(line);
      m.find();
      return m.group(2);
   }

   public int getLineNumber(String line) {
      Matcher m = EXCEPTION_LINE_PATTERN.matcher(line);
      m.find();

      try {
         return Integer.parseInt(m.group(3));
      } catch (NumberFormatException var4) {
         return 0;
      }
   }
}
