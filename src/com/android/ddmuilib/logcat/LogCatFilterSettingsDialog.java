package com.android.ddmuilib.logcat;

import com.android.ddmlib.Log.LogLevel;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public final class LogCatFilterSettingsDialog extends TitleAreaDialog {
   private static final String TITLE = "Logcat Message Filter Settings";
   private static final String DEFAULT_MESSAGE = "Filter logcat messages by the source\'s tag, pid or minimum log level.\nEmpty fields will match all messages.";
   private String mFilterName;
   private String mTag;
   private String mText;
   private String mPid;
   private String mAppName;
   private String mLogLevel;
   private Text mFilterNameText;
   private Text mTagFilterText;
   private Text mTextFilterText;
   private Text mPidFilterText;
   private Text mAppNameFilterText;
   private Combo mLogLevelCombo;
   private Button mOkButton;

   public LogCatFilterSettingsDialog(Shell parentShell) {
      super(parentShell);
      this.setDefaults("", "", "", "", "", LogLevel.VERBOSE);
   }

   public void setDefaults(String filterName, String tag, String text, String pid, String appName, LogLevel level) {
      this.mFilterName = filterName;
      this.mTag = tag;
      this.mText = text;
      this.mPid = pid;
      this.mAppName = appName;
      this.mLogLevel = level.getStringValue();
   }

   protected Control createDialogArea(Composite shell) {
      this.setTitle("Logcat Message Filter Settings");
      this.setMessage("Filter logcat messages by the source\'s tag, pid or minimum log level.\nEmpty fields will match all messages.");
      Composite parent = (Composite)super.createDialogArea(shell);
      Composite c = new Composite(parent, 2048);
      c.setLayout(new GridLayout(2, false));
      c.setLayoutData(new GridData(1808));
      this.createLabel(c, "Filter Name:");
      this.mFilterNameText = new Text(c, 2048);
      this.mFilterNameText.setLayoutData(new GridData(768));
      this.mFilterNameText.setText(this.mFilterName);
      this.createSeparator(c);
      this.createLabel(c, "by Log Tag:");
      this.mTagFilterText = new Text(c, 2048);
      this.mTagFilterText.setLayoutData(new GridData(768));
      this.mTagFilterText.setText(this.mTag);
      this.createLabel(c, "by Log Message:");
      this.mTextFilterText = new Text(c, 2048);
      this.mTextFilterText.setLayoutData(new GridData(768));
      this.mTextFilterText.setText(this.mText);
      this.createLabel(c, "by PID:");
      this.mPidFilterText = new Text(c, 2048);
      this.mPidFilterText.setLayoutData(new GridData(768));
      this.mPidFilterText.setText(this.mPid);
      this.createLabel(c, "by Application Name:");
      this.mAppNameFilterText = new Text(c, 2048);
      this.mAppNameFilterText.setLayoutData(new GridData(768));
      this.mAppNameFilterText.setText(this.mAppName);
      this.createLabel(c, "by Log Level:");
      this.mLogLevelCombo = new Combo(c, 12);
      this.mLogLevelCombo.setItems((String[])getLogLevels().toArray(new String[0]));
      this.mLogLevelCombo.select(getLogLevels().indexOf(this.mLogLevel));
      ModifyListener m = new ModifyListener() {
         public void modifyText(ModifyEvent arg0) {
            LogCatFilterSettingsDialog.DialogStatus status = LogCatFilterSettingsDialog.this.validateDialog();
            LogCatFilterSettingsDialog.this.mOkButton.setEnabled(status.valid);
            LogCatFilterSettingsDialog.this.setErrorMessage(status.message);
         }
      };
      this.mFilterNameText.addModifyListener(m);
      this.mTagFilterText.addModifyListener(m);
      this.mTextFilterText.addModifyListener(m);
      this.mPidFilterText.addModifyListener(m);
      this.mAppNameFilterText.addModifyListener(m);
      return c;
   }

   protected void createButtonsForButtonBar(Composite parent) {
      super.createButtonsForButtonBar(parent);
      this.mOkButton = this.getButton(0);
      LogCatFilterSettingsDialog.DialogStatus status = this.validateDialog();
      this.mOkButton.setEnabled(status.valid);
   }

   private LogCatFilterSettingsDialog.DialogStatus validateDialog() {
      if(this.mFilterNameText.getText().trim().equals("")) {
         return new LogCatFilterSettingsDialog.DialogStatus(false, "Please provide a name for this filter.", null);
      } else {
         String pidText = this.mPidFilterText.getText().trim();
         if(pidText.trim().length() > 0) {
            boolean tagText = false;

            int tagText1;
            try {
               tagText1 = Integer.parseInt(pidText);
            } catch (NumberFormatException var9) {
               return new LogCatFilterSettingsDialog.DialogStatus(false, "PID should be a positive integer.", null);
            }

            if(tagText1 < 0) {
               return new LogCatFilterSettingsDialog.DialogStatus(false, "PID should be a positive integer.", null);
            }
         }

         String tagText2 = this.mTagFilterText.getText().trim();
         if(tagText2.trim().length() > 0) {
            try {
               Pattern.compile(tagText2);
            } catch (PatternSyntaxException var8) {
               return new LogCatFilterSettingsDialog.DialogStatus(false, "Invalid regex used in tag field: " + var8.getMessage(), null);
            }
         }

         String messageText = this.mTextFilterText.getText().trim();
         if(messageText.trim().length() > 0) {
            try {
               Pattern.compile(messageText);
            } catch (PatternSyntaxException var7) {
               return new LogCatFilterSettingsDialog.DialogStatus(false, "Invalid regex used in text field: " + var7.getMessage(), null);
            }
         }

         String appNameText = this.mAppNameFilterText.getText().trim();
         if(appNameText.trim().length() > 0) {
            try {
               Pattern.compile(appNameText);
            } catch (PatternSyntaxException var6) {
               return new LogCatFilterSettingsDialog.DialogStatus(false, "Invalid regex used in application name field: " + var6.getMessage(), null);
            }
         }

         return new LogCatFilterSettingsDialog.DialogStatus(true, (String)null, null);
      }
   }

   private void createSeparator(Composite c) {
      Label l = new Label(c, 258);
      GridData gd = new GridData(768);
      gd.horizontalSpan = 2;
      l.setLayoutData(gd);
   }

   private void createLabel(Composite c, String text) {
      Label l = new Label(c, 0);
      l.setText(text);
      GridData gd = new GridData();
      gd.horizontalAlignment = 131072;
      l.setLayoutData(gd);
   }

   protected void okPressed() {
      this.mFilterName = this.mFilterNameText.getText();
      this.mTag = this.mTagFilterText.getText();
      this.mText = this.mTextFilterText.getText();
      this.mLogLevel = this.mLogLevelCombo.getText();
      this.mPid = this.mPidFilterText.getText();
      this.mAppName = this.mAppNameFilterText.getText();
      super.okPressed();
   }

   public String getFilterName() {
      return this.mFilterName;
   }

   public String getTag() {
      return this.mTag;
   }

   public String getText() {
      return this.mText;
   }

   public String getPid() {
      return this.mPid;
   }

   public String getAppName() {
      return this.mAppName;
   }

   public String getLogLevel() {
      return this.mLogLevel;
   }

   public static List<String> getLogLevels() {
      ArrayList logLevels = new ArrayList();
      LogLevel[] arr$ = LogLevel.values();
      int len$ = arr$.length;

      for(int i$ = 0; i$ < len$; ++i$) {
         LogLevel l = arr$[i$];
         logLevels.add(l.getStringValue());
      }

      return logLevels;
   }

   private static final class DialogStatus {
      final boolean valid;
      final String message;

      private DialogStatus(boolean isValid, String errMessage) {
         this.valid = isValid;
         this.message = errMessage;
      }

      // $FF: synthetic method
      DialogStatus(boolean x0, String x1, Object x2) {
         this(x0, x1);
      }
   }
}
