package com.android.ddmuilib.logcat;

import com.android.ddmlib.logcat.LogCatFilter;
import com.android.ddmuilib.logcat.LogCatFilterData;
import java.util.Map;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;

public final class LogCatFilterLabelProvider extends LabelProvider implements ITableLabelProvider {
   private Map<LogCatFilter, LogCatFilterData> mFilterData;

   public LogCatFilterLabelProvider(Map<LogCatFilter, LogCatFilterData> filterData) {
      this.mFilterData = filterData;
   }

   public Image getColumnImage(Object arg0, int arg1) {
      return null;
   }

   public String getColumnText(Object element, int index) {
      if(!(element instanceof LogCatFilter)) {
         return null;
      } else {
         LogCatFilter f = (LogCatFilter)element;
         LogCatFilterData fd = (LogCatFilterData)this.mFilterData.get(f);
         return fd != null && fd.getUnreadCount() > 0?String.format("%s (%d)", new Object[]{f.getName(), Integer.valueOf(fd.getUnreadCount())}):f.getName();
      }
   }
}
