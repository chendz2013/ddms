package com.android.ddmuilib.logcat;

import com.android.ddmlib.AndroidDebugBridge;
import com.android.ddmlib.IDevice;
import com.android.ddmlib.AndroidDebugBridge.IDeviceChangeListener;
import com.android.ddmuilib.logcat.LogCatReceiver;
import java.util.HashMap;
import java.util.Map;
import org.eclipse.jface.preference.IPreferenceStore;

public class LogCatReceiverFactory {
   public static final LogCatReceiverFactory INSTANCE = new LogCatReceiverFactory();
   private Map<String, LogCatReceiver> mReceiverCache = new HashMap();

   private LogCatReceiverFactory() {
      AndroidDebugBridge.addDeviceChangeListener(new IDeviceChangeListener() {
         public void deviceDisconnected(final IDevice device) {
            Thread t = new Thread(new Runnable() {
               public void run() {
                  LogCatReceiverFactory.this.removeReceiverFor(device);
               }
            }, "Remove logcat receiver for " + device.getSerialNumber());
            t.start();
         }

         public void deviceConnected(IDevice device) {
         }

         public void deviceChanged(IDevice device, int changeMask) {
         }
      });
   }

   private synchronized void removeReceiverFor(IDevice device) {
      LogCatReceiver r = (LogCatReceiver)this.mReceiverCache.get(device.getSerialNumber());
      if(r != null) {
         r.stop();
         this.mReceiverCache.remove(device.getSerialNumber());
      }

   }

   public synchronized LogCatReceiver newReceiver(IDevice device, IPreferenceStore prefs) {
      LogCatReceiver r = (LogCatReceiver)this.mReceiverCache.get(device.getSerialNumber());
      if(r != null) {
         return r;
      } else {
         r = new LogCatReceiver(device, prefs);
         this.mReceiverCache.put(device.getSerialNumber(), r);
         return r;
      }
   }
}
