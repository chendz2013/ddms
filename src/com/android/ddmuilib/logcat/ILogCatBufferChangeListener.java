package com.android.ddmuilib.logcat;

import com.android.ddmlib.logcat.LogCatMessage;
import java.util.List;

public interface ILogCatBufferChangeListener {
   void bufferChanged(List<LogCatMessage> var1, List<LogCatMessage> var2);
}
