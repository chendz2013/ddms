package com.android.ddmuilib.logcat;

import com.android.ddmlib.IDevice;
import com.android.ddmlib.Log;
import com.android.ddmlib.MultiLineReceiver;
import com.android.ddmlib.Log.LogLevel;
import com.android.ddmuilib.DdmUiPreferences;
import com.android.ddmuilib.ITableFocusListener;
import com.android.ddmuilib.SelectionDependentPanel;
import com.android.ddmuilib.TableHelper;
import com.android.ddmuilib.actions.ICommonAction;
import com.android.ddmuilib.logcat.EditFilterDialog;
import com.android.ddmuilib.logcat.LogColors;
import com.android.ddmuilib.logcat.LogFilter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.ControlListener;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

public class LogPanel extends SelectionDependentPanel {
   private static final int STRING_BUFFER_LENGTH = 10000;
   public static final int FILTER_NONE = 0;
   public static final int FILTER_MANUAL = 1;
   public static final int FILTER_AUTO_PID = 2;
   public static final int FILTER_AUTO_TAG = 3;
   public static final int FILTER_DEBUG = 4;
   public static final int COLUMN_MODE_MANUAL = 0;
   public static final int COLUMN_MODE_AUTO = 1;
   public static String PREFS_TIME;
   public static String PREFS_LEVEL;
   public static String PREFS_PID;
   public static String PREFS_TAG;
   public static String PREFS_MESSAGE;
   private static Pattern sLogPattern = Pattern.compile("^\\[\\s(\\d\\d-\\d\\d\\s\\d\\d:\\d\\d:\\d\\d\\.\\d+)\\s+(\\d*):(0x[0-9a-fA-F]+)\\s([VDIWE])/(.*)\\]$");
   private Composite mParent;
   private IPreferenceStore mStore;
   private TabFolder mFolders;
   private LogColors mColors;
   private LogPanel.ILogFilterStorageManager mFilterStorage;
   private LogPanel.LogCatOuputReceiver mCurrentLogCat;
   private LogPanel.LogMessage[] mBuffer = new LogPanel.LogMessage[10000];
   private int mBufferStart = -1;
   private int mBufferEnd = -1;
   private LogFilter[] mFilters;
   private LogFilter mDefaultFilter;
   private LogFilter mCurrentFilter;
   private int mFilterMode = 0;
   private IDevice mCurrentLoggedDevice = null;
   private ICommonAction mDeleteFilterAction;
   private ICommonAction mEditFilterAction;
   private ICommonAction[] mLogLevelActions;
   private LogPanel.LogMessageInfo mLastMessageInfo = null;
   private boolean mPendingAsyncRefresh = false;
   private String mDefaultLogSave;
   private int mColumnMode = 0;
   private Font mDisplayFont;
   private ITableFocusListener mGlobalListener;
   private LogPanel.LogCatViewInterface mLogCatViewInterface = null;

   public LogPanel(LogColors colors, LogPanel.ILogFilterStorageManager filterStorage, int mode) {
      this.mColors = colors;
      this.mFilterMode = mode;
      this.mFilterStorage = filterStorage;
      this.mStore = DdmUiPreferences.getStore();
   }

   public void setActions(ICommonAction deleteAction, ICommonAction editAction, ICommonAction[] logLevelActions) {
      this.mDeleteFilterAction = deleteAction;
      this.mEditFilterAction = editAction;
      this.mLogLevelActions = logLevelActions;
   }

   public void setColumnMode(int mode) {
      this.mColumnMode = mode;
   }

   public void setFont(Font font) {
      this.mDisplayFont = font;
      if(this.mFilters != null) {
         LogFilter[] table = this.mFilters;
         int len$ = table.length;

         for(int i$ = 0; i$ < len$; ++i$) {
            LogFilter f = table[i$];
            Table table1 = f.getTable();
            if(table1 != null) {
               table1.setFont(font);
            }
         }
      }

      if(this.mDefaultFilter != null) {
         Table var7 = this.mDefaultFilter.getTable();
         if(var7 != null) {
            var7.setFont(font);
         }
      }

   }

   public void deviceSelected() {
      this.startLogCat(this.getCurrentDevice());
   }

   public void clientSelected() {
   }

   protected Control createControl(Composite parent) {
      this.mParent = parent;
      Composite top = new Composite(parent, 0);
      top.setLayoutData(new GridData(1808));
      top.setLayout(new GridLayout(1, false));
      this.mFolders = new TabFolder(top, 0);
      this.mFolders.setLayoutData(new GridData(1808));
      this.mFolders.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            if(LogPanel.this.mCurrentFilter != null) {
               LogPanel.this.mCurrentFilter.setSelectedState(false);
            }

            LogPanel.this.mCurrentFilter = LogPanel.this.getCurrentFilter();
            LogPanel.this.mCurrentFilter.setSelectedState(true);
            LogPanel.this.updateColumns(LogPanel.this.mCurrentFilter.getTable());
            if(LogPanel.this.mCurrentFilter.getTempFilterStatus()) {
               LogPanel.this.initFilter(LogPanel.this.mCurrentFilter);
            }

            LogPanel.this.selectionChanged(LogPanel.this.mCurrentFilter);
         }
      });
      Composite bottom = new Composite(top, 0);
      bottom.setLayoutData(new GridData(768));
      bottom.setLayout(new GridLayout(3, false));
      Label label = new Label(bottom, 0);
      label.setText("Filter:");
      final Text filterText = new Text(bottom, 2052);
      filterText.setLayoutData(new GridData(768));
      filterText.addModifyListener(new ModifyListener() {
         public void modifyText(ModifyEvent e) {
            LogPanel.this.updateFilteringWith(filterText.getText());
         }
      });
      this.createFilters();
      int index = 0;
      if(this.mDefaultFilter != null) {
         this.createTab(this.mDefaultFilter, index++, false);
      }

      if(this.mFilters != null) {
         LogFilter[] arr$ = this.mFilters;
         int len$ = arr$.length;

         for(int i$ = 0; i$ < len$; ++i$) {
            LogFilter f = arr$[i$];
            this.createTab(f, index++, false);
         }
      }

      return top;
   }

   protected void postCreation() {
   }

   public void setFocus() {
      this.mFolders.setFocus();
   }

   public void startLogCat(final IDevice device) {
      if(device != this.mCurrentLoggedDevice) {
         if(this.mCurrentLoggedDevice != null) {
            this.stopLogCat(false);
            this.mCurrentLoggedDevice = null;
         }

         this.resetUI(false);
         if(device != null) {
            this.mCurrentLogCat = new LogPanel.LogCatOuputReceiver();
            (new Thread("Logcat") {
               public void run() {
                  while(!device.isOnline() && LogPanel.this.mCurrentLogCat != null && !LogPanel.this.mCurrentLogCat.isCancelled) {
                     try {
                        sleep(2000L);
                     } catch (InterruptedException var6) {
                        return;
                     }
                  }

                  if(LogPanel.this.mCurrentLogCat != null && !LogPanel.this.mCurrentLogCat.isCancelled) {
                     try {
                        LogPanel.this.mCurrentLoggedDevice = device;
                        device.executeShellCommand("logcat -v long", LogPanel.this.mCurrentLogCat, 0);
                     } catch (Exception var7) {
                        Log.e("Logcat", var7);
                     } finally {
                        LogPanel.this.mCurrentLogCat = null;
                        LogPanel.this.mCurrentLoggedDevice = null;
                     }

                  }
               }
            }).start();
         }

      }
   }

   public void stopLogCat(boolean inUiThread) {
      if(this.mCurrentLogCat != null) {
         this.mCurrentLogCat.isCancelled = true;
         this.mCurrentLogCat = null;

         for(int i = 0; i < 10000; ++i) {
            this.mBuffer[i] = null;
         }

         this.mBufferStart = -1;
         this.mBufferEnd = -1;
         this.resetFilters();
         this.resetUI(inUiThread);
      }

   }

   public void addFilter() {
      EditFilterDialog dlg = new EditFilterDialog(this.mFolders.getShell());
      if(dlg.open()) {
         LogPanel.LogMessage[] var2 = this.mBuffer;
         synchronized(this.mBuffer) {
            LogFilter filter = dlg.getFilter();
            this.addFilterToArray(filter);
            int index = this.mFilters.length - 1;
            if(this.mDefaultFilter != null) {
               ++index;
            }

            this.createTab(filter, index, true);
            if(this.mDefaultFilter != null) {
               this.initDefaultFilter();
            }

            if(this.mCurrentFilter != null) {
               this.mCurrentFilter.setSelectedState(false);
            }

            this.mFolders.setSelection(index);
            filter.setSelectedState(true);
            this.mCurrentFilter = filter;
            this.selectionChanged(filter);
            if(this.mFilterMode == 0) {
               this.mFilterMode = 1;
            }

            this.mFilterStorage.saveFilters(this.mFilters);
         }
      }

   }

   public void editFilter() {
      if(this.mCurrentFilter != null && this.mCurrentFilter != this.mDefaultFilter) {
         EditFilterDialog dlg = new EditFilterDialog(this.mFolders.getShell(), this.mCurrentFilter);
         if(dlg.open()) {
            LogPanel.LogMessage[] var2 = this.mBuffer;
            synchronized(this.mBuffer) {
               this.initFilter(this.mCurrentFilter);
               if(this.mDefaultFilter != null) {
                  this.initDefaultFilter();
               }

               this.mFilterStorage.saveFilters(this.mFilters);
            }
         }
      }

   }

   public void deleteFilter() {
      LogPanel.LogMessage[] var1 = this.mBuffer;
      synchronized(this.mBuffer) {
         if(this.mCurrentFilter != null && this.mCurrentFilter != this.mDefaultFilter) {
            this.removeFilterFromArray(this.mCurrentFilter);
            this.mCurrentFilter.dispose();
            this.mFolders.setSelection(0);
            if(this.mFilters.length > 0) {
               this.mCurrentFilter = this.mFilters[0];
            } else {
               this.mCurrentFilter = this.mDefaultFilter;
            }

            this.selectionChanged(this.mCurrentFilter);
            if(this.mDefaultFilter != null) {
               this.initDefaultFilter();
            }

            this.mFilterStorage.saveFilters(this.mFilters);
         }

      }
   }

   public boolean save() {
      LogPanel.LogMessage[] var1 = this.mBuffer;
      synchronized(this.mBuffer) {
         FileDialog dlg = new FileDialog(this.mParent.getShell(), 8192);
         dlg.setText("Save log...");
         dlg.setFileName("log.txt");
         String defaultPath = this.mDefaultLogSave;
         if(defaultPath == null) {
            defaultPath = System.getProperty("user.home");
         }

         dlg.setFilterPath(defaultPath);
         dlg.setFilterNames(new String[]{"Text Files (*.txt)"});
         dlg.setFilterExtensions(new String[]{"*.txt"});
         String fileName = dlg.open();
         if(fileName != null) {
            this.mDefaultLogSave = dlg.getFilterPath();
            Table currentTable = this.mCurrentFilter.getTable();
            int[] selection = currentTable.getSelectionIndices();
            Arrays.sort(selection);
            FileWriter writer = null;

            boolean len$;
            try {
               writer = new FileWriter(fileName);
               int[] e = selection;
               int var28 = selection.length;

               for(int e1 = 0; e1 < var28; ++e1) {
                  int i = e[e1];
                  TableItem item = currentTable.getItem(i);
                  LogPanel.LogMessage msg = (LogPanel.LogMessage)item.getData();
                  String line = msg.toString();
                  writer.write(line);
                  writer.write(10);
               }

               writer.flush();
               return true;
            } catch (IOException var25) {
               len$ = false;
            } finally {
               if(writer != null) {
                  try {
                     writer.close();
                  } catch (IOException var24) {
                     ;
                  }
               }

            }

            return len$;
         } else {
            return true;
         }
      }
   }

   public void clear() {
      LogPanel.LogMessage[] var1 = this.mBuffer;
      synchronized(this.mBuffer) {
         for(int arr$ = 0; arr$ < 10000; ++arr$) {
            this.mBuffer[arr$] = null;
         }

         this.mBufferStart = -1;
         this.mBufferEnd = -1;
         LogFilter[] var8 = this.mFilters;
         int len$ = var8.length;

         for(int i$ = 0; i$ < len$; ++i$) {
            LogFilter filter = var8[i$];
            filter.clear();
         }

         if(this.mDefaultFilter != null) {
            this.mDefaultFilter.clear();
         }

      }
   }

   public void copy(Clipboard clipboard) {
      Table currentTable = this.mCurrentFilter.getTable();
      copyTable(clipboard, currentTable);
   }

   public void selectAll() {
      Table currentTable = this.mCurrentFilter.getTable();
      currentTable.selectAll();
   }

   public void setTableFocusListener(ITableFocusListener listener) {
      this.mGlobalListener = listener;
      LogFilter[] arr$ = this.mFilters;
      int len$ = arr$.length;

      for(int i$ = 0; i$ < len$; ++i$) {
         LogFilter filter = arr$[i$];
         Table table = filter.getTable();
         this.addTableToFocusListener(table);
      }

      if(this.mDefaultFilter != null) {
         this.addTableToFocusListener(this.mDefaultFilter.getTable());
      }

   }

   private void addTableToFocusListener(final Table table) {
      final ITableFocusListener.IFocusedTableActivator activator = new ITableFocusListener.IFocusedTableActivator() {
         public void copy(Clipboard clipboard) {
            LogPanel.copyTable(clipboard, table);
         }

         public void selectAll() {
            table.selectAll();
         }
      };
      table.addFocusListener(new FocusListener() {
         public void focusGained(FocusEvent e) {
            LogPanel.this.mGlobalListener.focusGained(activator);
         }

         public void focusLost(FocusEvent e) {
            LogPanel.this.mGlobalListener.focusLost(activator);
         }
      });
   }

   private static void copyTable(Clipboard clipboard, Table table) {
      int[] selection = table.getSelectionIndices();
      Arrays.sort(selection);
      StringBuilder sb = new StringBuilder();
      int[] arr$ = selection;
      int len$ = selection.length;

      for(int i$ = 0; i$ < len$; ++i$) {
         int i = arr$[i$];
         TableItem item = table.getItem(i);
         LogPanel.LogMessage msg = (LogPanel.LogMessage)item.getData();
         String line = msg.toString();
         sb.append(line);
         sb.append('\n');
      }

      clipboard.setContents(new Object[]{sb.toString()}, new Transfer[]{TextTransfer.getInstance()});
   }

   public void setCurrentFilterLogLevel(int i) {
      LogFilter filter = this.getCurrentFilter();
      filter.setLogLevel(i);
      this.initFilter(filter);
   }

   private TabItem createTab(LogFilter filter, int index, boolean fillTable) {
      LogPanel.LogMessage[] var4 = this.mBuffer;
      synchronized(this.mBuffer) {
         TabItem item = null;
         if(index != -1) {
            item = new TabItem(this.mFolders, 0, index);
         } else {
            item = new TabItem(this.mFolders, 0);
         }

         item.setText(filter.getName());
         Composite top = new Composite(this.mFolders, 0);
         item.setControl(top);
         top.setLayout(new FillLayout());
         final Table t = new Table(top, 65538);
         t.addSelectionListener(new SelectionAdapter() {
            public void widgetDefaultSelected(SelectionEvent e) {
               if(LogPanel.this.mLogCatViewInterface != null) {
                  LogPanel.this.mLogCatViewInterface.onDoubleClick();
               }

            }
         });
         if(this.mDisplayFont != null) {
            t.setFont(this.mDisplayFont);
         }

         filter.setWidgets(item, t);
         t.setHeaderVisible(true);
         t.setLinesVisible(false);
         if(this.mGlobalListener != null) {
            this.addTableToFocusListener(t);
         }

         ControlListener listener = null;
         if(this.mColumnMode == 1) {
            listener = new ControlListener() {
               public void controlMoved(ControlEvent e) {
               }

               public void controlResized(ControlEvent e) {
                  Rectangle r = t.getClientArea();
                  int total = t.getColumn(0).getWidth();
                  total += t.getColumn(1).getWidth();
                  total += t.getColumn(2).getWidth();
                  total += t.getColumn(3).getWidth();
                  if(r.width > total) {
                     t.getColumn(4).setWidth(r.width - total);
                  }

               }
            };
            t.addControlListener(listener);
         }

         TableColumn col = TableHelper.createTableColumn(t, "Time", 16384, "00-00 00:00:00", PREFS_TIME, this.mStore);
         if(this.mColumnMode == 1) {
            col.addControlListener(listener);
         }

         col = TableHelper.createTableColumn(t, "", 16777216, "D", PREFS_LEVEL, this.mStore);
         if(this.mColumnMode == 1) {
            col.addControlListener(listener);
         }

         col = TableHelper.createTableColumn(t, "pid", 16384, "9999", PREFS_PID, this.mStore);
         if(this.mColumnMode == 1) {
            col.addControlListener(listener);
         }

         col = TableHelper.createTableColumn(t, "tag", 16384, "abcdefgh", PREFS_TAG, this.mStore);
         if(this.mColumnMode == 1) {
            col.addControlListener(listener);
         }

         col = TableHelper.createTableColumn(t, "Message", 16384, "abcdefghijklmnopqrstuvwxyz0123456789", PREFS_MESSAGE, this.mStore);
         if(this.mColumnMode == 1) {
            col.setResizable(false);
         }

         if(fillTable) {
            this.initFilter(filter);
         }

         return item;
      }
   }

   protected void updateColumns(Table table) {
      if(table != null) {
         byte index = 0;
         int var4 = index + 1;
         TableColumn col = table.getColumn(index);
         col.setWidth(this.mStore.getInt(PREFS_TIME));
         col = table.getColumn(var4++);
         col.setWidth(this.mStore.getInt(PREFS_LEVEL));
         col = table.getColumn(var4++);
         col.setWidth(this.mStore.getInt(PREFS_PID));
         col = table.getColumn(var4++);
         col.setWidth(this.mStore.getInt(PREFS_TAG));
         col = table.getColumn(var4++);
         col.setWidth(this.mStore.getInt(PREFS_MESSAGE));
      }

   }

   public void resetUI(boolean inUiThread) {
      Display d;
      if(this.mFilterMode != 2 && this.mFilterMode != 3) {
         if(!this.mFolders.isDisposed()) {
            if(inUiThread) {
               this.emptyTables();
            } else {
               d = this.mFolders.getDisplay();
               d.syncExec(new Runnable() {
                  public void run() {
                     if(!LogPanel.this.mFolders.isDisposed()) {
                        LogPanel.this.emptyTables();
                     }

                  }
               });
            }
         }
      } else if(inUiThread) {
         this.mFolders.dispose();
         this.mParent.pack(true);
         this.createControl(this.mParent);
      } else {
         d = this.mFolders.getDisplay();
         d.syncExec(new Runnable() {
            public void run() {
               LogPanel.this.mFolders.dispose();
               LogPanel.this.mParent.pack(true);
               LogPanel.this.createControl(LogPanel.this.mParent);
            }
         });
      }

   }

   protected void processLogLines(String[] lines) {
      if(lines.length > 10000) {
         Log.e("LogCat", "Receiving more lines than STRING_BUFFER_LENGTH");
      }

      ArrayList newMessages = new ArrayList();
      LogPanel.LogMessage[] var3 = this.mBuffer;
      synchronized(this.mBuffer) {
         String[] e = lines;
         int len$ = lines.length;

         for(int i$ = 0; i$ < len$; ++i$) {
            String line = e[i$];
            if(line.length() > 0) {
               Matcher matcher = sLogPattern.matcher(line);
               if(matcher.matches()) {
                  this.mLastMessageInfo = new LogPanel.LogMessageInfo();
                  this.mLastMessageInfo.time = matcher.group(1);
                  this.mLastMessageInfo.pidString = matcher.group(2);
                  this.mLastMessageInfo.pid = Integer.valueOf(this.mLastMessageInfo.pidString).intValue();
                  this.mLastMessageInfo.logLevel = LogLevel.getByLetterString(matcher.group(4));
                  this.mLastMessageInfo.tag = matcher.group(5).trim();
               } else {
                  LogPanel.LogMessage mc = new LogPanel.LogMessage();
                  if(this.mLastMessageInfo == null) {
                     this.mLastMessageInfo = new LogPanel.LogMessageInfo();
                     this.mLastMessageInfo.time = "??-?? ??:??:??.???";
                     this.mLastMessageInfo.pidString = "<unknown>";
                     this.mLastMessageInfo.pid = 0;
                     this.mLastMessageInfo.logLevel = LogLevel.INFO;
                     this.mLastMessageInfo.tag = "<unknown>";
                  }

                  mc.data = this.mLastMessageInfo;
                  mc.msg = line.replaceAll("\t", "    ");
                  this.processNewMessage(mc);
                  newMessages.add(mc);
               }
            }
         }

         if(!this.mPendingAsyncRefresh) {
            this.mPendingAsyncRefresh = true;

            try {
               Display var13 = this.mFolders.getDisplay();
               var13.asyncExec(new Runnable() {
                  public void run() {
                     LogPanel.this.asyncRefresh();
                  }
               });
            } catch (SWTException var11) {
               this.stopLogCat(false);
            }
         }

      }
   }

   private void asyncRefresh() {
      if(!this.mFolders.isDisposed()) {
         LogPanel.LogMessage[] var1 = this.mBuffer;
         synchronized(this.mBuffer) {
            try {
               if(this.mFilters != null) {
                  LogFilter[] arr$ = this.mFilters;
                  int len$ = arr$.length;

                  for(int i$ = 0; i$ < len$; ++i$) {
                     LogFilter f = arr$[i$];
                     f.flush();
                  }
               }

               if(this.mDefaultFilter != null) {
                  this.mDefaultFilter.flush();
               }
            } finally {
               this.mPendingAsyncRefresh = false;
            }
         }
      } else {
         this.stopLogCat(true);
      }

   }

   private void processNewMessage(LogPanel.LogMessage newMessage) {
      if(this.mFilterMode == 2 || this.mFilterMode == 3) {
         this.checkFilter(newMessage.data);
      }

      boolean messageIndex = true;
      int var9;
      if(this.mBufferStart == -1) {
         var9 = this.mBufferStart = 0;
         this.mBufferEnd = 1;
      } else {
         var9 = this.mBufferEnd;
         this.mBufferEnd = (this.mBufferEnd + 1) % 10000;
         if(this.mBufferEnd == this.mBufferStart) {
            this.mBufferStart = (this.mBufferStart + 1) % 10000;
         }
      }

      LogPanel.LogMessage oldMessage = null;
      if(this.mBuffer[var9] != null) {
         oldMessage = this.mBuffer[var9];
      }

      this.mBuffer[var9] = newMessage;
      boolean filtered = false;
      if(this.mFilters != null) {
         LogFilter[] arr$ = this.mFilters;
         int len$ = arr$.length;

         for(int i$ = 0; i$ < len$; ++i$) {
            LogFilter f = arr$[i$];
            filtered |= f.addMessage(newMessage, oldMessage);
         }
      }

      if(!filtered && this.mDefaultFilter != null) {
         this.mDefaultFilter.addMessage(newMessage, oldMessage);
      }

   }

   private void createFilters() {
      if(this.mFilterMode != 4 && this.mFilterMode != 1) {
         if(this.mFilterMode == 0) {
            this.mDefaultFilter = new LogFilter("Log");
            this.mDefaultFilter.setColors(this.mColors);
            this.mDefaultFilter.setSupportsDelete(false);
            this.mDefaultFilter.setSupportsEdit(false);
         }
      } else {
         this.mFilters = this.mFilterStorage.getFilterFromStore();
         if(this.mFilters != null) {
            LogFilter[] arr$ = this.mFilters;
            int len$ = arr$.length;

            for(int i$ = 0; i$ < len$; ++i$) {
               LogFilter f = arr$[i$];
               f.setColors(this.mColors);
            }
         }

         if(this.mFilterStorage.requiresDefaultFilter()) {
            this.mDefaultFilter = new LogFilter("Log");
            this.mDefaultFilter.setColors(this.mColors);
            this.mDefaultFilter.setSupportsDelete(false);
            this.mDefaultFilter.setSupportsEdit(false);
         }
      }

   }

   private boolean checkFilter(LogPanel.LogMessageInfo md) {
      return true;
   }

   private void addFilterToArray(LogFilter newFilter) {
      newFilter.setColors(this.mColors);
      if(this.mFilters != null && this.mFilters.length > 0) {
         LogFilter[] newFilters = new LogFilter[this.mFilters.length + 1];
         System.arraycopy(this.mFilters, 0, newFilters, 0, this.mFilters.length);
         newFilters[this.mFilters.length] = newFilter;
         this.mFilters = newFilters;
      } else {
         this.mFilters = new LogFilter[1];
         this.mFilters[0] = newFilter;
      }

   }

   private void removeFilterFromArray(LogFilter oldFilter) {
      int index = -1;

      for(int newFilters = 0; newFilters < this.mFilters.length; ++newFilters) {
         if(this.mFilters[newFilters] == oldFilter) {
            index = newFilters;
            break;
         }
      }

      if(index != -1) {
         LogFilter[] var4 = new LogFilter[this.mFilters.length - 1];
         System.arraycopy(this.mFilters, 0, var4, 0, index);
         System.arraycopy(this.mFilters, index + 1, var4, index, var4.length - index);
         this.mFilters = var4;
      }

   }

   private void initFilter(LogFilter filter) {
      if(filter.uiReady()) {
         if(filter == this.mDefaultFilter) {
            this.initDefaultFilter();
         } else {
            filter.clear();
            if(this.mBufferStart != -1) {
               int max = this.mBufferEnd;
               if(this.mBufferEnd < this.mBufferStart) {
                  max += 10000;
               }

               for(int i = this.mBufferStart; i < max; ++i) {
                  int realItemIndex = i % 10000;
                  filter.addMessage(this.mBuffer[realItemIndex], (LogPanel.LogMessage)null);
               }
            }

            filter.flush();
            filter.resetTempFilteringStatus();
         }
      }
   }

   private void initDefaultFilter() {
      this.mDefaultFilter.clear();
      if(this.mBufferStart != -1) {
         int max = this.mBufferEnd;
         if(this.mBufferEnd < this.mBufferStart) {
            max += 10000;
         }

         for(int i = this.mBufferStart; i < max; ++i) {
            int realItemIndex = i % 10000;
            LogPanel.LogMessage msg = this.mBuffer[realItemIndex];
            boolean filtered = false;
            LogFilter[] arr$ = this.mFilters;
            int len$ = arr$.length;

            for(int i$ = 0; i$ < len$; ++i$) {
               LogFilter f = arr$[i$];
               filtered |= f.accept(msg);
            }

            if(!filtered) {
               this.mDefaultFilter.addMessage(msg, (LogPanel.LogMessage)null);
            }
         }
      }

      this.mDefaultFilter.flush();
      this.mDefaultFilter.resetTempFilteringStatus();
   }

   private void resetFilters() {
      if(this.mFilterMode == 2 || this.mFilterMode == 3) {
         this.mFilters = null;
         this.createFilters();
      }

   }

   private LogFilter getCurrentFilter() {
      int index = this.mFolders.getSelectionIndex();
      return index != 0 && this.mFilters != null?this.mFilters[index - 1]:this.mDefaultFilter;
   }

   private void emptyTables() {
      LogFilter[] arr$ = this.mFilters;
      int len$ = arr$.length;

      for(int i$ = 0; i$ < len$; ++i$) {
         LogFilter f = arr$[i$];
         f.getTable().removeAll();
      }

      if(this.mDefaultFilter != null) {
         this.mDefaultFilter.getTable().removeAll();
      }

   }

   protected void updateFilteringWith(String text) {
      LogPanel.LogMessage[] var2 = this.mBuffer;
      synchronized(this.mBuffer) {
         LogFilter[] segments = this.mFilters;
         int keywords = segments.length;

         int tempPid;
         for(tempPid = 0; tempPid < keywords; ++tempPid) {
            LogFilter tempTag = segments[tempPid];
            tempTag.resetTempFiltering();
         }

         if(this.mDefaultFilter != null) {
            this.mDefaultFilter.resetTempFiltering();
         }

         String[] var14 = text.split(" ");
         ArrayList var15 = new ArrayList(var14.length);
         tempPid = -1;
         String var16 = null;

         for(int keywordsArray = 0; keywordsArray < var14.length; ++keywordsArray) {
            String arr$ = var14[keywordsArray];
            String[] len$;
            if(tempPid == -1 && arr$.startsWith("pid:")) {
               len$ = arr$.split(":");
               if(len$.length == 2 && len$[1].matches("^[0-9]*$")) {
                  tempPid = Integer.valueOf(len$[1]).intValue();
               }
            } else if(var16 == null && arr$.startsWith("tag:")) {
               len$ = var14[keywordsArray].split(":");
               if(len$.length == 2) {
                  var16 = len$[1];
               }
            } else {
               var15.add(arr$);
            }
         }

         if(tempPid != -1 || var16 != null || var15.size() > 0) {
            String[] var17 = (String[])var15.toArray(new String[var15.size()]);
            LogFilter[] var18 = this.mFilters;
            int var19 = var18.length;

            for(int i$ = 0; i$ < var19; ++i$) {
               LogFilter f = var18[i$];
               if(tempPid != -1) {
                  f.setTempPidFiltering(tempPid);
               }

               if(var16 != null) {
                  f.setTempTagFiltering(var16);
               }

               f.setTempKeywordFiltering(var17);
            }

            if(this.mDefaultFilter != null) {
               if(tempPid != -1) {
                  this.mDefaultFilter.setTempPidFiltering(tempPid);
               }

               if(var16 != null) {
                  this.mDefaultFilter.setTempTagFiltering(var16);
               }

               this.mDefaultFilter.setTempKeywordFiltering(var17);
            }
         }

         this.initFilter(this.mCurrentFilter);
      }
   }

   private void selectionChanged(LogFilter selectedFilter) {
      if(this.mLogLevelActions != null) {
         int level = selectedFilter.getLogLevel();

         for(int i = 0; i < this.mLogLevelActions.length; ++i) {
            ICommonAction a = this.mLogLevelActions[i];
            if(i == level - 2) {
               a.setChecked(true);
            } else {
               a.setChecked(false);
            }
         }
      }

      if(this.mDeleteFilterAction != null) {
         this.mDeleteFilterAction.setEnabled(selectedFilter.supportsDelete());
      }

      if(this.mEditFilterAction != null) {
         this.mEditFilterAction.setEnabled(selectedFilter.supportsEdit());
      }

   }

   public String getSelectedErrorLineMessage() {
      Table table = this.mCurrentFilter.getTable();
      int[] selection = table.getSelectionIndices();
      if(selection.length == 1) {
         TableItem item = table.getItem(selection[0]);
         LogPanel.LogMessage msg = (LogPanel.LogMessage)item.getData();
         if(msg.data.logLevel == LogLevel.ERROR || msg.data.logLevel == LogLevel.WARN) {
            return msg.msg;
         }
      }

      return null;
   }

   public void setLogCatViewInterface(LogPanel.LogCatViewInterface i) {
      this.mLogCatViewInterface = i;
   }

   public interface LogCatViewInterface {
      void onDoubleClick();
   }

   private class PsOutputReceiver extends MultiLineReceiver {
      private LogFilter mFilter;
      private TabItem mTabItem;
      private int mPid;
      private boolean mDone = false;

      PsOutputReceiver(int pid, LogFilter filter, TabItem tabItem) {
         this.mPid = pid;
         this.mFilter = filter;
         this.mTabItem = tabItem;
      }

      public boolean isCancelled() {
         return this.mDone;
      }

      public void processNewLines(String[] lines) {
         String[] arr$ = lines;
         int len$ = lines.length;

         for(int i$ = 0; i$ < len$; ++i$) {
            String line = arr$[i$];
            if(!line.startsWith("USER")) {
               int index = line.indexOf(32);
               if(index != -1) {
                  ++index;

                  while(line.charAt(index) == 32) {
                     ++index;
                  }

                  int index2 = line.indexOf(32, index);
                  String pidStr = line.substring(index, index2);
                  int pid = Integer.parseInt(pidStr);
                  if(pid == this.mPid) {
                     index = line.lastIndexOf(32);
                     final String name = line.substring(index + 1);
                     this.mFilter.setName(name);
                     Display d = LogPanel.this.mFolders.getDisplay();
                     d.asyncExec(new Runnable() {
                        public void run() {
                           PsOutputReceiver.this.mTabItem.setText(name);
                        }
                     });
                     this.mDone = true;
                     return;
                  }
               }
            }
         }

      }
   }

   private final class LogCatOuputReceiver extends MultiLineReceiver {
      public boolean isCancelled = false;

      public LogCatOuputReceiver() {
         this.setTrimLine(false);
      }

      public void processNewLines(String[] lines) {
         if(!this.isCancelled) {
            LogPanel.this.processLogLines(lines);
         }

      }

      public boolean isCancelled() {
         return this.isCancelled;
      }
   }

   protected static class LogMessage {
      public LogPanel.LogMessageInfo data;
      public String msg;

      public String toString() {
         return this.data.time + ": " + this.data.logLevel + "/" + this.data.tag + "(" + this.data.pidString + "): " + this.msg;
      }
   }

   protected static class LogMessageInfo {
      public LogLevel logLevel;
      public int pid;
      public String pidString;
      public String tag;
      public String time;
   }

   public interface ILogFilterStorageManager {
      LogFilter[] getFilterFromStore();

      void saveFilters(LogFilter[] var1);

      boolean requiresDefaultFilter();
   }
}
