package com.android.ddmuilib.logcat;

import com.android.ddmlib.Log;
import com.android.ddmlib.Log.LogLevel;
import com.android.ddmuilib.logcat.LogColors;
import com.android.ddmuilib.logcat.LogPanel;
import java.util.ArrayList;
import java.util.regex.PatternSyntaxException;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.widgets.ScrollBar;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;

public class LogFilter {
   public static final int MODE_PID = 1;
   public static final int MODE_TAG = 2;
   public static final int MODE_LEVEL = 4;
   private String mName;
   private int mMode = 0;
   private int mPid;
   private int mLogLevel;
   private String mTag;
   private Table mTable;
   private TabItem mTabItem;
   private boolean mIsCurrentTabItem = false;
   private int mUnreadCount = 0;
   private String[] mTempKeywordFilters;
   private int mTempPid = -1;
   private String mTempTag;
   private int mTempLogLevel = -1;
   private LogColors mColors;
   private boolean mTempFilteringStatus = false;
   private final ArrayList<LogPanel.LogMessage> mMessages = new ArrayList();
   private final ArrayList<LogPanel.LogMessage> mNewMessages = new ArrayList();
   private boolean mSupportsDelete = true;
   private boolean mSupportsEdit = true;
   private int mRemovedMessageCount = 0;

   public LogFilter(String name) {
      this.mName = name;
   }

   public LogFilter() {
   }

   public String toString() {
      StringBuilder sb = new StringBuilder(this.mName);
      sb.append(':');
      sb.append(this.mMode);
      if((this.mMode & 1) == 1) {
         sb.append(':');
         sb.append(this.mPid);
      }

      if((this.mMode & 4) == 4) {
         sb.append(':');
         sb.append(this.mLogLevel);
      }

      if((this.mMode & 2) == 2) {
         sb.append(':');
         sb.append(this.mTag);
      }

      return sb.toString();
   }

   public boolean loadFromString(String string) {
      String[] segments = string.split(":");
      byte index = 0;
      int var5 = index + 1;
      this.mName = segments[index];
      this.mMode = Integer.parseInt(segments[var5++]);
      if((this.mMode & 1) == 1) {
         this.mPid = Integer.parseInt(segments[var5++]);
      }

      if((this.mMode & 4) == 4) {
         this.mLogLevel = Integer.parseInt(segments[var5++]);
      }

      if((this.mMode & 2) == 2) {
         this.mTag = segments[var5++];
      }

      return true;
   }

   void setName(String name) {
      this.mName = name;
   }

   public String getName() {
      return this.mName;
   }

   public void setWidgets(TabItem tabItem, Table table) {
      this.mTable = table;
      this.mTabItem = tabItem;
   }

   public boolean uiReady() {
      return this.mTable != null && this.mTabItem != null;
   }

   public Table getTable() {
      return this.mTable;
   }

   public void dispose() {
      this.mTable.dispose();
      this.mTabItem.dispose();
      this.mTable = null;
      this.mTabItem = null;
   }

   public void resetFilteringMode() {
      this.mMode = 0;
   }

   public int getFilteringMode() {
      return this.mMode;
   }

   public void setPidMode(int pid) {
      if(pid != -1) {
         this.mMode |= 1;
      } else {
         this.mMode &= -2;
      }

      this.mPid = pid;
   }

   public int getPidFilter() {
      return (this.mMode & 1) == 1?this.mPid:-1;
   }

   public void setTagMode(String tag) {
      if(tag != null && tag.length() > 0) {
         this.mMode |= 2;
      } else {
         this.mMode &= -3;
      }

      this.mTag = tag;
   }

   public String getTagFilter() {
      return (this.mMode & 2) == 2?this.mTag:null;
   }

   public void setLogLevel(int level) {
      if(level == -1) {
         this.mMode &= -5;
      } else {
         this.mMode |= 4;
         this.mLogLevel = level;
      }

   }

   public int getLogLevel() {
      return (this.mMode & 4) == 4?this.mLogLevel:-1;
   }

   public boolean supportsDelete() {
      return this.mSupportsDelete;
   }

   public boolean supportsEdit() {
      return this.mSupportsEdit;
   }

   public void setSelectedState(boolean selected) {
      if(selected) {
         if(this.mTabItem != null) {
            this.mTabItem.setText(this.mName);
         }

         this.mUnreadCount = 0;
      }

      this.mIsCurrentTabItem = selected;
   }

   public boolean addMessage(LogPanel.LogMessage newMessage, LogPanel.LogMessage oldMessage) {
      ArrayList var3 = this.mMessages;
      synchronized(this.mMessages) {
         if(oldMessage != null) {
            int filter = this.mMessages.indexOf(oldMessage);
            if(filter != -1) {
               this.mMessages.remove(filter);
               ++this.mRemovedMessageCount;
            }

            filter = this.mNewMessages.indexOf(oldMessage);
            if(filter != -1) {
               this.mNewMessages.remove(filter);
            }
         }

         boolean var7 = this.accept(newMessage);
         if(var7) {
            this.mMessages.add(newMessage);
            this.mNewMessages.add(newMessage);
         }

         return var7;
      }
   }

   public void clear() {
      this.mRemovedMessageCount = 0;
      this.mNewMessages.clear();
      this.mMessages.clear();
      this.mTable.removeAll();
   }

   boolean accept(LogPanel.LogMessage logMessage) {
      if((this.mMode & 1) == 1 && this.mPid != logMessage.data.pid) {
         return false;
      } else if((this.mMode & 2) == 2 && (logMessage.data.tag == null || !logMessage.data.tag.equals(this.mTag))) {
         return false;
      } else {
         int msgLogLevel = logMessage.data.logLevel.getPriority();
         if(this.mTempLogLevel != -1) {
            if(this.mTempLogLevel > msgLogLevel) {
               return false;
            }
         } else if((this.mMode & 4) == 4 && this.mLogLevel > msgLogLevel) {
            return false;
         }

         if(this.mTempKeywordFilters != null) {
            String msg = logMessage.msg;
            String[] arr$ = this.mTempKeywordFilters;
            int len$ = arr$.length;

            for(int i$ = 0; i$ < len$; ++i$) {
               String kw = arr$[i$];

               try {
                  if(!msg.contains(kw) && !msg.matches(kw)) {
                     return false;
                  }
               } catch (PatternSyntaxException var9) {
                  return false;
               }
            }
         }

         return this.mTempPid != -1 && this.mTempPid != logMessage.data.pid?false:this.mTempTag == null || this.mTempTag.length() <= 0 || this.mTempTag.equals(logMessage.data.tag);
      }
   }

   public void flush() {
      ScrollBar bar = this.mTable.getVerticalBar();
      boolean scroll = bar.getMaximum() == bar.getSelection() + bar.getThumb();
      int topIndex = this.mTable.getTopIndex();
      this.mTable.setRedraw(false);
      int totalCount = this.mNewMessages.size();

      try {
         int e;
         for(e = 0; e < this.mRemovedMessageCount && this.mTable.getItemCount() > 0; ++e) {
            this.mTable.remove(0);
         }

         this.mRemovedMessageCount = 0;
         if(this.mUnreadCount > this.mTable.getItemCount()) {
            this.mUnreadCount = this.mTable.getItemCount();
         }

         for(e = 0; e < totalCount; ++e) {
            LogPanel.LogMessage msg = (LogPanel.LogMessage)this.mNewMessages.get(e);
            this.addTableItem(msg);
         }
      } catch (SWTException var7) {
         Log.e("LogFilter", var7);
      }

      this.mTable.setRedraw(true);
      if(scroll) {
         totalCount = this.mTable.getItemCount();
         if(totalCount > 0) {
            this.mTable.showItem(this.mTable.getItem(totalCount - 1));
         }
      } else if(this.mRemovedMessageCount > 0) {
         topIndex -= this.mRemovedMessageCount;
         if(topIndex < 0) {
            this.mTable.showItem(this.mTable.getItem(0));
         } else {
            this.mTable.showItem(this.mTable.getItem(topIndex));
         }
      }

      if(!this.mIsCurrentTabItem) {
         this.mUnreadCount += this.mNewMessages.size();
         totalCount = this.mTable.getItemCount();
         if(this.mUnreadCount > 0) {
            this.mTabItem.setText(this.mName + " (" + (this.mUnreadCount > totalCount?totalCount:this.mUnreadCount) + ")");
         } else {
            this.mTabItem.setText(this.mName);
         }
      }

      this.mNewMessages.clear();
   }

   void setColors(LogColors colors) {
      this.mColors = colors;
   }

   int getUnreadCount() {
      return this.mUnreadCount;
   }

   void setUnreadCount(int unreadCount) {
      this.mUnreadCount = unreadCount;
   }

   void setSupportsDelete(boolean support) {
      this.mSupportsDelete = support;
   }

   void setSupportsEdit(boolean support) {
      this.mSupportsEdit = support;
   }

   void setTempKeywordFiltering(String[] segments) {
      this.mTempKeywordFilters = segments;
      this.mTempFilteringStatus = true;
   }

   void setTempPidFiltering(int pid) {
      this.mTempPid = pid;
      this.mTempFilteringStatus = true;
   }

   void setTempTagFiltering(String tag) {
      this.mTempTag = tag;
      this.mTempFilteringStatus = true;
   }

   void resetTempFiltering() {
      if(this.mTempPid != -1 || this.mTempTag != null || this.mTempKeywordFilters != null) {
         this.mTempFilteringStatus = true;
      }

      this.mTempPid = -1;
      this.mTempTag = null;
      this.mTempKeywordFilters = null;
   }

   void resetTempFilteringStatus() {
      this.mTempFilteringStatus = false;
   }

   boolean getTempFilterStatus() {
      return this.mTempFilteringStatus;
   }

   private void addTableItem(LogPanel.LogMessage msg) {
      TableItem item = new TableItem(this.mTable, 0);
      item.setText(0, msg.data.time);
      item.setText(1, new String(new char[]{msg.data.logLevel.getPriorityLetter()}));
      item.setText(2, msg.data.pidString);
      item.setText(3, msg.data.tag);
      item.setText(4, msg.msg);
      item.setData(msg);
      if(msg.data.logLevel == LogLevel.INFO) {
         item.setForeground(this.mColors.infoColor);
      } else if(msg.data.logLevel == LogLevel.DEBUG) {
         item.setForeground(this.mColors.debugColor);
      } else if(msg.data.logLevel == LogLevel.ERROR) {
         item.setForeground(this.mColors.errorColor);
      } else if(msg.data.logLevel == LogLevel.WARN) {
         item.setForeground(this.mColors.warningColor);
      } else {
         item.setForeground(this.mColors.verboseColor);
      }

   }
}
