package com.android.ddmuilib.logcat;

import com.android.ddmlib.DdmConstants;
import com.android.ddmlib.IDevice;
import com.android.ddmlib.Log.LogLevel;
import com.android.ddmlib.logcat.LogCatFilter;
import com.android.ddmlib.logcat.LogCatMessage;
import com.android.ddmuilib.AbstractBufferFindTarget;
import com.android.ddmuilib.FindDialog;
import com.android.ddmuilib.ITableFocusListener;
import com.android.ddmuilib.ImageLoader;
import com.android.ddmuilib.SelectionDependentPanel;
import com.android.ddmuilib.TableHelper;
import com.android.ddmuilib.logcat.ILogCatBufferChangeListener;
import com.android.ddmuilib.logcat.ILogCatMessageSelectionListener;
import com.android.ddmuilib.logcat.LogCatFilterContentProvider;
import com.android.ddmuilib.logcat.LogCatFilterData;
import com.android.ddmuilib.logcat.LogCatFilterLabelProvider;
import com.android.ddmuilib.logcat.LogCatFilterSettingsDialog;
import com.android.ddmuilib.logcat.LogCatFilterSettingsSerializer;
import com.android.ddmuilib.logcat.LogCatReceiver;
import com.android.ddmuilib.logcat.LogCatReceiverFactory;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferenceConverter;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.FocusListener;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.ScrollBar;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

/**
 * LogCat面板
 */
public final class LogCatPanel extends SelectionDependentPanel implements ILogCatBufferChangeListener {
   public static final String LOGCAT_FILTERS_LIST = "logcat.view.filters.list";
   public static final String LOGCAT_VIEW_FONT_PREFKEY = "logcat.view.font";
   public static final String AUTO_SCROLL_LOCK_PREFKEY = "logcat.view.auto-scroll-lock";
   private static final String MSG_COLOR_PREFKEY_PREFIX = "logcat.msg.color.";
   public static final String VERBOSE_COLOR_PREFKEY = "logcat.msg.color.verbose";
   public static final String DEBUG_COLOR_PREFKEY = "logcat.msg.color.debug";
   public static final String INFO_COLOR_PREFKEY = "logcat.msg.color.info";
   public static final String WARN_COLOR_PREFKEY = "logcat.msg.color.warn";
   public static final String ERROR_COLOR_PREFKEY = "logcat.msg.color.error";
   public static final String ASSERT_COLOR_PREFKEY = "logcat.msg.color.assert";
   private static final String FONT_FAMILY;
   private static final FontData DEFAULT_LOGCAT_FONTDATA;
   private static final String LOGCAT_VIEW_COLSIZE_PREFKEY_PREFIX = "logcat.view.colsize.";
   private static final String DISPLAY_FILTERS_COLUMN_PREFKEY = "logcat.view.display.filters";
   private static final String DEFAULT_SEARCH_MESSAGE = "Search for messages. Accepts Java regexes. Prefix with pid:, app:, tag: or text: to limit scope.";
   private static final String DEFAULT_SEARCH_TOOLTIP = "Example search patterns:\n    sqlite (search for sqlite in text field)\n    app:browser (search for messages generated by the browser application)";
   private static final String IMAGE_ADD_FILTER = "add.png";
   private static final String IMAGE_DELETE_FILTER = "delete.png";
   private static final String IMAGE_EDIT_FILTER = "edit.png";
   private static final String IMAGE_SAVE_LOG_TO_FILE = "save.png";
   private static final String IMAGE_CLEAR_LOG = "clear.png";
   private static final String IMAGE_DISPLAY_FILTERS = "displayfilters.png";
   private static final String IMAGE_SCROLL_LOCK = "scroll_lock.png";
   private static final int[] WEIGHTS_SHOW_FILTERS;
   private static final int[] WEIGHTS_LOGCAT_ONLY;
   private static final int DEFAULT_FILTER_INDEX = 0;
   private static final Color VALID_FILTER_REGEX_COLOR;
   private static final Color INVALID_FILTER_REGEX_COLOR;
   private LogCatReceiver mReceiver;
   private IPreferenceStore mPrefStore;
   private List<LogCatFilter> mLogCatFilters;
   private Map<LogCatFilter, LogCatFilterData> mLogCatFilterData;
   private int mCurrentSelectedFilterIndex;
   private ToolItem mNewFilterToolItem;
   private ToolItem mDeleteFilterToolItem;
   private ToolItem mEditFilterToolItem;
   private TableViewer mFiltersTableViewer;
   private Combo mLiveFilterLevelCombo;
   private Text mLiveFilterText;
   private List<LogCatFilter> mCurrentFilters = Collections.emptyList();
   private Table mTable;
   private boolean mShouldScrollToLatestLog = true;
   private ToolItem mScrollLockCheckBox;
   private boolean mAutoScrollLock;
   private final Object mScrollBarSelectionListenerLock = new Object();
   private SelectionListener mScrollBarSelectionListener;
   private boolean mScrollBarListenerSet = false;
   private String mLogFileExportFolder;
   private Font mFont;
   private int mWrapWidthInChars;
   private Color mVerboseColor;
   private Color mDebugColor;
   private Color mInfoColor;
   private Color mWarnColor;
   private Color mErrorColor;
   private Color mAssertColor;
   private SashForm mSash;
   private List<LogCatMessage> mLogBuffer;
   private int mDeletedLogCount;
   private LogCatPanel.LogCatTableRefresherTask mCurrentRefresher;
   private List<ILogCatMessageSelectionListener> mMessageSelectionListeners;
   private ITableFocusListener mTableFocusListener;
   private FindDialog mFindDialog;
   private LogCatPanel.LogcatFindTarget mFindTarget = new LogCatPanel.LogcatFindTarget(null);

   public LogCatPanel(IPreferenceStore prefStore) {
      this.mPrefStore = prefStore;
      this.mLogBuffer = new ArrayList(5000);
      this.initializeFilters();
      this.setupDefaultPreferences();
      this.initializePreferenceUpdateListeners();
      this.mFont = this.getFontFromPrefStore();
      this.loadMessageColorPreferences();
      this.mAutoScrollLock = this.mPrefStore.getBoolean("logcat.view.auto-scroll-lock");
   }

   private void loadMessageColorPreferences() {
      if(this.mVerboseColor != null) {
         this.disposeMessageColors();
      }

      this.mVerboseColor = this.getColorFromPrefStore("logcat.msg.color.verbose");
      this.mDebugColor = this.getColorFromPrefStore("logcat.msg.color.debug");
      this.mInfoColor = this.getColorFromPrefStore("logcat.msg.color.info");
      this.mWarnColor = this.getColorFromPrefStore("logcat.msg.color.warn");
      this.mErrorColor = this.getColorFromPrefStore("logcat.msg.color.error");
      this.mAssertColor = this.getColorFromPrefStore("logcat.msg.color.assert");
   }

   private void initializeFilters() {
      this.mLogCatFilters = new ArrayList();
      this.mLogCatFilterData = new ConcurrentHashMap();
      String tag = "";
      String text = "";
      String pid = "";
      String app = "";
      LogCatFilter defaultFilter = new LogCatFilter("All messages (no filters)", tag, text, pid, app, LogLevel.VERBOSE);
      this.mLogCatFilters.add(defaultFilter);
      this.mLogCatFilterData.put(defaultFilter, new LogCatFilterData(defaultFilter));
      List savedFilters = this.getSavedFilters();
      Iterator i$ = savedFilters.iterator();

      while(i$.hasNext()) {
         LogCatFilter f = (LogCatFilter)i$.next();
         this.mLogCatFilters.add(f);
         this.mLogCatFilterData.put(f, new LogCatFilterData(f));
      }

   }

   private void setupDefaultPreferences() {
      PreferenceConverter.setDefault(this.mPrefStore, "logcat.view.font", DEFAULT_LOGCAT_FONTDATA);
      this.mPrefStore.setDefault("logcat.messagelist.max.size", 5000);
      this.mPrefStore.setDefault("logcat.view.display.filters", true);
      this.mPrefStore.setDefault("logcat.view.auto-scroll-lock", true);
      PreferenceConverter.setDefault(this.mPrefStore, "logcat.msg.color.verbose", new RGB(0, 0, 0));
      PreferenceConverter.setDefault(this.mPrefStore, "logcat.msg.color.debug", new RGB(0, 0, 127));
      PreferenceConverter.setDefault(this.mPrefStore, "logcat.msg.color.info", new RGB(0, 127, 0));
      PreferenceConverter.setDefault(this.mPrefStore, "logcat.msg.color.warn", new RGB(255, 127, 0));
      PreferenceConverter.setDefault(this.mPrefStore, "logcat.msg.color.error", new RGB(255, 0, 0));
      PreferenceConverter.setDefault(this.mPrefStore, "logcat.msg.color.assert", new RGB(255, 0, 0));
   }

   private void initializePreferenceUpdateListeners() {
      this.mPrefStore.addPropertyChangeListener(new IPropertyChangeListener() {
         public void propertyChange(PropertyChangeEvent event) {
            String changedProperty = event.getProperty();
            if(changedProperty.equals("logcat.view.font")) {
               if(LogCatPanel.this.mFont != null) {
                  LogCatPanel.this.mFont.dispose();
               }

               LogCatPanel.this.mFont = LogCatPanel.this.getFontFromPrefStore();
               LogCatPanel.this.recomputeWrapWidth();
               Display.getDefault().syncExec(new Runnable() {
                  public void run() {
                     TableItem[] arr$ = LogCatPanel.this.mTable.getItems();
                     int len$ = arr$.length;

                     for(int i$ = 0; i$ < len$; ++i$) {
                        TableItem it = arr$[i$];
                        it.setFont(LogCatPanel.this.mFont);
                     }

                  }
               });
            } else if(changedProperty.startsWith("logcat.msg.color.")) {
               LogCatPanel.this.loadMessageColorPreferences();
               Display.getDefault().syncExec(new Runnable() {
                  public void run() {
                     Color c = LogCatPanel.this.mVerboseColor;
                     TableItem[] arr$ = LogCatPanel.this.mTable.getItems();
                     int len$ = arr$.length;

                     for(int i$ = 0; i$ < len$; ++i$) {
                        TableItem it = arr$[i$];
                        Object data = it.getData();
                        if(data instanceof LogCatMessage) {
                           c = LogCatPanel.this.getForegroundColor((LogCatMessage)data);
                        }

                        it.setForeground(c);
                     }

                  }
               });
            } else if(changedProperty.equals("logcat.messagelist.max.size")) {
               LogCatPanel.this.mReceiver.resizeFifo(LogCatPanel.this.mPrefStore.getInt("logcat.messagelist.max.size"));
               LogCatPanel.this.reloadLogBuffer();
            } else if(changedProperty.equals("logcat.view.auto-scroll-lock")) {
               LogCatPanel.this.mAutoScrollLock = LogCatPanel.this.mPrefStore.getBoolean("logcat.view.auto-scroll-lock");
            }

         }
      });
   }

   private void saveFilterPreferences() {
      LogCatFilterSettingsSerializer serializer = new LogCatFilterSettingsSerializer();
      String e = serializer.encodeToPreferenceString(this.mLogCatFilters.subList(1, this.mLogCatFilters.size()), this.mLogCatFilterData);
      this.mPrefStore.setValue("logcat.view.filters.list", e);
   }

   private List<LogCatFilter> getSavedFilters() {
      LogCatFilterSettingsSerializer serializer = new LogCatFilterSettingsSerializer();
      String e = this.mPrefStore.getString("logcat.view.filters.list");
      return serializer.decodeFromPreferenceString(e);
   }

   public void deviceSelected() {
      IDevice device = this.getCurrentDevice();
      if(device != null) {
         if(this.mReceiver != null) {
            this.mReceiver.removeMessageReceivedEventListener(this);
            Iterator i$ = this.mLogCatFilters.iterator();

            while(i$.hasNext()) {
               LogCatFilter f = (LogCatFilter)i$.next();
               LogCatFilterData fd = (LogCatFilterData)this.mLogCatFilterData.get(f);
               fd.resetUnreadCount();
            }
         }

         this.mReceiver = LogCatReceiverFactory.INSTANCE.newReceiver(device, this.mPrefStore);
         this.mReceiver.addMessageReceivedEventListener(this);
         this.reloadLogBuffer();
         Display.getDefault().asyncExec(new Runnable() {
            public void run() {
               LogCatPanel.this.scrollToLatestLog();
            }
         });
      }
   }

   public void clientSelected() {
   }

   protected void postCreation() {
   }

   protected Control createControl(Composite parent) {
      GridLayout layout = new GridLayout(1, false);
      parent.setLayout(layout);
      this.createViews(parent);
      this.setupDefaults();
      return null;
   }

   private void createViews(Composite parent) {
      this.mSash = this.createSash(parent);
      this.createListOfFilters(this.mSash);
      this.createLogTableView(this.mSash);
      boolean showFilters = this.mPrefStore.getBoolean("logcat.view.display.filters");
      this.updateFiltersColumn(showFilters);
   }

   private SashForm createSash(Composite parent) {
      SashForm sash = new SashForm(parent, 256);
      sash.setLayoutData(new GridData(1808));
      return sash;
   }

   private void createListOfFilters(SashForm sash) {
      Composite c = new Composite(sash, 2048);
      GridLayout layout = new GridLayout(2, false);
      c.setLayout(layout);
      c.setLayoutData(new GridData(1808));
      this.createFiltersToolbar(c);
      this.createFiltersTable(c);
   }

   private void createFiltersToolbar(Composite parent) {
      Label l = new Label(parent, 0);
      l.setText("Saved Filters");
      GridData gd = new GridData();
      gd.horizontalAlignment = 16384;
      l.setLayoutData(gd);
      ToolBar t = new ToolBar(parent, 8388608);
      gd = new GridData();
      gd.horizontalAlignment = 131072;
      t.setLayoutData(gd);
      this.mNewFilterToolItem = new ToolItem(t, 8);
      this.mNewFilterToolItem.setImage(ImageLoader.getDdmUiLibLoader().loadImage("add.png", t.getDisplay()));
      this.mNewFilterToolItem.setToolTipText("Add a new logcat filter");
      this.mNewFilterToolItem.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent arg0) {
            LogCatPanel.this.addNewFilter();
         }
      });
      this.mDeleteFilterToolItem = new ToolItem(t, 8);
      this.mDeleteFilterToolItem.setImage(ImageLoader.getDdmUiLibLoader().loadImage("delete.png", t.getDisplay()));
      this.mDeleteFilterToolItem.setToolTipText("Delete selected logcat filter");
      this.mDeleteFilterToolItem.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent arg0) {
            LogCatPanel.this.deleteSelectedFilter();
         }
      });
      this.mEditFilterToolItem = new ToolItem(t, 8);
      this.mEditFilterToolItem.setImage(ImageLoader.getDdmUiLibLoader().loadImage("edit.png", t.getDisplay()));
      this.mEditFilterToolItem.setToolTipText("Edit selected logcat filter");
      this.mEditFilterToolItem.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent arg0) {
            LogCatPanel.this.editSelectedFilter();
         }
      });
   }

   private void addNewFilter(String defaultTag, String defaultText, String defaultPid, String defaultAppName, LogLevel defaultLevel) {
      LogCatFilterSettingsDialog d = new LogCatFilterSettingsDialog(Display.getCurrent().getActiveShell());
      d.setDefaults("", defaultTag, defaultText, defaultPid, defaultAppName, defaultLevel);
      if(d.open() == 0) {
         LogCatFilter f = new LogCatFilter(d.getFilterName().trim(), d.getTag().trim(), d.getText().trim(), d.getPid().trim(), d.getAppName().trim(), LogLevel.getByString(d.getLogLevel()));
         this.mLogCatFilters.add(f);
         this.mLogCatFilterData.put(f, new LogCatFilterData(f));
         this.mFiltersTableViewer.refresh();
         int idx = this.mLogCatFilters.size() - 1;
         this.mFiltersTableViewer.getTable().setSelection(idx);
         this.filterSelectionChanged();
         this.saveFilterPreferences();
      }
   }

   private void addNewFilter() {
      this.addNewFilter("", "", "", "", LogLevel.VERBOSE);
   }

   private void deleteSelectedFilter() {
      int selectedIndex = this.mFiltersTableViewer.getTable().getSelectionIndex();
      if(selectedIndex > 0) {
         LogCatFilter f = (LogCatFilter)this.mLogCatFilters.get(selectedIndex);
         this.mLogCatFilters.remove(selectedIndex);
         this.mLogCatFilterData.remove(f);
         this.mFiltersTableViewer.refresh();
         this.mFiltersTableViewer.getTable().setSelection(selectedIndex - 1);
         this.filterSelectionChanged();
         this.saveFilterPreferences();
      }
   }

   private void editSelectedFilter() {
      int selectedIndex = this.mFiltersTableViewer.getTable().getSelectionIndex();
      if(selectedIndex >= 0) {
         LogCatFilter curFilter = (LogCatFilter)this.mLogCatFilters.get(selectedIndex);
         LogCatFilterSettingsDialog dialog = new LogCatFilterSettingsDialog(Display.getCurrent().getActiveShell());
         dialog.setDefaults(curFilter.getName(), curFilter.getTag(), curFilter.getText(), curFilter.getPid(), curFilter.getAppName(), curFilter.getLogLevel());
         if(dialog.open() == 0) {
            LogCatFilter f = new LogCatFilter(dialog.getFilterName(), dialog.getTag(), dialog.getText(), dialog.getPid(), dialog.getAppName(), LogLevel.getByString(dialog.getLogLevel()));
            this.mLogCatFilters.set(selectedIndex, f);
            this.mFiltersTableViewer.refresh();
            this.mFiltersTableViewer.getTable().setSelection(selectedIndex);
            this.filterSelectionChanged();
            this.saveFilterPreferences();
         }
      }
   }

   public void selectTransientAppFilter(String appName) {
      assert this.mTable.getDisplay().getThread() == Thread.currentThread();

      LogCatFilter f = this.findTransientAppFilter(appName);
      if(f == null) {
         f = this.createTransientAppFilter(appName);
         this.mLogCatFilters.add(f);
         LogCatFilterData fd = new LogCatFilterData(f);
         fd.setTransient();
         this.mLogCatFilterData.put(f, fd);
      }

      this.selectFilterAt(this.mLogCatFilters.indexOf(f));
   }

   private LogCatFilter findTransientAppFilter(String appName) {
      Iterator i$ = this.mLogCatFilters.iterator();

      LogCatFilter f;
      LogCatFilterData fd;
      do {
         if(!i$.hasNext()) {
            return null;
         }

         f = (LogCatFilter)i$.next();
         fd = (LogCatFilterData)this.mLogCatFilterData.get(f);
      } while(fd == null || !fd.isTransient() || !f.getAppName().equals(appName));

      return f;
   }

   private LogCatFilter createTransientAppFilter(String appName) {
      LogCatFilter f = new LogCatFilter(appName + " (Session Filter)", "", "", "", appName, LogLevel.VERBOSE);
      return f;
   }

   private void selectFilterAt(int index) {
      this.mFiltersTableViewer.refresh();
      if(index != this.mFiltersTableViewer.getTable().getSelectionIndex()) {
         this.mFiltersTableViewer.getTable().setSelection(index);
         this.filterSelectionChanged();
      }

   }

   private void createFiltersTable(Composite parent) {
      Table table = new Table(parent, 65536);
      GridData gd = new GridData(1808);
      gd.horizontalSpan = 2;
      table.setLayoutData(gd);
      this.mFiltersTableViewer = new TableViewer(table);
      this.mFiltersTableViewer.setContentProvider(new LogCatFilterContentProvider());
      this.mFiltersTableViewer.setLabelProvider(new LogCatFilterLabelProvider(this.mLogCatFilterData));
      this.mFiltersTableViewer.setInput(this.mLogCatFilters);
      this.mFiltersTableViewer.getTable().addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent event) {
            LogCatPanel.this.filterSelectionChanged();
         }

         public void widgetDefaultSelected(SelectionEvent arg0) {
            LogCatPanel.this.editSelectedFilter();
         }
      });
   }

   private void createLogTableView(SashForm sash) {
      Composite c = new Composite(sash, 0);
      c.setLayout(new GridLayout());
      c.setLayoutData(new GridData(1808));
      this.createLiveFilters(c);
      this.createLogcatViewTable(c);
   }

   private void createLiveFilters(Composite parent) {
      Composite c = new Composite(parent, 0);
      c.setLayout(new GridLayout(3, false));
      c.setLayoutData(new GridData(768));
      this.mLiveFilterText = new Text(c, 2176);
      this.mLiveFilterText.setLayoutData(new GridData(768));
      this.mLiveFilterText.setMessage("Search for messages. Accepts Java regexes. Prefix with pid:, app:, tag: or text: to limit scope.");
      this.mLiveFilterText.setToolTipText("Example search patterns:\n    sqlite (search for sqlite in text field)\n    app:browser (search for messages generated by the browser application)");
      this.mLiveFilterText.addModifyListener(new ModifyListener() {
         public void modifyText(ModifyEvent arg0) {
            LogCatPanel.this.updateFilterTextColor();
            LogCatPanel.this.updateAppliedFilters();
         }
      });
      this.mLiveFilterLevelCombo = new Combo(c, 12);
      this.mLiveFilterLevelCombo.setItems((String[])LogCatFilterSettingsDialog.getLogLevels().toArray(new String[0]));
      this.mLiveFilterLevelCombo.select(0);
      this.mLiveFilterLevelCombo.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent arg0) {
            LogCatPanel.this.updateAppliedFilters();
         }
      });
      ToolBar toolBar = new ToolBar(c, 8388608);
      ToolItem saveToLog = new ToolItem(toolBar, 8);
      saveToLog.setImage(ImageLoader.getDdmUiLibLoader().loadImage("save.png", toolBar.getDisplay()));
      saveToLog.setToolTipText("Export Selected Items To Text File..");
      saveToLog.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent arg0) {
            LogCatPanel.this.saveLogToFile();
         }
      });
      ToolItem clearLog = new ToolItem(toolBar, 8);
      clearLog.setImage(ImageLoader.getDdmUiLibLoader().loadImage("clear.png", toolBar.getDisplay()));
      clearLog.setToolTipText("Clear Log");
      clearLog.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent arg0) {
            if(LogCatPanel.this.mReceiver != null) {
               LogCatPanel.this.mReceiver.clearMessages();
               LogCatPanel.this.refreshLogCatTable();
               LogCatPanel.this.resetUnreadCountForAllFilters();
               LogCatPanel.this.updateAppliedFilters();
            }

         }
      });
      final ToolItem showFiltersColumn = new ToolItem(toolBar, 32);
      showFiltersColumn.setImage(ImageLoader.getDdmUiLibLoader().loadImage("displayfilters.png", toolBar.getDisplay()));
      showFiltersColumn.setSelection(this.mPrefStore.getBoolean("logcat.view.display.filters"));
      showFiltersColumn.setToolTipText("Display Saved Filters View");
      showFiltersColumn.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent event) {
            boolean showFilters = showFiltersColumn.getSelection();
            LogCatPanel.this.mPrefStore.setValue("logcat.view.display.filters", showFilters);
            LogCatPanel.this.updateFiltersColumn(showFilters);
         }
      });
      this.mScrollLockCheckBox = new ToolItem(toolBar, 32);
      this.mScrollLockCheckBox.setImage(ImageLoader.getDdmUiLibLoader().loadImage("scroll_lock.png", toolBar.getDisplay()));
      this.mScrollLockCheckBox.setSelection(true);
      this.mScrollLockCheckBox.setToolTipText("Scroll Lock");
      this.mScrollLockCheckBox.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent event) {
            boolean scrollLock = LogCatPanel.this.mScrollLockCheckBox.getSelection();
            LogCatPanel.this.setScrollToLatestLog(scrollLock);
         }
      });
   }

   private void updateFilterTextColor() {
      String text = this.mLiveFilterText.getText();

      Color c;
      try {
         Pattern.compile(text.trim());
         c = VALID_FILTER_REGEX_COLOR;
      } catch (PatternSyntaxException var4) {
         c = INVALID_FILTER_REGEX_COLOR;
      }

      this.mLiveFilterText.setForeground(c);
   }

   private void updateFiltersColumn(boolean showFilters) {
      if(showFilters) {
         this.mSash.setWeights(WEIGHTS_SHOW_FILTERS);
      } else {
         this.mSash.setWeights(WEIGHTS_LOGCAT_ONLY);
      }

   }

   private void saveLogToFile() {
      final String fName = this.getLogFileTargetLocation();
      if(fName != null) {
         final List selectedMessages = this.getSelectedLogCatMessages();
         Thread t = new Thread(new Runnable() {
            public void run() {
               BufferedWriter w = null;

               try {
                  w = new BufferedWriter(new FileWriter(fName));
                  Iterator e = selectedMessages.iterator();

                  while(e.hasNext()) {
                     LogCatMessage m = (LogCatMessage)e.next();
                     w.append(m.toString());
                     w.newLine();
                  }
               } catch (final IOException var12) {
                  Display.getDefault().asyncExec(new Runnable() {
                     public void run() {
                        MessageDialog.openError(Display.getCurrent().getActiveShell(), "Unable to export selection to file.", "Unexpected error while saving selected messages to file: " + var12.getMessage());
                     }
                  });
               } finally {
                  if(w != null) {
                     try {
                        w.close();
                     } catch (IOException var11) {
                        ;
                     }
                  }

               }

            }
         });
         t.setName("Saving selected items to logfile..");
         t.start();
      }
   }

   private String getLogFileTargetLocation() {
      FileDialog fd = new FileDialog(Display.getCurrent().getActiveShell(), 8192);
      fd.setText("Save Log..");
      fd.setFileName("log.txt");
      if(this.mLogFileExportFolder == null) {
         this.mLogFileExportFolder = System.getProperty("user.home");
      }

      fd.setFilterPath(this.mLogFileExportFolder);
      fd.setFilterNames(new String[]{"Text Files (*.txt)"});
      fd.setFilterExtensions(new String[]{"*.txt"});
      String fName = fd.open();
      if(fName != null) {
         this.mLogFileExportFolder = fd.getFilterPath();
      }

      return fName;
   }

   private List<LogCatMessage> getSelectedLogCatMessages() {
      int[] indices = this.mTable.getSelectionIndices();
      Arrays.sort(indices);
      ArrayList selectedMessages = new ArrayList(indices.length);
      int[] arr$ = indices;
      int len$ = indices.length;

      for(int i$ = 0; i$ < len$; ++i$) {
         int i = arr$[i$];
         Object data = this.mTable.getItem(i).getData();
         if(data instanceof LogCatMessage) {
            selectedMessages.add((LogCatMessage)data);
         }
      }

      return selectedMessages;
   }

   private List<LogCatMessage> applyCurrentFilters(List<LogCatMessage> msgList) {
      ArrayList filteredItems = new ArrayList(msgList.size());
      Iterator i$ = msgList.iterator();

      while(i$.hasNext()) {
         LogCatMessage msg = (LogCatMessage)i$.next();
         if(this.isMessageAccepted(msg, this.mCurrentFilters)) {
            filteredItems.add(msg);
         }
      }

      return filteredItems;
   }

   private boolean isMessageAccepted(LogCatMessage msg, List<LogCatFilter> filters) {
      Iterator i$ = filters.iterator();

      LogCatFilter f;
      do {
         if(!i$.hasNext()) {
            return true;
         }

         f = (LogCatFilter)i$.next();
      } while(f.matches(msg));

      return false;
   }

   private void createLogcatViewTable(Composite parent) {
      this.mTable = new Table(parent, 65538);
      this.mTable.setLayoutData(new GridData(1808));
      this.mTable.getHorizontalBar().setVisible(true);
      String[] properties = new String[]{"Level", "Time", "PID", "TID", "Application", "Tag", "Text"};
      String[] sampleText = new String[]{"    ", "    00-00 00:00:00.0000 ", "    0000", "    0000", "    com.android.launcher", "    SampleTagText", "    Log Message field should be pretty long by default. As long as possible for correct display on Mac."};

      for(int textColumn = 0; textColumn < properties.length; ++textColumn) {
         TableHelper.createTableColumn(this.mTable, properties[textColumn], 16384, sampleText[textColumn], this.getColPreferenceKey(properties[textColumn]), this.mPrefStore);
      }

      this.mTable.setLinesVisible(false);
      this.mTable.setHeaderVisible(true);
      this.mTable.addListener(41, new Listener() {
         public void handleEvent(Event event) {
            event.height = event.gc.getFontMetrics().getHeight();
         }
      });
      TableColumn var8 = this.mTable.getColumn(properties.length - 1);
      var8.addControlListener(new ControlAdapter() {
         public void controlResized(ControlEvent event) {
            LogCatPanel.this.recomputeWrapWidth();
         }
      });
      this.addRightClickMenu(this.mTable);
      this.initDoubleClickListener();
      this.recomputeWrapWidth();
      this.mTable.addDisposeListener(new DisposeListener() {
         public void widgetDisposed(DisposeEvent arg0) {
            LogCatPanel.this.dispose();
         }
      });
      final ScrollBar vbar = this.mTable.getVerticalBar();
      this.mScrollBarSelectionListener = new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            if(LogCatPanel.this.mAutoScrollLock) {
               int diff = vbar.getThumb() + vbar.getSelection() - vbar.getMaximum();
               boolean isAtBottom = Math.abs(diff) < vbar.getThumb() / 2;
               if(isAtBottom != LogCatPanel.this.mShouldScrollToLatestLog) {
                  LogCatPanel.this.setScrollToLatestLog(isAtBottom);
                  LogCatPanel.this.mScrollLockCheckBox.setSelection(isAtBottom);
               }

            }
         }
      };
      this.startScrollBarMonitor(vbar);
      boolean MAX = true;
      boolean THUMB = true;
      vbar.setValues(9990, 0, 10000, 10, 1, 10);
   }

   private void startScrollBarMonitor(ScrollBar vbar) {
      Object var2 = this.mScrollBarSelectionListenerLock;
      synchronized(this.mScrollBarSelectionListenerLock) {
         if(!this.mScrollBarListenerSet) {
            this.mScrollBarListenerSet = true;
            vbar.addSelectionListener(this.mScrollBarSelectionListener);
         }

      }
   }

   private void stopScrollBarMonitor(ScrollBar vbar) {
      Object var2 = this.mScrollBarSelectionListenerLock;
      synchronized(this.mScrollBarSelectionListenerLock) {
         if(this.mScrollBarListenerSet) {
            this.mScrollBarListenerSet = false;
            vbar.removeSelectionListener(this.mScrollBarSelectionListener);
         }

      }
   }

   private void addRightClickMenu(final Table table) {
      Action filterAction = new Action("Filter similar messages...") {
         public void run() {
            List selectedMessages = LogCatPanel.this.getSelectedLogCatMessages();
            if(selectedMessages.size() == 0) {
               LogCatPanel.this.addNewFilter();
            } else {
               LogCatMessage m = (LogCatMessage)selectedMessages.get(0);
               LogCatPanel.this.addNewFilter(m.getTag(), m.getMessage(), m.getPid(), m.getAppName(), m.getLogLevel());
            }

         }
      };
      Action findAction = new Action("Find...") {
         public void run() {
            LogCatPanel.this.showFindDialog();
         }
      };
      MenuManager mgr = new MenuManager();
      mgr.add(filterAction);
      mgr.add(findAction);
      final Menu menu = mgr.createContextMenu(table);
      table.addListener(35, new Listener() {
         public void handleEvent(Event event) {
            Point pt = table.getDisplay().map((Control)null, table, new Point(event.x, event.y));
            Rectangle clientArea = table.getClientArea();
            boolean header = pt.y > clientArea.y && pt.y < clientArea.y + table.getHeaderHeight();
            table.setMenu(header?null:menu);
         }
      });
   }

   public void recomputeWrapWidth() {
      if(this.mTable != null && !this.mTable.isDisposed()) {
         TableColumn tc = this.mTable.getColumn(this.mTable.getColumnCount() - 1);
         int colWidth = tc.getWidth();
         GC gc = new GC(tc.getParent());
         gc.setFont(this.mFont);
         int avgCharWidth = gc.getFontMetrics().getAverageCharWidth();
         gc.dispose();
         byte MIN_CHARS_PER_LINE = 50;
         this.mWrapWidthInChars = Math.max(colWidth / avgCharWidth, MIN_CHARS_PER_LINE);
         byte OFFSET_AT_END_OF_LINE = 10;
         this.mWrapWidthInChars -= OFFSET_AT_END_OF_LINE;
      }
   }

   private void setScrollToLatestLog(boolean scroll) {
      this.mShouldScrollToLatestLog = scroll;
      if(scroll) {
         this.scrollToLatestLog();
      }

   }

   private String getColPreferenceKey(String field) {
      return "logcat.view.colsize." + field;
   }

   private Font getFontFromPrefStore() {
      FontData fd = PreferenceConverter.getFontData(this.mPrefStore, "logcat.view.font");
      return new Font(Display.getDefault(), fd);
   }

   private Color getColorFromPrefStore(String key) {
      RGB rgb = PreferenceConverter.getColor(this.mPrefStore, key);
      return new Color(Display.getDefault(), rgb);
   }

   private void setupDefaults() {
      byte defaultFilterIndex = 0;
      this.mFiltersTableViewer.getTable().setSelection(defaultFilterIndex);
      this.filterSelectionChanged();
   }

   private void filterSelectionChanged() {
      int idx = this.mFiltersTableViewer.getTable().getSelectionIndex();
      if(idx == -1) {
         idx = 0;
         this.mFiltersTableViewer.getTable().setSelection(idx);
      }

      this.mCurrentSelectedFilterIndex = idx;
      this.resetUnreadCountForAllFilters();
      this.updateFiltersToolBar();
      this.updateAppliedFilters();
   }

   private void resetUnreadCountForAllFilters() {
      Iterator i$ = this.mLogCatFilterData.values().iterator();

      while(i$.hasNext()) {
         LogCatFilterData fd = (LogCatFilterData)i$.next();
         fd.resetUnreadCount();
      }

      this.refreshFiltersTable();
   }

   private void updateFiltersToolBar() {
      boolean en = this.mCurrentSelectedFilterIndex != 0;
      this.mEditFilterToolItem.setEnabled(en);
      this.mDeleteFilterToolItem.setEnabled(en);
   }

   private void updateAppliedFilters() {
      this.mCurrentFilters = this.getFiltersToApply();
      this.reloadLogBuffer();
   }

   private List<LogCatFilter> getFiltersToApply() {
      ArrayList filters = new ArrayList();
      if(this.mCurrentSelectedFilterIndex != 0) {
         filters.add(this.getSelectedSavedFilter());
      }

      filters.addAll(this.getCurrentLiveFilters());
      return filters;
   }

   private List<LogCatFilter> getCurrentLiveFilters() {
      return LogCatFilter.fromString(this.mLiveFilterText.getText(), LogLevel.getByString(this.mLiveFilterLevelCombo.getText()));
   }

   private LogCatFilter getSelectedSavedFilter() {
      return (LogCatFilter)this.mLogCatFilters.get(this.mCurrentSelectedFilterIndex);
   }

   public void setFocus() {
   }

   public void bufferChanged(List<LogCatMessage> addedMessages, List<LogCatMessage> deletedMessages) {
      this.updateUnreadCount(addedMessages);
      this.refreshFiltersTable();
      List var3 = this.mLogBuffer;
      synchronized(this.mLogBuffer) {
         addedMessages = this.applyCurrentFilters(addedMessages);
         deletedMessages = this.applyCurrentFilters(deletedMessages);
         this.mLogBuffer.addAll(addedMessages);
         this.mDeletedLogCount += deletedMessages.size();
      }

      this.refreshLogCatTable();
   }

   private void reloadLogBuffer() {
      this.mTable.removeAll();
      List addedMessages = this.mLogBuffer;
      synchronized(this.mLogBuffer) {
         this.mLogBuffer.clear();
         this.mDeletedLogCount = 0;
      }

      if(this.mReceiver != null && this.mReceiver.getMessages() != null) {
         addedMessages = this.mReceiver.getMessages().getAllMessages();
         List deletedMessages = Collections.emptyList();
         this.bufferChanged(addedMessages, deletedMessages);
      }
   }

   private void updateUnreadCount(List<LogCatMessage> receivedMessages) {
      for(int i = 0; i < this.mLogCatFilters.size(); ++i) {
         if(i != this.mCurrentSelectedFilterIndex) {
            LogCatFilter f = (LogCatFilter)this.mLogCatFilters.get(i);
            LogCatFilterData fd = (LogCatFilterData)this.mLogCatFilterData.get(f);
            fd.updateUnreadCount(receivedMessages);
         }
      }

   }

   private void refreshFiltersTable() {
      Display.getDefault().asyncExec(new Runnable() {
         public void run() {
            if(!LogCatPanel.this.mFiltersTableViewer.getTable().isDisposed()) {
               LogCatPanel.this.mFiltersTableViewer.refresh();
            }
         }
      });
   }

   private void refreshLogCatTable() {
      synchronized(this) {
         if(this.mCurrentRefresher == null) {
            this.mCurrentRefresher = new LogCatPanel.LogCatTableRefresherTask(null);
            Display.getDefault().asyncExec(this.mCurrentRefresher);
         }

      }
   }

   private void scrollToLatestLog() {
      if(!this.mTable.isDisposed()) {
         this.mTable.setTopIndex(this.mTable.getItemCount() - 1);
      }

   }

   private List<String> wrapMessage(String msg, int wrapWidth) {
      if(msg.length() < wrapWidth) {
         return Collections.singletonList(msg);
      } else {
         ArrayList wrappedMessages = new ArrayList();
         int offset = 0;

         String s;
         for(int len = msg.length(); len > 0; wrappedMessages.add(s)) {
            int copylen = Math.min(wrapWidth, len);
            s = msg.substring(offset, offset + copylen);
            offset += copylen;
            len -= copylen;
            if(len > 0) {
               s = s + " ⏎";
            }
         }

         return wrappedMessages;
      }
   }

   private Color getForegroundColor(LogCatMessage m) {
      LogLevel l = m.getLogLevel();
      return l.equals(LogLevel.VERBOSE)?this.mVerboseColor:(l.equals(LogLevel.INFO)?this.mInfoColor:(l.equals(LogLevel.DEBUG)?this.mDebugColor:(l.equals(LogLevel.ERROR)?this.mErrorColor:(l.equals(LogLevel.WARN)?this.mWarnColor:(l.equals(LogLevel.ASSERT)?this.mAssertColor:this.mVerboseColor)))));
   }

   private void initDoubleClickListener() {
      this.mMessageSelectionListeners = new ArrayList(1);
      this.mTable.addSelectionListener(new SelectionAdapter() {
         public void widgetDefaultSelected(SelectionEvent arg0) {
            List selectedMessages = LogCatPanel.this.getSelectedLogCatMessages();
            if(selectedMessages.size() != 0) {
               Iterator i$ = LogCatPanel.this.mMessageSelectionListeners.iterator();

               while(i$.hasNext()) {
                  ILogCatMessageSelectionListener l = (ILogCatMessageSelectionListener)i$.next();
                  l.messageDoubleClicked((LogCatMessage)selectedMessages.get(0));
               }

            }
         }
      });
   }

   public void addLogCatMessageSelectionListener(ILogCatMessageSelectionListener l) {
      this.mMessageSelectionListeners.add(l);
   }

   public void setTableFocusListener(ITableFocusListener listener) {
      this.mTableFocusListener = listener;
      final ITableFocusListener.IFocusedTableActivator activator = new ITableFocusListener.IFocusedTableActivator() {
         public void copy(Clipboard clipboard) {
            LogCatPanel.this.copySelectionToClipboard(clipboard);
         }

         public void selectAll() {
            LogCatPanel.this.mTable.selectAll();
         }
      };
      this.mTable.addFocusListener(new FocusListener() {
         public void focusGained(FocusEvent e) {
            LogCatPanel.this.mTableFocusListener.focusGained(activator);
         }

         public void focusLost(FocusEvent e) {
            LogCatPanel.this.mTableFocusListener.focusLost(activator);
         }
      });
   }

   public void copySelectionToClipboard(Clipboard clipboard) {
      StringBuilder sb = new StringBuilder();
      Iterator i$ = this.getSelectedLogCatMessages().iterator();

      while(i$.hasNext()) {
         LogCatMessage m = (LogCatMessage)i$.next();
         sb.append(m.toString());
         sb.append('\n');
      }

      if(sb.length() > 0) {
         clipboard.setContents(new Object[]{sb.toString()}, new Transfer[]{TextTransfer.getInstance()});
      }

   }

   public void selectAll() {
      this.mTable.selectAll();
   }

   private void dispose() {
      if(this.mFont != null && !this.mFont.isDisposed()) {
         this.mFont.dispose();
      }

      if(this.mVerboseColor != null && !this.mVerboseColor.isDisposed()) {
         this.disposeMessageColors();
      }

   }

   private void disposeMessageColors() {
      this.mVerboseColor.dispose();
      this.mDebugColor.dispose();
      this.mInfoColor.dispose();
      this.mWarnColor.dispose();
      this.mErrorColor.dispose();
      this.mAssertColor.dispose();
   }

   public void showFindDialog() {
      if(this.mFindDialog == null) {
         this.mFindDialog = new FindDialog(Display.getDefault().getActiveShell(), this.mFindTarget);
         this.mFindDialog.open();
         this.mFindDialog = null;
      }
   }

   static {
      FONT_FAMILY = DdmConstants.CURRENT_PLATFORM == 3?"Monaco":"Courier New";
      int h = Display.getDefault().getSystemFont().getFontData()[0].getHeight();
      DEFAULT_LOGCAT_FONTDATA = new FontData(FONT_FAMILY, h, 0);
      WEIGHTS_SHOW_FILTERS = new int[]{15, 85};
      WEIGHTS_LOGCAT_ONLY = new int[]{0, 100};
      VALID_FILTER_REGEX_COLOR = Display.getDefault().getSystemColor(2);
      INVALID_FILTER_REGEX_COLOR = Display.getDefault().getSystemColor(3);
   }

   private class LogcatFindTarget extends AbstractBufferFindTarget {
      private LogcatFindTarget() {
      }

      public void selectAndReveal(int index) {
         LogCatPanel.this.mTable.deselectAll();
         LogCatPanel.this.mTable.select(index);
         LogCatPanel.this.mTable.showSelection();
      }

      public int getItemCount() {
         return LogCatPanel.this.mTable.getItemCount();
      }

      public String getItem(int index) {
         Object data = LogCatPanel.this.mTable.getItem(index).getData();
         return data != null?data.toString():null;
      }

      public int getStartingIndex() {
         int s = LogCatPanel.this.mTable.getSelectionIndex();
         return s != -1?s:this.getItemCount() - 1;
      }

      // $FF: synthetic method
      LogcatFindTarget(Object x1) {
         this();
      }
   }

   private class LogCatTableRefresherTask implements Runnable {
      private LogCatTableRefresherTask() {
      }

      public void run() {
         if(!LogCatPanel.this.mTable.isDisposed()) {
            LogCatPanel topIndex = LogCatPanel.this;
            synchronized(LogCatPanel.this) {
               LogCatPanel.this.mCurrentRefresher = null;
            }

            int var14 = LogCatPanel.this.mTable.getTopIndex();
            LogCatPanel.this.mTable.setRedraw(false);
            LogCatPanel.this.stopScrollBarMonitor(LogCatPanel.this.mTable.getVerticalBar());
            ArrayList newMessages;
            int deletedMessageCount;
            synchronized(LogCatPanel.this.mLogBuffer) {
               newMessages = new ArrayList(LogCatPanel.this.mLogBuffer);
               LogCatPanel.this.mLogBuffer.clear();
               deletedMessageCount = LogCatPanel.this.mDeletedLogCount;
               LogCatPanel.this.mDeletedLogCount = 0;
               LogCatPanel.this.mFindTarget.scrollBy(deletedMessageCount);
            }

            int originalItemCount = LogCatPanel.this.mTable.getItemCount();
            deletedMessageCount -= this.removeFromTable(LogCatPanel.this.mTable, deletedMessageCount);
            int deletedItemCount = originalItemCount - LogCatPanel.this.mTable.getItemCount();
            int index;
            if(deletedMessageCount > 0) {
               assert deletedMessageCount < newMessages.size();

               for(index = 0; index < deletedMessageCount; ++index) {
                  newMessages.remove(0);
               }
            }

            Iterator var15 = newMessages.iterator();

            while(var15.hasNext()) {
               LogCatMessage m = (LogCatMessage)var15.next();
               List wrappedMessageList = LogCatPanel.this.wrapMessage(m.getMessage(), LogCatPanel.this.mWrapWidthInChars);
               Color c = LogCatPanel.this.getForegroundColor(m);

               for(int i = 0; i < wrappedMessageList.size(); ++i) {
                  TableItem item = new TableItem(LogCatPanel.this.mTable, 0);
                  if(i == 0) {
                     item.setData(m);
                     item.setText(new String[]{Character.toString(m.getLogLevel().getPriorityLetter()), m.getTime(), m.getPid(), m.getTid(), m.getAppName(), m.getTag(), (String)wrappedMessageList.get(i)});
                  } else {
                     item.setText(new String[]{"", "", "", "", "", "", (String)wrappedMessageList.get(i)});
                  }

                  item.setForeground(c);
                  item.setFont(LogCatPanel.this.mFont);
               }
            }

            if(LogCatPanel.this.mShouldScrollToLatestLog) {
               LogCatPanel.this.scrollToLatestLog();
            } else {
               index = Math.max(var14 - deletedItemCount, 0);
               LogCatPanel.this.mTable.setTopIndex(index);
            }

            LogCatPanel.this.mTable.setRedraw(true);
            Display.getDefault().asyncExec(new Runnable() {
               public void run() {
                  if(!LogCatPanel.this.mTable.isDisposed()) {
                     LogCatPanel.this.startScrollBarMonitor(LogCatPanel.this.mTable.getVerticalBar());
                  }

               }
            });
         }
      }

      private int removeFromTable(Table table, int msgCount) {
         int deletedMessageCount = 0;

         int lastItemToDelete;
         for(lastItemToDelete = 0; deletedMessageCount < msgCount && lastItemToDelete < table.getItemCount(); ++lastItemToDelete) {
            TableItem item = table.getItem(lastItemToDelete);
            if(item.getData() != null) {
               ++deletedMessageCount;
            }
         }

         if(lastItemToDelete < table.getItemCount() && table.getItem(lastItemToDelete).getData() == null) {
            ++lastItemToDelete;
         }

         table.remove(0, lastItemToDelete - 1);
         return deletedMessageCount;
      }

      // $FF: synthetic method
      LogCatTableRefresherTask(Object x1) {
         this();
      }
   }
}
