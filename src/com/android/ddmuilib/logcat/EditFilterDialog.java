package com.android.ddmuilib.logcat;

import com.android.ddmuilib.ImageLoader;
import com.android.ddmuilib.logcat.LogFilter;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class EditFilterDialog extends Dialog {
   private static final int DLG_WIDTH = 400;
   private static final int DLG_HEIGHT = 260;
   private static final String IMAGE_WARNING = "warning.png";
   private static final String IMAGE_EMPTY = "empty.png";
   private Shell mParent;
   private Shell mShell;
   private boolean mOk;
   private LogFilter mFilter;
   private String mName;
   private String mTag;
   private String mPid;
   private int mLogLevel;
   private Button mOkButton;
   private Label mNameWarning;
   private Label mTagWarning;
   private Label mPidWarning;

   public EditFilterDialog(Shell parent) {
      super(parent, 67680);
      this.mOk = false;
   }

   public EditFilterDialog(Shell shell, LogFilter filter) {
      this(shell);
      this.mFilter = filter;
   }

   public boolean open() {
      this.createUI();
      if(this.mParent != null && this.mShell != null) {
         this.mShell.setMinimumSize(400, 260);
         Rectangle r = this.mParent.getBounds();
         int cx = r.x + r.width / 2;
         int x = cx - 200;
         int cy = r.y + r.height / 2;
         int y = cy - 130;
         this.mShell.setBounds(x, y, 400, 260);
         this.mShell.open();
         Display display = this.mParent.getDisplay();

         while(!this.mShell.isDisposed()) {
            if(!display.readAndDispatch()) {
               display.sleep();
            }
         }

         if(this.mOk) {
            if(this.mFilter == null) {
               this.mFilter = new LogFilter(this.mName);
            }

            this.mFilter.setTagMode(this.mTag);
            if(this.mPid != null && this.mPid.length() > 0) {
               this.mFilter.setPidMode(Integer.parseInt(this.mPid));
            } else {
               this.mFilter.setPidMode(-1);
            }

            this.mFilter.setLogLevel(this.getLogLevel(this.mLogLevel));
         }

         return this.mOk;
      } else {
         return false;
      }
   }

   public LogFilter getFilter() {
      return this.mFilter;
   }

   private void createUI() {
      this.mParent = this.getParent();
      this.mShell = new Shell(this.mParent, this.getStyle());
      this.mShell.setText("Log Filter");
      this.mShell.setLayout(new GridLayout(1, false));
      this.mShell.addListener(21, new Listener() {
         public void handleEvent(Event event) {
         }
      });
      Composite nameComposite = new Composite(this.mShell, 0);
      nameComposite.setLayoutData(new GridData(1808));
      nameComposite.setLayout(new GridLayout(3, false));
      Label l = new Label(nameComposite, 0);
      l.setText("Filter Name:");
      final Text filterNameText = new Text(nameComposite, 2052);
      if(this.mFilter != null) {
         this.mName = this.mFilter.getName();
         if(this.mName != null) {
            filterNameText.setText(this.mName);
         }
      }

      filterNameText.setLayoutData(new GridData(768));
      filterNameText.addModifyListener(new ModifyListener() {
         public void modifyText(ModifyEvent e) {
            EditFilterDialog.this.mName = filterNameText.getText().trim();
            EditFilterDialog.this.validate();
         }
      });
      this.mNameWarning = new Label(nameComposite, 0);
      this.mNameWarning.setImage(ImageLoader.getDdmUiLibLoader().loadImage("empty.png", this.mShell.getDisplay()));
      l = new Label(this.mShell, 258);
      l.setLayoutData(new GridData(768));
      Composite main = new Composite(this.mShell, 0);
      main.setLayoutData(new GridData(1808));
      main.setLayout(new GridLayout(3, false));
      l = new Label(main, 0);
      l.setText("by Log Tag:");
      final Text tagText = new Text(main, 2052);
      if(this.mFilter != null) {
         this.mTag = this.mFilter.getTagFilter();
         if(this.mTag != null) {
            tagText.setText(this.mTag);
         }
      }

      tagText.setLayoutData(new GridData(768));
      tagText.addModifyListener(new ModifyListener() {
         public void modifyText(ModifyEvent e) {
            EditFilterDialog.this.mTag = tagText.getText().trim();
            EditFilterDialog.this.validate();
         }
      });
      this.mTagWarning = new Label(main, 0);
      this.mTagWarning.setImage(ImageLoader.getDdmUiLibLoader().loadImage("empty.png", this.mShell.getDisplay()));
      l = new Label(main, 0);
      l.setText("by pid:");
      final Text pidText = new Text(main, 2052);
      if(this.mFilter != null) {
         if(this.mFilter.getPidFilter() != -1) {
            this.mPid = Integer.toString(this.mFilter.getPidFilter());
         } else {
            this.mPid = "";
         }

         pidText.setText(this.mPid);
      }

      pidText.setLayoutData(new GridData(768));
      pidText.addModifyListener(new ModifyListener() {
         public void modifyText(ModifyEvent e) {
            EditFilterDialog.this.mPid = pidText.getText().trim();
            EditFilterDialog.this.validate();
         }
      });
      this.mPidWarning = new Label(main, 0);
      this.mPidWarning.setImage(ImageLoader.getDdmUiLibLoader().loadImage("empty.png", this.mShell.getDisplay()));
      l = new Label(main, 0);
      l.setText("by Log level:");
      final Combo logCombo = new Combo(main, 12);
      GridData gd = new GridData(768);
      gd.horizontalSpan = 2;
      logCombo.setLayoutData(gd);
      logCombo.add("<none>");
      logCombo.add("Error");
      logCombo.add("Warning");
      logCombo.add("Info");
      logCombo.add("Debug");
      logCombo.add("Verbose");
      if(this.mFilter != null) {
         this.mLogLevel = this.getComboIndex(this.mFilter.getLogLevel());
         logCombo.select(this.mLogLevel);
      } else {
         logCombo.select(0);
      }

      logCombo.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            EditFilterDialog.this.mLogLevel = logCombo.getSelectionIndex();
            EditFilterDialog.this.validate();
         }
      });
      l = new Label(this.mShell, 258);
      l.setLayoutData(new GridData(768));
      Composite bottomComp = new Composite(this.mShell, 0);
      bottomComp.setLayoutData(new GridData(64));
      bottomComp.setLayout(new GridLayout(2, true));
      this.mOkButton = new Button(bottomComp, 0);
      this.mOkButton.setText("OK");
      this.mOkButton.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            EditFilterDialog.this.mOk = true;
            EditFilterDialog.this.mShell.close();
         }
      });
      this.mOkButton.setEnabled(false);
      this.mShell.setDefaultButton(this.mOkButton);
      Button cancelButton = new Button(bottomComp, 0);
      cancelButton.setText("Cancel");
      cancelButton.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            EditFilterDialog.this.mShell.close();
         }
      });
      this.validate();
   }

   protected int getLogLevel(int index) {
      return index == 0?-1:7 - index;
   }

   private int getComboIndex(int logLevel) {
      return logLevel == -1?0:7 - logLevel;
   }

   private void validate() {
      boolean result = true;
      if(this.mPid != null) {
         if(!this.mPid.matches("[0-9]*")) {
            this.mPidWarning.setImage(ImageLoader.getDdmUiLibLoader().loadImage("warning.png", this.mShell.getDisplay()));
            this.mPidWarning.setToolTipText("PID must be a number");
            result = false;
         } else {
            this.mPidWarning.setImage(ImageLoader.getDdmUiLibLoader().loadImage("empty.png", this.mShell.getDisplay()));
            this.mPidWarning.setToolTipText((String)null);
         }
      }

      if(this.mTag != null) {
         if(this.mTag.matches(".*[:|].*")) {
            this.mTagWarning.setImage(ImageLoader.getDdmUiLibLoader().loadImage("warning.png", this.mShell.getDisplay()));
            this.mTagWarning.setToolTipText("Tag cannot contain | or :");
            result = false;
         } else {
            this.mTagWarning.setImage(ImageLoader.getDdmUiLibLoader().loadImage("empty.png", this.mShell.getDisplay()));
            this.mTagWarning.setToolTipText((String)null);
         }
      }

      if(this.mName != null && this.mName.length() > 0) {
         if(this.mName.matches(".*[:|].*")) {
            this.mNameWarning.setImage(ImageLoader.getDdmUiLibLoader().loadImage("warning.png", this.mShell.getDisplay()));
            this.mNameWarning.setToolTipText("Name cannot contain | or :");
            result = false;
         } else {
            this.mNameWarning.setImage(ImageLoader.getDdmUiLibLoader().loadImage("empty.png", this.mShell.getDisplay()));
            this.mNameWarning.setToolTipText((String)null);
         }
      } else {
         result = false;
      }

      this.mOkButton.setEnabled(result);
   }
}
