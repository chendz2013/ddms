package com.android.ddmuilib.logcat;

import com.android.ddmlib.logcat.LogCatFilter;
import com.android.ddmlib.logcat.LogCatMessage;
import java.util.Iterator;
import java.util.List;

public class LogCatFilterData {
   private final LogCatFilter mFilter;
   private int mUnreadCount;
   private boolean mTransient;

   public LogCatFilterData(LogCatFilter f) {
      this.mFilter = f;
      this.mTransient = false;
   }

   public void updateUnreadCount(List<LogCatMessage> newMessages) {
      Iterator i$ = newMessages.iterator();

      while(i$.hasNext()) {
         LogCatMessage m = (LogCatMessage)i$.next();
         if(this.mFilter.matches(m)) {
            ++this.mUnreadCount;
         }
      }

   }

   public void resetUnreadCount() {
      this.mUnreadCount = 0;
   }

   public int getUnreadCount() {
      return this.mUnreadCount;
   }

   public void setTransient() {
      this.mTransient = true;
   }

   public boolean isTransient() {
      return this.mTransient;
   }
}
