package com.android.ddmuilib;

import com.android.ddmlib.Client;
import com.android.ddmlib.ClientData;
import com.android.ddmuilib.TablePanel;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

/**
 * 基本信息
 */
public class InfoPanel extends TablePanel {
   private Table mTable;
   private TableColumn mCol2;
   private static final String[] mLabels = new String[]{"DDM-感知?", "App描述:", "VM版本:", "进程ID:", "支持Profiling:", "支持HPROF:", "ABI版本:", "JVM设置:"};
   private static final int ENT_DDM_AWARE = 0;
   private static final int ENT_APP_DESCR = 1;
   private static final int ENT_VM_VERSION = 2;
   private static final int ENT_PROCESS_ID = 3;
   private static final int ENT_SUPPORTS_PROFILING = 4;
   private static final int ENT_SUPPORTS_HPROF = 5;
   private static final int ENT_ABI_FLAVOR = 6;
   private static final int ENT_JVM_FLAGS = 7;

   protected Control createControl(Composite parent) {
      this.mTable = new Table(parent, 65538);
      this.mTable.setHeaderVisible(false);
      this.mTable.setLinesVisible(false);
      TableColumn col1 = new TableColumn(this.mTable, 131072);
      col1.setText("名称");
      this.mCol2 = new TableColumn(this.mTable, 16384);
      this.mCol2.setText("PlaceHolderContentForWidth");

      for(int i = 0; i < mLabels.length; ++i) {
         TableItem item = new TableItem(this.mTable, 0);
         item.setText(0, mLabels[i]);
         item.setText(1, "-");
      }

      col1.pack();
      this.mCol2.pack();
      return this.mTable;
   }

   public void setFocus() {
      this.mTable.setFocus();
   }

   public void clientChanged(Client client, int changeMask) {
      if(client == this.getCurrentClient() && (changeMask & 7) == 7) {
         if(this.mTable.isDisposed()) {
            return;
         }

         this.mTable.getDisplay().asyncExec(new Runnable() {
            public void run() {
               InfoPanel.this.clientSelected();
            }
         });
      }

   }

   public void deviceSelected() {
   }

   public void clientSelected() {
      if(!this.mTable.isDisposed()) {
         Client client = this.getCurrentClient();
         if(client == null) {
            for(int item = 0; item < mLabels.length; ++item) {
               TableItem clientDescription = this.mTable.getItem(item);
               clientDescription.setText(1, "-");
            }
         } else {
            ClientData cd = client.getClientData();
            String vmIdentifier;
            String isDdmAware;
            String pid;
            String var12;
            synchronized(cd) {
               var12 = cd.getClientDescription() != null?cd.getClientDescription():"?";
               vmIdentifier = cd.getVmIdentifier() != null?cd.getVmIdentifier():"?";
               isDdmAware = cd.isDdmAware()?"yes":"no";
               pid = cd.getPid() != 0?String.valueOf(cd.getPid()):"?";
            }

            TableItem var11 = this.mTable.getItem(1);
            var11.setText(1, var12);
            var11 = this.mTable.getItem(2);
            var11.setText(1, vmIdentifier);
            var11 = this.mTable.getItem(0);
            var11.setText(1, isDdmAware);
            var11 = this.mTable.getItem(3);
            var11.setText(1, pid);
            var11 = this.mTable.getItem(4);
            if(cd.hasFeature("method-trace-profiling-streaming")) {
               var11.setText(1, "Yes");
            } else if(cd.hasFeature("method-trace-profiling")) {
               var11.setText(1, "Yes (Application must be able to write on the SD Card)");
            } else {
               var11.setText(1, "No");
            }

            var11 = this.mTable.getItem(5);
            if(cd.hasFeature("hprof-heap-dump-streaming")) {
               var11.setText(1, "Yes");
            } else if(cd.hasFeature("hprof-heap-dump")) {
               var11.setText(1, "Yes (Application must be able to write on the SD Card)");
            } else {
               var11.setText(1, "No");
            }

            var11 = this.mTable.getItem(6);
            String abi = cd.getAbi();
            var11.setText(1, abi == null?"":abi);
            var11 = this.mTable.getItem(7);
            String jvmFlags = cd.getJvmFlags();
            var11.setText(1, jvmFlags == null?"":jvmFlags);
         }

         this.mCol2.pack();
      }
   }

   protected void setTableFocusListener() {
      this.addTableToFocusListener(this.mTable);
   }
}
