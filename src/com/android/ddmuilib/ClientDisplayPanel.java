package com.android.ddmuilib;

import com.android.ddmlib.AndroidDebugBridge;
import com.android.ddmlib.AndroidDebugBridge.IClientChangeListener;
import com.android.ddmuilib.SelectionDependentPanel;

public abstract class ClientDisplayPanel extends SelectionDependentPanel implements IClientChangeListener {
   protected void postCreation() {
      AndroidDebugBridge.addClientChangeListener(this);
   }

   public void dispose() {
      AndroidDebugBridge.removeClientChangeListener(this);
   }
}
