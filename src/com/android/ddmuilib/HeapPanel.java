package com.android.ddmuilib;

import com.android.ddmlib.Client;
import com.android.ddmlib.ClientData;
import com.android.ddmlib.Log;
import com.android.ddmlib.ClientData.HeapInfo;
import com.android.ddmlib.HeapSegment.HeapSegmentElement;
import com.android.ddmuilib.BaseHeapPanel;
import com.android.ddmuilib.DdmUiPreferences;
import com.android.ddmuilib.ImageLoader;
import com.android.ddmuilib.TableHelper;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.PaletteData;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.labels.CategoryToolTipGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.CategoryItemRenderer;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.experimental.chart.swt.ChartComposite;
import org.jfree.experimental.swt.SWTUtils;

/**
 * 堆Panel
 */
public final class HeapPanel extends BaseHeapPanel {
   private static final String PREFS_STATS_COL_TYPE = "heapPanel.col0";
   private static final String PREFS_STATS_COL_COUNT = "heapPanel.col1";
   private static final String PREFS_STATS_COL_SIZE = "heapPanel.col2";
   private static final String PREFS_STATS_COL_SMALLEST = "heapPanel.col3";
   private static final String PREFS_STATS_COL_LARGEST = "heapPanel.col4";
   private static final String PREFS_STATS_COL_MEDIAN = "heapPanel.col5";
   private static final String PREFS_STATS_COL_AVERAGE = "heapPanel.col6";
   private static final int NOT_SELECTED = 0;
   private static final int NOT_ENABLED = 1;
   private static final int ENABLED = 2;
   private static final int NUM_PALETTE_ENTRIES = 10;
   private static final String[] mMapLegend = new String[10];
   private static final PaletteData mMapPalette = createPalette();
   private static final boolean DISPLAY_HEAP_BITMAP = false;
   private static final boolean DISPLAY_HILBERT_BITMAP = false;
   private static final int PLACEHOLDER_HILBERT_SIZE = 200;
   private static final int PLACEHOLDER_LINEAR_V_SIZE = 100;
   private static final int PLACEHOLDER_LINEAR_H_SIZE = 300;
   private static final int[] ZOOMS = new int[]{100, 50, 25};
   private static final NumberFormat sByteFormatter = NumberFormat.getInstance();
   private static final NumberFormat sLargeByteFormatter = NumberFormat.getInstance();
   private static final NumberFormat sCountFormatter = NumberFormat.getInstance();
   private Display mDisplay;
   private Composite mTop;
   private Label mUpdateStatus;
   private Table mHeapSummary;
   private Combo mDisplayMode;
   private Composite mDisplayBase;
   private StackLayout mDisplayStack;
   private Composite mStatisticsBase;
   private Table mStatisticsTable;
   private JFreeChart mChart;
   private ChartComposite mChartComposite;
   private Button mGcButton;
   private DefaultCategoryDataset mAllocCountDataSet;
   private Composite mLinearBase;
   private Label mLinearHeapImage;
   private Composite mHilbertBase;
   private Label mHilbertHeapImage;
   private Group mLegend;
   private Combo mZoom;
   private Image mHilbertImage;
   private Image mLinearImage;
   private Composite[] mLayout;
   private static final int HILBERT_DIR_N = 0;
   private static final int HILBERT_DIR_S = 1;
   private static final int HILBERT_DIR_E = 2;
   private static final int HILBERT_DIR_W = 3;

   private static PaletteData createPalette() {
      RGB[] colors = new RGB[10];
      colors[0] = new RGB(192, 192, 192);
      mMapLegend[0] = "(heap expansion area)";
      colors[1] = new RGB(0, 0, 0);
      mMapLegend[1] = "free";
      colors[2] = new RGB(0, 0, 255);
      mMapLegend[2] = "data object";
      colors[3] = new RGB(0, 255, 0);
      mMapLegend[3] = "class object";
      colors[4] = new RGB(255, 0, 0);
      mMapLegend[4] = "1-byte数组 (如byte[], boolean[])";
      colors[5] = new RGB(255, 128, 0);
      mMapLegend[5] = "2-byte数组 (如short[], char[])";
      colors[6] = new RGB(255, 255, 0);
      mMapLegend[6] = "4-byte数组 (如object[], int[], float[])";
      colors[7] = new RGB(255, 128, 128);
      mMapLegend[7] = "8-byte数组 (如long[], double[])";
      colors[8] = new RGB(255, 0, 255);
      mMapLegend[8] = "unknown object";
      colors[9] = new RGB(64, 64, 64);
      mMapLegend[9] = "non-Java object";
      return new PaletteData(colors);
   }

   public void clientChanged(Client client, int changeMask) {
      if(client == this.getCurrentClient() && ((changeMask & 32) == 32 || (changeMask & 64) == 64)) {
         try {
            this.mTop.getDisplay().asyncExec(new Runnable() {
               public void run() {
                  HeapPanel.this.clientSelected();
               }
            });
         } catch (SWTException var4) {
            ;
         }
      }

   }

   public void deviceSelected() {
   }

   public void clientSelected() {
      if(!this.mTop.isDisposed()) {
         Client client = this.getCurrentClient();
         Log.d("ddms", "HeapPanel: changed " + client);
         if(client != null) {
            ClientData cd = client.getClientData();
            if(client.isHeapUpdateEnabled()) {
               this.mGcButton.setEnabled(true);
               this.mDisplayMode.setEnabled(true);
               this.setUpdateStatus(2);
            } else {
               this.setUpdateStatus(1);
               this.mGcButton.setEnabled(false);
               this.mDisplayMode.setEnabled(false);
            }

            this.fillSummaryTable(cd);
            int mode = this.mDisplayMode.getSelectionIndex();
            if(mode == 0) {
               this.fillDetailedTable(client, false);
            }
         } else {
            this.mGcButton.setEnabled(false);
            this.mDisplayMode.setEnabled(false);
            this.fillSummaryTable((ClientData)null);
            this.fillDetailedTable((Client)null, true);
            this.setUpdateStatus(0);
         }

         this.mDisplayBase.layout();
      }
   }

   protected Control createControl(Composite parent) {
      this.mDisplay = parent.getDisplay();
      this.mTop = new Composite(parent, 0);
      this.mTop.setLayout(new GridLayout(1, false));
      this.mTop.setLayoutData(new GridData(1808));
      this.mUpdateStatus = new Label(this.mTop, 0);
      this.setUpdateStatus(0);
      Composite summarySection = new Composite(this.mTop, 0);
      GridLayout gl;
      summarySection.setLayout(gl = new GridLayout(2, false));
      gl.marginHeight = gl.marginWidth = 0;
      this.mHeapSummary = this.createSummaryTable(summarySection);
      this.mGcButton = new Button(summarySection, 8);
      this.mGcButton.setText("执行GC");
      this.mGcButton.setEnabled(false);
      this.mGcButton.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            Client client = HeapPanel.this.getCurrentClient();
            if(client != null) {//执行GC
               client.executeGarbageCollector();
            }

         }
      });
      Composite comboSection = new Composite(this.mTop, 0);
      gl = new GridLayout(2, false);
      gl.marginHeight = gl.marginWidth = 0;
      comboSection.setLayout(gl);
      Label displayLabel = new Label(comboSection, 0);
      displayLabel.setText("显示: ");
      this.mDisplayMode = new Combo(comboSection, 8);
      this.mDisplayMode.setEnabled(false);
      this.mDisplayMode.add("统计");
      this.mDisplayBase = new Composite(this.mTop, 0);
      this.mDisplayBase.setLayoutData(new GridData(1808));
      this.mDisplayStack = new StackLayout();
      this.mDisplayBase.setLayout(this.mDisplayStack);
      this.mStatisticsBase = new Composite(this.mDisplayBase, 0);
      this.mStatisticsBase.setLayout(gl = new GridLayout(1, false));
      gl.marginHeight = gl.marginWidth = 0;
      this.mDisplayStack.topControl = this.mStatisticsBase;
      this.mStatisticsTable = this.createDetailedTable(this.mStatisticsBase);//详细表
      this.mStatisticsTable.setLayoutData(new GridData(1808));
      this.createChart();//图表
      this.mLinearBase = new Composite(this.mDisplayBase, 0);
      gl = new GridLayout(1, false);
      gl.marginHeight = gl.marginWidth = 0;
      this.mLinearBase.setLayout(gl);
      this.mLinearHeapImage = new Label(this.mLinearBase, 0);
      this.mLinearHeapImage.setLayoutData(new GridData());
      this.mLinearHeapImage.setImage(ImageLoader.createPlaceHolderArt(this.mDisplay, 300, 100, this.mDisplay.getSystemColor(9)));
      Composite bottomSection = new Composite(this.mLinearBase, 0);
      gl = new GridLayout(1, false);
      gl.marginHeight = gl.marginWidth = 0;
      bottomSection.setLayout(gl);
      this.createLegend(bottomSection);
      this.mHilbertBase = new Composite(this.mDisplayBase, 0);
      gl = new GridLayout(2, false);
      gl.marginHeight = gl.marginWidth = 0;
      this.mHilbertBase.setLayout(gl);
      this.mHilbertBase.pack();
      this.mLayout = new Composite[]{this.mStatisticsBase, this.mLinearBase, this.mHilbertBase};
      this.mDisplayMode.select(0);
      this.mDisplayMode.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            int index = HeapPanel.this.mDisplayMode.getSelectionIndex();
            Client client = HeapPanel.this.getCurrentClient();
            if(client != null) {
               if(index == 0) {
                  HeapPanel.this.fillDetailedTable(client, true);
               } else {
                  HeapPanel.this.renderHeapData(client.getClientData(), index - 1, true);
               }
            }

            HeapPanel.this.mDisplayStack.topControl = HeapPanel.this.mLayout[index];
            HeapPanel.this.mDisplayBase.layout();
         }
      });
      this.mDisplayBase.layout();
      return this.mTop;
   }

   public void setFocus() {
      this.mHeapSummary.setFocus();
   }

   /**
    * 总览表
    * @param base
    * @return
     */
   private Table createSummaryTable(Composite base) {
      Table tab = new Table(base, 65540);
      tab.setHeaderVisible(true);
      tab.setLinesVisible(true);
      TableColumn col = new TableColumn(tab, 131072);
      col.setText("ID");
      col.pack();
      col = new TableColumn(tab, 131072);
      col.setText("000.000WW");
      col.pack();
      col.setText("堆大小");
      col = new TableColumn(tab, 131072);
      col.setText("000.000WW");
      col.pack();
      col.setText("已分配");
      col = new TableColumn(tab, 131072);
      col.setText("000.000WW");
      col.pack();
      col.setText("空闲");
      col = new TableColumn(tab, 131072);
      col.setText("000.00%");
      col.pack();
      col.setText("已使用(%)");
      col = new TableColumn(tab, 131072);
      col.setText("000,000,000");
      col.pack();
      col.setText("#对象个数");
      TableItem item = new TableItem(tab, 0);
      item.setText("");
      return tab;
   }

   /**
    * 详细表
    * @param base
    * @return
     */
   private Table createDetailedTable(Composite base) {
      IPreferenceStore store = DdmUiPreferences.getStore();
      Table tab = new Table(base, 65540);
      tab.setHeaderVisible(true);
      tab.setLinesVisible(true);
      TableHelper.createTableColumn(tab, "类型", 16384, "4-byte array (object[], int[], float[])", "heapPanel.col0", store);
      TableHelper.createTableColumn(tab, "个数", 131072, "00,000", "heapPanel.col1", store);
      TableHelper.createTableColumn(tab, "总大小", 131072, "000.000 WW", "heapPanel.col2", store);
      TableHelper.createTableColumn(tab, "最小的", 131072, "000.000 WW", "heapPanel.col3", store);
      TableHelper.createTableColumn(tab, "最大的", 131072, "000.000 WW", "heapPanel.col4", store);
      TableHelper.createTableColumn(tab, "中等的", 131072, "000.000 WW", "heapPanel.col5", store);
      TableHelper.createTableColumn(tab, "平均", 131072, "000.000 WW", "heapPanel.col6", store);
      tab.addSelectionListener(new SelectionAdapter() {//显示图表
         public void widgetSelected(SelectionEvent e) {
            Client client = HeapPanel.this.getCurrentClient();
            if(client != null) {
               int index = HeapPanel.this.mStatisticsTable.getSelectionIndex();
               TableItem item = HeapPanel.this.mStatisticsTable.getItem(index);
               if(item != null) {
                  Map heapMap = client.getClientData().getVmHeapData().getProcessedHeapMap();
                  ArrayList list = (ArrayList)heapMap.get(item.getData());
                  if(list != null) {
                     HeapPanel.this.showChart(list);
                  }
               }
            }

         }
      });
      return tab;
   }

   private void createChart() {
      this.mAllocCountDataSet = new DefaultCategoryDataset();
      this.mChart = ChartFactory.createBarChart((String)null, "大小", "个数", this.mAllocCountDataSet, PlotOrientation.VERTICAL, false, true, false);
      Font f = this.mStatisticsBase.getFont();
      FontData[] fData = f.getFontData();
      FontData firstFontData = fData[0];
      java.awt.Font awtFont = SWTUtils.toAwtFont(this.mStatisticsBase.getDisplay(), firstFontData, true);
      this.mChart.setTitle(new TextTitle("分配个数表(size-count)", awtFont));
      Plot plot = this.mChart.getPlot();
      if(plot instanceof CategoryPlot) {
         CategoryPlot categoryPlot = (CategoryPlot)plot;
         CategoryAxis domainAxis = categoryPlot.getDomainAxis();
         domainAxis.setCategoryLabelPositions(CategoryLabelPositions.DOWN_90);
         CategoryItemRenderer renderer = categoryPlot.getRenderer();
         renderer.setBaseToolTipGenerator(new CategoryToolTipGenerator() {
            public String generateToolTip(CategoryDataset dataset, int row, int column) {
               HeapPanel.ByteLong columnKey = (HeapPanel.ByteLong)dataset.getColumnKey(column);
               String rowKey = (String)dataset.getRowKey(row);
               Number value = dataset.getValue(rowKey, columnKey);
               return String.format("%1$d %2$s of %3$d bytes", new Object[]{Integer.valueOf(value.intValue()), rowKey, Long.valueOf(columnKey.getValue())});
            }
         });
      }

      this.mChartComposite = new ChartComposite(this.mStatisticsBase, 2048, this.mChart, 680, 420, 300, 200, 3000, 3000, true, true, true, true, false, true);
      this.mChartComposite.setLayoutData(new GridData(1808));
   }

   private static String prettyByteCount(long bytes) {
      double fracBytes = (double)bytes;
      String units = " B";
      if(fracBytes < 1024.0D) {
         return sByteFormatter.format(fracBytes) + units;
      } else {
         fracBytes /= 1024.0D;
         units = " KB";
         if(fracBytes >= 1024.0D) {
            fracBytes /= 1024.0D;
            units = " MB";
         }

         if(fracBytes >= 1024.0D) {
            fracBytes /= 1024.0D;
            units = " GB";
         }

         return sLargeByteFormatter.format(fracBytes) + units;
      }
   }

   private static String approximateByteCount(long bytes) {
      double fracBytes = (double)bytes;
      String units = "";
      if(fracBytes >= 1024.0D) {
         fracBytes /= 1024.0D;
         units = "K";
      }

      if(fracBytes >= 1024.0D) {
         fracBytes /= 1024.0D;
         units = "M";
      }

      if(fracBytes >= 1024.0D) {
         fracBytes /= 1024.0D;
         units = "G";
      }

      return sByteFormatter.format(fracBytes) + units;
   }

   private static String addCommasToNumber(long num) {
      return sCountFormatter.format(num);
   }

   private static String fractionalPercent(long num, long denom) {
      double val = (double)num / (double)denom;
      val *= 100.0D;
      NumberFormat nf = NumberFormat.getInstance();
      nf.setMinimumFractionDigits(2);
      nf.setMaximumFractionDigits(2);
      return nf.format(val) + "%";
   }

   /**
    * 总表的数据
    * @param cd
     */
   private void fillSummaryTable(ClientData cd) {
      if(!this.mHeapSummary.isDisposed()) {
         this.mHeapSummary.setRedraw(false);
         this.mHeapSummary.removeAll();
         int numRows = 0;
         if(cd != null) {
            synchronized(cd) {
               Iterator iter = cd.getVmHeapIds();

               while(iter.hasNext()) {
                  ++numRows;
                  Integer id = (Integer)iter.next();
                  //读取堆信息
                  HeapInfo info = cd.getVmHeapInfo(id.intValue());
                  if(info != null) {
                     TableItem item1 = new TableItem(this.mHeapSummary, 0);
                     item1.setText(0, id.toString());
                     item1.setText(1, prettyByteCount(info.sizeInBytes));
                     item1.setText(2, prettyByteCount(info.bytesAllocated));
                     item1.setText(3, prettyByteCount(info.sizeInBytes - info.bytesAllocated));
                     item1.setText(4, fractionalPercent(info.bytesAllocated, info.sizeInBytes));
                     item1.setText(5, addCommasToNumber(info.objectsAllocated));
                  }
               }
            }
         }

         if(numRows == 0) {
            TableItem item = new TableItem(this.mHeapSummary, 0);
            item.setText("");
         }

         this.mHeapSummary.pack();
         this.mHeapSummary.setRedraw(true);
      }
   }

   /**
    * 详细表
    * @param client
    * @param forceRedraw
     */
   private void fillDetailedTable(Client client, boolean forceRedraw) {
      if(client != null && client.isHeapUpdateEnabled()) {
         ClientData cd = client.getClientData();
         Map heapMap;
         synchronized(cd) {
            if(!this.serializeHeapData(cd.getVmHeapData()) && !forceRedraw) {
               return;
            }

            heapMap = cd.getVmHeapData().getProcessedHeapMap();
         }
         //读取选中的idx
         int index = this.mStatisticsTable.getSelectionIndex();
         Integer selectedKey = null;
         if(index != -1) {
            selectedKey = (Integer)this.mStatisticsTable.getItem(index).getData();
         }

         this.mStatisticsTable.setRedraw(false);
         this.mStatisticsTable.removeAll();
         if(heapMap != null) {
            int selectedIndex = -1;
            ArrayList selectedList = null;
            Set keys = heapMap.keySet();
            int iter = 0;
            Iterator i$ = keys.iterator();

            while(true) {
               ArrayList list;
               TableItem item;
               int count;
               do {
                  if(!i$.hasNext()) {
                     this.mStatisticsTable.setRedraw(true);
                     if(selectedIndex != -1) {
                        this.mStatisticsTable.setSelection(selectedIndex);
                        this.showChart(selectedList);
                     } else {
                        this.showChart((ArrayList)null);
                     }

                     return;
                  }

                  Integer key = (Integer)i$.next();
                  list = (ArrayList)heapMap.get(key);
                  if(key.equals(selectedKey)) {
                     selectedIndex = iter;
                     selectedList = list;
                  }

                  ++iter;
                  item = new TableItem(this.mStatisticsTable, 0);
                  item.setData(key);
                  item.setText(0, mMapLegend[key.intValue()]);//类型列表
                  count = list.size();
                  item.setText(1, addCommasToNumber((long)count));//个数
               } while(count <= 0);
               //最小和最大的
               item.setText(3, prettyByteCount((long)((HeapSegmentElement)list.get(0)).getLength()));
               item.setText(4, prettyByteCount((long)((HeapSegmentElement)list.get(count - 1)).getLength()));
               //计算中等值
               int median = count / 2;
               HeapSegmentElement element = (HeapSegmentElement)list.get(median);
               long size = (long)element.getLength();
               item.setText(5, prettyByteCount(size));//中等值
               long totalSize = 0L;
                //计算总大小和平均值
               for(int i = 0; i < count; ++i) {
                  element = (HeapSegmentElement)list.get(i);
                  size = (long)element.getLength();
                  totalSize += size;
               }

               item.setText(2, prettyByteCount(totalSize));//总大小
               item.setText(6, prettyByteCount(totalSize / (long)count));//平均值
            }
         } else {
            this.mStatisticsTable.setRedraw(true);
         }
      } else {
         this.mStatisticsTable.removeAll();
         this.showChart((ArrayList)null);
      }
   }

   private void showChart(ArrayList<HeapSegmentElement> list) {
      this.mAllocCountDataSet.clear();
      if(list != null) {
         String rowKey = "Alloc Count";
         long currentSize = -1L;
         int currentCount = 0;
         Iterator columnKey = list.iterator();

         while(columnKey.hasNext()) {
            HeapSegmentElement element = (HeapSegmentElement)columnKey.next();
            if((long)element.getLength() != currentSize) {
               if(currentSize != -1L) {
                  HeapPanel.ByteLong columnKey1 = new HeapPanel.ByteLong(currentSize, null);
                  this.mAllocCountDataSet.addValue((double)currentCount, rowKey, columnKey1);
               }

               currentSize = (long)element.getLength();
               currentCount = 1;
            } else {
               ++currentCount;
            }
         }

         if(currentSize != -1L) {
            HeapPanel.ByteLong var9 = new HeapPanel.ByteLong(currentSize, null);
            this.mAllocCountDataSet.addValue((double)currentCount, rowKey, var9);
         }
      }

   }

   private void createLegend(Composite parent) {
      this.mLegend = new Group(parent, 0);
      this.mLegend.setText(this.getLegendText(0));
      this.mLegend.setLayout(new GridLayout(2, false));
      RGB[] colors = mMapPalette.colors;

      for(int i = 0; i < 10; ++i) {
         Image tmpImage = this.createColorRect(parent.getDisplay(), colors[i]);
         Label l = new Label(this.mLegend, 0);
         l.setImage(tmpImage);
         l = new Label(this.mLegend, 0);
         l.setText(mMapLegend[i]);
      }

   }

   private String getLegendText(int level) {
      int bytes = 8 * (100 / ZOOMS[level]);
      return String.format("Key (1 pixel = %1$d bytes)", new Object[]{Integer.valueOf(bytes)});
   }

   private void setLegendText(int level) {
      this.mLegend.setText(this.getLegendText(level));
   }

   private Image createColorRect(Display display, RGB color) {
      byte width = 32;
      byte height = 16;
      Image img = new Image(display, width, height);
      GC gc = new GC(img);
      gc.setBackground(new Color(display, color));
      gc.fillRectangle(0, 0, width, height);
      gc.dispose();
      return img;
   }

   private void setUpdateStatus(int status) {
      switch(status) {
      case 0:
         this.mUpdateStatus.setText("选择一个app来看Heap更新");
         break;
      case 1:
         this.mUpdateStatus.setText("Heap更新未开启");
         break;
      case 2:
         this.mUpdateStatus.setText("每次GC之后会产生Heap更新");
         break;
      default:
         throw new RuntimeException();
      }

      this.mUpdateStatus.pack();
   }

   private int nextPow2(int value) {
      for(int i = 31; i >= 0; --i) {
         if((value & 1 << i) != 0) {
            if(i < 31) {
               return 1 << i + 1;
            }

            return Integer.MIN_VALUE;
         }
      }

      return 0;
   }

   private int zOrderData(ImageData id, byte[] pixData) {
      int maxX = 0;

      for(int i = 0; i < pixData.length; ++i) {
         int x = i & 1 | (i >>> 2 & 1) << 1 | (i >>> 4 & 1) << 2 | (i >>> 6 & 1) << 3 | (i >>> 8 & 1) << 4 | (i >>> 10 & 1) << 5 | (i >>> 12 & 1) << 6 | (i >>> 14 & 1) << 7 | (i >>> 16 & 1) << 8 | (i >>> 18 & 1) << 9 | (i >>> 20 & 1) << 10 | (i >>> 22 & 1) << 11 | (i >>> 24 & 1) << 12 | (i >>> 26 & 1) << 13 | (i >>> 28 & 1) << 14 | (i >>> 30 & 1) << 15;
         int y = (i >>> 1 & 1) << 0 | (i >>> 3 & 1) << 1 | (i >>> 5 & 1) << 2 | (i >>> 7 & 1) << 3 | (i >>> 9 & 1) << 4 | (i >>> 11 & 1) << 5 | (i >>> 13 & 1) << 6 | (i >>> 15 & 1) << 7 | (i >>> 17 & 1) << 8 | (i >>> 19 & 1) << 9 | (i >>> 21 & 1) << 10 | (i >>> 23 & 1) << 11 | (i >>> 25 & 1) << 12 | (i >>> 27 & 1) << 13 | (i >>> 29 & 1) << 14 | (i >>> 31 & 1) << 15;

         try {
            id.setPixel(x, y, pixData[i]);
            if(x > maxX) {
               maxX = x;
            }
         } catch (IllegalArgumentException var8) {
            System.out.println("bad pixels: i " + i + ", w " + id.width + ", h " + id.height + ", x " + x + ", y " + y);
            throw var8;
         }
      }

      return maxX;
   }

   private void hilbertWalk(ImageData id, InputStream pixData, int order, int x, int y, int dir) throws IOException {
      if(x < id.width && y < id.height) {
         int delta;
         if(order == 0) {
            try {
               delta = pixData.read();
               if(delta >= 0) {
                  id.setPixel(y, x, delta);
                  if(y > id.x) {
                     id.x = y;
                  }

                  if(x > id.y) {
                     id.y = x;
                  }
               }
            } catch (IllegalArgumentException var10) {
               System.out.println("bad pixels: order " + order + ", dir " + dir + ", w " + id.width + ", h " + id.height + ", x " + x + ", y " + y);
               throw var10;
            }
         } else {
            --order;
            delta = 1 << order;
            int nextX = x + delta;
            int nextY = y + delta;
            switch(dir) {
            case 0:
               this.hilbertWalk(id, pixData, order, x, y, 2);
               this.hilbertWalk(id, pixData, order, nextX, y, 0);
               this.hilbertWalk(id, pixData, order, nextX, nextY, 0);
               this.hilbertWalk(id, pixData, order, x, nextY, 3);
               break;
            case 1:
               this.hilbertWalk(id, pixData, order, nextX, nextY, 3);
               this.hilbertWalk(id, pixData, order, x, nextY, 1);
               this.hilbertWalk(id, pixData, order, x, y, 1);
               this.hilbertWalk(id, pixData, order, nextX, y, 2);
               break;
            case 2:
               this.hilbertWalk(id, pixData, order, x, y, 0);
               this.hilbertWalk(id, pixData, order, x, nextY, 2);
               this.hilbertWalk(id, pixData, order, nextX, nextY, 2);
               this.hilbertWalk(id, pixData, order, nextX, y, 1);
               break;
            case 3:
               this.hilbertWalk(id, pixData, order, nextX, nextY, 1);
               this.hilbertWalk(id, pixData, order, nextX, y, 3);
               this.hilbertWalk(id, pixData, order, x, y, 3);
               this.hilbertWalk(id, pixData, order, x, nextY, 0);
               break;
            default:
               throw new RuntimeException("Unexpected Hilbert direction " + dir);
            }
         }

      }
   }

   private Point hilbertOrderData(ImageData id, byte[] pixData) {
      int order = 0;

      for(int p = 1; p < id.width; p *= 2) {
         ++order;
      }

      Point var9 = new Point(0, 0);
      int oldIdX = id.x;
      int oldIdY = id.y;
      id.x = id.y = 0;

      try {
         this.hilbertWalk(id, new ByteArrayInputStream(pixData), order, 0, 0, 2);
         var9.x = id.x;
         var9.y = id.y;
      } catch (IOException var8) {
         System.err.println("Exception during hilbertWalk()");
         var9.x = id.height;
         var9.y = id.width;
      }

      id.x = oldIdX;
      id.y = oldIdY;
      return var9;
   }

   private ImageData createHilbertHeapImage(byte[] pixData) {
      int w = (int)Math.sqrt(2097152.0D);
      w = this.nextPow2(w);
      ImageData id = new ImageData(w, w, 8, mMapPalette);
      Point maxP = this.hilbertOrderData(id, pixData);
      int factor = 100 / ZOOMS[this.mZoom.getSelectionIndex()];
      if(factor != 1) {
         int croppedId = maxP.x % factor;
         if(croppedId != 0) {
            maxP.x += factor - croppedId;
         }

         croppedId = maxP.y % factor;
         if(croppedId != 0) {
            maxP.y += factor - croppedId;
         }
      }

      if(maxP.y < id.height) {
         id = new ImageData(id.width, maxP.y, id.depth, id.palette, id.scanlinePad, id.data);
      }

      if(maxP.x < id.width) {
         ImageData var10 = new ImageData(maxP.x, id.height, id.depth, id.palette);
         int[] buffer = new int[maxP.x];

         for(int l = 0; l < id.height; ++l) {
            id.getPixels(0, l, maxP.x, buffer, 0);
            var10.setPixels(0, l, maxP.x, buffer, 0);
         }

         id = var10;
      }

      if(factor != 1) {
         id = id.scaledTo(id.width / factor, id.height / factor);
      }

      return id;
   }

   private void renderHeapData(ClientData cd, int mode, boolean forceRedraw) {
      byte[] pixData;
      synchronized(cd) {
         if(!this.serializeHeapData(cd.getVmHeapData()) && !forceRedraw) {
            return;
         }

         pixData = this.getSerializedData();
      }

      Image image;
      if(pixData != null) {
         ImageData width;
         if(mode == 1) {
            width = this.createHilbertHeapImage(pixData);
         } else {
            width = this.createLinearHeapImage(pixData, 200, mMapPalette);
         }

         image = new Image(this.mDisplay, width);
      } else {
         short height;
         short width1;
         if(mode == 1) {
            height = 200;
            width1 = 200;
         } else {
            width1 = 300;
            height = 100;
         }

         image = new Image(this.mDisplay, width1, height);
         GC gc = new GC(image);
         gc.setForeground(this.mDisplay.getSystemColor(3));
         gc.drawLine(0, 0, width1 - 1, height - 1);
         gc.dispose();
         gc = null;
      }

      if(mode == 1) {
         if(this.mHilbertImage != null) {
            this.mHilbertImage.dispose();
         }

         this.mHilbertImage = image;
         this.mHilbertHeapImage.setImage(this.mHilbertImage);
         this.mHilbertHeapImage.pack(true);
         this.mHilbertBase.layout();
         this.mHilbertBase.pack(true);
      } else {
         if(this.mLinearImage != null) {
            this.mLinearImage.dispose();
         }

         this.mLinearImage = image;
         this.mLinearHeapImage.setImage(this.mLinearImage);
         this.mLinearHeapImage.pack(true);
         this.mLinearBase.layout();
         this.mLinearBase.pack(true);
      }

   }

   protected void setTableFocusListener() {
      this.addTableToFocusListener(this.mHeapSummary);
   }

   static {
      sByteFormatter.setMinimumFractionDigits(0);
      sByteFormatter.setMaximumFractionDigits(1);
      sLargeByteFormatter.setMinimumFractionDigits(3);
      sLargeByteFormatter.setMaximumFractionDigits(3);
      sCountFormatter.setGroupingUsed(true);
   }

   private static class ByteLong implements Comparable<HeapPanel.ByteLong> {
      private long mValue;

      private ByteLong(long value) {
         this.mValue = value;
      }

      public long getValue() {
         return this.mValue;
      }

      public String toString() {
         return HeapPanel.approximateByteCount(this.mValue);
      }

      public int compareTo(HeapPanel.ByteLong other) {
         return this.mValue != other.mValue?(this.mValue < other.mValue?-1:1):0;
      }

      // $FF: synthetic method
      ByteLong(long x0, Object x1) {
         this(x0);
      }
   }
}
