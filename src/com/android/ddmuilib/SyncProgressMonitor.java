package com.android.ddmuilib;

import com.android.ddmlib.SyncService.ISyncProgressMonitor;
import org.eclipse.core.runtime.IProgressMonitor;

public class SyncProgressMonitor implements ISyncProgressMonitor {
   private IProgressMonitor mMonitor;
   private String mName;

   public SyncProgressMonitor(IProgressMonitor monitor, String name) {
      this.mMonitor = monitor;
      this.mName = name;
   }

   public void start(int totalWork) {
      this.mMonitor.beginTask(this.mName, totalWork);
   }

   public void stop() {
      this.mMonitor.done();
   }

   public void advance(int work) {
      this.mMonitor.worked(work);
   }

   public boolean isCanceled() {
      return this.mMonitor.isCanceled();
   }

   public void startSubTask(String name) {
      this.mMonitor.subTask(name);
   }
}
