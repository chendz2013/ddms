package com.android.ddmuilib;

import com.android.ddmlib.Client;
import com.android.ddmlib.ThreadInfo;
import com.android.ddmuilib.DdmUiPreferences;
import com.android.ddmuilib.StackTracePanel;
import com.android.ddmuilib.TableHelper;
import com.android.ddmuilib.TablePanel;
import java.util.Date;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWTException;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Sash;
import org.eclipse.swt.widgets.Table;

public class ThreadPanel extends TablePanel {
   private static final String PREFS_THREAD_COL_ID = "threadPanel.Col0";
   private static final String PREFS_THREAD_COL_TID = "threadPanel.Col1";
   private static final String PREFS_THREAD_COL_STATUS = "threadPanel.Col2";
   private static final String PREFS_THREAD_COL_UTIME = "threadPanel.Col3";
   private static final String PREFS_THREAD_COL_STIME = "threadPanel.Col4";
   private static final String PREFS_THREAD_COL_NAME = "threadPanel.Col5";
   private static final String PREFS_THREAD_SASH = "threadPanel.sash";
   private static final String PREFS_STACK_COLUMN = "threadPanel.stack.col0";
   private Display mDisplay;
   private Composite mBase;
   private Label mNotEnabled;
   private Label mNotSelected;
   private Composite mThreadBase;
   private Table mThreadTable;
   private TableViewer mThreadViewer;
   private Composite mStackTraceBase;
   private Button mRefreshStackTraceButton;
   private Label mStackTraceTimeLabel;
   private StackTracePanel mStackTracePanel;
   private Table mStackTraceTable;
   private boolean mMustStopRecurringThreadUpdate = false;
   private boolean mRecurringThreadUpdateRunning = false;
   private Object mLock = new Object();
   private static final String[] THREAD_STATUS = new String[]{"Zombie", "Runnable", "TimedWait", "Monitor", "Wait", "Initializing", "Starting", "Native", "VmWait", "Suspended"};

   protected Control createControl(Composite parent) {
      this.mDisplay = parent.getDisplay();
      final IPreferenceStore store = DdmUiPreferences.getStore();
      this.mBase = new Composite(parent, 0);
      this.mBase.setLayout(new StackLayout());
      this.mNotEnabled = new Label(this.mBase, 16777280);
      this.mNotEnabled.setText("Thread updates not enabled for selected client\n(use toolbar button to enable)");
      this.mNotSelected = new Label(this.mBase, 16777280);
      this.mNotSelected.setText("no client is selected");
      this.mThreadBase = new Composite(this.mBase, 0);
      this.mThreadBase.setLayout(new FormLayout());
      this.mThreadTable = new Table(this.mThreadBase, 65538);
      this.mThreadTable.setHeaderVisible(true);
      this.mThreadTable.setLinesVisible(true);
      TableHelper.createTableColumn(this.mThreadTable, "ID(VM分配)", 131072, "888", "threadPanel.Col0", store);
      TableHelper.createTableColumn(this.mThreadTable, "Tid(Linux分配)", 131072, "88888", "threadPanel.Col1", store);
      TableHelper.createTableColumn(this.mThreadTable, "状态", 16384, "timed-wait", "threadPanel.Col2", store);
      TableHelper.createTableColumn(this.mThreadTable, "utime(用户)", 131072, "utime", "threadPanel.Col3", store);
      TableHelper.createTableColumn(this.mThreadTable, "stime(系统)", 131072, "utime", "threadPanel.Col4", store);
      TableHelper.createTableColumn(this.mThreadTable, "名称", 16384, "android.class.ReallyLongClassName.MethodName", "threadPanel.Col5", store);
      this.mThreadViewer = new TableViewer(this.mThreadTable);
      this.mThreadViewer.setContentProvider(new ThreadPanel.ThreadContentProvider(null));
      this.mThreadViewer.setLabelProvider(new ThreadPanel.ThreadLabelProvider(null));
      this.mThreadViewer.addSelectionChangedListener(new ISelectionChangedListener() {
         public void selectionChanged(SelectionChangedEvent event) {
            ThreadPanel.this.requestThreadStackTrace(ThreadPanel.this.getThreadSelection(event.getSelection()));
         }
      });
      this.mThreadViewer.addDoubleClickListener(new IDoubleClickListener() {
         public void doubleClick(DoubleClickEvent event) {
            ThreadPanel.this.requestThreadStackTrace(ThreadPanel.this.getThreadSelection(event.getSelection()));
         }
      });
      final Sash sash = new Sash(this.mThreadBase, 256);
      Color darkGray = parent.getDisplay().getSystemColor(16);
      sash.setBackground(darkGray);
      this.mStackTraceBase = new Composite(this.mThreadBase, 0);
      this.mStackTraceBase.setLayout(new GridLayout(2, false));
      this.mRefreshStackTraceButton = new Button(this.mStackTraceBase, 8);
      this.mRefreshStackTraceButton.setText("刷新");
      this.mRefreshStackTraceButton.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            ThreadPanel.this.requestThreadStackTrace(ThreadPanel.this.getThreadSelection((ISelection)null));
         }
      });
      this.mStackTraceTimeLabel = new Label(this.mStackTraceBase, 0);
      this.mStackTraceTimeLabel.setLayoutData(new GridData(768));
      this.mStackTracePanel = new StackTracePanel();
      this.mStackTraceTable = this.mStackTracePanel.createPanel(this.mStackTraceBase, "threadPanel.stack.col0", store);
      GridData gd;
      this.mStackTraceTable.setLayoutData(gd = new GridData(1808));
      gd.horizontalSpan = 2;
      FormData data = new FormData();
      data.top = new FormAttachment(0, 0);
      data.bottom = new FormAttachment(sash, 0);
      data.left = new FormAttachment(0, 0);
      data.right = new FormAttachment(100, 0);
      this.mThreadTable.setLayoutData(data);
      final FormData sashData = new FormData();
      if(store != null && store.contains("threadPanel.sash")) {
         sashData.top = new FormAttachment(0, store.getInt("threadPanel.sash"));
      } else {
         sashData.top = new FormAttachment(50, 0);
      }

      sashData.left = new FormAttachment(0, 0);
      sashData.right = new FormAttachment(100, 0);
      sash.setLayoutData(sashData);
      data = new FormData();
      data.top = new FormAttachment(sash, 0);
      data.bottom = new FormAttachment(100, 0);
      data.left = new FormAttachment(0, 0);
      data.right = new FormAttachment(100, 0);
      this.mStackTraceBase.setLayoutData(data);
      sash.addListener(13, new Listener() {
         public void handleEvent(Event e) {
            Rectangle sashRect = sash.getBounds();
            Rectangle panelRect = ThreadPanel.this.mThreadBase.getClientArea();
            int bottom = panelRect.height - sashRect.height - 100;
            e.y = Math.max(Math.min(e.y, bottom), 100);
            if(e.y != sashRect.y) {
               sashData.top = new FormAttachment(0, e.y);
               store.setValue("threadPanel.sash", e.y);
               ThreadPanel.this.mThreadBase.layout();
            }

         }
      });
      ((StackLayout)this.mBase.getLayout()).topControl = this.mNotSelected;
      return this.mBase;
   }

   public void setFocus() {
      this.mThreadTable.setFocus();
   }

   public void clientChanged(Client client, int changeMask) {
      if(client == this.getCurrentClient()) {
         if((changeMask & 8) == 0 && (changeMask & 16) == 0) {
            if((changeMask & 256) != 0) {
               try {
                  this.mThreadTable.getDisplay().asyncExec(new Runnable() {
                     public void run() {
                        ThreadPanel.this.updateThreadStackCall();
                     }
                  });
               } catch (SWTException var4) {
                  ;
               }
            }
         } else {
            try {
               this.mThreadTable.getDisplay().asyncExec(new Runnable() {
                  public void run() {
                     ThreadPanel.this.clientSelected();
                  }
               });
            } catch (SWTException var5) {
               ;
            }
         }
      }

   }

   public void deviceSelected() {
   }

   public void clientSelected() {
      if(!this.mThreadTable.isDisposed()) {
         Client client = this.getCurrentClient();
         this.mStackTracePanel.setCurrentClient(client);
         if(client != null) {
            if(!client.isThreadUpdateEnabled()) {
               ((StackLayout)this.mBase.getLayout()).topControl = this.mNotEnabled;
               this.mThreadViewer.setInput((Object)null);
               this.mMustStopRecurringThreadUpdate = true;
            } else {
               ((StackLayout)this.mBase.getLayout()).topControl = this.mThreadBase;
               this.mThreadViewer.setInput(client);
               Object var2 = this.mLock;
               synchronized(this.mLock) {
                  if(!this.mRecurringThreadUpdateRunning) {
                     this.startRecurringThreadUpdate();
                  } else if(this.mMustStopRecurringThreadUpdate) {
                     this.mMustStopRecurringThreadUpdate = false;
                  }
               }
            }
         } else {
            ((StackLayout)this.mBase.getLayout()).topControl = this.mNotSelected;
            this.mThreadViewer.setInput((Object)null);
         }

         this.mBase.layout();
      }
   }

   private void requestThreadStackTrace(ThreadInfo selectedThread) {
      if(selectedThread != null) {
         Client client = (Client)this.mThreadViewer.getInput();
         if(client != null) {
            client.requestThreadStackTrace(selectedThread.getThreadId());
         }
      }

   }

   private void updateThreadStackCall() {
      Client client = this.getCurrentClient();
      if(client != null) {
         ThreadInfo selectedThread = this.getThreadSelection((ISelection)null);
         if(selectedThread != null) {
            this.updateThreadStackTrace(selectedThread);
         } else {
            this.updateThreadStackTrace((ThreadInfo)null);
         }
      }

   }

   private void updateThreadStackTrace(ThreadInfo thread) {
      this.mStackTracePanel.setViewerInput(thread);
      if(thread != null) {
         this.mRefreshStackTraceButton.setEnabled(true);
         long stackcallTime = thread.getStackCallTime();
         if(stackcallTime != 0L) {
            String label = (new Date(stackcallTime)).toString();
            this.mStackTraceTimeLabel.setText(label);
         } else {
            this.mStackTraceTimeLabel.setText("");
         }
      } else {
         this.mRefreshStackTraceButton.setEnabled(true);
         this.mStackTraceTimeLabel.setText("");
      }

   }

   protected void setTableFocusListener() {
      this.addTableToFocusListener(this.mThreadTable);
      this.addTableToFocusListener(this.mStackTraceTable);
   }

   private void startRecurringThreadUpdate() {
      this.mRecurringThreadUpdateRunning = true;
      short initialWait = 1000;
      this.mDisplay.timerExec(initialWait, new Runnable() {
         public void run() {
            synchronized(ThreadPanel.this.mLock) {
               if(!ThreadPanel.this.mMustStopRecurringThreadUpdate) {
                  Client client = ThreadPanel.this.getCurrentClient();
                  if(client != null) {
                     client.requestThreadUpdate();
                     ThreadPanel.this.mDisplay.timerExec(DdmUiPreferences.getThreadRefreshInterval() * 1000, this);
                  } else {
                     ThreadPanel.this.mRecurringThreadUpdateRunning = false;
                  }
               } else {
                  ThreadPanel.this.mRecurringThreadUpdateRunning = false;
                  ThreadPanel.this.mMustStopRecurringThreadUpdate = false;
               }

            }
         }
      });
   }

   private ThreadInfo getThreadSelection(ISelection selection) {
      if(selection == null) {
         selection = this.mThreadViewer.getSelection();
      }

      if(selection instanceof IStructuredSelection) {
         IStructuredSelection structuredSelection = (IStructuredSelection)selection;
         Object object = structuredSelection.getFirstElement();
         if(object instanceof ThreadInfo) {
            return (ThreadInfo)object;
         }
      }

      return null;
   }

   private static class ThreadLabelProvider implements ITableLabelProvider {
      private ThreadLabelProvider() {
      }

      public Image getColumnImage(Object element, int columnIndex) {
         return null;
      }

      /**
       * 读取每个列的信息
       * @param element
       * @param columnIndex
       * @return
        */
      public String getColumnText(Object element, int columnIndex) {
         if(element instanceof ThreadInfo) {
            ThreadInfo thread = (ThreadInfo)element;
            switch(columnIndex) {
            case 0:
               return (thread.isDaemon()?"(Daemon)":"") + String.valueOf(thread.getThreadId());
            case 1:
               return String.valueOf(thread.getTid());
            case 2:
               if(thread.getStatus() >= 0 && thread.getStatus() < ThreadPanel.THREAD_STATUS.length) {
                  return ThreadPanel.THREAD_STATUS[thread.getStatus()];
               }

               return "unknown";
            case 3:
               return String.valueOf(thread.getUtime());
            case 4:
               return String.valueOf(thread.getStime());
            case 5:
               return thread.getThreadName();
            }
         }

         return null;
      }

      public void addListener(ILabelProviderListener listener) {
      }

      public void dispose() {
      }

      public boolean isLabelProperty(Object element, String property) {
         return false;
      }

      public void removeListener(ILabelProviderListener listener) {
      }

      // $FF: synthetic method
      ThreadLabelProvider(Object x0) {
         this();
      }
   }

   private static class ThreadContentProvider implements IStructuredContentProvider {
      private ThreadContentProvider() {
      }

      public Object[] getElements(Object inputElement) {
         return (Object[])(inputElement instanceof Client?((Client)inputElement).getClientData().getThreads():new Object[0]);
      }

      public void dispose() {
      }

      public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
      }

      // $FF: synthetic method
      ThreadContentProvider(Object x0) {
         this();
      }
   }
}
