package com.android.ddmuilib;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

public abstract class Panel {
   public final Control createPanel(Composite parent) {
      Control panelControl = this.createControl(parent);
      this.postCreation();
      return panelControl;
   }

   protected abstract void postCreation();

   protected abstract Control createControl(Composite var1);

   public abstract void setFocus();
}
