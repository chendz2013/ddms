package com.android.ddmuilib;

import com.android.ddmlib.Log;

public abstract class BackgroundThread extends Thread {
   private boolean mQuit = false;

   public final void quit() {
      this.mQuit = true;
      Log.d("ddms", "Waiting for BackgroundThread to quit");

      try {
         this.join();
      } catch (InterruptedException var2) {
         var2.printStackTrace();
      }

   }

   protected final boolean isQuitting() {
      return this.mQuit;
   }
}
