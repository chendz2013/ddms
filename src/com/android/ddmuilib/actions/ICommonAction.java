package com.android.ddmuilib.actions;

public interface ICommonAction {
   void setEnabled(boolean var1);

   void setChecked(boolean var1);

   void setRunnable(Runnable var1);
}
