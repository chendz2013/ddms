package com.android.ddmuilib.actions;

import com.android.ddmuilib.actions.ICommonAction;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;

public class ToolItemAction implements ICommonAction {
   public ToolItem item;

   public ToolItemAction(ToolBar parent, int style) {
      this.item = new ToolItem(parent, style);
   }

   public void setChecked(boolean checked) {
      this.item.setSelection(checked);
   }

   public void setEnabled(boolean enabled) {
      this.item.setEnabled(enabled);
   }

   public void setRunnable(final Runnable runnable) {
      this.item.addSelectionListener(new SelectionAdapter() {
         public void widgetSelected(SelectionEvent e) {
            runnable.run();
         }
      });
   }
}
