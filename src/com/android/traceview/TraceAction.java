package com.android.traceview;

import com.android.traceview.Call;

final class TraceAction {
   public static final int ACTION_ENTER = 0;
   public static final int ACTION_EXIT = 1;
   public static final int ACTION_INCOMPLETE = 2;
   public final int mAction;
   public final Call mCall;

   public TraceAction(int action, Call call) {
      this.mAction = action;
      this.mCall = call;
   }
}
