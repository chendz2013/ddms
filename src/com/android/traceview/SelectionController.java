package com.android.traceview;

import com.android.traceview.Selection;
import java.util.ArrayList;
import java.util.Observable;

public class SelectionController extends Observable {
   private ArrayList<Selection> mSelections;

   public void change(ArrayList<Selection> selections, Object arg) {
      this.mSelections = selections;
      this.setChanged();
      this.notifyObservers(arg);
   }

   public ArrayList<Selection> getSelections() {
      return this.mSelections;
   }
}
