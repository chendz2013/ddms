package com.android.traceview;

import com.android.traceview.MethodData;
import com.android.traceview.ProfileData;

public class ProfileSelf extends ProfileData {
   public ProfileSelf(MethodData methodData) {
      this.mElement = methodData;
      this.mContext = methodData;
   }

   public String getProfileName() {
      return "* self自身";
   }

   public long getElapsedInclusiveCpuTime() {
      return this.mElement.getTopExclusiveCpuTime();
   }

   public long getElapsedInclusiveRealTime() {
      return this.mElement.getTopExclusiveRealTime();
   }
}
