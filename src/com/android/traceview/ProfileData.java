package com.android.traceview;

import com.android.traceview.MethodData;

public class ProfileData {
   protected MethodData mElement;
   protected MethodData mContext;
   protected boolean mElementIsParent;
   protected long mElapsedInclusiveCpuTime;
   protected long mElapsedInclusiveRealTime;
   protected int mNumCalls;

   public ProfileData() {
   }

   public ProfileData(MethodData context, MethodData element, boolean elementIsParent) {
      this.mContext = context;
      this.mElement = element;
      this.mElementIsParent = elementIsParent;
   }

   public String getProfileName() {
      return this.mElement.getProfileName();
   }

   public MethodData getMethodData() {
      return this.mElement;
   }

   public void addElapsedInclusive(long cpuTime, long realTime) {
      this.mElapsedInclusiveCpuTime += cpuTime;
      this.mElapsedInclusiveRealTime += realTime;
      ++this.mNumCalls;
   }

   public void setElapsedInclusive(long cpuTime, long realTime) {
      this.mElapsedInclusiveCpuTime = cpuTime;
      this.mElapsedInclusiveRealTime = realTime;
   }

   public long getElapsedInclusiveCpuTime() {
      return this.mElapsedInclusiveCpuTime;
   }

   public long getElapsedInclusiveRealTime() {
      return this.mElapsedInclusiveRealTime;
   }

   public void setNumCalls(int numCalls) {
      this.mNumCalls = numCalls;
   }

   public String getNumCalls() {
      int totalCalls;
      if(this.mElementIsParent) {
         totalCalls = this.mContext.getTotalCalls();
      } else {
         totalCalls = this.mElement.getTotalCalls();
      }

      return String.format("%d/%d", new Object[]{Integer.valueOf(this.mNumCalls), Integer.valueOf(totalCalls)});
   }

   public boolean isParent() {
      return this.mElementIsParent;
   }

   public MethodData getContext() {
      return this.mContext;
   }
}
