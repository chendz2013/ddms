package com.android.traceview;

import com.android.traceview.Call;
import com.android.traceview.MethodData;
import com.android.traceview.ProfileProvider;
import com.android.traceview.ThreadData;
import com.android.traceview.TimeBase;
import com.android.traceview.TimeLineView;
import com.android.traceview.TraceAction;
import com.android.traceview.TraceReader;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.BufferUnderflowException;
import java.nio.ByteOrder;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileChannel.MapMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DmTraceReader extends TraceReader {
   private static final int TRACE_MAGIC = 1464814675;
   private static final int METHOD_TRACE_ENTER = 0;
   private static final int METHOD_TRACE_EXIT = 1;
   private static final int METHOD_TRACE_UNROLL = 2;
   private static final long MIN_CONTEXT_SWITCH_TIME_USEC = 100L;
   private int mVersionNumber;
   private boolean mRegression;
   private ProfileProvider mProfileProvider;
   private String mTraceFileName;
   private MethodData mTopLevel;
   private ArrayList<Call> mCallList;
   private HashMap<String, String> mPropertiesMap;
   private HashMap<Integer, MethodData> mMethodMap;
   private HashMap<Integer, ThreadData> mThreadMap;
   private ThreadData[] mSortedThreads;
   private MethodData[] mSortedMethods;
   private long mTotalCpuTime;
   private long mTotalRealTime;
   private MethodData mContextSwitch;
   private int mRecordSize;
   private DmTraceReader.ClockSource mClockSource;
   private static final Pattern mIdNamePattern = Pattern.compile("(\\d+)\t(.*)");
   static final int PARSE_VERSION = 0;
   static final int PARSE_THREADS = 1;
   static final int PARSE_METHODS = 2;
   static final int PARSE_OPTIONS = 4;

   public DmTraceReader(String traceFileName, boolean regression) throws IOException {
      this.mTraceFileName = traceFileName;
      this.mRegression = regression;
      this.mPropertiesMap = new HashMap();
      this.mMethodMap = new HashMap();
      this.mThreadMap = new HashMap();
      this.mCallList = new ArrayList();
      this.mTopLevel = new MethodData(0, "(顶层)");
      this.mContextSwitch = new MethodData(-1, "(context切换)");
      this.mMethodMap.put(Integer.valueOf(0), this.mTopLevel);
      this.mMethodMap.put(Integer.valueOf(-1), this.mContextSwitch);
      this.generateTrees();
   }

   void generateTrees() throws IOException {
      long offset = this.parseKeys();
      this.parseData(offset);
      this.analyzeData();
   }

   public ProfileProvider getProfileProvider() {
      if(this.mProfileProvider == null) {
         this.mProfileProvider = new ProfileProvider(this);
      }

      return this.mProfileProvider;
   }

   private MappedByteBuffer mapFile(String filename, long offset) throws IOException {
      MappedByteBuffer buffer = null;
      FileInputStream dataFile = new FileInputStream(filename);

      MappedByteBuffer var8;
      try {
         File file = new File(filename);
         FileChannel fc = dataFile.getChannel();
         buffer = fc.map(MapMode.READ_ONLY, offset, file.length() - offset);
         buffer.order(ByteOrder.LITTLE_ENDIAN);
         var8 = buffer;
      } finally {
         dataFile.close();
      }

      return var8;
   }

   private void readDataFileHeader(MappedByteBuffer buffer) {
      int magic = buffer.getInt();
      if(magic != 1464814675) {
         System.err.printf("Error: magic number mismatch; got 0x%x, expected 0x%x\n", new Object[]{Integer.valueOf(magic), Integer.valueOf(1464814675)});
         throw new RuntimeException();
      } else {
         short version = buffer.getShort();
         if(version != this.mVersionNumber) {
            System.err.printf("Error: version number mismatch; got %d in data header but %d in options\n", new Object[]{Integer.valueOf(version), Integer.valueOf(this.mVersionNumber)});
            throw new RuntimeException();
         } else if(version >= 1 && version <= 3) {
            int offsetToData = buffer.getShort() - 16;
            buffer.getLong();
            if(version == 1) {
               this.mRecordSize = 9;
            } else if(version == 2) {
               this.mRecordSize = 10;
            } else {
               this.mRecordSize = buffer.getShort();
               offsetToData -= 2;
            }

            while(offsetToData-- > 0) {
               buffer.get();
            }

         } else {
            System.err.printf("Error: unsupported trace version number %d.  Please use a newer version of TraceView to read this file.", new Object[]{Integer.valueOf(version)});
            throw new RuntimeException();
         }
      }
   }

   private void parseData(long offset) throws IOException {
      MappedByteBuffer buffer = this.mapFile(this.mTraceFileName, offset);
      this.readDataFileHeader(buffer);
      ArrayList trace = null;
      if(this.mClockSource == DmTraceReader.ClockSource.THREAD_CPU) {
         trace = new ArrayList();
      }

      boolean haveThreadClock = this.mClockSource != DmTraceReader.ClockSource.WALL;
      boolean haveGlobalClock = this.mClockSource != DmTraceReader.ClockSource.THREAD_CPU;
      ThreadData prevThreadData = null;

      while(true) {
         long var34;
         int var32;
         short var33;
         long var36;
         int var37;
         try {
            var37 = this.mRecordSize;
            if(this.mVersionNumber == 1) {
               var33 = buffer.get();
               --var37;
            } else {
               var33 = buffer.getShort();
               var37 -= 2;
            }

            var32 = buffer.getInt();
            var37 -= 4;
            switch(DmTraceReader.SyntheticClass_1.$SwitchMap$com$android$traceview$DmTraceReader$ClockSource[this.mClockSource.ordinal()]) {
            case 1:
               var34 = 0L;
               var36 = (long)buffer.getInt();
               var37 -= 4;
               break;
            case 2:
               var34 = (long)buffer.getInt();
               var36 = (long)buffer.getInt();
               var37 -= 8;
               break;
            case 3:
            default:
               var34 = (long)buffer.getInt();
               var36 = 0L;
               var37 -= 4;
            }

            while(var37-- > 0) {
               buffer.get();
            }
         } catch (BufferUnderflowException var28) {
            Iterator i$ = this.mThreadMap.values().iterator();

            ThreadData threadData;
            while(i$.hasNext()) {
               threadData = (ThreadData)i$.next();
               threadData.endTrace(trace);
            }

            if(!haveGlobalClock) {
               long var29 = 0L;
               prevThreadData = null;
               Iterator rootCall = trace.iterator();

               while(rootCall.hasNext()) {
                  TraceAction traceAction = (TraceAction)rootCall.next();
                  Call call = traceAction.mCall;
                  ThreadData threadData1 = call.getThreadData();
                  long threadTime;
                  if(traceAction.mAction == 0) {
                     threadTime = call.mThreadStartTime;
                     var29 += call.mThreadStartTime - threadData1.mThreadCurrentTime;
                     call.mGlobalStartTime = var29;
                     if(!threadData1.mHaveGlobalTime) {
                        threadData1.mHaveGlobalTime = true;
                        threadData1.mGlobalStartTime = var29;
                     }

                     threadData1.mThreadCurrentTime = threadTime;
                  } else if(traceAction.mAction == 1) {
                     threadTime = call.mThreadEndTime;
                     var29 += call.mThreadEndTime - threadData1.mThreadCurrentTime;
                     call.mGlobalEndTime = var29;
                     threadData1.mGlobalEndTime = var29;
                     threadData1.mThreadCurrentTime = threadTime;
                  }
               }
            }

            for(int var30 = this.mCallList.size() - 1; var30 >= 0; --var30) {
               Call var31 = (Call)this.mCallList.get(var30);
               var34 = var31.mGlobalEndTime - var31.mGlobalStartTime;
               var31.mExclusiveRealTime = Math.max(var34 - var31.mInclusiveRealTime, 0L);
               var31.mInclusiveRealTime = var34;
               var31.finish();
            }

            this.mTotalCpuTime = 0L;
            this.mTotalRealTime = 0L;

            Call var35;
            for(i$ = this.mThreadMap.values().iterator(); i$.hasNext(); this.mTotalRealTime += var35.mInclusiveRealTime) {
               threadData = (ThreadData)i$.next();
               var35 = threadData.getRootCall();
               threadData.updateRootCallTimeBounds();
               var35.finish();
               this.mTotalCpuTime += var35.mInclusiveCpuTime;
            }

            if(this.mRegression) {
               System.out.format("totalCpuTime %dus\n", new Object[]{Long.valueOf(this.mTotalCpuTime)});
               System.out.format("totalRealTime %dus\n", new Object[]{Long.valueOf(this.mTotalRealTime)});
               this.dumpThreadTimes();
               this.dumpCallTimes();
            }

            return;
         }

         var37 = var32 & 3;
         var32 &= -4;
         MethodData methodData = (MethodData)this.mMethodMap.get(Integer.valueOf(var32));
         if(methodData == null) {
            String threadData2 = String.format("(0x%1$x)", new Object[]{Integer.valueOf(var32)});
            methodData = new MethodData(var32, threadData2);
            this.mMethodMap.put(Integer.valueOf(var32), methodData);
         }

         ThreadData var38 = (ThreadData)this.mThreadMap.get(Integer.valueOf(var33));
         if(var38 == null) {
            String elapsedGlobalTime = String.format("[%1$d]", new Object[]{Integer.valueOf(var33)});
            var38 = new ThreadData(var33, elapsedGlobalTime, this.mTopLevel);
            this.mThreadMap.put(Integer.valueOf(var33), var38);
         }

         long var39 = 0L;
         if(haveGlobalClock) {
            if(!var38.mHaveGlobalTime) {
               var38.mGlobalStartTime = var36;
               var38.mHaveGlobalTime = true;
            } else {
               var39 = var36 - var38.mGlobalEndTime;
            }

            var38.mGlobalEndTime = var36;
         }

         if(haveThreadClock) {
            long call1 = 0L;
            if(!var38.mHaveThreadTime) {
               var38.mThreadStartTime = var34;
               var38.mThreadCurrentTime = var34;
               var38.mHaveThreadTime = true;
            } else {
               call1 = var34 - var38.mThreadEndTime;
            }

            var38.mThreadEndTime = var34;
            Call top;
            if(!haveGlobalClock) {
               if(prevThreadData != null && prevThreadData != var38) {
                  top = prevThreadData.enter(this.mContextSwitch, trace);
                  top.mThreadStartTime = prevThreadData.mThreadEndTime;
                  this.mCallList.add(top);
                  Call top1 = var38.top();
                  if(top1.getMethodData() == this.mContextSwitch) {
                     var38.exit(this.mContextSwitch, trace);
                     long switchCall = call1 / 2L;
                     top1.mThreadStartTime += switchCall;
                     top1.mThreadEndTime = top1.mThreadStartTime;
                  }
               }

               prevThreadData = var38;
            } else {
               long var42 = var39 - call1;
               if(var42 > 100L) {
                  Call var41 = var38.enter(this.mContextSwitch, trace);
                  long beforeSwitch = call1 / 2L;
                  long afterSwitch = call1 - beforeSwitch;
                  var41.mGlobalStartTime = var36 - var39 + beforeSwitch;
                  var41.mGlobalEndTime = var36 - afterSwitch;
                  var41.mThreadStartTime = var34 - afterSwitch;
                  var41.mThreadEndTime = var41.mThreadStartTime;
                  var38.exit(this.mContextSwitch, trace);
                  this.mCallList.add(var41);
               }
            }

            top = var38.top();
            top.addCpuTime(call1);
         }

         Call var40;
         switch(var37) {
         case 0:
            var40 = var38.enter(methodData, trace);
            if(haveGlobalClock) {
               var40.mGlobalStartTime = var36;
            }

            if(haveThreadClock) {
               var40.mThreadStartTime = var34;
            }

            this.mCallList.add(var40);
            break;
         case 1:
         case 2:
            var40 = var38.exit(methodData, trace);
            if(var40 != null) {
               if(haveGlobalClock) {
                  var40.mGlobalEndTime = var36;
               }

               if(haveThreadClock) {
                  var40.mThreadEndTime = var34;
               }
            }
            break;
         default:
            throw new RuntimeException("Unrecognized method action: " + var37);
         }
      }
   }

   long parseKeys() throws IOException {
      long offset = 0L;
      BufferedReader in = null;

      try {
         in = new BufferedReader(new InputStreamReader(new FileInputStream(this.mTraceFileName), "US-ASCII"));
         byte ex = 0;
         String line = null;

         label130:
         while(true) {
            while(true) {
               line = in.readLine();
               if(line == null) {
                  throw new IOException("Key section does not have an *end marker");
               }

               offset += (long)(line.length() + 1);
               if(line.startsWith("*")) {
                  if(line.equals("*version")) {
                     ex = 0;
                     continue;
                  }

                  if(line.equals("*threads")) {
                     ex = 1;
                     continue;
                  }

                  if(line.equals("*methods")) {
                     ex = 2;
                     continue;
                  }

                  if(line.equals("*end")) {
                     break label130;
                  }
               }

               switch(ex) {
               case 0:
                  this.mVersionNumber = Integer.decode(line).intValue();
                  ex = 4;
                  break;
               case 1:
                  this.parseThread(line);
                  break;
               case 2:
                  this.parseMethod(line);
               case 3:
               default:
                  break;
               case 4:
                  this.parseOption(line);
               }
            }
         }
      } catch (FileNotFoundException var9) {
         System.err.println(var9.getMessage());
      } finally {
         if(in != null) {
            in.close();
         }

      }

      if(this.mClockSource == null) {
         this.mClockSource = DmTraceReader.ClockSource.THREAD_CPU;
      }

      return offset;
   }

   void parseOption(String line) {
      String[] tokens = line.split("=");
      if(tokens.length == 2) {
         String key = tokens[0];
         String value = tokens[1];
         this.mPropertiesMap.put(key, value);
         if(key.equals("clock")) {
            if(value.equals("thread-cpu")) {
               this.mClockSource = DmTraceReader.ClockSource.THREAD_CPU;
            } else if(value.equals("wall")) {
               this.mClockSource = DmTraceReader.ClockSource.WALL;
            } else if(value.equals("dual")) {
               this.mClockSource = DmTraceReader.ClockSource.DUAL;
            }
         }
      }

   }

   void parseThread(String line) {
      String idStr = null;
      String name = null;
      Matcher matcher = mIdNamePattern.matcher(line);
      if(matcher.find()) {
         idStr = matcher.group(1);
         name = matcher.group(2);
      }

      if(idStr != null) {
         if(name == null) {
            name = "(unknown)";
         }

         int id = Integer.decode(idStr).intValue();
         this.mThreadMap.put(Integer.valueOf(id), new ThreadData(id, name, this.mTopLevel));
      }
   }

   void parseMethod(String line) {
      String[] tokens = line.split("\t");
      int id = Long.decode(tokens[0]).intValue();
      String className = tokens[1];
      String methodName = null;
      String signature = null;
      String pathname = null;
      int lineNumber = -1;
      if(tokens.length == 6) {
         methodName = tokens[2];
         signature = tokens[3];
         pathname = tokens[4];
         lineNumber = Integer.decode(tokens[5]).intValue();
         pathname = this.constructPathname(className, pathname);
      } else if(tokens.length > 2) {
         if(tokens[3].startsWith("(")) {
            methodName = tokens[2];
            signature = tokens[3];
         } else {
            pathname = tokens[2];
            lineNumber = Integer.decode(tokens[3]).intValue();
         }
      }

      this.mMethodMap.put(Integer.valueOf(id), new MethodData(id, className, methodName, signature, pathname, lineNumber));
   }

   private String constructPathname(String className, String pathname) {
      int index = className.lastIndexOf(47);
      if(index > 0 && index < className.length() - 1 && pathname.endsWith(".java")) {
         pathname = className.substring(0, index + 1) + pathname;
      }

      return pathname;
   }

   private void analyzeData() {
      final TimeBase timeBase = this.getPreferredTimeBase();
      Collection tv = this.mThreadMap.values();
      this.mSortedThreads = (ThreadData[])tv.toArray(new ThreadData[tv.size()]);
      Arrays.sort(this.mSortedThreads, new Comparator() {
         @Override
         public int compare(Object o1, Object o2) {
            return compare((ThreadData)o1,(ThreadData)o2);
         }

         public int compare(ThreadData td1, ThreadData td2) {
            return timeBase.getTime(td2) > timeBase.getTime(td1)?1:(timeBase.getTime(td2) < timeBase.getTime(td1)?-1:td2.getName().compareTo(td1.getName()));
         }
      });
      Collection mv = this.mMethodMap.values();
      MethodData[] methods = (MethodData[])mv.toArray(new MethodData[mv.size()]);
      Arrays.sort(methods, new Comparator() {
         @Override
         public int compare(Object o1, Object o2) {
            return compare((MethodData)o1,(MethodData)o2);
         }

         public int compare(MethodData md1, MethodData md2) {
            return timeBase.getElapsedInclusiveTime(md2) > timeBase.getElapsedInclusiveTime(md1)?1:(timeBase.getElapsedInclusiveTime(md2) < timeBase.getElapsedInclusiveTime(md1)?-1:md1.getName().compareTo(md2.getName()));
         }
      });
      int nonZero = 0;
      MethodData[] ii = methods;
      int i$ = methods.length;

      int call;
      for(call = 0; call < i$; ++call) {
         MethodData i$1 = ii[call];
         if(timeBase.getElapsedInclusiveTime(i$1) == 0L) {
            break;
         }

         ++nonZero;
      }

      this.mSortedMethods = new MethodData[nonZero];
      int var11 = 0;
      MethodData[] var12 = methods;
      call = methods.length;

      MethodData md;
      int var14;
      for(var14 = 0; var14 < call; ++var14) {
         md = var12[var14];
         if(timeBase.getElapsedInclusiveTime(md) == 0L) {
            break;
         }

         md.setRank(var11);
         this.mSortedMethods[var11++] = md;
      }

      var12 = this.mSortedMethods;
      call = var12.length;

      for(var14 = 0; var14 < call; ++var14) {
         md = var12[var14];
         md.analyzeData(timeBase);
      }

      Iterator var13 = this.mCallList.iterator();

      while(var13.hasNext()) {
         Call var15 = (Call)var13.next();
         var15.updateName();
      }

      if(this.mRegression) {
         this.dumpMethodStats();
      }

   }

   public ArrayList<TimeLineView.Record> getThreadTimeRecords() {
      ArrayList timeRecs = new ArrayList();
      ThreadData[] i$ = this.mSortedThreads;
      int call = i$.length;

      TimeLineView.Record record;
      for(int i$1 = 0; i$1 < call; ++i$1) {
         ThreadData threadData = i$[i$1];
         if(!threadData.isEmpty() && threadData.getId() != 0) {
            record = new TimeLineView.Record(threadData, threadData.getRootCall());
            timeRecs.add(record);
         }
      }

      Iterator var7 = this.mCallList.iterator();

      while(var7.hasNext()) {
         Call var8 = (Call)var7.next();
         record = new TimeLineView.Record(var8.getThreadData(), var8);
         timeRecs.add(record);
      }

      if(this.mRegression) {
         this.dumpTimeRecs(timeRecs);
         System.exit(0);
      }

      return timeRecs;
   }

   private void dumpThreadTimes() {
      System.out.print("\nThread Times\n");
      System.out.print("id  t-start    t-end  g-start    g-end     name\n");
      Iterator i$ = this.mThreadMap.values().iterator();

      while(i$.hasNext()) {
         ThreadData threadData = (ThreadData)i$.next();
         System.out.format("%2d %8d %8d %8d %8d  %s\n", new Object[]{Integer.valueOf(threadData.getId()), Long.valueOf(threadData.mThreadStartTime), Long.valueOf(threadData.mThreadEndTime), Long.valueOf(threadData.mGlobalStartTime), Long.valueOf(threadData.mGlobalEndTime), threadData.getName()});
      }

   }

   private void dumpCallTimes() {
      System.out.print("\nCall Times\n");
      System.out.print("id  t-start    t-end  g-start    g-end    excl.    incl.  method\n");
      Iterator i$ = this.mCallList.iterator();

      while(i$.hasNext()) {
         Call call = (Call)i$.next();
         System.out.format("%2d %8d %8d %8d %8d %8d %8d  %s\n", new Object[]{Integer.valueOf(call.getThreadId()), Long.valueOf(call.mThreadStartTime), Long.valueOf(call.mThreadEndTime), Long.valueOf(call.mGlobalStartTime), Long.valueOf(call.mGlobalEndTime), Long.valueOf(call.mExclusiveCpuTime), Long.valueOf(call.mInclusiveCpuTime), call.getMethodData().getName()});
      }

   }

   private void dumpMethodStats() {
      System.out.print("\nMethod Stats\n");
      System.out.print("Excl Cpu  Incl Cpu  Excl Real Incl Real    Calls  Method\n");
      MethodData[] arr$ = this.mSortedMethods;
      int len$ = arr$.length;

      for(int i$ = 0; i$ < len$; ++i$) {
         MethodData md = arr$[i$];
         System.out.format("%9d %9d %9d %9d %9s  %s\n", new Object[]{Long.valueOf(md.getElapsedExclusiveCpuTime()), Long.valueOf(md.getElapsedInclusiveCpuTime()), Long.valueOf(md.getElapsedExclusiveRealTime()), Long.valueOf(md.getElapsedInclusiveRealTime()), md.getCalls(), md.getProfileName()});
      }

   }

   private void dumpTimeRecs(ArrayList<TimeLineView.Record> timeRecs) {
      System.out.print("\nTime Records\n");
      System.out.print("id  t-start    t-end  g-start    g-end  method\n");
      Iterator i$ = timeRecs.iterator();

      while(i$.hasNext()) {
         TimeLineView.Record record = (TimeLineView.Record)i$.next();
         Call call = (Call)record.block;
         System.out.format("%2d %8d %8d %8d %8d  %s\n", new Object[]{Integer.valueOf(call.getThreadId()), Long.valueOf(call.mThreadStartTime), Long.valueOf(call.mThreadEndTime), Long.valueOf(call.mGlobalStartTime), Long.valueOf(call.mGlobalEndTime), call.getMethodData().getName()});
      }

   }

   public HashMap<Integer, String> getThreadLabels() {
      HashMap labels = new HashMap();
      Iterator i$ = this.mThreadMap.values().iterator();

      while(i$.hasNext()) {
         ThreadData t = (ThreadData)i$.next();
         labels.put(Integer.valueOf(t.getId()), t.getName());
      }

      return labels;
   }

   public MethodData[] getMethods() {
      return this.mSortedMethods;
   }

   public ThreadData[] getThreads() {
      return this.mSortedThreads;
   }

   public long getTotalCpuTime() {
      return this.mTotalCpuTime;
   }

   public long getTotalRealTime() {
      return this.mTotalRealTime;
   }

   public boolean haveCpuTime() {
      return this.mClockSource != DmTraceReader.ClockSource.WALL;
   }

   public boolean haveRealTime() {
      return this.mClockSource != DmTraceReader.ClockSource.THREAD_CPU;
   }

   public HashMap<String, String> getProperties() {
      return this.mPropertiesMap;
   }

   public TimeBase getPreferredTimeBase() {
      return this.mClockSource == DmTraceReader.ClockSource.WALL?TimeBase.REAL_TIME:TimeBase.CPU_TIME;
   }

   public String getClockSource() {
      switch(DmTraceReader.SyntheticClass_1.$SwitchMap$com$android$traceview$DmTraceReader$ClockSource[this.mClockSource.ordinal()]) {
      case 1:
         return "real time";
      case 2:
         return "real time, dual clock";
      case 3:
         return "cpu time";
      default:
         return null;
      }
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] $SwitchMap$com$android$traceview$DmTraceReader$ClockSource = new int[DmTraceReader.ClockSource.values().length];

      static {
         try {
            $SwitchMap$com$android$traceview$DmTraceReader$ClockSource[DmTraceReader.ClockSource.WALL.ordinal()] = 1;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            $SwitchMap$com$android$traceview$DmTraceReader$ClockSource[DmTraceReader.ClockSource.DUAL.ordinal()] = 2;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            $SwitchMap$com$android$traceview$DmTraceReader$ClockSource[DmTraceReader.ClockSource.THREAD_CPU.ordinal()] = 3;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }

   private static enum ClockSource {
      THREAD_CPU,
      WALL,
      DUAL;
   }
}
