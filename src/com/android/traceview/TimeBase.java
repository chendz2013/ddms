package com.android.traceview;

import com.android.traceview.MethodData;
import com.android.traceview.ProfileData;
import com.android.traceview.ThreadData;

interface TimeBase {
   TimeBase CPU_TIME = new TimeBase.CpuTimeBase();
   TimeBase REAL_TIME = new TimeBase.RealTimeBase();

   long getTime(ThreadData var1);

   long getElapsedInclusiveTime(MethodData var1);

   long getElapsedExclusiveTime(MethodData var1);

   long getElapsedInclusiveTime(ProfileData var1);

   public static final class RealTimeBase implements TimeBase {
      public long getTime(ThreadData threadData) {
         return threadData.getRealTime();
      }

      public long getElapsedInclusiveTime(MethodData methodData) {
         return methodData.getElapsedInclusiveRealTime();
      }

      public long getElapsedExclusiveTime(MethodData methodData) {
         return methodData.getElapsedExclusiveRealTime();
      }

      public long getElapsedInclusiveTime(ProfileData profileData) {
         return profileData.getElapsedInclusiveRealTime();
      }
   }

   public static final class CpuTimeBase implements TimeBase {
      public long getTime(ThreadData threadData) {
         return threadData.getCpuTime();
      }

      public long getElapsedInclusiveTime(MethodData methodData) {
         return methodData.getElapsedInclusiveCpuTime();
      }

      public long getElapsedExclusiveTime(MethodData methodData) {
         return methodData.getElapsedExclusiveCpuTime();
      }

      public long getElapsedInclusiveTime(ProfileData profileData) {
         return profileData.getElapsedInclusiveCpuTime();
      }
   }
}
