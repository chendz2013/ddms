package com.android.traceview;

import com.android.traceview.MethodData;
import com.android.traceview.ProfileData;
import com.android.traceview.ProfileNode;
import com.android.traceview.ProfileSelf;
import com.android.traceview.TraceReader;
import com.android.traceview.TraceUnits;
import com.android.utils.SdkUtils;
import java.io.InputStream;
import java.util.Arrays;
import org.eclipse.jface.viewers.IColorProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.swt.widgets.TreeItem;
//profile的数据--列名
class ProfileProvider implements ITreeContentProvider {
   private MethodData[] mRoots;
   private SelectionAdapter mListener;
   private TreeViewer mTreeViewer;
   private TraceReader mReader;
   private Image mSortUp;
   private Image mSortDown;
   private String[] mColumnNames = new String[]{"-方法名称-", "CPU%+", "CPU+", "CPU-%", "CPU-", "实际+%", "实际+", "实际-%", "实际-", "调用次数+递归/总次数", "CPU时间/调用次数", "实际时间/调用次数"};
   private int[] mColumnWidths = new int[]{400, 75, 75, 75, 75, 75, 75, 75, 75, 80, 80, 80};
   private int[] mColumnAlignments = new int[]{16384, 131072, 131072, 131072, 131072, 131072, 131072, 131072, 131072, 16777216, 131072, 131072};
   private static final int COL_NAME = 0;
   private static final int COL_INCLUSIVE_CPU_TIME_PER = 1;
   private static final int COL_INCLUSIVE_CPU_TIME = 2;
   private static final int COL_EXCLUSIVE_CPU_TIME_PER = 3;
   private static final int COL_EXCLUSIVE_CPU_TIME = 4;
   private static final int COL_INCLUSIVE_REAL_TIME_PER = 5;
   private static final int COL_INCLUSIVE_REAL_TIME = 6;
   private static final int COL_EXCLUSIVE_REAL_TIME_PER = 7;
   private static final int COL_EXCLUSIVE_REAL_TIME = 8;
   private static final int COL_CALLS = 9;
   private static final int COL_CPU_TIME_PER_CALL = 10;
   private static final int COL_REAL_TIME_PER_CALL = 11;
   private long mTotalCpuTime;
   private long mTotalRealTime;
   private int mPrevMatchIndex = -1;

   public ProfileProvider(TraceReader reader) {
      this.mRoots = reader.getMethods();
      this.mReader = reader;
      this.mTotalCpuTime = reader.getTotalCpuTime();
      this.mTotalRealTime = reader.getTotalRealTime();
      Display display = Display.getCurrent();
      InputStream in = this.getClass().getClassLoader().getResourceAsStream("icons/sort_up.png");
      this.mSortUp = new Image(display, in);
      in = this.getClass().getClassLoader().getResourceAsStream("icons/sort_down.png");
      this.mSortDown = new Image(display, in);
   }

   private MethodData doMatchName(String name, int startIndex) {
      boolean hasUpper = SdkUtils.hasUpperCaseCharacter(name);

      for(int ii = startIndex; ii < this.mRoots.length; ++ii) {
         MethodData md = this.mRoots[ii];
         String fullName = md.getName();
         if(!hasUpper) {
            fullName = fullName.toLowerCase();
         }

         if(fullName.indexOf(name) != -1) {
            this.mPrevMatchIndex = ii;
            return md;
         }
      }

      this.mPrevMatchIndex = -1;
      return null;
   }

   public MethodData findMatchingName(String name) {
      return this.doMatchName(name, 0);
   }

   public MethodData findNextMatchingName(String name) {
      return this.doMatchName(name, this.mPrevMatchIndex + 1);
   }

   public MethodData findMatchingTreeItem(TreeItem item) {
      if(item == null) {
         return null;
      } else {
         String text = item.getText();
         if(!Character.isDigit(text.charAt(0))) {
            return null;
         } else {
            int spaceIndex = text.indexOf(32);
            String numstr = text.substring(0, spaceIndex);
            int rank = Integer.valueOf(numstr).intValue();
            MethodData[] arr$ = this.mRoots;
            int len$ = arr$.length;

            for(int i$ = 0; i$ < len$; ++i$) {
               MethodData md = arr$[i$];
               if(md.getRank() == rank) {
                  return md;
               }
            }

            return null;
         }
      }
   }

   public void setTreeViewer(TreeViewer treeViewer) {
      this.mTreeViewer = treeViewer;
   }

   public String[] getColumnNames() {
      return this.mColumnNames;
   }

   public int[] getColumnWidths() {
      int[] widths = Arrays.copyOf(this.mColumnWidths, this.mColumnWidths.length);
      if(!this.mReader.haveCpuTime()) {
         widths[4] = 0;
         widths[3] = 0;
         widths[2] = 0;
         widths[1] = 0;
         widths[10] = 0;
      }

      if(!this.mReader.haveRealTime()) {
         widths[8] = 0;
         widths[7] = 0;
         widths[6] = 0;
         widths[5] = 0;
         widths[11] = 0;
      }

      return widths;
   }

   public int[] getColumnAlignments() {
      return this.mColumnAlignments;
   }

   public Object[] getChildren(Object element) {
      if(element instanceof MethodData) {
         MethodData pn1 = (MethodData)element;
         return pn1.getProfileNodes();
      } else if(element instanceof ProfileNode) {
         ProfileNode pn = (ProfileNode)element;
         return pn.getChildren();
      } else {
         return new Object[0];
      }
   }

   public Object getParent(Object element) {
      return null;
   }

   public boolean hasChildren(Object element) {
      return element instanceof MethodData?true:element instanceof ProfileNode;
   }

   public Object[] getElements(Object element) {
      return this.mRoots;
   }

   public void dispose() {
   }

   public void inputChanged(Viewer arg0, Object arg1, Object arg2) {
   }

   public Object getRoot() {
      return "root";
   }

   public SelectionAdapter getColumnListener() {
      if(this.mListener == null) {
         this.mListener = new ProfileProvider.ColumnListener();
      }

      return this.mListener;
   }

   public LabelProvider getLabelProvider() {
      return new ProfileProvider.ProfileLabelProvider();
   }

   class ColumnListener extends SelectionAdapter {
      MethodData.Sorter sorter = new MethodData.Sorter();

      public void widgetSelected(SelectionEvent event) {
         TreeColumn column = (TreeColumn)event.widget;
         String name = column.getText();
         Tree tree = column.getParent();
         tree.setRedraw(false);
         TreeColumn[] columns = tree.getColumns();
         TreeColumn[] direction = columns;
         int len$ = columns.length;

         for(int i$ = 0; i$ < len$; ++i$) {
            TreeColumn col = direction[i$];
            col.setImage((Image)null);
         }

         if(name == ProfileProvider.this.mColumnNames[0]) {
            this.sorter.setColumn(MethodData.Sorter.Column.BY_NAME);
            Arrays.sort(ProfileProvider.this.mRoots, this.sorter);
         } else if(name == ProfileProvider.this.mColumnNames[4]) {
            this.sorter.setColumn(MethodData.Sorter.Column.BY_EXCLUSIVE_CPU_TIME);
            Arrays.sort(ProfileProvider.this.mRoots, this.sorter);
         } else if(name == ProfileProvider.this.mColumnNames[3]) {
            this.sorter.setColumn(MethodData.Sorter.Column.BY_EXCLUSIVE_CPU_TIME);
            Arrays.sort(ProfileProvider.this.mRoots, this.sorter);
         } else if(name == ProfileProvider.this.mColumnNames[2]) {
            this.sorter.setColumn(MethodData.Sorter.Column.BY_INCLUSIVE_CPU_TIME);
            Arrays.sort(ProfileProvider.this.mRoots, this.sorter);
         } else if(name == ProfileProvider.this.mColumnNames[1]) {
            this.sorter.setColumn(MethodData.Sorter.Column.BY_INCLUSIVE_CPU_TIME);
            Arrays.sort(ProfileProvider.this.mRoots, this.sorter);
         } else if(name == ProfileProvider.this.mColumnNames[8]) {
            this.sorter.setColumn(MethodData.Sorter.Column.BY_EXCLUSIVE_REAL_TIME);
            Arrays.sort(ProfileProvider.this.mRoots, this.sorter);
         } else if(name == ProfileProvider.this.mColumnNames[7]) {
            this.sorter.setColumn(MethodData.Sorter.Column.BY_EXCLUSIVE_REAL_TIME);
            Arrays.sort(ProfileProvider.this.mRoots, this.sorter);
         } else if(name == ProfileProvider.this.mColumnNames[6]) {
            this.sorter.setColumn(MethodData.Sorter.Column.BY_INCLUSIVE_REAL_TIME);
            Arrays.sort(ProfileProvider.this.mRoots, this.sorter);
         } else if(name == ProfileProvider.this.mColumnNames[5]) {
            this.sorter.setColumn(MethodData.Sorter.Column.BY_INCLUSIVE_REAL_TIME);
            Arrays.sort(ProfileProvider.this.mRoots, this.sorter);
         } else if(name == ProfileProvider.this.mColumnNames[9]) {
            this.sorter.setColumn(MethodData.Sorter.Column.BY_CALLS);
            Arrays.sort(ProfileProvider.this.mRoots, this.sorter);
         } else if(name == ProfileProvider.this.mColumnNames[10]) {
            this.sorter.setColumn(MethodData.Sorter.Column.BY_CPU_TIME_PER_CALL);
            Arrays.sort(ProfileProvider.this.mRoots, this.sorter);
         } else if(name == ProfileProvider.this.mColumnNames[11]) {
            this.sorter.setColumn(MethodData.Sorter.Column.BY_REAL_TIME_PER_CALL);
            Arrays.sort(ProfileProvider.this.mRoots, this.sorter);
         }

         MethodData.Sorter.Direction var10 = this.sorter.getDirection();
         if(var10 == MethodData.Sorter.Direction.INCREASING) {
            column.setImage(ProfileProvider.this.mSortDown);
         } else {
            column.setImage(ProfileProvider.this.mSortUp);
         }

         tree.setRedraw(true);
         ProfileProvider.this.mTreeViewer.refresh();
      }
   }

   class ProfileLabelProvider extends LabelProvider implements ITableLabelProvider, IColorProvider {
      Color colorRed;
      Color colorParentsBack;
      Color colorChildrenBack;
      TraceUnits traceUnits;

      public ProfileLabelProvider() {
         Display display = Display.getCurrent();
         this.colorRed = display.getSystemColor(3);
         this.colorParentsBack = new Color(display, 230, 230, 255);
         this.colorChildrenBack = new Color(display, 255, 255, 210);
         this.traceUnits = ProfileProvider.this.mReader.getTraceUnits();
      }

      public String getColumnText(Object element, int col) {
         double val1;
         double total1;
         if(element instanceof MethodData) {
            MethodData pn = (MethodData)element;
            if(col == 0) {
               return pn.getProfileName();
            }

            if(col == 4) {
               total1 = (double)pn.getElapsedExclusiveCpuTime();
               total1 = this.traceUnits.getScaledValue(total1);
               return String.format("%.3f", new Object[]{Double.valueOf(total1)});
            }

            if(col == 3) {
               total1 = (double)pn.getElapsedExclusiveCpuTime();
               val1 = total1 * 100.0D / (double)ProfileProvider.this.mTotalCpuTime;
               return String.format("%.1f%%", new Object[]{Double.valueOf(val1)});
            }

            if(col == 2) {
               total1 = (double)pn.getElapsedInclusiveCpuTime();
               total1 = this.traceUnits.getScaledValue(total1);
               return String.format("%.3f", new Object[]{Double.valueOf(total1)});
            }

            if(col == 1) {
               total1 = (double)pn.getElapsedInclusiveCpuTime();
               val1 = total1 * 100.0D / (double)ProfileProvider.this.mTotalCpuTime;
               return String.format("%.1f%%", new Object[]{Double.valueOf(val1)});
            }

            if(col == 8) {
               total1 = (double)pn.getElapsedExclusiveRealTime();
               total1 = this.traceUnits.getScaledValue(total1);
               return String.format("%.3f", new Object[]{Double.valueOf(total1)});
            }

            if(col == 7) {
               total1 = (double)pn.getElapsedExclusiveRealTime();
               val1 = total1 * 100.0D / (double)ProfileProvider.this.mTotalRealTime;
               return String.format("%.1f%%", new Object[]{Double.valueOf(val1)});
            }

            if(col == 6) {
               total1 = (double)pn.getElapsedInclusiveRealTime();
               total1 = this.traceUnits.getScaledValue(total1);
               return String.format("%.3f", new Object[]{Double.valueOf(total1)});
            }

            if(col == 5) {
               total1 = (double)pn.getElapsedInclusiveRealTime();
               val1 = total1 * 100.0D / (double)ProfileProvider.this.mTotalRealTime;
               return String.format("%.1f%%", new Object[]{Double.valueOf(val1)});
            }

            if(col == 9) {
               return pn.getCalls();
            }

            int total;
            double val;
            if(col == 10) {
               total = pn.getTotalCalls();
               val = (double)pn.getElapsedInclusiveCpuTime();
               val /= (double)total;
               val = this.traceUnits.getScaledValue(val);
               return String.format("%.3f", new Object[]{Double.valueOf(val)});
            }

            if(col == 11) {
               total = pn.getTotalCalls();
               val = (double)pn.getElapsedInclusiveRealTime();
               val /= (double)total;
               val = this.traceUnits.getScaledValue(val);
               return String.format("%.3f", new Object[]{Double.valueOf(val)});
            }
         } else {
            MethodData context;
            double per;
            if(element instanceof ProfileSelf) {
               ProfileSelf pn3 = (ProfileSelf)element;
               if(col == 0) {
                  return pn3.getProfileName();
               }

               if(col == 2) {
                  total1 = (double)pn3.getElapsedInclusiveCpuTime();
                  total1 = this.traceUnits.getScaledValue(total1);
                  return String.format("%.3f", new Object[]{Double.valueOf(total1)});
               }

               if(col == 1) {
                  val1 = (double)pn3.getElapsedInclusiveCpuTime();
                  context = pn3.getContext();
                  total1 = (double)context.getElapsedInclusiveCpuTime();
                  per = val1 * 100.0D / total1;
                  return String.format("%.1f%%", new Object[]{Double.valueOf(per)});
               }

               if(col == 6) {
                  total1 = (double)pn3.getElapsedInclusiveRealTime();
                  total1 = this.traceUnits.getScaledValue(total1);
                  return String.format("%.3f", new Object[]{Double.valueOf(total1)});
               }

               if(col == 5) {
                  val1 = (double)pn3.getElapsedInclusiveRealTime();
                  context = pn3.getContext();
                  total1 = (double)context.getElapsedInclusiveRealTime();
                  per = val1 * 100.0D / total1;
                  return String.format("%.1f%%", new Object[]{Double.valueOf(per)});
               }

               return "";
            }

            if(element instanceof ProfileData) {
               ProfileData pn2 = (ProfileData)element;
               if(col == 0) {
                  return pn2.getProfileName();
               }

               if(col == 2) {
                  total1 = (double)pn2.getElapsedInclusiveCpuTime();
                  total1 = this.traceUnits.getScaledValue(total1);
                  return String.format("%.3f", new Object[]{Double.valueOf(total1)});
               }

               if(col == 1) {
                  val1 = (double)pn2.getElapsedInclusiveCpuTime();
                  context = pn2.getContext();
                  total1 = (double)context.getElapsedInclusiveCpuTime();
                  per = val1 * 100.0D / total1;
                  return String.format("%.1f%%", new Object[]{Double.valueOf(per)});
               }

               if(col == 6) {
                  total1 = (double)pn2.getElapsedInclusiveRealTime();
                  total1 = this.traceUnits.getScaledValue(total1);
                  return String.format("%.3f", new Object[]{Double.valueOf(total1)});
               }

               if(col == 5) {
                  val1 = (double)pn2.getElapsedInclusiveRealTime();
                  context = pn2.getContext();
                  total1 = (double)context.getElapsedInclusiveRealTime();
                  per = val1 * 100.0D / total1;
                  return String.format("%.1f%%", new Object[]{Double.valueOf(per)});
               }

               if(col == 9) {
                  return pn2.getNumCalls();
               }

               return "";
            }

            if(element instanceof ProfileNode) {
               ProfileNode pn1 = (ProfileNode)element;
               if(col == 0) {
                  return pn1.getLabel();
               }

               return "";
            }
         }

         return "col" + col;
      }

      public Image getColumnImage(Object element, int col) {
         if(col != 0) {
            return null;
         } else if(element instanceof MethodData) {
            MethodData pd1 = (MethodData)element;
            return pd1.getImage();
         } else if(element instanceof ProfileData) {
            ProfileData pd = (ProfileData)element;
            MethodData md = pd.getMethodData();
            return md.getImage();
         } else {
            return null;
         }
      }

      public Color getForeground(Object element) {
         return null;
      }

      public Color getBackground(Object element) {
         if(element instanceof ProfileData) {
            ProfileData pn1 = (ProfileData)element;
            return pn1.isParent()?this.colorParentsBack:this.colorChildrenBack;
         } else if(element instanceof ProfileNode) {
            ProfileNode pn = (ProfileNode)element;
            return pn.isParent()?this.colorParentsBack:this.colorChildrenBack;
         } else {
            return null;
         }
      }
   }
}
