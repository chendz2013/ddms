package com.android.traceview;

import com.android.traceview.Call;
import com.android.traceview.ProfileData;
import com.android.traceview.ProfileNode;
import com.android.traceview.ProfileSelf;
import com.android.traceview.TimeBase;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Image;

/**
 * 每个method的统计数据
 */
public class MethodData {
   private int mId;
   private int mRank = -1;
   private String mClassName;
   private String mMethodName;
   private String mSignature;
   private String mName;
   private String mProfileName;
   private String mPathname;
   private int mLineNumber;
   private long mElapsedExclusiveCpuTime;
   private long mElapsedInclusiveCpuTime;
   private long mTopExclusiveCpuTime;
   private long mElapsedExclusiveRealTime;
   private long mElapsedInclusiveRealTime;
   private long mTopExclusiveRealTime;
   private int[] mNumCalls = new int[2];
   private Color mColor;
   private Color mFadedColor;
   private Image mImage;
   private Image mFadedImage;
   private HashMap<Integer, ProfileData> mParents;
   private HashMap<Integer, ProfileData> mChildren;
   private HashMap<Integer, ProfileData> mRecursiveParents;
   private HashMap<Integer, ProfileData> mRecursiveChildren;
   private ProfileNode[] mProfileNodes;
   private int mX;
   private int mY;
   private double mWeight;

   public MethodData(int id, String className) {
      this.mId = id;
      this.mClassName = className;
      this.mMethodName = null;
      this.mSignature = null;
      this.mPathname = null;
      this.mLineNumber = -1;
      this.computeName();
      this.computeProfileName();
   }

   public MethodData(int id, String className, String methodName, String signature, String pathname, int lineNumber) {
      this.mId = id;
      this.mClassName = className;
      this.mMethodName = methodName;
      this.mSignature = signature;
      this.mPathname = pathname;
      this.mLineNumber = lineNumber;
      this.computeName();
      this.computeProfileName();
   }

   public double addWeight(int x, int y, double weight) {
      if(this.mX == x && this.mY == y) {
         this.mWeight += weight;
      } else {
         this.mX = x;
         this.mY = y;
         this.mWeight = weight;
      }

      return this.mWeight;
   }

   public void clearWeight() {
      this.mWeight = 0.0D;
   }

   public int getRank() {
      return this.mRank;
   }

   public void setRank(int rank) {
      this.mRank = rank;
      this.computeProfileName();
   }

   public void addElapsedExclusive(long cpuTime, long realTime) {
      this.mElapsedExclusiveCpuTime += cpuTime;
      this.mElapsedExclusiveRealTime += realTime;
   }

   public void addElapsedInclusive(long cpuTime, long realTime, boolean isRecursive, Call parent) {
      if(!isRecursive) {
         this.mElapsedInclusiveCpuTime += cpuTime;
         this.mElapsedInclusiveRealTime += realTime;
         ++this.mNumCalls[0];
      } else {
         ++this.mNumCalls[1];
      }

      if(parent != null) {
         MethodData parentMethod = parent.getMethodData();
         if(parent.isRecursive()) {
            parentMethod.mRecursiveChildren = this.updateInclusive(cpuTime, realTime, parentMethod, this, false, parentMethod.mRecursiveChildren);
         } else {
            parentMethod.mChildren = this.updateInclusive(cpuTime, realTime, parentMethod, this, false, parentMethod.mChildren);
         }

         if(isRecursive) {
            this.mRecursiveParents = this.updateInclusive(cpuTime, realTime, this, parentMethod, true, this.mRecursiveParents);
         } else {
            this.mParents = this.updateInclusive(cpuTime, realTime, this, parentMethod, true, this.mParents);
         }

      }
   }

   private HashMap<Integer, ProfileData> updateInclusive(long cpuTime, long realTime, MethodData contextMethod, MethodData elementMethod, boolean elementIsParent, HashMap<Integer, ProfileData> map) {
      ProfileData elementData;
      if(map == null) {
         map = new HashMap(4);
      } else {
         elementData = (ProfileData)map.get(Integer.valueOf(elementMethod.mId));
         if(elementData != null) {
            elementData.addElapsedInclusive(cpuTime, realTime);
            return map;
         }
      }

      elementData = new ProfileData(contextMethod, elementMethod, elementIsParent);
      elementData.setElapsedInclusive(cpuTime, realTime);
      elementData.setNumCalls(1);
      map.put(Integer.valueOf(elementMethod.mId), elementData);
      return map;
   }

   public void analyzeData(TimeBase timeBase) {
      ProfileData[] sortedParents = this.sortProfileData(this.mParents, timeBase);
      ProfileData[] sortedChildren = this.sortProfileData(this.mChildren, timeBase);
      ProfileData[] sortedRecursiveParents = this.sortProfileData(this.mRecursiveParents, timeBase);
      ProfileData[] sortedRecursiveChildren = this.sortProfileData(this.mRecursiveChildren, timeBase);
      sortedChildren = this.addSelf(sortedChildren);
      ArrayList nodes = new ArrayList();
      ProfileNode profileNode;
      if(this.mParents != null) {
         profileNode = new ProfileNode("父亲", this, sortedParents, true, false);
         nodes.add(profileNode);
      }

      if(this.mChildren != null) {
         profileNode = new ProfileNode("孩子", this, sortedChildren, false, false);
         nodes.add(profileNode);
      }

      if(this.mRecursiveParents != null) {
         profileNode = new ProfileNode("父亲(递归)", this, sortedRecursiveParents, true, true);
         nodes.add(profileNode);
      }

      if(this.mRecursiveChildren != null) {
         profileNode = new ProfileNode("孩子(递归)", this, sortedRecursiveChildren, false, true);
         nodes.add(profileNode);
      }

      this.mProfileNodes = (ProfileNode[])nodes.toArray(new ProfileNode[nodes.size()]);
   }

   private ProfileData[] sortProfileData(HashMap<Integer, ProfileData> map, final TimeBase timeBase) {
      if(map == null) {
         return null;
      } else {
         Collection values = map.values();
         ProfileData[] sorted = (ProfileData[])values.toArray(new ProfileData[values.size()]);
         Arrays.sort(sorted, new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
               return compare((ProfileData)o1, (ProfileData)o2);
            }

            public int compare(ProfileData pd1, ProfileData pd2) {
               return timeBase.getElapsedInclusiveTime(pd2) > timeBase.getElapsedInclusiveTime(pd1)?1:(timeBase.getElapsedInclusiveTime(pd2) < timeBase.getElapsedInclusiveTime(pd1)?-1:0);
            }
         });
         return sorted;
      }
   }

   private ProfileData[] addSelf(ProfileData[] children) {
      ProfileData[] pdata;
      if(children == null) {
         pdata = new ProfileData[1];
      } else {
         pdata = new ProfileData[children.length + 1];
         System.arraycopy(children, 0, pdata, 1, children.length);
      }

      pdata[0] = new ProfileSelf(this);
      return pdata;
   }

   public void addTopExclusive(long cpuTime, long realTime) {
      this.mTopExclusiveCpuTime += cpuTime;
      this.mTopExclusiveRealTime += realTime;
   }

   public long getTopExclusiveCpuTime() {
      return this.mTopExclusiveCpuTime;
   }

   public long getTopExclusiveRealTime() {
      return this.mTopExclusiveRealTime;
   }

   public int getId() {
      return this.mId;
   }

   private void computeName() {
      if(this.mMethodName == null) {
         this.mName = this.mClassName;
      } else {
         StringBuilder sb = new StringBuilder();
         sb.append(this.mClassName);
         sb.append(".");
         sb.append(this.mMethodName);
         sb.append(" ");
         sb.append(this.mSignature);
         this.mName = sb.toString();
      }
   }

   public String getName() {
      return this.mName;
   }

   public String getClassName() {
      return this.mClassName;
   }

   public String getMethodName() {
      return this.mMethodName;
   }

   public String getProfileName() {
      return this.mProfileName;
   }

   public String getSignature() {
      return this.mSignature;
   }

   public void computeProfileName() {
      if(this.mRank == -1) {
         this.mProfileName = this.mName;
      } else {
         StringBuilder sb = new StringBuilder();
         sb.append(this.mRank);
         sb.append(" ");
         sb.append(this.getName());
         this.mProfileName = sb.toString();
      }
   }

   public String getCalls() {
      return String.format("%d+%d", new Object[]{Integer.valueOf(this.mNumCalls[0]), Integer.valueOf(this.mNumCalls[1])});
   }

   public int getTotalCalls() {
      return this.mNumCalls[0] + this.mNumCalls[1];
   }

   public Color getColor() {
      return this.mColor;
   }

   public void setColor(Color color) {
      this.mColor = color;
   }

   public void setImage(Image image) {
      this.mImage = image;
   }

   public Image getImage() {
      return this.mImage;
   }

   public String toString() {
      return this.getName();
   }

   public long getElapsedExclusiveCpuTime() {
      return this.mElapsedExclusiveCpuTime;
   }

   public long getElapsedExclusiveRealTime() {
      return this.mElapsedExclusiveRealTime;
   }

   public long getElapsedInclusiveCpuTime() {
      return this.mElapsedInclusiveCpuTime;
   }

   public long getElapsedInclusiveRealTime() {
      return this.mElapsedInclusiveRealTime;
   }

   public void setFadedColor(Color fadedColor) {
      this.mFadedColor = fadedColor;
   }

   public Color getFadedColor() {
      return this.mFadedColor;
   }

   public void setFadedImage(Image fadedImage) {
      this.mFadedImage = fadedImage;
   }

   public Image getFadedImage() {
      return this.mFadedImage;
   }

   public void setPathname(String pathname) {
      this.mPathname = pathname;
   }

   public String getPathname() {
      return this.mPathname;
   }

   public void setLineNumber(int lineNumber) {
      this.mLineNumber = lineNumber;
   }

   public int getLineNumber() {
      return this.mLineNumber;
   }

   public ProfileNode[] getProfileNodes() {
      return this.mProfileNodes;
   }

   public static class Sorter implements Comparator<MethodData> {
      private MethodData.Sorter.Column mColumn;
      private MethodData.Sorter.Direction mDirection;

      public int compare(MethodData md1, MethodData md2) {
         int time11;
         if(this.mColumn == MethodData.Sorter.Column.BY_NAME) {
            time11 = md1.getName().compareTo(md2.getName());
            return this.mDirection == MethodData.Sorter.Direction.INCREASING?time11:-time11;
         } else if(this.mColumn == MethodData.Sorter.Column.BY_INCLUSIVE_CPU_TIME) {
            return md2.getElapsedInclusiveCpuTime() > md1.getElapsedInclusiveCpuTime()?(this.mDirection == MethodData.Sorter.Direction.INCREASING?-1:1):(md2.getElapsedInclusiveCpuTime() < md1.getElapsedInclusiveCpuTime()?(this.mDirection == MethodData.Sorter.Direction.INCREASING?1:-1):md1.getName().compareTo(md2.getName()));
         } else if(this.mColumn == MethodData.Sorter.Column.BY_EXCLUSIVE_CPU_TIME) {
            return md2.getElapsedExclusiveCpuTime() > md1.getElapsedExclusiveCpuTime()?(this.mDirection == MethodData.Sorter.Direction.INCREASING?-1:1):(md2.getElapsedExclusiveCpuTime() < md1.getElapsedExclusiveCpuTime()?(this.mDirection == MethodData.Sorter.Direction.INCREASING?1:-1):md1.getName().compareTo(md2.getName()));
         } else if(this.mColumn == MethodData.Sorter.Column.BY_INCLUSIVE_REAL_TIME) {
            return md2.getElapsedInclusiveRealTime() > md1.getElapsedInclusiveRealTime()?(this.mDirection == MethodData.Sorter.Direction.INCREASING?-1:1):(md2.getElapsedInclusiveRealTime() < md1.getElapsedInclusiveRealTime()?(this.mDirection == MethodData.Sorter.Direction.INCREASING?1:-1):md1.getName().compareTo(md2.getName()));
         } else if(this.mColumn == MethodData.Sorter.Column.BY_EXCLUSIVE_REAL_TIME) {
            return md2.getElapsedExclusiveRealTime() > md1.getElapsedExclusiveRealTime()?(this.mDirection == MethodData.Sorter.Direction.INCREASING?-1:1):(md2.getElapsedExclusiveRealTime() < md1.getElapsedExclusiveRealTime()?(this.mDirection == MethodData.Sorter.Direction.INCREASING?1:-1):md1.getName().compareTo(md2.getName()));
         } else if(this.mColumn == MethodData.Sorter.Column.BY_CALLS) {
            time11 = md1.getTotalCalls() - md2.getTotalCalls();
            return time11 == 0?md1.getName().compareTo(md2.getName()):(this.mDirection == MethodData.Sorter.Direction.INCREASING?time11:-time11);
         } else {
            double time1;
            double time2;
            double diff;
            byte result;
            if(this.mColumn == MethodData.Sorter.Column.BY_CPU_TIME_PER_CALL) {
               time1 = (double)md1.getElapsedInclusiveCpuTime();
               time1 /= (double)md1.getTotalCalls();
               time2 = (double)md2.getElapsedInclusiveCpuTime();
               time2 /= (double)md2.getTotalCalls();
               diff = time1 - time2;
               result = 0;
               if(diff < 0.0D) {
                  result = -1;
               } else if(diff > 0.0D) {
                  result = 1;
               }

               return result == 0?md1.getName().compareTo(md2.getName()):(this.mDirection == MethodData.Sorter.Direction.INCREASING?result:-result);
            } else if(this.mColumn == MethodData.Sorter.Column.BY_REAL_TIME_PER_CALL) {
               time1 = (double)md1.getElapsedInclusiveRealTime();
               time1 /= (double)md1.getTotalCalls();
               time2 = (double)md2.getElapsedInclusiveRealTime();
               time2 /= (double)md2.getTotalCalls();
               diff = time1 - time2;
               result = 0;
               if(diff < 0.0D) {
                  result = -1;
               } else if(diff > 0.0D) {
                  result = 1;
               }

               return result == 0?md1.getName().compareTo(md2.getName()):(this.mDirection == MethodData.Sorter.Direction.INCREASING?result:-result);
            } else {
               return 0;
            }
         }
      }

      public void setColumn(MethodData.Sorter.Column column) {
         if(this.mColumn == column) {
            if(this.mDirection == MethodData.Sorter.Direction.INCREASING) {
               this.mDirection = MethodData.Sorter.Direction.DECREASING;
            } else {
               this.mDirection = MethodData.Sorter.Direction.INCREASING;
            }
         } else if(column == MethodData.Sorter.Column.BY_NAME) {
            this.mDirection = MethodData.Sorter.Direction.INCREASING;
         } else {
            this.mDirection = MethodData.Sorter.Direction.DECREASING;
         }

         this.mColumn = column;
      }

      public MethodData.Sorter.Column getColumn() {
         return this.mColumn;
      }

      public void setDirection(MethodData.Sorter.Direction direction) {
         this.mDirection = direction;
      }

      public MethodData.Sorter.Direction getDirection() {
         return this.mDirection;
      }

      public static enum Direction {
         INCREASING,
         DECREASING;
      }

      public static enum Column {
         BY_NAME,
         BY_EXCLUSIVE_CPU_TIME,
         BY_EXCLUSIVE_REAL_TIME,
         BY_INCLUSIVE_CPU_TIME,
         BY_INCLUSIVE_REAL_TIME,
         BY_CALLS,
         BY_REAL_TIME_PER_CALL,
         BY_CPU_TIME_PER_CALL;
      }
   }
}
