package com.android.traceview;

import com.android.traceview.MethodData;
import com.android.traceview.ProfileData;

public class ProfileNode {
   private String mLabel;
   private MethodData mMethodData;
   private ProfileData[] mChildren;
   private boolean mIsParent;
   private boolean mIsRecursive;

   public ProfileNode(String label, MethodData methodData, ProfileData[] children, boolean isParent, boolean isRecursive) {
      this.mLabel = label;
      this.mMethodData = methodData;
      this.mChildren = children;
      this.mIsParent = isParent;
      this.mIsRecursive = isRecursive;
   }

   public String getLabel() {
      return this.mLabel;
   }

   public ProfileData[] getChildren() {
      return this.mChildren;
   }

   public boolean isParent() {
      return this.mIsParent;
   }

   public boolean isRecursive() {
      return this.mIsRecursive;
   }
}
