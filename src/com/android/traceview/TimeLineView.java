package com.android.traceview;

import com.android.traceview.Call;
import com.android.traceview.MethodData;
import com.android.traceview.Selection;
import com.android.traceview.SelectionController;
import com.android.traceview.TickScaler;
import com.android.traceview.TraceReader;
import com.android.traceview.TraceUnits;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;
import org.eclipse.jface.resource.FontRegistry;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseMoveListener;
import org.eclipse.swt.events.MouseWheelListener;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.ScrollBar;
//时间轴
public class TimeLineView extends Composite implements Observer {
   private HashMap<String, TimeLineView.RowData> mRowByName = new HashMap();
   private TimeLineView.RowData[] mRows;
   private TimeLineView.Segment[] mSegments;
   private HashMap<Integer, String> mThreadLabels;
   private TimeLineView.Timescale mTimescale;
   private TimeLineView.Surface mSurface;
   private TimeLineView.RowLabels mLabels;
   private SashForm mSashForm;
   private int mScrollOffsetY;
   public static final int PixelsPerTick = 50;
   private TickScaler mScaleInfo = new TickScaler(0.0D, 0.0D, 0, 50);
   private static final int LeftMargin = 10;
   private static final int RightMargin = 60;
   private Color mColorBlack;
   private Color mColorGray;
   private Color mColorDarkGray;
   private Color mColorForeground;
   private Color mColorRowBack;
   private Color mColorZoomSelection;
   private FontRegistry mFontRegistry;
   private static final int rowHeight = 20;
   private static final int rowYMargin = 12;
   private static final int rowYMarginHalf = 6;
   private static final int rowYSpace = 32;
   private static final int majorTickLength = 8;
   private static final int minorTickLength = 4;
   private static final int timeLineOffsetY = 58;
   private static final int tickToFontSpacing = 2;
   private static final int topMargin = 90;
   private int mMouseRow = -1;
   private int mNumRows;
   private int mStartRow;
   private int mEndRow;
   private TraceUnits mUnits;
   private String mClockSource;
   private boolean mHaveCpuTime;
   private boolean mHaveRealTime;
   private int mSmallFontWidth;
   private int mSmallFontHeight;
   private SelectionController mSelectionController;
   private MethodData mHighlightMethodData;
   private Call mHighlightCall;
   private static final int MinInclusiveRange = 3;
   private boolean mSetFonts = false;

   public TimeLineView(Composite parent, TraceReader reader, SelectionController selectionController) {
      super(parent, 0);
      this.mSelectionController = selectionController;
      selectionController.addObserver(this);
      this.mUnits = reader.getTraceUnits();
      this.mClockSource = reader.getClockSource();
      this.mHaveCpuTime = reader.haveCpuTime();
      this.mHaveRealTime = reader.haveRealTime();
      this.mThreadLabels = reader.getThreadLabels();
      Display display = this.getDisplay();
      this.mColorGray = display.getSystemColor(15);
      this.mColorDarkGray = display.getSystemColor(16);
      this.mColorBlack = display.getSystemColor(2);
      this.mColorForeground = display.getSystemColor(2);
      this.mColorRowBack = new Color(display, 240, 240, 255);
      this.mColorZoomSelection = new Color(display, 230, 230, 230);
      this.mFontRegistry = new FontRegistry(display);
      this.mFontRegistry.put("small", new FontData[]{new FontData("Arial", 8, 0)});
      this.mFontRegistry.put("courier8", new FontData[]{new FontData("Courier New", 8, 1)});
      this.mFontRegistry.put("medium", new FontData[]{new FontData("Courier New", 10, 0)});
      Image image = new Image(display, new Rectangle(100, 100, 100, 100));
      GC gc = new GC(image);
      if(this.mSetFonts) {
         gc.setFont(this.mFontRegistry.get("small"));
      }

      this.mSmallFontWidth = gc.getFontMetrics().getAverageCharWidth();
      this.mSmallFontHeight = gc.getFontMetrics().getHeight();
      image.dispose();
      gc.dispose();
      this.setLayout(new FillLayout());
      this.mSashForm = new SashForm(this, 256);
      this.mSashForm.setBackground(this.mColorGray);
      this.mSashForm.SASH_WIDTH = 3;
      Composite composite = new Composite(this.mSashForm, 0);
      GridLayout layout = new GridLayout(1, true);
      layout.marginHeight = 0;
      layout.marginWidth = 0;
      layout.verticalSpacing = 1;
      composite.setLayout(layout);
      TimeLineView.BlankCorner corner = new TimeLineView.BlankCorner(composite);
      GridData gridData = new GridData(768);
      gridData.heightHint = 90;
      corner.setLayoutData(gridData);
      this.mLabels = new TimeLineView.RowLabels(composite);
      gridData = new GridData(1808);
      this.mLabels.setLayoutData(gridData);
      composite = new Composite(this.mSashForm, 0);
      layout = new GridLayout(1, true);
      layout.marginHeight = 0;
      layout.marginWidth = 0;
      layout.verticalSpacing = 1;
      composite.setLayout(layout);
      this.mTimescale = new TimeLineView.Timescale(composite);
      gridData = new GridData(768);
      gridData.heightHint = 90;
      this.mTimescale.setLayoutData(gridData);
      this.mSurface = new TimeLineView.Surface(composite);
      gridData = new GridData(1808);
      this.mSurface.setLayoutData(gridData);
      this.mSashForm.setWeights(new int[]{1, 5});
      final ScrollBar vBar = this.mSurface.getVerticalBar();
      vBar.addListener(13, new Listener() {
         public void handleEvent(Event e) {
            TimeLineView.this.mScrollOffsetY = vBar.getSelection();
            Point dim = TimeLineView.this.mSurface.getSize();
            int newScrollOffsetY = TimeLineView.this.computeVisibleRows(dim.y);
            if(newScrollOffsetY != TimeLineView.this.mScrollOffsetY) {
               TimeLineView.this.mScrollOffsetY = newScrollOffsetY;
               vBar.setSelection(newScrollOffsetY);
            }

            TimeLineView.this.mLabels.redraw();
            TimeLineView.this.mSurface.redraw();
         }
      });
      final ScrollBar hBar = this.mSurface.getHorizontalBar();
      hBar.addListener(13, new Listener() {
         public void handleEvent(Event e) {
            TimeLineView.this.mSurface.setScaleFromHorizontalScrollBar(hBar.getSelection());
            TimeLineView.this.mSurface.redraw();
         }
      });
      this.mSurface.addListener(11, new Listener() {
         public void handleEvent(Event e) {
            Point dim = TimeLineView.this.mSurface.getSize();
            if(dim.y >= TimeLineView.this.mNumRows * 32) {
               vBar.setVisible(false);
            } else {
               vBar.setVisible(true);
            }

            int newScrollOffsetY = TimeLineView.this.computeVisibleRows(dim.y);
            if(newScrollOffsetY != TimeLineView.this.mScrollOffsetY) {
               TimeLineView.this.mScrollOffsetY = newScrollOffsetY;
               vBar.setSelection(newScrollOffsetY);
            }

            int spaceNeeded = TimeLineView.this.mNumRows * 32;
            vBar.setMaximum(spaceNeeded);
            vBar.setThumb(dim.y);
            TimeLineView.this.mLabels.redraw();
            TimeLineView.this.mSurface.redraw();
         }
      });
      this.mSurface.addMouseListener(new MouseAdapter() {
         public void mouseUp(MouseEvent me) {
            TimeLineView.this.mSurface.mouseUp(me);
         }

         public void mouseDown(MouseEvent me) {
            TimeLineView.this.mSurface.mouseDown(me);
         }

         public void mouseDoubleClick(MouseEvent me) {
            TimeLineView.this.mSurface.mouseDoubleClick(me);
         }
      });
      this.mSurface.addMouseMoveListener(new MouseMoveListener() {
         public void mouseMove(MouseEvent me) {
            TimeLineView.this.mSurface.mouseMove(me);
         }
      });
      this.mSurface.addMouseWheelListener(new MouseWheelListener() {
         public void mouseScrolled(MouseEvent me) {
            TimeLineView.this.mSurface.mouseScrolled(me);
         }
      });
      this.mTimescale.addMouseListener(new MouseAdapter() {
         public void mouseUp(MouseEvent me) {
            TimeLineView.this.mTimescale.mouseUp(me);
         }

         public void mouseDown(MouseEvent me) {
            TimeLineView.this.mTimescale.mouseDown(me);
         }

         public void mouseDoubleClick(MouseEvent me) {
            TimeLineView.this.mTimescale.mouseDoubleClick(me);
         }
      });
      this.mTimescale.addMouseMoveListener(new MouseMoveListener() {
         public void mouseMove(MouseEvent me) {
            TimeLineView.this.mTimescale.mouseMove(me);
         }
      });
      this.mLabels.addMouseMoveListener(new MouseMoveListener() {
         public void mouseMove(MouseEvent me) {
            TimeLineView.this.mLabels.mouseMove(me);
         }
      });
      this.setData(reader.getThreadTimeRecords());
   }

   public void update(Observable objservable, Object arg) {
      if(arg != "TimeLineView") {
         boolean foundHighlight = false;
         ArrayList selections = this.mSelectionController.getSelections();
         Iterator i$ = selections.iterator();

         while(i$.hasNext()) {
            Selection selection = (Selection)i$.next();
            Selection.Action action = selection.getAction();
            if(action == Selection.Action.Highlight) {
               String name = selection.getName();
               if(name == "MethodData") {
                  foundHighlight = true;
                  this.mHighlightMethodData = (MethodData)selection.getValue();
                  this.mHighlightCall = null;
                  this.startHighlighting();
               } else if(name == "Call") {
                  foundHighlight = true;
                  this.mHighlightCall = (Call)selection.getValue();
                  this.mHighlightMethodData = null;
                  this.startHighlighting();
               }
            }
         }

         if(!foundHighlight) {
            this.mSurface.clearHighlights();
         }

      }
   }

   public void setData(ArrayList<TimeLineView.Record> records) {
      if(records == null) {
         records = new ArrayList();
      }

      Collections.sort(records, new Comparator() {
         @Override
         public int compare(Object o1, Object o2) {
            return compare((TimeLineView.Record)o1,(TimeLineView.Record)o2);
         }

         public int compare(TimeLineView.Record r1, TimeLineView.Record r2) {
            long start1 = r1.block.getStartTime();
            long start2 = r2.block.getStartTime();
            if(start1 > start2) {
               return 1;
            } else if(start1 < start2) {
               return -1;
            } else {
               long end1 = r1.block.getEndTime();
               long end2 = r2.block.getEndTime();
               return end1 > end2?-1:(end1 < end2?1:0);
            }
         }
      });
      ArrayList segmentList = new ArrayList();
      double minVal = 0.0D;
      if(records.size() > 0) {
         minVal = (double)((TimeLineView.Record)records.get(0)).block.getStartTime();
      }

      double maxVal = 0.0D;
      Iterator rv = records.iterator();

      while(rv.hasNext()) {
         TimeLineView.Record ii = (TimeLineView.Record)rv.next();
         TimeLineView.Row top = ii.row;
         TimeLineView.Block block = ii.block;
         if(!block.isIgnoredBlock()) {
            String rowName = top.getName();
            TimeLineView.RowData rd = (TimeLineView.RowData)this.mRowByName.get(rowName);
            if(rd == null) {
               rd = new TimeLineView.RowData(top);
               this.mRowByName.put(rowName, rd);
            }

            long blockStartTime = block.getStartTime();
            long blockEndTime = block.getEndTime();
            if(blockEndTime > rd.mEndTime) {
               long top1 = Math.max(blockStartTime, rd.mEndTime);
               rd.mElapsed = blockEndTime - top1;
               rd.mEndTime = blockEndTime;
            }

            if((double)blockEndTime > maxVal) {
               maxVal = (double)blockEndTime;
            }

            TimeLineView.Block var27 = rd.top();
            if(var27 == null) {
               rd.push(block);
            } else {
               long topStartTime = var27.getStartTime();
               long topEndTime = var27.getEndTime();
               if(topEndTime >= blockStartTime) {
                  if(topStartTime < blockStartTime) {
                     TimeLineView.Segment segment = new TimeLineView.Segment(rd, var27, topStartTime, blockStartTime);
                     segmentList.add(segment);
                  }

                  if(topEndTime == blockStartTime) {
                     rd.pop();
                  }

                  rd.push(block);
               } else {
                  popFrames(rd, var27, blockStartTime, segmentList);
                  rd.push(block);
               }
            }
         }
      }

      rv = this.mRowByName.values().iterator();

      while(rv.hasNext()) {
         TimeLineView.RowData var25 = (TimeLineView.RowData)rv.next();
         TimeLineView.Block var26 = var25.top();
         popFrames(var25, var26, 2147483647L, segmentList);
      }

      this.mSurface.setRange(minVal, maxVal);
      this.mSurface.setLimitRange(minVal, maxVal);
      Collection var23 = this.mRowByName.values();
      this.mRows = (TimeLineView.RowData[])var23.toArray(new TimeLineView.RowData[var23.size()]);
      Arrays.sort(this.mRows, new Comparator() {
         @Override
         public int compare(Object o1, Object o2) {
            return compare((TimeLineView.RowData)o1, (TimeLineView.RowData)o2);
         }

         public int compare(TimeLineView.RowData rd1, TimeLineView.RowData rd2) {
            return (int)(rd2.mElapsed - rd1.mElapsed);
         }
      });

      int var24;
      for(var24 = 0; var24 < this.mRows.length; ++var24) {
         this.mRows[var24].mRank = var24;
      }

      this.mNumRows = 0;

      for(var24 = 0; var24 < this.mRows.length && this.mRows[var24].mElapsed != 0L; ++var24) {
         ++this.mNumRows;
      }

      this.mSegments = (TimeLineView.Segment[])segmentList.toArray(new TimeLineView.Segment[segmentList.size()]);
      Arrays.sort(this.mSegments, new Comparator() {
         @Override
         public int compare(Object o1, Object o2) {
            return compare((TimeLineView.Segment)o1,(TimeLineView.Segment)o2);
         }

         public int compare(TimeLineView.Segment bd1, TimeLineView.Segment bd2) {
            TimeLineView.RowData rd1 = bd1.mRowData;
            TimeLineView.RowData rd2 = bd2.mRowData;
            int diff = rd1.mRank - rd2.mRank;
            if(diff == 0) {
               long timeDiff = bd1.mStartTime - bd2.mStartTime;
               if(timeDiff == 0L) {
                  timeDiff = bd1.mEndTime - bd2.mEndTime;
               }

               return (int)timeDiff;
            } else {
               return diff;
            }
         }
      });
   }

   private static void popFrames(TimeLineView.RowData rd, TimeLineView.Block top, long startTime, ArrayList<TimeLineView.Segment> segmentList) {
      long topEndTime = top.getEndTime();

      long lastEndTime;
      TimeLineView.Segment bd;
      for(lastEndTime = top.getStartTime(); topEndTime <= startTime; topEndTime = top.getEndTime()) {
         if(topEndTime > lastEndTime) {
            bd = new TimeLineView.Segment(rd, top, lastEndTime, topEndTime);
            segmentList.add(bd);
            lastEndTime = topEndTime;
         }

         rd.pop();
         top = rd.top();
         if(top == null) {
            return;
         }
      }

      if(lastEndTime < startTime) {
         bd = new TimeLineView.Segment(rd, top, lastEndTime, startTime);
         segmentList.add(bd);
      }

   }

   private int computeVisibleRows(int ydim) {
      int offsetY = this.mScrollOffsetY;
      int spaceNeeded = this.mNumRows * 32;
      if(offsetY + ydim > spaceNeeded) {
         offsetY = spaceNeeded - ydim;
         if(offsetY < 0) {
            offsetY = 0;
         }
      }

      this.mStartRow = offsetY / 32;
      this.mEndRow = (offsetY + ydim) / 32;
      if(this.mEndRow >= this.mNumRows) {
         this.mEndRow = this.mNumRows - 1;
      }

      return offsetY;
   }

   private void startHighlighting() {
      this.mSurface.mHighlightStep = 0;
      this.mSurface.mFadeColors = true;
      this.mSurface.mCachedEndRow = -1;
      this.getDisplay().timerExec(0, this.mSurface.mHighlightAnimator);
   }

   private static class Range {
      Point mXdim = new Point(0, 0);
      int mY;
      Color mColor;

      Range(int xStart, int width, int y, Color color) {
         this.mXdim.x = xStart;
         this.mXdim.y = width;
         this.mY = y;
         this.mColor = color;
      }
   }

   private static class Pixel {
      int mStart;
      double mMaxWeight;
      TimeLineView.Segment mSegment;
      Color mColor;
      TimeLineView.RowData mRowData;

      private Pixel() {
         this.mStart = -2;
      }

      public void setFields(int start, double weight, TimeLineView.Segment segment, Color color, TimeLineView.RowData rowData) {
         this.mStart = start;
         this.mMaxWeight = weight;
         this.mSegment = segment;
         this.mColor = color;
         this.mRowData = rowData;
      }

      // $FF: synthetic method
      Pixel(Object x0) {
         this();
      }
   }

   private static class Strip {
      int mX;
      int mY;
      int mWidth;
      int mHeight;
      TimeLineView.RowData mRowData;
      TimeLineView.Segment mSegment;
      Color mColor;

      Strip(int x, int y, int width, int height, TimeLineView.RowData rowData, TimeLineView.Segment segment, Color color) {
         this.mX = x;
         this.mY = y;
         this.mWidth = width;
         this.mHeight = height;
         this.mRowData = rowData;
         this.mSegment = segment;
         this.mColor = color;
      }
   }

   private static class Segment {
      private TimeLineView.RowData mRowData;
      private TimeLineView.Block mBlock;
      private long mStartTime;
      private long mEndTime;
      private boolean mIsContextSwitch;

      Segment(TimeLineView.RowData rowData, TimeLineView.Block block, long startTime, long endTime) {
         this.mRowData = rowData;
         if(block.isContextSwitch()) {
            this.mBlock = block.getParentBlock();
            this.mIsContextSwitch = true;
         } else {
            this.mBlock = block;
         }

         this.mStartTime = startTime;
         this.mEndTime = endTime;
      }
   }

   private static class RowData {
      private String mName;
      private int mRank;
      private long mElapsed;
      private long mEndTime;
      private ArrayList<TimeLineView.Block> mStack;

      RowData(TimeLineView.Row row) {
         this.mName = row.getName();
         this.mStack = new ArrayList();
      }

      public void push(TimeLineView.Block block) {
         this.mStack.add(block);
      }

      public TimeLineView.Block top() {
         return this.mStack.size() == 0?null:(TimeLineView.Block)this.mStack.get(this.mStack.size() - 1);
      }

      public void pop() {
         if(this.mStack.size() != 0) {
            this.mStack.remove(this.mStack.size() - 1);
         }
      }
   }

   private class Surface extends Canvas {
      private static final int TotalXMargin = 70;
      private static final int yMargin = 1;
      private static final int MinZoomPixelMargin = 10;
      private TimeLineView.GraphicsState mGraphicsState;
      private Point mMouse;
      private int mMouseMarkStartX;
      private int mMouseMarkEndX;
      private boolean mDebug;
      private ArrayList<TimeLineView.Strip> mStripList;
      private ArrayList<TimeLineView.Range> mHighlightExclusive;
      private ArrayList<TimeLineView.Range> mHighlightInclusive;
      private int mMinStripHeight;
      private double mCachedMinVal;
      private double mCachedMaxVal;
      private int mCachedStartRow;
      private int mCachedEndRow;
      private double mScalePixelsPerRange;
      private double mScaleMinVal;
      private double mScaleMaxVal;
      private double mLimitMinVal;
      private double mLimitMaxVal;
      private double mMinDataVal;
      private double mMaxDataVal;
      private Cursor mNormalCursor;
      private Cursor mIncreasingCursor;
      private Cursor mDecreasingCursor;
      private static final int ZOOM_TIMER_INTERVAL = 10;
      private static final int HIGHLIGHT_TIMER_INTERVAL = 50;
      private static final int ZOOM_STEPS = 8;
      private int mHighlightHeight;
      private final int[] highlightHeights;
      private final int HIGHLIGHT_STEPS;
      private boolean mFadeColors;
      private boolean mShowHighlightName;
      private double[] mZoomFractions;
      private int mZoomStep;
      private int mZoomMouseStart;
      private int mZoomMouseEnd;
      private int mMouseStartDistance;
      private int mMouseEndDistance;
      private Point mMouseSelect;
      private double mZoomFixed;
      private double mZoomFixedPixel;
      private double mFixedPixelStartDistance;
      private double mFixedPixelEndDistance;
      private double mZoomMin2Fixed;
      private double mMin2ZoomMin;
      private double mFixed2ZoomMax;
      private double mZoomMax2Max;
      private double mZoomMin;
      private double mZoomMax;
      private Runnable mZoomAnimator;
      private Runnable mHighlightAnimator;
      private int mHighlightStep;

      public Surface(Composite parent) {
         super(parent, 262912);
         this.mGraphicsState = TimeLineView.GraphicsState.Normal;
         this.mMouse = new Point(10, 0);
         this.mDebug = false;
         this.mStripList = new ArrayList();
         this.mHighlightExclusive = new ArrayList();
         this.mHighlightInclusive = new ArrayList();
         this.mMinStripHeight = 2;
         this.mHighlightHeight = 4;
         this.highlightHeights = new int[]{0, 2, 4, 5, 6, 5, 4, 2, 4, 5, 6};
         this.HIGHLIGHT_STEPS = this.highlightHeights.length;
         this.mMouseSelect = new Point(0, 0);
         Display display = this.getDisplay();
         this.mNormalCursor = new Cursor(display, 2);
         this.mIncreasingCursor = new Cursor(display, 12);
         this.mDecreasingCursor = new Cursor(display, 13);
         this.initZoomFractionsWithExp();
         this.addPaintListener(new PaintListener() {
            public void paintControl(PaintEvent pe) {
               Surface.this.draw(pe.display, pe.gc);
            }
         });
         this.mZoomAnimator = new Runnable() {
            public void run() {
               Surface.this.animateZoom();
            }
         };
         this.mHighlightAnimator = new Runnable() {
            public void run() {
               Surface.this.animateHighlight();
            }
         };
      }

      private void initZoomFractionsWithExp() {
         this.mZoomFractions = new double[8];
         int next = 0;

         int ii;
         for(ii = 0; ii < 4; ++next) {
            this.mZoomFractions[next] = (double)(1 << ii) / 16.0D;
            ++ii;
         }

         for(ii = 2; ii < 6; ++next) {
            this.mZoomFractions[next] = (double)((1 << ii) - 1) / (double)(1 << ii);
            ++ii;
         }

      }

      private void initZoomFractionsWithSinWave() {
         this.mZoomFractions = new double[8];

         for(int ii = 0; ii < 8; ++ii) {
            double offset = 3.141592653589793D * (double)ii / 8.0D;
            this.mZoomFractions[ii] = (Math.sin(4.71238898038469D + offset) + 1.0D) / 2.0D;
         }

      }

      public void setRange(double minVal, double maxVal) {
         this.mMinDataVal = minVal;
         this.mMaxDataVal = maxVal;
         TimeLineView.this.mScaleInfo.setMinVal(minVal);
         TimeLineView.this.mScaleInfo.setMaxVal(maxVal);
      }

      public void setLimitRange(double minVal, double maxVal) {
         this.mLimitMinVal = minVal;
         this.mLimitMaxVal = maxVal;
      }

      public void resetScale() {
         TimeLineView.this.mScaleInfo.setMinVal(this.mLimitMinVal);
         TimeLineView.this.mScaleInfo.setMaxVal(this.mLimitMaxVal);
      }

      public void setScaleFromHorizontalScrollBar(int selection) {
         double minVal = TimeLineView.this.mScaleInfo.getMinVal();
         double maxVal = TimeLineView.this.mScaleInfo.getMaxVal();
         double visibleRange = maxVal - minVal;
         minVal = this.mLimitMinVal + (double)selection;
         maxVal = minVal + visibleRange;
         if(maxVal > this.mLimitMaxVal) {
            maxVal = this.mLimitMaxVal;
            minVal = maxVal - visibleRange;
         }

         TimeLineView.this.mScaleInfo.setMinVal(minVal);
         TimeLineView.this.mScaleInfo.setMaxVal(maxVal);
         this.mGraphicsState = TimeLineView.GraphicsState.Scrolling;
      }

      private void updateHorizontalScrollBar() {
         double minVal = TimeLineView.this.mScaleInfo.getMinVal();
         double maxVal = TimeLineView.this.mScaleInfo.getMaxVal();
         double visibleRange = maxVal - minVal;
         double fullRange = this.mLimitMaxVal - this.mLimitMinVal;
         ScrollBar hBar = this.getHorizontalBar();
         if(fullRange > visibleRange) {
            hBar.setVisible(true);
            hBar.setMinimum(0);
            hBar.setMaximum((int)Math.ceil(fullRange));
            hBar.setThumb((int)Math.ceil(visibleRange));
            hBar.setSelection((int)Math.floor(minVal - this.mLimitMinVal));
         } else {
            hBar.setVisible(false);
         }

      }

      private void draw(Display display, GC gc) {
         if(TimeLineView.this.mSegments.length != 0) {
            Image image = new Image(display, this.getBounds());
            GC gcImage = new GC(image);
            if(TimeLineView.this.mSetFonts) {
               gcImage.setFont(TimeLineView.this.mFontRegistry.get("small"));
            }

            if(this.mGraphicsState == TimeLineView.GraphicsState.Scaling) {
               double dim = (double)(this.mMouse.x - this.mMouseMarkStartX);
               double blockColor;
               if(dim > 0.0D) {
                  blockColor = this.mScaleMinVal - dim / this.mScalePixelsPerRange;
                  if(blockColor < this.mLimitMinVal) {
                     blockColor = this.mLimitMinVal;
                  }

                  TimeLineView.this.mScaleInfo.setMinVal(blockColor);
               } else if(dim < 0.0D) {
                  blockColor = this.mScaleMaxVal - dim / this.mScalePixelsPerRange;
                  if(blockColor > this.mLimitMaxVal) {
                     blockColor = this.mLimitMaxVal;
                  }

                  TimeLineView.this.mScaleInfo.setMaxVal(blockColor);
               }
            }

            Point dim1 = this.getSize();
            int blockName;
            if(TimeLineView.this.mStartRow != this.mCachedStartRow || TimeLineView.this.mEndRow != this.mCachedEndRow || TimeLineView.this.mScaleInfo.getMinVal() != this.mCachedMinVal || TimeLineView.this.mScaleInfo.getMaxVal() != this.mCachedMaxVal) {
               this.mCachedStartRow = TimeLineView.this.mStartRow;
               this.mCachedEndRow = TimeLineView.this.mEndRow;
               blockName = dim1.x - 70;
               TimeLineView.this.mScaleInfo.setNumPixels(blockName);
               boolean blockColor2 = this.mGraphicsState == TimeLineView.GraphicsState.Scaling || this.mGraphicsState == TimeLineView.GraphicsState.Animating || this.mGraphicsState == TimeLineView.GraphicsState.Scrolling;
               TimeLineView.this.mScaleInfo.computeTicks(blockColor2);
               this.mCachedMinVal = TimeLineView.this.mScaleInfo.getMinVal();
               this.mCachedMaxVal = TimeLineView.this.mScaleInfo.getMaxVal();
               if(this.mLimitMinVal > TimeLineView.this.mScaleInfo.getMinVal()) {
                  this.mLimitMinVal = TimeLineView.this.mScaleInfo.getMinVal();
               }

               if(this.mLimitMaxVal < TimeLineView.this.mScaleInfo.getMaxVal()) {
                  this.mLimitMaxVal = TimeLineView.this.mScaleInfo.getMaxVal();
               }

               this.computeStrips();
               this.updateHorizontalScrollBar();
            }

            if(TimeLineView.this.mNumRows > 2) {
               gcImage.setBackground(TimeLineView.this.mColorRowBack);

               for(blockName = 1; blockName < TimeLineView.this.mNumRows; blockName += 2) {
                  TimeLineView.RowData blockColor1 = TimeLineView.this.mRows[blockName];
                  int blockDetails = blockColor1.mRank * 32 - TimeLineView.this.mScrollOffsetY;
                  gcImage.fillRectangle(0, blockDetails, dim1.x, 32);
               }
            }

            if(this.drawingSelection()) {
               this.drawSelection(display, gcImage);
            }

            String blockName1 = null;
            Color blockColor3 = null;
            String blockDetails1 = null;
            if(this.mDebug) {
               double selectBlock = TimeLineView.this.mScaleInfo.getPixelsPerRange();
               System.out.printf("dim.x %d pixels %d minVal %f, maxVal %f ppr %f rpp %f\n", new Object[]{Integer.valueOf(dim1.x), Integer.valueOf(dim1.x - 70), Double.valueOf(TimeLineView.this.mScaleInfo.getMinVal()), Double.valueOf(TimeLineView.this.mScaleInfo.getMaxVal()), Double.valueOf(selectBlock), Double.valueOf(1.0D / selectBlock)});
            }

            TimeLineView.Block selectBlock1 = null;
            Iterator lineEnd = this.mStripList.iterator();

            while(lineEnd.hasNext()) {
               TimeLineView.Strip md = (TimeLineView.Strip)lineEnd.next();
               if(md.mColor != null) {
                  gcImage.setBackground(md.mColor);
                  gcImage.fillRectangle(md.mX, md.mY - TimeLineView.this.mScrollOffsetY, md.mWidth, md.mHeight);
                  if(TimeLineView.this.mMouseRow == md.mRowData.mRank) {
                     if(this.mMouse.x >= md.mX && this.mMouse.x < md.mX + md.mWidth) {
                        TimeLineView.Block mouseX = md.mSegment.mBlock;
                        blockName1 = mouseX.getName();
                        blockColor3 = md.mColor;
                        if(TimeLineView.this.mHaveCpuTime) {
                           if(TimeLineView.this.mHaveRealTime) {
                              blockDetails1 = String.format("excl cpu %s, incl cpu %s, excl real %s, incl real %s", new Object[]{TimeLineView.this.mUnits.labelledString(mouseX.getExclusiveCpuTime()), TimeLineView.this.mUnits.labelledString(mouseX.getInclusiveCpuTime()), TimeLineView.this.mUnits.labelledString(mouseX.getExclusiveRealTime()), TimeLineView.this.mUnits.labelledString(mouseX.getInclusiveRealTime())});
                           } else {
                              blockDetails1 = String.format("excl cpu %s, incl cpu %s", new Object[]{TimeLineView.this.mUnits.labelledString(mouseX.getExclusiveCpuTime()), TimeLineView.this.mUnits.labelledString(mouseX.getInclusiveCpuTime())});
                           }
                        } else {
                           blockDetails1 = String.format("excl real %s, incl real %s", new Object[]{TimeLineView.this.mUnits.labelledString(mouseX.getExclusiveRealTime()), TimeLineView.this.mUnits.labelledString(mouseX.getInclusiveRealTime())});
                        }
                     }

                     if(this.mMouseSelect.x >= md.mX && this.mMouseSelect.x < md.mX + md.mWidth) {
                        selectBlock1 = md.mSegment.mBlock;
                     }
                  }
               }
            }

            this.mMouseSelect.x = 0;
            this.mMouseSelect.y = 0;
            if(selectBlock1 != null) {
               ArrayList lineEnd1 = new ArrayList();
               TimeLineView.RowData md1 = TimeLineView.this.mRows[TimeLineView.this.mMouseRow];
               lineEnd1.add(Selection.highlight("Thread", md1.mName));
               lineEnd1.add(Selection.highlight("Call", selectBlock1));
               int mouseX1 = this.mMouse.x - 10;
               double mouseXval = TimeLineView.this.mScaleInfo.pixelToValue(mouseX1);
               lineEnd1.add(Selection.highlight("Time", Double.valueOf(mouseXval)));
               TimeLineView.this.mSelectionController.change(lineEnd1, "TimeLineView");
               TimeLineView.this.mHighlightMethodData = null;
               TimeLineView.this.mHighlightCall = (Call)selectBlock1;
               TimeLineView.this.startHighlighting();
            }

            int lineEnd2;
            if(TimeLineView.this.mMouseRow >= 0 && TimeLineView.this.mMouseRow < TimeLineView.this.mNumRows && this.mHighlightStep == 0) {
               gcImage.setForeground(TimeLineView.this.mColorGray);
               lineEnd2 = TimeLineView.this.mMouseRow * 32 - TimeLineView.this.mScrollOffsetY;
               gcImage.drawLine(0, lineEnd2, dim1.x, lineEnd2);
               gcImage.drawLine(0, lineEnd2 + 32, dim1.x, lineEnd2 + 32);
            }

            this.drawHighlights(gcImage, dim1);
            gcImage.setForeground(TimeLineView.this.mColorDarkGray);
            lineEnd2 = Math.min(dim1.y, TimeLineView.this.mNumRows * 32);
            gcImage.drawLine(this.mMouse.x, 0, this.mMouse.x, lineEnd2);
            if(blockName1 != null) {
               TimeLineView.this.mTimescale.setMethodName(blockName1);
               TimeLineView.this.mTimescale.setMethodColor(blockColor3);
               TimeLineView.this.mTimescale.setDetails(blockDetails1);
               this.mShowHighlightName = false;
            } else if(this.mShowHighlightName) {
               MethodData md2 = TimeLineView.this.mHighlightMethodData;
               if(md2 == null && TimeLineView.this.mHighlightCall != null) {
                  md2 = TimeLineView.this.mHighlightCall.getMethodData();
               }

               if(md2 == null) {
                  System.out.printf("null highlight?\n", new Object[0]);
               }

               if(md2 != null) {
                  TimeLineView.this.mTimescale.setMethodName(md2.getProfileName());
                  TimeLineView.this.mTimescale.setMethodColor(md2.getColor());
                  TimeLineView.this.mTimescale.setDetails((String)null);
               }
            } else {
               TimeLineView.this.mTimescale.setMethodName((String)null);
               TimeLineView.this.mTimescale.setMethodColor((Color)null);
               TimeLineView.this.mTimescale.setDetails((String)null);
            }

            TimeLineView.this.mTimescale.redraw();
            gc.drawImage(image, 0, 0);
            image.dispose();
            gcImage.dispose();
         }
      }

      private void drawHighlights(GC gc, Point dim) {
         int height = this.mHighlightHeight;
         if(height > 0) {
            Iterator i$ = this.mHighlightExclusive.iterator();

            TimeLineView.Range range;
            int x1;
            int x2;
            while(i$.hasNext()) {
               range = (TimeLineView.Range)i$.next();
               gc.setBackground(range.mColor);
               x1 = range.mXdim.x;
               x2 = range.mXdim.y;
               gc.fillRectangle(x1, range.mY - height - TimeLineView.this.mScrollOffsetY, x2, height);
            }

            --height;
            if(height <= 0) {
               height = 1;
            }

            gc.setForeground(TimeLineView.this.mColorDarkGray);
            gc.setBackground(TimeLineView.this.mColorDarkGray);
            i$ = this.mHighlightInclusive.iterator();

            while(i$.hasNext()) {
               range = (TimeLineView.Range)i$.next();
               x1 = range.mXdim.x;
               x2 = range.mXdim.y;
               boolean drawLeftEnd = false;
               boolean drawRightEnd = false;
               if(x1 >= 10) {
                  drawLeftEnd = true;
               } else {
                  x1 = 10;
               }

               if(x2 >= 10) {
                  drawRightEnd = true;
               } else {
                  x2 = dim.x - 60;
               }

               int y1 = range.mY + 20 + 2 - TimeLineView.this.mScrollOffsetY;
               if(x2 - x1 < 3) {
                  int points = x2 - x1;
                  if(points < 2) {
                     points = 2;
                  }

                  gc.fillRectangle(x1, y1, points, height);
               } else {
                  int[] var12;
                  if(drawLeftEnd) {
                     if(drawRightEnd) {
                        var12 = new int[]{x1, y1, x1, y1 + height, x2, y1 + height, x2, y1};
                        gc.drawPolyline(var12);
                     } else {
                        var12 = new int[]{x1, y1, x1, y1 + height, x2, y1 + height};
                        gc.drawPolyline(var12);
                     }
                  } else if(drawRightEnd) {
                     var12 = new int[]{x1, y1 + height, x2, y1 + height, x2, y1};
                     gc.drawPolyline(var12);
                  } else {
                     var12 = new int[]{x1, y1 + height, x2, y1 + height};
                     gc.drawPolyline(var12);
                  }

                  if(!drawLeftEnd) {
                     var12 = new int[]{x1 + 7, y1 + height - 4, x1, y1 + height, x1 + 7, y1 + height + 4};
                     gc.fillPolygon(var12);
                  }

                  if(!drawRightEnd) {
                     var12 = new int[]{x2 - 7, y1 + height - 4, x2, y1 + height, x2 - 7, y1 + height + 4};
                     gc.fillPolygon(var12);
                  }
               }
            }

         }
      }

      private boolean drawingSelection() {
         return this.mGraphicsState == TimeLineView.GraphicsState.Marking || this.mGraphicsState == TimeLineView.GraphicsState.Animating;
      }

      private void drawSelection(Display display, GC gc) {
         Point dim = this.getSize();
         gc.setForeground(TimeLineView.this.mColorGray);
         gc.drawLine(this.mMouseMarkStartX, 0, this.mMouseMarkStartX, dim.y);
         gc.setBackground(TimeLineView.this.mColorZoomSelection);
         int mouseX = this.mGraphicsState == TimeLineView.GraphicsState.Animating?this.mMouseMarkEndX:this.mMouse.x;
         int width;
         int x;
         if(this.mMouseMarkStartX < mouseX) {
            x = this.mMouseMarkStartX;
            width = mouseX - this.mMouseMarkStartX;
         } else {
            x = mouseX;
            width = this.mMouseMarkStartX - mouseX;
         }

         gc.fillRectangle(x, 0, width, dim.y);
      }

      private void computeStrips() {
         double minVal = TimeLineView.this.mScaleInfo.getMinVal();
         double maxVal = TimeLineView.this.mScaleInfo.getMaxVal();
         TimeLineView.Pixel[] pixels = new TimeLineView.Pixel[TimeLineView.this.mNumRows];

         int callMethod;
         for(callMethod = 0; callMethod < TimeLineView.this.mNumRows; ++callMethod) {
            pixels[callMethod] = new TimeLineView.Pixel(null);
         }

         for(callMethod = 0; callMethod < TimeLineView.this.mSegments.length; ++callMethod) {
            TimeLineView.this.mSegments[callMethod].mBlock.clearWeight();
         }

         this.mStripList.clear();
         this.mHighlightExclusive.clear();
         this.mHighlightInclusive.clear();
         MethodData var38 = null;
         long callStart = 0L;
         long callEnd = -1L;
         TimeLineView.RowData callRowData = null;
         int prevMethodStart = -1;
         int prevMethodEnd = -1;
         int prevCallStart = -1;
         int prevCallEnd = -1;
         int pix;
         int ii;
         int rd;
         Color color;
         if(TimeLineView.this.mHighlightCall != null) {
            ii = -1;
            pix = -1;
            callStart = TimeLineView.this.mHighlightCall.getStartTime();
            callEnd = TimeLineView.this.mHighlightCall.getEndTime();
            var38 = TimeLineView.this.mHighlightCall.getMethodData();
            if((double)callStart >= minVal) {
               ii = TimeLineView.this.mScaleInfo.valueToPixel((double)callStart);
            }

            if((double)callEnd <= maxVal) {
               pix = TimeLineView.this.mScaleInfo.valueToPixel((double)callEnd);
            }

            rd = TimeLineView.this.mHighlightCall.getThreadId();
            String y1 = (String)TimeLineView.this.mThreadLabels.get(Integer.valueOf(rd));
            callRowData = (TimeLineView.RowData)TimeLineView.this.mRowByName.get(y1);
            int block = callRowData.mRank * 32 + 6;
            color = var38.getColor();
            this.mHighlightInclusive.add(new TimeLineView.Range(ii + 10, pix + 10, block, color));
         }

         TimeLineView.Segment[] var39 = TimeLineView.this.mSegments;
         pix = var39.length;

         for(rd = 0; rd < pix; ++rd) {
            TimeLineView.Segment var40 = var39[rd];
            if((double)var40.mEndTime > minVal && (double)var40.mStartTime < maxVal) {
               TimeLineView.Block var43 = var40.mBlock;
               color = var43.getColor();
               if(color != null) {
                  double recordStart = Math.max((double)var40.mStartTime, minVal);
                  double recordEnd = Math.min((double)var40.mEndTime, maxVal);
                  if(recordStart != recordEnd) {
                     int pixelStart = TimeLineView.this.mScaleInfo.valueToPixel(recordStart);
                     int pixelEnd = TimeLineView.this.mScaleInfo.valueToPixel(recordEnd);
                     int width = pixelEnd - pixelStart;
                     boolean isContextSwitch = var40.mIsContextSwitch;
                     TimeLineView.RowData rd1 = var40.mRowData;
                     MethodData md = var43.getMethodData();
                     int y11 = rd1.mRank * 32 + 6;
                     if(rd1.mRank > TimeLineView.this.mEndRow) {
                        break;
                     }

                     int weight;
                     int pix1;
                     if(TimeLineView.this.mHighlightMethodData != null) {
                        if(TimeLineView.this.mHighlightMethodData == md) {
                           if(prevMethodStart != pixelStart || prevMethodEnd != pixelEnd) {
                              prevMethodStart = pixelStart;
                              prevMethodEnd = pixelEnd;
                              pix1 = width;
                              if(width == 0) {
                                 pix1 = 1;
                              }

                              this.mHighlightExclusive.add(new TimeLineView.Range(pixelStart + 10, pix1, y11, color));
                              callStart = var43.getStartTime();
                              weight = -1;
                              if((double)callStart >= minVal) {
                                 weight = TimeLineView.this.mScaleInfo.valueToPixel((double)callStart);
                              }

                              int strip = -1;
                              callEnd = var43.getEndTime();
                              if((double)callEnd <= maxVal) {
                                 strip = TimeLineView.this.mScaleInfo.valueToPixel((double)callEnd);
                              }

                              if(prevCallStart != weight || prevCallEnd != strip) {
                                 prevCallStart = weight;
                                 prevCallEnd = strip;
                                 this.mHighlightInclusive.add(new TimeLineView.Range(weight + 10, strip + 10, y11, color));
                              }
                           }
                        } else if(this.mFadeColors) {
                           color = md.getFadedColor();
                        }
                     } else if(TimeLineView.this.mHighlightCall != null) {
                        if(var40.mStartTime >= callStart && var40.mEndTime <= callEnd && var38 == md && callRowData == rd1) {
                           if(prevMethodStart != pixelStart || prevMethodEnd != pixelEnd) {
                              prevMethodStart = pixelStart;
                              prevMethodEnd = pixelEnd;
                              pix1 = width;
                              if(width == 0) {
                                 pix1 = 1;
                              }

                              this.mHighlightExclusive.add(new TimeLineView.Range(pixelStart + 10, pix1, y11, color));
                           }
                        } else if(this.mFadeColors) {
                           color = md.getFadedColor();
                        }
                     }

                     TimeLineView.Pixel var46 = pixels[rd1.mRank];
                     double var47;
                     if(var46.mStart != pixelStart) {
                        if(var46.mSegment != null) {
                           this.emitPixelStrip(rd1, y11, var46);
                        }

                        if(width == 0) {
                           var47 = this.computeWeight(recordStart, recordEnd, isContextSwitch, pixelStart);
                           var47 = var43.addWeight(pixelStart, rd1.mRank, var47);
                           if(var47 > var46.mMaxWeight) {
                              var46.setFields(pixelStart, var47, var40, color, rd1);
                           }
                        } else {
                           weight = pixelStart + 10;
                           TimeLineView.Strip var45 = new TimeLineView.Strip(weight, isContextSwitch?y11 + 20 - 1:y11, width, isContextSwitch?1:20, rd1, var40, color);
                           this.mStripList.add(var45);
                        }
                     } else {
                        var47 = this.computeWeight(recordStart, recordEnd, isContextSwitch, pixelStart);
                        var47 = var43.addWeight(pixelStart, rd1.mRank, var47);
                        if(var47 > var46.mMaxWeight) {
                           var46.setFields(pixelStart, var47, var40, color, rd1);
                        }

                        if(width == 1) {
                           this.emitPixelStrip(rd1, y11, var46);
                           ++pixelStart;
                           var47 = this.computeWeight(recordStart, recordEnd, isContextSwitch, pixelStart);
                           var47 = var43.addWeight(pixelStart, rd1.mRank, var47);
                           var46.setFields(pixelStart, var47, var40, color, rd1);
                        } else if(width > 1) {
                           this.emitPixelStrip(rd1, y11, var46);
                           ++pixelStart;
                           --width;
                           int x1 = pixelStart + 10;
                           TimeLineView.Strip strip1 = new TimeLineView.Strip(x1, isContextSwitch?y11 + 20 - 1:y11, width, isContextSwitch?1:20, rd1, var40, color);
                           this.mStripList.add(strip1);
                        }
                     }
                  }
               }
            }
         }

         for(ii = 0; ii < TimeLineView.this.mNumRows; ++ii) {
            TimeLineView.Pixel var42 = pixels[ii];
            if(var42.mSegment != null) {
               TimeLineView.RowData var41 = var42.mRowData;
               int var44 = var41.mRank * 32 + 6;
               this.emitPixelStrip(var41, var44, var42);
            }
         }

      }

      private double computeWeight(double start, double end, boolean isContextSwitch, int pixel) {
         if(isContextSwitch) {
            return 0.0D;
         } else {
            double pixelStartFraction = TimeLineView.this.mScaleInfo.valueToPixelFraction(start);
            double pixelEndFraction = TimeLineView.this.mScaleInfo.valueToPixelFraction(end);
            double leftEndPoint = Math.max(pixelStartFraction, (double)pixel - 0.5D);
            double rightEndPoint = Math.min(pixelEndFraction, (double)pixel + 0.5D);
            double weight = rightEndPoint - leftEndPoint;
            return weight;
         }
      }

      private void emitPixelStrip(TimeLineView.RowData rd, int y, TimeLineView.Pixel pixel) {
         if(pixel.mSegment != null) {
            int x = pixel.mStart + 10;
            int height = (int)(pixel.mMaxWeight * 20.0D * 0.75D);
            if(height < this.mMinStripHeight) {
               height = this.mMinStripHeight;
            }

            int remainder = 20 - height;
            TimeLineView.Strip strip;
            if(remainder > 0) {
               strip = new TimeLineView.Strip(x, y, 1, remainder, rd, pixel.mSegment, this.mFadeColors?TimeLineView.this.mColorGray:TimeLineView.this.mColorBlack);
               this.mStripList.add(strip);
            }

            strip = new TimeLineView.Strip(x, y + remainder, 1, height, rd, pixel.mSegment, pixel.mColor);
            this.mStripList.add(strip);
            pixel.mSegment = null;
            pixel.mMaxWeight = 0.0D;
         }
      }

      private void mouseMove(MouseEvent me) {
         Point dim = TimeLineView.this.mSurface.getSize();
         int x = me.x;
         if(x < 10) {
            x = 10;
         }

         if(x > dim.x - 60) {
            x = dim.x - 60;
         }

         this.mMouse.x = x;
         this.mMouse.y = me.y;
         TimeLineView.this.mTimescale.setVbarPosition(x);
         if(this.mGraphicsState == TimeLineView.GraphicsState.Marking) {
            TimeLineView.this.mTimescale.setMarkEnd(x);
         }

         if(this.mGraphicsState == TimeLineView.GraphicsState.Normal) {
            TimeLineView.this.mSurface.setCursor(this.mNormalCursor);
         } else if(this.mGraphicsState == TimeLineView.GraphicsState.Marking) {
            if(this.mMouse.x >= this.mMouseMarkStartX) {
               TimeLineView.this.mSurface.setCursor(this.mIncreasingCursor);
            } else {
               TimeLineView.this.mSurface.setCursor(this.mDecreasingCursor);
            }
         }

         int rownum = (this.mMouse.y + TimeLineView.this.mScrollOffsetY) / 32;
         if(me.y < 0 || me.y >= dim.y) {
            rownum = -1;
         }

         if(TimeLineView.this.mMouseRow != rownum) {
            TimeLineView.this.mMouseRow = rownum;
            TimeLineView.this.mLabels.redraw();
         }

         this.redraw();
      }

      private void mouseDown(MouseEvent me) {
         Point dim = TimeLineView.this.mSurface.getSize();
         int x = me.x;
         if(x < 10) {
            x = 10;
         }

         if(x > dim.x - 60) {
            x = dim.x - 60;
         }

         this.mMouseMarkStartX = x;
         this.mGraphicsState = TimeLineView.GraphicsState.Marking;
         TimeLineView.this.mSurface.setCursor(this.mIncreasingCursor);
         TimeLineView.this.mTimescale.setMarkStart(this.mMouseMarkStartX);
         TimeLineView.this.mTimescale.setMarkEnd(this.mMouseMarkStartX);
         this.redraw();
      }

      private void mouseUp(MouseEvent me) {
         TimeLineView.this.mSurface.setCursor(this.mNormalCursor);
         if(this.mGraphicsState != TimeLineView.GraphicsState.Marking) {
            this.mGraphicsState = TimeLineView.GraphicsState.Normal;
         } else {
            this.mGraphicsState = TimeLineView.GraphicsState.Animating;
            Point dim = TimeLineView.this.mSurface.getSize();
            if(me.y > 0 && me.y < dim.y) {
               int x = me.x;
               if(x < 10) {
                  x = 10;
               }

               if(x > dim.x - 60) {
                  x = dim.x - 60;
               }

               this.mMouseMarkEndX = x;
               int dist = this.mMouseMarkEndX - this.mMouseMarkStartX;
               if(dist < 0) {
                  dist = -dist;
               }

               if(dist <= 2) {
                  this.mGraphicsState = TimeLineView.GraphicsState.Normal;
                  this.mMouseSelect.x = this.mMouseMarkStartX;
                  this.mMouseSelect.y = me.y;
                  this.redraw();
               } else {
                  if(this.mMouseMarkEndX < this.mMouseMarkStartX) {
                     int minVal = this.mMouseMarkEndX;
                     this.mMouseMarkEndX = this.mMouseMarkStartX;
                     this.mMouseMarkStartX = minVal;
                  }

                  if(this.mMouseMarkStartX <= 20 && this.mMouseMarkEndX >= dim.x - 60 - 10) {
                     this.mGraphicsState = TimeLineView.GraphicsState.Normal;
                     this.redraw();
                  } else {
                     double minVal1 = TimeLineView.this.mScaleInfo.getMinVal();
                     double maxVal = TimeLineView.this.mScaleInfo.getMaxVal();
                     double ppr = TimeLineView.this.mScaleInfo.getPixelsPerRange();
                     this.mZoomMin = minVal1 + (double)(this.mMouseMarkStartX - 10) / ppr;
                     this.mZoomMax = minVal1 + (double)(this.mMouseMarkEndX - 10) / ppr;
                     if(this.mZoomMin < this.mMinDataVal) {
                        this.mZoomMin = this.mMinDataVal;
                     }

                     if(this.mZoomMax > this.mMaxDataVal) {
                        this.mZoomMax = this.mMaxDataVal;
                     }

                     int xdim = dim.x - 70;
                     TickScaler scaler = new TickScaler(this.mZoomMin, this.mZoomMax, xdim, 50);
                     scaler.computeTicks(false);
                     this.mZoomMin = scaler.getMinVal();
                     this.mZoomMax = scaler.getMaxVal();
                     this.mMouseMarkStartX = (int)((this.mZoomMin - minVal1) * ppr + 10.0D);
                     this.mMouseMarkEndX = (int)((this.mZoomMax - minVal1) * ppr + 10.0D);
                     TimeLineView.this.mTimescale.setMarkStart(this.mMouseMarkStartX);
                     TimeLineView.this.mTimescale.setMarkEnd(this.mMouseMarkEndX);
                     this.mMouseEndDistance = dim.x - 60 - this.mMouseMarkEndX;
                     this.mMouseStartDistance = this.mMouseMarkStartX - 10;
                     this.mZoomMouseStart = this.mMouseMarkStartX;
                     this.mZoomMouseEnd = this.mMouseMarkEndX;
                     this.mZoomStep = 0;
                     this.mMin2ZoomMin = this.mZoomMin - minVal1;
                     this.mZoomMax2Max = maxVal - this.mZoomMax;
                     this.mZoomFixed = this.mZoomMin + (this.mZoomMax - this.mZoomMin) * this.mMin2ZoomMin / (this.mMin2ZoomMin + this.mZoomMax2Max);
                     this.mZoomFixedPixel = (this.mZoomFixed - minVal1) * ppr + 10.0D;
                     this.mFixedPixelStartDistance = this.mZoomFixedPixel - 10.0D;
                     this.mFixedPixelEndDistance = (double)(dim.x - 60) - this.mZoomFixedPixel;
                     this.mZoomMin2Fixed = this.mZoomFixed - this.mZoomMin;
                     this.mFixed2ZoomMax = this.mZoomMax - this.mZoomFixed;
                     this.getDisplay().timerExec(10, this.mZoomAnimator);
                     this.redraw();
                     this.update();
                  }
               }
            } else {
               this.mGraphicsState = TimeLineView.GraphicsState.Normal;
               this.redraw();
            }
         }
      }

      private void mouseScrolled(MouseEvent me) {
         this.mGraphicsState = TimeLineView.GraphicsState.Scrolling;
         double tMin = TimeLineView.this.mScaleInfo.getMinVal();
         double tMax = TimeLineView.this.mScaleInfo.getMaxVal();
         double zoomFactor = 2.0D;
         double tMinRef = this.mLimitMinVal;
         double tMaxRef = this.mLimitMaxVal;
         double tMaxNew;
         double t;
         double tMinNew;
         if(me.count > 0) {
            Point factor = TimeLineView.this.mSurface.getSize();
            int x = me.x;
            if(x < 10) {
               x = 10;
            }

            if(x > factor.x - 60) {
               x = factor.x - 60;
            }

            double ppr = TimeLineView.this.mScaleInfo.getPixelsPerRange();
            t = tMin + (double)(x - 10) / ppr;
            tMinNew = Math.max(tMinRef, t - (t - tMin) / zoomFactor);
            tMaxNew = Math.min(tMaxRef, t + (tMax - t) / zoomFactor);
         } else {
            double factor1 = (tMax - tMin) / (tMaxRef - tMinRef);
            if(factor1 >= 1.0D) {
               return;
            }

            t = (factor1 * tMinRef - tMin) / (factor1 - 1.0D);
            tMinNew = Math.max(tMinRef, t - zoomFactor * (t - tMin));
            tMaxNew = Math.min(tMaxRef, t + zoomFactor * (tMax - t));
         }

         TimeLineView.this.mScaleInfo.setMinVal(tMinNew);
         TimeLineView.this.mScaleInfo.setMaxVal(tMaxNew);
         TimeLineView.this.mSurface.redraw();
      }

      private void mouseDoubleClick(MouseEvent me) {
      }

      public void startScaling(int mouseX) {
         Point dim = TimeLineView.this.mSurface.getSize();
         int x = mouseX;
         if(mouseX < 10) {
            x = 10;
         }

         if(x > dim.x - 60) {
            x = dim.x - 60;
         }

         this.mMouseMarkStartX = x;
         this.mGraphicsState = TimeLineView.GraphicsState.Scaling;
         this.mScalePixelsPerRange = TimeLineView.this.mScaleInfo.getPixelsPerRange();
         this.mScaleMinVal = TimeLineView.this.mScaleInfo.getMinVal();
         this.mScaleMaxVal = TimeLineView.this.mScaleInfo.getMaxVal();
      }

      public void stopScaling(int mouseX) {
         this.mGraphicsState = TimeLineView.GraphicsState.Normal;
      }

      private void animateHighlight() {
         ++this.mHighlightStep;
         if(this.mHighlightStep >= this.HIGHLIGHT_STEPS) {
            this.mFadeColors = false;
            this.mHighlightStep = 0;
            this.mCachedEndRow = -1;
         } else {
            this.mFadeColors = true;
            this.mShowHighlightName = true;
            this.mHighlightHeight = this.highlightHeights[this.mHighlightStep];
            this.getDisplay().timerExec(50, this.mHighlightAnimator);
         }

         this.redraw();
      }

      private void clearHighlights() {
         this.mShowHighlightName = false;
         this.mHighlightHeight = 0;
         TimeLineView.this.mHighlightMethodData = null;
         TimeLineView.this.mHighlightCall = null;
         this.mFadeColors = false;
         this.mHighlightStep = 0;
         this.mCachedEndRow = -1;
         this.redraw();
      }

      private void animateZoom() {
         ++this.mZoomStep;
         if(this.mZoomStep > 8) {
            this.mGraphicsState = TimeLineView.GraphicsState.Normal;
            this.mCachedMinVal = TimeLineView.this.mScaleInfo.getMinVal() + 1.0D;
         } else if(this.mZoomStep == 8) {
            TimeLineView.this.mScaleInfo.setMinVal(this.mZoomMin);
            TimeLineView.this.mScaleInfo.setMaxVal(this.mZoomMax);
            this.mMouseMarkStartX = 10;
            Point fraction = this.getSize();
            this.mMouseMarkEndX = fraction.x - 60;
            TimeLineView.this.mTimescale.setMarkStart(this.mMouseMarkStartX);
            TimeLineView.this.mTimescale.setMarkEnd(this.mMouseMarkEndX);
            this.getDisplay().timerExec(10, this.mZoomAnimator);
         } else {
            double var9 = this.mZoomFractions[this.mZoomStep];
            this.mMouseMarkStartX = (int)((double)this.mZoomMouseStart - var9 * (double)this.mMouseStartDistance);
            this.mMouseMarkEndX = (int)((double)this.mZoomMouseEnd + var9 * (double)this.mMouseEndDistance);
            TimeLineView.this.mTimescale.setMarkStart(this.mMouseMarkStartX);
            TimeLineView.this.mTimescale.setMarkEnd(this.mMouseMarkEndX);
            double ppr;
            if(this.mZoomMin2Fixed >= this.mFixed2ZoomMax) {
               ppr = (this.mZoomFixedPixel - (double)this.mMouseMarkStartX) / this.mZoomMin2Fixed;
            } else {
               ppr = ((double)this.mMouseMarkEndX - this.mZoomFixedPixel) / this.mFixed2ZoomMax;
            }

            double newMin = this.mZoomFixed - this.mFixedPixelStartDistance / ppr;
            double newMax = this.mZoomFixed + this.mFixedPixelEndDistance / ppr;
            TimeLineView.this.mScaleInfo.setMinVal(newMin);
            TimeLineView.this.mScaleInfo.setMaxVal(newMax);
            this.getDisplay().timerExec(10, this.mZoomAnimator);
         }

         this.redraw();
      }
   }

   private static enum GraphicsState {
      Normal,
      Marking,
      Scaling,
      Animating,
      Scrolling;
   }

   private class Timescale extends Canvas {
      private Point mMouse = new Point(10, 0);
      private Cursor mZoomCursor;
      private String mMethodName = null;
      private Color mMethodColor = null;
      private String mDetails;
      private int mMethodStartY;
      private int mDetailsStartY;
      private int mMarkStartX;
      private int mMarkEndX;
      private static final int METHOD_BLOCK_MARGIN = 10;

      public Timescale(Composite parent) {
         super(parent, 0);
         Display display = this.getDisplay();
         this.mZoomCursor = new Cursor(display, 9);
         this.setCursor(this.mZoomCursor);
         this.mMethodStartY = TimeLineView.this.mSmallFontHeight + 1;
         this.mDetailsStartY = this.mMethodStartY + TimeLineView.this.mSmallFontHeight + 1;
         this.addPaintListener(new PaintListener() {
            public void paintControl(PaintEvent pe) {
               Timescale.this.draw(pe.display, pe.gc);
            }
         });
      }

      public void setVbarPosition(int x) {
         this.mMouse.x = x;
      }

      public void setMarkStart(int x) {
         this.mMarkStartX = x;
      }

      public void setMarkEnd(int x) {
         this.mMarkEndX = x;
      }

      public void setMethodName(String name) {
         this.mMethodName = name;
      }

      public void setMethodColor(Color color) {
         this.mMethodColor = color;
      }

      public void setDetails(String details) {
         this.mDetails = details;
      }

      private void mouseMove(MouseEvent me) {
         me.y = -1;
         TimeLineView.this.mSurface.mouseMove(me);
      }

      private void mouseDown(MouseEvent me) {
         TimeLineView.this.mSurface.startScaling(me.x);
         TimeLineView.this.mSurface.redraw();
      }

      private void mouseUp(MouseEvent me) {
         TimeLineView.this.mSurface.stopScaling(me.x);
      }

      private void mouseDoubleClick(MouseEvent me) {
         TimeLineView.this.mSurface.resetScale();
         TimeLineView.this.mSurface.redraw();
      }

      private void draw(Display display, GC gc) {
         Point dim = this.getSize();
         Image image = new Image(display, this.getBounds());
         GC gcImage = new GC(image);
         if(TimeLineView.this.mSetFonts) {
            gcImage.setFont(TimeLineView.this.mFontRegistry.get("medium"));
         }

         if(TimeLineView.this.mSurface.drawingSelection()) {
            this.drawSelection(display, gcImage);
         }

         this.drawTicks(display, gcImage);
         gcImage.setForeground(TimeLineView.this.mColorDarkGray);
         gcImage.drawLine(this.mMouse.x, 58, this.mMouse.x, dim.y);
         this.drawTickLegend(display, gcImage);
         this.drawMethod(display, gcImage);
         this.drawDetails(display, gcImage);
         gc.drawImage(image, 0, 0);
         image.dispose();
         gcImage.dispose();
      }

      private void drawSelection(Display display, GC gc) {
         Point dim = this.getSize();
         gc.setForeground(TimeLineView.this.mColorGray);
         gc.drawLine(this.mMarkStartX, 58, this.mMarkStartX, dim.y);
         gc.setBackground(TimeLineView.this.mColorZoomSelection);
         int x;
         int width;
         if(this.mMarkStartX < this.mMarkEndX) {
            x = this.mMarkStartX;
            width = this.mMarkEndX - this.mMarkStartX;
         } else {
            x = this.mMarkEndX;
            width = this.mMarkStartX - this.mMarkEndX;
         }

         if(width > 1) {
            gc.fillRectangle(x, 58, width, dim.y);
         }

      }

      private void drawTickLegend(Display display, GC gc) {
         int mouseX = this.mMouse.x - 10;
         double mouseXval = TimeLineView.this.mScaleInfo.pixelToValue(mouseX);
         String info = TimeLineView.this.mUnits.labelledString(mouseXval);
         gc.setForeground(TimeLineView.this.mColorForeground);
         gc.drawString(info, 12, 1, true);
         double maxVal = TimeLineView.this.mScaleInfo.getMaxVal();
         info = TimeLineView.this.mUnits.labelledString(maxVal);
         if(TimeLineView.this.mClockSource != null) {
            info = String.format(" max %s (%s)", new Object[]{info, TimeLineView.this.mClockSource});
         } else {
            info = String.format(" max %s ", new Object[]{info});
         }

         Point extent = gc.stringExtent(info);
         Point dim = this.getSize();
         int x1 = dim.x - 60 - extent.x;
         gc.drawString(info, x1, 1, true);
      }

      private void drawMethod(Display display, GC gc) {
         if(this.mMethodName != null) {
            byte x1 = 10;
            int y1 = this.mMethodStartY;
            gc.setBackground(this.mMethodColor);
            int width = 2 * TimeLineView.this.mSmallFontWidth;
            gc.fillRectangle(x1, y1, width, TimeLineView.this.mSmallFontHeight);
            int x11 = x1 + width + 10;
            gc.drawString(this.mMethodName, x11, y1, true);
         }
      }

      private void drawDetails(Display display, GC gc) {
         if(this.mDetails != null) {
            int x1 = 10 + 2 * TimeLineView.this.mSmallFontWidth + 10;
            int y1 = this.mDetailsStartY;
            gc.drawString(this.mDetails, x1, y1, true);
         }
      }

      private void drawTicks(Display display, GC gc) {
         Point dim = this.getSize();
         byte y2 = 66;
         byte y3 = 62;
         int y4 = y2 + 2;
         gc.setForeground(TimeLineView.this.mColorForeground);
         gc.drawLine(10, 58, dim.x - 60, 58);
         double minVal = TimeLineView.this.mScaleInfo.getMinVal();
         double maxVal = TimeLineView.this.mScaleInfo.getMaxVal();
         double minMajorTick = TimeLineView.this.mScaleInfo.getMinMajorTick();
         double tickIncrement = TimeLineView.this.mScaleInfo.getTickIncrement();
         double minorTickIncrement = tickIncrement / 5.0D;
         double pixelsPerRange = TimeLineView.this.mScaleInfo.getPixelsPerRange();
         double x;
         int x1;
         if(minVal < minMajorTick) {
            gc.setForeground(TimeLineView.this.mColorGray);
            x = minMajorTick;

            for(x1 = 1; x1 <= 4; ++x1) {
               x -= minorTickIncrement;
               if(x < minVal) {
                  break;
               }

               int tickString = 10 + (int)(0.5D + (x - minVal) * pixelsPerRange);
               gc.drawLine(tickString, 58, tickString, y3);
            }
         }

         if(tickIncrement > 10.0D) {
            for(x = minMajorTick; x <= maxVal; x += tickIncrement) {
               x1 = 10 + (int)(0.5D + (x - minVal) * pixelsPerRange);
               gc.setForeground(TimeLineView.this.mColorForeground);
               gc.drawLine(x1, 58, x1, y2);
               if(x > maxVal) {
                  break;
               }

               String var26 = TimeLineView.this.mUnits.valueOf(x);
               gc.drawString(var26, x1, y4, true);
               gc.setForeground(TimeLineView.this.mColorGray);
               double xMinor = x;

               for(int ii = 1; ii <= 4; ++ii) {
                  xMinor += minorTickIncrement;
                  if(xMinor > maxVal) {
                     break;
                  }

                  x1 = 10 + (int)(0.5D + (xMinor - minVal) * pixelsPerRange);
                  gc.drawLine(x1, 58, x1, y3);
               }
            }

         }
      }
   }

   private class BlankCorner extends Canvas {
      public BlankCorner(Composite parent) {
         super(parent, 0);
         this.addPaintListener(new PaintListener() {
            public void paintControl(PaintEvent pe) {
               BlankCorner.this.draw(pe.display, pe.gc);
            }
         });
      }

      private void draw(Display display, GC gc) {
         Image image = new Image(display, this.getBounds());
         gc.drawImage(image, 0, 0);
         image.dispose();
      }
   }

   private class RowLabels extends Canvas {
      private static final int labelMarginX = 2;

      public RowLabels(Composite parent) {
         super(parent, 262144);
         this.addPaintListener(new PaintListener() {
            public void paintControl(PaintEvent pe) {
               RowLabels.this.draw(pe.display, pe.gc);
            }
         });
      }

      private void mouseMove(MouseEvent me) {
         int rownum = (me.y + TimeLineView.this.mScrollOffsetY) / 32;
         if(TimeLineView.this.mMouseRow != rownum) {
            TimeLineView.this.mMouseRow = rownum;
            this.redraw();
            TimeLineView.this.mSurface.redraw();
         }

      }

      private void draw(Display display, GC gc) {
         if(TimeLineView.this.mSegments.length != 0) {
            Point dim = this.getSize();
            Image image = new Image(display, this.getBounds());
            GC gcImage = new GC(image);
            if(TimeLineView.this.mSetFonts) {
               gcImage.setFont(TimeLineView.this.mFontRegistry.get("medium"));
            }

            int offsetY;
            if(TimeLineView.this.mNumRows > 2) {
               gcImage.setBackground(TimeLineView.this.mColorRowBack);

               for(offsetY = 1; offsetY < TimeLineView.this.mNumRows; offsetY += 2) {
                  TimeLineView.RowData y1 = TimeLineView.this.mRows[offsetY];
                  int rd = y1.mRank * 32 - TimeLineView.this.mScrollOffsetY;
                  gcImage.fillRectangle(0, rd, dim.x, 32);
               }
            }

            offsetY = 6 - TimeLineView.this.mScrollOffsetY;

            int var12;
            for(var12 = TimeLineView.this.mStartRow; var12 <= TimeLineView.this.mEndRow; ++var12) {
               TimeLineView.RowData var13 = TimeLineView.this.mRows[var12];
               int y11 = var13.mRank * 32 + offsetY;
               Point extent = gcImage.stringExtent(var13.mName);
               int x1 = dim.x - extent.x - 2;
               gcImage.drawString(var13.mName, x1, y11, true);
            }

            if(TimeLineView.this.mMouseRow >= TimeLineView.this.mStartRow && TimeLineView.this.mMouseRow <= TimeLineView.this.mEndRow) {
               gcImage.setForeground(TimeLineView.this.mColorGray);
               var12 = TimeLineView.this.mMouseRow * 32 - TimeLineView.this.mScrollOffsetY;
               gcImage.drawRectangle(0, var12, dim.x, 32);
            }

            gc.drawImage(image, 0, 0);
            image.dispose();
            gcImage.dispose();
         }
      }
   }

   public static class Record {
      TimeLineView.Row row;
      TimeLineView.Block block;

      public Record(TimeLineView.Row row, TimeLineView.Block block) {
         this.row = row;
         this.block = block;
      }
   }

   public interface Row {
      int getId();

      String getName();
   }

   public interface Block {
      String getName();

      MethodData getMethodData();

      long getStartTime();

      long getEndTime();

      Color getColor();

      double addWeight(int var1, int var2, double var3);

      void clearWeight();

      long getExclusiveCpuTime();

      long getInclusiveCpuTime();

      long getExclusiveRealTime();

      long getInclusiveRealTime();

      boolean isContextSwitch();

      boolean isIgnoredBlock();

      TimeLineView.Block getParentBlock();
   }
}
