package com.android.traceview;

public class Selection {
   private Selection.Action mAction;
   private String mName;
   private Object mValue;

   public Selection(Selection.Action action, String name, Object value) {
      this.mAction = action;
      this.mName = name;
      this.mValue = value;
   }

   public static Selection highlight(String name, Object value) {
      return new Selection(Selection.Action.Highlight, name, value);
   }

   public static Selection include(String name, Object value) {
      return new Selection(Selection.Action.Include, name, value);
   }

   public static Selection exclude(String name, Object value) {
      return new Selection(Selection.Action.Exclude, name, value);
   }

   public void setName(String name) {
      this.mName = name;
   }

   public String getName() {
      return this.mName;
   }

   public void setValue(Object value) {
      this.mValue = value;
   }

   public Object getValue() {
      return this.mValue;
   }

   public void setAction(Selection.Action action) {
      this.mAction = action;
   }

   public Selection.Action getAction() {
      return this.mAction;
   }

   public static enum Action {
      Highlight,
      Include,
      Exclude,
      Aggregate;
   }
}
