package com.android.traceview;

import java.text.DecimalFormat;

public class TraceUnits {
   private TraceUnits.TimeScale mTimeScale;
   private double mScale;
   DecimalFormat mFormatter;

   public TraceUnits() {
      this.mTimeScale = TraceUnits.TimeScale.MicroSeconds;
      this.mScale = 1.0D;
      this.mFormatter = new DecimalFormat();
   }

   public double getScaledValue(long value) {
      return (double)value * this.mScale;
   }

   public double getScaledValue(double value) {
      return value * this.mScale;
   }

   public String valueOf(long value) {
      return this.valueOf((double)value);
   }

   public String valueOf(double value) {
      double scaled = value * this.mScale;
      String pattern;
      if((double)((int)scaled) == scaled) {
         pattern = "###,###";
      } else {
         pattern = "###,###.###";
      }

      this.mFormatter.applyPattern(pattern);
      return this.mFormatter.format(scaled);
   }

   public String labelledString(double value) {
      String units = this.label();
      String num = this.valueOf(value);
      return String.format("%s: %s", new Object[]{units, num});
   }

   public String labelledString(long value) {
      return this.labelledString((double)value);
   }

   public String label() {
      return this.mScale == 1.0D?"微秒":(this.mScale == 0.001D?"毫秒":(this.mScale == 1.0E-6D?"秒":null));
   }

   public void setTimeScale(TraceUnits.TimeScale val) {
      this.mTimeScale = val;
      switch(TraceUnits.SyntheticClass_1.$SwitchMap$com$android$traceview$TraceUnits$TimeScale[val.ordinal()]) {
      case 1:
         this.mScale = 1.0E-6D;
         break;
      case 2:
         this.mScale = 0.001D;
         break;
      case 3:
         this.mScale = 1.0D;
      }

   }

   public TraceUnits.TimeScale getTimeScale() {
      return this.mTimeScale;
   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] $SwitchMap$com$android$traceview$TraceUnits$TimeScale = new int[TraceUnits.TimeScale.values().length];

      static {
         try {
            $SwitchMap$com$android$traceview$TraceUnits$TimeScale[TraceUnits.TimeScale.Seconds.ordinal()] = 1;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            $SwitchMap$com$android$traceview$TraceUnits$TimeScale[TraceUnits.TimeScale.MilliSeconds.ordinal()] = 2;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            $SwitchMap$com$android$traceview$TraceUnits$TimeScale[TraceUnits.TimeScale.MicroSeconds.ordinal()] = 3;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }

   public static enum TimeScale {
      Seconds,
      MilliSeconds,
      MicroSeconds;
   }
}
