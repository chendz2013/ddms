package com.android.monkeyrunner;

import com.android.chimpchat.core.ChimpRect;
import com.android.monkeyrunner.JythonUtils;
import com.android.monkeyrunner.doc.MonkeyRunnerExported;
import java.util.LinkedList;
import java.util.logging.Logger;
import org.python.core.ClassDictInit;
import org.python.core.PyInteger;
import org.python.core.PyList;
import org.python.core.PyObject;

@MonkeyRunnerExported(
   doc = "Represents the coordinates of a rectangular object"
)
public class MonkeyRect extends PyObject implements ClassDictInit {
   private static final Logger LOG = Logger.getLogger(MonkeyRect.class.getName());
   private ChimpRect rect;
   @MonkeyRunnerExported(
      doc = "The x coordinate of the left side of the rectangle"
   )
   public int left;
   @MonkeyRunnerExported(
      doc = "The y coordinate of the top side of the rectangle"
   )
   public int top;
   @MonkeyRunnerExported(
      doc = "The x coordinate of the right side of the rectangle"
   )
   public int right;
   @MonkeyRunnerExported(
      doc = "The y coordinate of the bottom side of the rectangle"
   )
   public int bottom;

   public static void classDictInit(PyObject dict) {
      JythonUtils.convertDocAnnotationsForClass(MonkeyRect.class, dict);
   }

   public MonkeyRect(ChimpRect rect) {
      this.rect = rect;
      this.left = rect.left;
      this.right = rect.right;
      this.top = rect.top;
      this.bottom = rect.bottom;
   }

   @MonkeyRunnerExported(
      doc = "Returns the width of the rectangle",
      returns = "The width of the rectangle as an integer"
   )
   public PyInteger getWidth() {
      return new PyInteger(this.right - this.left);
   }

   @MonkeyRunnerExported(
      doc = "Returns the height of the rectangle",
      returns = "The height of the rectangle as an integer"
   )
   public PyInteger getHeight() {
      return new PyInteger(this.bottom - this.top);
   }

   @MonkeyRunnerExported(
      doc = "Returns a two item list that contains the x and y value of the center of the rectangle",
      returns = "The center coordinates as a two item list of integers"
   )
   public PyList getCenter() {
      LinkedList center = new LinkedList();
      center.add(new PyInteger(this.left + (this.right - this.left) / 2));
      center.add(new PyInteger(this.top + (this.bottom - this.top) / 2));
      return new PyList(center);
   }
}
