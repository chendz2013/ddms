package com.android.monkeyrunner;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;
import java.io.File;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MonkeyRunnerOptions {
   private static final Logger LOG = Logger.getLogger(MonkeyRunnerOptions.class.getName());
   private static String DEFAULT_MONKEY_SERVER_ADDRESS = "127.0.0.1";
   private static int DEFAULT_MONKEY_PORT = 12345;
   private final int port;
   private final String hostname;
   private final File scriptFile;
   private final String backend;
   private final Collection<File> plugins;
   private final Collection<String> arguments;
   private final Level logLevel;

   private MonkeyRunnerOptions(String hostname, int port, File scriptFile, String backend, Level logLevel, Collection<File> plugins, Collection<String> arguments) {
      this.hostname = hostname;
      this.port = port;
      this.scriptFile = scriptFile;
      this.backend = backend;
      this.logLevel = logLevel;
      this.plugins = plugins;
      this.arguments = arguments;
   }

   public int getPort() {
      return this.port;
   }

   public String getHostname() {
      return this.hostname;
   }

   public File getScriptFile() {
      return this.scriptFile;
   }

   public String getBackendName() {
      return this.backend;
   }

   public Collection<File> getPlugins() {
      return this.plugins;
   }

   public Collection<String> getArguments() {
      return this.arguments;
   }

   public Level getLogLevel() {
      return this.logLevel;
   }

   private static void printUsage(String message) {
      System.out.println(message);
      System.out.println("Usage: monkeyrunner [options] SCRIPT_FILE");
      System.out.println("");
      System.out.println("    -s      MonkeyServer IP Address.");
      System.out.println("    -p      MonkeyServer TCP Port.");
      System.out.println("    -v      MonkeyServer Logging level (ALL, FINEST, FINER, FINE, CONFIG, INFO, WARNING, SEVERE, OFF)");
      System.out.println("");
      System.out.println("");
   }

   public static MonkeyRunnerOptions processOptions(String[] args) {
      int index = 0;
      String hostname = DEFAULT_MONKEY_SERVER_ADDRESS;
      File scriptFile = null;
      int port = DEFAULT_MONKEY_PORT;
      String backend = "adb";
      Level logLevel = Level.SEVERE;
      Builder pluginListBuilder = ImmutableList.builder();
      Builder argumentBuilder = ImmutableList.builder();

      while(index < args.length) {
         String argument = args[index++];
         if("-s".equals(argument)) {
            if(index == args.length) {
               printUsage("Missing Server after -s");
               return null;
            }

            hostname = args[index++];
         } else if("-p".equals(argument)) {
            if(index == args.length) {
               printUsage("Missing Server port after -p");
               return null;
            }

            port = Integer.parseInt(args[index++]);
         } else if("-v".equals(argument)) {
            if(index == args.length) {
               printUsage("Missing Log Level after -v");
               return null;
            }

            logLevel = Level.parse(args[index++]);
         } else if("-be".equals(argument)) {
            if(index == args.length) {
               printUsage("Missing backend name after -be");
               return null;
            }

            backend = args[index++];
         } else if("-plugin".equals(argument)) {
            if(index == args.length) {
               printUsage("Missing plugin path after -plugin");
               return null;
            }

            File plugin = new File(args[index++]);
            if(!plugin.exists()) {
               printUsage("Plugin file doesn\'t exist");
               return null;
            }

            if(!plugin.canRead()) {
               printUsage("Can\'t read plugin file");
               return null;
            }

            pluginListBuilder.add(plugin);
         } else if(!"-u".equals(argument)) {
            if(argument.startsWith("-") && scriptFile == null) {
               printUsage("Unrecognized argument: " + argument + ".");
               return null;
            }

            if(scriptFile == null) {
               scriptFile = new File(argument);
               if(!scriptFile.exists()) {
                  printUsage("Can\'t open specified script file");
                  return null;
               }

               if(!scriptFile.canRead()) {
                  printUsage("Can\'t open specified script file");
                  return null;
               }
            } else {
               argumentBuilder.add(argument);
            }
         }
      }

      return new MonkeyRunnerOptions(hostname, port, scriptFile, backend, logLevel, pluginListBuilder.build(), argumentBuilder.build());
   }
}
