package com.android.monkeyrunner.doc;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.CONSTRUCTOR, ElementType.TYPE, ElementType.FIELD})
public @interface MonkeyRunnerExported {
   String doc();

   String[] args() default {};

   String[] argDocs() default {};

   String returns() default "returns nothing.";
}
