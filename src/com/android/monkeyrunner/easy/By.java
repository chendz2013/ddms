package com.android.monkeyrunner.easy;

import com.android.chimpchat.hierarchyviewer.HierarchyViewer;
import com.android.hierarchyviewerlib.models.ViewNode;
import com.android.monkeyrunner.JythonUtils;
import com.android.monkeyrunner.doc.MonkeyRunnerExported;
import com.google.common.base.Preconditions;
import org.python.core.ArgParser;
import org.python.core.ClassDictInit;
import org.python.core.PyObject;

public class By extends PyObject implements ClassDictInit {
   private String id;

   public static void classDictInit(PyObject dict) {
      JythonUtils.convertDocAnnotationsForClass(By.class, dict);
   }

   By(String id) {
      this.id = id;
   }

   @MonkeyRunnerExported(
      doc = "Select an object by id.",
      args = {"id"},
      argDocs = {"The identifier of the object."}
   )
   public static By id(PyObject[] args, String[] kws) {
      ArgParser ap = JythonUtils.createArgParser(args, kws);
      Preconditions.checkNotNull(ap);
      String id = ap.getString(0);
      return new By(id);
   }

   public static By id(String id) {
      return new By(id);
   }

   public ViewNode findView(HierarchyViewer viewer) {
      return viewer.findViewById(this.id);
   }
}
