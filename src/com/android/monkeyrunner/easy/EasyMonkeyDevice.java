package com.android.monkeyrunner.easy;

import com.android.chimpchat.core.TouchPressType;
import com.android.chimpchat.hierarchyviewer.HierarchyViewer;
import com.android.hierarchyviewerlib.models.ViewNode;
import com.android.monkeyrunner.JythonUtils;
import com.android.monkeyrunner.MonkeyDevice;
import com.android.monkeyrunner.doc.MonkeyRunnerExported;
import com.android.monkeyrunner.easy.By;
import com.google.common.base.Preconditions;
import java.util.Set;
import org.eclipse.swt.graphics.Point;
import org.python.core.ArgParser;
import org.python.core.ClassDictInit;
import org.python.core.Py;
import org.python.core.PyException;
import org.python.core.PyInteger;
import org.python.core.PyObject;
import org.python.core.PyTuple;

@MonkeyRunnerExported(
   doc = "MonkeyDevice with easier methods to refer to objects."
)
public class EasyMonkeyDevice extends PyObject implements ClassDictInit {
   private MonkeyDevice mDevice;
   private HierarchyViewer mHierarchyViewer;
   private static final Set<String> EXPORTED_METHODS = JythonUtils.getMethodNames(EasyMonkeyDevice.class);

   public static void classDictInit(PyObject dict) {
      JythonUtils.convertDocAnnotationsForClass(EasyMonkeyDevice.class, dict);
   }

   @MonkeyRunnerExported(
      doc = "Creates EasyMonkeyDevice with an underlying MonkeyDevice.",
      args = {"device"},
      argDocs = {"MonkeyDevice to extend."}
   )
   public EasyMonkeyDevice(MonkeyDevice device) {
      this.mDevice = device;
      this.mHierarchyViewer = device.getImpl().getHierarchyViewer();
   }

   @MonkeyRunnerExported(
      doc = "Sends a touch event to the selected object.",
      args = {"selector", "type"},
      argDocs = {"The selector identifying the object.", "The event type as returned by TouchPressType()."}
   )
   public void touch(PyObject[] args, String[] kws) {
      ArgParser ap = JythonUtils.createArgParser(args, kws);
      Preconditions.checkNotNull(ap);
      By selector = this.getSelector(ap, 0);
      String tmpType = ap.getString(1);
      TouchPressType type = TouchPressType.fromIdentifier(tmpType);
      Preconditions.checkNotNull(type, "Invalid touch type: " + tmpType);
      this.touch(selector, type);
   }

   public void touch(By selector, TouchPressType type) {
      Point p = this.getElementCenter(selector);
      this.mDevice.getImpl().touch(p.x, p.y, type);
   }

   @MonkeyRunnerExported(
      doc = "Types a string into the specified object.",
      args = {"selector", "text"},
      argDocs = {"The selector identifying the object.", "The text to type into the object."}
   )
   public void type(PyObject[] args, String[] kws) {
      ArgParser ap = JythonUtils.createArgParser(args, kws);
      Preconditions.checkNotNull(ap);
      By selector = this.getSelector(ap, 0);
      String text = ap.getString(1);
      this.type(selector, text);
   }

   public void type(By selector, String text) {
      Point p = this.getElementCenter(selector);
      this.mDevice.getImpl().touch(p.x, p.y, TouchPressType.DOWN_AND_UP);
      this.mDevice.getImpl().type(text);
   }

   @MonkeyRunnerExported(
      doc = "Locates the coordinates of the selected object.",
      args = {"selector"},
      argDocs = {"The selector identifying the object."},
      returns = "Tuple containing (x,y,w,h) location and size."
   )
   public PyTuple locate(PyObject[] args, String[] kws) {
      ArgParser ap = JythonUtils.createArgParser(args, kws);
      Preconditions.checkNotNull(ap);
      By selector = this.getSelector(ap, 0);
      ViewNode node = selector.findView(this.mHierarchyViewer);
      Point p = HierarchyViewer.getAbsolutePositionOfView(node);
      PyTuple tuple = new PyTuple(new PyObject[]{new PyInteger(p.x), new PyInteger(p.y), new PyInteger(node.width), new PyInteger(node.height)});
      return tuple;
   }

   @MonkeyRunnerExported(
      doc = "Checks if the specified object exists.",
      args = {"selector"},
      returns = "True if the object exists.",
      argDocs = {"The selector identifying the object."}
   )
   public boolean exists(PyObject[] args, String[] kws) {
      ArgParser ap = JythonUtils.createArgParser(args, kws);
      Preconditions.checkNotNull(ap);
      By selector = this.getSelector(ap, 0);
      return this.exists(selector);
   }

   public boolean exists(By selector) {
      ViewNode node = selector.findView(this.mHierarchyViewer);
      return node != null;
   }

   @MonkeyRunnerExported(
      doc = "Checks if the specified object is visible.",
      args = {"selector"},
      returns = "True if the object is visible.",
      argDocs = {"The selector identifying the object."}
   )
   public boolean visible(PyObject[] args, String[] kws) {
      ArgParser ap = JythonUtils.createArgParser(args, kws);
      Preconditions.checkNotNull(ap);
      By selector = this.getSelector(ap, 0);
      return this.visible(selector);
   }

   public boolean visible(By selector) {
      ViewNode node = selector.findView(this.mHierarchyViewer);
      return this.mHierarchyViewer.visible(node);
   }

   @MonkeyRunnerExported(
      doc = "Obtain the text in the selected input box.",
      args = {"selector"},
      argDocs = {"The selector identifying the object."},
      returns = "Text in the selected input box."
   )
   public String getText(PyObject[] args, String[] kws) {
      ArgParser ap = JythonUtils.createArgParser(args, kws);
      Preconditions.checkNotNull(ap);
      By selector = this.getSelector(ap, 0);
      return this.getText(selector);
   }

   public String getText(By selector) {
      ViewNode node = selector.findView(this.mHierarchyViewer);
      return this.mHierarchyViewer.getText(node);
   }

   @MonkeyRunnerExported(
      doc = "Gets the id of the focused window.",
      returns = "The symbolic id of the focused window or None."
   )
   public String getFocusedWindowId(PyObject[] args, String[] kws) {
      return this.getFocusedWindowId();
   }

   public String getFocusedWindowId() {
      return this.mHierarchyViewer.getFocusedWindowName();
   }

   public PyObject __findattr_ex__(String name) {
      return !EXPORTED_METHODS.contains(name)?this.mDevice.__findattr_ex__(name):super.__findattr_ex__(name);
   }

   private By getSelector(ArgParser ap, int i) {
      return (By)ap.getPyObject(i).__tojava__(By.class);
   }

   private Point getElementCenter(By selector) {
      ViewNode node = selector.findView(this.mHierarchyViewer);
      if(node == null) {
         throw new PyException(Py.ValueError, String.format("View not found: %s", new Object[]{selector}));
      } else {
         Point p = HierarchyViewer.getAbsoluteCenterOfView(node);
         return p;
      }
   }
}
