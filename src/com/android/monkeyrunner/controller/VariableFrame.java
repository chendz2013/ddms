package com.android.monkeyrunner.controller;

import com.android.chimpchat.core.IChimpDevice;
import com.google.common.collect.Sets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;

public class VariableFrame extends JFrame {
   private static final Logger LOG = Logger.getLogger(VariableFrame.class.getName());
   private static final ExecutorService EXECUTOR = Executors.newCachedThreadPool();
   private IChimpDevice device;

   private static <E> E getNthElement(Set<E> set, int offset) {
      int current = 0;

      for(Iterator i$ = set.iterator(); i$.hasNext(); ++current) {
         Object elem = i$.next();
         if(current == offset) {
            return (E)elem;
         }
      }

      return null;
   }

   public VariableFrame() {
      super("Variables");
      this.setDefaultCloseOperation(1);
      this.setLayout(new BoxLayout(this.getContentPane(), 1));
      final VariableFrame.VariableTableModel tableModel = new VariableFrame.VariableTableModel(null);
      JButton refreshButton = new JButton("Refresh");
      this.add(refreshButton);
      refreshButton.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent e) {
            tableModel.refresh();
         }
      });
      JTable table = new JTable(tableModel);
      this.add(table);
      tableModel.addTableModelListener(new TableModelListener() {
         public void tableChanged(TableModelEvent e) {
            VariableFrame.this.pack();
         }
      });
      this.addWindowListener(new WindowAdapter() {
         public void windowOpened(WindowEvent e) {
            tableModel.refresh();
         }
      });
      this.pack();
   }

   public void init(IChimpDevice device) {
      this.device = device;
   }

   private class VariableTableModel extends AbstractTableModel {
      private final TreeSet<VariableFrame.VariableHolder> set;

      private VariableTableModel() {
         this.set = Sets.newTreeSet();
      }

      public void refresh() {
         Collection variables = VariableFrame.this.device.getPropertyList();
         Iterator i$ = variables.iterator();

         while(i$.hasNext()) {
            final String variable = (String)i$.next();
            VariableFrame.EXECUTOR.execute(new Runnable() {
               public void run() {
                  String value = VariableFrame.this.device.getProperty(variable);
                  if(value == null) {
                     value = "";
                  }

                  synchronized(VariableTableModel.this.set) {
                     VariableTableModel.this.set.add(new VariableFrame.VariableHolder(variable, value));
                     SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                           VariableTableModel.this.fireTableDataChanged();
                        }
                     });
                  }
               }
            });
         }

      }

      public int getColumnCount() {
         return 2;
      }

      public int getRowCount() {
         TreeSet var1 = this.set;
         synchronized(this.set) {
            return this.set.size();
         }
      }

      public Object getValueAt(int rowIndex, int columnIndex) {
         TreeSet var4 = this.set;
         VariableFrame.VariableHolder nthElement;
         synchronized(this.set) {
            nthElement = (VariableFrame.VariableHolder)VariableFrame.getNthElement(this.set, rowIndex);
         }

         return columnIndex == 0?nthElement.getKey():nthElement.getValue();
      }

      // $FF: synthetic method
      VariableTableModel(Object x1) {
         this();
      }
   }

   private static class VariableHolder implements Comparable<VariableFrame.VariableHolder> {
      private final String key;
      private final String value;

      public VariableHolder(String key, String value) {
         this.key = key;
         this.value = value;
      }

      public String getKey() {
         return this.key;
      }

      public String getValue() {
         return this.value;
      }

      public int compareTo(VariableFrame.VariableHolder o) {
         return this.key.compareTo(o.key);
      }
   }
}
