package com.android.monkeyrunner.controller;

import com.android.chimpchat.ChimpChat;
import com.android.chimpchat.core.IChimpDevice;
import com.android.monkeyrunner.controller.MonkeyControllerFrame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.TreeMap;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class MonkeyController extends JFrame {
   private static final Logger LOG = Logger.getLogger(MonkeyController.class.getName());

   public static void main(String[] args) {
      SwingUtilities.invokeLater(new Runnable() {
         public void run() {
            TreeMap options = new TreeMap();
            options.put("backend", "adb");
            ChimpChat chimpchat = ChimpChat.getInstance(options);
            final IChimpDevice device = chimpchat.waitForConnection();
            MonkeyControllerFrame mf = new MonkeyControllerFrame(device);
            mf.setVisible(true);
            mf.addWindowListener(new WindowAdapter() {
               public void windowClosed(WindowEvent e) {
                  device.dispose();
               }
            });
         }
      });
   }
}
