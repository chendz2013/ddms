package com.android.monkeyrunner.controller;

import com.android.chimpchat.core.IChimpDevice;
import com.android.chimpchat.core.IChimpImage;
import com.android.chimpchat.core.PhysicalButton;
import com.android.chimpchat.core.TouchPressType;
import com.android.monkeyrunner.controller.VariableFrame;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

public class MonkeyControllerFrame extends JFrame {
   private static final Logger LOG = Logger.getLogger(MonkeyControllerFrame.class.getName());
   private final JButton refreshButton = new JButton("Refresh");
   private final JButton variablesButton = new JButton("Variable");
   private final JLabel imageLabel = new JLabel();
   private final VariableFrame variableFrame;
   private final IChimpDevice device;
   private BufferedImage currentImage;
   private final TouchPressType DOWN_AND_UP;
   private final Timer timer;

   private JButton createToolbarButton(PhysicalButton hardButton) {
      JButton button = new JButton(new MonkeyControllerFrame.PressAction(hardButton));
      button.setText(hardButton.getKeyName());
      return button;
   }

   public MonkeyControllerFrame(IChimpDevice chimpDevice) {
      super("MonkeyController");
      this.DOWN_AND_UP = TouchPressType.DOWN_AND_UP;
      this.timer = new Timer(1000, new ActionListener() {
         public void actionPerformed(ActionEvent e) {
            MonkeyControllerFrame.this.updateScreen();
         }
      });
      this.device = chimpDevice;
      this.setDefaultCloseOperation(3);
      this.setLayout(new BoxLayout(this.getContentPane(), 1));
      JToolBar toolbar = new JToolBar();
      toolbar.add(this.createToolbarButton(PhysicalButton.HOME));
      toolbar.add(this.createToolbarButton(PhysicalButton.BACK));
      toolbar.add(this.createToolbarButton(PhysicalButton.SEARCH));
      toolbar.add(this.createToolbarButton(PhysicalButton.MENU));
      this.add(toolbar);
      this.add(this.refreshButton);
      this.add(this.variablesButton);
      this.add(this.imageLabel);
      this.refreshButton.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent e) {
            MonkeyControllerFrame.this.updateScreen();
         }
      });
      this.variableFrame = new VariableFrame();
      this.variablesButton.addActionListener(new ActionListener() {
         public void actionPerformed(ActionEvent e) {
            MonkeyControllerFrame.this.variableFrame.setVisible(true);
         }
      });
      this.imageLabel.addMouseListener(new MouseAdapter() {
         public void mouseClicked(MouseEvent event) {
            MonkeyControllerFrame.this.device.touch(event.getX(), event.getY(), MonkeyControllerFrame.this.DOWN_AND_UP);
            MonkeyControllerFrame.this.updateScreen();
         }
      });
      KeyboardFocusManager focusManager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
      focusManager.addKeyEventDispatcher(new KeyEventDispatcher() {
         public boolean dispatchKeyEvent(KeyEvent event) {
            if(400 == event.getID()) {
               MonkeyControllerFrame.this.device.type(Character.toString(event.getKeyChar()));
            }

            return false;
         }
      });
      SwingUtilities.invokeLater(new Runnable() {
         public void run() {
            MonkeyControllerFrame.this.init();
            MonkeyControllerFrame.this.variableFrame.init(MonkeyControllerFrame.this.device);
         }
      });
      this.pack();
   }

   private void updateScreen() {
      IChimpImage snapshot = this.device.takeSnapshot();
      this.currentImage = snapshot.createBufferedImage();
      this.imageLabel.setIcon(new ImageIcon(this.currentImage));
      this.pack();
   }

   private void init() {
      this.updateScreen();
      this.timer.start();
   }

   private class PressAction extends AbstractAction {
      private final PhysicalButton button;

      public PressAction(PhysicalButton button) {
         this.button = button;
      }

      public void actionPerformed(ActionEvent event) {
         MonkeyControllerFrame.this.device.press(this.button.getKeyName(), MonkeyControllerFrame.this.DOWN_AND_UP);
         MonkeyControllerFrame.this.updateScreen();
      }
   }
}
