package com.android.monkeyrunner;

import com.android.monkeyrunner.doc.MonkeyRunnerExported;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.google.common.collect.ImmutableMap.Builder;
import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.python.core.ArgParser;
import org.python.core.Py;
import org.python.core.PyBoolean;
import org.python.core.PyDictionary;
import org.python.core.PyFloat;
import org.python.core.PyInteger;
import org.python.core.PyList;
import org.python.core.PyNone;
import org.python.core.PyObject;
import org.python.core.PyReflectedField;
import org.python.core.PyReflectedFunction;
import org.python.core.PyString;
import org.python.core.PyStringMap;
import org.python.core.PyTuple;

public final class JythonUtils {
   private static final Logger LOG = Logger.getLogger(JythonUtils.class.getCanonicalName());
   private static final Map<Class<? extends PyObject>, Class<?>> PYOBJECT_TO_JAVA_OBJECT_MAP;
   private static final Predicate<AccessibleObject> SHOULD_BE_DOCUMENTED;
   private static final Predicate<Field> IS_FIELD_STATIC;

   public static ArgParser createArgParser(PyObject[] args, String[] kws) {
      StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
      StackTraceElement element = stackTrace[2];
      String methodName = element.getMethodName();
      String className = element.getClassName();

      Class clz;
      try {
         clz = Class.forName(className);
      } catch (ClassNotFoundException var11) {
         LOG.log(Level.SEVERE, "Got exception: ", var11);
         return null;
      }

      Method m;
      try {
         m = clz.getMethod(methodName, new Class[]{PyObject[].class, String[].class});
      } catch (SecurityException var9) {
         LOG.log(Level.SEVERE, "Got exception: ", var9);
         return null;
      } catch (NoSuchMethodException var10) {
         LOG.log(Level.SEVERE, "Got exception: ", var10);
         return null;
      }

      MonkeyRunnerExported annotation = (MonkeyRunnerExported)m.getAnnotation(MonkeyRunnerExported.class);
      return new ArgParser(methodName, args, kws, annotation.args());
   }

   public static double getFloat(ArgParser ap, int position) {
      PyObject arg = ap.getPyObject(position);
      if(Py.isInstance(arg, PyFloat.TYPE)) {
         return ((PyFloat)arg).asDouble();
      } else if(Py.isInstance(arg, PyInteger.TYPE)) {
         return ((PyInteger)arg).asDouble();
      } else {
         throw Py.TypeError("Unable to parse argument: " + position);
      }
   }

   public static double getFloat(ArgParser ap, int position, double defaultValue) {
      PyObject arg = ap.getPyObject(position, new PyFloat(defaultValue));
      if(Py.isInstance(arg, PyFloat.TYPE)) {
         return ((PyFloat)arg).asDouble();
      } else if(Py.isInstance(arg, PyInteger.TYPE)) {
         return ((PyInteger)arg).asDouble();
      } else {
         throw Py.TypeError("Unable to parse argument: " + position);
      }
   }

   public static List<Object> getList(ArgParser ap, int position) {
      PyObject arg = ap.getPyObject(position, Py.None);
      if(Py.isInstance(arg, PyNone.TYPE)) {
         return Collections.emptyList();
      } else {
         ArrayList ret = Lists.newArrayList();
         PyList array = (PyList)arg;

         for(int x = 0; x < array.__len__(); ++x) {
            PyObject item = array.__getitem__(x);
            Class javaClass = (Class)PYOBJECT_TO_JAVA_OBJECT_MAP.get(item.getClass());
            if(javaClass != null) {
               ret.add(item.__tojava__(javaClass));
            }
         }

         return ret;
      }
   }

   public static Map<String, Object> getMap(ArgParser ap, int position) {
      PyObject arg = ap.getPyObject(position, Py.None);
      if(Py.isInstance(arg, PyNone.TYPE)) {
         return Collections.emptyMap();
      } else {
         HashMap ret = Maps.newHashMap();
         PyDictionary dict = (PyDictionary)arg;
         PyList items = dict.items();

         for(int x = 0; x < items.__len__(); ++x) {
            PyTuple item = (PyTuple)items.__getitem__(x);
            String key = (String)item.__getitem__(0).__str__().__tojava__(String.class);
            PyObject value = item.__getitem__(1);
            Class javaClass = (Class)PYOBJECT_TO_JAVA_OBJECT_MAP.get(value.getClass());
            if(javaClass != null) {
               ret.put(key, value.__tojava__(javaClass));
            }
         }

         return ret;
      }
   }

   private static PyObject convertObject(Object o) {
      if(o instanceof String) {
         return new PyString((String)o);
      } else if(o instanceof Double) {
         return new PyFloat(((Double)o).doubleValue());
      } else if(o instanceof Integer) {
         return new PyInteger(((Integer)o).intValue());
      } else if(o instanceof Float) {
         float f = ((Float)o).floatValue();
         return new PyFloat(f);
      } else {
         return (PyObject)(o instanceof Boolean?new PyBoolean(((Boolean)o).booleanValue()):Py.None);
      }
   }

   public static PyDictionary convertMapToDict(Map<String, Object> map) {
      HashMap resultMap = Maps.newHashMap();
      Iterator i$ = map.entrySet().iterator();

      while(i$.hasNext()) {
         Entry entry = (Entry)i$.next();
         resultMap.put(new PyString((String)entry.getKey()), convertObject(entry.getValue()));
      }

      return new PyDictionary(resultMap);
   }

   public static void convertDocAnnotationsForClass(Class<?> clz, PyObject dict) {
      Preconditions.checkNotNull(dict);
      Preconditions.checkArgument(dict instanceof PyStringMap);
      if(clz.isAnnotationPresent(MonkeyRunnerExported.class)) {
         MonkeyRunnerExported functions = (MonkeyRunnerExported)clz.getAnnotation(MonkeyRunnerExported.class);
         String i$ = buildClassDoc(functions, clz);
         dict.__setitem__("__doc__", new PyString(i$));
      }

      HashSet var11 = Sets.newHashSet();
      Iterator var13 = dict.asIterable().iterator();

      while(var13.hasNext()) {
         PyObject name = (PyObject)var13.next();
         var11.add(name.toString());
      }

      Collection var12 = Collections2.filter(var11, new Predicate() {
         @Override
         public boolean equals(Object obj) {
            return super.equals(obj);
         }

         @Override
         public boolean apply(Object o) {
            return apply((String)o);
         }

         public boolean apply(String value) {
            return !value.startsWith("__");
         }
      });
      Method[] var14 = clz.getMethods();
      int var16 = var14.length;

      int i$1;
      String fieldName;
      PyObject pyField;
      MonkeyRunnerExported doc;
      for(i$1 = 0; i$1 < var16; ++i$1) {
         Method f = var14[i$1];
         if(f.isAnnotationPresent(MonkeyRunnerExported.class)) {
            fieldName = f.getName();
            pyField = dict.__finditem__(fieldName);
            if(pyField != null && pyField instanceof PyReflectedFunction) {
               PyReflectedFunction realPyfield = (PyReflectedFunction)pyField;
               doc = (MonkeyRunnerExported)f.getAnnotation(MonkeyRunnerExported.class);
               realPyfield.__doc__ = new PyString(buildDoc(doc));
               var12.remove(fieldName);
            }
         }
      }

      Field[] var15 = clz.getFields();
      var16 = var15.length;

      for(i$1 = 0; i$1 < var16; ++i$1) {
         Field var18 = var15[i$1];
         if(var18.isAnnotationPresent(MonkeyRunnerExported.class)) {
            fieldName = var18.getName();
            pyField = dict.__finditem__(fieldName);
            if(pyField != null && pyField instanceof PyReflectedField) {
               PyReflectedField var19 = (PyReflectedField)pyField;
               doc = (MonkeyRunnerExported)var18.getAnnotation(MonkeyRunnerExported.class);
               var12.remove(fieldName);
            }
         }
      }

      var13 = var12.iterator();

      while(var13.hasNext()) {
         String var17 = (String)var13.next();
         dict.__delitem__(var17);
      }

   }

   private static String buildClassDoc(MonkeyRunnerExported doc, Class<?> clz) {
      Collection annotatedFields = Collections2.filter(Arrays.asList(clz.getFields()), SHOULD_BE_DOCUMENTED);
      Collection staticFields = Collections2.filter(annotatedFields, IS_FIELD_STATIC);
      Collection nonStaticFields = Collections2.filter(annotatedFields, Predicates.not(IS_FIELD_STATIC));
      StringBuilder sb = new StringBuilder();
      Iterator i$ = splitString(doc.doc(), 80).iterator();

      while(i$.hasNext()) {
         String f = (String)i$.next();
         sb.append(f).append("\n");
      }

      Field f1;
      if(staticFields.size() > 0) {
         sb.append("\nClass Fields: \n");
         i$ = staticFields.iterator();

         while(i$.hasNext()) {
            f1 = (Field)i$.next();
            sb.append(buildFieldDoc(f1));
         }
      }

      if(nonStaticFields.size() > 0) {
         sb.append("\n\nFields: \n");
         i$ = nonStaticFields.iterator();

         while(i$.hasNext()) {
            f1 = (Field)i$.next();
            sb.append(buildFieldDoc(f1));
         }
      }

      return sb.toString();
   }

   private static String buildFieldDoc(Field f) {
      MonkeyRunnerExported annotation = (MonkeyRunnerExported)f.getAnnotation(MonkeyRunnerExported.class);
      StringBuilder sb = new StringBuilder();
      int indentOffset = 5 + f.getName().length();
      String indent = makeIndent(indentOffset);
      sb.append("  ").append(f.getName()).append(" - ");
      boolean first = true;
      Iterator i$ = splitString(annotation.doc(), 80 - indentOffset).iterator();

      while(i$.hasNext()) {
         String line = (String)i$.next();
         if(first) {
            first = false;
            sb.append(line).append("\n");
         } else {
            sb.append(indent).append(line).append("\n");
         }
      }

      return sb.toString();
   }

   private static String buildDoc(MonkeyRunnerExported doc) {
      Collection docs = splitString(doc.doc(), 80);
      StringBuilder sb = new StringBuilder();
      Iterator args = docs.iterator();

      while(args.hasNext()) {
         String argDocs = (String)args.next();
         sb.append(argDocs).append("\n");
      }

      if(doc.args() != null && doc.args().length > 0) {
         String[] var12 = doc.args();
         String[] var13 = doc.argDocs();
         sb.append("\n  Args:\n");

         for(int x = 0; x < doc.args().length; ++x) {
            sb.append("    ").append(var12[x]);
            if(var13 != null && var13.length > x) {
               sb.append(" - ");
               int indentOffset = var12[x].length() + 3 + 4;
               Collection lines = splitString(var13[x], 80 - indentOffset);
               boolean first = true;
               String indent = makeIndent(indentOffset);
               Iterator i$ = lines.iterator();

               while(i$.hasNext()) {
                  String line = (String)i$.next();
                  if(first) {
                     first = false;
                     sb.append(line).append("\n");
                  } else {
                     sb.append(indent).append(line).append("\n");
                  }
               }
            }
         }
      }

      return sb.toString();
   }

   private static String makeIndent(int indentOffset) {
      if(indentOffset == 0) {
         return "";
      } else {
         StringBuffer sb;
         for(sb = new StringBuffer(); indentOffset > 0; --indentOffset) {
            sb.append(' ');
         }

         return sb.toString();
      }
   }

   private static Collection<String> splitString(String source, int offset) {
      BreakIterator boundary = BreakIterator.getLineInstance();
      boundary.setText(source);
      ArrayList lines = Lists.newArrayList();
      StringBuilder currentLine = new StringBuilder();
      int start = boundary.first();

      for(int end = boundary.next(); end != -1; end = boundary.next()) {
         String b = source.substring(start, end);
         if(currentLine.length() + b.length() < offset) {
            currentLine.append(b);
         } else {
            lines.add(currentLine.toString());
            currentLine = new StringBuilder(b);
         }

         start = end;
      }

      lines.add(currentLine.toString());
      return lines;
   }

   public static Set<String> getMethodNames(Class<?> clazz) {
      HashSet methodNames = new HashSet();
      Method[] arr$ = clazz.getMethods();
      int len$ = arr$.length;

      for(int i$ = 0; i$ < len$; ++i$) {
         Method m = arr$[i$];
         if(m.isAnnotationPresent(MonkeyRunnerExported.class)) {
            methodNames.add(m.getName());
         }
      }

      return methodNames;
   }

   static {
      Builder builder = ImmutableMap.builder();
      builder.put(PyString.class, String.class);
      builder.put(PyFloat.class, Double.class);
      builder.put(PyInteger.class, Integer.class);
      builder.put(PyBoolean.class, Boolean.class);
      PYOBJECT_TO_JAVA_OBJECT_MAP = builder.build();
      SHOULD_BE_DOCUMENTED = new Predicate() {

         @Override
         public boolean apply(Object o) {
            return apply((AccessibleObject)o);
         }

         public boolean apply(AccessibleObject ao) {
            return ao.isAnnotationPresent(MonkeyRunnerExported.class);
         }
      };
      IS_FIELD_STATIC = new Predicate() {
         @Override
         public boolean apply(Object o) {
            return apply((Field)o);
         }

         public boolean apply(Field f) {
            return (f.getModifiers() & 8) != 0;
         }
      };
   }
}
