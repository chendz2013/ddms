package com.android.monkeyrunner.recorder;

import com.android.monkeyrunner.recorder.actions.Action;
import com.google.common.collect.Lists;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import javax.swing.AbstractListModel;

public class ActionListModel extends AbstractListModel {
   private List<Action> actionList = Lists.newArrayList();

   public void add(Action a) {
      this.actionList.add(a);
      int newIndex = this.actionList.size() - 1;
      this.fireIntervalAdded(this, newIndex, newIndex);
   }

   public Object getElementAt(int arg0) {
      return ((Action)this.actionList.get(arg0)).getDisplayName();
   }

   public int getSize() {
      return this.actionList.size();
   }

   public void export(File selectedFile) throws FileNotFoundException {
      PrintWriter out = new PrintWriter(selectedFile);
      Iterator i$ = this.actionList.iterator();

      while(i$.hasNext()) {
         Action a = (Action)i$.next();
         out.println(a.serialize());
      }

      out.close();
   }
}
