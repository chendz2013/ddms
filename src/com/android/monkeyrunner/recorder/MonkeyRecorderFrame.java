package com.android.monkeyrunner.recorder;

import com.android.chimpchat.core.IChimpDevice;
import com.android.chimpchat.core.IChimpImage;
import com.android.monkeyrunner.MonkeyDevice;
import com.android.monkeyrunner.recorder.ActionListModel;
import com.android.monkeyrunner.recorder.actions.Action;
import com.android.monkeyrunner.recorder.actions.DragAction;
import com.android.monkeyrunner.recorder.actions.PressAction;
import com.android.monkeyrunner.recorder.actions.TouchAction;
import com.android.monkeyrunner.recorder.actions.TypeAction;
import com.android.monkeyrunner.recorder.actions.WaitAction;
import com.google.common.collect.BiMap;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

public class MonkeyRecorderFrame extends JFrame {
   private static final Logger LOG = Logger.getLogger(MonkeyRecorderFrame.class.getName());
   private final IChimpDevice device;
   private static final long serialVersionUID = 1L;
   private JPanel jContentPane = null;
   private JLabel display = null;
   private JScrollPane historyPanel = null;
   private JPanel actionPanel = null;
   private JButton waitButton = null;
   private JButton pressButton = null;
   private JButton typeButton = null;
   private JButton flingButton = null;
   private JButton exportActionButton = null;
   private JButton refreshButton = null;
   private BufferedImage currentImage;
   private BufferedImage scaledImage = new BufferedImage(320, 480, 2);
   private JList historyList;
   private ActionListModel actionListModel;
   private final Timer refreshTimer = new Timer(1000, new ActionListener() {
      public void actionPerformed(ActionEvent e) {
         MonkeyRecorderFrame.this.refreshDisplay();
      }
   });

   public MonkeyRecorderFrame(IChimpDevice device) {
      this.device = device;
      this.initialize();
   }

   private void initialize() {
      this.setSize(400, 600);
      this.setContentPane(this.getJContentPane());
      this.setTitle("MonkeyRecorder");
      SwingUtilities.invokeLater(new Runnable() {
         public void run() {
            MonkeyRecorderFrame.this.refreshDisplay();
         }
      });
      this.refreshTimer.start();
   }

   private void refreshDisplay() {
      IChimpImage snapshot = this.device.takeSnapshot();
      this.currentImage = snapshot.createBufferedImage();
      Graphics2D g = this.scaledImage.createGraphics();
      g.drawImage(this.currentImage, 0, 0, this.scaledImage.getWidth(), this.scaledImage.getHeight(), (ImageObserver)null);
      g.dispose();
      this.display.setIcon(new ImageIcon(this.scaledImage));
      this.pack();
   }

   private JPanel getJContentPane() {
      if(this.jContentPane == null) {
         this.display = new JLabel();
         this.jContentPane = new JPanel();
         this.jContentPane.setLayout(new BorderLayout());
         this.jContentPane.add(this.display, "Center");
         this.jContentPane.add(this.getHistoryPanel(), "East");
         this.jContentPane.add(this.getActionPanel(), "North");
         this.display.setPreferredSize(new Dimension(320, 480));
         this.display.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent event) {
               MonkeyRecorderFrame.this.touch(event);
            }
         });
      }

      return this.jContentPane;
   }

   private JScrollPane getHistoryPanel() {
      if(this.historyPanel == null) {
         this.historyPanel = new JScrollPane();
         this.historyPanel.getViewport().setView(this.getHistoryList());
      }

      return this.historyPanel;
   }

   private JList getHistoryList() {
      if(this.historyList == null) {
         this.actionListModel = new ActionListModel();
         this.historyList = new JList(this.actionListModel);
      }

      return this.historyList;
   }

   private JPanel getActionPanel() {
      if(this.actionPanel == null) {
         this.actionPanel = new JPanel();
         this.actionPanel.setLayout(new BoxLayout(this.getActionPanel(), 0));
         this.actionPanel.add(this.getWaitButton(), (Object)null);
         this.actionPanel.add(this.getPressButton(), (Object)null);
         this.actionPanel.add(this.getTypeButton(), (Object)null);
         this.actionPanel.add(this.getFlingButton(), (Object)null);
         this.actionPanel.add(this.getExportActionButton(), (Object)null);
         this.actionPanel.add(this.getRefreshButton(), (Object)null);
      }

      return this.actionPanel;
   }

   private JButton getWaitButton() {
      if(this.waitButton == null) {
         this.waitButton = new JButton();
         this.waitButton.setText("Wait");
         this.waitButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
               String howLongStr = JOptionPane.showInputDialog("How many seconds to wait?");
               if(howLongStr != null) {
                  float howLong = Float.parseFloat(howLongStr);
                  MonkeyRecorderFrame.this.addAction(new WaitAction(howLong));
               }

            }
         });
      }

      return this.waitButton;
   }

   private JButton getPressButton() {
      if(this.pressButton == null) {
         this.pressButton = new JButton();
         this.pressButton.setText("Press a Button");
         this.pressButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
               JPanel panel = new JPanel();
               JLabel text = new JLabel("What button to press?");
               JComboBox keys = new JComboBox(PressAction.KEYS);
               keys.setEditable(true);
               JComboBox direction = new JComboBox(PressAction.DOWNUP_FLAG_MAP.values().toArray());
               panel.add(text);
               panel.add(keys);
               panel.add(direction);
               int result = JOptionPane.showConfirmDialog((Component)null, panel, "Input", 2);
               if(result == 0) {
                  BiMap lookupMap = PressAction.DOWNUP_FLAG_MAP.inverse();
                  String flag = (String)lookupMap.get(direction.getSelectedItem());
                  MonkeyRecorderFrame.this.addAction(new PressAction((String)keys.getSelectedItem(), flag));
               }

            }
         });
      }

      return this.pressButton;
   }

   private JButton getTypeButton() {
      if(this.typeButton == null) {
         this.typeButton = new JButton();
         this.typeButton.setText("Type Something");
         this.typeButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
               String whatToType = JOptionPane.showInputDialog("What to type?");
               if(whatToType != null) {
                  MonkeyRecorderFrame.this.addAction(new TypeAction(whatToType));
               }

            }
         });
      }

      return this.typeButton;
   }

   private JButton getFlingButton() {
      if(this.flingButton == null) {
         this.flingButton = new JButton();
         this.flingButton.setText("Fling");
         this.flingButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
               JPanel panel = new JPanel();
               panel.setLayout(new BoxLayout(panel, 1));
               panel.add(new JLabel("Which Direction to fling?"));
               JComboBox directionChooser = new JComboBox(DragAction.Direction.getNames());
               panel.add(directionChooser);
               panel.add(new JLabel("How long to drag (in ms)?"));
               JTextField ms = new JTextField();
               ms.setText("1000");
               panel.add(ms);
               panel.add(new JLabel("How many steps to do it in?"));
               JTextField steps = new JTextField();
               steps.setText("10");
               panel.add(steps);
               int result = JOptionPane.showConfirmDialog((Component)null, panel, "Input", 2);
               if(result == 0) {
                  DragAction.Direction dir = DragAction.Direction.valueOf((String)directionChooser.getSelectedItem());
                  long millis = Long.parseLong(ms.getText());
                  int numSteps = Integer.parseInt(steps.getText());
                  MonkeyRecorderFrame.this.addAction(MonkeyRecorderFrame.this.newFlingAction(dir, numSteps, millis));
               }

            }
         });
      }

      return this.flingButton;
   }

   private DragAction newFlingAction(DragAction.Direction dir, int numSteps, long millis) {
      int width = Integer.parseInt(this.device.getProperty("display.width"));
      int height = Integer.parseInt(this.device.getProperty("display.height"));
      width = (int)((float)width * 0.8F);
      height = (int)((float)height * 0.8F);
      int minW = (int)((float)width * 0.2F);
      int minH = (int)((float)height * 0.2F);
      int midWidth = width / 2;
      int midHeight = height / 2;
      int startx = minW;
      int starty = minH;
      int endx = minW;
      int endy = minH;
      switch(MonkeyRecorderFrame.SyntheticClass_1.$SwitchMap$com$android$monkeyrunner$recorder$actions$DragAction$Direction[dir.ordinal()]) {
      case 1:
         endx = midWidth;
         startx = midWidth;
         starty = height;
         break;
      case 2:
         endx = midWidth;
         startx = midWidth;
         endy = height;
         break;
      case 3:
         endy = midHeight;
         starty = midHeight;
         endx = width;
         break;
      case 4:
         endy = midHeight;
         starty = midHeight;
         startx = width;
      }

      return new DragAction(dir, startx, starty, endx, endy, numSteps, millis);
   }

   private JButton getExportActionButton() {
      if(this.exportActionButton == null) {
         this.exportActionButton = new JButton();
         this.exportActionButton.setText("Export Actions");
         this.exportActionButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
               JFileChooser fc = new JFileChooser();
               if(fc.showSaveDialog((Component)null) == 0) {
                  try {
                     MonkeyRecorderFrame.this.actionListModel.export(fc.getSelectedFile());
                  } catch (FileNotFoundException var4) {
                     MonkeyRecorderFrame.LOG.log(Level.SEVERE, "Unable to save file", var4);
                  }
               }

            }
         });
      }

      return this.exportActionButton;
   }

   private JButton getRefreshButton() {
      if(this.refreshButton == null) {
         this.refreshButton = new JButton();
         this.refreshButton.setText("Refresh Display");
         this.refreshButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
               MonkeyRecorderFrame.this.refreshDisplay();
            }
         });
      }

      return this.refreshButton;
   }

   private void touch(MouseEvent event) {
      int x = event.getX();
      int y = event.getY();
      double scalex = (double)this.currentImage.getWidth() / (double)this.scaledImage.getWidth();
      double scaley = (double)this.currentImage.getHeight() / (double)this.scaledImage.getHeight();
      x = (int)((double)x * scalex);
      y = (int)((double)y * scaley);
      switch(event.getID()) {
      case 500:
         this.addAction(new TouchAction(x, y, MonkeyDevice.DOWN_AND_UP));
         break;
      case 501:
         this.addAction(new TouchAction(x, y, MonkeyDevice.DOWN));
         break;
      case 502:
         this.addAction(new TouchAction(x, y, MonkeyDevice.UP));
      }

   }

   public void addAction(Action a) {
      this.actionListModel.add(a);

      try {
         a.execute(this.device);
      } catch (Exception var3) {
         LOG.log(Level.SEVERE, "Unable to execute action!", var3);
      }

   }

   // $FF: synthetic class
   static class SyntheticClass_1 {
      // $FF: synthetic field
      static final int[] $SwitchMap$com$android$monkeyrunner$recorder$actions$DragAction$Direction = new int[DragAction.Direction.values().length];

      static {
         try {
            $SwitchMap$com$android$monkeyrunner$recorder$actions$DragAction$Direction[DragAction.Direction.NORTH.ordinal()] = 1;
         } catch (NoSuchFieldError var4) {
            ;
         }

         try {
            $SwitchMap$com$android$monkeyrunner$recorder$actions$DragAction$Direction[DragAction.Direction.SOUTH.ordinal()] = 2;
         } catch (NoSuchFieldError var3) {
            ;
         }

         try {
            $SwitchMap$com$android$monkeyrunner$recorder$actions$DragAction$Direction[DragAction.Direction.EAST.ordinal()] = 3;
         } catch (NoSuchFieldError var2) {
            ;
         }

         try {
            $SwitchMap$com$android$monkeyrunner$recorder$actions$DragAction$Direction[DragAction.Direction.WEST.ordinal()] = 4;
         } catch (NoSuchFieldError var1) {
            ;
         }

      }
   }
}
