package com.android.monkeyrunner.recorder;

import com.android.chimpchat.ChimpChat;
import com.android.chimpchat.core.IChimpDevice;
import com.android.monkeyrunner.MonkeyDevice;
import com.android.monkeyrunner.recorder.MonkeyRecorderFrame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MonkeyRecorder {
   private static final Logger LOG = Logger.getLogger(MonkeyRecorder.class.getName());
   private static final Object LOCK = new Object();

   public static void start(MonkeyDevice device) {
      start(device.getImpl());
   }

   static void start(final IChimpDevice device) {
      MonkeyRecorderFrame frame = new MonkeyRecorderFrame(device);
      frame.setDefaultCloseOperation(3);
      frame.addWindowListener(new WindowAdapter() {
         public void windowClosed(WindowEvent e) {
            device.dispose();
            synchronized(MonkeyRecorder.LOCK) {
               MonkeyRecorder.LOCK.notifyAll();
            }
         }
      });
      frame.setVisible(true);
      Object var2 = LOCK;
      synchronized(LOCK) {
         try {
            LOCK.wait();
         } catch (InterruptedException var5) {
            LOG.log(Level.SEVERE, "Unexpected Exception", var5);
         }

      }
   }

   public static void main(String[] args) {
      ChimpChat chimp = ChimpChat.getInstance();
      start(chimp.waitForConnection());
   }
}
