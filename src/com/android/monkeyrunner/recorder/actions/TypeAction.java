package com.android.monkeyrunner.recorder.actions;

import com.android.chimpchat.core.IChimpDevice;
import com.android.monkeyrunner.recorder.actions.Action;
import com.android.monkeyrunner.recorder.actions.PyDictUtilBuilder;

public class TypeAction implements Action {
   private final String whatToType;

   public TypeAction(String whatToType) {
      this.whatToType = whatToType;
   }

   public String getDisplayName() {
      return String.format("Type \"%s\"", new Object[]{this.whatToType});
   }

   public String serialize() {
      String pydict = PyDictUtilBuilder.newBuilder().add("message", this.whatToType).build();
      return "TYPE|" + pydict;
   }

   public void execute(IChimpDevice device) {
      device.type(this.whatToType);
   }
}
