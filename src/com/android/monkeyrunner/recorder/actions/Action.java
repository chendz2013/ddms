package com.android.monkeyrunner.recorder.actions;

import com.android.chimpchat.core.IChimpDevice;

public interface Action {
   String serialize();

   String getDisplayName();

   void execute(IChimpDevice var1) throws Exception;
}
