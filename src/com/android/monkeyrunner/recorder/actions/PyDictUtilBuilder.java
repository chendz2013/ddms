package com.android.monkeyrunner.recorder.actions;

public class PyDictUtilBuilder {
   private StringBuilder sb = new StringBuilder();

   public PyDictUtilBuilder() {
      this.sb.append("{");
   }

   public static PyDictUtilBuilder newBuilder() {
      return new PyDictUtilBuilder();
   }

   private void addHelper(String key, String value) {
      this.sb.append("\'").append(key).append("\'");
      this.sb.append(":").append(value).append(",");
   }

   public PyDictUtilBuilder add(String key, int value) {
      this.addHelper(key, Integer.toString(value));
      return this;
   }

   public PyDictUtilBuilder add(String key, float value) {
      this.addHelper(key, Float.toString(value));
      return this;
   }

   public PyDictUtilBuilder add(String key, String value) {
      this.addHelper(key, "\'" + value + "\'");
      return this;
   }

   public String build() {
      this.sb.append("}");
      return this.sb.toString();
   }

   public PyDictUtilBuilder addTuple(String key, int x, int y) {
      String valuestr = "(" + x + "," + y + ")";
      this.addHelper(key, valuestr);
      return this;
   }
}
