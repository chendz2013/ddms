package com.android.monkeyrunner.recorder.actions;

import com.android.chimpchat.core.IChimpDevice;
import com.android.chimpchat.core.TouchPressType;
import com.android.monkeyrunner.MonkeyDevice;
import com.android.monkeyrunner.recorder.actions.Action;
import com.android.monkeyrunner.recorder.actions.PyDictUtilBuilder;
import com.google.common.collect.BiMap;
import com.google.common.collect.ImmutableBiMap;

public class PressAction implements Action {
   public static String[] KEYS = new String[]{"MENU", "HOME", "SEARCH"};
   public static final BiMap<String, String> DOWNUP_FLAG_MAP;
   private final String key;
   private final String downUpFlag;

   public PressAction(String key, String downUpFlag) {
      this.key = key;
      this.downUpFlag = downUpFlag;
   }

   public PressAction(String key) {
      this(key, MonkeyDevice.DOWN_AND_UP);
   }

   public String getDisplayName() {
      return String.format("%s button %s", new Object[]{DOWNUP_FLAG_MAP.get(this.downUpFlag), this.key});
   }

   public String serialize() {
      String pydict = PyDictUtilBuilder.newBuilder().add("name", this.key).add("type", this.downUpFlag).build();
      return "PRESS|" + pydict;
   }

   public void execute(IChimpDevice device) {
      device.press(this.key, TouchPressType.fromIdentifier(this.downUpFlag));
   }

   static {
      DOWNUP_FLAG_MAP = ImmutableBiMap.of(MonkeyDevice.DOWN_AND_UP, "Press", MonkeyDevice.DOWN, "Down", MonkeyDevice.UP, "Up");
   }
}
