package com.android.monkeyrunner.recorder.actions;

import com.android.chimpchat.core.IChimpDevice;
import com.android.monkeyrunner.recorder.actions.Action;
import com.android.monkeyrunner.recorder.actions.PyDictUtilBuilder;

public class DragAction implements Action {
   private final long timeMs;
   private final int steps;
   private final int startx;
   private final int starty;
   private final int endx;
   private final int endy;
   private final DragAction.Direction dir;

   public DragAction(DragAction.Direction dir, int startx, int starty, int endx, int endy, int numSteps, long millis) {
      this.dir = dir;
      this.startx = startx;
      this.starty = starty;
      this.endx = endx;
      this.endy = endy;
      this.steps = numSteps;
      this.timeMs = millis;
   }

   public String getDisplayName() {
      return String.format("Fling %s", new Object[]{this.dir.name().toLowerCase()});
   }

   public String serialize() {
      float duration = (float)this.timeMs / 1000.0F;
      String pydict = PyDictUtilBuilder.newBuilder().addTuple("start", this.startx, this.starty).addTuple("end", this.endx, this.endy).add("duration", duration).add("steps", this.steps).build();
      return "DRAG|" + pydict;
   }

   public void execute(IChimpDevice device) {
      device.drag(this.startx, this.starty, this.endx, this.endy, this.steps, this.timeMs);
   }

   public static enum Direction {
      NORTH,
      SOUTH,
      EAST,
      WEST;

      private static String[] names;

      public static String[] getNames() {
         return names;
      }

      static {
         DragAction.Direction[] values = values();
         names = new String[values.length];

         for(int x = 0; x < values.length; ++x) {
            names[x] = values[x].name();
         }

      }
   }
}
