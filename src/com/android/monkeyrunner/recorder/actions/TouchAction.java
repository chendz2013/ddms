package com.android.monkeyrunner.recorder.actions;

import com.android.chimpchat.core.IChimpDevice;
import com.android.chimpchat.core.TouchPressType;
import com.android.monkeyrunner.MonkeyDevice;
import com.android.monkeyrunner.recorder.actions.Action;
import com.android.monkeyrunner.recorder.actions.PyDictUtilBuilder;
import com.google.common.collect.BiMap;
import com.google.common.collect.ImmutableBiMap;

public class TouchAction implements Action {
   public static final BiMap<String, String> DOWNUP_FLAG_MAP;
   private final int x;
   private final int y;
   private final String direction;

   public TouchAction(int x, int y, String direction) {
      this.x = x;
      this.y = y;
      this.direction = direction;
   }

   public String getDisplayName() {
      return String.format("%s touchscreen at (%d, %d)", new Object[]{DOWNUP_FLAG_MAP.get(this.direction), Integer.valueOf(this.x), Integer.valueOf(this.y)});
   }

   public void execute(IChimpDevice device) throws Exception {
      device.touch(this.x, this.y, TouchPressType.fromIdentifier(this.direction));
   }

   public String serialize() {
      String pydict = PyDictUtilBuilder.newBuilder().add("x", this.x).add("y", this.y).add("type", this.direction).build();
      return "TOUCH|" + pydict;
   }

   static {
      DOWNUP_FLAG_MAP = ImmutableBiMap.of(MonkeyDevice.DOWN_AND_UP, "Tap", MonkeyDevice.DOWN, "Down", MonkeyDevice.UP, "Up");
   }
}
