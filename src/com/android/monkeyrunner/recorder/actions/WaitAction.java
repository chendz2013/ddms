package com.android.monkeyrunner.recorder.actions;

import com.android.chimpchat.core.IChimpDevice;
import com.android.monkeyrunner.recorder.actions.Action;
import com.android.monkeyrunner.recorder.actions.PyDictUtilBuilder;

public class WaitAction implements Action {
   private final float howLongSeconds;

   public WaitAction(float howLongSeconds) {
      this.howLongSeconds = howLongSeconds;
   }

   public String getDisplayName() {
      return String.format("Wait for %g seconds", new Object[]{Float.valueOf(this.howLongSeconds)});
   }

   public String serialize() {
      String pydict = PyDictUtilBuilder.newBuilder().add("seconds", this.howLongSeconds).build();
      return "WAIT|" + pydict;
   }

   public void execute(IChimpDevice device) throws Exception {
      long ms = (long)(1000.0F * this.howLongSeconds);
      Thread.sleep(ms);
   }
}
