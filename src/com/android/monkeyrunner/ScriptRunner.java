package com.android.monkeyrunner;

import com.android.monkeyrunner.MonkeyRunnerOptions;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;
import com.google.common.collect.ImmutableMap.Builder;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.python.core.Py;
import org.python.core.PyException;
import org.python.core.PyObject;
import org.python.core.PySystemState;
import org.python.util.JLineConsole;
import org.python.util.PythonInterpreter;

public class ScriptRunner {
   private static final Logger LOG = Logger.getLogger(MonkeyRunnerOptions.class.getName());
   private final Object scope;
   private final String variable;

   private ScriptRunner(Object scope, String variable) {
      this.scope = scope;
      this.variable = variable;
   }

   public static ScriptRunner newInstance(Object scope, String variable) {
      return new ScriptRunner(scope, variable);
   }

   public static int run(String executablePath, String scriptfilename, Collection<String> args, Map<String, Predicate<PythonInterpreter>> plugins) {
      File f = new File(scriptfilename);
      ArrayList classpath = Lists.newArrayList(new String[]{f.getParent()});
      classpath.addAll(plugins.keySet());
      String[] argv = new String[args.size() + 1];
      argv[0] = f.getAbsolutePath();
      int x = 1;

      String e;
      for(Iterator python = args.iterator(); python.hasNext(); argv[x++] = e) {
         e = (String)python.next();
      }

      initPython(executablePath, classpath, argv);
      PythonInterpreter var15 = new PythonInterpreter();
      Iterator var16 = plugins.entrySet().iterator();

      while(var16.hasNext()) {
         Entry entry = (Entry)var16.next();

         boolean success;
         try {
            success = ((Predicate)entry.getValue()).apply(var15);
         } catch (Exception var14) {
            LOG.log(Level.SEVERE, "Plugin Main through an exception.", var14);
            continue;
         }

         if(!success) {
            LOG.severe("Plugin Main returned error for: " + (String)entry.getKey());
         }
      }

      var15.set("__name__", "__main__");
      var15.set("__file__", scriptfilename);

      try {
         var15.execfile(scriptfilename);
         return 0;
      } catch (PyException var13) {
         if(Py.SystemExit.equals(var13.type)) {
            return ((Integer)var13.value.__tojava__(Integer.class)).intValue();
         } else {
            LOG.log(Level.SEVERE, "Script terminated due to an exception", var13);
            return 1;
         }
      }
   }

   public static void runString(String executablePath, String script) {
      initPython(executablePath);
      PythonInterpreter python = new PythonInterpreter();
      python.exec(script);
   }

   public static Map<String, PyObject> runStringAndGet(String executablePath, String script, String... names) {
      return runStringAndGet(executablePath, script, (Collection)Arrays.asList(names));
   }

   public static Map<String, PyObject> runStringAndGet(String executablePath, String script, Collection<String> names) {
      initPython(executablePath);
      PythonInterpreter python = new PythonInterpreter();
      python.exec(script);
      Builder builder = ImmutableMap.builder();
      Iterator i$ = names.iterator();

      while(i$.hasNext()) {
         String name = (String)i$.next();
         builder.put(name, python.get(name));
      }

      return builder.build();
   }

   private static void initPython(String executablePath) {
      List arg = Collections.emptyList();
      initPython(executablePath, arg, new String[]{""});
   }

   private static void initPython(String executablePath, Collection<String> pythonPath, String[] argv) {
      Properties props = new Properties();
      StringBuilder sb = new StringBuilder();
      sb.append(System.getProperty("java.class.path"));
      Iterator frameworkDir = pythonPath.iterator();

      while(frameworkDir.hasNext()) {
         String monkeyRunnerJar = (String)frameworkDir.next();
         sb.append(":").append(monkeyRunnerJar);
      }

      props.setProperty("python.path", sb.toString());
      props.setProperty("python.verbose", "error");
      props.setProperty("python.executable", executablePath);
      PythonInterpreter.initialize(System.getProperties(), props, argv);
      String frameworkDir1 = System.getProperty("java.ext.dirs");
      File monkeyRunnerJar1 = new File(frameworkDir1, "monkeyrunner.jar");
      if(monkeyRunnerJar1.canRead()) {
         PySystemState.packageManager.addJar(monkeyRunnerJar1.getAbsolutePath(), false);
      }

   }

   public static void console(String executablePath) {
      initPython(executablePath);
      JLineConsole python = new JLineConsole();
      python.interact();
   }
}
