package com.android.monkeyrunner;

import com.android.monkeyrunner.MonkeyRunner;
import com.android.monkeyrunner.doc.MonkeyRunnerExported;
import com.google.clearsilver.jsilver.JSilver;
import com.google.clearsilver.jsilver.data.Data;
import com.google.clearsilver.jsilver.resourceloader.ClassLoaderResourceLoader;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public final class MonkeyRunnerHelp {
   private static final String HELP = "help";
   private static final String NAME = "name";
   private static final String DOC = "doc";
   private static final String ARGUMENT = "argument";
   private static final String RETURNS = "returns";
   private static final String TYPE = "type";
   private static Comparator<Member> MEMBER_SORTER = new Comparator() {
      @Override
      public int compare(Object o1, Object o2) {
         return compare((Member)o1, (Member)o2);
      }

      public int compare(Member o1, Member o2) {
         return o1.getName().compareTo(o2.getName());
      }
   };
   private static Comparator<Class<?>> CLASS_SORTER = new Comparator() {
      @Override
      public int compare(Object o1, Object o2) {
         return compare((Class<?>)o1, (Class<?>)o2);
      }

      public int compare(Class<?> o1, Class<?> o2) {
         return o1.getName().compareTo(o2.getName());
      }
   };

   private static void getAllExportedClasses(Set<Field> fields, Set<Method> methods, Set<Constructor<?>> constructors, Set<Class<?>> enums) {
      final HashSet classesVisited = Sets.newHashSet();
      HashSet classesToVisit = Sets.newHashSet();
      classesToVisit.add(MonkeyRunner.class);
      Predicate haventSeen = new Predicate() {
         @Override
         public boolean apply(Object o) {
            return apply((Class<?>)o);
         }

         public boolean apply(Class<?> clz) {
            return !classesVisited.contains(clz);
         }
      };

      while(!classesToVisit.isEmpty()) {
         classesVisited.addAll(classesToVisit);
         ArrayList newClasses = Lists.newArrayList();
         Iterator i$ = classesToVisit.iterator();

         while(i$.hasNext()) {
            Class clz = (Class)i$.next();
            if(clz.isEnum() && clz.isAnnotationPresent(MonkeyRunnerExported.class)) {
               enums.add(clz);
            }

            Constructor[] arr$ = clz.getConstructors();
            int len$ = arr$.length;

            int i$1;
            for(i$1 = 0; i$1 < len$; ++i$1) {
               Constructor toAdd = arr$[i$1];
               newClasses.addAll(Collections2.filter(Arrays.asList(toAdd.getParameterTypes()), haventSeen));
               if(toAdd.isAnnotationPresent(MonkeyRunnerExported.class)) {
                  constructors.add(toAdd);
               }
            }

            Field[] var15 = clz.getFields();
            len$ = var15.length;

            for(i$1 = 0; i$1 < len$; ++i$1) {
               Field var19 = var15[i$1];
               if(haventSeen.apply(var19.getClass())) {
                  newClasses.add(var19.getClass());
               }

               if(var19.isAnnotationPresent(MonkeyRunnerExported.class)) {
                  fields.add(var19);
               }
            }

            Method[] var16 = clz.getMethods();
            len$ = var16.length;

            for(i$1 = 0; i$1 < len$; ++i$1) {
               Method var17 = var16[i$1];
               newClasses.addAll(Collections2.filter(Arrays.asList(var17.getParameterTypes()), haventSeen));
               if(haventSeen.apply(var17.getReturnType())) {
                  newClasses.add(var17.getReturnType());
               }

               if(var17.isAnnotationPresent(MonkeyRunnerExported.class)) {
                  methods.add(var17);
               }
            }

            Class[] var14 = clz.getClasses();
            len$ = var14.length;

            for(i$1 = 0; i$1 < len$; ++i$1) {
               Class var18 = var14[i$1];
               if(haventSeen.apply(var18)) {
                  newClasses.add(var18);
               }
            }
         }

         classesToVisit.clear();
         classesToVisit.addAll(newClasses);
      }

   }

   public static String helpString(String format) {
      ClassLoaderResourceLoader resourceLoader = new ClassLoaderResourceLoader(MonkeyRunner.class.getClassLoader(), "com/android/monkeyrunner");
      JSilver jsilver = new JSilver(resourceLoader);
      Data hdf;
      if(!"html".equals(format) && !"text".equals(format) && !"sdk-docs".equals(format)) {
         if("hdf".equals(format)) {
            hdf = buildHelpHdf(jsilver);
            return hdf.toString();
         } else {
            return "";
         }
      } else {
         try {
            hdf = buildHelpHdf(jsilver);
            return jsilver.render(format + ".cs", hdf);
         } catch (IOException var4) {
            return "";
         }
      }
   }

   private static void paragraphsIntoHDF(String prefix, String value, Data hdf) {
      String[] paragraphs = value.split("\n");
      int x = 0;
      String[] arr$ = paragraphs;
      int len$ = paragraphs.length;

      for(int i$ = 0; i$ < len$; ++i$) {
         String para = arr$[i$];
         hdf.setValue(prefix + "." + x, para);
         ++x;
      }

   }

   private static Data buildHelpHdf(JSilver jsilver) {
      Data hdf = jsilver.createData();
      int outputItemCount = 0;
      TreeSet fields = Sets.newTreeSet(MEMBER_SORTER);
      TreeSet methods = Sets.newTreeSet(MEMBER_SORTER);
      TreeSet constructors = Sets.newTreeSet(MEMBER_SORTER);
      TreeSet classes = Sets.newTreeSet(CLASS_SORTER);
      getAllExportedClasses(fields, methods, constructors, classes);

      Iterator i$;
      String prefix;
      MonkeyRunnerExported annotation;
      for(i$ = classes.iterator(); i$.hasNext(); ++outputItemCount) {
         Class m = (Class)i$.next();
         prefix = "help." + outputItemCount + ".";
         hdf.setValue(prefix + "name", m.getCanonicalName());
         annotation = (MonkeyRunnerExported)m.getAnnotation(MonkeyRunnerExported.class);
         paragraphsIntoHDF(prefix + "doc", annotation.doc(), hdf);
         hdf.setValue(prefix + "type", MonkeyRunnerHelp.Type.ENUM.name());
         Object[] className = m.getEnumConstants();
         String[] methodName = annotation.argDocs();
         if(className.length > 0) {
            for(int argDocs = 0; argDocs < className.length; ++argDocs) {
               String aargs = prefix + "argument" + "." + argDocs + ".";
               hdf.setValue(aargs + "name", className[argDocs].toString());
               if(methodName.length > argDocs) {
                  paragraphsIntoHDF(aargs + "doc", methodName[argDocs], hdf);
               }
            }
         }
      }

      for(i$ = methods.iterator(); i$.hasNext(); ++outputItemCount) {
         Method var17 = (Method)i$.next();
         prefix = "help." + outputItemCount + ".";
         annotation = (MonkeyRunnerExported)var17.getAnnotation(MonkeyRunnerExported.class);
         String var18 = var17.getDeclaringClass().getCanonicalName();
         String var19 = var18 + "." + var17.getName();
         hdf.setValue(prefix + "name", var19);
         paragraphsIntoHDF(prefix + "doc", annotation.doc(), hdf);
         if(annotation.args().length > 0) {
            String[] var20 = annotation.argDocs();
            String[] var21 = annotation.args();

            for(int x = 0; x < var21.length; ++x) {
               String argPrefix = prefix + "argument" + "." + x + ".";
               hdf.setValue(argPrefix + "name", var21[x]);
               if(var20.length > x) {
                  paragraphsIntoHDF(argPrefix + "doc", var20[x], hdf);
               }
            }
         }

         if(!"".equals(annotation.returns())) {
            paragraphsIntoHDF(prefix + "returns", annotation.returns(), hdf);
         }
      }

      return hdf;
   }

   public static Collection<String> getAllDocumentedClasses() {
      TreeSet fields = Sets.newTreeSet(MEMBER_SORTER);
      TreeSet methods = Sets.newTreeSet(MEMBER_SORTER);
      TreeSet constructors = Sets.newTreeSet(MEMBER_SORTER);
      TreeSet classes = Sets.newTreeSet(CLASS_SORTER);
      getAllExportedClasses(fields, methods, constructors, classes);
      HashSet allClasses = Sets.newHashSet();
      allClasses.addAll(classes);
      Iterator i$ = fields.iterator();

      while(i$.hasNext()) {
         Field constructor = (Field)i$.next();
         allClasses.add(constructor.getDeclaringClass());
      }

      i$ = methods.iterator();

      while(i$.hasNext()) {
         Method constructor2 = (Method)i$.next();
         allClasses.add(constructor2.getDeclaringClass());
      }

      i$ = constructors.iterator();

      while(i$.hasNext()) {
         Constructor constructor1 = (Constructor)i$.next();
         allClasses.add(constructor1.getDeclaringClass());
      }

      return Collections2.transform(allClasses, new Function() {
         @Override
         public Object apply(Object o) {
            return apply((Class<?>)o);
         }

         public String apply(Class<?> clz) {
            return clz.getName();
         }
      });
   }

   private static enum Type {
      ENUM,
      FIELD,
      METHOD;
   }
}
