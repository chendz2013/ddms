package com.android.monkeyrunner;

import com.google.common.collect.Maps;
import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;

public class MonkeyFormatter extends Formatter {
   public static final Formatter DEFAULT_INSTANCE = new MonkeyFormatter();
   private static final SimpleDateFormat FORMAT = new SimpleDateFormat("yyMMdd HH:mm:ss.SSS");
   private static Map<Level, String> LEVEL_TO_STRING_CACHE = Maps.newHashMap();

   private static final String levelToString(Level level) {
      String levelName = (String)LEVEL_TO_STRING_CACHE.get(level);
      if(levelName == null) {
         levelName = level.getName().substring(0, 1);
         LEVEL_TO_STRING_CACHE.put(level, levelName);
      }

      return levelName;
   }

   private static String getHeader(LogRecord record) {
      StringBuilder sb = new StringBuilder();
      sb.append(FORMAT.format(new Date(record.getMillis()))).append(":");
      sb.append(levelToString(record.getLevel())).append(" ");
      sb.append("[").append(Thread.currentThread().getName()).append("] ");
      String loggerName = record.getLoggerName();
      if(loggerName != null) {
         sb.append("[").append(loggerName).append("]");
      }

      return sb.toString();
   }

   public String format(LogRecord record) {
      Throwable thrown = record.getThrown();
      String header = getHeader(record);
      StringBuilder sb = new StringBuilder();
      sb.append(header);
      sb.append(" ").append(this.formatMessage(record));
      sb.append("\n");
      if(thrown != null) {
         MonkeyFormatter.PrintWriterWithHeader pw = new MonkeyFormatter.PrintWriterWithHeader(header);
         thrown.printStackTrace(pw);
         sb.append(pw.toString());
      }

      return sb.toString();
   }

   private class PrintWriterWithHeader extends PrintWriter {
      private  ByteArrayOutputStream out;
      private  String header;

      public PrintWriterWithHeader(String header) {
         this(header, null);
      }

      public PrintWriterWithHeader(String header, ByteArrayOutputStream out) {
         super(out, true);
         this.header = header;
         this.out = out;
      }

      public void println(Object x) {
         this.print(this.header);
         super.println(x);
      }

      public void println(String x) {
         this.print(this.header);
         super.println(x);
      }

      public String toString() {
         return this.out.toString();
      }
   }
}
