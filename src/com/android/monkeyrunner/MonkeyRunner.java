package com.android.monkeyrunner;

import com.android.chimpchat.ChimpChat;
import com.android.chimpchat.core.ChimpImageBase;
import com.android.chimpchat.core.IChimpDevice;
import com.android.chimpchat.core.IChimpImage;
import com.android.monkeyrunner.JythonUtils;
import com.android.monkeyrunner.MonkeyDevice;
import com.android.monkeyrunner.MonkeyImage;
import com.android.monkeyrunner.MonkeyRunnerHelp;
import com.android.monkeyrunner.doc.MonkeyRunnerExported;
import com.google.common.base.Functions;
import com.google.common.base.Preconditions;
import com.google.common.collect.Collections2;
import java.awt.Component;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.JOptionPane;
import org.python.core.ArgParser;
import org.python.core.ClassDictInit;
import org.python.core.PyException;
import org.python.core.PyObject;

@MonkeyRunnerExported(
   doc = "Main entry point for MonkeyRunner"
)
public class MonkeyRunner extends PyObject implements ClassDictInit {
   private static final Logger LOG = Logger.getLogger(MonkeyRunner.class.getCanonicalName());
   private static ChimpChat chimpchat;

   public static void classDictInit(PyObject dict) {
      JythonUtils.convertDocAnnotationsForClass(MonkeyRunner.class, dict);
   }

   static void setChimpChat(ChimpChat chimp) {
      chimpchat = chimp;
   }

   @MonkeyRunnerExported(
      doc = "Waits for the workstation to connect to the device.",
      args = {"timeout", "deviceId"},
      argDocs = {"The timeout in seconds to wait. The default is to wait indefinitely.", "A regular expression that specifies the device name. See the documentation for \'adb\' in the Developer Guide to learn more about device names."},
      returns = "A ChimpDevice object representing the connected device."
   )
   public static MonkeyDevice waitForConnection(PyObject[] args, String[] kws) {
      ArgParser ap = JythonUtils.createArgParser(args, kws);
      Preconditions.checkNotNull(ap);

      long timeoutMs;
      try {
         double device = JythonUtils.getFloat(ap, 0);
         timeoutMs = (long)(device * 1000.0D);
      } catch (PyException var7) {
         timeoutMs = Long.MAX_VALUE;
      }

      IChimpDevice device1 = chimpchat.waitForConnection(timeoutMs, ap.getString(1, ".*"));
      MonkeyDevice chimpDevice = new MonkeyDevice(device1);
      return chimpDevice;
   }

   @MonkeyRunnerExported(
      doc = "Pause the currently running program for the specified number of seconds.",
      args = {"seconds"},
      argDocs = {"The number of seconds to pause."}
   )
   public static void sleep(PyObject[] args, String[] kws) {
      ArgParser ap = JythonUtils.createArgParser(args, kws);
      Preconditions.checkNotNull(ap);
      double seconds = JythonUtils.getFloat(ap, 0);
      long ms = (long)(seconds * 1000.0D);

      try {
         Thread.sleep(ms);
      } catch (InterruptedException var8) {
         LOG.log(Level.SEVERE, "Error sleeping", var8);
      }

   }

   @MonkeyRunnerExported(
      doc = "Format and display the API reference for MonkeyRunner.",
      args = {"format"},
      argDocs = {"The desired format for the output, either \'text\' for plain text or \'html\' for HTML markup."},
      returns = "A string containing the help text in the desired format."
   )
   public static String help(PyObject[] args, String[] kws) {
      ArgParser ap = JythonUtils.createArgParser(args, kws);
      Preconditions.checkNotNull(ap);
      String format = ap.getString(0, "text");
      return MonkeyRunnerHelp.helpString(format);
   }

   @MonkeyRunnerExported(
      doc = "Display an alert dialog to the process running the current script.  The dialog is modal, so the script stops until the user dismisses the dialog.",
      args = {"message", "title", "okTitle"},
      argDocs = {"The message to display in the dialog.", "The dialog\'s title. The default value is \'Alert\'.", "The text to use in the dialog button. The default value is \'OK\'."}
   )
   public static void alert(PyObject[] args, String[] kws) {
      ArgParser ap = JythonUtils.createArgParser(args, kws);
      Preconditions.checkNotNull(ap);
      String message = ap.getString(0);
      String title = ap.getString(1, "Alert");
      String buttonTitle = ap.getString(2, "OK");
      alert(message, title, buttonTitle);
   }

   @MonkeyRunnerExported(
      doc = "Display a dialog that accepts input. The dialog is ,modal, so the script stops until the user clicks one of the two dialog buttons. To enter a value, the user enters the value and clicks the \'OK\' button. To quit the dialog without entering a value, the user clicks the \'Cancel\' button. Use the supplied arguments for this method to customize the text for these buttons.",
      args = {"message", "initialValue", "title", "okTitle", "cancelTitle"},
      argDocs = {"The prompt message to display in the dialog.", "The initial value to supply to the user. The default is an empty string)", "The dialog\'s title. The default is \'Input\'", "The text to use in the dialog\'s confirmation button. The default is \'OK\'.The text to use in the dialog\'s \'cancel\' button. The default is \'Cancel\'."},
      returns = "The test entered by the user, or None if the user canceled the input;"
   )
   public static String input(PyObject[] args, String[] kws) {
      ArgParser ap = JythonUtils.createArgParser(args, kws);
      Preconditions.checkNotNull(ap);
      String message = ap.getString(0);
      String initialValue = ap.getString(1, "");
      String title = ap.getString(2, "Input");
      return input(message, initialValue, title);
   }

   @MonkeyRunnerExported(
      doc = "Display a choice dialog that allows the user to select a single item from a list of items.",
      args = {"message", "choices", "title"},
      argDocs = {"The prompt message to display in the dialog.", "An iterable Python type containing a list of choices to display", "The dialog\'s title. The default is \'Input\'"},
      returns = "The 0-based numeric offset of the selected item in the iterable."
   )
   public static int choice(PyObject[] args, String[] kws) {
      ArgParser ap = JythonUtils.createArgParser(args, kws);
      Preconditions.checkNotNull(ap);
      String message = ap.getString(0);
      Collection choices = Collections2.transform(JythonUtils.getList(ap, 1), Functions.toStringFunction());
      String title = ap.getString(2, "Input");
      return choice(message, title, choices);
   }

   @MonkeyRunnerExported(
      doc = "Loads a MonkeyImage from a file.",
      args = {"path"},
      argDocs = {"The path to the file to load.  This file path is in terms of the computer running MonkeyRunner and not a path on the Android Device. "},
      returns = "A new MonkeyImage representing the specified file"
   )
   public static MonkeyImage loadImageFromFile(PyObject[] args, String[] kws) {
      ArgParser ap = JythonUtils.createArgParser(args, kws);
      Preconditions.checkNotNull(ap);
      String path = ap.getString(0);
      IChimpImage image = ChimpImageBase.loadImageFromFile(path);
      return new MonkeyImage(image);
   }

   public static void alert(String message, String title, String okTitle) {
      Object[] options = new Object[]{okTitle};
      JOptionPane.showOptionDialog((Component)null, message, title, -1, 1, (Icon)null, options, options[0]);
   }

   public static int choice(String message, String title, Collection<String> choices) {
      Object[] possibleValues = choices.toArray();
      Object selectedValue = JOptionPane.showInputDialog((Component)null, message, title, 3, (Icon)null, possibleValues, possibleValues[0]);

      for(int x = 0; x < possibleValues.length; ++x) {
         if(possibleValues[x].equals(selectedValue)) {
            return x;
         }
      }

      return -1;
   }

   public static String input(String message, String initialValue, String title) {
      return (String)JOptionPane.showInputDialog((Component)null, message, title, 3, (Icon)null, (Object[])null, initialValue);
   }
}
