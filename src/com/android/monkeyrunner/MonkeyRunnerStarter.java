package com.android.monkeyrunner;

import com.android.chimpchat.ChimpChat;
import com.android.monkeyrunner.MonkeyFormatter;
import com.android.monkeyrunner.MonkeyRunner;
import com.android.monkeyrunner.MonkeyRunnerOptions;
import com.android.monkeyrunner.ScriptRunner;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;
import java.util.jar.Attributes;
import java.util.jar.JarFile;
import java.util.jar.Manifest;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;
import org.python.util.PythonInterpreter;

public class MonkeyRunnerStarter {
   private static final Logger LOG = Logger.getLogger(MonkeyRunnerStarter.class.getName());
   private static final String MONKEY_RUNNER_MAIN_MANIFEST_NAME = "MonkeyRunnerStartupRunner";
   private final ChimpChat chimp;
   private final MonkeyRunnerOptions options;

   public MonkeyRunnerStarter(MonkeyRunnerOptions options) {
      TreeMap chimp_options = new TreeMap();
      chimp_options.put("backend", options.getBackendName());
      this.options = options;
      this.chimp = ChimpChat.getInstance(chimp_options);
      MonkeyRunner.setChimpChat(this.chimp);
   }

   private int run() {
      String monkeyRunnerPath = System.getProperty("com.android.monkeyrunner.bindir") + File.separator + "monkeyrunner";
      Map plugins = this.handlePlugins();
      if(this.options.getScriptFile() == null) {
         ScriptRunner.console(monkeyRunnerPath);
         this.chimp.shutdown();
         return 0;
      } else {
         int error = ScriptRunner.run(monkeyRunnerPath, this.options.getScriptFile().getAbsolutePath(), this.options.getArguments(), plugins);
         this.chimp.shutdown();
         return error;
      }
   }

   private Predicate<PythonInterpreter> handlePlugin(File f) {
      JarFile jarFile;
      try {
         jarFile = new JarFile(f);
      } catch (IOException var16) {
         LOG.log(Level.SEVERE, "Unable to open plugin file.  Is it a jar file? " + f.getAbsolutePath(), var16);
         return Predicates.alwaysFalse();
      }

      Manifest manifest;
      try {
         manifest = jarFile.getManifest();
      } catch (IOException var15) {
         LOG.log(Level.SEVERE, "Unable to get manifest file from jar: " + f.getAbsolutePath(), var15);
         return Predicates.alwaysFalse();
      }

      Attributes mainAttributes = manifest.getMainAttributes();
      String pluginClass = mainAttributes.getValue("MonkeyRunnerStartupRunner");
      if(pluginClass == null) {
         return Predicates.alwaysTrue();
      } else {
         URL url;
         try {
            url = f.toURI().toURL();
         } catch (MalformedURLException var14) {
            LOG.log(Level.SEVERE, "Unable to convert file to url " + f.getAbsolutePath(), var14);
            return Predicates.alwaysFalse();
         }

         URLClassLoader classLoader = new URLClassLoader(new URL[]{url}, ClassLoader.getSystemClassLoader());

         Class clz;
         try {
            clz = Class.forName(pluginClass, true, classLoader);
         } catch (ClassNotFoundException var13) {
            LOG.log(Level.SEVERE, "Unable to load the specified plugin: " + pluginClass, var13);
            return Predicates.alwaysFalse();
         }

         Object loadedObject;
         try {
            loadedObject = clz.newInstance();
         } catch (InstantiationException var11) {
            LOG.log(Level.SEVERE, "Unable to load the specified plugin: " + pluginClass, var11);
            return Predicates.alwaysFalse();
         } catch (IllegalAccessException var12) {
            LOG.log(Level.SEVERE, "Unable to load the specified plugin (did you make it public?): " + pluginClass, var12);
            return Predicates.alwaysFalse();
         }

         if(loadedObject instanceof Runnable) {
            final Runnable run = (Runnable)loadedObject;
            return new Predicate() {

               @Override
               public boolean apply(Object o) {
                  return apply((PythonInterpreter)o);
               }

               public boolean apply(PythonInterpreter i) {
                  run.run();
                  return true;
               }
            };
         } else if(loadedObject instanceof Predicate) {
            return (Predicate)loadedObject;
         } else {
            LOG.severe("Unable to coerce object into correct type: " + pluginClass);
            return Predicates.alwaysFalse();
         }
      }
   }

   private Map<String, Predicate<PythonInterpreter>> handlePlugins() {
      Builder builder = ImmutableMap.builder();
      Iterator i$ = this.options.getPlugins().iterator();

      while(i$.hasNext()) {
         File f = (File)i$.next();
         builder.put(f.getAbsolutePath(), this.handlePlugin(f));
      }

      return builder.build();
   }

   private static final void replaceAllLogFormatters(Formatter form, Level level) {
      LogManager mgr = LogManager.getLogManager();
      Enumeration loggerNames = mgr.getLoggerNames();

      while(loggerNames.hasMoreElements()) {
         String loggerName = (String)loggerNames.nextElement();
         Logger logger = mgr.getLogger(loggerName);
         Handler[] arr$ = logger.getHandlers();
         int len$ = arr$.length;

         for(int i$ = 0; i$ < len$; ++i$) {
            Handler handler = arr$[i$];
            handler.setFormatter(form);
            handler.setLevel(level);
         }
      }

   }

   public static void main(String[] args) {
      MonkeyRunnerOptions options = MonkeyRunnerOptions.processOptions(args);
      if(options != null) {
         replaceAllLogFormatters(MonkeyFormatter.DEFAULT_INSTANCE, options.getLogLevel());
         MonkeyRunnerStarter runner = new MonkeyRunnerStarter(options);
         int error = runner.run();
         System.exit(error);
      }
   }
}
